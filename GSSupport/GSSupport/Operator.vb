' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains all basic operator information in its local variables.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable()> Public Class [Operator]
    Private m_operatorID As Integer
    Private m_operatorSymbol As String
    Private m_operatorText As String
    Private m_operatorTypeID As Integer
    Private m_operatorTypeName As String
    Private m_operatorReturnType As Integer

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR 
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with operator symbol argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal mySymbol As String)
        m_operatorSymbol = mySymbol

        Select Case (mySymbol)
            Case ("==")
                m_operatorText = "is"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean
            Case ("!=")
                m_operatorText = "is not"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean
            Case (">")
                m_operatorText = "is greater than"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean
            Case (">=")
                m_operatorText = "is greater than or equal to"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean

            Case ("<")
                m_operatorText = "is less than"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean

            Case ("<=")
                m_operatorText = "is less than or equal to"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean

            Case ("LIKE")
                m_operatorText = "is like"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean

            Case ("NOTLIKE")
                m_operatorText = "is not like"
                m_operatorTypeName = "Comparison"
                m_operatorReturnType = vbBoolean


        End Select
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator ID
    ' ----------  ARPATECH  ----------
    Public Property OperatorID() As Integer
        Get
            Return m_operatorID
        End Get
        Set(ByVal Value As Integer)
            m_operatorID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator type
    ' ----------  ARPATECH  ----------
    Public Property OperatorTypeID() As Integer
        Get
            Return m_operatorTypeID
        End Get
        Set(ByVal Value As Integer)
            m_operatorTypeID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator text
    ' ----------  ARPATECH  ----------
    Public Property OperatorText() As String
        Get
            Return m_operatorText
        End Get
        Set(ByVal Value As String)
            m_operatorText = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator symbol
    ' ----------  ARPATECH  ----------
    Public Property OperatorSymbol() As String
        Get
            Return m_operatorSymbol
        End Get
        Set(ByVal Value As String)
            m_operatorSymbol = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator name
    ' ----------  ARPATECH  ----------
    Public Property OperatorTypeName() As String
        Get
            Return m_operatorTypeName
        End Get
        Set(ByVal Value As String)
            m_operatorTypeName = Value
        End Set
    End Property

End Class

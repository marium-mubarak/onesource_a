' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains all functions for Workflow�s processing like saving,
'                       fetching, updating and some other related functions for other processes.
' Inherits          :   ---------
' Implements        :   IWorkflow, IMQUtils, IGSEventListener, IDisposable
' Friends           :   WorkflowMonitor
' ****************** ARPATECH (General Description) ******************

Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Threading
Imports System.Messaging
Imports GSSupport
Imports System.Data.SqlClient
Imports System.Data
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports GSSupport.GSAppGlobals
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text



<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class Workflow
    Implements IWorkflow
    Implements IMQUtils
    Implements IGSEventListener
    Implements IDisposable


    Friend WorkflowMonitor




    Private m_workflowID As Integer = 0
    Private m_lastUpdate As Date
    Private m_cancelOnError As Boolean = True
    Private m_oncePerSLAInstance As Boolean = False
    Private m_makeInactiveOnCompletion As Boolean = False
    Private m_workflowName As New String("NewWorkflow")
    Private m_workflowDescription As New String("")
    Protected Friend m_workflowSequence As New ArrayList()
    Private m_displayRows As New ArrayList()
    Private m_workflowStepCount As Integer = 0
    Private m_isActive As Boolean = True
    Private m_evaluatedARs As New Hashtable()
    Private m_mqLabel As String
    Private m_gsListener As GSEventListener
    Private m_gsManager As GSSupportManager
    Private m_createSubARS As Boolean = False
    Private handle As IntPtr
    ' Other managed resource this class uses.
    ' Private Components As Component
    ' Track whether Dispose has been called.
    Private disposed As Boolean = False








    ' ----------  ARPATECH  ----------
    '       SAME CLASS INTERFACE
    ' ----------  ARPATECH  ----------
    <System.Runtime.InteropServices.ComVisible(True)> Public Interface IWorkflow
        Property WorkflowName() As String
        Property WorkflowDescription() As String
        Property MQLabel() As String
        ReadOnly Property LastUpdate() As Date
        Property CancelOnError() As Boolean
        Property OncePerSLA() As Boolean
        Property MakeInactiveOnCompletion() As Boolean
        Property IsActive() As Boolean
        ReadOnly Property WorkflowID() As Integer
        Function GetText() As String

        Function AddAction(ByVal a As Action) As Integer
        Function AddActionConj(ByVal ac As String) As Integer
        Function AddQualification(ByVal q As Qualification) As Integer
        Function AddWorkflowStep(ByVal q As Qualification, ByVal a As Action, ByVal ac As String, Optional ByVal s As Integer = 0) As Integer
        Function Validate() As String
        Function GetStepsCount() As Integer
        Function SequenceDump() As String
        Function GetStep(ByVal index As Integer) As WorkflowStep
        Function Load(ByVal workflowID As Integer) As Boolean
        Function GetRowCount() As Integer
        Function Copy() As Workflow
        Property GSManager() As GSSupportManager


        Sub Reset()
        Function Save() As Boolean

    End Interface

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' WF ID
    ' ----------  ARPATECH  ----------
    Public ReadOnly Property WorkflowID() As Integer Implements IWorkflow.WorkflowID
        Get
            Return m_workflowID
        End Get
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' last update date
    ' ----------  ARPATECH  ----------
    Public ReadOnly Property LastUpdate() As Date Implements IWorkflow.LastUpdate
        Get
            Return m_lastUpdate
        End Get
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' inactive on completion status
    ' ----------  ARPATECH  ----------
    Public Property MakeInactiveOnCompletion() As Boolean Implements IWorkflow.MakeInactiveOnCompletion
        Get
            Return m_makeInactiveOnCompletion
        End Get
        Set(ByVal Value As Boolean)
            m_makeInactiveOnCompletion = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' gs manager object
    ' ----------  ARPATECH  ----------
    Public Property GSManager() As GSSupportManager Implements IWorkflow.GSManager
        Get
            Return m_gsManager
        End Get
        Set(ByVal Value As GSSupportManager)
            m_gsManager = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' active status
    ' ----------  ARPATECH  ----------
    Public Property IsActive() As Boolean Implements IWorkflow.IsActive
        Get
            Return m_isActive
        End Get
        Set(ByVal Value As Boolean)
            m_isActive = Value
            If m_isActive = False Then
                Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
                Conn.Open()

                Dim Cmd As New SqlCommand()
                Dim sql As String

                Try

                    sql = "update tblWorkflow set isActive = 0 where workflowID = " & m_workflowID

                    Cmd.Connection = Conn
                    Cmd.CommandText = sql

                    Cmd.ExecuteNonQuery()

                    '  StopListener()

                Catch ex As Exception
                    EventLog.WriteEntry("Application", "Unable to set workflow to inactive: " & ex.Message)
                Finally
                    Conn.Close()

                End Try


            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' on error cancel status
    ' ----------  ARPATECH  ----------
    Public Property CancelOnError() As Boolean Implements IWorkflow.CancelOnError
        Get
            Return m_cancelOnError
        End Get
        Set(ByVal Value As Boolean)
            m_cancelOnError = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' once per SLA
    ' ----------  ARPATECH  ----------
    Public Property OncePerSLA() As Boolean Implements IWorkflow.OncePerSLA
        Get
            Return m_oncePerSLAInstance
        End Get
        Set(ByVal Value As Boolean)
            m_oncePerSLAInstance = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' work flow name
    ' ----------  ARPATECH  ----------
    Public Property WorkflowName() As String Implements IWorkflow.WorkflowName
        Get
            Return m_workflowName
        End Get
        Set(ByVal Value As String)
            m_workflowName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' MQ Label
    ' ----------  ARPATECH  ----------
    Public Property MQLabel() As String Implements IWorkflow.MQLabel
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' work flow description
    ' ----------  ARPATECH  ----------
    Public Property WorkflowDescription() As String Implements IWorkflow.WorkflowDescription
        Get
            Return m_workflowDescription
        End Get
        Set(ByVal Value As String)
            m_workflowDescription = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()


    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with multiple arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal myID As Integer, ByVal myName As String, ByVal doCancel As Boolean, ByVal oncePerSLA As Boolean, ByVal makeInactive As Boolean, ByVal lastUpdate As Date)
        m_workflowID = myID
        m_workflowName = myName
        m_cancelOnError = doCancel
        m_makeInactiveOnCompletion = makeInactive
        m_lastUpdate = lastUpdate
        m_oncePerSLAInstance = oncePerSLA
        LoadSequence()
        '   EventLog.WriteEntry("Application", "Workflow steps loaded; checking subARs")
        SetCreateSubARs()
        '    EventLog.WriteEntry("Application", "Create Sub ARs is " & m_createSubARS & " for workflow " & m_workflowID)


        '  RestartListener()

    End Sub


    ' ----------  ARPATECH  ----------
    ' function for creating MSM queue
    ' ----------  ARPATECH  ----------
    Public Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage
        'Dim f As New System.Messaging.ActiveXMessageFormatter()


        Dim l As String
        Dim m As New MSMQMessage()

        Try
            Dim t As Type = GetType(GSSupport.Workflow)


            Dim serializer As New XmlSerializer(t)

            Dim objxml As New String("")

            Dim ns As New XmlSerializerNamespaces()

            ns.Add("GSSupport", "https://onesource.global-serve.com")

            Dim ms As MemoryStream = New MemoryStream()

            Dim writer As New XmlTextWriter(ms, New UTF8Encoding())

            ' Serialize using the XmlTextWriter.

            serializer.Serialize(writer, Me, ns)

            Dim arChars As Char()
            ReDim arChars(ms.Length)

            ms.ToArray.CopyTo(arChars, 0)

            objxml = New String(arChars)


            writer.Close()
            'Dim mexml As String = ObjToXML(Me)


            m.Label = "WF=" & CStr(m_workflowID) & "&NAME=" & m_workflowName & "&UPDATE=" & Now().ToString()

            m.Body = objxml


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error converting Workflow to message: " & e.Message & "," & e.Source & "," & e.StackTrace)
        End Try
        Return m


    End Function

    ' Sub GetClassID(ByRef classid As System.Guid) Implements IPersist.GetClassID

    '   End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for loading WF sequence 
    ' ----------  ARPATECH  ----------
    Private Sub LoadSequence()

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim StepCount As Integer
        '  EventLog.WriteEntry("Workflow.LoadSequence", "Loading workflow steps")
        StepCount = 0
        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try


            Dim dr As SqlDataReader
            Cmd.CommandText = "select IsNull (qualificationid, 0) as qualificationid, " & _
            " IsNull(actionid, 0) as actionid from tblWorkflowDetail where isActive=1 and workflowid = " & m_workflowID & " order by workflowSeqNum"
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                ' First check if wf exists and update date
                '       EventLog.WriteEntry("Application", "1")
                ' Create a new wf object
                Dim wfs As New WorkflowStep()
                Dim actionID = dr.GetInt32(dr.GetOrdinal("actionid"))
                Dim qualificationID = dr.GetInt32(dr.GetOrdinal("qualificationid"))

                '       EventLog.WriteEntry("Application", "action is " & actionID)

                If actionID > 0 Then
                    wfs.Action = New ActionFactory().CreateAction(actionID)
                End If
                '      EventLog.WriteEntry("Application", "qualificationid  is " & qualificationID)
                If qualificationID > 0 Then
                    wfs.Qualification = New Qualification(qualificationID)
                End If

                m_workflowSequence.Add(wfs)



                StepCount = StepCount + 1
                '            EventLog.WriteEntry("Application", "3")

            End While
            dr.Close()
            m_workflowStepCount = StepCount

        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to update DB:" & e.Message & ", and the sql=" & Cmd.CommandText)

        Finally
            Conn.Close()
            Conn = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function for retriving text from  
    ' qualification
    ' ----------  ARPATECH  ----------
    Public Function GetText() As String Implements IWorkflow.GetText
        Dim s As String = ""
        Dim i As Integer = 0

        ' Add title and description


        For i = 0 To m_workflowSequence.Count
            Dim ws As WorkflowStep
            ws = m_workflowSequence.Item(i)
            If Not IsNothing(ws.Qualification) Then
                s = s & ws.Qualification.GetText()
            End If
            If Not IsNothing(ws.Action) Then
                s = s & ws.Action.ActionName
            End If
        Next

        If Len(s) > 0 Then
            s = "If " & s
        End If


        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function for clearing / reseting workflows
    ' ----------  ARPATECH  ----------
    Public Sub Reset() Implements IWorkflow.Reset
        If Not IsNothing(m_workflowSequence) Then
            m_workflowSequence.Clear()
        End If
        If Not IsNothing(m_displayRows) Then
            m_displayRows.Clear()
        End If

    End Sub

    ' ----------  ARPATECH  ----------
    ' function for adding Work flow step
    ' to work flows steps
    ' ----------  ARPATECH  ----------
    Function AddWorkflowStep(ByVal q As Qualification, ByVal a As Action, Optional ByVal s As Integer = 0) As Integer
        Dim ws As New WorkflowStep(q, a)
        If s > 0 Then
            m_workflowSequence.Insert(s, ws)
        Else
            m_workflowSequence.Add(ws)
        End If
        UpdateDisplayRows()
        Return m_workflowSequence.Count()

    End Function

    ' ----------  ARPATECH  ----------
    ' function for adding work flow steps
    ' to work flows sequence
    ' ----------  ARPATECH  ----------
    Function AddWorkflowStep(ByVal q As Qualification, ByVal a As Action, ByVal ac As String, Optional ByVal s As Integer = 0) As Integer Implements IWorkflow.AddWorkflowStep
        Dim ws As New WorkflowStep(q, a, New Conjunction(ac))
        If s > 0 Then
            m_workflowSequence.Insert(s, ws)
        Else
            m_workflowSequence.Add(ws)
        End If
        UpdateDisplayRows()
        Return m_workflowSequence.Count()

    End Function


    ' ----------  ARPATECH  ----------
    ' function for adding action
    ' to work flows steps
    ' ----------  ARPATECH  ----------
    Function AddAction(ByVal a As Action) As Integer Implements IWorkflow.AddAction
        Dim ws As New WorkflowStep()
        Dim i As Integer
        '   EventLog.WriteEntry("Application", " count is " & m_workflowSequence.Count)
        If Not IsNothing(m_workflowSequence) And m_workflowSequence.Count > 0 Then

            ' See if the action bucket in the last workflow step is empty

            ws = m_workflowSequence.Item(m_workflowSequence.Count - 1)
            If IsNothing(ws.Action) Then
                ws.Action = a
            Else
                ' Otherwise add a new step
                m_workflowSequence.Add(New WorkflowStep(a))
            End If
        End If

        UpdateDisplayRows()

        Return m_workflowSequence.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' function for adding co junction
    ' to work flows steps
    ' ----------  ARPATECH  ----------
    Public Function AddActionConj(ByVal ac As String) As Integer Implements IWorkflow.AddActionConj

        Dim ws As New WorkflowStep()
        Dim i As Integer


        If Not IsNothing(m_workflowSequence) And m_workflowSequence.Count > 0 Then

            ' See if the action bucket in the last workflow step is empty

            ws = m_workflowSequence.Item(m_workflowSequence.Count - 1)

            ws.ActionConjunction = New Conjunction(ac)

        End If
        ' End If

        UpdateDisplayRows()

        Return m_workflowSequence.Count
        ' Return 1

    End Function

    ' ----------  ARPATECH  ----------
    ' function for adding qualification 
    ' to work flows steps
    ' ----------  ARPATECH  ----------
    Function AddQualification(ByVal q As Qualification) As Integer Implements IWorkflow.AddQualification
        If Not IsNothing(m_workflowSequence) And Not IsNothing(q) Then
            m_workflowSequence.Add(New WorkflowStep(q))
        End If

        UpdateDisplayRows()

        Return m_workflowSequence.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' function for validating work flows
    ' ----------  ARPATECH  ----------
    Function Validate() As String Implements IWorkflow.Validate
        Dim s As String = ""
        Dim i As Integer = 0

        UpdateDisplayRows()


        If GetActionCount() < 1 Then
            s = s & "ERROR: You must include at least ONE (1) action clause.\n\r"
        End If

        If Not IsNothing(m_workflowSequence) Then

            For i = 0 To m_workflowSequence.Count - 1
                Dim ws As New WorkflowStep()

                If Not IsNothing(m_workflowSequence.Item(i)) Then
                    ws = m_workflowSequence.Item(i)
                    If Not IsNothing(ws.Qualification) Then
                        ' EventLog.WriteEntry("Application", "check parens is " & ws.Qualification.CheckParens())
                        If Not ws.Qualification.CheckParens() Then
                            '               EventLog.WriteEntry("Application", "check parens is false")
                            s = s & "ERROR: Qualification #" & i + 1 & " has " & ws.Qualification.GetLParens() & _
                            " left parens and " & ws.Qualification.GetRParens() & " right parens.\n\r"

                        End If
                    End If
                End If

            Next

        End If

        If Len(s) < 1 Then
            s = "No errors found."
        End If
        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function to get action counts 
    ' of work flows
    ' ----------  ARPATECH  ----------
    Function GetActionCount() As Integer
        Dim ac As Integer = 0
        Dim i As Integer = 0
        If Not IsNothing(m_workflowSequence) Then


            For i = 0 To m_workflowSequence.Count - 1
                Dim wf As New WorkflowStep()

                If Not IsNothing(m_workflowSequence.Item(i)) Then

                    wf = m_workflowSequence.Item(i)

                    If Not IsNothing(wf.Action) Then

                        If Not IsNothing(wf.Action.ActionID) And wf.Action.ActionID > 0 Then

                            ac = ac + 1
                        End If
                    End If
                End If



            Next
        End If

        Return ac

    End Function

    ' ----------  ARPATECH  ----------
    ' function to get Work Flow steps counts 
    ' ----------  ARPATECH  ----------
    Function GetStepsCount() As Integer Implements IWorkflow.GetStepsCount
        Dim i As Integer = 0
        If Not IsNothing(m_workflowSequence) Then
            i = m_workflowSequence.Count
        End If
        Return i
    End Function

    ' ----------  ARPATECH  ----------
    ' function to squence dumps from WF
    ' ----------  ARPATECH  ----------
    Function SequenceDump() As String Implements IWorkflow.SequenceDump
        Dim s As String
        Dim i
        For i = 0 To m_workflowSequence.Count - 1
            If Not IsNothing(m_workflowSequence.Item(i)) Then
                Dim ws As WorkflowStep
                ws = m_workflowSequence.Item(i)
                If Not IsNothing(ws.Qualification) Then
                    s = s & "Qualification" & i & " = " & ws.Qualification.GetText
                End If
                If Not IsNothing(ws.Action) Then
                    s = s & ", Action" & i & " = " & ws.Action.ActionID
                End If
                s = s & "<br>"
            End If

        Next
        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' function to get Work Flows step
    ' ----------  ARPATECH  ----------
    Function GetStep(ByVal index As Integer) As WorkflowStep Implements IWorkflow.GetStep
        Dim ws As New WorkflowStep()
        If Not IsNothing(m_workflowSequence) Then
            If Not IsNothing(m_workflowSequence.Item(index)) Then
                ws = m_workflowSequence.Item(index)
            End If

        End If
        Return ws
    End Function


    ' ----------  ARPATECH  ----------
    ' function to save Work Flow 
    ' and returns save
    ' ----------  ARPATECH  ----------
    Function Save() As Boolean Implements IWorkflow.Save
        Dim i As Integer = 0

        ' First update DB and get a workflow ID if I don't have one already...
        If Not WFUpdateDB() Then
            EventLog.WriteEntry("Application", "Unable to save workflow to DB")
            Return False

        End If
        ' Next let's make sure each qualification is saved...
        If Not SaveWorkflowSteps() Then
            EventLog.WriteEntry("Application", "Unable to save qualifications.")
            Return False
        End If

        UpdateDisplayRows()

        If Not UpdateQ() Then
            EventLog.WriteEntry("Application", "Unable to save to workflow Q.")
            Return False
        End If




        '  RestartListener()

        Return True


    End Function

    ' ----------  ARPATECH  ----------
    ' function to update Work Flows 
    ' and return process completion status
    ' ----------  ARPATECH  ----------
    Function WFUpdateDB() As Boolean
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim isSubmitted As Boolean = True

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        '   Cmd.Transaction = myTrans
        Cmd.CommandType = CommandType.StoredProcedure

        Try

            If m_workflowID > 0 Then
                Cmd.CommandText = "sp_UpdateWorkflow"
                SqlCommandBuilder.DeriveParameters(Cmd)
                Cmd.Parameters("@workflowid").Value = m_workflowID
                Cmd.Parameters("@workflowname").Value = m_workflowName
                Cmd.Parameters("@workflowdesc").Value = m_workflowDescription
                Cmd.Parameters("@cancelonerror").Value = m_cancelOnError
                Cmd.Parameters("@oncepersla").Value = m_oncePerSLAInstance
                Cmd.Parameters("@inactiveoncomplete").Value = m_makeInactiveOnCompletion
                Cmd.ExecuteNonQuery()
            Else
                Cmd.CommandText = "sp_AddWorkflow"
                SqlCommandBuilder.DeriveParameters(Cmd)
                Cmd.Parameters("@workflowname").Value = m_workflowName
                Cmd.Parameters("@workflowdesc").Value = m_workflowDescription
                Cmd.Parameters("@cancelonerror").Value = m_cancelOnError
                Cmd.Parameters("@oncepersla").Value = m_oncePerSLAInstance
                Cmd.Parameters("@inactiveoncomplete").Value = m_makeInactiveOnCompletion

                ' Output Parameters
                Cmd.Parameters("@workflowid").Value = m_workflowID
                Cmd.ExecuteNonQuery()
                m_workflowID = Cmd.Parameters("@workflowid").Value

            End If






            '      myTrans.Commit()

        Catch e As Exception
            '       myTrans.Rollback()
            isSubmitted = False
            EventLog.WriteEntry("Application", "Failed to update DB:" & e.ToString)
        Finally
            'Cmd = Nothing
            Conn.Close()
        End Try

        Return isSubmitted


    End Function

    ' ----------  ARPATECH  ----------
    ' function to save Work Flows 
    ' and return process completion status
    ' ----------  ARPATECH  ----------
    Function SaveWorkflowSteps() As Boolean

        Dim i As Integer = 0
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()

        Try

            Dim Cmd As New SqlCommand()

            Cmd.Connection = Conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_AddWorkflowStep"


            ' If I already have a workflowID then I'm going delete all existing qualifications
            ' and create new ones.
            If Not DeleteWorkflowSteps() Then
                Return False
            End If

            For i = 0 To m_workflowSequence.Count - 1
                Dim ws As New WorkflowStep()
                If Not IsNothing(m_workflowSequence.Item(i)) Then

                    ws = m_workflowSequence.Item(i)
                    If Not IsNothing(ws.Qualification) Then

                        If Not ws.Qualification.Save Then
                            EventLog.WriteEntry("Application", "Error saving qualification; WF #" & m_workflowID & " step " & i)
                            Return False

                        End If
                    End If

                    If Not IsNothing(ws.Action) Then
                        ' Update DB
                        SqlCommandBuilder.DeriveParameters(Cmd)
                        Cmd.Parameters("@workflowID").Value = m_workflowID
                        Cmd.Parameters("@workflowSeqNum").Value = i
                        Cmd.Parameters("@actionID").Value = ws.Action.ActionID

                        If Not IsNothing(ws.Qualification) Then
                            Cmd.Parameters("@qualificationid").Value = ws.Qualification.QualificationID
                        End If
                        If Not IsNothing(ws.ActionConjunction) Then
                            Cmd.Parameters("@actionConjStr").Value = ws.ActionConjunction.Text
                        End If

                        Cmd.ExecuteNonQuery()

                    End If
                End If
            Next
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Saving Workflow:" & e.Message)
            Return False
        Finally
            Conn.Close()


        End Try

        Return True
    End Function

    ' ----------  ARPATECH  ----------
    ' function to delete Work Flows 
    ' and return process completion status
    ' ----------  ARPATECH  ----------
    Function DeleteWorkflowSteps() As Boolean
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_DeleteWorkflowQualifications"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@workflowid").Value = m_workflowID
            Cmd.ExecuteNonQuery()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error deleting workflow qualifications: " & e.Message)
            Return False

        Finally
            Conn.Close()

        End Try
        Return True

    End Function

    ' ----------  ARPATECH  ----------
    ' function for loading workflow and 
    ' then return
    ' ----------  ARPATECH  ----------
    Function LoadWorkflow(ByVal wfid As Integer) As Boolean Implements IWorkflow.Load
        '  EventLog.WriteEntry("Application", "Loading wf " & wfid)
        m_workflowSequence.Clear()
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        m_workflowID = wfid

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        sql = "select IsNull(qualificationid, 0) as qualificationid, " & _
            " isNull(actionid, 0) as actionid, workflowSeqNum, IsNull(actionConjID, 0) as actionconjid from tblWorkflowDetail where workflowid =" & wfid & _
            " order by workflowSeqNum"

        Cmd.Connection = Conn
        Try

            Cmd.CommandText = sql

            Dim dr As SqlDataReader


            dr = Cmd.ExecuteReader
            While dr.Read()
                Dim qid As Integer
                Dim aid As Integer
                Dim actionconjid As Integer


                qid = dr.GetInt32(dr.GetOrdinal("qualificationid"))
                aid = dr.GetInt32(dr.GetOrdinal("actionid"))
                actionconjid = dr.GetInt32(dr.GetOrdinal("actionconjid"))




                Dim ws As New WorkflowStep()
                If qid > 0 Then
                    '           EventLog.WriteEntry("Application", "creating qualification  " & qid)
                    ws.Qualification = New Qualification(qid)
                    '            EventLog.WriteEntry("Application", "created qual  " & qid)
                End If

                If aid > 0 Then
                    ws.Action = New ActionFactory().CreateAction(aid)
                    If actionconjid > 0 Then
                        ws.ActionConjunction = New Conjunction(actionconjid)

                    End If
                End If

                m_workflowSequence.Add(ws)
            End While
            dr.Close()

            sql = "select * from tblWorkflow where workflowID= " & m_workflowID
            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_workflowName = dr.GetString(dr.GetOrdinal("workflowname"))
                m_workflowDescription = dr.GetString(dr.GetOrdinal("workflowdesc"))
                m_cancelOnError = dr.GetBoolean(dr.GetOrdinal("cancelOnError"))
                m_oncePerSLAInstance = dr.GetBoolean(dr.GetOrdinal("oncepersla"))

                m_makeInactiveOnCompletion = dr.GetBoolean(dr.GetOrdinal("makeInactiveOnCompletion"))
            End While
            dr.Close()
            '   EventLog.WriteEntry("Application", "Workflow loaded and Count is" & GetStepsCount())
            UpdateDisplayRows()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading workflow " & wfid & " from DB: " & e.Message)
            Return False

        Finally
            Conn.Close()

        End Try
        Return True

    End Function


    ' ----------  ARPATECH  ----------
    ' routine for updating work flow 
    ' display rows
    ' ----------  ARPATECH  ----------
    Sub UpdateDisplayRows()
        m_displayRows.Clear()


        Dim i, n
        Dim eventID As Integer

        eventID = CInt(GSSupportManager.GetSingleDBValueInt("select isNull(fieldid, 0) from tblFields where Upper(fieldname) = 'ARCATEGORYID'"))

        For i = 0 To m_workflowSequence.Count - 1
            If Not IsNothing(m_workflowSequence.Item(i)) Then
                Dim ws As WorkflowStep
                ws = m_workflowSequence.Item(i)
                If Not IsNothing(ws.Qualification) Then
                    Dim fu As New Formula()
                    If Not IsNothing(ws.Qualification.Formula) Then
                        fu = ws.Qualification.Formula
                        If Not IsNothing(fu.FormulaDetails) Then
                            Dim isEvent As Boolean = False
                            Dim wasEvent As Boolean = False
                            Dim row As New WorkflowDisplayRow()
                            For n = 0 To fu.FormulaDetails.Count - 1
                                If isEvent Then
                                    wasEvent = True
                                Else
                                    wasEvent = False
                                End If
                                isEvent = False

                                If Not IsNothing(fu.FormulaDetails.Item(n)) Then
                                    Dim fd As New FormulaDetail()
                                    Dim lParenCount As Integer = 0


                                    fd = fu.FormulaDetails.Item(n)
                                    If (wasEvent Or i = 1) AndAlso fd.EndClause > 0 Then
                                        row.EndClause = row.EndClause + (fd.EndClause - 1)
                                    Else
                                        row.EndClause = row.EndClause + fd.EndClause
                                    End If



                                    Dim fi As New Field()

                                    If Not IsNothing(fd.Var1) And fd.Var1.GetType.ToString = "GSSupport.Field" Then
                                        fi = fd.Var1

                                        If fi.FieldID = eventID Then
                                            isEvent = True
                                            row.BeginClause = row.BeginClause + (fd.BeginClause - 1)

                                        Else
                                            row.FieldID = fi.FieldID
                                            row.IsQualification = True
                                            row.IsAction = False
                                            row.BeginClause = row.BeginClause + fd.BeginClause
                                            row.ConjunctionStr = fd.Conjunction.Text
                                            row.OperatorStr = fd.[Operator].OperatorSymbol

                                        End If
                                    End If

                                    If Not IsNothing(fd.Var2) And fd.Var2.GetType.ToString = "GSSupport.Field" Then
                                        fi = fd.Var2

                                        If Not isEvent Then
                                            row.FieldTextValue = fi.FieldTextValue
                                            row.FieldDislayValue = fi.FieldDisplayValue
                                        Else
                                            If IsNumeric(fi.FieldTextValue) Then

                                                row.CategoryID = CInt(fi.FieldTextValue)
                                            End If

                                        End If
                                    End If
                                    ' If I have an ar category and a value then add it
                                    '              EventLog.WriteEntry("Application", "catid is " & row.CategoryID & ", fieldid is " & row.FieldID & ", value is " & row.FieldTextValue)
                                    If row.CategoryID > 0 AndAlso row.FieldID > 0 AndAlso row.FieldID <> eventID Then
                                        '             EventLog.WriteEntry("Application", "adding display row field " & row.FieldID)
                                        m_displayRows.Add(row)
                                        row = New WorkflowDisplayRow()

                                    End If

                                End If
                            Next
                        End If
                    End If
                End If

                If Not IsNothing(ws.Action) Then
                    If ws.Action.ActionID > 0 Then
                        '              EventLog.WriteEntry("Application", "Adding action " & ws.Action.ActionID)
                        Dim r As New WorkflowDisplayRow()
                        r.ActionID = ws.Action.ActionID
                        r.IsAction = True
                        r.IsQualification = False


                        If Not IsNothing(ws.ActionConjunction) Then
                            r.actionConjunctionStr = ws.ActionConjunction.Text
                        End If
                        m_displayRows.Add(r)
                    End If
                End If
            End If
        Next
        '  EventLog.WriteEntry("Application", "In end Step Count is" & GetStepsCount())
    End Sub

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' row count
    ' ----------  ARPATECH  ----------
    Function GetRowCount() As Integer Implements IWorkflow.GetRowCount

        Return m_displayRows.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' display row
    ' ----------  ARPATECH  ----------
    Function GetDisplayRow(ByVal index As Integer) As WorkflowDisplayRow
        Dim m As New WorkflowDisplayRow()
        If Not IsNothing(m_displayRows.Item(index)) Then
            m = m_displayRows.Item(index)
        End If

        Return m

    End Function

    ' ----------  ARPATECH  ----------
    ' function to copy workflows and return
    ' ----------  ARPATECH  ----------
    Function Copy() As Workflow Implements IWorkflow.Copy
        Dim wf As New Workflow()
        Dim i As Integer = 0
        Dim n As Integer = 0


        ' Copy current wf properties
        wf.WorkflowName = m_workflowName & "_Copy"
        wf.WorkflowDescription = m_workflowDescription
        wf.CancelOnError = m_cancelOnError
        wf.MakeInactiveOnCompletion = m_makeInactiveOnCompletion
        wf.m_workflowSequence.Clear()


        ' Set all the IDs to zero so they get saved...

        For i = 0 To m_workflowSequence.Count - 1


            If Not IsNothing(m_workflowSequence.Item(i)) Then
                Dim ws As New WorkflowStep()
                ws = m_workflowSequence.Item(i)


                If Not IsNothing(ws.Qualification) Then
                    Dim q As New Qualification()
                    q = ws.Qualification
                    q.QualificationID = 0
                    If Not IsNothing(q.Formula) Then
                        Dim f As New Formula()
                        f = q.Formula
                        f.FormulaID = 0
                        If Not IsNothing(f.FormulaDetails) Then

                            For n = 0 To f.FormulaDetails.Count - 1
                                If Not IsNothing(f.FormulaDetails.Item(n)) Then
                                    Dim fd As New FormulaDetail()
                                    fd = f.FormulaDetails.Item(n)
                                    fd.FormulaDetailID = 0
                                End If
                            Next

                        End If


                    End If
                    ' ws.Qualification = q
                    wf.AddQualification(q)

                End If
                If Not IsNothing(ws.Action) Then
                    Dim a As Action
                    a = New ActionFactory().CreateAction(ws.Action.ActionID)
                    wf.AddAction(a)
                    If Not IsNothing(ws.ActionConjunction) Then
                        wf.AddActionConj(ws.ActionConjunction.Text)
                    End If

                End If

            End If


        Next


        Return wf


    End Function

    ' ----------  ARPATECH  ----------
    ' updating MSM queue and
    ' returns status
    ' ----------  ARPATECH  ----------
    Function UpdateQ() As Boolean
        Dim b As Boolean = False
        Try
            Dim mes As MSMQMessage = CreateMQMessage()
            Dim label As String = mes.Label

            ' First remove any SLA of the same name...

            Dim strFormatName As String
            Dim qinfo As New MSMQQueueInfo()
            Dim qDest As MSMQQueue

            ' Create a direct format name.
            strFormatName = "DIRECT=OS:" & GSSupportManager.GetQHost() & WorkflowQ


            ' Set the FormatName property of the MSMQQueueInfo object.

            qinfo.FormatName = strFormatName
            qDest = qinfo.Open(MQ_RECEIVE_ACCESS, MQ_DENY_NONE)
            qDest.Reset()

            Dim m As MSMQMessage = qDest.PeekCurrent(ReceiveTimeout:=500)

            ' Peek at messages in the queue using the cursor.
            Do While Not m Is Nothing
                If m.Label = label Then
                    qDest.ReceiveCurrent()
                    Exit Do
                End If
                m = qDest.PeekNext(ReceiveTimeout:=500)
            Loop
            b = SendToQ(IncomingWorkflowQ, CreateMQMessage)
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error sending workflow to Q: " & e.Message)

        End Try
        Return b
    End Function

  


   


   

    ' See if first qualification matches; if so launch workflow monitor.

    ' ----------  ARPATECH  ----------
    ' function the determines that 
    ' passed action request's qualification matches
    ' ----------  ARPATECH  ----------
    Function MatchAR(ByVal ar As ActionRequest, Optional ByRef expr As String = "") As Boolean
        Dim i As Integer = 0
        Dim steps As Integer = 0
        If Not m_isActive Then
            Return False
        End If


        Try


            '      GSSupportManager.LogARFields(ar)

            If Not IsNothing(m_workflowSequence) AndAlso m_workflowSequence.Count > 0 Then

                For i = 0 To m_workflowSequence.Count - 1

                    Dim q As New Qualification()
                    If Not IsNothing(m_workflowSequence(i)) Then

                        '    EventLog.WriteEntry("Application", "i is " & i & " 1")
                        Dim ws As WorkflowStep = m_workflowSequence(i)
                        If Not IsNothing(ws.Qualification) AndAlso ws.Qualification.QualificationID > 0 Then
                            ' Evaluate the action request if a qualification exists
                            ' return number of executed steps if it isn't true.

                            q = ws.Qualification

                            If q.Evaluate(ar, expr) Then


                                ' If it's oncePerSLA/Ticket and we've done it then bail
                                If m_oncePerSLAInstance AndAlso Not IsNothing(ar.GSTicketID) AndAlso ar.GSTicketID > 0 Then
                                    ' EventLog.WriteEntry("Application", "TICKET ID IS " & ar.GSTicketID)
                                    Dim isDoneAlready As Boolean = False
                                    Dim openslaid As Integer = GSSupportManager.GetSingleDBValueInt("select min(fieldid) from tblFields where upper(fieldname)='OPENSLA'")

                                    If ar.ContainsField(openslaid) Then
                                        ' If theres a TACID in the ar get it...
                                        Dim sval As String = ar.GetFieldTextValue(openslaid)
                                        '  EventLog.WriteEntry("Application", "OPEN SLA IS " & sval)
                                        If Len(sval) > 0 AndAlso IsNumeric(sval) Then
                                            isDoneAlready = Me.checkWFSLAExecuted(ar.GSTicketID, CInt(sval))

                                        End If

                                    Else

                                        ' Otherwise check with just the ticket ID
                                        isDoneAlready = Me.checkWFSLAExecuted(ar.GSTicketID, -1)


                                    End If
                                    If isDoneAlready Then
                                        Return False
                                    End If
                                End If


                                ' Still here?  Start the workflow...

                                Dim wm As New WorkflowMonitor(ar, Me)

                                ' If we're once only log that we've done this
                                If m_oncePerSLAInstance AndAlso Not IsNothing(ar.GSTicketID) AndAlso ar.GSTicketID > 0 Then

                                    Dim openslaid As Integer = GSSupportManager.GetSingleDBValueInt("select min(fieldid) from tblFields where upper(fieldname)='OPENSLA'")

                                    If ar.ContainsField(openslaid) Then
                                        ' If theres a TACID in the ar get it...
                                        Dim sval As String = ar.GetFieldTextValue(openslaid)
                                        If Len(sval) > 0 AndAlso IsNumeric(sval) Then
                                            Me.LogSLAWF(ar.GSTicketID, CInt(sval))
                                            Return True
                                        End If

                                    End If

                                    Me.LogSLAWF(ar.GSTicketID, -1)

                                    Return True
                                End If



                                Return True
                            Else
                                Return False
                            End If
                        End If

                    End If

                Next
            End If
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error matching AR for arid:" & ar.ActionRequestID & ", workflow " & Me.WorkflowID & e.Message)
        End Try

        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY sub routine
    ' ----------  ARPATECH  ----------
    Public Sub OnActionCompleteEvent(ByVal sender As Object, ByVal e As ActionCompleteEventArgs) Implements IGSEventListener.OnActionCompleteEvent

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine to be called
    ' on action request event
    ' ----------  ARPATECH  ----------
    Public Sub OnActionRequestEvent(ByVal sender As Object, ByVal e As ActionRequestEventArgs) Implements IGSEventListener.OnActionRequestEvent

        Dim ar As New ActionRequest()
        ar = e.ActionRequest

        '     EventLog.WriteEntry("Application", "wf" & m_workflowID & " got at " & ar.ActionRequestID)
        Dim expr As New String("")
        Dim isMatch As Boolean = MatchAR(ar, expr)
        LogQualificationResult(ar.ActionRequestID, isMatch, expr)

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine to be called on GS timer
    ' event each time
    ' ----------  ARPATECH  ----------
    Public Sub OnGSTimerEvent(ByVal sender As Object, ByVal e As GSTimerEventArgs) Implements IGSEventListener.OnGSTimerEvent
        'Exit Sub
        Dim isTimerEvent As Boolean
        isTimerEvent = False
        isTimerEvent = GSSupportManager.IsWorkFlowWithTimer(m_workflowID)

        If (isTimerEvent) Then
            EventLog.WriteEntry("Application", "isTimerEvent has value " & isTimerEvent & " for workFlow " & m_workflowID)
            EventLog.WriteEntry("Application", "wf " & m_workflowID & " got timer event")
            If IsNothing(sender) OrElse IsNothing(e) Then
                Exit Sub
            End If
            Dim ar As New ActionRequest()
            ar = e.ActionRequest
            Dim newar As New ActionRequest()
            Dim i As Integer = 0
            newar.CategoryName = "Time"
            newar.ActionRequestID = ar.ActionRequestID
            '    newAR.CategoryID = ar.CategoryID
            '    newAR.CategoryName = ar.CategoryName
            newar.IsChild = True
            newar.Qualify = False
            newar.GenerateTicket = False

            For i = 0 To ar.Fields.Count - 1
                Dim f As Field = ar.Fields.Item(i)
                If Not IsNothing(f) AndAlso Not IsNothing(f.FieldName) Then
                    If InStr(UCase(f.FieldName), "GMT") > 0 Then
                        newar.AddField(f)
                    End If
                End If

            Next

            Dim expr As New String("")
            Dim isMatch As Boolean = MatchAR(ar, expr)
            LogQualificationResult(newar.ActionRequestID, isMatch, expr)
            If m_createSubARS Then 'AndAlso Not ar.IsChild Then
                ' Gotta create a duplicate AR for each ticket and open SLA
                EvaluateOpenTicketARs(newar)

            End If
        Else
            'EventLog.WriteEntry("Application", "wf " & m_workflowID & " is not 142 or 144")
            Exit Sub
        End If
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for creating action
    ' request accorging to th tickets
    ' ----------  ARPATECH  ----------
    Sub SetCreateSubARs()
        Dim timeEventID, ARCategoryFieldID As Integer
        Dim arCheckAllFields As New ArrayList()


        'Get Time AR Category ID
        '  timeEventID = GSSupportManager.GetSingleDBValueInt("select ARCatID from tblARCategories where ARCategoryName='Time'")
        '   ARCategoryFieldID = GSSupportManager.GetSingleDBValueInt("select fieldID from tblFields where fieldname='arcategoryid'")


        ' Now get all the fields that require checking for each ticket
        Dim sql As String
        sql = "select fieldID from tblFields where checkForEachTkt = 1 and isActive=1"
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim i, n, z As Integer

        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader

            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            While dr.Read()
                arCheckAllFields.Add(dr.GetInt32(0))
            End While
            dr.Close()

            ' Now check each qualification for Time Category type and 
            ' checkForEachTicket field.
            If Not IsNothing(m_workflowSequence) AndAlso m_workflowSequence.Count > 0 Then

                For i = 0 To m_workflowSequence.Count - 1


                    Dim q As New Qualification()
                    If Not IsNothing(m_workflowSequence(i)) Then
                        Dim ws As WorkflowStep = m_workflowSequence(i)
                        If Not IsNothing(ws.Qualification) AndAlso ws.Qualification.QualificationID > 0 Then
                            ' Evaluate the action request if a qualification exists
                            ' return number of executed steps if it isn't true.
                            q = ws.Qualification
                            If Not IsNothing(q.Formula) Then
                                Dim f As Formula = q.Formula
                                For n = 0 To f.FormulaDetails.Count - 1
                                    ' EventLog.WriteEntry("Application", "formula detail count is  " & n)
                                    Dim fd As FormulaDetail = f.FormulaDetails.Item(n)
                                    '  EventLog.WriteEntry("Application", "fd var1 type is  " & fd.Var1.GetType.ToString())

                                    If fd.Var1.GetType.ToString = "GSSupport.Field" Then
                                        Dim fld As Field = fd.Var1
                                        ' EventLog.WriteEntry("Application", "field ID is  " & fld.FieldID)

                                        For z = 0 To arCheckAllFields.Count - 1
                                            If CInt(arCheckAllFields.Item(z)) = fld.FieldID Then

                                                m_createSubARS = True
                                                Exit Sub

                                            End If

                                        Next
                                    End If

                                Next
                                ' Only need to check first qualification
                            End If



                        End If
                    End If

                Next


            End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error setting wf.createsubars value: " & e.Message)


        Finally
            Conn.Close()

        End Try

        m_createSubARS = False



    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine to be called
    ' on ticeting event
    ' ----------  ARPATECH  ----------
    Public Sub OnTicketEvent(ByVal sender As Object, ByVal e As TicketEventArgs) Implements IGSEventListener.OnTicketEvent
        '    EventLog.WriteEntry("Application", Me.WorkflowID & " RECEIVED TICKET EVENT")
        Dim ar As New ActionRequest()
        ar = e.CreateAR()
        Dim expr As New String("")
        '   GSSupportManager.LogARDetails(ar)
        Dim isMatch As Boolean = MatchAR(ar, expr)
        LogQualificationResult(ar.ActionRequestID, isMatch, expr)


    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Listner
    ' ----------  ARPATECH  ----------
    Public Property GSListener() As GSEventListener Implements IGSEventListener.GSListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            m_gsListener = Value

            ' Register the events I'm interested in...
            AddHandler m_gsListener.ActionRequestEvent, New ActionRequestEventHandler(AddressOf Me.OnActionRequestEvent)
            AddHandler m_gsListener.TicketEvent, New TicketEventHandler(AddressOf Me.OnTicketEvent)
            AddHandler m_gsListener.OrderEvent, New OrderEventHandler(AddressOf Me.OnOrderEvent)
            ' I am uncommenting the following part , this will start the evaluation of workflow
            AddHandler m_gsListener.GSTimerEvent, New GSTimerEventHandler(AddressOf Me.OnGSTimerEvent)


        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for qualification
    ' logging in DB
    ' ----------  ARPATECH  ----------
    Sub LogQualificationResult(ByVal arid As Integer, ByVal isMatch As Boolean, Optional ByVal expr As String = "")
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim m As Integer
        If isMatch Then
            m = 1
        Else
            m = 0
        End If

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Try

            sql = "insert into tblQualificationLog (actionRequestID, workflowid, isMatch, expression) values (" & arid & "," & m_workflowID & "," & m & ", '" & expr & "')"

            Cmd.Connection = Conn
            Cmd.CommandText = sql

            Cmd.ExecuteNonQuery()


        Catch ex As Exception
            EventLog.WriteEntry("Application", "Unable to update workflow qualificaltion log: " & sql & ex.Message)
        Finally
            Conn.Close()

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub orutine for evaluating open ticket
    ' with respective action request
    ' ----------  ARPATECH  ----------
    Sub EvaluateOpenTicketARs(ByVal ar As ActionRequest)
        ' EventLog.WriteEntry("Application", "In EvaulateOpenTicketARs")
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim m As Integer
        Dim tmpDate As Date
        Dim newAR As New ActionRequest()


        Dim Cmd As New SqlCommand()
        Dim sql As String

        Try

            sql = "select distinct t.TicketContactName, t.TicketContactPhoneVoice, t.ticketid, isnull(c3.workphonenumber, '') as submittinguserphone, isnull(c.workphonenumber, '') as affectedcontactphone, isnull(ass.serialnum,'') as assetserial, t.customerTicketID, t.sfaTicketID, ar.companyid, ar.arname, ar.submittedbyuserid, u.username, ar.ardesc, ar.addressid, IsNull(co1.countryid, 0) as usercountryid, " & _
            " IsNull(co1.countryname, '') as usercountryname, t.AppointmentDate, " & _
            " IsNull(co2.countryid, 0) as assetcountryid,  ca.address1, ca.address2, ca.city, ca.stateprovinceregion, ca.postalcode, " & _
            " ca.coverageid as addresscoverage, ass.coverageid as assetcoverage, " & _
            " IsNull(st.IsBreach, 0) as isbreach, IsNull(st.closedWithinSLA, 0) as isclosedwithinsla, isnull(st.isException, 0) as isexception, " & _
            " IsNull(co2.countryname, '') as assetcountryname, ca.customeraddressname as addressname, isnull(tz.timezonename, '') as timezonename, " & _
            " ar.arTypeID, art.arTypeName, ar.requestdatelocal, ar.requestDate, ar.DateReceived, t.createdate, t.priorityid, t.sfaid, " & _
            " t.csrid, t.escalationLevel, t.tktstatusid, sfa.sfaname, IsNull(c.contactid, '') as contactid,  " & _
            " IsNull(st.minutestobreach, 0) as 'minstobreach', ts.tktStatusName, tp.PriorityName, st.slainstanceid,  " & _
            " st.tacid, s.slaname, tac.tacname, c2.email as csremail, c2.firstname + ' ' + c2.lastname as 'csrname',  " & _
            " t.lastupdate, awa.customeraddressid as assetaddressid, " & _
            " IsNull(tac.maxminutes - isnull(st.minutesToBreach, 0), 0) as elapsedslamins, " & _
            " c.email as affectedcontactemail, c3.email as submittinguseremail, IsNull(ts.statusIntValue, 0) as statusIntValue " & _
            " from tblTicket t  join tblActionRequest ar on t.arid = ar.arid  " & _
            "  join tblActionRequestType art on art.arTypeID = ar.arTypeID  " & _
            " left join tblContact c on c.contactid = ar.affectedUserContactid  " & _
            " left join tblContact c2 on t.csrid = c2.contactid  " & _
            " left join tblContact c3 on ar.submittedbyuserid = c3.userid " & _
            "  join tblParentCompanies p on ar.companyid = p.parentcompanyid  " & _
            "  join tblTicketStatus ts on t.tktstatusid = ts.tktstatusid  " & _
            "  join tblPriority tp on  t.priorityid = tp.priorityid  " & _
            " left join tblSLATracking st on t.ticketid = st.ticketid  " & _
            " left join tblTermsAndConditions tac on tac.tacid = st.tacid  " & _
            " left join tblSLA s on tac.slaid = s.slaid  " & _
            "  join tblCustomerAddresses ca on ar.addressid = ca.customeraddressid   " & _
            "  join tblCountries co1 on co1.countryid = ca.countryid   " & _
            " left join GSNetP_tbl_assets ass on ar.assetid = ass.assetid   " & _
            "  left join GSNetP_tbl_AddrWithAssets awa on ass.addrwithassetsid = awa.addrwithassetsid   " & _
            " left join tblCountries co2 on co2.countryid = awa.countryid   " & _
            " left join tblSFAs sfa on sfa.sfaid = t.sfaid   " & _
            " left join tblUsers u on u.userid = ar.submittedbyuserid " & _
            " left join tblTimeZones tz on tz.timezoneid = ar.requesttimezoneid " & _
            " where ts.isClosed !=1 and ts.isCancelled != 1 " & _
            " and p.parentcompanyname not like '%DEMO%' and ar.isActive=1 and t.isactive=1 " & _
            " and upper(ar.arname) not like '%GSTEST%' and upper(username) not like '%DEMO%' "
            Cmd.Connection = Conn
            Cmd.CommandText = sql
            '   EventLog.WriteEntry("Application", sql)

            Dim dr As SqlDataReader


            dr = Cmd.ExecuteReader
            While dr.Read()
                ' Check values
                Dim tmps As String = "'"
                Dim iii As Integer = 0
                For iii = 0 To dr.FieldCount - 1
                    tmps = tmps & dr.GetValue(iii).ToString() & ","
                Next

                Dim createAR As Boolean = True
                newAR = New ActionRequest()


                If m_oncePerSLAInstance Then
                    ' If there's not TACID just use ticketid
                    If dr.IsDBNull(dr.GetOrdinal("tacid")) Then
                        If checkWFSLAExecuted(dr.GetInt32(dr.GetOrdinal("ticketid")), -1) Then
                            createAR = False
                            ' EventLog.WriteEntry("Application", "CreateAR is false for no tacid, tkt " & dr.GetInt32(dr.GetOrdinal("ticketid")))
                        End If
                    Else
                        If Not dr.IsDBNull(dr.GetOrdinal("ticketid")) AndAlso Not dr.IsDBNull(dr.GetOrdinal("tacid")) Then
                            If checkWFSLAExecuted(dr.GetInt32(dr.GetOrdinal("ticketid")), dr.GetInt32(dr.GetOrdinal("tacid"))) Then
                                createAR = False
                                '  EventLog.WriteEntry("Application", "CreateAR is false for tacid" & dr.GetInt32(dr.GetOrdinal("tacid")) & ", tkt " & dr.GetInt32(dr.GetOrdinal("ticketid")))

                            End If
                        End If
                    End If
                    ' If there's an existing workflow instance with my workflow ID for this
                    ' SLA instance then bail...

                End If

                '  If Not createAR Then
                '    EventLog.WriteEntry("Application", "Create ar false for ticket " & dr.GetInt32(dr.GetOrdinal("ticketid")))

                '   End If


                If createAR Then


                    '        newAR = ar
                    newAR.ActionRequestID = ar.ActionRequestID
                    newAR.CategoryID = ar.CategoryID
                    newAR.CategoryName = ar.CategoryName
                    newAR.IsChild = True
                    newAR.Qualify = False
                    newAR.GenerateTicket = False

                    ' Add date fields

                    '    EventLog.WriteEntry("Application", "1")
                    newAR.GSTicketID = dr.GetInt32(dr.GetOrdinal("ticketid"))
                    '    EventLog.WriteEntry("Application", "AR ticket id is " & ar.GSTicketID)
                    newAR.AddFieldByDisplay("GS Ticket Number", CStr(dr.GetInt32(dr.GetOrdinal("ticketid"))))
                    newAR.AddFieldByDisplay("Ticket Submitted By", CStr(dr.GetInt32(dr.GetOrdinal("submittedbyuserid"))), dr.GetString(dr.GetOrdinal("username")))
                    newAR.SetCompanyID(dr.GetInt32(dr.GetOrdinal("companyID")), CStr(dr.GetInt32(dr.GetOrdinal("companyID"))))


                    If Not dr.IsDBNull(dr.GetOrdinal("tacid")) Then
                        newAR.AddFieldByDisplay("Open SLA", CStr(dr.GetInt32(dr.GetOrdinal("tacid"))), dr.GetString(dr.GetOrdinal("slaname")) + "." + dr.GetString(dr.GetOrdinal("tacname")))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("affectedcontactemail")) Then
                        newAR.AddFieldByDisplay("Email of Affected User", CStr(dr.GetString(dr.GetOrdinal("affectedcontactemail"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("affectedcontactphone")) Then
                        newAR.AddFieldByDisplay("Affected Contact Phone", CStr(dr.GetString(dr.GetOrdinal("affectedcontactphone"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("submittinguserphone")) Then
                        newAR.AddFieldByDisplay("Submitting User Phone", CStr(dr.GetString(dr.GetOrdinal("submittinguserphone"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("addresscoverage")) Then
                        newAR.AddFieldByDisplay("Coverage Type", CStr(dr.GetInt32(dr.GetOrdinal("addresscoverage"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("assetcoverage")) Then
                        newAR.AddFieldByDisplay("Coverage Type", CStr(dr.GetInt32(dr.GetOrdinal("assetcoverage"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("submittinguseremail")) Then
                        newAR.AddFieldByDisplay("Email of Submitting User", CStr(dr.GetString(dr.GetOrdinal("submittinguseremail"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("assetserial")) Then
                        newAR.AddFieldByDisplay("Serial Number", CStr(dr.GetString(dr.GetOrdinal("assetserial"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("customerTicketID")) Then
                        newAR.AddFieldByDisplay("Customer Ticket ID", CStr(dr.GetString(dr.GetOrdinal("customerTicketID"))))
                    End If

                    Dim addressText As New String("")

                    If Not dr.IsDBNull(dr.GetOrdinal("addressname")) Then
                        newAR.AddFieldByDisplay("Address Name", dr.GetString(dr.GetOrdinal("addressname")))
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("addressname"))) & vbCrLf

                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("address1")) Then
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("address1"))) & vbCrLf
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("address2")) Then
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("address2"))) & vbCrLf
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("city")) Then
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("city"))) & " "
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("stateprovinceregion")) Then
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("stateprovinceregion"))) & " "
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("postalcode")) Then
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("postalcode"))) & " "
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("usercountryname")) Then
                        addressText = addressText & CStr(dr.GetString(dr.GetOrdinal("usercountryname")))
                    End If



                    If Not dr.IsDBNull(dr.GetOrdinal("sfaTicketID")) Then
                        newAR.AddFieldByDisplay("GSP Ticket ID", CStr(dr.GetString(dr.GetOrdinal("sfaTicketID"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("TicketContactName")) Then
                        newAR.AddFieldByDisplay("Ticket Contact Name", CStr(dr.GetString(dr.GetOrdinal("TicketContactName"))))
                    Else
                        newAR.AddFieldByDisplay("Ticket Contact Name", "")

                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("TicketContactPhoneVoice")) Then
                        newAR.AddFieldByDisplay("Ticket Contact Phone", CStr(dr.GetString(dr.GetOrdinal("TicketContactPhoneVoice"))))
                    Else
                        newAR.AddFieldByDisplay("Ticket Contact Phone", "")
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("arname")) Then
                        newAR.AddFieldByDisplay("Ticket Summary", CStr(dr.GetString(dr.GetOrdinal("arname"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("ardesc")) Then
                        newAR.AddFieldByDisplay("Ticket Description", CStr(dr.GetString(dr.GetOrdinal("ardesc"))))
                    End If

                    If dr.GetBoolean(dr.GetOrdinal("isbreach")) = True Then
                        newAR.AddFieldByDisplay("SLA Resolution", "3", "Breach")
                    ElseIf dr.GetBoolean(dr.GetOrdinal("isclosedwithinsla")) = True Then
                        newAR.AddFieldByDisplay("SLA Resolution", "1", "Closed Within SLA")
                    ElseIf dr.GetBoolean(dr.GetOrdinal("isexception")) = True Then
                        newAR.AddFieldByDisplay("SLA Resolution", "2", "Exception")
                    End If

                    If dr.GetInt32(dr.GetOrdinal("minstobreach")) > 0 Then
                        newAR.AddFieldByDisplay("Minutes Until Violation", CStr(dr.GetInt32(dr.GetOrdinal("minstobreach"))))
                        newAR.AddFieldByDisplay("Hours Until Violation", CStr(dr.GetInt32(dr.GetOrdinal("minstobreach")) / 60))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("elapsedslamins")) Then
                        newAR.AddFieldByDisplay("Elapsed SLA Minutes", CStr(dr.GetInt32(dr.GetOrdinal("elapsedslamins"))))
                    End If


                    newAR.AddFieldByDisplay("Ticket Priority", CStr(dr.GetInt32(dr.GetOrdinal("priorityid"))), dr.GetString(dr.GetOrdinal("priorityName")))
                    newAR.SetTypeID(dr.GetInt32(dr.GetOrdinal("arTypeID")), dr.GetString(dr.GetOrdinal("arTypeName")))

                    newAR.AddFieldByDisplay("Request Type", CStr(dr.GetInt32(dr.GetOrdinal("arTypeID"))), dr.GetString(dr.GetOrdinal("arTypeName")))
                    newAR.AddFieldByDisplay("Ticket Status", CStr(dr.GetInt32(dr.GetOrdinal("tktstatusid"))), dr.GetString(dr.GetOrdinal("tktStatusName")))
                    newAR.AddFieldByDisplay("Ticket Status Value", CStr(dr.GetInt32(dr.GetOrdinal("statusIntValue"))), dr.GetString(dr.GetOrdinal("tktStatusName")))

                    newAR.AddFieldByDisplay("Ticket Escalation Level", CStr(dr.GetInt32(dr.GetOrdinal("escalationlevel"))))

                    If Not dr.IsDBNull(dr.GetOrdinal("usercountryid")) Then
                        newAR.AddFieldByDisplay("Country of Affected User", CStr(dr.GetInt32(dr.GetOrdinal("usercountryid"))), dr.GetString(dr.GetOrdinal("usercountryname")))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("assetcountryid")) Then
                        newAR.AddFieldByDisplay("Country of Affected Asset", CStr(dr.GetInt32(dr.GetOrdinal("assetcountryid"))), dr.GetString(dr.GetOrdinal("assetcountryname")))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("addressid")) Then
                        newAR.AddFieldByDisplay("Address of Affected User", CStr(dr.GetInt32(dr.GetOrdinal("addressid"))), addressText)
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("assetaddressid")) Then
                        newAR.AddFieldByDisplay("Address of Affected Asset", CStr(dr.GetInt32(dr.GetOrdinal("assetaddressid"))), addressText)
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("AppointmentDate")) AndAlso Year(dr.GetDateTime(dr.GetOrdinal("AppointmentDate"))) > 2000 Then
                        newAR.AddFieldByDisplay("Appointment Date", CStr(dr.GetDateTime(dr.GetOrdinal("AppointmentDate"))))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("sfaid")) Then
                        newAR.AddFieldByDisplay("FSC", CStr(dr.GetInt32(dr.GetOrdinal("sfaid"))))
                        ' If there's an SFA get SFA email addresses...
                        Dim addid As Integer = 0
                        If Not dr.IsDBNull(dr.GetOrdinal("addressid")) Then
                            addid = dr.GetInt32(dr.GetOrdinal("addressid"))
                        End If
                        If addid < 1 And Not dr.IsDBNull(dr.GetOrdinal("assetaddressid")) Then
                            addid = dr.GetInt32(dr.GetOrdinal("assetaddressid"))
                        End If
                        'If addid > 0 Then
                        'Dim s As String = GSManager.GetSFAEmails(dr.GetInt32(dr.GetOrdinal("sfaid")), addid)
                        'newAR.AddFieldByDisplay("Email of Assigned FSC", s)
                        'End If
                        Dim s As String = GSManager.GetSFAEmails(dr.GetInt32(dr.GetOrdinal("sfaid")))
                        newAR.AddFieldByDisplay("Email of Assigned FSC", s)
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("csrid")) Then
                        newAR.AddFieldByDisplay("CSR", CStr(dr.GetInt32(dr.GetOrdinal("csrid"))), dr.GetString(dr.GetOrdinal("csrname")))
                        If Not dr.IsDBNull(dr.GetOrdinal("csremail")) Then
                            newAR.AddFieldByDisplay("Email of Assigned CSR", dr.GetString(dr.GetOrdinal("csremail")))
                        End If


                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("createDate")) AndAlso IsDate(dr.GetDateTime(dr.GetOrdinal("createDate"))) Then
                        tmpDate = dr.GetDateTime(dr.GetOrdinal("createDate"))
                        If Year(tmpDate) > MinYear Then

                            newAR.AddFieldByDisplay("Minutes Since Ticket Creation", CStr(DateDiff(DateInterval.Minute, tmpDate, Now())))
                            newAR.AddFieldByDisplay("Hours Since Ticket Creation", CStr(DateDiff(DateInterval.Hour, tmpDate, Now())))
                        End If
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("lastupdate")) AndAlso IsDate(dr.GetDateTime(dr.GetOrdinal("lastupdate"))) Then
                        tmpDate = dr.GetDateTime(dr.GetOrdinal("lastupdate"))
                        newAR.AddFieldByDisplay("Minutes Since Ticket Update", CStr(DateDiff(DateInterval.Minute, tmpDate, Now())))
                        newAR.AddFieldByDisplay("Hours Since Ticket Update", CStr(DateDiff(DateInterval.Hour, tmpDate, Now())))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("requestdate")) AndAlso IsDate(dr.GetDateTime(dr.GetOrdinal("requestdate"))) Then
                        tmpDate = dr.GetDateTime(dr.GetOrdinal("requestdate"))


                        If Year(tmpDate) > MinYear Then
                            '  EventLog.WriteEntry("Application", " elapsedhrsreq= " & CStr(CInt(DateDiff(DateInterval.Hour, tmpDate, Now()))))
                            ' EventLog.WriteEntry("Application", "Year is " & Year(tmpDate))
                            newAR.AddFieldByDisplay("Minutes Since Request", CStr(DateDiff(DateInterval.Minute, tmpDate, Now())))
                            '  ar.AddField("elapsedhrsreq", CStr(CInt(DateDiff(DateInterval.Hour, tmpDate, Now()))))

                        End If

                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("requestdatelocal")) AndAlso IsDate(dr.GetDateTime(dr.GetOrdinal("requestdatelocal"))) Then
                        tmpDate = dr.GetDateTime(dr.GetOrdinal("requestdatelocal"))
                        newAR.AddFieldByDisplay("Request Date (Local Time)", tmpDate, tmpDate & " " & dr.GetString(dr.GetOrdinal("timezonename")))
                        newAR.AddFieldByDisplay("Ticket Submitted Date", tmpDate, tmpDate & " " & dr.GetString(dr.GetOrdinal("timezonename")))
                    End If

                    newAR.IsChild = True
                    newAR.AddFieldByDisplay("Ticket CSM Changed", "False", "False")
                    newAR.AddFieldByDisplay("Ticket GSP Changed", "False", "False")
                    newAR.AddFieldByDisplay("Ticket Status Changed", "False", "False")
                    newAR.AddFieldByDisplay("Ticket Priority Changed", "False", "False")
                    newAR.AddFieldByDisplay("Ticket Escalation Changed", "False", "False")
                    newAR.AddFieldByDisplay("Ticket Comment Added", "False", "False")
                    Dim expr As New String("")
                    GSSupportManager.LogARDetails(newAR)
                    ' EventLog.WriteEntry("Application", "2")

                    Dim isMatch As Boolean = MatchAR(newAR, expr)

                    '  EventLog.WriteEntry("Application", "3")

                    ' If Me.WorkflowID = 80 Then
                    '  EventLog.WriteEntry("Application", "ticket is " & ar.GSTicketID & " , " & " isMatch is " & isMatch & " expr is " & expr)

                    ' End If
                    '    GSSupportManager.LogFieldsToEventLog()
                    '   LogQualificationResult(newAR.ActionRequestID, isMatch, expr)


                End If


            End While

            dr.Close()

        Catch ex As Exception
            newAR = New ActionRequest()
            EventLog.WriteEntry("Application", "Error in EvaluateOpenTicketAR: " & ex.Message)

        Finally
            Conn.Close()

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine to call finalize
    ' ----------  ARPATECH  ----------
    Public Sub Destroy()
        Me.Finalize()
    End Sub

    ' Implement Idisposable.
    ' Do not make this method Overridable.
    ' A derived class should not be able to override this method.
    ' ----------  ARPATECH  ----------
    ' another dispose method with different argument
    ' in place of finalize
    ' ----------  ARPATECH  ----------
    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Try


            '  EventLog.WriteEntry("Application", "disposing wf hash " & Me.GetType().GetHashCode())
            If Not IsNothing(m_gsListener) Then
                RemoveHandler m_gsListener.ActionRequestEvent, New ActionRequestEventHandler(AddressOf Me.OnActionRequestEvent)
                RemoveHandler m_gsListener.TicketEvent, New TicketEventHandler(AddressOf Me.OnTicketEvent)
                RemoveHandler m_gsListener.GSTimerEvent, New GSTimerEventHandler(AddressOf Me.OnGSTimerEvent)
                m_gsListener = Nothing
            End If


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Disposing of Workflow: " & e.Message)


        Finally


            Dispose(True)
        End Try




        ' Take yourself off of the finalization queue
        ' to prevent finalization code for this object
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ' ----------  ARPATECH  ----------
    ' inserts work flow log in database
    ' ----------  ARPATECH  ----------
    Private Sub LogSLAWF(ByVal ticketid As Integer, ByVal tacid As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim sql As String
        If tacid > 0 Then
            sql = "insert into tblSLAWFLog (workflowid, ticketid, tacid) values (" & m_workflowID & "," & ticketid & "," & tacid & ")"
        Else
            sql = "insert into tblSLAWFLog (workflowid, ticketid ) values (" & m_workflowID & "," & ticketid & ")"

        End If


        Try
            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            Cmd.CommandText = sql
            Cmd.ExecuteNonQuery()

        Catch e As Exception

            EventLog.WriteEntry("Application", "Error logging SLAWF for " & m_workflowID & ":" & e.Message)

        Finally
            Conn.Close()
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that checks either work flow SLA
    ' has executed
    ' ----------  ARPATECH  ----------
    Private Function checkWFSLAExecuted(ByVal ticketID As Integer, ByVal tacID As Integer) As Boolean
        Dim b As Boolean = False
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim sql As String
        If tacID < 1 Then
            sql = "select count(ticketID) from tblSLAWFLog where tacid is null and ticketID=" & ticketID & " and " & _
                 " workflowid = " & m_workflowID

        Else
            sql = "select count(tacid) from tblSLAWFLog where ticketID=" & ticketID & " and " & _
                   " tacid = " & tacID & " and workflowid = " & m_workflowID

        End If


        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Dim dr As SqlDataReader
        Cmd.CommandText = sql
        dr = Cmd.ExecuteReader
        Try

            While dr.Read()

                Dim i As Integer = dr.GetInt32(0)

                If i > 0 Then
                    b = True
                End If
            End While
        Catch e As Exception

            EventLog.WriteEntry("Application", "Error checking SLA Executed for WF " & m_workflowID)


        Finally
            Conn.Close()
        End Try
        Return b
    End Function


    ' ----------  ARPATECH  ----------
    ' dispose if we didn't call finalize
    ' ----------  ARPATECH  ----------
    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        ' Check to see if Dispose has already been called.
        If Not (Me.disposed) Then
            ' If disposing equals true, dispose all managed 
            ' and unmanaged resources.
            If (disposing) Then
                ' Dispose managed resources.
                '    Components.Dispose()
            End If
            ' Release unmanaged resources. If disposing is false,
            ' only the following code is executed.      
            ' CloseHandle(handle)
            handle = IntPtr.Zero
            ' Note that this is not thread safe.
            ' Another thread could start disposing the object
            ' after the managed resources are disposed,
            ' but before the disposed flag is set to true.
        End If
        Me.disposed = True
    End Sub

    ' ----------  ARPATECH  ----------
    ' finalize on object destory
    ' ----------  ARPATECH  ----------
    ' This Finalize method will run only if the 
    ' Dispose method does not get called.
    ' By default, methods are NotOverridable. 
    ' This prevents a derived class from overriding this method.
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that checks either work flow SLA
    ' has executed
    ' ----------  ARPATECH  ----------
    Function hasExecuted(ByVal tktid As Integer, ByVal tacid As Integer) As Boolean
        Dim b As Boolean = False
        If m_oncePerSLAInstance Then
            ' If there's not TACID just use ticketid
            If tacid < 1 Then
                Return checkWFSLAExecuted(tktid, -1)

            End If
        Else
            Return checkWFSLAExecuted(tktid, tacid)



        End If

        Return b
    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine for ordering event
    ' ----------  ARPATECH  ----------
    Public Sub OnOrderEvent(ByVal sender As Object, ByVal e As OrderEventArgs) Implements IGSEventListener.OnOrderEvent
        Try
            Dim ar As ActionRequest = e.ActionRequest
            '  EventLog.WriteEntry("Application", "workflow " & Me.WorkflowID & " RECEIVED ORDER EVENT AR" & ar.ActionRequestID)

            Dim expr As New String("")
            '   GSSupportManager.LogARDetails(ar)
            Dim isMatch As Boolean = MatchAR(ar, expr)
            LogQualificationResult(ar.ActionRequestID, isMatch, expr)

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Workflow OnOrderEvent error:" & ex.Message)

        End Try




    End Sub




End Class

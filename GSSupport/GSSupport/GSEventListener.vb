' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is an event listener for Ticket, Order, ActionRequest,
'                       ActionComplete and GSTimerEvent.
' Inherits          :   ---------
' Implements        :   IGSEventListner
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Delegate Sub TicketEventHandler(ByVal sender As Object, ByVal e As TicketEventArgs)
<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Delegate Sub OrderEventHandler(ByVal sender As Object, ByVal e As OrderEventArgs)
<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Delegate Sub ActionRequestEventHandler(ByVal sender As Object, ByVal e As ActionRequestEventArgs)
<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Delegate Sub ActionCompleteEventHandler(ByVal sender As Object, ByVal e As ActionCompleteEventArgs)
<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Delegate Sub GSTimerEventHandler(ByVal sender As Object, ByVal e As GSTimerEventArgs)

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
Public Interface IGSEventListener

    Property GSListener() As GSEventListener
    Sub OnTicketEvent(ByVal sender As Object, ByVal e As TicketEventArgs)
    Sub OnOrderEvent(ByVal sender As Object, ByVal e As OrderEventArgs)
    Sub OnActionRequestEvent(ByVal sender As Object, ByVal e As ActionRequestEventArgs)
    Sub OnGSTimerEvent(ByVal sender As Object, ByVal e As GSTimerEventArgs)
    Sub OnActionCompleteEvent(ByVal sender As Object, ByVal e As ActionCompleteEventArgs)

End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class GSEventListener
    Public Event TicketEvent As TicketEventHandler
    Public Event OrderEvent As OrderEventHandler
    Public Event ActionRequestEvent As ActionRequestEventHandler
    Public Event ActionCompleteEvent As ActionCompleteEventHandler
    Public Event GSTimerEvent As GSTimerEventHandler


    ' ----------  ARPATECH  ----------
    ' sub routine for raising action complete
    ' event for provided object
    ' ----------  ARPATECH  ----------
    Public Sub RaiseActionCompleteEvent(ByVal o As Object, ByVal e As ActionCompleteEventArgs)
        Try


            If IsNothing(o) Then
                o = Me

            End If
            If IsNothing(e) Then
                EventLog.WriteEntry("Application", "args is nothing")

            End If
            RaiseEvent ActionCompleteEvent(o, e)

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Unable to raise ac event:" & ex.Message())

        End Try
    End Sub


    ' ----------  ARPATECH  ----------
    ' sub routine for raising action request
    ' event for provided object
    ' ----------  ARPATECH  ----------
    Public Sub RaiseActionRequestEvent(ByVal o As Object, ByVal e As ActionRequestEventArgs)
        Try

            If IsNothing(o) Then
                EventLog.WriteEntry("Application", "Error raising AR event: object is nothing")
                o = Me

            End If

            If IsNothing(e) Then
                EventLog.WriteEntry("Application", "Error raising AR event: event is nothing")
                o = Me

            End If

            RaiseEvent ActionRequestEvent(o, e)

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Unable to raise ar event:" & ex.Message())

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for raising order
    ' event for provided object
    ' ----------  ARPATECH  ----------
    Public Sub RaiseOrderEvent(ByVal o As Object, ByVal e As OrderEventArgs)
        Try

            ' Dim ar As ActionRequest = CType(o, ActionRequest)

            If IsNothing(o) Then
                EventLog.WriteEntry("Application", "Error raising order event: object is nothing")

            End If

            If IsNothing(e) Then
                EventLog.WriteEntry("Application", "Error raising order event: event args is nothing")

            End If

            RaiseEvent OrderEvent(o, e)
            ' EventLog.WriteEntry("Application", "ORDER EVENT RAISED!")
        Catch ex As Exception
            EventLog.WriteEntry("Application", "GS Event Listener unable to raise order event:" & ex.Message() & "," & ex.Source & "," & ex.StackTrace)
            '  GSSupportManager.RestartService()
        End Try
    End Sub



    ' ----------  ARPATECH  ----------
    ' sub routine for raising timer
    ' event for provided object
    ' ----------  ARPATECH  ----------
    Public Sub RaiseGSTimerEvent(ByVal o As Object, ByVal e As GSTimerEventArgs)
        Try
            EventLog.WriteEntry("Application", "Starting GSTimer Event")
            Dim ar As ActionRequest = CType(o, ActionRequest)

            If IsNothing(o) Then
                EventLog.WriteEntry("Application", "Error raising gstimer event: object is nothing")

            End If

            If IsNothing(e) Then
                EventLog.WriteEntry("Application", "Error raising gstimer event: event args is nothing")

            End If

            RaiseEvent GSTimerEvent(o, e)

            EventLog.WriteEntry("Application", "Finishing GSTimer Event")

        Catch ex As Exception
            EventLog.WriteEntry("Application", "GS Event Listener unable to raise gstimer event:" & ex.Message() & "," & ex.Source & "," & ex.StackTrace)
            '  GSSupportManager.RestartService()
        End Try
    End Sub



    ' ----------  ARPATECH  ----------
    ' sub routine to raise ticket event
    ' according to the provided object
    ' ----------  ARPATECH  ----------
    Public Sub RaiseTicketEvent(ByVal o As Object, ByVal e As TicketEventArgs)
        Try

            If IsNothing(o) Then
                EventLog.WriteEntry("Application", "Error raising ticket event: object is nothing")
                o = Me

            End If
            If IsNothing(e) Then
                EventLog.WriteEntry("Application", "Error raising ticket event: ticket event args is nothing")

            End If
            RaiseEvent TicketEvent(o, e)
            ' Dim oea As New OrderEventArgs()
            ' RaiseOrderEvent(o, oea)

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Unable to raise ticket event:" & ex.Message() & "," & ex.Source & "," & ex.StackTrace)



        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub
End Class

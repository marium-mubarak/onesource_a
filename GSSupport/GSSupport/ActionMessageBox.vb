' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class just shows a message box for any given message to inform user.
' Inherits          :   Action
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Windows.Forms

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionMessageBox
    Inherits Action



    Private m_message As New String("")


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' message to show
    ' ----------  ARPATECH  ----------
    Public Property Message() As String
        Get
            Return m_message
        End Get
        Set(ByVal Value As String)
            m_message = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' execute function to execute this
    ' class instance which will show a message box
    ' ----------  ARPATECH  ----------
    Public Overrides Sub Execute()
        '     EventLog.WriteEntry("Application", "About to launch messagebox action")
        MessageBox.Show(m_message)
        '     EventLog.WriteEntry("Application", "launched messagebox action")
    End Sub





End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class which contains a interface named “IServiceResponseOutbound”
'                       but it does not implement it. This class basically checks the service is
'                       inbound or outbound, then it converts success response or error response
'                       to XML and send pass it on.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Xml.Schema       'Need this for cXML Validation
Imports System.Data
Imports System.Xml
Imports System.IO
Imports System.Collections
Imports System.Data.SqlClient
Imports System.Text
Imports System.Web



' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IServiceResponseOutbound
    Property responseType() As String
    Property GSTicketID() As Integer
End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionServiceResponseOutbound
   
    Inherits Action
    Implements IServiceResponseOutbound
    Private DBString As String = GSSupportManager.GetDBString()
    Dim LogFolderPath As String = GSSupportManager.GeteConnLogPath
    Dim timestamp = Date.Now.ToString("yyyyMMddTHHmmss")
    Private OutboundFilePath As String = LogFolderPath & "/OutBound/FieldServiceRequest/"
    Private InboundFilepath As String = LogFolderPath & "/InBound/FieldServiceResponse/"

    Private m_responseType As New String("")
    Private m_responseTypeID As Integer

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' response type as string
    ' ----------  ARPATECH  ----------
    Property ResponseType() As String Implements IServiceResponseOutbound.responseType
        Get
            Return m_responseType
        End Get
        Set(ByVal Value As String)
            m_responseType = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket ID as integer
    ' ----------  ARPATECH  ----------
    Property GSTicketID() As Integer Implements IServiceResponseOutbound.GSTicketID
        Get
            Return m_responseTypeID
        End Get
        Set(ByVal Value As Integer)
            m_responseTypeID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for this class instance execution
    ' ----------  ARPATECH  ----------
    Public Overrides Sub Execute()
        Try
            EventLog.WriteEntry("Application", "Starting Execute ResponseOutbound")
            EventLog.WriteEntry("Application", "Inside GSSupport SendServideResponseOutbound ResponseType is: " & m_responseType)
            EventLog.WriteEntry("Application", "Inside GSSupport SendServideResponseOutbound ResponseID is: " & m_responseTypeID)

            If Len(m_responseType) < 1 Then
                EventLog.WriteEntry("Application", "Error excuting Send Service Response Outbound : responseType not set.")
                Exit Sub
            End If
            If m_responseTypeID < 1 Then
                EventLog.WriteEntry("Application", "Error excuting Send Service Response Outbound: responseType ID not set.")
                Exit Sub
            End If

            PostXML(m_responseType, m_responseTypeID)

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error executing Send Service Response: " & e.Message)
        End Try
    End Sub
    
    ' ----------  ARPATECH  ----------
    ' function to send XML to client via email
    ' ----------  ARPATECH  ----------
    Public Function PostXML(ByVal responseType As String, ByVal ResponseTypeId As Integer)
        Dim OutboundFileName As String = OutboundFilePath & responseType & "_" & ResponseTypeId & "_" & "FSROutBoundXML" & timestamp & ".xml"
        Dim InboundFileName As String = InboundFilepath & responseType & "_" & ResponseTypeId & "_" & "FSResponseInBoundXML" & timestamp & ".xml"

        Dim XmlFiletoPost As String
        Dim HTTPPostURL As String
        Dim SqltblEconnParams As String
        Dim ReturnedResponse As String
        XmlFiletoPost = ""
        'If message is Success, Then call function that builds Success XML message
        If Trim(responseType) = "" Then
            EventLog.WriteEntry("Application", "responseType is empty. i am exiting the function")
            Exit Function
        End If
        If ResponseTypeId <= 0 Then
            EventLog.WriteEntry("Application", "ResponseTypeId is not valid. i am exiting the function")
            Exit Function
        End If
        Try
            If (Trim(UCase(responseType)) = "SUCCESS") Then
                XmlFiletoPost = BuildSuccessXML(ResponseTypeId, OutboundFileName)
            Else
                'If message is error, then we build an error message to post
                If (Trim(UCase(responseType)) = "ERROR") Then
                    XmlFiletoPost = BuildErrorXML(ResponseTypeId, OutboundFileName)
                End If
            End If
            EventLog.WriteEntry("Application", "This is output XML file created by Build XML function: " & XmlFiletoPost)
            If Len(XmlFiletoPost) < 1 Then
                Exit Try
            End If
            'Validate the XML file
            'EventLog.WriteEntry("Application", "Now validating file: " & XmlFiletoPost)
            'ValidateXMLFile(XmlFiletoPost)

            'Dim StringToPostValue = myXmlTextReader.ReadString
            'Now if XML string to post is not empty, lets post it to the Client.

            'Now Posting the XML Document to the according URL

            If (Trim(UCase(responseType)) = "SUCCESS") Then
                SqltblEconnParams = "SELECT HTTPPostURL FROM  tblEconnectionParams WHERE CompanyID " & _
                                  " in (Select Companyid from tblActionRequest where arid  " & _
                                  " in (Select ARId from tblTicket where ticketid = " & ResponseTypeId & "))"
                HTTPPostURL = GSSupportManager.GetSingleDBValueString(SqltblEconnParams)

            End If
            'If message is error, then we build an error message to post
            If (Trim(UCase(responseType)) = "ERROR") Then
                Dim ParentCompanyID = GSSupportManager.GetSingleDBValueInt("select ParentCompanyID from tblCustomers where Upper(customername) in ( Select ClientID from tblFieldServiceRequestInbound WHERE tblFieldServiceReqID =" & ResponseTypeId & ")")
                SqltblEconnParams = ("SELECT HTTPPostURL FROM  tblEconnectionParams WHERE CompanyID = " & ParentCompanyID)
                HTTPPostURL = GSSupportManager.GetSingleDBValueString(SqltblEconnParams)
            End If

            EventLog.WriteEntry("Application", "HTTP Posting URL is  " & HTTPPostURL)
            If Len(HTTPPostURL) < 0 Then
                Exit Try
            Else
                ReturnedResponse = HTTPPost(XmlFiletoPost, HTTPPostURL)
                EventLog.WriteEntry("Application", "HTTP Posting Done ")
            End If
            EventLog.WriteEntry("Application", "logging incoming response to the file: " & InboundFileName)
            'Log the returned response to the folder
            WriteFile(ReturnedResponse, InboundFileName)
            EventLog.WriteEntry("Application", "Sending email Alert Message")
            'Now send Alert email to econnection department
            SendAlertEmail(XmlFiletoPost, responseType)
            Exit Function
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Sending outboundServiceResponse:" & e.Message & ", " & e.Source)
        End Try

        'There is error
        responseType = responseType & " No response sent to Client "
        SendAlertEmail(XmlFiletoPost, responseType)
        EventLog.WriteEntry("Application", "Couldn't create or post XML message. XmlFiletoPost " & XmlFiletoPost & " HTTP URL: " & HTTPPostURL)

    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine for sending email to client
    ' after setting required vlaues
    ' ----------  ARPATECH  ----------
    Sub SendMail(ByVal sendTo As String, ByVal sendFrom As String, ByVal Emailbody As String, ByVal EmailSubject As String, ByVal EmailPriority As Integer)
        Dim ae As New ActionEmail
        ae.SendTo = sendTo
        ae.From = sendFrom
        ae.Body = Emailbody
        ae.Subject = EmailSubject
        ae.Priority = EmailPriority
        ae.Execute()
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that validates XML file format
    ' but dont sets its string value for caller
    ' ----------  ARPATECH  ----------
    Public Function ValidateXMLFile(ByVal filename As String) As String
        Console.WriteLine("Inside ValidateDTD, fileName is : " & filename)
        'Now Validate the XML with corresponding DTD
        Dim tr As XmlTextReader = New XmlTextReader(filename)
        Dim vr As XmlValidatingReader = New XmlValidatingReader(tr)
        vr.ValidationType = ValidationType.DTD
        AddHandler vr.ValidationEventHandler, AddressOf ValidationCallBack
        While (vr.Read())
        End While
        Console.WriteLine("DTD Validation finished")
    End Function

    ' ----------  ARPATECH  ----------
    ' function that generates success XML
    ' of nay ticket for 3 kind for clients
    ' WORLDSPAN, VALEANT PHARMACEUTICALS and GALLO
    ' ----------  ARPATECH  ----------
    Public Function BuildSuccessXML(ByVal ResponseTypeId As Integer, ByVal OutboundFileName As String) As String
        ' EventLog.WriteEntry("Application", "I am at the beginning of the BuildSuccessXML function")
        Dim myds As DataSet
        Dim mydr As DataRow
        Dim XmlFiletoPost
        Dim StatusName As New String("")
        Dim CustomerName As String
        CustomerName = ""

        CustomerName = GSSupportManager.GetSingleDBValueString("SELECT PartnerId FROM  tblEconnectionParams WHERE CompanyID in (Select Companyid from tblActionRequest where arid  in (Select ARId from tblTicket where ticketid = " & ResponseTypeId & "))")
        EventLog.WriteEntry("application", "CustomerName: " & CustomerName)

        If InStr(UCase(CustomerName), "WORLDSPAN") > 0 Then
            XmlFiletoPost = BuildGenericSuccessXML(ResponseTypeId, OutboundFileName)
        ElseIf InStr(UCase(CustomerName), "VALEANT PHARMACEUTICALS") > 0 Then
            XmlFiletoPost = BuildGenericSuccessXML(ResponseTypeId, OutboundFileName)
        Else
            If InStr(UCase(CustomerName), "GALLO") > 0 Then
                EventLog.WriteEntry("Application", "No active econnection for Gallo. Quit the procedure.")
                Exit Function
                Dim SqltblStatus As String = "SELECT * FROM  tblTicketStatus WHERE tktStatusID  " & _
                                               " in (Select tktStatusID from tblticket where ticketid = " & ResponseTypeId & ")"

                Dim IsCancelled
                Dim IsClosed
                myds = GetDataTable(SqltblStatus)
                For Each mydr In myds.Tables("dt").Rows
                    StatusName = stringSaturate(mydr("tktStatusName").ToString)
                    IsCancelled = stringSaturate(mydr("isCancelled").ToString)
                    IsClosed = stringSaturate(mydr("isClosed").ToString)
                Next
                myds.Clear()
                mydr = Nothing
                If IsCancelled Or IsClosed Or ((InStr(UCase(StatusName), "CLOSED") > 0) Or (InStr(UCase(StatusName), "CANCELLED") > 0)) Then
                    'If ((InStr(UCase(StatusName), "CLOSED") > 0) Or (InStr(UCase(StatusName), "CANCELLED") > 0)) Then
                    EventLog.WriteEntry("Application", "I am Calling  BuildGalloCLoseXML ")
                    XmlFiletoPost = BuildGalloCLoseXML(ResponseTypeId, OutboundFileName)
                Else
                    EventLog.WriteEntry("Application", "I am Calling  BuildGalloUpdateXML ")
                    XmlFiletoPost = BuildGalloUpdateXML(ResponseTypeId, OutboundFileName)
                    ' End If
                End If
            End If
        End If
        'Navigate TblOrders
        Return (XmlFiletoPost)

    End Function

    ' ----------  ARPATECH  ----------
    ' function that builds XML for closed tickets
    ' ----------  ARPATECH  ----------
    Public Function BuildGalloCLoseXML(ByVal TicketId As Integer, ByVal OutboundFileName As String) As String
        Dim myds
        Dim mydr
        Dim Statusname
        Dim PartnerID
        Dim ToDomain
        Dim FromDomain
        Dim SenderDomain
        Dim ToDomainValue
        Dim FromDomainValue
        Dim SenderDomainValue

        Dim SqltblEconnParams As String = "SELECT * FROM  tblEconnectionParams WHERE CompanyID " & _
                                            " in (Select Companyid from tblActionRequest where arid  " & _
                                            " in (Select ARId from tblTicket where ticketid = " & TicketId & "))"
        myds = GetDataTable(SqltblEconnParams)

        For Each mydr In myds.Tables("dt").Rows
            PartnerID = mydr("PartnerID").ToString
            ToDomain = mydr("ToDomain").ToString
            FromDomain = mydr("FromDomain").ToString
            SenderDomain = mydr("SenderDomain").ToString
            ToDomainValue = mydr("ToCredentialId").ToString
            FromDomainValue = mydr("FromCredentialId").ToString
            SenderDomainValue = mydr("SenderCredentialId").ToString
        Next


        ' Dim OutPutFileName As String

        Dim SqltblStatus As String = "SELECT tktStatusName FROM  tblTicketStatus WHERE tktStatusID = " & _
                                                      " in (Select tktStatusID from tblticket where ticketid = " & TicketId
        Statusname = GSSupportManager.GetSingleDBValueString(SqltblStatus)
        Dim sDate = Date.Now.ToString("yyyy-MM-ddTHH:mm:ss")
        'Start building XML message
        Dim myXmlTextReader As XmlTextReader = Nothing
        Dim myXmlTextWriter As XmlTextWriter = Nothing
        myXmlTextWriter = New XmlTextWriter(OutboundFileName, Nothing)
        Try
            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented
            myXmlTextWriter.WriteStartDocument(False)
            myXmlTextWriter.WriteDocType("helpDesk", Nothing, "helpDesk.dtd", Nothing)
            myXmlTextWriter.WriteStartElement("helpDesk")
            myXmlTextWriter.WriteAttributeString("version", "1.0")
            myXmlTextWriter.WriteAttributeString("payloadID", "1.0")
            myXmlTextWriter.WriteAttributeString("timestamp", sDate)
            'Request Header Starts here
            myXmlTextWriter.WriteStartElement("Header", Nothing)
            myXmlTextWriter.WriteStartElement("from", Nothing)
            myXmlTextWriter.WriteAttributeString("type", FromDomain)
            myXmlTextWriter.WriteString(FromDomainValue)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("to", Nothing)
            myXmlTextWriter.WriteAttributeString("type", ToDomain)
            myXmlTextWriter.WriteString(ToDomainValue)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("sender", Nothing)
            myXmlTextWriter.WriteAttributeString("type", SenderDomain)
            myXmlTextWriter.WriteString(SenderDomainValue)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'Header ends here
            'Now start the request
            myXmlTextWriter.WriteStartElement("request", Nothing)
            myXmlTextWriter.WriteStartElement("troubleTicketRequest", Nothing)
            myXmlTextWriter.WriteAttributeString("action", "close")
            myXmlTextWriter.WriteStartElement("troubleTicket", Nothing)
            myXmlTextWriter.WriteAttributeString("ticketId", TicketId)
            myXmlTextWriter.WriteAttributeString("referenceTicketId", TicketId)
            myXmlTextWriter.WriteElementString("Status", Statusname)
            myXmlTextWriter.WriteElementString("resolution", Statusname)
            'Close troubleTicket
            myXmlTextWriter.WriteEndElement()
            'Close troubleTicketRequest
            myXmlTextWriter.WriteEndElement()
            'Close request
            myXmlTextWriter.WriteEndElement()
            'Close helpDesk
            myXmlTextWriter.WriteEndElement()
            'Write the XML to file and close the writer
            myXmlTextWriter.Flush()
            myXmlTextWriter.Close()

        Catch e As Exception
            Console.WriteLine("Exception: {0}", e.ToString())
        Finally
            Console.WriteLine()
            Console.WriteLine("Processing of the file {0} complete.", OutboundFileName)
            'Close the writer
            If Not myXmlTextWriter Is Nothing Then
                myXmlTextWriter.Close()
            End If
        End Try
        Return (OutboundFileName)
    End Function

    ' ----------  ARPATECH  ----------
    ' function that builds XML for updated tickets
    ' ----------  ARPATECH  ----------
    Public Function BuildGalloUpdateXML(ByVal TicketId As Integer, ByVal OutboundFileName As String) As String
        Dim myds
        Dim mydr
        Dim Statusname
        Dim PartnerID
        Dim ToDomain
        Dim FromDomain
        Dim SenderDomain
        Dim ToDomainValue
        Dim FromDomainValue
        Dim SenderDomainValue

        Dim SqltblEconnParams As String = "SELECT * FROM  tblEconnectionParams WHERE CompanyID " & _
                                            " in (Select Companyid from tblActionRequest where arid  " & _
                                            " in (Select ARId from tblTicket where ticketid = " & TicketId & "))"
        myds = GetDataTable(SqltblEconnParams)

        For Each mydr In myds.Tables("dt").Rows
            PartnerID = mydr("PartnerID").ToString
            ToDomain = mydr("ToDomain").ToString
            FromDomain = mydr("FromDomain").ToString
            SenderDomain = mydr("SenderDomain").ToString
            ToDomainValue = mydr("ToCredentialId").ToString
            FromDomainValue = mydr("FromCredentialId").ToString
            SenderDomainValue = mydr("SenderCredentialId").ToString
        Next


        ' Dim OutPutFileName As String

        Dim SqltblStatus As String = "SELECT tktStatusName FROM  tblTicketStatus WHERE tktStatusID = " & _
                                                      " in (Select tktStatusID from tblticket where ticketid = " & TicketId
        Statusname = GSSupportManager.GetSingleDBValueString(SqltblStatus)
        Dim sDate = Date.Now.ToString("yyyy-MM-ddTHH:mm:ss")
        'Start building XML message
        Dim myXmlTextReader As XmlTextReader = Nothing
        Dim myXmlTextWriter As XmlTextWriter = Nothing
        myXmlTextWriter = New XmlTextWriter(OutboundFileName, Nothing)
        Try
            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented
            myXmlTextWriter.WriteStartDocument(False)
            myXmlTextWriter.WriteDocType("helpDesk", Nothing, "helpDesk.dtd", Nothing)
            myXmlTextWriter.WriteStartElement("helpDesk")
            myXmlTextWriter.WriteAttributeString("version", "1.0")
            myXmlTextWriter.WriteAttributeString("payloadID", "1.0")
            myXmlTextWriter.WriteAttributeString("timestamp", sDate)
            'Request Header Starts here
            myXmlTextWriter.WriteStartElement("Header", Nothing)
            myXmlTextWriter.WriteStartElement("from", Nothing)
            myXmlTextWriter.WriteAttributeString("type", FromDomain)
            myXmlTextWriter.WriteString(FromDomainValue)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("to", Nothing)
            myXmlTextWriter.WriteAttributeString("type", ToDomain)
            myXmlTextWriter.WriteString(ToDomainValue)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("sender", Nothing)
            myXmlTextWriter.WriteAttributeString("type", SenderDomain)
            myXmlTextWriter.WriteString(SenderDomainValue)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'Header ends here
            'Now start the request
            myXmlTextWriter.WriteStartElement("request", Nothing)
            myXmlTextWriter.WriteStartElement("troubleTicketRequest", Nothing)
            myXmlTextWriter.WriteAttributeString("action", "close")
            myXmlTextWriter.WriteStartElement("troubleTicket", Nothing)
            myXmlTextWriter.WriteAttributeString("ticketId", TicketId)
            myXmlTextWriter.WriteAttributeString("referenceTicketId", TicketId)
            myXmlTextWriter.WriteElementString("Status", Statusname)
            myXmlTextWriter.WriteElementString("resolution", Statusname)
            'Close troubleTicket
            myXmlTextWriter.WriteEndElement()
            'Close troubleTicketRequest
            myXmlTextWriter.WriteEndElement()
            'Close request
            myXmlTextWriter.WriteEndElement()
            'Close helpDesk
            myXmlTextWriter.WriteEndElement()
            'Write the XML to file and close the writer
            myXmlTextWriter.Flush()
            myXmlTextWriter.Close()

        Catch e As Exception
            Console.WriteLine("Exception: {0}", e.ToString())
        Finally
            Console.WriteLine()
            Console.WriteLine("Processing of the file {0} complete.", OutboundFileName)
            'Close the writer
            If Not myXmlTextWriter Is Nothing Then
                myXmlTextWriter.Close()
            End If
        End Try
        'Now Posting the XML Document to the according URL
        '  HTTPPost(args)
        ' Console.WriteLine("Http Posting finished")
        Return (OutboundFileName)

    End Function

    ' ----------  ARPATECH  ----------
    ' function that creates a XML for errors
    ' raised during ticketing process
    ' ----------  ARPATECH  ----------
    Public Function BuildGenericErrorXML(ByVal InputFSRId As Integer, ByVal OutboundFileName As String) As String
        If InputFSRId < 1 Then
            EventLog.WriteEntry("Application", "Input FSRID is not valid: " & InputFSRId)
            EventLog.WriteEntry("Application", "Exiting BuildGenericErrorXML: " & InputFSRId)

            Exit Function
        End If
        Dim myds As DataSet
        Dim mydr As DataRow
        Dim ProcessingInstructionName As New String("")
        Dim ProcessingInstructionValue As New String("")
        Dim RequestHeaderStatus As New String("")
        Dim ClientID As New String("")
        Dim CustomerReqNum As New String("")
        Dim PrimeContractorReqNum As New String("")
        Dim AssignedServiceProvider As New String("")
        Dim ServiceProviderReqNum As New String("")
        Dim TimeStamp As New String("")
        Dim ServiceRequestSummary As New String("")
        Dim ServiceRequestDescription As New String("")
        Dim ReqCategoryCode As New String("")
        Dim ReqCategoryText As New String("")
        Dim Priority As New String("")
        Dim VendorEngNum As New String("")
        Dim AssetSerialNum As New String("")
        Dim AssetSerialTag As New String("")
        Dim AssetManuf As New String("")
        Dim AssetType As New String("")
        Dim AssetModel As New String("")
        Dim LocationNumber As New String("")
        Dim CustomerName As New String("")
        Dim AddressID As New String("")
        Dim Address1 As New String("")
        Dim Address2 As New String("")
        Dim PriorityIntValue As Integer = -1
        Dim City As New String("")
        Dim StateProvince As New String("")
        Dim timezoneid As Integer = 0
        Dim PostalCode As New String("")
        Dim CountryCode As New String("")
        Dim Country As New String("")
        Dim CountryID As Integer = 0
        Dim PhoneNum As New String("")
        Dim Email As New String("")
        Dim CustomerContact As New String("")
        Dim HoursOfOperation As New String("")
        Dim StatusCode As New String("")
        Dim StatusText As New String("")
        Dim ResolutionText As New String("")
        Dim ResolutionTime As New String("")
        Dim ETA As New String("")
        Dim CommentType As New String("")
        Dim StatusComment As New String("")
        Dim CustomerContactID As New String("")
        Dim PriorityID As New String("")
        Dim sDate = Date.Now.ToString("yyyy-MM-ddTHH:mm:ss")
        'Start building XML message
        Dim myXmlTextWriter As XmlTextWriter = Nothing


        EventLog.WriteEntry("Application", "I am inside BuildGenericErrorXML")

        Try
            Dim Sqltbl As String = "Select * from tblFieldServiceRequestInbound WHERE tblFieldServiceReqID =" & InputFSRId
            myds = GetDataTable(Sqltbl)

            For Each mydr In myds.Tables("dt").Rows

                ProcessingInstructionName = stringSaturate(mydr("ProcessingInstructionName").ToString)
                ProcessingInstructionValue = stringSaturate(mydr("ProcessingInstructionValue").ToString)
                ClientID = stringSaturate(mydr("ClientID").ToString)
                CustomerReqNum = stringSaturate(mydr("CustomerReqNum").ToString)
                PrimeContractorReqNum = stringSaturate(mydr("PrimeContractorReqNum").ToString)
                AssignedServiceProvider = stringSaturate(mydr("AssignedServiceProvider").ToString)
                ServiceProviderReqNum = stringSaturate(mydr("ServiceProviderReqNum").ToString)
                CustomerReqNum = stringSaturate(mydr("CustomerReqNum").ToString)
                PrimeContractorReqNum = stringSaturate(mydr("PrimeContractorReqNum").ToString)
                AssignedServiceProvider = stringSaturate(mydr("AssignedServiceProvider").ToString)
                TimeStamp = stringSaturate(mydr("TimeStamp").ToString)
                ServiceRequestSummary = stringSaturate(mydr("ServiceRequestSummary").ToString)
                ServiceRequestDescription = stringSaturate(mydr("ServiceRequestDescription").ToString)
                ReqCategoryCode = stringSaturate(mydr("ReqCategoryCode").ToString)
                ReqCategoryText = stringSaturate(mydr("ReqCategoryText").ToString)
                PriorityID = stringSaturate(mydr("PriorityID").ToString)
                Priority = stringSaturate(mydr("Priority").ToString)
                VendorEngNum = stringSaturate(mydr("VendorEngNum").ToString)
                AssetSerialNum = stringSaturate(mydr("AssetSerialNum").ToString)
                AssetSerialTag = stringSaturate(mydr("AssetSerialTag").ToString)
                AssetManuf = stringSaturate(mydr("AssetManuf").ToString)
                AssetType = stringSaturate(mydr("AssetType").ToString)
                AssetModel = stringSaturate(mydr("AssetModel").ToString)
                LocationNumber = stringSaturate(mydr("LocationNumber").ToString)
                CustomerName = stringSaturate(mydr("CustomerName").ToString)
                Address1 = stringSaturate(mydr("Address1").ToString)
                Address2 = stringSaturate(mydr("Address2").ToString)
                City = stringSaturate(mydr("City").ToString)
                StateProvince = stringSaturate(mydr("StateProvince").ToString)
                PostalCode = stringSaturate(mydr("PostalCode").ToString)
                CountryCode = stringSaturate(mydr("CountryCode").ToString)
                Country = stringSaturate(mydr("Country").ToString)
                PhoneNum = stringSaturate(mydr("PhoneNum").ToString)
                Email = stringSaturate(mydr("Email").ToString)
                CustomerContactID = stringSaturate(mydr("CustomerContactID").ToString)
                CustomerContact = stringSaturate(mydr("CustomerContact").ToString)
                HoursOfOperation = stringSaturate(mydr("HoursOfOperation").ToString)
                StatusCode = stringSaturate(mydr("StatusCode").ToString)
                StatusText = stringSaturate(mydr("StatusText").ToString)
                ResolutionText = stringSaturate(mydr("ResolutionText").ToString)
                ResolutionTime = stringSaturate(mydr("ResolutionTime").ToString)
                ETA = stringSaturate(mydr("ETA").ToString)
                CommentType = stringSaturate(mydr("CommentType").ToString)
                StatusComment = stringSaturate(mydr("StatusComment").ToString)

            Next
            myds.Clear()
            mydr = Nothing
            EventLog.WriteEntry("Application", "Inside BuildGenericErrorXML: OutboundFileName: " & OutboundFileName)

            myXmlTextWriter = New XmlTextWriter(OutboundFileName, System.Text.Encoding.UTF8)


            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented
            myXmlTextWriter.WriteStartDocument(False)
            'myXmlTextWriter.WriteDocType("FieldServiceXML", Nothing, "FieldServiceRequest.dtd", Nothing)

            myXmlTextWriter.WriteStartElement("FieldServiceXML")
            'write ProcessingInstruction if there is value
            If Len(ProcessingInstructionValue) > 0 AndAlso Len(ProcessingInstructionName) > 0 Then
                myXmlTextWriter.WriteProcessingInstruction(ProcessingInstructionName, ProcessingInstructionValue)
            End If
            'Request Header Starts here
            myXmlTextWriter.WriteStartElement("RequestHeader", Nothing)
            myXmlTextWriter.WriteAttributeString("status", "Update")
            myXmlTextWriter.WriteAttributeString("version", "1.2")
            myXmlTextWriter.WriteElementString("ClientID", CustomerName)
            myXmlTextWriter.WriteElementString("ClientRequestNumber", CustomerReqNum)
            myXmlTextWriter.WriteElementString("PrimeContractorRequestNumber", PrimeContractorReqNum)
            myXmlTextWriter.WriteElementString("AssignedServiceProvider", AssignedServiceProvider)
            myXmlTextWriter.WriteElementString("ServiceProviderRequestNumber", ServiceProviderReqNum)
            myXmlTextWriter.WriteElementString("Timestamp", sDate)
            myXmlTextWriter.WriteEndElement()
            'Request Header Ends here
            'ServiceRequest Starts here
            myXmlTextWriter.WriteStartElement("ServiceRequest", Nothing)
            myXmlTextWriter.WriteElementString("Summary", ServiceRequestSummary)
            myXmlTextWriter.WriteElementString("Description", ServiceRequestDescription)

            'Start Request Category Segment
            myXmlTextWriter.WriteStartElement("RequestCategory", Nothing)
            myXmlTextWriter.WriteElementString("CategoryCode", ReqCategoryCode)
            myXmlTextWriter.WriteElementString("CategoryText", ReqCategoryText)
            myXmlTextWriter.WriteEndElement()


            'End Request Category Segment
            myXmlTextWriter.WriteStartElement("Priority", Nothing)
            myXmlTextWriter.WriteAttributeString("id", PriorityID)
            myXmlTextWriter.WriteString(Priority)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("VendorEngineerNumber", VendorEngNum)

            'Start Asset Segment here
            myXmlTextWriter.WriteStartElement("Asset", Nothing)
            myXmlTextWriter.WriteElementString("SerialNumber", AssetSerialNum)
            myXmlTextWriter.WriteElementString("AssetTag", AssetSerialTag)
            myXmlTextWriter.WriteElementString("Manufacturer", AssetManuf)
            myXmlTextWriter.WriteElementString("Type", AssetType)
            myXmlTextWriter.WriteElementString("Model", AssetModel)
            myXmlTextWriter.WriteEndElement()
            'End Asset Segment here

            'Customer Segment Begins here
            myXmlTextWriter.WriteStartElement("Customer", Nothing)
            myXmlTextWriter.WriteElementString("LocationNumber", LocationNumber)
            myXmlTextWriter.WriteElementString("CustomerName", CustomerName)
            'Begins Address Segment
            myXmlTextWriter.WriteStartElement("Address", Nothing)
            myXmlTextWriter.WriteAttributeString("id", AddressID)
            myXmlTextWriter.WriteElementString("Address1", Address1)
            myXmlTextWriter.WriteElementString("Address2", Address2)
            myXmlTextWriter.WriteElementString("City", City)
            myXmlTextWriter.WriteElementString("StateProvince", StateProvince)
            myXmlTextWriter.WriteElementString("PostalCode", PostalCode)
            myXmlTextWriter.WriteStartElement("Country", Nothing)
            myXmlTextWriter.WriteAttributeString("isoCountryCode", CountryCode)
            myXmlTextWriter.WriteString(Country)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'End Of Address Segment
            myXmlTextWriter.WriteElementString("PhoneNumber", PhoneNum)
            myXmlTextWriter.WriteElementString("EmailAddress", Email)
            myXmlTextWriter.WriteStartElement("Contact", Nothing)
            myXmlTextWriter.WriteAttributeString("id", CustomerContactID)
            myXmlTextWriter.WriteString(CustomerContact)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("HoursOfOperation", HoursOfOperation)
            myXmlTextWriter.WriteEndElement()
            'Customer segment Ends here
            'Status Segment Begins here
            myXmlTextWriter.WriteStartElement("Status", Nothing)
            myXmlTextWriter.WriteElementString("StatusCode", StatusCode)
            myXmlTextWriter.WriteElementString("StatusText", StatusText)
            myXmlTextWriter.WriteElementString("CommentType", "R")
            'Since this is Error Update outBound, the Status message contains error 
            StatusComment = "Error--- Request has NOT been processed."
            myXmlTextWriter.WriteElementString("StatusComment", StatusComment)
            myXmlTextWriter.WriteEndElement()
            'Status Segment Ends here
            myXmlTextWriter.WriteElementString("ResolutionText", ResolutionText)
            myXmlTextWriter.WriteElementString("ResolutionTime", ResolutionTime)
            myXmlTextWriter.WriteElementString("ETA", ETA)

            myXmlTextWriter.WriteEndElement()
            'ServiceRequest Ends Here
            myXmlTextWriter.WriteEndElement()
            'FieldServiceXML Ends Here

            'Write the XML to file and close the writer
            myXmlTextWriter.Flush()
            myXmlTextWriter.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Inside BuildGenericErrorXML. Message " & e.Message & " Source: " & e.Source & " : " & e.ToString)
        Finally
            Console.WriteLine()
            Console.WriteLine("Processing of the file {0} complete.", OutboundFileName)
            'Close the writer
            If Not myXmlTextWriter Is Nothing Then
                myXmlTextWriter.Close()
            End If
        End Try


        'Now Posting the XML Document to the according URL
        '  HTTPPost(args)
        ' Console.WriteLine("Http Posting finished")
        Return (OutboundFileName)
    End Function

    ' ----------  ARPATECH  ----------
    ' function that creates a XML for succcss
    ' after ticketing process
    ' ----------  ARPATECH  ----------
    Public Function BuildGenericSuccessXML(ByVal InputTicketId As Integer, ByVal OutboundFileName As String) As String
        If InputTicketId < 1 Then
            EventLog.WriteEntry("Application", "Cannot proceed with the provided OneSource TicketID: " & InputTicketId)
            Exit Function
        End If
        Dim myds As DataSet
        Dim mydr As DataRow
        Dim customerTicketID
        Dim tktStatusID
        Dim priorityid
        Dim ticketID = InputTicketId
        Dim arID
        Dim sfaticketid
        Dim sfaid
        Dim assetid
        Dim etaDate
        Dim resolutionDate
        Dim resolutionText


        Dim Sqltbl As String = "SELECT * FROM  tblTicket WHERE ticketID = " & InputTicketId
        ' EventLog.WriteEntry("Application", "Sqltbl: " & Sqltbl)
        myds = GetDataTable(Sqltbl)
        For Each mydr In myds.Tables("dt").Rows
            customerTicketID = mydr("customerTicketID").ToString
            tktStatusID = mydr("tktStatusID").ToString
            priorityid = mydr("priorityid").ToString
            ticketID = mydr("ticketID").ToString
            arID = mydr("arID").ToString
            sfaticketid = mydr("sfaticketid").ToString
            sfaid = mydr("sfaid").ToString
            assetid = mydr("assetid").ToString
            etaDate = mydr("etaDate").ToString
            resolutionDate = mydr("resolutionDate").ToString
            resolutionText = mydr("resolutionText").ToString
        Next
        myds.Clear()
        mydr = Nothing

        If Len(arID) > 0 Then
            arID = CInt(arID)
        Else
            EventLog.WriteEntry("Application", "ARId is not valid" & arID)
            EventLog.WriteEntry("Application", "Exitting function BuildWSLSuccessXML as cannot find proper arID for TicketID: " & InputTicketId)
            Exit Function
        End If
        'tblActionRequest
        Dim SqlActionRequest = "SELECT * FROM  tblActionRequest WHERE arID = " & CInt(arID) & " and isactive=1"
        EventLog.WriteEntry("Application", "SqlActionRequest: " & SqlActionRequest)
        Dim arTypeID
        Dim DateReceived
        Dim submittedByUserID
        'Dim assetid
        Dim companyID
        Dim addressid
        Dim affectedUserContactID
        'Dim priorityid
        Dim gscomments
        Dim serialnum
        Dim assettagnum
        Dim Summary
        Dim Description


        myds = GetDataTable(SqlActionRequest)
        For Each mydr In myds.Tables("dt").Rows
            DateReceived = mydr("DateReceived").ToString
            submittedByUserID = mydr("submittedByUserID").ToString
            assetid = mydr("assetid").ToString
            companyID = mydr("companyID").ToString
            addressid = mydr("addressid").ToString
            affectedUserContactID = mydr("affectedUserContactID").ToString
            priorityid = mydr("priorityid").ToString
            gscomments = mydr("gscomments").ToString
            serialnum = mydr("serialnum").ToString
            assettagnum = mydr("assettagnum").ToString
            arTypeID = mydr("arTypeID").ToString
            Summary = mydr("arName").ToString
            Description = mydr("arDesc").ToString

        Next
        myds.Clear()
        mydr = Nothing
        If Len(Summary) > 0 Then
            Summary = Replace(Summary, "<p>", "vbcrlf")
            Summary = Replace(Summary, "</p>", "")
        End If
        If Len(Description) > 0 Then
            Description = Replace(Description, "<p>", vbCrLf)
            Description = Replace(Description, "</p>", "")
        End If

        Dim SQLXrefCustAdd = " SELECT XrefCustomerAddressesID, CustomerAddressCode, XrefCode FROM tblXrefCustomerAddresses WHERE  CustomerAddressID =  " & CInt(addressid)
        Dim XrefCustomerAddressesID
        Dim CustomerAddressCode
        Dim XrefCode
        Dim LocationNumber

        myds = GetDataTable(SQLXrefCustAdd)
        For Each mydr In myds.Tables("dt").Rows
            XrefCustomerAddressesID = mydr("XrefCustomerAddressesID").ToString
            CustomerAddressCode = mydr("CustomerAddressCode").ToString
            LocationNumber = mydr("XrefCode").ToString
        Next
        myds.Clear()
        mydr = Nothing


        Dim CustomerAddressName
        Dim CustomerID
        Dim Address1
        Dim Address2
        Dim City
        Dim StateProvinceRegion
        Dim PostalCode
        Dim CountryID
        Dim PhoneNumber
        Dim EmailAddress
        Dim Contact
        Dim HoursOfOperation
        Dim StatusCode
        Dim StatusText
        Dim statusComment
        '  Dim ResolutionTime
        Dim CustomerName
        Dim CategoryCode
        Dim CategoryText
        Dim Priority
        Dim PriorityIntId
        Dim SerialNumber
        Dim AssetTag
        Dim Manufacturer
        Dim AssetType
        Dim Model

        Dim CountryName
        Dim isoCountryCodeID
        Dim isoCountryCode

        ' EventLog.WriteEntry("Application", "XrefCustomerAddressesID: " & XrefCustomerAddressesID)
        If Len(addressid) > 0 Then
            addressid = CInt(addressid)

            Dim SQLCustomerAdd = "SELECT CustomerAddressID, CustomerAddressName, CustomerID, Address1, Address2, City, StateProvinceRegion, PostalCode, CountryID FROM tblCustomerAddresses WHERE CustomerAddressID = " & addressid & " and isActive=1"
            '    EventLog.WriteEntry("Application", "SQLCustomerAdd: " & SQLCustomerAdd)
            myds = GetDataTable(SQLCustomerAdd)
            For Each mydr In myds.Tables("dt").Rows
                CustomerAddressName = mydr("CustomerAddressName").ToString
                CustomerID = mydr("CustomerID").ToString
                Address1 = mydr("Address1").ToString
                Address2 = mydr("Address2").ToString
                City = mydr("City").ToString
                StateProvinceRegion = mydr("StateProvinceRegion").ToString
                PostalCode = mydr("PostalCode").ToString
                CountryID = mydr("CountryID").ToString
            Next
            myds.Clear()
            mydr = Nothing
        Else
            EventLog.WriteEntry("Application", "CustomerAddressid is not valid" & addressid)
        End If



        If Len(CustomerID) > 0 Then
            CustomerID = CInt(CustomerID)
        Else
            CustomerID = 0
        End If

        EventLog.WriteEntry("Application", "AssetID: " & assetid)
        If Len(assetid) > 0 Then
            assetid = CInt(assetid)
        Else
            assetid = 0
        End If

        If assetid > 0 Then
            Dim SQLAsset = "Select  AssetType, Manufacturer, Description FROM GSNetP_tbl_AssetsOwned  WHERE AssetsOwnedID in (Select assetsownedid from GSNetp_tbl_Assets where  assetid = " & assetid & ")"
            EventLog.WriteEntry("Application", "SQLAsset: " & SQLAsset)
            myds = GetDataTable(SQLAsset)
            For Each mydr In myds.Tables("dt").Rows
                Manufacturer = mydr("Manufacturer").ToString
                AssetType = mydr("AssetType").ToString
                Model = mydr("Description").ToString
            Next
            myds.Clear()
            mydr = Nothing
        End If

        If CustomerID > 0 Then
            Dim SQLCustomers = "SELECT CustomerName, Phone, Email FROM  tblCustomers WHERE CustomerID = " & CustomerID
            ' EventLog.WriteEntry("Application", "SQLCustomers: " & SQLCustomers)
            myds = GetDataTable(SQLCustomers)
            For Each mydr In myds.Tables("dt").Rows
                CustomerName = mydr("CustomerName").ToString
                PhoneNumber = mydr("Phone").ToString
                EmailAddress = mydr("Email").ToString
            Next
            myds.Clear()
            mydr = Nothing
        End If
        If Len(arTypeID) > 0 Then
            arTypeID = CInt(arTypeID)
        Else
            arTypeID = 0
        End If
        If arTypeID > 0 Then
            CategoryCode = GSSupportManager.GetSingleDBValueString("SELECT  arTypeName FROM  tblActionRequestType WHERE arTypeID =" & arTypeID)
            CategoryText = CategoryCode
        End If
        'If there is a XREF setup for ticket status, 
        'Get the status of the ticket from tblXRefTicketStatus
        If Len(tktStatusID) > 0 Then
            tktStatusID = CInt(tktStatusID)
        Else
            tktStatusID = 0
        End If
        If CInt(tktStatusID) > 0 Then
            Dim SQLXRefTicketStatus = "SELECT XRefStatusName, XRefStatusCode FROM tblXRefTicketStatus WHERE CompanyID = " & companyID & " AND TicketStatusID = " & tktStatusID
            myds = GetDataTable(SQLXRefTicketStatus)
            For Each mydr In myds.Tables("dt").Rows
                StatusText = mydr("XRefStatusName").ToString
                StatusCode = mydr("XRefStatusCode").ToString
            Next
            myds.Clear()
            mydr = Nothing
        End If

        'Get the status of the ticket

        If Len(StatusCode) = 0 Then
            Dim SQLTicketStatus = "SELECT tktStatusID, tktStatusName,  tktStatusDesc FROM tblTicketStatus WHERE tktStatusID =" & CInt(tktStatusID) & " and CompanyID = " & CInt(companyID)
            myds = GetDataTable(SQLTicketStatus)
            For Each mydr In myds.Tables("dt").Rows
                StatusCode = mydr("tktStatusName").ToString
                StatusText = mydr("tktStatusDesc").ToString
            Next
            myds.Clear()
            mydr = Nothing
        End If
        'Get the information on who updated ticket last
        Dim TktUpdatedUserID = GSSupportManager.GetSingleDBValueInt("SELECT LastupdateUserID FROM tblTicketTracking WHERE ticketlogid in (SELECT MAX(ticketlogid) as TicketLogID FROM tblTicketTracking  WHERE ticketid =" & ticketID & ")")
        'If ticket was last updated by CasAgent, then send confirmation notice.
        'Client doesn't want to receive the last comment that they posted to this ticket.
        'Since this comment is already in their system.
        ' Else send Actural comment on the ticket
        If TktUpdatedUserID = 3695 Then
            statusComment = "Update Received and Processed by GlobalServe"
        Else
            'Get the latest comment from tblTicketTracking
            Dim SQLTktTracking = "SELECT comment FROM tblTicketTracking WHERE ticketlogid in (SELECT MAX(ticketlogid) as TicketLogID FROM tblTicketTracking  WHERE ticketid =" & ticketID & ")"
            statusComment = GSSupportManager.GetSingleDBValueString(SQLTktTracking)

            If Len(statusComment) > 0 Then
                statusComment = Replace(statusComment, vbCrLf, "<br />")
                statusComment = Replace(statusComment, vbCr, "<br />")
                statusComment = Replace(statusComment, vbLf, "<br />")
                statusComment = Replace(statusComment, "</p>", "<br />")
                statusComment = Replace(statusComment, "<p>", "  ")
            End If
        End If

        'Get ISO CountryCode and CountryName from tblCountries.
        If CInt(CountryID) > 0 Then
            Dim SQLCountryCode = "SELECT CountryName,  CountryCodeID FROM tblCountries WHERE CountryID =" & CInt(CountryID)
            myds = GetDataTable(SQLCountryCode)
            For Each mydr In myds.Tables("dt").Rows
                CountryName = mydr("CountryName").ToString
                isoCountryCodeID = mydr("CountryCodeID").ToString
            Next
            myds.Clear()
            mydr = Nothing
            If CInt(isoCountryCodeID) > 0 Then
                isoCountryCode = GSSupportManager.GetSingleDBValueString("SELECT ISOCountryCodeA2, ISOCountryCodeA3 FROM tblCountryCodes WHERE CountryCodeID =" & CInt(isoCountryCodeID))
            End If
        End If
        'Get Priority Related Information
        If Len(priorityid) > 0 Then
            priorityid = CInt(priorityid)
        Else
            priorityid = 0
        End If
        If priorityid > 0 Then
            Dim SQLtblPriority = "SELECT priorityName, priorityIntValue FROM  tblPriority WHERE priorityID = " & CInt(priorityid)
            myds = GetDataTable(SQLtblPriority)
            For Each mydr In myds.Tables("dt").Rows
                Priority = mydr("priorityName").ToString
                PriorityIntId = mydr("priorityIntValue").ToString
            Next
            myds.Clear()
            mydr = Nothing
        End If
        'Get Contact information
        If CInt(companyID) > 0 Then
            Contact = GSSupportManager.GetSingleDBValueString("SELECT Contact FROM  tblParentCompanies WHERE ParentCompanyID =" & CInt(companyID))
        End If


        'Start building XML message
        Dim myXmlTextWriter As XmlTextWriter = Nothing
        Dim sDate = Date.Now.ToString("yyyy-MM-ddTHH:mm:ss")
        resolutionText = Trim(resolutionText)
        'Now if there is value for resolution text, then resolution time is timestamp.
        If Len(resolutionText) > 1 And Len(Trim(resolutionDate)) < 1 Then
            resolutionDate = sDate
        End If

        ' HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.ContentType = "text/xml"
        myXmlTextWriter = New XmlTextWriter(OutboundFileName, System.Text.Encoding.UTF8)

        Try
            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented
            myXmlTextWriter.WriteStartDocument(False)
            'myXmlTextWriter.WriteDocType("FieldServiceXML", Nothing, "FieldServiceRequest.dtd", Nothing)

            myXmlTextWriter.WriteStartElement("FieldServiceXML")
            myXmlTextWriter.WriteProcessingInstruction("ContentType", "text/xml")

            'Request Header Starts here
            myXmlTextWriter.WriteStartElement("RequestHeader", Nothing)
            myXmlTextWriter.WriteAttributeString("version", "1.2")
            myXmlTextWriter.WriteAttributeString("status", "Update")
            myXmlTextWriter.WriteElementString("ClientID", CustomerName)
            myXmlTextWriter.WriteElementString("ClientRequestNumber", customerTicketID)
            myXmlTextWriter.WriteElementString("PrimeContractorRequestNumber", ticketID)
            myXmlTextWriter.WriteElementString("AssignedServiceProvider", "")
            myXmlTextWriter.WriteElementString("ServiceProviderRequestNumber", sfaticketid)
            myXmlTextWriter.WriteElementString("Timestamp", sDate)
            myXmlTextWriter.WriteEndElement()
            'Request Header Ends here
            'ServiceRequest Starts here
            myXmlTextWriter.WriteStartElement("ServiceRequest", Nothing)
            myXmlTextWriter.WriteElementString("Summary", Summary)
            myXmlTextWriter.WriteElementString("Description", Description)

            'Start Request Category Segment
            myXmlTextWriter.WriteStartElement("RequestCategory", Nothing)
            myXmlTextWriter.WriteElementString("CategoryCode", CategoryCode)
            myXmlTextWriter.WriteElementString("CategoryText", CategoryText)
            myXmlTextWriter.WriteEndElement()


            'End Request Category Segment
            myXmlTextWriter.WriteStartElement("Priority", Nothing)
            myXmlTextWriter.WriteAttributeString("id", PriorityIntId)
            myXmlTextWriter.WriteString(Priority)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("VendorEngineerNumber", "")

            'Start Asset Segment here
            myXmlTextWriter.WriteStartElement("Asset", Nothing)
            myXmlTextWriter.WriteElementString("SerialNumber", SerialNumber)
            myXmlTextWriter.WriteElementString("AssetTag", AssetTag)
            myXmlTextWriter.WriteElementString("Manufacturer", Manufacturer)
            myXmlTextWriter.WriteElementString("Type", AssetType)
            myXmlTextWriter.WriteElementString("Model", Model)
            myXmlTextWriter.WriteEndElement()
            'End Asset Segment here

            'Customer Segment Begins here
            myXmlTextWriter.WriteStartElement("Customer", Nothing)
            myXmlTextWriter.WriteElementString("LocationNumber", LocationNumber)
            myXmlTextWriter.WriteElementString("CustomerName", CustomerName)
            'Begins Address Segment
            myXmlTextWriter.WriteStartElement("Address", Nothing)
            myXmlTextWriter.WriteAttributeString("id", addressid)
            myXmlTextWriter.WriteElementString("Address1", Address1)
            myXmlTextWriter.WriteElementString("Address2", Address2)
            myXmlTextWriter.WriteElementString("City", City)
            myXmlTextWriter.WriteElementString("StateProvince", StateProvinceRegion)
            myXmlTextWriter.WriteElementString("PostalCode", PostalCode)
            myXmlTextWriter.WriteStartElement("Country", Nothing)
            myXmlTextWriter.WriteAttributeString("isoCountryCode", isoCountryCode)
            myXmlTextWriter.WriteString(CountryName)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'End Of Address Segment
            myXmlTextWriter.WriteElementString("PhoneNumber", PhoneNumber)
            myXmlTextWriter.WriteElementString("EmailAddress", EmailAddress)
            myXmlTextWriter.WriteStartElement("Contact", Nothing)
            myXmlTextWriter.WriteAttributeString("id", affectedUserContactID)
            myXmlTextWriter.WriteString(Contact)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("HoursOfOperation", HoursOfOperation)
            myXmlTextWriter.WriteEndElement()
            'Customer segment Ends here
            'Status Segment Begins here
            myXmlTextWriter.WriteStartElement("Status", Nothing)
            myXmlTextWriter.WriteElementString("StatusCode", StatusCode)
            myXmlTextWriter.WriteElementString("StatusText", StatusText)
            myXmlTextWriter.WriteElementString("CommentType", "R")
            myXmlTextWriter.WriteElementString("StatusComment", statusComment)
            myXmlTextWriter.WriteEndElement()
            'Status Segment Ends here
            myXmlTextWriter.WriteElementString("ResolutionText", resolutionText)
            myXmlTextWriter.WriteElementString("ResolutionTime", resolutionDate)
            myXmlTextWriter.WriteElementString("ETA", etaDate)

            myXmlTextWriter.WriteEndElement()
            'ServiceRequest Ends Here
            myXmlTextWriter.WriteEndElement()
            'FieldServiceXML Ends Here

            'Write the XML to file and close the writer
            myXmlTextWriter.Flush()
            myXmlTextWriter.Close()

        Catch e As Exception
            Console.WriteLine("Exception: {0}", e.ToString())
        Finally
            Console.WriteLine()
            Console.WriteLine("Processing of the file {0} complete.", OutboundFileName)
            'Close the writer
            If Not myXmlTextWriter Is Nothing Then
                myXmlTextWriter.Close()
            End If
        End Try


        'Now Posting the XML Document to the according URL
        '  HTTPPost(args)
        ' Console.WriteLine("Http Posting finished")
        Return (OutboundFileName)
    End Function

    ' ----------  ARPATECH  ----------
    ' this function calls another function
    ' for generating error XML according to
    ' three client ID's
    ' ----------  ARPATECH  ----------
    Public Function BuildErrorXML(ByVal ResponseTypeId As Integer, ByVal OutboundFileName As String) As String
        Dim XmlFiletoPost
        Dim ClientID As String
        ClientID = ""
        EventLog.WriteEntry("Application", "Inside BuildErrorXML. REsponsetypeid= " & ResponseTypeId)
        Try
            Dim Sqltbl As String = "Select ClientID from tblFieldServiceRequestInbound WHERE tblFieldServiceReqID =" & CInt(ResponseTypeId)
            ClientID = GSSupportManager.GetSingleDBValueString(Sqltbl)
            EventLog.WriteEntry("Application", "ClientID " & ClientID)

            If Len(ClientID) < 1 Then
                EventLog.WriteEntry("Application", "ClientID " & ClientID & " is not valid")
                XmlFiletoPost = ""
                Exit Function
            End If
            If InStr(UCase(ClientID), "WORLDSPAN") > 0 Then
                XmlFiletoPost = BuildGenericErrorXML(ResponseTypeId, OutboundFileName)
            End If
            If InStr(UCase(ClientID), "VALEANT PHARMACEUTICALS") > 0 Then
                XmlFiletoPost = BuildGenericErrorXML(ResponseTypeId, OutboundFileName)
            End If
            If InStr(UCase(ClientID), "GALLO") > 0 Then
                EventLog.WriteEntry("Application", "No active econnection for Gallo. Quit the procedure.")
                Exit Function
                XmlFiletoPost = BuildGALLOErrorXML(ResponseTypeId)
            End If
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Inside BuildErrorXML. Message " & e.Message & " Error Source: " & e.Source)

        End Try
        'Navigate TblOrders
        Return (XmlFiletoPost)
    End Function

    ' ----------  ARPATECH  ----------
    ' this function calls another function
    ' for generating error XML only for GALLO client
    ' ----------  ARPATECH  ----------
    Public Function BuildGALLOErrorXML(ByVal InputFSRId As Integer) As String
        Dim OutPutFileName As String
        Dim myds
        Dim mydr
        Dim RequestHeaderStatus As New String("")
        Dim ClientID As New String("")
        Dim CustomerReqNum As New String("")
        Dim PrimeContractorReqNum As New String("")
        Dim AssignedServiceProvider As New String("")
        Dim ServiceProviderReqNum As New String("")
        Dim TimeStamp As New String("")
        Dim ServiceRequestSummary As New String("")
        Dim ServiceRequestDescription As New String("")
        Dim ReqCategoryCode As New String("")
        Dim ReqCategoryText As New String("")
        Dim Priority As New String("")
        Dim VendorEngNum As New String("")
        Dim AssetSerialNum As New String("")
        Dim AssetSerialTag As New String("")
        Dim AssetManuf As New String("")
        Dim AssetType As New String("")
        Dim AssetModel As New String("")
        Dim LocationNumber As New String("")
        Dim CustomerName As New String("")
        Dim AddressID As New String("")
        Dim Address1 As New String("")
        Dim Address2 As New String("")
        Dim PriorityIntValue As Integer = -1
        Dim City As New String("")
        Dim StateProvince As New String("")
        Dim timezoneid As Integer = 0
        Dim PostalCode As New String("")
        Dim CountryCode As New String("")
        Dim Country As New String("")
        Dim CountryID As Integer = 0
        Dim PhoneNum As New String("")
        Dim Email As New String("")
        Dim CustomerContact As New String("")
        Dim HoursOfOperation As New String("")
        Dim StatusCode As New String("")
        Dim StatusText As New String("")
        Dim ResolutionText As New String("")
        Dim ResolutionTime As New String("")
        Dim ETA As New String("")
        Dim CommentType As New String("")
        Dim StatusComment As New String("")
        Dim CustomerContactID As New String("")
        Dim PriorityID As New String("")
        Dim sDate = Date.Now.ToString("yyyy-MM-ddTHH:mm:ss")
        Dim Sqltbl As String = "Select * from tblFieldServiceRequestInbound WHERE tblFieldServiceReqID =" & InputFSRId
        myds = GetDataTable(Sqltbl)

        For Each mydr In myds.Tables("dt").Rows
            ClientID = stringSaturate(mydr("ClientID").ToString)
            CustomerReqNum = stringSaturate(mydr("CustomerReqNum").ToString)
            PrimeContractorReqNum = stringSaturate(mydr("PrimeContractorReqNum").ToString)
            AssignedServiceProvider = stringSaturate(mydr("AssignedServiceProvider").ToString)
            ServiceProviderReqNum = stringSaturate(mydr("ServiceProviderReqNum").ToString)
            CustomerReqNum = stringSaturate(mydr("CustomerReqNum").ToString)
            PrimeContractorReqNum = stringSaturate(mydr("PrimeContractorReqNum").ToString)
            AssignedServiceProvider = stringSaturate(mydr("AssignedServiceProvider").ToString)
            ServiceRequestSummary = stringSaturate(mydr("ServiceRequestSummary").ToString)
            ServiceRequestDescription = stringSaturate(mydr("ServiceRequestDescription").ToString)
            StatusCode = stringSaturate(mydr("StatusCode").ToString)
            StatusText = stringSaturate(mydr("StatusText").ToString)
            ResolutionText = stringSaturate(mydr("ResolutionText").ToString)
            ResolutionTime = stringSaturate(mydr("ResolutionTime").ToString)
            ETA = stringSaturate(mydr("ETA").ToString)
            CommentType = stringSaturate(mydr("CommentType").ToString)
            StatusComment = stringSaturate(mydr("StatusComment").ToString)
        Next
        myds.Clear()
        mydr = Nothing
        Dim Statusname

        'Start building XML message
        Dim myXmlTextWriter As XmlTextWriter = Nothing
        myXmlTextWriter = New XmlTextWriter(OutPutFileName, Nothing)
        Try
            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented
            myXmlTextWriter.WriteStartDocument(False)
            myXmlTextWriter.WriteDocType("helpDesk", Nothing, "helpDesk.dtd", Nothing)
            myXmlTextWriter.WriteStartElement("helpDesk")
            myXmlTextWriter.WriteAttributeString("version", "1.0")
            myXmlTextWriter.WriteAttributeString("payloadID", "1.0")
            myXmlTextWriter.WriteAttributeString("timestamp", sDate)
            'Request Header Starts here
            myXmlTextWriter.WriteStartElement("Header", Nothing)
            myXmlTextWriter.WriteStartElement("from", Nothing)
            myXmlTextWriter.WriteAttributeString("type", "DUNS")
            myXmlTextWriter.WriteString("DUNS VALUE")
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("to", Nothing)
            myXmlTextWriter.WriteAttributeString("type", "DUNS")
            myXmlTextWriter.WriteString("DUNS VALUE")
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("sender", Nothing)
            myXmlTextWriter.WriteAttributeString("type", "DUNS")
            myXmlTextWriter.WriteString("DUNS VALUE")
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'Header ends here
            'Now start the request
            myXmlTextWriter.WriteStartElement("request", Nothing)
            myXmlTextWriter.WriteStartElement("troubleTicketRequest", Nothing)
            myXmlTextWriter.WriteAttributeString("action", "Update")
            myXmlTextWriter.WriteStartElement("troubleTicket", Nothing)
            myXmlTextWriter.WriteAttributeString("ticketId", CustomerReqNum)
            myXmlTextWriter.WriteAttributeString("referenceTicketId", PrimeContractorReqNum)
            myXmlTextWriter.WriteElementString("subject", ServiceRequestSummary)
            myXmlTextWriter.WriteElementString("Description", "Error Processing Request")
            myXmlTextWriter.WriteElementString("sevirity", "")
            myXmlTextWriter.WriteElementString("Status", StatusCode)
            'Close troubleTicket
            myXmlTextWriter.WriteEndElement()
            'Close troubleTicketRequest
            myXmlTextWriter.WriteEndElement()
            'Close request
            myXmlTextWriter.WriteEndElement()
            'Close helpDesk
            myXmlTextWriter.WriteEndElement()
            'Write the XML to file and close the writer
            myXmlTextWriter.Flush()
            myXmlTextWriter.Close()

        Catch e As Exception
            Console.WriteLine("Exception: {0}", e.ToString())
        Finally
            Console.WriteLine()
            Console.WriteLine("Processing of the file {0} complete.", OutPutFileName)
            'Close the writer
            If Not myXmlTextWriter Is Nothing Then
                myXmlTextWriter.Close()
            End If
        End Try
        'Now Posting the XML Document to the according URL
        '  HTTPPost(args)
        ' Console.WriteLine("Http Posting finished")
        Return (OutPutFileName)

    End Function

    ' ----------  ARPATECH  ----------
    ' function for replacing some values with SPACE
    ' ----------  ARPATECH  ----------
    Public Function stringSaturate(ByVal sString As String) As String
        If sString <> "" Then
            sString = Trim(sString)
            sString = Replace(sString, "  ", " ")
            sString = Replace(sString, "'", " ")
            sString = Replace(sString, """", " ")
            sString = Replace(sString, vbTab, " ")
            sString = Replace(sString, vbCrLf, " ")
            sString = Replace(sString, vbNewLine, " ")
            Return sString
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' this function reads data according to 
    ' query passed and returns as Dataset
    ' ----------  ARPATECH  ----------
    Public Function GetDataTable(ByVal PassSql As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Dim sqlConn As SqlConnection = New SqlConnection(DBString)

        ' Dim sqlConn As SqlConnection = New SqlConnection(GSSupportManager.GetDBString())
        sqlConn.Open()
        Dim SqlDataAdaptertblOrders As SqlDataAdapter = New SqlDataAdapter(PassSql, sqlConn)
        Try
            SqlDataAdaptertblOrders.Fill(ds, "dt")
        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to Load while trying to process " & PassSql & e.Message)
            ds.Clear()
            sqlConn.Close()
            sqlConn = Nothing
        End Try
        Return ds

    End Function

    ' ----------  ARPATECH  ----------
    ' function writes provided information to file
    ' ----------  ARPATECH  ----------
    Public Function WriteFile(ByVal InputString As String, ByVal FileName As String) As String
        'FileName = SaturateFileNameString(FileName)
        'Console.WriteLine("string file name: " & FileName)
        Dim oFile As System.IO.File
        Dim oWrite As System.IO.StreamWriter
        Try
            oWrite = oFile.CreateText(FileName)
            oWrite.WriteLine(InputString)
            oWrite.Close()

        Catch E As Exception
            ' let the user know that the information could not be written
            EventLog.WriteEntry("application", "WRITE FILE EXCEPTION: " & Chr(13) & E.Message)
        End Try
    End Function

    ' ----------  ARPATECH  ----------
    ' this function replaces / \ : space to hypen(-)
    ' ----------  ARPATECH  ----------
    Public Function SaturateFileNameString(ByVal passString As String) As String
        passString = Replace(passString, "/", "_")
        passString = Replace(passString, "\", "_")
        passString = Replace(passString, " ", "_")
        passString = Replace(passString, ":", "_")
        Return passString
    End Function

    ' ----------  ARPATECH  ----------
    ' this function posts specified file 
    ' to a web header specified
    ' ----------  ARPATECH  ----------
    Public Function HTTPPost(ByVal FileName As String, ByVal HTTPPostURL As String) As String  'ByVal StringToPost As String)
        Dim StringToPost
        StringToPost = ""
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(FileName)
            Dim line As String
            ' Read the lines from the file until the end 
            ' of the file is reached.
            Do
                line = sr.ReadLine()
                line = Replace(line, "&lt;br /&gt;", "<br />")
                StringToPost = StringToPost & line & " "
            Loop Until line Is Nothing
            sr.Close()
        Catch E As Exception
            ' Let the user know what went wrong.
            EventLog.WriteEntry("application", "The file " & FileName & " could not be read. Error: " & E.Message)
        End Try
        Dim web As New System.Net.WebClient
        web.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
        web.Headers.Add("ContentType", "text/xml")
        Dim dataString As Byte() = System.Text.Encoding.ASCII.GetBytes(StringToPost)
        ' Dim res As Byte() = web.UploadData("http://10.10.20.152/eCommerce/CompaqCAFE/inboundorder.asp", "POST", dataString)
        ' Dim res As Byte() = web.UploadData("http://b2bintegration.global-serve.com:7001/xml", "POST", dataString)
        'this is how you do the HTTP Posting of any string
        Dim res As Byte() = web.UploadData(HTTPPostURL, "POST", dataString)
        Dim ReturnedResponse = System.Text.Encoding.ASCII.GetString(res)
        'EventLog.WriteEntry("Application", "returned response: " & ReturnedResponse)
        Return ReturnedResponse
    End Function

    ' ----------  ARPATECH  ----------
    ' function for validating call back event
    ' ----------  ARPATECH  ----------
    Public Shared Sub ValidationCallBack(ByVal sender As Object, ByVal args As ValidationEventArgs)
        Console.WriteLine("***Validation error")
        Console.WriteLine("Severity:{0}", args.Severity)
        Console.WriteLine("Message:{0}", args.Message)
    End Sub

    ' ----------  ARPATECH  ----------
    ' this function sends a alert email to
    ' global serve team for any unusual event
    ' ----------  ARPATECH  ----------
    Public Function SendAlertEmail(ByVal OutputFileName As String, ByVal AlertType As String) As Boolean

        Dim StringToPost
        Try
            ' Create an instance of TextReader to read from a file.
            Dim sr As StreamReader = New StreamReader(OutputFileName)
            Dim line As String
            ' Read the lines from the file until the end 
            ' of the file is reached.
            Do
                line = sr.ReadLine()
                StringToPost = StringToPost & line & vbCrLf
            Loop Until line Is Nothing
            sr.Close()
            SendMail("econnect@global-serve.com", "eConnection-ServiceResponse", StringToPost, "e-Connection Error " & AlertType & " - FSR Posting Done", 2)

        Catch E As Exception
            ' Let the user know what went wrong.
            EventLog.WriteEntry("application", "Couldn't send the Alert Email. Error: " & E.Message & " Source: " & E.Source)
        End Try
        'Send Email
        EventLog.WriteEntry("application", "Alert Email is sent")
    End Function


End Class

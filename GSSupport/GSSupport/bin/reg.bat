net stop "World Wide Web Publishing Service"
net stop "Simple Mail Transport Protocol (SMTP)"
net stop "IIS Admin Service"
net stop "Message Queuing"

regasm /u GSSupport.dll
regasm /tlb GSSupport.dll

gacutil /u MSMQ
gacutil /i MSMQ.dll

gacutil /u GSSupport
gacutil /i GSSupport.dll

net start "Message Queuing"
net start "IIS Admin Service"
net start "Simple Mail Transport Protocol (SMTP)"
net start "World Wide Web Publishing Service"



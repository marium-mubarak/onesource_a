' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is a monitoring class for every action request. It is capable of
'                       notifying the status, startdate, and enddate of ActionRequests and also
'                       shows workflow instance details as well.
' Inherits          :   ---------
' Implements        :   IActionMonitor and IMQUtils
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports GSSupport.GSAppGlobals
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionMonitor
    Implements IActionMonitor
    Implements IMQUtils


    Private m_action As Action = New ActionEmpty()
    Private m_actionID As Integer = 0
    Private m_actionInstanceID As Integer = 0
    Private m_inProgress As Boolean = False
    Private m_startDate As Date
    Private m_endDate As Date
    Private m_gsTicketID As Integer = -1
    Private m_actionRequest As ActionRequest
    Private m_retryCount As Integer = 0
    Private m_actionStatus As New String("")
    Private m_mqLabel As New String("")
    Private m_actionEvent As New EventArgs()
    Private m_gsSupportManager As GSSupportManager
    Private m_gsListener As GSEventListener
    Private m_actions As Action()


    ' ----------  ARPATECH  ----------
    '       SAME CLASS INTERFACE
    ' ----------  ARPATECH  ----------
    Public Interface IActionMonitor
        Sub Execute()
        Sub Execute(ByVal action As Action)
        Property InProgress() As Boolean
        Property IsError() As Boolean
        Property ActionStatus() As String
        Property MQLabel() As String
        ReadOnly Property StartDate() As Date
        ReadOnly Property EndDate() As Date
        ReadOnly Property ActionInstanceID() As Integer
        Property Action() As Action
        Property ActionID() As Integer
        Function GetActionRequestID() As Integer
        Property ActionRequest() As ActionRequest
        Function GetWorkflowInstanceID() As Integer
        Function GetScheduleID() As Integer
        Function GetGSTicketID() As Integer
        Property RetryCount() As Integer
        Function Process(ByVal path As String) As Boolean
        Sub FireActionEvent()
        Property Manager() As GSSupportManager



    End Interface

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Sub New(ByVal act As Action)

        Me.Action = act
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Action ID variable
    ' ----------  ARPATECH  ----------
    Public Property ActionID() As Integer Implements IActionMonitor.ActionID
        Get
            Return m_actionID
        End Get
        Set(ByVal Value As Integer)
            m_actionID = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action variable but also action arrays as well
    ' ----------  ARPATECH  ----------
    '  <XmlArrayItem(Type:=GetType(GSSupport.ActionUpdateTicket)), _
    '    XmlArrayItem(Type:=GetType(GSSupport.ActionUpdateOrder)), _
    ' <XmlArrayItem(Type:=GetType(GSSupport.ActionEmail))> _
    ' <System.Xml.Serialization.XmlIgnore()> _
    ' <XmlElement(IsNullable:=False)> _
    ' <System.Xml.Serialization.XmlIgnore()> _
    Public Property Action() As Action Implements IActionMonitor.Action
        Get

            If Not IsNothing(m_actions) AndAlso UBound(m_actions) >= 0 Then

                Return m_actions(0)
            Else

                Return New ActionEmpty()

            End If

        End Get
        Set(ByVal Value As Action)
            Try

                m_action = Value
                '  m_actionID = Value.ActionID

                ReDim m_actions(1)

                m_actions(0) = Value

                If IsNothing(m_action) Then
                    EventLog.WriteEntry("Application", "m_action is nothing")
                End If

            Catch e As Exception
                EventLog.WriteEntry("Application", "Error setting action: " & e.Message & Value.GetType.ToString())
            End Try

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' a variable determining error status
    ' ----------  ARPATECH  ----------
    Public Property IsError() As Boolean Implements IActionMonitor.IsError
        Get
            Return m_action.IsError
        End Get
        Set(ByVal Value As Boolean)
            m_action.IsError = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' a variable determining action status
    ' ----------  ARPATECH  ----------
    Public Property ActionStatus() As String Implements IActionMonitor.ActionStatus
        Get
            Return m_actionStatus
        End Get
        Set(ByVal Value As String)
            If Not IsNothing(m_action) AndAlso Len(m_actionStatus) > 0 AndAlso m_actionStatus <> Value Then
                LogActionEvent("Status changed to " & m_actionStatus, "Information")
            End If
            m_actionStatus = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Listener variable
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
Public Property GSListener() As GSEventListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            m_gsListener = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' from MQLabel Variable
    ' ----------  ARPATECH  ----------
    Public Property MQLabel() As String Implements IActionMonitor.MQLabel
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' support manager type variable
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
Public Property Manager() As GSSupportManager Implements IActionMonitor.Manager
        Get
            Return m_gsSupportManager
        End Get
        Set(ByVal Value As GSSupportManager)
            m_gsSupportManager = Value
            If Not IsNothing(m_action) Then
                m_action.Manager = m_gsSupportManager
            End If
            If Not IsNothing(Value.GSListener) And IsNothing(m_gsListener) Then
                m_gsListener = Value.GSListener
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' AR type variable and if no value is passed
    ' it will create from ActionClass
    ' ----------  ARPATECH  ----------
    '<System.Xml.Serialization.XmlIgnore()> _
    Public Property ActionRequest() As ActionRequest Implements IActionMonitor.ActionRequest

        Get
            Return m_actionRequest
        End Get
        Set(ByVal Value As ActionRequest)
            Try
                m_actionRequest = Value



                ' If I don't have an action or it is empty, try to create one with the AR
                If (IsNothing(m_action) OrElse UCase(m_action.GetType.ToString) = "GSSUPPORT.ACTIONEMPTY") And Not IsNothing(m_actionRequest.RequestedActionID) Then
                    Dim af As New ActionFactory()
                    Dim a As Action = af.CreateActionFromAR(m_actionRequest)
                    If Not IsNothing(a) Then
                        Me.Action = a
                    End If
                End If

                If Not IsNothing(m_action) Then
                    m_action.ActionRequest = m_actionRequest
                End If


            Catch e As Exception
                EventLog.WriteEntry("Application", "Error setting AM.AR: " & e.Message())
            End Try
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From retries counting variable
    ' ----------  ARPATECH  ----------
    Public Property RetryCount() As Integer Implements IActionMonitor.RetryCount

        Get
            Return m_retryCount
        End Get
        Set(ByVal Value As Integer)
            m_retryCount = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A readonly Property for Retrieving
    ' action intance ID
    ' ----------  ARPATECH  ----------
    Public ReadOnly Property ActionInstanceID() As Integer Implements IActionMonitor.ActionInstanceID
        Get
            Return m_actionInstanceID
        End Get

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for cheking current status of Action Monitor
    ' ----------  ARPATECH  ----------
    Public Property InProgress() As Boolean Implements IActionMonitor.InProgress
        Get
            Return m_inProgress
        End Get
        Set(ByVal Value As Boolean)
            m_inProgress = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A readonly Property for Retrieving
    ' start date of action monitor
    ' ----------  ARPATECH  ----------
    Public ReadOnly Property StartDate() As Date Implements IActionMonitor.StartDate
        Get
            Return m_startDate
        End Get

    End Property

    ' ----------  ARPATECH  ----------
    ' A read only Property for Retrieving
    ' end date of action monitor
    ' ----------  ARPATECH  ----------
    Public ReadOnly Property EndDate() As Date Implements IActionMonitor.EndDate
        Get
            Return m_endDate
        End Get
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for running this class instance
    ' overloaded with action argument
    ' ----------  ARPATECH  ----------
    Sub Execute(ByVal a As Action) Implements IActionMonitor.Execute
        Me.Action = a
        Execute()
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for running this class instance
    ' overloaded with no argument
    ' ----------  ARPATECH  ----------
    Sub Execute() Implements IActionMonitor.Execute

        '    EventLog.WriteEntry("Application", "AM EXECUTING ACTION...")
        Try

            If IsNothing(m_action) Then

                EventLog.WriteEntry("Application", "Attempted to run Execute() without setting an action.")
            Else

                Dim iActionInstanceID As Integer
                iActionInstanceID = UpdateDBStart()
                If iActionInstanceID < 1 Then
                    EventLog.WriteEntry("Application", "Unable to update DB on Action Start.")
                Else
                    m_inProgress = True
                    m_actionInstanceID = iActionInstanceID
                    m_action.ActionInstanceID = iActionInstanceID
                End If
                m_action.Execute()


                UpdateDBEnd()
                Dim manager As New GSSupportManager()
                manager.ActionPostProc(m_action.ActionRequest.MQLabel, CreateMQMessage())

            End If
        Catch e As Exception

        Finally

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that updates DB at start
    ' and returns action instance ID
    ' ----------  ARPATECH  ----------
    ' Update database at start of action execution... returns actionInstanceID
    Function UpdateDBStart() As Integer
        Dim iActionInstanceID As Integer = 0

        If IsNothing(m_actionRequest) Then
            EventLog.WriteEntry("Application", "Error AM.UpdateDB Start: AR is nothing")
        End If


        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_LaunchAction"

            SqlCommandBuilder.DeriveParameters(Cmd)



            Cmd.Parameters("@actionid").Value = m_actionRequest.RequestedActionID

            If Not IsNothing(m_actionRequest.GSTicketID) AndAlso m_actionRequest.GSTicketID > 0 Then
                Cmd.Parameters("@ticketid").Value = m_actionRequest.GSTicketID
            End If
            Cmd.Parameters("@arid").Value = m_actionRequest.ActionRequestID


            If Not IsNothing(m_actionRequest.WorkflowInstance) AndAlso Not IsNothing(m_actionRequest.WorkflowInstance.WorkflowInstanceID) AndAlso m_actionRequest.WorkflowInstance.WorkflowInstanceID > 0 Then
                Cmd.Parameters("@workflowinstanceid").Value = m_actionRequest.WorkflowInstance.WorkflowInstanceID
            End If

            If Not IsNothing(m_actionRequest.ScheduleID) AndAlso m_actionRequest.ScheduleID > 0 Then
                Cmd.Parameters("@scheduleid").Value = m_actionRequest.ScheduleID
            End If


            If Not IsNothing(m_actionRequest.SubmitUserID) Then
                Cmd.Parameters("@userid").Value = m_actionRequest.SubmitUserID
            End If

            Cmd.Parameters("@actioninstanceid").Value = 0

            Cmd.ExecuteNonQuery()

            iActionInstanceID = Cmd.Parameters("@actioninstanceid").Value


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error ActionLaunchDB: " & e.Message)

        Finally
            Conn.Close()
        End Try
        Return iActionInstanceID

    End Function

    ' ----------  ARPATECH  ----------
    ' function that updates DB at end
    ' and returns action instance ID
    ' ----------  ARPATECH  ----------
    ' Update database at start of action execution... returns actionInstanceID
    Sub UpdateDBEnd()


        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        If IsNothing(Me.Action) Then
            EventLog.WriteEntry("Application", "ERROR: Action Monitor tried to end an action before the action was set.")

        End If

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn


        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_LogActionEnd"

            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@actioninstanceid").Value = m_actionInstanceID
            Cmd.Parameters("@isError").Value = m_action.IsError
            Cmd.Parameters("@errStr").Value = m_action.ErrString

            Cmd.ExecuteNonQuery()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error logging action end: " & e.Message)

        Finally
            Conn.Close()
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for logging all action events occurred
    ' overloaded with message and type
    ' ----------  ARPATECH  ----------
    Sub LogActionEvent(ByVal message As String, ByVal type As String)
        Dim iActionInstanceID As Integer
        iActionInstanceID = 0

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        If IsNothing(Me.Action) Then
            EventLog.WriteEntry("Application", "Error logging action event: no action set.")
            Exit Sub

        End If

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn


        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_LogActionEvent"

            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@actioninstanceid").Value = Me.ActionInstanceID
            Cmd.Parameters("@status").Value = Me.ActionStatus
            Cmd.Parameters("@type").Value = type
            Cmd.Parameters("@comment").Value = message
            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error logging action event: " & e.Message)

        Finally
            Conn.Close()
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for logging all action events occurred
    ' overloaded with message and type, status and action instance ID
    ' ----------  ARPATECH  ----------
    Public Shared Sub LogActionEvent(ByVal actionInstanceID As Integer, ByVal status As String, ByVal message As String, ByVal type As String)
        Dim iActionInstanceID As Integer
        iActionInstanceID = 0

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())


        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn


        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_LogActionEvent"

            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@actioninstanceid").Value = actionInstanceID
            Cmd.Parameters("@status").Value = status
            ' Cmd.Parameters("@type").Value = type
            Cmd.Parameters("@comment").Value = message
            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error logging action event: " & e.Message)

        Finally
            Conn.Close()
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function for retreiving action request ID
    ' ----------  ARPATECH  ----------
    Public Function GetActionRequestID() As Integer Implements IActionMonitor.GetActionRequestID
        Dim arid As Integer = 0
        If Not IsNothing(m_actionRequest) Then
            arid = m_actionRequest.ActionRequestID
        End If
        GetActionRequestID = arid

    End Function

    ' ----------  ARPATECH  ----------
    ' function for retreiving action ticket ID
    ' ----------  ARPATECH  ----------
    Public Function GetGSTicketID() As Integer Implements IActionMonitor.GetGSTicketID
        GetGSTicketID = m_actionRequest.GSTicketID
    End Function

    ' ----------  ARPATECH  ----------
    ' function for retreiving work flow ID
    ' ----------  ARPATECH  ----------
    Public Function GetWorkflowInstanceID() As Integer Implements IActionMonitor.GetWorkflowInstanceID
        Dim wfiid As Integer = 0
        If Not IsNothing(m_actionRequest) Then
            wfiid = m_actionRequest.WorkflowInstance.WorkflowInstanceID
        End If
        GetWorkflowInstanceID = wfiid
    End Function

    ' ----------  ARPATECH  ----------
    ' function for retreiving work schedule id
    ' ----------  ARPATECH  ----------
    Public Function GetScheduleID() As Integer Implements IActionMonitor.GetScheduleID
        Dim sid As Integer = 0
        If Not IsNothing(m_actionRequest) Then
            sid = m_actionRequest.ScheduleID()
        End If
        GetScheduleID = sid
    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY / Commented CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()
        'ReDim m_actions(1)
        'm_actions(0) = New ActionEmpty()
        ' Me.Actions = m_actions

    End Sub

    ' ----------  ARPATECH  ----------
    ' Function that is suppose to do something
    ' but actully does nothing
    ' ----------  ARPATECH  ----------
    Function Process(ByVal o As String) As Boolean Implements IActionMonitor.Process
        Dim b As Boolean = True
        Dim newO As New Object()
        Dim fb As New System.Messaging.BinaryMessageFormatter()
        Dim mq As New System.Messaging.MessageQueue()


        Try


            ' m.Body = o




            '  MessageBox.Show("Received " & CStr(o))
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Receiving AR: " & e.Message & "," & e.Source)

        End Try

        Return b
    End Function

    ' ----------  ARPATECH  ----------
    ' fucntion that serializes all this
    ' class instance and returns MSMQ message type
    ' ----------  ARPATECH  ----------
    Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage
        Dim l As String
        Dim m As New MSMQMessage()

        Try
            Dim t As Type = GetType(GSSupport.ActionMonitor)


            Dim serializer As New XmlSerializer(t)
            Dim objxml As New String("")
            Dim ns As New XmlSerializerNamespaces()
            Dim ms As MemoryStream = New MemoryStream()
            Dim writer As New XmlTextWriter(ms, New UTF8Encoding())

            ' Serialize using the XmlTextWriter.
            serializer.Serialize(writer, Me, ns)

            Dim arChars As Char()
            ReDim arChars(ms.Length)

            ms.ToArray.CopyTo(arChars, 0)

            objxml = New String(arChars)


            writer.Close()
            l = CStr(Now())
            If Not IsNothing(m_actionInstanceID) Then
                l = l & "&AiD=" & CStr(m_actionInstanceID)
            End If
            If Not IsNothing(m_action) AndAlso Not IsNothing(m_action.ActionRequest) Then
                l = l & "&AR=" & CStr(m_action.ActionRequest.ActionRequestID) & "&ERR=" & CStr(m_action.IsError)
            End If

            m.Label = l
            m_mqLabel = l

            m.Body = objxml


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error converting Action Monitor to message: " & e.Message & "," & e.Source & "," & e.StackTrace & e.InnerException.Message)
        End Try
        Return m

    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine for calling FireActionEvent
    ' of Action Class
    ' ----------  ARPATECH  ----------
    Public Sub FireActionEvent() Implements IActionMonitor.FireActionEvent
        Try
            ' Do whatever action-specific postprocessing needs to be done.
            If IsNothing(m_action) Then
                EventLog.WriteEntry("Application", "Error: tried to fire action event with no action.")
                Exit Sub
            End If
            m_action.FireActionEvent()

            ' If it was an ActionUpdateTicket check for new SLAs
            '  If UCase(m_action.GetType.ToString) = "GSSUPPORT.ACTIONUPDATETICKET" Then

            '   Dim aut As ActionUpdateTicket = m_action
            ' If aut.IsNewType Then
            '      aut.RecheckSLAs(m_gsSupportManager)
            '  End If
            '  End If


        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error AM.action firing actionevent:" & ex.Message)

        End Try

        Try


            ' Raise a generic Action Complete Event
            Dim e As New ActionCompleteEventArgs()
            e.ActionMonitor = Me
            m_actionRequest.GSListener.RaiseActionCompleteEvent(Me, e)
        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error AM firing actioncompleteevent:" & ex.Message)

        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for action instance value
    ' ----------  ARPATECH  ----------
    <XmlArrayItem(Type:=GetType(GSSupport.ActionUpdateTicket)), _
       XmlArrayItem(Type:=GetType(GSSupport.ActionUpdateOrder)), _
              XmlArrayItem(Type:=GetType(GSSupport.ActionCXMLOrderRequest)), _
             XmlArrayItem(Type:=GetType(GSSupport.ActionEmpty)), _
             XmlArrayItem(Type:=GetType(GSSupport.ActionServiceResponseOutbound)), _
               XmlArrayItem(Type:=GetType(GSSupport.ActionCommandExec)), _
    XmlArrayItem(Type:=GetType(GSSupport.ActionEmail))> _
   Public Property Actions() As Action()
        Get
            Return m_actions
        End Get
        Set(ByVal Value As Action())
            m_actions = Value
            '     If Not IsNothing(m_actions(0)) Then
            '       m_action = m_actions(0)
            '   End If
        End Set
    End Property
End Class

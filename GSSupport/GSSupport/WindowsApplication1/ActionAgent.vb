﻿' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains only one Sub �ExecuteAction�. This sub creates
'                       instance of ActionMonitorClass and executes it.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.Timers
Imports System.Messaging
Imports GSSupport
Imports System.Collections
Imports System.Data.SqlClient
Imports System.DateTime
Imports System.Runtime.Remoting.Lifetime
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.Runtime.Remoting
Imports System.Security.Policy

Imports MSMQ
Imports MSMQ.MQMSGCLASS
Imports MSMQ.MQMSGCURSOR
Imports MSMQ.MQMSGTRACE



Imports MSMQ.MQTRANSACTION
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports MSMQ.MQERROR
Imports Microsoft.VisualBasic
Imports System.Management

Public Class ActionAgent
    Public ar As ActionRequest
    ' Public am As ActionMonitor

    ' ----------  ARPATECH  ----------
    ' sub routine executed on Execute()
    ' ----------  ARPATECH  ----------
    Sub ExecuteAction()

        Dim am As New ActionMonitor()
        Try
            EventLog.WriteEntry("Applicaiton", "Service executing action")

            am.ActionRequest = ar
            am.Execute()

            Dim m As MSMQMessage = am.CreateMQMessage()


            If Not SendToQ(ActionPostProcQ, m) Then
                EventLog.WriteEntry("Application", "Unable to send processed action to action postproc q")
            End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error executing action:" & e.Message)
        Finally
            '   am = Nothing
            '' GC.Collect(GC.GetGeneration(am))
            ' Thread.CurrentThread.Abort()

        End Try

    End Sub

End Class

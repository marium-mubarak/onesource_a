﻿Imports System
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.Timers
Imports System.Messaging
Imports GSSupport
Imports System.Collections
Imports System.Data.SqlClient
Imports System.DateTime
Imports System.Runtime.Remoting.Lifetime
Imports System.Threading
Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Runtime.Remoting
Imports System.Security.Policy
Imports System.Xml.Serialization
Imports System.Globalization.Calendar
Imports MSMQ
Imports MSMQ.MQMSGCLASS
Imports MSMQ.MQMSGCURSOR
Imports MSMQ.MQMSGTRACE
Imports System.Xml.Schema       'Need this for XML Validation
Imports System.Data
Imports MSMQ.MQTRANSACTION
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports MSMQ.MQERROR
Imports Microsoft.VisualBasic
Imports System.Management



Public Class Form1

   Friend ARMessageHandler 

    'knowledge    
    '  Protected myTimer As New System.Timers.Timer(60000)600000
    Protected myTimer As New System.Timers.Timer(60000)

    Public manager As GSSupportManager
    Private binformat As New System.Messaging.BinaryMessageFormatter()
    ' SortedList with vbTimeZoneConst as Key, FieldName as text Value
    Public m_dateFields As New ArrayList()
    Public m_gsListener As GSEventListener
    Public m_AlertEmail As String
    Private m_arProcQ As ArrayList
    Public m_timeConverter As New GSTime.TimeConverter()
    Public m_Workflows As New SortedList()
    Private m_casID As New Integer()

    Private m_customerxreftype As New Integer()

    ' Declare Message Queuing objects
    Public q As MSMQQueue
    Private WithEvents qevent As MSMQEvent

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        myTimer.Start()

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    ' ----------  ARPATECH  ----------
    ' sub routine for updating workflow
    ' on receiving completion signal
    ' ----------  ARPATECH  ----------
    Private Sub OnReceiveWorkflowCompleted(ByVal sender As System.Object, ByVal e As System.Messaging.ReceiveCompletedEventArgs)

        Try
            Dim arh As New ARMessageHandler()
            arh.service = Me
            arh.e = e
            arh.sender = sender
            arh.UpdateWorkflow()

            '  Dim MyThread As New System.Threading.Thread(AddressOf arh.UpdateWorkflow)
            '  MyThread.Start()

        Catch ex As Exception
            EventLog.WriteEntry("Workflow Listener Error", "Error forwarding action request:" & ex.Message)
        Finally
            RestartWorkflowListener()
        End Try

    End Sub
    ' ----------  ARPATECH  ----------
    ' sub routine for updating SLA
    ' on receiving completion signal
    ' ----------  ARPATECH  ----------
    Private Sub OnReceiveSLACompleted(ByVal sender As System.Object, ByVal e As System.Messaging.ReceiveCompletedEventArgs)

        Try

            Dim arh As New ARMessageHandler()
            arh.service = Me
            arh.e = e
            arh.sender = sender

            Dim MyThread As New System.Threading.Thread(AddressOf arh.UpdateSLA)
            MyThread.Start()

        Catch ex As Exception
            EventLog.WriteEntry("SLA Listener Error", "Error forwarding new SLA:" & ex.Message)
        Finally
            RestartSLAListener()
        End Try

    End Sub
    ' ----------  ARPATECH  ----------
    ' sub routine for updating NOTHING
    ' on receiving completion signal
    ' ----------  ARPATECH  ----------
    Private Sub OnReceiveCompleted(ByVal sender As System.Object, ByVal e As System.Messaging.ReceiveCompletedEventArgs)

        Try
            '  HandleAR(sender, e)

            ' Dim MyThread As New System.Threading.Thread(AddressOf arh.HandleAR)
            ' MyThread.Start()

        Catch ex As Exception
            EventLog.WriteEntry("AR Listener Error", "Error forwarding action request:" & ex.Message)
        Finally
            RestartListener()
        End Try
    End Sub
    ' ----------  ARPATECH  ----------
    ' sub routine that works on start
    ' and performs some besic operations
    ' and creates new threads as per requirement
    ' ----------  ARPATECH  ----------
    Protected Sub OnStart(ByVal args() As String)

        GetDateFields()
        manager = New GSSupportManager()
        m_gsListener = New GSEventListener()
        manager.GSListener = m_gsListener

        GC.KeepAlive(m_timeConverter)
        m_Workflows = New SortedList()

        m_casID = GSSupportManager.GetSingleDBValueInt("select userid from tblUsers where upper(username) = 'CASAGENT'")
        m_customerxreftype = GSSupportManager.GetSingleDBValueInt("select xreftypeid from tblXrefTypes where xrefTypeCode = 'C'")
        m_AlertEmail = GSSupportManager.GetAlertEmail()
        EventLog.WriteEntry("GSSupport Service", manager.LoadWorkflows(m_Workflows) & " workflows added.")
        EventLog.WriteEntry("GSSupport Service", manager.LoadSchedules & " schedules added.")
        EventLog.WriteEntry("GSSupport Service", manager.LoadSLAs & " slas added.")
        EventLog.WriteEntry("GSSupport Service", manager.LoadSLAsInProgress & " slas in progress added.")
        EventLog.WriteEntry("GSSupport Service", manager.LoadFields & " fields added.")


        Dim t1 As New System.Threading.Thread(AddressOf IncomingARQListen)
        t1.Start()

        Dim t2 As New System.Threading.Thread(AddressOf OrderEventQListen)
        t2.Start()

        Dim t3 As New System.Threading.Thread(AddressOf UpdatesQListen)
        t3.Start()

        Dim t4 As New System.Threading.Thread(AddressOf EConnectionListen)
        t4.Start()

        Dim t5 As New System.Threading.Thread(AddressOf NewOrderListen)
        t5.Start()

        Dim t6 As New System.Threading.Thread(AddressOf OrderChangeListen)
        t6.Start()

        ' Start Timer for events
        AddHandler myTimer.Elapsed, New System.Timers.ElapsedEventHandler(AddressOf Me.myTimer_Elapsed)
        myTimer.Start()

        ' Go ahead and send timer event
        '  SendEventAR()

    End Sub
    ' ----------  ARPATECH  ----------
    ' sub routine that works on stop event
    ' ----------  ARPATECH  ----------
    Protected Sub OnStop()
        'EventLog.WriteEntry("GS Support Service stopped")
        myTimer.Enabled = False
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine that works on pause event
    ' ----------  ARPATECH  ----------
    Protected Sub OnPause()
        'EventLog.WriteEntry("GS Support Service paused")
        myTimer.Enabled = False
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine that works on continue event
    ' ----------  ARPATECH  ----------
    Protected Sub OnContinue()
        'EventLog.WriteEntry("GS Support Service continued")
        myTimer.Enabled = True
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for writing in log files
    ' AR field with value and name
    ' ----------  ARPATECH  ----------
    Private Sub LogARFields(ByVal m As System.Messaging.Message)
        Dim ar As New ActionRequest()
        Dim i As Integer
        Dim o As Object
        Try
            o = m.Body
            ar = CType(m.Body, GSSupport.ActionRequest)

            For i = 0 To ar.Fields.Count - 1
                Dim f As New Field()
                If Not IsNothing(ar.Fields.GetByIndex(i)) Then
                    f = ar.Fields.GetByIndex(i)
                    EventLog.WriteEntry("AR Log", CStr(ar.Fields.GetKey(i)) & ", " & f.FieldName & ", " & f.FieldTextValue & ", " & f.FieldDisplayValue)
                End If
            Next

        Catch e As Exception
            EventLog.WriteEntry("AR Log", "Error logging ar fields:" & e.Message)
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for listening ARs
    ' ----------  ARPATECH  ----------
    Private Sub RestartListener()
        ' Listen for incoming action requests...
        Try
            Dim mq As MessageQueue = New MessageQueue(GSSupportManager.GetQHost() & IncomingARQ)
            mq.DefaultPropertiesToSend.AppSpecific = 100
            mq.Formatter = New BinaryMessageFormatter()

            AddHandler mq.ReceiveCompleted, New ReceiveCompletedEventHandler(AddressOf OnReceiveCompleted)
            mq.BeginReceive()

        Catch e As Exception
            EventLog.WriteEntry("AR Listener", "Error restarting support listner:" & e.Message)
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for listening workflow
    ' ----------  ARPATECH  ----------
    Private Sub RestartWorkflowListener()
        ' Listen for incoming workflow additions/modifications...
        Try
            Dim mq As MessageQueue = New MessageQueue(GSSupportManager.GetQHost() & IncomingWorkflowQ)

            mq.Formatter = New BinaryMessageFormatter()

            AddHandler mq.ReceiveCompleted, New ReceiveCompletedEventHandler(AddressOf OnReceiveWorkflowCompleted)
            mq.BeginReceive()
            '   EventLog.WriteEntry("Workflow Listener", "Listening to IncomingWorkflowQ")
        Catch e As Exception
            EventLog.WriteEntry("Workflow Listener", "Error restarting workflow listner:" & e.Message)
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for listening SLAs
    ' ----------  ARPATECH  ----------
    Private Sub RestartSLAListener()
        ' Listen for incoming SLA additions/modifications...
        Try
            Dim mq As MessageQueue = New MessageQueue(GSSupportManager.GetQHost() & IncomingSLAQ)

            mq.Formatter = New BinaryMessageFormatter()

            AddHandler mq.ReceiveCompleted, New ReceiveCompletedEventHandler(AddressOf OnReceiveSLACompleted)
            mq.BeginReceive()

        Catch e As Exception
            EventLog.WriteEntry("Workflow Listener", "Error restarting workflow listner:" & e.Message)
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' Function for loading Date Fields
    ' from DB
    ' ----------  ARPATECH  ----------
    Function GetDateFields() As ArrayList

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim datecount As Integer = 0

        Conn.Open()

        Dim Cmd As New SqlCommand()
        m_dateFields.Clear()
        Try

            Dim dr As SqlDataReader
            Cmd.CommandText = "select fieldID, IsNull(fieldname, '') as fieldname, IsNull(vbConst, 0) as vbconst from tblFields where isActive = 1 and vbConst > 0 and fieldDataTypeID = " & vbDate & " and fieldid in (select fieldid from tblTimeZones)"
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                'If Not m_dateFields.ContainsKey(dr.GetInt32(dr.GetOrdinal("vbconst"))) Then
                Dim f As Field = New Field()
                f.FieldID = dr.GetInt32(dr.GetOrdinal("fieldID"))
                f.FieldName = dr.GetString(dr.GetOrdinal("fieldName"))
                f.VBConst = dr.GetInt32(dr.GetOrdinal("vbconst"))
                m_dateFields.Add(f)

                datecount = datecount + 1
                ' End If
            End While
            dr.Close()
        Catch e As Exception
            'EventLog.WriteEntry("Failed to Load Date Fields:" & e.Message)
        Finally
            Conn.Close()
            Cmd = Nothing
            Conn = Nothing
        End Try
    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine that works on start
    ' and performs some besic operations
    ' and creates new threads as per requirement
    ' ----------  ARPATECH  ----------
    Public Function GetDataTable(ByVal PassSql As String) As DataSet

        Dim ds As DataSet = New DataSet()
        Dim dt As DataTable = New DataTable()
        Dim sqlConn As SqlConnection = New SqlConnection(GSSupportManager.GetDBString())
        sqlConn.Open()
        Dim SqlDataAdaptertblOrders As SqlDataAdapter = New SqlDataAdapter(PassSql, sqlConn)
        Try
            SqlDataAdaptertblOrders.Fill(ds, "dt")
        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to Load while trying to process " & PassSql & e.Message)
            ds.Clear()
            sqlConn.Close()
            sqlConn = Nothing
        End Try
        Return ds

    End Function
    ' ----------  ARPATECH  ----------
    ' this function takes argument of
    ' customer last name and Parent Company ID
    ' and returns customer contact ID
    ' ----------  ARPATECH  ----------
    Public Function GetCustomerContactId(ByVal CustomerContact As String, ByVal ParentCompanyID As Integer) As Integer
        Dim CustomerContactID
        If CustomerContact <> "" Then
            Dim LastName As New String("")
            Dim LastPosition = InStr(CustomerContact, " ")
            If LastPosition > 1 Then
                LastName = Mid(CustomerContact, 1, LastPosition)
                'MsgBox("FirstName1 : " & FirstName)
            Else
                LastName = CustomerContact
            End If
            CustomerContactID = GSSupportManager.GetSingleDBValueInt("Select ContactID as ContactID from tblContact where CompanyID = " & ParentCompanyID & _
                " and ContactID in ( Select max(contactId) from tblContact where upper(lastname) like '" & UCase(Trim(LastName)) & "%')")
        End If
        Return CustomerContactID
    End Function
    ' ----------  ARPATECH  ----------
    ' this function returns the priority ID
    ' for the given parent company
    ' ----------  ARPATECH  ----------
    Public Function GetPriorityID(ByVal PriorityIntValue As Integer, ByVal Priority As String, ByVal ParentCompanyID As Integer) As Integer
        Dim PriorityID
        If Not IsNothing(PriorityIntValue) AndAlso IsNumeric(PriorityIntValue) AndAlso CInt(PriorityIntValue) > 0 And Not IsNothing(Priority) AndAlso (Priority <> "") AndAlso (ParentCompanyID > 0) Then
            PriorityID = GSSupportManager.GetSingleDBValueInt("Select priorityID from tblPriority where Companyid = " & ParentCompanyID & _
                                " And PriorityIntValue = " & PriorityIntValue & " and upper(priorityname) = '" & UCase(Priority) & "' and isactive=1")
        End If
        If (PriorityID < 1) And Not IsNothing(Priority) AndAlso (Priority <> "") And (ParentCompanyID > 0) Then
            PriorityID = GSSupportManager.GetSingleDBValueInt("select priorityid from tblPriority where companyid = " & ParentCompanyID & _
                                   " and upper(priorityname) = '" & UCase(Priority) & "' and isactive=1")
        End If
        If (PriorityID < 1) And Not IsNothing(PriorityIntValue) AndAlso IsNumeric(PriorityIntValue) AndAlso CInt(PriorityIntValue) > 0 AndAlso (ParentCompanyID > 0) Then
            PriorityID = GSSupportManager.GetSingleDBValueInt("Select priorityID from tblPriority where Companyid = " & ParentCompanyID & _
                                "  and isactive=1 And PriorityIntValue = " & PriorityIntValue)
        End If
        ' If I don't have a valid priority ID try to get default for this company
        If PriorityID < 1 Then
            PriorityID = GSSupportManager.GetSingleDBValueInt("select min(priorityid) from tblPriority where isActive=1 and companyid= " & ParentCompanyID & _
            " and priorityintvalue = (select min(prioritydefaultint) from tblPriority where companyid=" & ParentCompanyID & ")")
        End If


        ' EventLog.WriteEntry("priority Id is : " & PriorityID)
        Return PriorityID
    End Function
    ' ----------  ARPATECH  ----------
    ' this function returns ticket status of 
    ' any provided company
    ' ----------  ARPATECH  ----------
    Public Function GetStatusId(ByVal StatusCode As String, ByVal StatusText As String, ByVal ParentCompanyId As Integer, ByVal arTypeID As Integer) As Integer
        Dim StatusID As Integer
        StatusID = 0
        If ParentCompanyId > 0 Then
            If (arTypeID > 0) AndAlso Not IsNothing(StatusCode) AndAlso Len(StatusCode) > 0 Then
                StatusID = GSSupportManager.GetSingleDBValueInt("select ticketstatusid from tblXrefTicketStatus where " & _
                                                " upper(xrefstatuscode) ='" & Trim(UCase(StatusCode)) & "' and companyid=" & ParentCompanyId & " and artypeid=" & arTypeID & " and isactive=1")
            End If
            ' EventLog.WriteEntry("StatusID " & StatusID)
            If StatusID < 1 AndAlso (arTypeID > 0) AndAlso Not IsNothing(StatusText) AndAlso Len(StatusText) > 0 Then
                StatusID = GSSupportManager.GetSingleDBValueInt("select ticketstatusid from tblXrefTicketStatus where " & _
                     " upper(xrefstatusname) ='" & UCase(StatusText) & "' and companyid=" & ParentCompanyId & " and arTypeID=" & arTypeID & " and isactive=1")

            End If
            If StatusID < 1 AndAlso Not IsNothing(StatusText) AndAlso Len(StatusText) > 0 AndAlso Len(StatusCode) > 0 Then
                StatusID = GSSupportManager.GetSingleDBValueInt("select ticketstatusid from tblXrefTicketStatus where " & _
                     " upper(xrefstatusname) ='" & UCase(StatusText) & "' and upper(xrefstatuscode) ='" & UCase(StatusCode) & "' and companyid=" & ParentCompanyId & "  and isactive=1")
            End If

            If StatusID < 1 AndAlso Len(StatusCode) > 0 Then
                StatusID = GSSupportManager.GetSingleDBValueInt("select ticketstatusid from tblXrefTicketStatus where " & _
                     " upper(xrefstatuscode) ='" & UCase(StatusCode) & "' and companyid=" & ParentCompanyId & "  and isactive=1")
            End If
            If StatusID < 1 AndAlso Not IsNothing(StatusText) AndAlso Len(StatusText) > 0 Then
                StatusID = GSSupportManager.GetSingleDBValueInt("select ticketstatusid from tblXrefTicketStatus where " & _
                     " upper(xrefstatusname) ='" & UCase(StatusText) & "' and upper(xrefstatuscode) ='" & UCase(StatusCode) & "' and companyid=" & ParentCompanyId & "  and isactive=1")
            End If
            If StatusID < 1 Then
                StatusID = GSSupportManager.GetSingleDBValueInt("select tktstatusid from tblTicketStatus where " & _
               " upper(tktstatusname) ='" & UCase(StatusText) & "' and companyid=" & ParentCompanyId & "  and isactive=1")
            End If
        End If
        'If it is WorldSpan, and you still don't have the statusID, Pick NEW
        If StatusID < 1 And ParentCompanyId = 1186 And (arTypeID > 0) Then
            EventLog.WriteEntry("Application", "I am inside worldspan specific StatusID logic")
            StatusID = GSSupportManager.GetSingleDBValueInt("select ticketstatusid from tblXrefTicketStatus where " & _
           " upper(XREFStatusCode) ='NEW' and companyid=" & ParentCompanyId & " and artypeid= " & arTypeID & "   and isactive=1")
        End If

        Return StatusID
    End Function

    ' ----------  ARPATECH  ----------
    ' this function returns Server Date/Time
    ' by converting client TZ in Server TZ
    ' ----------  ARPATECH  ----------
    Public Function GetTimeZone(ByVal CustAddressID As Integer, ByVal timeZoneId As Integer) As Date

        Dim tzvbconst As Integer = GSSupportManager.GetTimeZoneVBConst(ServerTimeZone)
        If timeZoneId > 0 Then
            tzvbconst = GSSupportManager.GetTimeZoneVBConst(timeZoneId)
        End If
        'EventLog.WriteEntry("TimezoneID is " & timeZoneId)
        Dim ConvertTime As Date

        'Now get the local date time using the TimeZoneID
        Dim tz
        ' EventLog.WriteEntry("timezone is " & timezoneid)

        Dim ddate, tz1, tz2
        ddate = Now()
        tz1 = ServerTimeZone
        tz = CreateObject("GSTime.TimeConverter")
        'EventLog.WriteEntry("ConvertTime is " & ConvertTime & " tz1: " & tz1 & " tzvbconst: " & tzvbconst)
        ConvertTime = Me.manager.ConvertTime(ddate, tz1, tzvbconst)
        'EventLog.WriteEntry("After calling ConvertTime function, ConvertTime is " & ConvertTime)

        If ConvertTime.Year < 2000 Then
            ConvertTime = Now
        End If
        Return ConvertTime
    End Function
    ' ----------  ARPATECH  ----------
    ' this function changes GSName in DB
    ' with the provided one and returns name from DB
    ' ----------  ARPATECH  ----------
    Public Function GetCleanGSName(ByVal AssetManuf As String) As String
        If Len(AssetManuf) > 0 Then
            'EventLog.WriteEntry("I am here inside Assetmanuf : " & AssetManuf)
            Dim ConnMan As New SqlConnection(GSSupportManager.GetDBString())
            Dim CmdMan As New SqlCommand
            ConnMan.Open()
            CmdMan.Connection = ConnMan
            CmdMan.CommandText = "sp_ConvertToGSNames"
            CmdMan.CommandType = Data.CommandType.StoredProcedure
            SqlCommandBuilder.DeriveParameters(CmdMan)
            CmdMan.Parameters("@Name").Value = AssetManuf
            CmdMan.ExecuteNonQuery()
            AssetManuf = CmdMan.Parameters("@Name").Value
            'EventLog.WriteEntry("After sp_ConvertToGSNames, AssetManuf" & AssetManuf)
            ConnMan.Close()
        End If
        Return AssetManuf
    End Function

    ' ----------  ARPATECH  ----------
    ' this function changes GSModelName in DB
    ' with the provided one and returns name from DB
    ' ----------  ARPATECH  ----------
    Public Function GetCleanGSModelName(ByVal AssetModel As String, ByVal ClientID As String, ByVal AssetManuf As String) As String

        Dim ConnManMod As New SqlConnection(GSSupportManager.GetDBString())
        Dim CmdManMod As New SqlCommand
        ConnManMod.Open()
        CmdManMod.Connection = ConnManMod

        'EventLog.WriteEntry("I am here before calling sp_ConvertToManMod")
        CmdManMod.CommandText = "sp_ConvertToManMod"
        CmdManMod.CommandType = Data.CommandType.StoredProcedure
        SqlCommandBuilder.DeriveParameters(CmdManMod)
        CmdManMod.Parameters("@ManMod").Value = AssetModel
        CmdManMod.Parameters("@CompanyName").Value = ClientID
        CmdManMod.Parameters("@Manufacturer").Value = AssetManuf
        CmdManMod.ExecuteNonQuery()
        If Not IsNothing(CmdManMod.Parameters("@Model").Value) Then
            AssetModel = CmdManMod.Parameters("@Model").Value
            EventLog.WriteEntry("application", "model is :" & AssetModel)
        Else
            EventLog.WriteEntry("application", "model is nothing")
        End If
        ConnManMod.Close()
        Return AssetModel
    End Function

    ' ----------  ARPATECH  ----------
    ' this function returns customer Address
    ' ID from DB
    ' ----------  ARPATECH  ----------
    Public Function GetCustomerAddressID(ByVal Addressid As String, ByVal LocationNumber As String, ByVal gscustomerid As Integer, ByVal AssetSerialNum As String, ByVal AssetID As Integer) As Integer

        Dim CustAddressID As Integer = 0
        Dim ParentCompanyId As Integer

        If gscustomerid > 0 Then
            ParentCompanyId = GSSupportManager.GetSingleDBValueInt("Select  ParentCompanyId from tblCustomers where customerid=" & gscustomerid)

            'EventLog.WriteEntry("Inside getCustomerAddressId, CustomerName is: " & CustomerName)
            'EventLog.WriteEntry("Inside getCustomerAddressId, ParentCompanyId is: " & ParentCompanyId)
            ' EventLog.WriteEntry("getting CustomerAddressID using AddressID " & Addressid)
            ' We want to use Location number as a first variable to look for TimeZoneid. 
            'If Location Number is not available, use AddressID
            'If AddressId is not available, use AssetId
            'If AssetID is not available, use ISOCountryCode
            'If ISOCountryCode is not available, use Country Name

            If CustAddressID < 1 And Not IsNothing(LocationNumber) AndAlso Trim(LocationNumber) <> "" Then
                'EventLog.WriteEntry("Inside getCustomerAddressId, Using AddressId to get CustomerAddressID. AddressId: " & Addressid)
                CustAddressID = GSSupportManager.GetSingleDBValueInt("select isnull(min(CustomerAddressID), -1) as CustomerAddressID " & _
                                        " from tblXrefLocations where " & _
                                        " upper(XREFCode) = '" & UCase(LocationNumber) & "'  and Companyid = " & ParentCompanyId & " and isActive = 1")
                'EventLog.WriteEntry("Inside getCustomerAddressId, select isnull(min(CustomerAddressID), -1) as CustomerAddressID " & _
                ' " from tblXrefLocations where " & _
                '" upper(XREFCode) = '" & UCase(LocationNumber) & "'  and Companyid = " & ParentCompanyId & " and isActive = 1")

            End If

            'If you don't get customer addressid using location number use Addressid
            'If the AddressID is Available, initialize Customer's addressID with it.

            If CustAddressID < 1 And IsNumeric(Addressid) AndAlso Addressid > 0 Then
                'EventLog.WriteEntry("Inside getCustomerAddressId, Using AddressId to get CustomerAddressID. AddressId: " & Addressid)

                If Len(Trim(Addressid)) > 0 Then
                    Addressid = CInt(Addressid)
                End If
                CustAddressID = GSSupportManager.GetSingleDBValueInt("select customeraddressid from tblCustomerAddresses where customerid=" & gscustomerid & " and customeraddressid=" & Addressid & " and isActive = 1")
            End If
        End If
        'Using asset to pull the address is prohibited.


        '  If CustAddressID < 1 Then
        ' Try to get addressid from assetid
        ' If AssetID > 1 Then
        'AssetID = GSSupportManager.GetSingleDBValueInt("select assetid from gsnetp_tbl_assets where isactive=1 and  assetid=" & AssetID & _
        '" and assetsownedid in (select assetsownedid from gsnetp_tbl_assetsowned where customerid=" & gscustomerid & ") and isActive = 1 ")
        '
        '       ElseIf Len(AssetSerialNum) > 0 Then
        '          AssetID = GSSupportManager.GetSingleDBValueInt("select assetid from gsnetp_tbl_assets where serialnum = '" & AssetSerialNum & _
        '         "' and assetsownedid in (select assetsownedid from gsnetp_tbl_assetsowned where customerid=" & gscustomerid & ") and isActive = 1")
        '
        '      End If
        '       '09-09-2003 COMMENT
        'Do not pull the customer address informationg using Asset Id any more
        ' If AssetID > 0 Then
        'CustAddressID = GSSupportManager.GetSingleDBValueInt("select customeraddressid from gsnetp_tbl_AddrWithAssets where " & _
        '" addrwithassetsid in (select max(addrwithassetsid) from gsnetp_tbl_assets where assetid=" & AssetID & ")")
        'End If
        '     End If
        '    End If
        Return CustAddressID
    End Function

    ' ----------  ARPATECH  ----------
    ' this function returns Ticket ID
    ' from Customer Requirement ID
    ' ----------  ARPATECH  ----------
    Public Function GetTicketId(ByVal parentcompanyid As Integer, ByVal customerReqNum As String, ByVal PrimeContractorReqNum As String) As Integer
        Dim GSTicketId As Integer = 0
        Dim CountTicketID
        CountTicketID = 0
        ' EventLog.WriteEntry("PrimeContractorReqNum is " & PrimeContractorReqNum)
        If PrimeContractorReqNum <> "" Then
            PrimeContractorReqNum = CInt(PrimeContractorReqNum)
            '  EventLog.WriteEntry("PrimeContractorReqNum is " & PrimeContractorReqNum)
            ' First make sure this ticket is valid and for this customer           
            GSTicketId = GSSupportManager.GetSingleDBValueInt("select max(ticketid) from " & _
                                        " tblTicket join tblActionRequest on tblActionrequest.arid " & _
                                        " = tblTicket.arid and tblActionRequest.CompanyID = " & parentcompanyid & " where ticketid= '" & PrimeContractorReqNum & "'")

            ' EventLog.WriteEntry("GSTicketId is " & GSTicketId)

        End If
        ' If GSTicketId < 1 And customerReqNum <> "" Then
        'CountTicketID = GSSupportManager.GetSingleDBValueInt("select count(ticketid) from " & _
        '           " tblTicket join tblActionRequest on tblActionrequest.arid " & _
        '         " = tblTicket.arid and tblActionRequest.CompanyID = " & parentcompanyid & " where Customerticketid= '" & customerReqNum & "'")
        'If CountTicketID > 0 Then
        '   PrimeContractorReqNum = GSSupportManager.GetSingleDBValueInt("select max(ticketid) from " & _
        '                          " tblTicket join tblActionRequest on tblActionrequest.arid " & _
        '                         " = tblTicket.arid and tblActionRequest.CompanyID = " & parentcompanyid & " where Customerticketid= '" & customerReqNum & "'")
        'GSTicketId = PrimeContractorReqNum
        '
        '       End If
        '      End If
        EventLog.WriteEntry("application", "GSTicketId is " & GSTicketId)
        Return GSTicketId
    End Function

    ' ----------  ARPATECH  ----------
    ' this sub routine is for time 
    ' and runs after a specified time period
    ' and calls another sub routine
    ' ----------  ARPATECH  ----------
    Protected Sub myTimer_Elapsed(ByVal source As Object, ByVal e As ElapsedEventArgs)
        ' EventLog.WriteEntry("GS Support Service", "Support timer elapsed; sending system AR")


        Try
            ' EventLog.WriteEntry("GSSupport Service", "timer time is " & m_timeConverter.ConvertTime(Now(), 10, 13))

            '  Dim armh As New ARMessageHandler()

            '  armh.service = Me
            '   armh.TimeConverter = m_timeConverter

            '   armh.SendEventAR()
            ' EventLog.WriteEntry("Sending timer request: " & Err.Description)
            SendEventAR()
            ' EventLog.WriteEntry("back from send event AR")
            'GC.Collect(GC.GetGeneration(Me))
            'GC.Collect(GC.GetGeneration(Me.manager))
            ' GC.Collect(GC.GetGeneration(Me.m_gsListener))



            ' Dim MyThread As New System.Threading.Thread(AddressOf Me.SendEventAR)
            '  Dim MyThread As New System.Threading.Thread(AddressOf armh.SendEventAR)
            ' MyThread.Start()

        Catch ex As Exception
            EventLog.WriteEntry("GS Support Service", "timer_elapsed error:" & ex.Message)
        Finally

        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine reloads all managerial
    ' routines and functions
    ' ----------  ARPATECH  ----------
    Public Sub ReloadManager()
        EventLog.WriteEntry("GS Support Service", "Reloading manager...")

        manager = New GSSupportManager()
        m_gsListener = New GSEventListener()
        manager.GSListener = m_gsListener
        m_timeConverter = New GSTime.TimeConverter()
        GC.KeepAlive(m_timeConverter)
    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine is a system generated 
    ' timer event for sending Action Request
    ' ----------  ARPATECH  ----------
    Sub SendEventAR()
        Dim ar As New ActionRequest()

        Dim args As New GSTimerEventArgs()
        Try

            If IsNothing(manager) OrElse IsNothing(m_gsListener) OrElse IsNothing(m_timeConverter) Then
                EventLog.WriteEntry("Application", "Restarting Manager")
                ReloadManager()
            End If

            ar.GenerateTicket = False
            ar.IsChild = False

            ar.GSListener = m_gsListener

            ar.CategoryName = "Time"

            ar.Manager = manager

            ar.Qualify = False

            ar.Summary = "System generated timer event."

            ar.RequestDate = Now()

            ar.RequestDateLocal = Now()

            ar.SubmitDate = Now()

            ar.RequestDateTimeZoneVBConst = ServerTimeZone

            ar.ARUpdateDB()
            If ar.ActionRequestID > 0 Then

                AddDateFields(ar)

                args.ActionRequest = ar

                '  GSSupportManager.LogARDetails(ar)
                'EventLog.WriteEntry("About to raise timer event")
                m_gsListener.RaiseGSTimerEvent(ar, args)
                'EventLog.WriteEntry("Timer event raised.")

            Else

                EventLog.WriteEntry("GS Support Service", "Unable to create AR timer request.")

            End If

        Catch ex As Exception
            EventLog.WriteEntry("GS Support Service", "Error raising timer event:" & ex.Message)
        Finally

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine adds all date fields 
    ' of AR to memory variables
    ' ----------  ARPATECH  ----------
    Sub AddDateFields(ByRef ar As ActionRequest)
        Try


            Dim i As Integer

            For i = 0 To m_dateFields.Count - 1
                'EventLog.WriteEntry("Application", " field is " & m_dateFields.GetKey(i))
                If IsNothing(m_timeConverter) Then
                    EventLog.WriteEntry("Application", "TimeConverter Is Nothing")
                    m_timeConverter = New GSTime.TimeConverter()
                    '  Else
                    ' EventLog.WriteEntry("Application", "Is Not Nothing")
                    '     EventLog.WriteEntry("Application", "Instance is " & m_timeConverter.GetType.)
                    '  EventLog.WriteEntry("Application", m_timeConverter.ConvertTime(Now(), 10, 13))
                End If
                '     EventLog.WriteEntry("Application", m_dateFields.GetKey(i) & "-" & Now() & "-" & m_dateFields.GetByIndex(i))
                Dim f As Field = New Field()
                f = m_dateFields.Item(i)
                Dim tmpDate As Date = m_timeConverter.ConvertTime(Now(), ServerTimeZone, f.VBConst)
                f.FieldTextValue = tmpDate.ToString
                f.FieldDisplayValue = tmpDate.ToString
                f.FieldNativeValue = tmpDate

                '   ar.AddField(m_dateFields.GetKey(i), m_timeConverter.ConvertTime(Now(), ServerTimeZone, m_dateFields.GetByIndex(i)))
                ar.AddField(f)

                '   EventLog.WriteEntry("Application", "B")
            Next


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error adding date fields: " & e.Message)
            ReloadManager()
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine adds a asset to DB
    ' ----------  ARPATECH  ----------
    Public Function AddAsset(ByVal AssetManuf As String, ByVal AssetModel As String, ByVal AssetType As String, ByVal ParentCompanyID As Integer, ByVal AssetSerialNum As String, ByVal CustAddressID As Integer) As Integer
        Dim ConnNew As New SqlConnection(GSSupportManager.GetDBString())
        Dim Cmd As New SqlCommand()
        Dim gscustomerid
        Dim AssetID
        If ParentCompanyID > 0 Then
            gscustomerid = GSSupportManager.GetSingleDBValueInt("select customerid from tblCustomers where parentcompanyid=" & ParentCompanyID)
        End If
        If Len(AssetSerialNum) > 0 AndAlso CustAddressID > 0 Then
            ' Try to get part category
            Dim partcategoryname As String
            Dim partcategoryID As Integer
            If Len(AssetType) > 0 Then
                partcategoryID = GSSupportManager.GetSingleDBValueInt("select partcategoryid from " & _
                " tblxrefpartcategories where companyid = " & ParentCompanyID & " and upper(xrefpartcategoryname) " & _
                " = '" & UCase(AssetType) & "'")
                If partcategoryID > 0 Then
                    partcategoryname = GSSupportManager.GetSingleDBValueString("select partcategoryname from tblPartCategories where partcategoryid=" & partcategoryID)
                Else
                    ' Otherwise just use OTHER
                    partcategoryID = GSSupportManager.GetSingleDBValueInt("select partcategoryid from tblPartcategories where upper(partcategoryname) = 'OTHER' " & _
                    " and companyid in (select min(parentcompanyid) from tblParentCompanies where parentcompanyname='GlobalServe')")
                    partcategoryname = "Other"
                End If
            End If

            ' Try to get manufacturerid

            Dim mfgid As Integer = 0
            Dim birthtypeid As Integer = 0
            birthtypeid = GSSupportManager.GetSingleDBValueInt("select birthtypeid from tblAssetBirthTypes where upper(birthtypename)='ECONNECTION'")

            If Len(AssetManuf) > 0 Then
                Cmd.CommandText = "sp_ConvertToGSNames"
                Cmd.CommandType = Data.CommandType.StoredProcedure
                SqlCommandBuilder.DeriveParameters(Cmd)
                Cmd.Parameters("@Name").Value = AssetManuf
                Cmd.ExecuteNonQuery()
                AssetManuf = Cmd.Parameters("@Name").Value
                mfgid = GSSupportManager.GetSingleDBValueInt("select manufacturerid from tblManufacturers where upper(ManufacturerName)='" & UCase(AssetManuf) & "'")

            End If

            Cmd.CommandText = "sp_AddExistingAsset"
            Cmd.CommandType = Data.CommandType.StoredProcedure
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@customerid").Value = gscustomerid
            Cmd.Parameters("@customeraddressid").Value = CustAddressID
            If mfgid > 0 Then
                Cmd.Parameters("@mfgid").Value = mfgid
            End If

            Cmd.Parameters("@mfgname").Value = AssetManuf
            ' Cmd2.Parameters("@mfgsku").Value = 
            Cmd.Parameters("@desc").Value = AssetModel
            Cmd.Parameters("@assetmodel").Value = AssetModel

            Cmd.Parameters("@partcategoryid").Value = partcategoryID
            Cmd.Parameters("@partcategoryname").Value = partcategoryname

            Cmd.Parameters("@serial").Value = AssetSerialNum
            Cmd.Parameters("@assettype").Value = "P"
            '    Cmd2.Parameters("@assetstatusid").Value = inservice
            '    Cmd2.Parameters("@assetstatusname").Value = "In Service"
            Cmd.Parameters("@userid").Value = m_casID
            Cmd.Parameters("@birthtypeid").Value = birthtypeid
            Cmd.Parameters("@assetid").Value = AssetID
            'Cmd2.Parameters("@ticketid").Value = AssetID
            Cmd.ExecuteNonQuery()
            AssetID = Cmd.Parameters("@assetid").Value

            'Else
            '    eBody = "ERROR: Serial Number provided doesn't exist and service not set to add new assets." & vbCrLf & eBody

            'tryNew = False
            Return AssetID
            ConnNew.Close()
            Cmd = Nothing
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' this function deserialize the AR fields
    ' and returns deserialized AR's
    ' ----------  ARPATECH  ----------
    Public Function DeserializeAR(ByVal xml As String) As ActionRequest

        Dim serializer As New XmlSerializer(GetType(GSSupport.ActionRequest))

        Dim ms As MemoryStream = New MemoryStream()

        Dim sr As StringReader = New StringReader(xml)
        Dim reader As New XmlTextReader(sr)

        ' Declare an object variable of the type to be deserialized.
        Dim ar As ActionRequest
        Try

            ' Use the Deserialize method to restore the object's state.

            ar = CType(serializer.Deserialize(reader), ActionRequest)

            ar.DeserializeFields()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error deserializing AR: " & xml & ", " & e.Message & "," & e.InnerException.Message)
            ' & "," & e.InnerException.InnerException.Message & "," & e.InnerException.InnerException.InnerException.Message & "," & e.InnerException.InnerException.InnerException.InnerException.Message & ",")
        End Try

        Return ar


    End Function

    ' ----------  ARPATECH  ----------
    ' this function deserialize the AR fields
    ' and returns deserialized AR's
    ' ----------  ARPATECH  ----------
    Public Sub HandleAR(ByVal msg As MSMQMessage)
        Try
            Dim label As String = msg.Label
            Dim xml As String = CStr(msg.Body)

            '     Dim m2 As New System.Messaging.Message()

            '   Dim bw As New BinaryMessageFormatter()
            '    EventLog.WriteEntry("Application", "In handle AR with " & label)
            ' EventLog.WriteEntry("Application", "In handle AR with xml " & CStr(xml))
            Dim m2 As New MSMQMessage()


            Dim qsent As Boolean
            Dim qname As String
            ' EventLog.WriteEntry("Application", "getting AR from xml")
            Dim ar As ActionRequest

            ' ar = CType(m.Body, GSSupport.ActionRequest)

            ar = DeserializeAR(xml)

            ' EventLog.WriteEntry("Application", "Created AR")

            ar.GSListener = m_gsListener
            ar.Manager = manager
            qname = ARPostProcQ

            ' If it's a completed action fire action complete event.
            If InStr(label, ActionCompleteString) > 0 Then

                '  EventLog.WriteEntry("IS a Completed Action")
                Dim am As New ActionMonitor()
                ' EventLog.WriteEntry("Getting AM")
                am = ar.CompletedAction
                ' EventLog.WriteEntry("Setting Manager")
                am.Manager = Me.manager
                ' EventLog.WriteEntry("Setting AR")
                am.ActionRequest = ar
                ' EventLog.WriteEntry("Firing Action Event")
                ' EventLog.WriteEntry("Application", "About to fire action event")
                Dim MyThread As New System.Threading.Thread(AddressOf am.FireActionEvent)
                MyThread.Start()
                ' am.FireActionEvent()
                ' EventLog.WriteEntry("Application", "Fired action event")



            Else

                AddDateFields(ar)
                ' If it needs to be qualified fire an Action Request Event
                If InStr(label, QualifyString) > 0 Then

                    Dim ev As New ActionRequestEventArgs()
                    ev.ActionRequest = ar

                    ' Otherwise add all times for all time zones


                    If IsNothing(ar.RequestDateLocal) Then

                        ConvertToServerTime(ar)

                    End If
                    'EventLog.WriteEntry("Application", "About to raise ar event")

                    m_gsListener.RaiseActionRequestEvent(ar, ev)
                    '  EventLog.WriteEntry("Application", "Done raising ar event")


                Else

                    '    EventLog.WriteEntry("Application", "About to execute action")
                    '  ExecuteAction(ar)
                    Dim aa As New ActionAgent()
                    aa.ar = ar
                    Dim MyThread As New System.Threading.Thread(AddressOf aa.ExecuteAction)
                    MyThread.Start()
                    '     EventLog.WriteEntry("Application", "Action Executed")

                End If




            End If

            qsent = SendToQ(qname, msg)

            GSSupportManager.LogARDetails(ar)


        Catch ex As Exception
            EventLog.WriteEntry("AR Listener Error", "Error forwarding action request:" & ex.Message)
        Finally

        End Try
    End Sub
    ' ----------  ARPATECH  ----------
    ' this function converts client TZ to
    ' Server TZ and updates DB accordingly
    ' ----------  ARPATECH  ----------
    Sub ConvertToServerTime(ByRef ar As ActionRequest)
        Dim tz1 As Integer
        Dim tz2 As Integer
        tz1 = manager.GetTimeZoneVBConst(ar.RequestDateTimeZoneID)
        ar.RequestDate = manager.ConvertTime(ar.RequestDateLocal, tz1, ServerTimeZone)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader

            sql = "update tblActionRequest set requestdate='" & ar.RequestDate & "' where arid=" & ar.ActionRequestID

            '    EventLog.WriteEntry("Application", "sql is " & sql)
            Cmd.CommandText = sql
            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error gettimg time zone: " & e.Message)


        Finally
            Conn.Close()

        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' this function executes any give AR
    ' ----------  ARPATECH  ----------
    Sub ExecuteAction(ByVal ar As ActionRequest)

        Dim am As New ActionMonitor()
        Try
            '  EventLog.WriteEntry("Service executing action")

            am.ActionRequest = ar
            am.Execute()

            Dim m As MSMQMessage = am.CreateMQMessage()


            If Not SendToQ(ActionPostProcQ, m) Then
                EventLog.WriteEntry("Application", "Unable to send processed action to action postproc q")
            End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error executing action:" & e.Message)
        Finally
            '   am = Nothing
            '' GC.Collect(GC.GetGeneration(am))
            ' Thread.CurrentThread.Abort()

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' this function creates an order
    ' on basis of XML sent
    ' ----------  ARPATECH  ----------
    Public Function CreateOrderEventAR(ByVal xml As String)
        Dim ar As New ActionRequest()
        Dim SessionTrackingNumber As Integer = 0
        Dim OrderTrackingNumber As Integer = 0
        Dim DestinationTrackingNumber As Integer = 0
        Dim OrderItemNumber As Integer = 0
        Dim BuyerUserID As Integer
        Dim ChangeAccepted As Boolean = False
        Dim ChangeShipped As Boolean = False
        Dim OrderAcceptDate As New Date()
        Dim ChangeItemShipped As Boolean = False
        Dim ChangeETA As Boolean = False
        Dim ETA As New Date()
        Dim OrderShipDate As New Date()
        Dim ItemShipDate As New Date()
        Dim sSQL As New String("")
        Dim testdate = New Date(2000, 1, 1)

        Try

            Dim doc As New XmlDocument()

            '  EventLog.WriteEntry("XML IS " & xml)
            doc.LoadXml(xml)

            Dim elemList As XmlNodeList

            elemList = doc.GetElementsByTagName("SessionTrackingNumber")
            SessionTrackingNumber = CInt(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("OrderItemNumber")
            OrderItemNumber = CInt(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("OrderTrackingNumber")
            OrderTrackingNumber = CInt(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("DestinationTrackingNumber")
            DestinationTrackingNumber = CInt(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("ChangeShipped")
            ChangeShipped = CBool(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("ChangeItemShipped")
            ChangeItemShipped = CBool(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("ChangeAccepted")
            ChangeAccepted = CBool(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("ChangeETA")
            ChangeETA = CBool(elemList(0).InnerText)



            elemList = doc.GetElementsByTagName("ETA")
            ETA = CDate(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("OrderShipDate")
            OrderShipDate = CDate(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("ItemShipDate")
            ItemShipDate = CDate(elemList(0).InnerText)

            elemList = doc.GetElementsByTagName("OrderAcceptDate")
            OrderAcceptDate = CDate(elemList(0).InnerText)

            If SessionTrackingNumber < 1 Or OrderTrackingNumber < 1 Or DestinationTrackingNumber < 1 Then
                Console.WriteLine("Invalid order number.")
                EventLog.WriteEntry("Application", "Unable to get order ID from OrderEvent XML")
                Return ar
            End If

            Dim objActionMonitor = New GSSupport.ActionMonitor()
            Dim objActionUpdateOrder = New GSSupport.ActionUpdateOrder()



            ' Set Action Request Properties
            ar.Qualify = False
            ar.CategoryName = "Order Change"
            ar.SubmitDate = Now()
            ar.RequestDate = Now()
            ar.RequestDateLocal = Now()
            ar.RequestDateTimeZoneVBConst = ServerTimeZone
            ar.SubmitUserID = 185

            ar.AddFieldByDisplay("Session Tracking Number", CStr(SessionTrackingNumber))
            ar.AddFieldByDisplay("Order Tracking Number", CStr(OrderTrackingNumber))
            ar.AddFieldByDisplay("Destination Tracking Number", CStr(DestinationTrackingNumber))

            If OrderAcceptDate > testdate Then
                ar.AddFieldByDisplay("Order Accept Date", MonthName(OrderAcceptDate.Month) & " " & OrderAcceptDate.Day & ", " & OrderAcceptDate.Year)
            End If

            If ETA > testdate Then
                ar.AddFieldByDisplay("Estimated Ship Date", MonthName(ETA.Month) & " " & ETA.Day & ", " & ETA.Year)
            End If

            If OrderShipDate > testdate Then
                ar.AddFieldByDisplay("Order Ship Date", MonthName(OrderShipDate.Month) & " " & OrderShipDate.Day & ", " & OrderShipDate.Year)
            End If

            If ItemShipDate > testdate Then
                '    ar.AddField("itemshipdate", MonthName(ItemShipDate.Month) & ", " & ItemShipDate.Day & ", " & ItemShipDate.Year)
            End If


            ' Get order properties from DB
            sSQL = "select c.email, cur.currencySymbol, ordercreatedbyuserid, u2.username as ordercreatedbyusername, c2.email as ordercreatedbyuseremail, tblOrders.customerid, orderdate, co.countryname, tblCustomers.parentcompanyid, parentcompanyname, orderedbyuserid, orderdestinationcountryid, " & _
            " orderdestinationcurrencyid, tblOrders.orderstatusid, os.orderstatusname, orderpo, totalordervalue, totalordervalueusd, u.username " & _
            " from tblOrders, tblCustomers, tblContact c, tblCurrencies cur, tblCountries co, tblUsers u, tblOrderStatus os, tblParentCompanies, tblUsers u2, tblContact c2 where sessiontrackingnumber = " & SessionTrackingNumber & " and " & _
            " ordertrackingnumber = " & OrderTrackingNumber & " and tblOrders.CustomerID = tblCustomers.CustomerID and u2.userid =* tblOrders.ordercreatedbyuserid " & _
            " and c2.userid =* tblOrders.ordercreatedbyuserid and c.userid =* orderedbyuserid and cur.currencyid = orderdestinationcurrencyid and co.countryid = tblOrders.orderdestinationcountryid " & _
            " and u.userid = orderedbyuserid and os.orderstatusid = tblOrders.orderstatusid and tblCustomers.parentcompanyid = tblParentCompanies.parentcompanyid"

            '        EventLog.WriteEntry(sSQL)

            ' Add contact email field
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Conn.Open()

            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            Cmd.CommandText = sSQL
            dr = Cmd.ExecuteReader

            While dr.Read()
                '  ar.AddFieldByDisplay("Order Status", dr.GetInt32(dr.GetOrdinal("orderstatusid")), dr.GetString(dr.GetOrdinal("orderstatusname")))

                ar.AddFieldByDisplay("Ordered By User", dr.GetInt32(dr.GetOrdinal("orderedbyuserid")), dr.GetString(dr.GetOrdinal("username")))
                ar.AddFieldByDisplay("Ordered By User Email", dr.GetString(dr.GetOrdinal("email")))

                If Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyuseremail")) Then
                    ar.AddFieldByDisplay("Email of Order Creator", dr.GetString(dr.GetOrdinal("ordercreatedbyuseremail")))
                Else
                    ar.AddFieldByDisplay("Email of Order Creator", dr.GetString(dr.GetOrdinal("email")))

                End If

                If Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyusername")) Then
                    ar.AddFieldByDisplay("Order Created By User", dr.GetInt32(dr.GetOrdinal("ordercreatedbyuserid")), dr.GetString(dr.GetOrdinal("ordercreatedbyusername")))
                Else
                    ar.AddFieldByDisplay("Order Created By User", dr.GetInt32(dr.GetOrdinal("orderedbyuserid")), dr.GetString(dr.GetOrdinal("username")))


                End If


                '    ar.AddField("customerid", dr.GetInt32(dr.GetOrdinal("customerid")), dr.GetString(dr.GetOrdinal("parentcompanyname")))
                '  ar.AddField("companyid", dr.GetInt32(dr.GetOrdinal("parentcompanyid")), dr.GetString(dr.GetOrdinal("parentcompanyname")))
                ar.CompanyID = dr.GetInt32(dr.GetOrdinal("parentcompanyid"))
                ar.AddFieldByDisplay("Order Date", dr.GetDateTime(dr.GetOrdinal("orderdate")))
                ar.AddFieldByDisplay("Order Destination Country", dr.GetInt32(dr.GetOrdinal("orderdestinationcountryid")), dr.GetString(dr.GetOrdinal("countryname")))
                ar.AddFieldByDisplay("Order Destination Currency", dr.GetInt32(dr.GetOrdinal("orderdestinationcurrencyid")), dr.GetString(dr.GetOrdinal("currencySymbol")))
                ar.AddFieldByDisplay("Total Order Value", dr.GetDecimal(dr.GetOrdinal("totalordervalue")))
                ar.AddFieldByDisplay("Total Order Value USD", dr.GetDecimal(dr.GetOrdinal("totalordervalueusd")))

                Dim textval As String

                If ChangeAccepted Then
                    ar.AddFieldByDisplay("Order Accept Date Changed", "true", "true")

                    textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                    " tblPreference where upper(preferenceName)='NOTIFYACCEPTED')")
                    If textval = "1" Or textval.ToUpper = "TRUE" Then
                        ar.AddFieldByDisplay("Notify Buyer - Order Acceptance", "true", "true")
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyusername")) Then
                        textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
    " tblPreference where upper(preferenceName)='NOTIFYACCEPTED')")
                        If textval = "1" Or textval.ToUpper = "TRUE" Then
                            ar.AddFieldByDisplay("Notify Order Creator - Order Acceptance", "true", "true")
                        End If
                        '    Else
                        '       ar.AddField("ordercreatornotifyaccepted", "true", "true")

                    End If




                End If

                If ChangeItemShipped Then
                    '  ar.AddField("changeorderitemship", "true", "true")

                    textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                    " tblPreference where upper(preferenceName)='NOTIFYITEMSHIP')")
                    If textval = "1" Or textval.ToUpper = "TRUE" Then
                        '       ar.AddField("buyernotifyitemship", "true", "true")
                    End If

                    textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
        " tblPreference where upper(preferenceName)='NOTIFYITEMSHIP')")
                    If textval = "1" Or textval.ToUpper = "TRUE" Then
                        ' ar.AddField("ordercreatornotifyitemship", "true", "true")
                    End If

                    '    If ItemShipDate > testdate Then
                    '        ar.AddField("itemshipdate", ItemShipDate.ToString)
                    '   End If

                End If

                If ChangeShipped Then

                    ar.AddFieldByDisplay("Order Ship Date Changed", "true", "true")

                    textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                    " tblPreference where upper(preferenceName)='NOTIFYORDERSHIP')")

                    If textval = "1" Or textval.ToUpper = "TRUE" Then
                        ar.AddFieldByDisplay("Notify Buyer - Order Ship", "true", "true")
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyusername")) Then


                        textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
    " tblPreference where upper(preferenceName)='NOTIFYORDERSHIP')")

                        If textval = "1" Or textval.ToUpper = "TRUE" Then
                            ar.AddFieldByDisplay("Notify Order Creator - Order Ship", "true", "true")
                        End If
                        ' Else
                        '    ar.AddField("ordercreatornotifyordership", "true", "true")

                    End If
                End If


                '   If OrderShipDate > testdate Then
                '   ar.AddField("ordershipdate", OrderShipDate.ToString)
                ' End If





                ' Do they want to be notified if ETA changes?
                Dim notifiedETAChange As Boolean = False
                '  EventLog.WriteEntry("Change ETA is " & ChangeETA)
                If ChangeETA Then
                    ar.AddFieldByDisplay("Order ETA Changed", "true", "true")
                    textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                    " tblPreference where upper(preferenceName)='NOTIFYETACHANGE')")
                    If textval = "1" Or textval.ToUpper = "TRUE" Then
                        ar.AddFieldByDisplay("Notify Buyer - ETA Change", "true", "true")
                        notifiedETAChange = True
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr.IsDBNull(dr.GetOrdinal("ordercreatedbyusername")) Then
                        textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
    " tblPreference where upper(preferenceName)='NOTIFYETACHANGE')")

                        If textval = "1" Or textval.ToUpper = "TRUE" Then
                            ar.AddFieldByDisplay("Notify Order Creator  - ETA Change", "true", "true")
                            notifiedETAChange = True
                        End If
                        '  Else
                        '     ar.AddField("ordercreatornotifyeta", "true", "true")

                    End If


                End If
                ' See if the want to know if ETA is later than a certain date
                ' Don't do it if we've already notified them that the ETA has changed...
                '  If Not notifiedETAChange And ChangeETA Then

                '  textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr.GetInt32(dr.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                '        " tblPreference where upper(preferenceName)='NOTIFYETADAYS')")
                '  If IsNumeric(textval) AndAlso CInt(textval) > 0 Then
                '     ar.AddField("buyernotifyetadays", textval, textval)
                '      If Not IsNothing(ETA) Then
                '        Dim days = DateDiff(DateInterval.Day, Now, ETA)
                '        ar.AddField("orderetadays", textval, textval)
                '    End If
                ' End If


                '   End If





            End While

            dr.Close()

            ' Get order item information
            '     If OrderItemNumber > 0 Then

            '   sSQL = "select ccp.partdescription, mp.manufacturersku, ccp.partcategoryid, pc.partcategoryname, mp.manufacturerid, m.manufacturername " & _
            '   " from tblCustomerCountryParts ccp, tblpartcategories pc, tblmanufacturerparts mp, tblmanufacturers m, tblOrderItems oi where " & _
            '   " ccp.manufacturerpartid = mp.manufacturerpartid and mp.manufacturerid = m.manufacturerid and ccp.partcategoryid = pc.partcategoryid and " & _
            '   " ccp.customerpartid= oi.customerpartid and oi.orderitemid=" & OrderItemNumber

            '   Cmd.CommandText = sSQL
            '   dr = Cmd.ExecuteReader

            '   While dr.Read()
            '      ar.AddField("orderitemdescription", dr.GetString(dr.GetOrdinal("partdescription")))
            '      ar.AddField("orderitemsku", dr.GetString(dr.GetOrdinal("manufacturersku")))
            '       ar.AddField("orderitemcustomercategory", dr.GetInt32(dr.GetOrdinal("partCategoryid")), dr.GetString(dr.GetOrdinal("partcategoryname")))
            '       ar.AddField("orderitemmanufacturerid", dr.GetInt32(dr.GetOrdinal("manufacturerid")), dr.GetString(dr.GetOrdinal("manufacturername")))
            '   End While

            '    End If

            '    dr.Close()
            Conn.Close()

            objActionUpdateOrder.SessionTrackingNumber = SessionTrackingNumber
            objActionUpdateOrder.OrderTrackingNumber = OrderTrackingNumber
            objActionUpdateOrder.DestinationTrackingNumber = DestinationTrackingNumber
            objActionUpdateOrder.ChangeAccepted = ChangeAccepted
            objActionUpdateOrder.ChangeShipped = ChangeShipped
            objActionUpdateOrder.ChangeETA = ChangeETA
            objActionUpdateOrder.ETA = ETA
            objActionUpdateOrder.Manager = manager
            ' EventLog.WriteEntry("AR session is " & objActionUpdateOrder.SessionTrackingNumber)

            objActionMonitor.Action = objActionUpdateOrder
            ar.CompletedAction = objActionMonitor

            '   EventLog.WriteEntry("c")

            If Not ar.ARUpdateDB() Then
                'EventLog.WriteEntry("ERROR: Unable to update DB with OrderEventAR")
            End If
            '  GSSupportManager.LogARDetails(ar)


        Catch e As Exception
            'EventLog.WriteEntry("Error creating OrderEventAR: " & e.Message)

        End Try
        Return ar

    End Function

    ' ----------  ARPATECH  ----------
    ' this function creates an order
    ' on basis of XML sent
    ' ----------  ARPATECH  ----------
    Public Sub IncomingARQListen()
        Dim qinfo As New MSMQQueueInfo()
        Dim q As MSMQQueue
        Dim msg As MSMQMessage

        ' Associate the MSMQQueueInfo object with the queue.
        qinfo.PathName = GSSupportManager.GetQHost & IncomingARQ

        ' Open the queue.
        On Error GoTo ErrorHandler
        q = qinfo.Open(Access:=MQ_RECEIVE_ACCESS, _
                               ShareMode:=MQ_DENY_NONE)

        ' Read messages in the queue.
        Do While True

            msg = Nothing
            msg = q.Receive(ReceiveTimeout:=1000)
            If msg Is Nothing Then
                ' q.Close()
                ' Exit Sub
                Thread.CurrentThread.Sleep(10000)
            Else
                ' EventLog.WriteEntry("Handling OrderEvent " + msg.Label)

                Dim s As String = CStr(msg.Body)

                '  HandleAR(msg.Label, s)
                '  EventLog.WriteEntry("Application", "Start HandleAR")
                '    Dim t1 As New System.Threading.Thread(AddressOf IncomingARQListen)
                '    t1.Start()
                HandleAR(msg)
                ' EventLog.WriteEntry("Application", "End HandleAR")
                q.Reset()

            End If


        Loop

ErrorHandler:
        'EventLog.WriteEntry("Error Listening to IncomingAR Q: " + Hex(Err.Number) + " was returned." _
        '+ Chr(13) + Err.Description)
        If Not q Is Nothing And q.IsOpen2 Then
            q.Close()
        End If


    End Sub
    ' ----------  ARPATECH  ----------
    ' this sub routine receives all 
    ' order events in que
    ' ----------  ARPATECH  ----------
    Public Sub OrderEventQListen()
        Dim qinfo As New MSMQQueueInfo()
        Dim q As MSMQQueue
        Dim msg As MSMQMessage

        ' Associate the MSMQQueueInfo object with the queue.
        qinfo.PathName = GSSupportManager.GetQHost & OrderEventQ

        ' Open the queue.
        On Error GoTo ErrorHandler
        q = qinfo.Open(Access:=MQ_RECEIVE_ACCESS, _
                               ShareMode:=MQ_DENY_NONE)

        ' Read messages in the queue.
        Do While True
            msg = Nothing
            msg = q.Receive(ReceiveTimeout:=1000)
            If msg Is Nothing Then
                ' q.Close()
                ' Exit Sub
                Thread.CurrentThread.Sleep(30000)
            Else
                ' EventLog.WriteEntry("Handling OrderEvent " + msg.Label)

                Dim s As String = CStr(msg.Body)
                '   EventLog.WriteEntry("About to create OrderEvent AR")
                Dim ar As ActionRequest = CreateOrderEventAR(s)
                '   EventLog.WriteEntry("Created OrderEvent AR")
                ar.Manager = manager
                ' ar.GSListener = Me.m_gsListener
                Dim am As ActionMonitor = ar.CompletedAction

                am.Manager = manager
                '  am.GSListener = Me.m_gsListener
                am.ActionRequest = ar

                am.FireActionEvent()
                q.Reset()

            End If


        Loop

ErrorHandler:
        'EventLog.WriteEntry("Error Listening to OrderEventQ: " + Hex(Err.Number) + " was returned." _
        ' Chr(13) + Err.Description)
        If Not q Is Nothing And q.IsOpen2 Then
            q.Close()
        End If


    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine updates all 
    ' order events in queue
    ' ----------  ARPATECH  ----------
    Public Sub UpdatesQListen()
        Dim wfqinfo As New MSMQQueueInfo()
        Dim slaqinfo As New MSMQQueueInfo()
        Dim qwf, qsla As MSMQQueue
        Dim wfmsg, slamsg As MSMQMessage

        ' Associate the MSMQQueueInfo object with the queue.
        wfqinfo.PathName = GSSupportManager.GetQHost & IncomingWorkflowQ
        slaqinfo.PathName = GSSupportManager.GetQHost & IncomingSLAQ

        ' Open the queue.
        On Error GoTo ErrorHandler
        qwf = wfqinfo.Open(Access:=MQ_RECEIVE_ACCESS, _
                               ShareMode:=MQ_DENY_NONE)

        qsla = slaqinfo.Open(Access:=MQ_RECEIVE_ACCESS, _
                       ShareMode:=MQ_DENY_NONE)

        ' Read messages in the incoming workflow queue.
        Do While True
            wfmsg = Nothing
            wfmsg = qwf.Receive(ReceiveTimeout:=1000)
            If wfmsg Is Nothing Then
                Thread.CurrentThread.Sleep(10000)
            Else
                UpdateWorkflow(wfmsg.Label)
                qwf.Reset()

            End If

            slamsg = Nothing
            slamsg = qsla.Receive(ReceiveTimeout:=1000)
            If slamsg Is Nothing Then
                Thread.CurrentThread.Sleep(10000)
            Else
                UpdateSLA(slamsg.Label)
                qsla.Reset()

            End If


        Loop

ErrorHandler:
        'EventLog.WriteEntry("Error Listening to OrderEventQ: " + Hex(Err.Number) + " was returned." _
        '+ Chr(13) + Err.Description)
        If Not q Is Nothing And q.IsOpen2 Then
            q.Close()
        End If


    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine updates provided workflow 
    ' ----------  ARPATECH  ----------
    Public Sub UpdateWorkflow(ByVal label As String)
        Try
            Dim bw As New System.Messaging.BinaryMessageFormatter()

            Dim m2 As New System.Messaging.Message()


            Dim qsent As Boolean
            Dim qname As String
            Dim tmpstr As String
            Dim myWFID As New String("")
            Dim wfid As Integer


            If Mid(label, 1, 3) = "WF=" Then
                myWFID = Mid(label, 1, InStr(label, "&"))
                tmpstr = Mid(myWFID, InStr(myWFID, "=") + 1)
                tmpstr = Mid(tmpstr, 1, Len(tmpstr) - 1)
                wfid = CInt(tmpstr)
            End If

            ' EventLog.WriteEntry("Application", "wfid is " & wfid)

            Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & WorkflowQ)

            Dim menum As System.Messaging.MessageEnumerator
            menum = mqq.GetMessageEnumerator()
            While menum.MoveNext()
                If InStr(menum.Current.Label, myWFID) > 0 Then
                    menum.RemoveCurrent()
                    Exit While
                End If
            End While

            If m_Workflows.ContainsKey(wfid) Then
                Dim oldwf As Workflow = m_Workflows.Item(wfid)

                m_Workflows.Remove(wfid)

                oldwf.Dispose()


            End If

            manager.LoadWorkflow(wfid, m_Workflows)


        Catch e As Exception
            EventLog.WriteEntry("GS Support Service", "Error updating workflow:" & e.Message)
        Finally

        End Try
    End Sub
    ' ----------  ARPATECH  ----------
    ' this sub routine updates provided SLA
    ' ----------  ARPATECH  ----------
    Public Sub UpdateSLA(ByVal label As String)
        Try



            Dim qsent As Boolean
            Dim qname As String
            Dim myLabel As String
            Dim mySLAID As Integer
            Dim slastring As New String("")

            ' EventLog.WriteEntry("GSSupportSvc", "Message " & m.Label & "received.")



            If Mid(myLabel, 1, 4) = "SLA=" Then
                Dim tmpstr As String
                slastring = Mid(label, 1, InStr(label, "&"))
                tmpstr = Mid(slastring, InStr(slastring, "=") + 1)
                tmpstr = Mid(tmpstr, 1, Len(tmpstr) - 1)
                mySLAID = CInt(tmpstr)
            End If
            '  EventLog.WriteEntry("Application", "sla is " & mySLAID)
            If Len(mySLAID) = 0 Or Not IsNumeric(mySLAID) Then
                EventLog.WriteEntry("Application", "Unable to get updated SLAID from label")
                Exit Sub
            End If

            Dim sla As New SLA(mySLAID)
            sla.GSListener = m_gsListener
            sla.Manager = manager

            ' EventLog.WriteEntry("Application", "sla is " & mySLAID)

            ' Check Q and see if there's an old version...

            Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & SLAQ)
            Dim menum As System.Messaging.MessageEnumerator
            menum = mqq.GetMessageEnumerator()
            While menum.MoveNext()
                If InStr(menum.Current.Label, slastring) > 0 Then
                    menum.RemoveCurrent()
                    Exit While
                End If
            End While

            ' Now send updated SLA to SLA Q
            '   Dim m2 As Message = sla.CreateMQMessage
            ' SendToQ(SLAQ, m2)
            manager.LoadSLAs(mySLAID)

        Catch e As Exception
            EventLog.WriteEntry("GS Support Service", "Error updating sla:" & e.Message)
        Finally

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine reads all the messages
    ' in incoming workflow queue.
    ' ----------  ARPATECH  ----------
    Public Sub EConnectionListen()
        ' Read messages in the incoming workflow queue.
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim Conn2 As New SqlConnection(GSSupportManager.GetDBString())
        Dim AlertEmailTo As String
        AlertEmailTo = GSSupportManager.GetAlertEmail
        Dim Cmd As New SqlCommand
        Dim Cmd2 As New SqlCommand
        Dim dr As SqlDataReader
        'dr.Close()
        Dim DoAddAsset As Boolean = False
        ' Dim DoAddAddress As Boolean = False
        Do While True
            Try
                ' EventLog.WriteEntry("Starting Econnection Loop")

                Dim sql As String
                sql = "Select * from tblFieldServiceRequestInbound WHERE tblFieldServiceReqID in " & _
                                           " (select min(tblFieldServiceReqID) from tblFieldServiceRequestInbound)"

                Conn.Open()
                Conn2.Open()


                Cmd.Connection = Conn
                Cmd2.Connection = Conn2
                Cmd.CommandText = sql

                dr = Cmd.ExecuteReader
                Dim doContinue As Boolean = True

                While dr.Read()

                    Dim coverageID As Integer = 0
                    Dim tblFieldServiceReqID As New String("")
                    Dim ProcessingInstructionName As New String("")
                    Dim ProcessingInstructionValue As New String("")
                    Dim RequestHeaderStatus As New String("")
                    Dim ClientID As New String("")
                    Dim CustomerReqNum As New String("")
                    Dim PrimeContractorReqNum As New String("")
                    Dim AssignedServiceProvider As New String("")
                    Dim ServiceProviderReqNum As New String("")
                    Dim TimeStamp As New String("")
                    Dim ServiceRequestSummary As New String("")
                    Dim ServiceRequestDescription As New String("")
                    Dim ReqCategoryCode As New String("")
                    Dim ReqCategoryText As New String("")
                    Dim Priority As New String("")
                    Dim VendorEngNum As New String("")
                    Dim AssetSerialNum As New String("")
                    Dim AssetSerialTag As New String("")
                    Dim AssetManuf As New String("")
                    Dim AssetType As New String("")
                    Dim AssetModel As New String("")
                    Dim LocationNumber As New String("")
                    Dim CustomerName As New String("")
                    Dim AddressID As New String("")
                    Dim Address1 As New String("")
                    Dim Address2 As New String("")
                    Dim PriorityIntValue As Integer = -1
                    Dim City As New String("")
                    Dim StateProvince As New String("")
                    Dim PostalCode As New String("")
                    Dim CountryCode As New String("")
                    Dim Country As New String("")
                    Dim CountryID As Integer = 0
                    Dim PhoneNum As New String("")
                    Dim eBody As New String("")
                    Dim Email As New String("")
                    Dim CustomerContact As New String("")
                    Dim timeZoneId As Integer = 0
                    Dim HoursOfOperation
                    Dim StatusCode As New String("")
                    Dim StatusText As New String("")
                    Dim StatusID As Integer = 0
                    Dim ResolutionText As New String("")
                    Dim ResolutionTime As New String("")
                    Dim ETA As New String("")
                    Dim CommentType As New String("")
                    Dim StatusComment As New String("")
                    Dim CustomerContactID As New String("")
                    Dim PriorityID As Integer = -1
                    Dim GSComment As String
                    Dim ParentCompanyID As Integer = 0
                    Dim CustAddressID As Integer = 0
                    Dim CustAddressName As New String("")
                    Dim AssetID As Integer = 0
                    Dim ReturnedValue As Boolean = False
                    Dim eSubject As New String("")
                    Dim epriority As Integer = 2 '2 is high, 1 is low, 0 is normal

                    Dim inserttblname As New String("")
                    Dim ReturnedStatus As New String("")
                    Dim LaunchMethod As New String("")
                    Dim IDValue As New String("")
                    Dim GSTicketID As New Integer
                    Dim tryUpdate As Boolean = True
                    Dim tryNew As Boolean = True
                    Dim arTypeID As Integer = -1
                    Dim partyid As Integer = -1
                    Dim gscustomerid As Integer = -1
                    Dim emailSubject As New String("")
                    Dim ar As New ActionRequest
                    Dim ConvertTime As Date
                    Dim mfgid As Integer = 0

                    Try
                        If Not doContinue Then
                            Exit Sub
                        End If
                        eBody = ""
                        tblFieldServiceReqID = dr.GetInt32(dr.GetOrdinal("tblFieldServiceReqID"))
                        If Not dr.IsDBNull(dr.GetOrdinal("RequestHeaderStatus")) Then
                            RequestHeaderStatus = UCase(stringSaturate(dr.GetString(dr.GetOrdinal("RequestHeaderStatus"))))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ProcessingInstructionName")) Then
                            ProcessingInstructionName = UCase(stringSaturate(dr.GetString(dr.GetOrdinal("ProcessingInstructionName"))))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ProcessingInstructionValue")) Then
                            ProcessingInstructionValue = UCase(stringSaturate(dr.GetString(dr.GetOrdinal("ProcessingInstructionValue"))))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("CustomerReqNum")) Then
                            CustomerReqNum = stringSaturate(dr.GetString(dr.GetOrdinal("CustomerReqNum")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("PrimeContractorReqNum")) Then
                            PrimeContractorReqNum = stringSaturate(dr.GetString(dr.GetOrdinal("PrimeContractorReqNum")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AssignedServiceProvider")) Then
                            AssignedServiceProvider = stringSaturate(dr.GetString(dr.GetOrdinal("AssignedServiceProvider")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ServiceProviderReqNum")) Then
                            ServiceProviderReqNum = stringSaturate(dr.GetString(dr.GetOrdinal("ServiceProviderReqNum")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("TimeStamp")) Then
                            TimeStamp = stringSaturate(dr.GetString(dr.GetOrdinal("TimeStamp")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ServiceRequestSummary")) Then
                            ServiceRequestSummary = stringSaturate(dr.GetString(dr.GetOrdinal("ServiceRequestSummary")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ServiceRequestDescription")) Then
                            ServiceRequestDescription = stringSaturate(dr.GetString(dr.GetOrdinal("ServiceRequestDescription")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ReqCategoryCode")) Then
                            ReqCategoryCode = stringSaturate(dr.GetString(dr.GetOrdinal("ReqCategoryCode")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ReqCategoryText")) Then
                            ReqCategoryText = stringSaturate(dr.GetString(dr.GetOrdinal("ReqCategoryText")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("PriorityID")) Then
                            PriorityIntValue = stringSaturate(dr.GetString(dr.GetOrdinal("PriorityID")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("Priority")) Then
                            Priority = stringSaturate(dr.GetString(dr.GetOrdinal("Priority")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("VendorEngNum")) Then
                            VendorEngNum = stringSaturate(dr.GetString(dr.GetOrdinal("VendorEngNum")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AssetSerialNum")) Then
                            AssetSerialNum = stringSaturate(dr.GetString(dr.GetOrdinal("AssetSerialNum")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AssetSerialTag")) Then
                            AssetSerialTag = stringSaturate(dr.GetString(dr.GetOrdinal("AssetSerialTag")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AssetManuf")) Then
                            AssetManuf = stringSaturate(dr.GetString(dr.GetOrdinal("AssetManuf")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AssetType")) Then
                            AssetType = stringSaturate(dr.GetString(dr.GetOrdinal("AssetType")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AssetModel")) Then
                            AssetModel = stringSaturate(dr.GetString(dr.GetOrdinal("AssetModel")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("LocationNumber")) Then
                            LocationNumber = stringSaturate(dr.GetString(dr.GetOrdinal("LocationNumber")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("CustomerName")) Then
                            CustomerName = stringSaturate(dr.GetString(dr.GetOrdinal("CustomerName")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("AddressID")) Then
                            AddressID = stringSaturate(dr.GetString(dr.GetOrdinal("AddressID")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("Address1")) Then
                            Address1 = stringSaturate(dr.GetString(dr.GetOrdinal("Address1")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("Address2")) Then
                            Address2 = stringSaturate(dr.GetString(dr.GetOrdinal("Address2")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("City")) Then
                            City = stringSaturate(dr.GetString(dr.GetOrdinal("City")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("StateProvince")) Then
                            StateProvince = stringSaturate(dr.GetString(dr.GetOrdinal("StateProvince")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("PostalCode")) Then
                            PostalCode = stringSaturate(dr.GetString(dr.GetOrdinal("PostalCode")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("CountryCode")) Then
                            CountryCode = stringSaturate(dr.GetString(dr.GetOrdinal("CountryCode")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("Country")) Then
                            Country = stringSaturate(dr.GetString(dr.GetOrdinal("Country")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("PhoneNum")) Then
                            PhoneNum = stringSaturate(dr.GetString(dr.GetOrdinal("PhoneNum")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("Email")) Then
                            Email = stringSaturate(dr.GetString(dr.GetOrdinal("Email")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("CustomerContactID")) Then
                            CustomerContactID = stringSaturate(dr.GetString(dr.GetOrdinal("CustomerContactID")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("CustomerContact")) Then
                            CustomerContact = stringSaturate(dr.GetString(dr.GetOrdinal("CustomerContact")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("HoursOfOperation")) Then
                            HoursOfOperation = stringSaturate(dr.GetString(dr.GetOrdinal("HoursOfOperation")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("StatusCode")) Then
                            StatusCode = stringSaturate(dr.GetString(dr.GetOrdinal("StatusCode")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("StatusText")) Then
                            StatusText = stringSaturate(dr.GetString(dr.GetOrdinal("StatusText")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ResolutionText")) Then
                            ResolutionText = stringSaturate(dr.GetString(dr.GetOrdinal("ResolutionText")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ResolutionTime")) Then
                            ResolutionTime = stringSaturate(dr.GetString(dr.GetOrdinal("ResolutionTime")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("ETA")) Then
                            ETA = stringSaturate(dr.GetString(dr.GetOrdinal("ETA")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("CommentType")) Then
                            CommentType = stringSaturate(dr.GetString(dr.GetOrdinal("CommentType")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("StatusComment")) Then
                            StatusComment = stringSaturate(dr.GetString(dr.GetOrdinal("StatusComment")))
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("clientid")) Then
                            ClientID = stringSaturate(dr.GetString(dr.GetOrdinal("clientid")))
                        End If

                        ' EventLog.WriteEntry("Before GS Comment: ")
                        GSComment = "** GS Ticket Number: " & PrimeContractorReqNum & vbCrLf & _
                            "** Customer Name: " & ClientID & vbCrLf & _
                            "** Customer Ticket Number: " & CustomerReqNum & vbCrLf & _
                            "** AssetSerialNum: " & AssetSerialNum & vbCrLf & _
                           "** AssetSerialTag: " & AssetSerialTag & vbCrLf & _
                           "** Asset Manufacturer: " & AssetManuf & vbCrLf & _
                        "** AssetType: " & AssetType & vbCrLf & _
                           "** AssetModel: " & AssetModel & vbCrLf & _
                           "** ReqCategoryCode: " & ReqCategoryCode & vbCrLf & _
                           "** ReqCategoryText: " & ReqCategoryText & vbCrLf & _
                           "** PriorityID: " & PriorityIntValue & vbCrLf & _
                           "** Priority: " & Priority & vbCrLf & _
                           "** LocationNumber: " & LocationNumber & vbCrLf & _
                           "** CustomerName: " & CustomerName & vbCrLf & _
                           "** AddressID: " & AddressID & vbCrLf & _
                           "** Address: " & Address1 & "  " & Address2 & _
                           "** City: " & City & vbCrLf & _
                           "** StateProvince:  " & StateProvince & vbCrLf & _
                           "** PostalCode: " & PostalCode & vbCrLf & _
                           "** CountryCode: " & CountryCode & vbCrLf & _
                           "** Country: " & Country & vbCrLf & _
                           "** Phone: " & PhoneNum & vbCrLf & _
                           "** Email: " & Email & vbCrLf & _
                           "** CustomerContactID: " & CustomerContactID & vbCrLf & _
                           "** CustomerContact: " & CustomerContact & vbCrLf & _
                           "** StatusCode: " & StatusCode & vbCrLf & _
                           "** StatusText: " & StatusText & vbCrLf & _
                            "** Description: " & Trim(ServiceRequestDescription) & " Summary: " & Trim(ServiceRequestSummary)
                        'EventLog.WriteEntry("GSComment: " & GSComment)


                        'Sunita's Asset Piece
                        'Seperat the Manufacturer and Model for the Model we get from WorldSpan
                        'For the TI1600 Printers
                        'Call sp only if asset model has value
                        'EventLog.WriteEntry("Before calling GetCleanGSModelName, AssetModel is " & AssetModel & "ClientID is " & ClientID & "AssetManuf is " & AssetManuf)
                        If AssetModel <> "" AndAlso ClientID <> "" AndAlso AssetManuf <> "" AndAlso mfgid > 0 Then
                            AssetModel = GetCleanGSModelName(AssetModel, ClientID, AssetManuf)
                        End If
                        'EventLog.WriteEntry("After calling GetCleanGSModelName, AssetModel is " & AssetModel)
                        'End Sunita's Asset Piece
                        'Get the Parent Company ID using the name of the customer
                        'Name of the customer comes as ClientID in FSXML
                        If Not IsNothing(ClientID) AndAlso ClientID <> "" Then
                            ParentCompanyID = GSSupportManager.GetSingleDBValueInt("select ParentCompanyID from tblCustomers where Upper(customername) = '" & ClientID & "' ")
                            EventLog.WriteEntry("application", "ParentCompanyID" & ParentCompanyID)

                            If ParentCompanyID < 1 Then
                                'EventLog.WriteEntry("Unable to get company ID for " & ClientID)
                                eBody = "Unable to get company ID for " & ClientID & vbCrLf & eBody
                                ReturnedValue = False
                                tryNew = False
                                tryUpdate = False
                            Else
                                gscustomerid = GSSupportManager.GetSingleDBValueInt("select customerid from tblCustomers where parentcompanyid=" & ParentCompanyID)
                                EventLog.WriteEntry("application", "Before calling  arTypeID:" & arTypeID)

                                arTypeID = GSSupportManager.GetSingleDBValueInt("Select isNull(min(ARTypeID),0) from tblActionRequestType where Companyid = " & ParentCompanyID & _
                                                                                     " And isActive=1  And (upper(artypeName) = '" & UCase(ReqCategoryText) & "' or upper(arTypeName) = '" & UCase(ReqCategoryCode) & "')")

                                EventLog.WriteEntry("application", "After calling,  arTypeID:" & arTypeID)
                                If arTypeID < 1 Then
                                    eBody = "INVALID TICKET Type: " & ReqCategoryText & vbCrLf & eBody
                                    tryNew = False
                                End If
                                If Len(StatusCode) > 0 Or Len(StatusText) > 0 Then
                                    EventLog.WriteEntry("application", "Before calling GetStatusID, StatusID: " & StatusID & " and StatusCode " & StatusCode)
                                    StatusID = GetStatusId(StatusCode, StatusText, ParentCompanyID, arTypeID)
                                    EventLog.WriteEntry("application", "After Calling GetStatusid, StatusID: " & StatusID & " and StatusCode " & StatusCode)
                                    If StatusID < 1 Then
                                        eBody = "INVALID TICKET STATUS: " & StatusCode & vbCrLf & eBody
                                        'tryUpdate = False
                                        tryNew = False
                                    End If
                                End If

                                'Get the manufacturer id using Asset if available
                                'If not, then use manufacturername to get the mfgid
                                'If not, if the customer is WorldSpan, use GS as MFG so that the ticket gets routed to GSP properly

                                If AssetSerialNum <> "" Then
                                    AssetID = GSSupportManager.GetSingleDBValueInt("select assetid from gsnetp_tbl_assets where isactive=1 and serialnum = '" & AssetSerialNum & _
                                                            "' and assetsownedid in (select assetsownedid from gsnetp_tbl_assetsowned where " & _
                                                            " customerid =" & gscustomerid & ")")
                                End If

                                'EventLog.WriteEntry("AssetID using AssetSerialNum is " & AssetID)
                                If Not IsNothing(AssetID) AndAlso AssetID > 0 Then
                                    ' ar.AddField("assetid", AssetID)

                                    Try
                                        sql = "select ao.assettypeid, isnull(ao.model, '') as model, ao.manufacturer, ao.manufacturerid, ao.customerpartcategoryid, pc.partcategoryname " & _
                                        " from GSNetP_tbl_assetsowned ao left join tblPartCategories pc on pc.partcategoryid = ao.customerpartcategoryid where " & _
                                        " ao.assetsownedid in (select max(assetsownedid) from gsnetp_tbl_assets where assetid=" & AssetID & ")"
                                        Dim Conn3 As New SqlConnection(GSSupportManager.GetDBString())
                                        Dim Cmd3 As New SqlCommand

                                        Conn3.Open()
                                        Cmd3.Connection = Conn3
                                        Dim dr3 As SqlDataReader
                                        Cmd3.CommandText = sql
                                        dr3 = Cmd3.ExecuteReader

                                        While dr3.Read()
                                            If Not dr3.IsDBNull(dr3.GetOrdinal("model")) Then
                                                ar.AddFieldByDisplay("Model of Affected Asset", dr3.GetString(dr3.GetOrdinal("model")), dr3.GetString(dr3.GetOrdinal("model")))
                                            End If
                                            If Not dr3.IsDBNull(dr3.GetOrdinal("assettypeid")) Then
                                                ar.AddFieldByDisplay("Manufacturer SKU of Affected Asset", dr3.GetString(dr3.GetOrdinal("assettypeid")), dr3.GetString(dr3.GetOrdinal("assettypeid")))
                                            End If
                                            If Not dr3.IsDBNull(dr3.GetOrdinal("customerpartcategoryid")) Then
                                                ar.AddFieldByDisplay("Category of Affected Asset", dr3.GetInt32(dr3.GetOrdinal("customerpartcategoryid")), dr3.GetString(dr3.GetOrdinal("partcategoryname")))
                                            End If
                                            'if  manufacturerid from previous query is not there, assign manufacturerid from this query

                                            If Not dr3.IsDBNull(dr3.GetOrdinal("manufacturerid")) Then
                                                mfgid = dr3.GetInt32(dr3.GetOrdinal("manufacturerid"))
                                                EventLog.WriteEntry("GS Support Service", "MFGID dr3.IsDBNull(dr3.GetOrdinal(manufacturerid)) is :" & mfgid)
                                                ar.AddFieldByDisplay("Manufacturer of Affected Asset", dr3.GetInt32(dr3.GetOrdinal("manufacturerid")), dr3.GetString(dr3.GetOrdinal("manufacturer")))
                                            End If

                                        End While
                                        dr3.Close()
                                        Conn3.Close()
                                        'EventLog.WriteEntry("mfgid using AssetID " & AssetID & " is " & mfgid)

                                    Catch e As Exception
                                        EventLog.WriteEntry("application", "Error getting asset info: " & e.Message)
                                    End Try
                                End If

                                'Sunita's Manuf Piece
                                If Len(AssetManuf) > 0 Then
                                    'EventLog.WriteEntry("Before sp_ConvertToGSNames, AssetManuf: " & AssetManuf)
                                    AssetManuf = GetCleanGSName(AssetManuf)
                                    'EventLog.WriteEntry("After sp_ConvertToGSNames, AssetManuf: " & AssetManuf)
                                    If mfgid < 1 Then
                                        mfgid = GSSupportManager.GetSingleDBValueInt("select isNull(manufacturerid,0) from tblManufacturers where upper(ManufacturerName)='" & UCase(AssetManuf) & "'")
                                        'EventLog.WriteEntry("mfgid using AssetManuf: " & AssetManuf & " is: " & mfgid)
                                        If mfgid > 0 Then
                                            'EventLog.WriteEntry("AddFieldByDisplay mfgid  " & mfgid & " assetmanuf is " & AssetManuf)
                                            ar.AddFieldByDisplay("Manufacturer of Affected Asset", mfgid, AssetManuf)
                                        End If
                                    End If

                                End If
                                'EventLog.WriteEntry("mfgid After evaluating AssetID and AssetManuf  is " & mfgid)

                                If mfgid < 1 And ParentCompanyID = 1186 Then
                                    'For Worldspan, if mfgid is still not there, then use globalserve as manufacturer 
                                    'this will get the workflow of assigning GSP working
                                    'Which in turn will reduce the SLA exception
                                    EventLog.WriteEntry("GS Support Service", "MFGID inside WS customized piece is  :" & mfgid)
                                    mfgid = GSSupportManager.GetSingleDBValueInt("select isNull(manufacturerid,0) from tblManufacturers where upper(ManufacturerName)='GLOBALSERVE'")
                                    EventLog.WriteEntry("GS Support Service", "MFGID inside WS customized piece with Globalserve as mfg is is :" & mfgid)
                                    ar.AddFieldByDisplay("Manufacturer of Affected Asset", mfgid, "GlobalServe")
                                End If



                                'Get the proper Priority id for that ParentCompanyID
                                'If Priotity Id is available use it , other wise use the element value that
                                'is inside the Priority tag
                                'If both are unavailable use INFO as default Priority type.
                                PriorityID = GetPriorityID(PriorityIntValue, Priority, ParentCompanyID)
                                EventLog.WriteEntry("application", "PriorityID: " & PriorityID & " Priority: " & Priority & " and Priorityintvalue " & PriorityIntValue)

                                If IsNothing(PriorityID) OrElse Not IsNumeric(PriorityID) OrElse PriorityID < 1 Then
                                    eBody = "INVALID TICKET PRIORITY: " & Priority & vbCrLf & eBody
                                    tryNew = False
                                End If

                                If gscustomerid > 0 Then
                                    'Get customer AddressId with info provided
                                    'EventLog.WriteEntry("Before calling getCustomerAddressId is: " & CustAddressID)
                                    If Len(Trim(PrimeContractorReqNum)) > 0 Then
                                        If CustAddressID < 1 And RequestHeaderStatus = "UPDATE" And CInt(PrimeContractorReqNum) > 0 Then
                                            CustAddressID = GSSupportManager.GetSingleDBValueInt("Select AddressID FROM tblActionRequest WHERE ARID in (SELECT ArID FROM tblTicket WHERE     (TicketID = " & CInt(PrimeContractorReqNum) & "))")
                                            EventLog.WriteEntry("application", "CustAddressID for  RequestHeaderStatus type : " & RequestHeaderStatus & " and Ticket id= " & CInt(PrimeContractorReqNum) & " is: " & CustAddressID)
                                        End If
                                    End If

                                    If CustAddressID < 1 Then
                                        CustAddressID = GetCustomerAddressID(AddressID, LocationNumber, gscustomerid, AssetSerialNum, AssetID)
                                        EventLog.WriteEntry("application", "CustAddressID after calling getCustomerAddressId is: " & CustAddressID)
                                    End If
                                    If CustAddressID < 1 Then
                                        ' Let's try to get the addressid from other info
                                        CustAddressID = GSSupportManager.GetSingleDBValueInt("select isnull(min(customeraddressid), 0) from tblCustomerAddresses where " & _
                                        " upper(customeraddressname) = '" & UCase(CustomerName) & "' and address1 = '" & Replace(Address1, "'", "''") & "' and customerid in " & _
                                        " (select customerid from tblCustomers where parentcompanyid=" & ParentCompanyID & ")")
                                        EventLog.WriteEntry("application", "After matching address name and customername, customerAddressId is: " & CustAddressID)
                                    End If
                                End If
                                If IsNothing(CustAddressID) OrElse Not IsNumeric(CustAddressID) OrElse CustAddressID < 1 Then
                                    eBody = "Location information is not valid. LocationNumber : " & LocationNumber & vbCrLf & eBody
                                    tryNew = False
                                End If
                                If CountryID < 1 AndAlso CustAddressID > 0 Then
                                    CountryID = GSSupportManager.GetSingleDBValueInt("select isnull(countryid, 0) from tblCustomerAddresses where customeraddressid=" & CustAddressID)
                                End If
                                ' Can't enter ticket if no addressid
                                If CustAddressID < 1 Or CountryID < 1 Then
                                    eBody = "Unable to get customer address for: " & LocationNumber & vbCrLf & eBody
                                    tryNew = False
                                End If

                                If (timeZoneId <= 0) And CustAddressID > 0 Then
                                    timeZoneId = GSSupportManager.GetSingleDBValueInt("select IsNull(TimeZoneID, 0) as TimeZoneID " & _
                                                        " from tblCustomerAddresses where CustomerAddressId = " & CustAddressID)
                                    If timeZoneId <= 0 Then
                                        timeZoneId = GSSupportManager.GetSingleDBValueInt("select IsNull(TimeZoneID, 0) as TimeZoneID " & _
                                                                  " from tblCountryDefaultTimeZone where countryid in (select countryid from tblCustomerAddresses where " & _
                                                                  " CustomerAddressId = " & CustAddressID & ")")
                                    End If

                                End If
                                EventLog.WriteEntry("Application", "Before Calling ConverTime, CustAddressID: " & CustAddressID & " TimeZoneID: " & timeZoneId)
                                If (CustAddressID > 0) AndAlso (timeZoneId > 0) Then
                                    ConvertTime = GetTimeZone(CustAddressID, timeZoneId)
                                End If
                                If (CustomerContactID = "") And (CustomerContact <> "") Then
                                    CustomerContactID = GetCustomerContactId(CustomerContact, ParentCompanyID)
                                End If
                                '  If the Serial Number is Available, use it to get the AssetID



                                ' If  have no assetID add the asset and flag is set to add asset, lets add it.
                                ' Dim eConnectionFormatID 'xml or email...
                                Dim econnectionTypeID As Integer  'is support request
                                Dim eConnectionStandard As String 'rely on Asset or not

                                econnectionTypeID = GSSupportManager.GetSingleDBValueInt("select econnectionTypeID from tblEconnectionParams where CompanyID = " & ParentCompanyID)
                                eConnectionStandard = GSSupportManager.GetSingleDBValueString("select standard from tblEconnectionParams where companyid=" & ParentCompanyID & " and econnectionTypeID = " & econnectionTypeID)
                                EventLog.WriteEntry("application", "econnectionStandard " & eConnectionStandard)
                                'Fail to log ticket if no asset info is provided.
                                ' If (Trim(UCase(eConnectionStandard)) = "ASSET") Then
                                'If IsNothing(AssetID) OrElse Not IsNumeric(AssetID) OrElse AssetID < 1 Then
                                '   eBody = "Asset information Provided is not valid. AssetID : " & AssetID & vbCrLf & eBody
                                '  tryNew = False
                                'End If
                                'End If

                                'Use following code if you want to add asset electronically

                                'If DoAddAsset AndAlso Len(AssetSerialNum) > 0 AndAlso CustAddressID > 0 Then
                                'If (Trim(UCase(eConnectionStandard)) = "ASSET") And (AssetID < 1) Then
                                ' AssetID = AddAsset(AssetManuf, AssetModel, AssetType, ParentCompanyID, AssetSerialNum, CustAddressID)
                                ' End If
                                ' End If

                                If Len(CustomerReqNum) > 0 Then
                                    '   EventLog.WriteEntry("Adding customer req num " & CustomerReqNum)
                                    ar.CustomerRequestID = CustomerReqNum
                                    ar.AddFieldByDisplay("Customer Ticket ID", CustomerReqNum)
                                End If

                                If Len(ServiceProviderReqNum) > 0 Then
                                    ar.SupplierRequestID = ServiceProviderReqNum
                                    ar.AddFieldByDisplay("GSP Ticket ID", ServiceProviderReqNum)
                                End If
                                If Not IsNothing(ServiceRequestSummary) AndAlso Len(ServiceRequestSummary) > 0 Then
                                    ar.Summary = ServiceRequestSummary
                                End If

                                If Not IsNothing(ServiceRequestDescription) AndAlso Len(ServiceRequestDescription) > 0 Then
                                    ar.Description = ServiceRequestDescription
                                End If
                                ' end of moved stuff

                                'Check to see if the status came empty. If not start processing.
                                ' IF the Header status is empty, or junk value, send email to CC, move date to error table and quit
                                If RequestHeaderStatus = "NEW" And Not tryNew Then
                                    ReturnedValue = False
                                ElseIf RequestHeaderStatus = "UPDATE" And Not tryUpdate Then
                                    ReturnedValue = False
                                Else
                                    RequestHeaderStatus = Trim(RequestHeaderStatus)
                                    RequestHeaderStatus = UCase(RequestHeaderStatus)

                                    Dim isSubmitted As Boolean = True
                                    'If the request is for the creation of new ticket
                                    'The following code will be executed
                                    If (RequestHeaderStatus = "NEW") And (ClientID <> "") And (ParentCompanyID > 0) And tryNew Then
                                        ar.GenerateTicket = True
                                        ar.Qualify = True
                                        ar.SetCompanyID(ParentCompanyID, ClientID)
                                        If AssetID > 0 Then
                                            ar.GSAssetID = AssetID
                                        End If
                                        ar.SubmitUserID = m_casID
                                        ar.AddFieldByDisplay("Submitting User Phone", "")
                                        ar.SubmitDate = New Date(Now.Ticks)
                                        ar.RequestDate = New Date(Now.Ticks)
                                        ' EventLog.WriteEntry(ar.SubmitDate & ar.RequestDate)
                                        ar.RequestDateLocal = ConvertTime
                                        ar.TicketCreateLocal = ConvertTime
                                        ar.RequestDateTimeZoneID = timeZoneId

                                        'ar.RequestDateLocalGMTOffset = requestdategmtoffset
                                        ' ar.RequestDateLocalGMTOffsetString = requestdategmtoffsetstring traffic roles 
                                        If Not IsNothing(CustAddressID) AndAlso Len(CStr(CustAddressID)) > 0 Then
                                            'EventLog.WriteEntry("Customer AddressID is  " & CustAddressID)
                                            ar.AddressID = CustAddressID

                                            ' CustAddressName = GSSupportManager.GetSingleDBValueString("select customeraddressname from tblCustomerAddresses where customeraddressid=" & CustAddressID)
                                            CustAddressName = GSSupportManager.GetSingleDBValueString("select isnull(customeraddressname, '') + isnull(CHAR(13) + CHAR(10) +  address1, '') + isnull(CHAR(13) + CHAR(10)  + address2, '') + isnull(CHAR(13) + CHAR(10) +  city, '') + isnull('  ' + stateprovinceregion, '') + isnull(' ' + postalcode, '')  + isnull(' ' + countryname, '') from tblcustomeraddresses, tblcountries where tblcountries.countryid = tblcustomeraddresses.countryid and tblcustomeraddresses.customeraddressid=" & CustAddressID)
                                            ar.SetAddressID(CustAddressID, CustAddressName)
                                            ar.AddFieldByDisplay("Address of Affected User", CustAddressID, CustAddressName)

                                            CountryID = GSSupportManager.GetSingleDBValueInt("select countryid from tblCustomerAddresses where customeraddressid=" & CustAddressID)
                                            Country = GSSupportManager.GetSingleDBValueString("select countryname from tblCountries where countryid=" & CountryID)

                                            ar.AddFieldByDisplay("Country of Affected User", CountryID, Country)
                                            '      If AssetID > 0 Then
                                            ar.AddFieldByDisplay("Country of Affected Asset", CountryID, Country)
                                            ar.AddFieldByDisplay("Address of Affected Asset", CustAddressID, CustAddressName)
                                            ar.AddFieldByDisplay("Serial Number", AssetSerialNum)


                                            ' End If

                                            '     ar.AddFieldByDisplay("customeraddressid", CustAddressID, CustAddressName)
                                            coverageID = GSSupportManager.GetSingleDBValueInt("select isNull(coverageid, 0) from tblCustomerAddresses where customeraddressid=" & CustAddressID)
                                            If Not IsNothing(coverageID) AndAlso IsNumeric(coverageID) AndAlso CInt(coverageID) > 0 Then
                                                ar.AddFieldByDisplay("Coverage Type", CStr(coverageID))
                                            Else
                                                ar.AddFieldByDisplay("Coverage Type", "0")
                                            End If
                                        End If

                                        ' ar.AffectedContactID = affectedcontactid
                                        ' Company ID must come before address so we can find country
                                        If IsNumeric(PriorityID) AndAlso CInt(PriorityID) > 0 Then
                                            ar.SetPriorityID(PriorityID, Priority)
                                        End If

                                        If IsNumeric(arTypeID) AndAlso CInt(arTypeID) > 0 Then
                                            ar.SetTypeID(arTypeID, ReqCategoryText)
                                        End If

                                        If IsNumeric(CustomerContactID) AndAlso CInt(CustomerContactID) > 0 Then
                                            ar.SetAffectedContactID(CustomerContactID, CustomerContact)
                                            ar.AddFieldByDisplay("Affected Contact Phone", GSSupportManager.GetSingleDBValueString("select workphonenumber from tblContact where contactid=" & CustomerContactID))
                                        End If



                                        If RequestHeaderStatus = "UPDATE" Then
                                            ar.CategoryName = "Ticket Change"
                                        Else
                                            ar.CategoryName = "New Request"
                                        End If
                                        ar.AddFieldByDisplay("Ticket Submitted By", m_casID, "casagent")
                                        ar.AddFieldByDisplay("Ticket Summary", ServiceRequestSummary)
                                        ar.AddFieldByDisplay("Ticket Description", ServiceRequestDescription)
                                        If Not IsNothing(GSComment) And Len(GSComment) > 0 Then
                                            ar.GSComments = GSComment
                                        End If

                                        '  ar.LogFieldsToEventLog()
                                        '   GSSupportManager.LogARDetails(ar)
                                        If Not ar.Submit() Then
                                            EventLog.WriteEntry("Application", "Error submitting new eConnnection request.")
                                        Else
                                            ReturnedValue = True
                                            GSTicketID = ar.GSTicketID
                                        End If
                                        'If the Request is for Updating the Existing Ticket
                                        'The following code will be executed

                                    ElseIf (RequestHeaderStatus = "UPDATE") And tryUpdate Then
                                        'If the ticket doesn't have valid GSTicketId (PrimeContractorReqNum), then send email to CC, move data to error table and quit.
                                        'Else Process
                                        Dim tmpint As Integer = 0
                                        tmpint = GetTicketId(ParentCompanyID, CustomerReqNum, PrimeContractorReqNum)

                                        If IsNumeric(tmpint) And tmpint > 0 Then
                                            PrimeContractorReqNum = tmpint
                                            GSTicketID = tmpint
                                            Try
                                                If tmpint = 0 Then
                                                    eBody = "INVALID TICKET: Ticket ID " & PrimeContractorReqNum & " is not valid for this customer." & vbCrLf & eBody
                                                    tryUpdate = False
                                                Else
                                                    'This applies only to Update request.
                                                    'If it is worldspan or Valeant Pharmaceuticals,
                                                    'Don't change the status if it is an update message
                                                    'we don't want customer making status change
                                                    If ((ParentCompanyID = 1186) Or (ParentCompanyID = 1965)) And (RequestHeaderStatus = "UPDATE") Then
                                                        Dim tmpStatusID As Integer = GSSupportManager.GetSingleDBValueInt("select tktstatusid from " & _
                                                                                        " tblTicket join tblActionRequest on tblActionrequest.arid " & _
                                                                                        " = tblTicket.arid and tblActionRequest.CompanyID = " & ParentCompanyID & " where ticketid=" & PrimeContractorReqNum)
                                                        If tmpStatusID > 0 Then
                                                            'EventLog.WriteEntry("inside Update ticket, Existing ticket status id in OneSource is: " & tmpStatusID & " where as TicketUpdate message from client came with ticket statusid " & StatusID)
                                                            StatusID = tmpStatusID
                                                        End If
                                                    End If
                                                    ' Prepare competed action...
                                                    Dim aut As New ActionUpdateTicket
                                                    aut.LoadDefaultValues(GSTicketID)
                                                    aut.UserID = m_casID

                                                    Dim am As New ActionMonitor
                                                    Cmd2.CommandType = Data.CommandType.StoredProcedure
                                                    Cmd2.CommandText = "sp_UpdateTicketStatus"
                                                    SqlCommandBuilder.DeriveParameters(Cmd2)
                                                    Cmd2.Parameters("@userid").Value = m_casID
                                                    ' CmdSp.Parameters("@requestdate").Value = Now()
                                                    Cmd2.Parameters("@logdatelocal").Value = ConvertTime
                                                    Cmd2.Parameters("@ticketid").Value = PrimeContractorReqNum
                                                    ar.GSTicketID = GSTicketID
                                                    Cmd2.Parameters("@customerticketid").Value = CustomerReqNum
                                                    Cmd2.Parameters("@sfaticketid").Value = ServiceProviderReqNum
                                                    EventLog.WriteEntry("application", "ConvertTime is: " & ConvertTime)
                                                    If Len(StatusComment) > 0 Then
                                                        Cmd2.Parameters("@comments").Value = StatusComment
                                                    End If
                                                    Cmd2.Parameters("@isActive").Value = True
                                                    If IsNumeric(PriorityID) AndAlso PriorityID > 0 Then
                                                        Cmd2.Parameters("@PriorityID").Value = PriorityID
                                                        aut.PriorityID = PriorityID
                                                    End If

                                                    If IsNumeric(StatusID) AndAlso StatusID > 0 Then
                                                        Cmd2.Parameters("@tktstatusid").Value = StatusID
                                                        aut.StatusID = StatusID
                                                    End If

                                                    Cmd2.ExecuteNonQuery()
                                                    ar.Qualify = False
                                                    ar.CategoryName = "Ticket Change"
                                                    ar.RequestDateTimeZoneVBConst = ServerTimeZone
                                                    ' Create Action Monitor, ActionUpdateTicket
                                                    aut.IsComplete = True
                                                    aut.IsError = False
                                                    ' aut.TypeID = arTypeID
                                                    ' ActionUpdateTicket is the completed Action for the Action Monitor
                                                    am.Action = aut
                                                    ' ActionMonitor is the contains the completed action for the Action Request
                                                    ar.CompletedAction = am
                                                    ar.IsCompletedAction = True
                                                    If ar.Submit() Then
                                                        ReturnedValue = True
                                                    Else
                                                        ReturnedValue = False
                                                    End If
                                                End If

                                            Catch e As Exception
                                                doContinue = False
                                                EventLog.WriteEntry("Application", "Error updating eConnection ticket: " & e.Message)
                                            End Try
                                        Else

                                            'Ticket doesn't have valid GSTicketId 
                                            'so send email and quit.

                                            ReturnedValue = False
                                            eBody = " Failed to Process request with tblFieldServiceReqID " & tblFieldServiceReqID & vbCrLf & _
                                                     " Doesn't have proper GS Ticket ID" & vbCrLf & _
                                                     " Details of the ticket: "

                                            inserttblname = "tblFieldServReqInbError"

                                        End If
                                        'Ticket has blank as the Status Value.
                                        'It can either have value 'NEW' or 'UPDATE'
                                    Else
                                        ReturnedValue = False
                                        eBody = " Failed to Process request with tblFieldServiceReqID " & tblFieldServiceReqID & vbCrLf & _
                                                  " Invalid Request Header : ' " & RequestHeaderStatus & " '" & vbCrLf & _
                                                  " Must be NEW or UPDATE." & vbCrLf & _
                                                  " Details of the ticket: "

                                        inserttblname = "tblFieldServReqInbError"
                                        ' EventLog.WriteEntry(eBody)
                                    End If
                                End If
                            End If
                        End If
                    Catch e As Exception
                        doContinue = False
                        EventLog.WriteEntry("application", "Error processing InboundFieldRequest row: " & e.Message)
                    Finally

                        'Now get ready to call Agent to send response to Client
                        'Also move the data from tblFieldServiceRequestInbound to
                        'either tblFieldServReqProcessed (if ReturnedValue = True)
                        'or tblFieldServRewError( if ReturnedValue =  false)
                        Try

                            If ReturnedValue = True Then
                                ReturnedStatus = "Update"
                                inserttblname = "tblFieldServReqInbProcessed"
                                LaunchMethod = "LaunchAgentSuccess"
                                If RequestHeaderStatus = "NEW" Then
                                    IDValue = CInt(GSTicketID)
                                ElseIf RequestHeaderStatus = "UPDATE" Then
                                    IDValue = CInt(PrimeContractorReqNum)
                                End If
                            Else
                                ReturnedValue = False
                                ReturnedStatus = "Error"
                                inserttblname = "tblFieldServReqInbError"
                                LaunchMethod = "LaunchAgentError"
                                IDValue = CInt(tblFieldServiceReqID)
                                Dim GSContactEmail As String
                                GSContactEmail = ""
                                GSContactEmail = GSSupportManager.GetSingleDBValueString("Select GSContactEmail FROM tblParentCompanies WHERE ParentCompanyID = " & ParentCompanyID)

                                If GSContactEmail <> "" Then
                                    GSContactEmail = GSContactEmail & "; econnect@global-serve.com"
                                    'EventLog.WriteEntry("application", "GSContactEmail from  tblParentCompanies : " & GSContactEmail)
                                Else
                                    GSContactEmail = "econnect@global-serve.com"
                                    EventLog.WriteEntry("application", "Cannot find GSContactEmail in  tblParentCompanies, so used : " & GSContactEmail)
                                End If
                                'If the error occured at any point, send email to CC
                                ' SendMail("cc@global-serve.com;econnect@global-serve.com", "Automailer@global-serve.com", eBody, RequestHeaderStatus & " GS Support Error Alert")

                                eBody = eBody & vbNewLine & " Here is the summary and description of the Request " & vbNewLine & _
                                                               "**" & GSComment
                                EventLog.WriteEntry("application", "Sending error email to : " & GSContactEmail)
                                SendMail(GSContactEmail, "Automailer@global-serve.com", eBody, "e-Connection Error- " & RequestHeaderStatus & " GS Support Error Alert for Customer Request Number: " & CustomerReqNum, epriority)
                            End If

                            'Now Move data to the Secondary Table
                            'To the table "tblFieldServReqInbProcessed" in the case of Success and
                            ' tblFieldServReqInbError in the case of Failure

                            Dim MoveDataSQL As String
                            Cmd2 = New SqlCommand
                            Cmd2.Connection = Conn2

                            MoveDataSQL = "Insert into " & inserttblname & "   values (" & tblFieldServiceReqID & " ,'" & ProcessingInstructionName & "','" & ProcessingInstructionValue & "','" & RequestHeaderStatus & "','" & ClientID & "','" & CustomerReqNum & "','" & PrimeContractorReqNum & _
                          "','" & AssignedServiceProvider & "','" & ServiceProviderReqNum & "','" & TimeStamp & _
                           "','" & ServiceRequestSummary & "','" & ServiceRequestDescription & _
                           "','" & ReqCategoryCode & "','" & ReqCategoryText & "','" & PriorityID & "','" & Priority & "','" & VendorEngNum & _
                           "','" & AssetSerialNum & "','" & AssetSerialTag & "','" & AssetManuf & "','" & AssetType & "','" & AssetModel & _
                           "','" & LocationNumber & "','" & CustomerName & "','" & AddressID & "','" & Address1 & "','" & Address2 & _
                           "','" & City & "','" & StateProvince & "','" & PostalCode & "','" & CountryCode & "','" & Country & "','" & PhoneNum & _
                           "','" & Email & "','" & CustomerContactID & "','" & CustomerContact & "','" & HoursOfOperation & "','" & StatusCode & _
                           "','" & StatusText & "','" & ResolutionText & "','" & ResolutionTime & _
                           "','" & ETA & "','" & CommentType & "','" & StatusComment & "')"


                            Cmd2.CommandText = MoveDataSQL
                            Cmd2.ExecuteNonQuery()
                            'Now Call the according Agent (Success or Error)
                            'pass the LaunchAgent Method (which is LaunchAgentError in the case of error and LaunchAgentSuccess otherwise) and the 
                            'IdValue (which is tblFieldServiceReqID in the case of Error and TicketId in the case of Success
                            Dim eConnectionFormatID
                            eConnectionFormatID = 0
                            eConnectionFormatID = GSSupportManager.GetSingleDBValueInt("select eConnectionFormatID from tblEconnectionParams where CompanyID = " & ParentCompanyID)
                            'EventLog.WriteEntry("econnection formatid: " & eConnectionFormatID)

                            If (ParentCompanyID = 1186) AndAlso (eConnectionFormatID = 1) Then
                                'EventLog.WriteEntry("Loading Agent")
                                CallAgent(IDValue, LaunchMethod)
                            End If
                            If (ParentCompanyID = 1965) AndAlso (eConnectionFormatID = 1) Then
                                'EventLog.WriteEntry("calling GSSupport.ActionServiceResponseOutbound with LaunchMethod = " & LaunchMethod & " And IDValue " & IDValue)
                                Dim aFSR As New ActionServiceResponseOutbound

                                If LaunchMethod = "LaunchAgentSuccess" Then
                                    aFSR.GSTicketID = IDValue
                                    aFSR.ResponseType = "SUCCESS"
                                    ' aFSR.Execute()
                                Else
                                    aFSR.GSTicketID = IDValue
                                    aFSR.ResponseType = "ERROR"
                                    aFSR.Execute()

                                End If
                            End If
                        Catch e As Exception
                            'EventLog.WriteEntry("Error postprocessing eConnection request: " & e.Message & " Source: " & e.Source & " e to string: " & e.ToString)
                        Finally

                            'Once you are done processing request and moving the data to the right Table
                            'Delete it from the old table so that  next one gets processed
                            'The request are processed in FIFO order
                            Cmd2 = New SqlCommand
                            Cmd2.Connection = Conn2
                            sql = "Delete from tblFieldServiceRequestInbound where tblFieldServiceReqID = " & tblFieldServiceReqID

                            Cmd2.CommandText = sql
                            Cmd2.ExecuteNonQuery()

                            '   CountSelect = CountSelect - 1
                        End Try
                    End Try
                End While

                dr.Close()
                Thread.CurrentThread.Sleep(30000)
            Catch e As Exception
                '("Error listening for eConnections " & e.Message)
            Finally
                If Not IsNothing(dr) And Not dr.IsClosed Then
                    dr.Close()
                End If
                Conn.Close()
                Conn2.Close()
                ' Thread.CurrentThread.Suspend()
            End Try
        Loop



    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine reads new orders
    ' and send email for all the new orders
    ' ----------  ARPATECH  ----------
    Public Sub NewOrderListen()

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim Cmd As New SqlCommand
        Dim Conn2 As New SqlConnection(GSSupportManager.GetDBString())
        Dim Cmd2 As New SqlCommand

        Do While True
            Try
                Dim sql As String
                Dim dr As SqlDataReader
                Dim doContinue As Boolean = True

                sql = "SELECT * FROM tblOrdersProcessQueue"

                Conn.Open()
                Conn2.Open()
                Cmd.Connection = Conn
                Cmd2.Connection = Conn2

                Cmd.CommandText = sql
                dr = Cmd.ExecuteReader

                While dr.Read()
                    Dim SessionTrackingNumber As New String("")
                    Dim OrderTrackingNumber As New String("")
                    Dim DestinationTrackingNumber As New String("")
                    Dim SFAID As New String("")
                    Dim ReturnedValue As Boolean = True

                    Try
                        If Not doContinue Then
                            Exit Sub
                        End If

                        If Not dr.IsDBNull(dr.GetOrdinal("SessionTrackingNumber")) Then
                            SessionTrackingNumber = dr.GetInt32(dr.GetOrdinal("SessionTrackingNumber"))
                            If Len(Trim(SessionTrackingNumber)) = 0 Then
                                ReturnedValue = False
                            End If
                        Else
                            ReturnedValue = False
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("OrderTrackingNumber")) Then
                            OrderTrackingNumber = dr.GetInt16(dr.GetOrdinal("OrderTrackingNumber"))
                            If Len(Trim(OrderTrackingNumber)) = 0 Then
                                ReturnedValue = False
                            End If
                        Else
                            ReturnedValue = False
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("DestinationTrackingNumber")) Then
                            DestinationTrackingNumber = dr.GetInt16(dr.GetOrdinal("DestinationTrackingNumber"))
                            If Len(Trim(DestinationTrackingNumber)) = 0 Then
                                ReturnedValue = False
                            End If
                        Else
                            ReturnedValue = False
                        End If
                        If Not dr.IsDBNull(dr.GetOrdinal("SFAID")) Then
                            SFAID = dr.GetInt32(dr.GetOrdinal("SFAID"))
                            If Len(Trim(SFAID)) = 0 Then
                                ReturnedValue = False
                            End If
                        Else
                            ReturnedValue = False
                        End If

                        If ReturnedValue = True Then
                            Dim ar As New ActionRequest
                            Dim SFAName As New String("")

                            SFAName = GSSupportManager.GetSingleDBValueString("SELECT SFAName FROM tblSFAs WHERE SFAID=" & SFAID)

                            ar.Qualify = True
                            ar.SubmitDate = New Date(Now.Ticks)
                            ar.RequestDate = New Date(Now.Ticks)
                            ar.RequestDateTimeZoneVBConst = ServerTimeZone
                            ar.CategoryName = "New Order"

                            ar.AddFieldByDisplay("Session Tracking Number", SessionTrackingNumber)
                            ar.AddFieldByDisplay("Order Tracking Number", OrderTrackingNumber)
                            ar.AddFieldByDisplay("Destination Tracking Number", DestinationTrackingNumber)
                            ar.AddFieldByDisplay("FSC", SFAID)

                            If Not ar.Submit() Then
                                EventLog.WriteEntry("Application", "Error submitting new order request.")
                                doContinue = False
                                Try
                                    Dim MoveDataSQL As String
                                    Dim DeleteSQL As String

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    DeleteSQL = "DELETE FROM tblOrdersFailedQueue WHERE SessionTrackingNumber=" & SessionTrackingNumber & _
                                        " AND OrderTrackingNumber=" & OrderTrackingNumber & _
                                        " AND DestinationTrackingNumber=" & DestinationTrackingNumber & _
                                        " AND SFAID=" & SFAID
                                    Cmd2.CommandText = DeleteSQL
                                    Cmd2.ExecuteNonQuery()

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    MoveDataSQL = "INSERT INTO tblOrdersFailedQueue (SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, SFAID)" & _
                                        " VALUES (" & SessionTrackingNumber & "," & OrderTrackingNumber & "," & DestinationTrackingNumber & "," & SFAID & ")"
                                    Cmd2.CommandText = MoveDataSQL
                                    Cmd2.ExecuteNonQuery()

                                    SendMail("orderalerts@global-serve.com", "Automailer@global-serve.com", "Error in processing order", "GlobalServe ONESOURCE - Order Processing Error - ORDER # " & SessionTrackingNumber & "-" & OrderTrackingNumber & "-" & DestinationTrackingNumber & " - " & SFAName)

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    DeleteSQL = "DELETE FROM tblOrdersProcessQueue WHERE SessionTrackingNumber=" & SessionTrackingNumber & _
                                        " AND OrderTrackingNumber=" & OrderTrackingNumber & _
                                        " AND DestinationTrackingNumber=" & DestinationTrackingNumber & _
                                        " AND SFAID=" & SFAID
                                    Cmd2.CommandText = DeleteSQL
                                    Cmd2.ExecuteNonQuery()
                                Catch e As Exception
                                    'EventLog.WriteEntry("Error postprocessing new order: AR not submitted: " & e.Message)
                                End Try
                            Else
                                Try
                                    Dim MoveDataSQL As String
                                    Dim DeleteSQL As String

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    DeleteSQL = "DELETE FROM tblOrdersSentQueue WHERE SessionTrackingNumber=" & SessionTrackingNumber & _
                                        " AND OrderTrackingNumber=" & OrderTrackingNumber & _
                                        " AND DestinationTrackingNumber=" & DestinationTrackingNumber & _
                                        " AND SFAID=" & SFAID
                                    Cmd2.CommandText = DeleteSQL
                                    Cmd2.ExecuteNonQuery()

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    MoveDataSQL = "INSERT INTO tblOrdersSentQueue (SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, SFAID)" & _
                                        " VALUES (" & SessionTrackingNumber & "," & OrderTrackingNumber & "," & DestinationTrackingNumber & "," & SFAID & ")"
                                    Cmd2.CommandText = MoveDataSQL
                                    Cmd2.ExecuteNonQuery()

                                    SendMail("orderalerts@global-serve.com", "Automailer@global-serve.com", "Order processed sucessfully", "GlobalServe ONESOURCE - Order Processing Successful - ORDER # " & SessionTrackingNumber & "-" & OrderTrackingNumber & "-" & DestinationTrackingNumber & " - " & SFAName)

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    DeleteSQL = "DELETE FROM tblOrdersProcessQueue WHERE SessionTrackingNumber=" & SessionTrackingNumber & _
                                        " AND OrderTrackingNumber=" & OrderTrackingNumber & _
                                        " AND DestinationTrackingNumber=" & DestinationTrackingNumber & _
                                        " AND SFAID=" & SFAID
                                    Cmd2.CommandText = DeleteSQL
                                    Cmd2.ExecuteNonQuery()

                                    Cmd2 = New SqlCommand
                                    Cmd2.Connection = Conn2
                                    DeleteSQL = "DELETE FROM tblOrdersFailedQueue WHERE SessionTrackingNumber=" & SessionTrackingNumber & _
                                        " AND OrderTrackingNumber=" & OrderTrackingNumber & _
                                        " AND DestinationTrackingNumber=" & DestinationTrackingNumber & _
                                        " AND SFAID=" & SFAID
                                    Cmd2.CommandText = DeleteSQL
                                    Cmd2.ExecuteNonQuery()
                                Catch e As Exception
                                    ' EventLog.WriteEntry("Error postprocessing new order : AR submitted: " & e.Message)
                                End Try
                            End If
                        End If
                    Catch e As Exception
                        doContinue = False
                        'EventLog.WriteEntry("Error processing new order: " & e.Message)
                    End Try
                End While
                dr.Close()
                Thread.CurrentThread.Sleep(30000)
            Catch e As Exception
                'EventLog.WriteEntry("Error listening for new orders " & e.Message)
            Finally
                Conn.Close()
                Conn2.Close()
            End Try
        Loop

    End Sub

    ' ----------  ARPATECH  ----------
    ' this sub routine reads for updated orders
    ' ----------  ARPATECH  ----------
    Public Sub OrderChangeListen()

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim Cmd As New SqlCommand
        Dim Conn2 As New SqlConnection(GSSupportManager.GetDBString())
        Dim Cmd2 As New SqlCommand
        Dim sql As String
        Dim dr As SqlDataReader
        Dim dr2 As SqlDataReader

        Do While True
            Try

                Dim doContinue As Boolean = True


                sql = "SELECT si.ociid, si.sessiontrackingnumber, si.ordertrackingnumber, si.destinationtrackingnumber, " & _
                " si.changeeta, si.changeordership, si.changeorderaccept, si.ordereta, si.ordershipdate, " & _
                " si.orderacceptdate, o.orderedbyuserid, c.email, cur.currencySymbol, ordercreatedbyuserid, " & _
                " u2.username as ordercreatedbyusername, c2.email as ordercreatedbyuseremail, o.customerid, orderdate, co.countryname, cust.parentcompanyid, parentcompanyname, orderedbyuserid, orderdestinationcountryid, " & _
                " orderdestinationcurrencyid, o.orderstatusid, os.orderstatusname, orderpo, totalordervalue, totalordervalueusd, u.username, " & _
                " od.ETA as oldeta, od.dateshipped as oldshipped, od.dateacceptedbysupplier as oldaccepted " & _
                " from tblOrderChangeIncoming si left join tblOrders o on o.sessiontrackingnumber = si.sessiontrackingnumber and o.ordertrackingnumber = si.ordertrackingnumber" & _
                " left join tblCustomers cust on o.customerid = cust.customerid " & _
                " left join tblusers u on u.userid = o.orderedbyuserid " & _
                " left join tblOrderStatus os on os.orderstatusid = o.orderstatusid " & _
                " left join tblContact c on c.userid = u.userid" & _
                " left join tblCurrencies cur on cur.currencyid = o.orderdestinationcurrencyid " & _
                " left join tblCountries co on co.countryid = o.orderdestinationcountryid " & _
                " left join tblUsers u2 on u2.userid = o.ordercreatedbyuserid " & _
                " left join tblParentCompanies pc on cust.parentcompanyid = pc.parentcompanyid " & _
                " left join tblContact c2 on c2.userid = o.ordercreatedbyuserid " & _
                " left join tblOrderDestinations od on od.sessiontrackingnumber = si.sessiontrackingnumber " & _
                " and od.ordertrackingnumber = si.ordertrackingnumber and od.destinationtrackingnumber = si.destinationtrackingnumber "

                Conn.Open()
                Conn2.Open()
                Cmd.Connection = Conn
                Cmd2.Connection = Conn2

                Cmd.CommandText = sql
                dr = Cmd.ExecuteReader



                While dr.Read()

                    Dim ar As New ActionRequest
                    Dim ociid As Integer
                    Dim objActionMonitor = New GSSupport.ActionMonitor
                    Dim objActionUpdateOrder = New GSSupport.ActionUpdateOrder

                    Dim SessionTrackingNumber As New String("")
                    Dim OrderTrackingNumber As New String("")
                    Dim DestinationTrackingNumber As New String("")
                    Dim BuyerUserID As Integer
                    Dim ChangeAccepted As Boolean = False
                    Dim ChangeShipped As Boolean = False
                    Dim ChangeAcceptedInt As Integer = 0
                    Dim ChangeShippedInt As Integer = 0
                    Dim ChangeETAInt As Integer = 0
                    Dim ChangeETA As Boolean = False
                    Dim ChangeItemShipped As Boolean = False
                    Dim OrderItemNumber As Integer = 0
                    Dim ETA As New Date(1900, 1, 1)
                    Dim OrderShipDate As New Date(1900, 1, 1)
                    Dim ItemShipDate As New Date(1900, 1, 1)
                    Dim OrderAcceptDate As New Date(1900, 1, 1)

                    Dim ReturnedValue As Boolean = True
                    Dim errmsg As New String("")

                    If Not dr.IsDBNull(dr.GetOrdinal("SessionTrackingNumber")) Then
                        ociid = dr.GetInt32(dr.GetOrdinal("ociid"))

                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("SessionTrackingNumber")) Then
                        SessionTrackingNumber = dr.GetInt32(dr.GetOrdinal("SessionTrackingNumber"))
                        If SessionTrackingNumber = 0 Then
                            ReturnedValue = False
                            errmsg = errmsg & "Bad SessionTrackingNumber: " & SessionTrackingNumber & vbCrLf
                        End If
                    Else
                        ReturnedValue = False
                        errmsg = errmsg & "SessionTrackingNumber is null" & vbCrLf
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("OrderTrackingNumber")) Then
                        OrderTrackingNumber = dr.GetInt32(dr.GetOrdinal("OrderTrackingNumber"))
                        If OrderTrackingNumber = 0 Then
                            ReturnedValue = False
                            errmsg = errmsg & "Bad OrderTrackingNumber: " & OrderTrackingNumber & vbCrLf
                        End If
                    Else
                        ReturnedValue = False
                        errmsg = errmsg & "OrderTrackingNumber is null" & vbCrLf
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("DestinationTrackingNumber")) Then
                        DestinationTrackingNumber = dr.GetInt32(dr.GetOrdinal("DestinationTrackingNumber"))
                        If DestinationTrackingNumber = 0 Then
                            ReturnedValue = False
                            errmsg = errmsg & "Bad DestinationTrackingNumber: " & DestinationTrackingNumber & vbCrLf

                        End If
                    Else
                        ReturnedValue = False
                        errmsg = errmsg & "DestinationTrackingNumber is null" & vbCrLf
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("ordereta")) Then
                        ETA = dr.GetDateTime(dr.GetOrdinal("ordereta"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("orderacceptdate")) Then
                        OrderAcceptDate = dr.GetDateTime(dr.GetOrdinal("orderacceptdate"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("ordershipdate")) Then
                        OrderShipDate = dr.GetDateTime(dr.GetOrdinal("ordershipdate"))
                    End If




                    ' Set Action Request Properties
                    ar.Qualify = False
                    ar.CategoryName = "Order Change"
                    ar.SubmitDate = Now()
                    ar.RequestDate = Now()
                    ar.RequestDateLocal = Now()
                    ar.RequestDateTimeZoneVBConst = ServerTimeZone
                    ar.SubmitUserID = 185
                    Dim testdate As New Date(1970, 1, 1, 1, 1, 1)


                    ar.AddFieldByDisplay("Session Tracking Number", CStr(SessionTrackingNumber))
                    ar.AddFieldByDisplay("Order Tracking Number", CStr(OrderTrackingNumber))
                    ar.AddFieldByDisplay("Destination Tracking Number", CStr(DestinationTrackingNumber))



                    If dr.GetBoolean(dr.GetOrdinal("changeorderaccept")) = True Then
                        ChangeAccepted = True
                        '    ar.AddField("changeorderaccept", "True", "True")
                    End If

                    If dr.GetBoolean(dr.GetOrdinal("changeordership")) = True Then
                        ChangeShipped = True
                        '    ar.AddField("changeorderaccept", "True", "True")
                    End If

                    '   If ETA > testdate AndAlso (dr.IsDBNull(dr.GetOrdinal("oldeta")) OrElse dr.GetDateTime(dr.GetOrdinal("oldeta")).Day <> ETA.Day OrElse dr.GetDateTime(dr.GetOrdinal("oldeta")).Month <> ETA.Month OrElse dr.GetDateTime(dr.GetOrdinal("oldeta")).Year <> ETA.Year) Then
                    If dr.GetBoolean(dr.GetOrdinal("changeeta")) = True Then
                        ChangeETA = True
                        '    ar.AddField("changeordereta", "True", "True")
                    End If

                    If Not IsNothing(OrderAcceptDate) AndAlso IsDate(OrderAcceptDate) AndAlso OrderAcceptDate > testdate Then
                        ar.AddFieldByDisplay("Order Accept Date", MonthName(OrderAcceptDate.Month) & " " & OrderAcceptDate.Day & ", " & OrderAcceptDate.Year)
                    End If

                    If Not IsNothing(ETA) AndAlso IsDate(ETA) AndAlso ETA > testdate Then
                        ar.AddFieldByDisplay("Estimated Ship Date", MonthName(ETA.Month) & " " & ETA.Day & ", " & ETA.Year)
                    End If

                    If Not IsNothing(OrderShipDate) AndAlso IsDate(OrderShipDate) AndAlso OrderShipDate > testdate Then
                        ar.AddFieldByDisplay("Order Ship Date", MonthName(OrderShipDate.Month) & " " & OrderShipDate.Day & ", " & OrderShipDate.Year)
                    End If

                    ' Get order properties from DB
                    Dim sSQL As String = "select c.email, cur.currencySymbol, ordercreatedbyuserid, u2.username as ordercreatedbyusername, c2.email as ordercreatedbyuseremail, tblOrders.customerid, orderdate, co.countryname, tblCustomers.parentcompanyid, parentcompanyname, orderedbyuserid, orderdestinationcountryid, " & _
                " orderdestinationcurrencyid, tblOrders.orderstatusid, os.orderstatusname, orderpo, totalordervalue, totalordervalueusd, u.username " & _
                " from tblOrders, tblCustomers, tblContact c, tblCurrencies cur, tblCountries co, tblUsers u, tblOrderStatus os, tblParentCompanies, tblUsers u2, tblContact c2 where sessiontrackingnumber = " & SessionTrackingNumber & " and " & _
                " ordertrackingnumber = " & OrderTrackingNumber & " and tblOrders.CustomerID = tblCustomers.CustomerID and u2.userid =* tblOrders.ordercreatedbyuserid " & _
                " and c2.userid =* tblOrders.ordercreatedbyuserid and c.userid =* orderedbyuserid and cur.currencyid = orderdestinationcurrencyid and co.countryid = tblOrders.orderdestinationcountryid " & _
                " and u.userid = orderedbyuserid and os.orderstatusid = tblOrders.orderstatusid and tblCustomers.parentcompanyid = tblParentCompanies.parentcompanyid"

                    Cmd2.CommandText = sSQL
                    dr2 = Cmd2.ExecuteReader

                    While dr2.Read()
                        If Not dr2.IsDBNull(dr2.GetOrdinal("orderstatusid")) Then
                            '      ar.AddField("orderstatusid", dr2.GetInt32(dr2.GetOrdinal("orderstatusid")), dr2.GetString(dr2.GetOrdinal("orderstatusname")))
                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("orderedbyuserid")) Then
                            ar.AddFieldByDisplay("Ordered By User", dr2.GetInt32(dr2.GetOrdinal("orderedbyuserid")), dr2.GetString(dr2.GetOrdinal("username")))

                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("email")) Then
                            ar.AddFieldByDisplay("Ordered By User Email", dr2.GetString(dr2.GetOrdinal("email")))
                        End If


                        If Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyuseremail")) Then
                            ar.AddFieldByDisplay("Email of Order Creator", dr2.GetString(dr2.GetOrdinal("ordercreatedbyuseremail")))
                        ElseIf Not dr2.IsDBNull(dr2.GetOrdinal("email")) Then
                            ar.AddFieldByDisplay("Email of Order Creator", dr2.GetString(dr2.GetOrdinal("email")))

                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyusername")) Then
                            ar.AddFieldByDisplay("Order Created By User", dr2.GetInt32(dr2.GetOrdinal("ordercreatedbyuserid")), dr2.GetString(dr2.GetOrdinal("ordercreatedbyusername")))
                        ElseIf Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr2.IsDBNull(dr2.GetOrdinal("orderedbyuserid")) AndAlso Not dr2.IsDBNull(dr2.GetOrdinal("username")) Then
                            ar.AddFieldByDisplay("Order Created By User", dr2.GetInt32(dr2.GetOrdinal("orderedbyuserid")), dr2.GetString(dr2.GetOrdinal("username")))


                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("customerid")) Then
                            '      ar.AddField("customerid", dr2.GetInt32(dr2.GetOrdinal("customerid")), dr2.GetString(dr2.GetOrdinal("parentcompanyname")))
                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("parentcompanyid")) Then
                            '  ar.AddField("companyid", dr2.GetInt32(dr2.GetOrdinal("parentcompanyid")), dr2.GetString(dr2.GetOrdinal("parentcompanyname")))
                            ar.CompanyID = dr2.GetInt32(dr2.GetOrdinal("parentcompanyid"))

                        End If



                        If Not dr2.IsDBNull(dr2.GetOrdinal("orderdate")) Then
                            ar.AddFieldByDisplay("Order Date", dr2.GetDateTime(dr2.GetOrdinal("orderdate")))

                        End If
                        If Not dr2.IsDBNull(dr2.GetOrdinal("orderdestinationcountryid")) Then
                            ar.AddFieldByDisplay("Order Destination Country", dr2.GetInt32(dr2.GetOrdinal("orderdestinationcountryid")), dr2.GetString(dr2.GetOrdinal("countryname")))
                        End If
                        If Not dr2.IsDBNull(dr2.GetOrdinal("orderdestinationcurrencyid")) Then
                            ar.AddFieldByDisplay("Order Destination Currency", dr2.GetInt32(dr2.GetOrdinal("orderdestinationcurrencyid")), dr2.GetString(dr2.GetOrdinal("currencySymbol")))
                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("totalordervalue")) Then
                            ar.AddFieldByDisplay("Total Order Value", dr2.GetDecimal(dr2.GetOrdinal("totalordervalue")))
                        End If

                        If Not dr2.IsDBNull(dr2.GetOrdinal("totalordervalueusd")) Then
                            ar.AddFieldByDisplay("Total Order Value USD", dr2.GetDecimal(dr2.GetOrdinal("totalordervalueusd")))
                        End If

                        Dim textval As String

                        If ChangeAccepted Then
                            ChangeAcceptedInt = 1
                            ar.AddFieldByDisplay("Order Accept Date Changed", "true", "true")

                            textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr2.GetInt32(dr2.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                            " tblPreference where upper(preferenceName)='NOTIFYACCEPTED')")
                            If textval = "1" Or textval.ToUpper = "TRUE" Then
                                ar.AddFieldByDisplay("Notify Buyer - Order Acceptance", "true", "true")
                            End If
                            If Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyusername")) Then
                                textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr2.GetInt32(dr2.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
            " tblPreference where upper(preferenceName)='NOTIFYACCEPTED')")
                                If textval = "1" Or textval.ToUpper = "TRUE" Then
                                    ar.AddFieldByDisplay("Notify Order Creator - Order Acceptance", "true", "true")
                                End If
                                '    Else
                                '       ar.AddField("ordercreatornotifyaccepted", "true", "true")

                            End If

                        End If


                        If ChangeShipped Then
                            ChangeShippedInt = 1

                            ar.AddFieldByDisplay("Order Ship Date Changed", "true", "true")

                            textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr2.GetInt32(dr2.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                            " tblPreference where upper(preferenceName)='NOTIFYORDERSHIP')")

                            If textval = "1" Or textval.ToUpper = "TRUE" Then
                                ar.AddFieldByDisplay("Notify Buyer - Order Ship", "true", "true")
                            End If
                            If Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyusername")) Then


                                textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr2.GetInt32(dr2.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
            " tblPreference where upper(preferenceName)='NOTIFYORDERSHIP')")

                                If textval = "1" Or textval.ToUpper = "TRUE" Then
                                    ar.AddFieldByDisplay("Notify Order Creator - Order Ship", "true", "true")
                                End If
                                ' Else
                                '    ar.AddField("ordercreatornotifyordership", "true", "true")

                            End If
                        End If

                        ' Do they want to be notified if ETA changes?
                        Dim notifiedETAChange As Boolean = False

                        If ChangeETA Then
                            ChangeETAInt = 1
                            ar.AddFieldByDisplay("Order ETA Changed", "true", "true")
                            textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr2.GetInt32(dr2.GetOrdinal("orderedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
                            " tblPreference where upper(preferenceName)='NOTIFYETACHANGE')")
                            If textval = "1" Or textval.ToUpper = "TRUE" Then
                                ar.AddFieldByDisplay("Notify Buyer - ETA Change", "true", "true")
                                notifiedETAChange = True
                            End If
                            If Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyuserid")) AndAlso Not dr2.IsDBNull(dr2.GetOrdinal("ordercreatedbyusername")) Then
                                textval = GSSupportManager.GetSingleDBValueString("select preferencevalue from tblUserPreference where userid=" & dr2.GetInt32(dr2.GetOrdinal("ordercreatedbyuserid")) & " and preferenceID = (select isNull(preferenceid, 0) from " & _
            " tblPreference where upper(preferenceName)='NOTIFYETACHANGE')")

                                If textval = "1" Or textval.ToUpper = "TRUE" Then
                                    ar.AddFieldByDisplay("Notify Order Creator  - ETA Change", "true", "true")
                                    notifiedETAChange = True
                                End If
                                '  Else
                                '     ar.AddField("ordercreatornotifyeta", "true", "true")

                            End If

                        End If


                    End While

                    dr2.Close()
                    objActionUpdateOrder.SessionTrackingNumber = SessionTrackingNumber
                    objActionUpdateOrder.OrderTrackingNumber = OrderTrackingNumber
                    objActionUpdateOrder.DestinationTrackingNumber = DestinationTrackingNumber
                    objActionUpdateOrder.ChangeAccepted = ChangeAccepted
                    objActionUpdateOrder.ChangeShipped = ChangeShipped
                    objActionUpdateOrder.ChangeETA = ChangeETA
                    objActionUpdateOrder.ETA = ETA
                    objActionUpdateOrder.Manager = manager

                    objActionMonitor.Action = objActionUpdateOrder
                    ar.CompletedAction = objActionMonitor


                    If Not ar.ARUpdateDB() Then
                        ' EventLog.WriteEntry("ERROR: Unable to update DB with OrderEventAR")
                    End If


                    ' ar.GSListener = Me.m_gsListener
                    Dim am As ActionMonitor = ar.CompletedAction
                    ar.Manager = manager

                    am.Manager = manager
                    '  am.GSListener = Me.m_gsListener
                    am.ActionRequest = ar

                    am.FireActionEvent()

                    ' GSSupportManager.LogARDetails(ar)

                    ' Remove 
                    Dim MoveDataSQL As String
                    Dim DeleteSQL As String

                    DeleteSQL = "delete from tblOrderChangeIncoming where ociid=" & ociid

                    Cmd2.CommandText = DeleteSQL
                    Cmd2.ExecuteNonQuery()

                    If ReturnedValue Then
                        ' EverythingOK
                        sSQL = "insert into tblOrderChangeProcessed (sessiontrackingnumber, ordertrackingnumber, destinationtrackingnumber," & _
                    " changeorderaccept, changeordership, changeeta, ordereta, ordershipdate, orderacceptdate) " & _
                     "values (" & SessionTrackingNumber & "," & OrderTrackingNumber & "," & DestinationTrackingNumber & "," & _
                     ChangeAcceptedInt & "," & ChangeShippedInt & "," & ChangeETAInt & ",'" & ETA & "','" & OrderShipDate & "','" & OrderAcceptDate & "')"
                    Else
                        'Problem... move to error log
                        sSQL = "insert into tblOrderChangeIncomingErrs (sessiontrackingnumber, ordertrackingnumber, destinationtrackingnumber," & _
              " changeorderaccept, changeordership, changeeta, ordereta, ordershipdate, orderacceptdate, errstring) " & _
               "values (" & SessionTrackingNumber & "," & OrderTrackingNumber & "," & DestinationTrackingNumber & "," & _
               ChangeAcceptedInt & "," & ChangeShippedInt & "," & ChangeETAInt & ",'" & ETA & "','" & OrderShipDate & "','" & OrderAcceptDate & "','" & errmsg & "')"

                    End If

                    Cmd2 = New SqlCommand
                    Cmd2.Connection = Conn2

                    Cmd2.CommandText = sSQL
                    Cmd2.ExecuteNonQuery()
                    '  dr.Close()
                End While


            Catch e As Exception
                'EventLog.WriteEntry("Error listening for order change " & e.Message)
            Finally

                Conn.Close()
                Conn2.Close()
            End Try
            Thread.CurrentThread.Sleep(120000)

        Loop

    End Sub

    ' ----------  ARPATECH  ----------
    ' this function replaces spaces 
    ' with ' " , " Tab CrLf New Line
    ' ----------  ARPATECH  ----------
    Public Function stringSaturate(ByVal sString As String) As String
        If sString <> "" Then
            sString = Trim(sString)
            sString = Replace(sString, "  ", " ")
            sString = Replace(sString, "'", " ")
            sString = Replace(sString, """", " ")
            sString = Replace(sString, ",", " ")
            sString = Replace(sString, vbTab, "  ")
            sString = Replace(sString, vbCrLf, " ")
            sString = Replace(sString, vbNewLine, " ")
            Return sString
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' this function sends email
    ' by using ActionEmail Class
    ' ----------  ARPATECH  ----------
    Sub SendMail(ByVal sendTo As String, ByVal sendFrom As String, ByVal Emailbody As String, ByVal EmailSubject As String, Optional ByVal priority As Integer = 1)
        Dim ae As New ActionEmail
        EventLog.WriteEntry("Application", "arg is " & Emailbody)
        ae.SendTo = sendTo
        ae.From = sendFrom
        ae.Body = Emailbody
        ae.Priority = priority
        ae.Subject = EmailSubject
        EventLog.WriteEntry("Application", "body is " & ae.Body)
        ae.Execute()
    End Sub

    ' ----------  ARPATECH  ----------
    ' this function executes specific 
    ' sub routine of any object
    ' ----------  ARPATECH  ----------
    Public Function CallAgent(ByVal IDValue As Integer, ByVal LaunchMethod As String)
        Try

            Dim strResults As New String("")
            Dim psCmd As New ProcessStartInfo
            psCmd.FileName = Environ("ComSpec")
            '     EventLog.WriteEntry("ID is " & IDValue & ", method is " & LaunchMethod)
            psCmd.Arguments = "/c  runagent " & GSSupportManager.GetAgentPath & " -m" & LaunchMethod & " -p" & IDValue
            psCmd.RedirectStandardInput = False
            psCmd.RedirectStandardOutput = True

            psCmd.UseShellExecute = False
            Dim procCmd = Process.Start(psCmd)  'Launch Process

            'Read in Results & Write to current cmd session
            strResults = procCmd.StandardOUtput.ReadToEnd

            procCmd.WaitForExit()
        Catch e As Exception
            'EventLog.WriteEntry("Error launching agent: " & e.Message)
        Finally

        End Try

    End Function

End Class


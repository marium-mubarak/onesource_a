' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains different functions for Action Request, like updating
'                       AR time to Server Time, updating SLA, reloading workflow from DB,
'                       handing Action Request etc.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System
Imports System.ServiceProcess
Imports System.Diagnostics
Imports System.Timers
Imports System.Messaging
Imports GSSupport
Imports System.Collections
Imports System.Data.SqlClient
Imports System.DateTime

Imports System.Runtime.Remoting.Lifetime
Imports System.Threading

Imports System.IO
Imports System.Text
Imports System.Runtime.Remoting
Imports System.Security.Policy

Imports Microsoft.VisualBasic
Imports System.Management


Public Class ARMessageHandler
    Public sender As System.Object
    Public e As System.Messaging.ReceiveCompletedEventArgs
    Public service As New Form1()
    Friend GSSupportService
    Friend Workflow
    Private isDebug = True
    Public TimeConverter As New GSTime.TimeConverter()


    ' ----------  ARPATECH  ----------
    ' Sub routine checks if a AR is completed
    ' or not and fires events accordingly
    ' ----------  ARPATECH  ----------
    ' Take AR and fires appropriate events
    Public Sub HandleAR()
        Try
            Dim bw As New System.Messaging.BinaryMessageFormatter()
            Dim mq1 As System.Messaging.MessageQueue = CType(sender, System.Messaging.MessageQueue)
            Dim m As System.Messaging.Message = mq1.EndReceive(e.AsyncResult)
            Dim m2 As New System.Messaging.Message()
            Dim qsent As Boolean
            Dim qname As String

            EventLog.WriteEntry("GSSupportSvc", "Message " & m.Label & "received by HandleAR.")

            Dim ar As New ActionRequest()
            ar = CType(m.Body, GSSupport.ActionRequest)
            If isDebug Then
                GSSupportManager.LogARDetails(ar)
            End If
            ar.GSListener = service.m_gsListener
            ar.Manager = service.manager
            qname = ARPostProcQ

            ' If it's a completed action fire action complete event.
            If InStr(m.Label, ActionCompleteString) > 0 Then

                Dim am As ActionMonitor
                am = ar.CompletedAction
                EventLog.WriteEntry("GSSupportSvc", "Set action monitor")


                am.ActionRequest = ar

                '         EventLog.WriteEntry("GSSupportSvc", "Set action monitor action request")

                am.Manager = service.manager
                '      EventLog.WriteEntry("GSSupportSvc", "Set service manager")

                am.FireActionEvent()


            Else

                ' Otherwise add all times for all time zones
                AddDateFields(ar)

                ' If it needs to be qualified fire an Action Request Event
                If InStr(m.Label, QualifyString) > 0 Then
                    Dim ev As New ActionRequestEventArgs()
                    ev.ActionRequest = ar
                    If IsNothing(service) Then
                        EventLog.WriteEntry("Application", "Service is Nothing")
                    End If

                    If IsNothing(service.m_gsListener) Then
                        EventLog.WriteEntry("Application", "Service.listenr is Nothing")
                    End If
                    service.m_gsListener.RaiseActionRequestEvent(ar, ev)

                    If IsNothing(ar.RequestDateLocal) Then

                        ConvertToServerTime(ar)

                    End If

                Else
                    ' Otherwise it's an action to be processed;
                    ' create thread and execute.

                    Dim aa As New ActionAgent()
                    aa.ar = ar

                    Dim MyThread As New System.Threading.Thread(AddressOf aa.ExecuteAction)
                    MyThread.Start()


                End If
            End If
            bw.Write(m2, ar)

            m2.Label = m.Label


            qsent = SendToQ(qname, m2)

            ' LogARFields(m2)
            '     EventLog.WriteEntry("Application", "qsent is " & qsent)


        Catch ex As Exception
            EventLog.WriteEntry("AR Listener Error", "Error forwarding action request:" & ex.Message)
        Finally

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' Sub routine converts AR time to
    ' Server time and save to DB
    ' ----------  ARPATECH  ----------
    Sub ConvertToServerTime(ByRef ar As ActionRequest)
        Dim tz1 As Integer
        Dim tz2 As Integer

        tz1 = service.manager.GetTimeZoneVBConst(ar.RequestDateTimeZoneID)

        ar.RequestDate = service.manager.ConvertTime(ar.RequestDateLocal, tz1, ServerTimeZone)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader


            sql = "update tblActionRequest set requestdate='" & ar.RequestDate & "' where arid=" & ar.ActionRequestID

            '    EventLog.WriteEntry("Application", "sql is " & sql)
            Cmd.CommandText = sql
            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error gettimg time zone: " & e.Message)


        Finally
            Conn.Close()

        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' Sub routine adds GSSupport fields 
    ' to supplied AR fields
    ' ----------  ARPATECH  ----------
    Sub AddDateFields(ByRef ar As ActionRequest)
        Try


            Dim i As Integer
            ' EventLog.WriteEntry("GSSupport Service", "Function time is " & TimeConverter.ConvertTime(Now(), 10, 13))

            For i = 0 To service.m_dateFields.Count - 1
                '    EventLog.WriteEntry("Application", "getkey is " & service.m_dateFields.GetKey(i) & IsNothing(service) & IsNothing(service.m_timeConverter))
                Dim f As New Field()
                f = service.m_dateFields.Item(i)
                f.FieldTextValue = service.m_timeConverter.ConvertTime(Now(), ServerTimeZone, f.VBConst)
                ar.AddField(f)
                '  ar.AddField(service.m_dateFields.GetKey(i), service.m_timeConverter.ConvertTime(Now(), ServerTimeZone, service.m_dateFields.GetByIndex(i)))
            Next


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error adding date fields: " & e.Message)
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' a timer sub routine which raises 
    ' AR request for that time and updates in DB
    ' (didn't find its call from any class)
    ' ----------  ARPATECH  ----------
    Sub SendEventAR()
        Try

            Dim ar As New ActionRequest()

            Dim args As New GSTimerEventArgs()

            ar.GenerateTicket = False
            ar.GSListener = service.m_gsListener

            ar.CategoryName = "Time"

            ar.Manager = service.manager
            ar.Qualify = False
            ar.Summary = "System generated timer event."

            ar.RequestDate = Now()
            ar.RequestDateLocal = Now()
            ar.SubmitDate = Now()

            ar.RequestDateTimeZoneVBConst = ServerTimeZone

            ar.ARUpdateDB()

            AddDateFields(ar)

            args.ActionRequest = ar

            service.m_gsListener.RaiseGSTimerEvent(ar, args)


        Catch ex As Exception
            EventLog.WriteEntry("GS Support Service", "Error raising timer event:" & ex.Message)
        Finally

        End Try

    End Sub

    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine that delete updated 
    ' workflow and reload from DB
    ' ----------  ARPATECH  ----------
    ' Delete updated workflow and reload from DB
    Public Sub UpdateWorkflow()
        Try
            Dim bw As New System.Messaging.BinaryMessageFormatter()
            Dim mq1 As System.Messaging.MessageQueue = CType(sender, System.Messaging.MessageQueue)
            Dim m As System.Messaging.Message = mq1.EndReceive(e.AsyncResult)
            Dim m2 As New System.Messaging.Message()


            Dim qsent As Boolean
            Dim qname As String
            Dim myLabel As String
            Dim tmpstr As String
            Dim myWFID As New String("")
            Dim wfid As Integer

            EventLog.WriteEntry("GSSupportSvc", "Message " & m.Label & "receivedd.")


            myLabel = m.Label

            If Mid(myLabel, 1, 3) = "WF=" Then
                myWFID = Mid(myLabel, 1, InStr(myLabel, "&"))
                tmpstr = Mid(myWFID, InStr(myWFID, "=") + 1)
                tmpstr = Mid(tmpstr, 1, Len(tmpstr) - 1)
                wfid = CInt(tmpstr)
            End If

            EventLog.WriteEntry("Application", "wfid is " & wfid)

            Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & WorkflowQ)

            Dim menum As System.Messaging.MessageEnumerator
            menum = mqq.GetMessageEnumerator()
            While menum.MoveNext()
                If InStr(menum.Current.Label, myWFID) > 0 Then
                    menum.RemoveCurrent()
                    Exit While
                End If
            End While

            If service.m_Workflows.ContainsKey(wfid) Then
                Dim oldwf As Workflow = service.m_Workflows.Item(wfid)

                service.m_Workflows.Remove(wfid)

                oldwf.Dispose()


            End If

            service.manager.LoadWorkflow(wfid, service.m_Workflows)


        Catch e As Exception
            EventLog.WriteEntry("GS Support Service", "Error updating workflow:" & e.Message)
        Finally

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' Sub routine updates an SLA
    ' but condition is not clear
    ' ----------  ARPATECH  ----------
    ' Take AR and fires appropriate events
    Public Sub UpdateSLA()
        Try
            Dim bw As New System.Messaging.BinaryMessageFormatter()
            Dim mq1 As System.Messaging.MessageQueue = CType(sender, System.Messaging.MessageQueue)
            Dim m As System.Messaging.Message = mq1.EndReceive(e.AsyncResult)
            Dim m2 As New System.Messaging.Message()

            Dim qsent As Boolean
            Dim qname As String
            Dim myLabel As String
            Dim mySLAID As New String("")

            ' EventLog.WriteEntry("GSSupportSvc", "Message " & m.Label & "received.")

            Dim sla As New SLA()
            sla = CType(m.Body, GSSupport.SLA)
            sla.GSListener = service.m_gsListener
            sla.Manager = service.manager

            myLabel = m.Label

            If Mid(myLabel, 1, 4) = "SLA=" Then
                mySLAID = Mid(myLabel, 1, InStr(myLabel, "&"))
            End If

            '      EventLog.WriteEntry("Application", "sla is " & mySLAID)

            ' Check Q and see if there's an old version...

            Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & SLAQ)

            Dim menum As System.Messaging.MessageEnumerator
            menum = mqq.GetMessageEnumerator()
            While menum.MoveNext()
                If InStr(menum.Current.Label, mySLAID) > 0 Then
                    menum.RemoveCurrent()
                    Exit While
                End If
            End While

            ' Now send updated SLA to SLA Q
            m2 = sla.CreateMQMessage
            SendToQ(SLAQ, m2)

        Catch e As Exception
            EventLog.WriteEntry("GS Support Service", "Error updating sla:" & e.Message)
        Finally

        End Try
    End Sub




End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class just reads value of a given Conjunction from DB and stores it to
'                       local variables, which can be then retrieved by some specified properties 
'                       for every column.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data
Imports System.Data.SqlClient

<Serializable()> Public Class Conjunction
    Private m_text As New String("")
    Private m_displayText As New String("")
    Private m_conjID As Integer = 0
    Private m_conjSymbol As New String("")

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' text variable
    ' ----------  ARPATECH  ----------
    Public Property Text() As String
        Get
            Return m_text
        End Get
        Set(ByVal Value As String)
            m_text = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Cojuntion ID
    ' ----------  ARPATECH  ----------
    Public Property ConjID() As Integer
        Get
            Return m_conjID
        End Get
        Set(ByVal Value As Integer)
            m_conjID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' display text variable
    ' ----------  ARPATECH  ----------
    Public Property DisplayText() As String
        Get
            Return m_displayText
        End Get
        Set(ByVal Value As String)
            m_displayText = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' cojuntion symbol
    ' ----------  ARPATECH  ----------
    Public Property ConjSymbol() As String
        Get
            Return m_conjSymbol
        End Get
        Set(ByVal Value As String)
            m_conjSymbol = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with Co junction name 
    ' argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal name As String)
        m_text = name
        If UCase(m_text) = "ELSEIF" Then
            m_displayText = "Else If"
        Else
            m_displayText = m_text
        End If
        If UCase(m_text) = "AND" Then
            m_conjSymbol = "&&"
        ElseIf UCase(m_text) = "OR" Then
            m_conjSymbol = "||"
        End If
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with Co junction ID 
    ' argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal conjid As Integer)
        Console.WriteLine("1a")
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Console.WriteLine("1ab")
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        sql = "select IsNull(conjid, 0) as conjid, IsNull(conjname,'') as conjname, isnull(conjdisplay, '') as conjdisplay from tblConjunctions where conjid=" & conjid

        Cmd.Connection = Conn
        Try
            Cmd.CommandText = sql

            Dim dr As SqlDataReader


            dr = Cmd.ExecuteReader

            If Not IsNothing(dr) Then
                While dr.Read()

                    m_conjID = dr.GetInt32(dr.GetOrdinal("conjid"))
                    m_displayText = dr.GetString(dr.GetOrdinal("conjdisplay"))
                    m_text = dr.GetString(dr.GetOrdinal("conjname"))

                End While
            End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to create conj " & conjid)
        Finally
            Conn.Close()
        End Try

    End Sub
End Class

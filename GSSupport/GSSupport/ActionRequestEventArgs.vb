' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class sets and returns arguments for each and every ActionRequest
'                       Object.
' Inherits          :   EventArgs
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionRequestEventArgs
    Inherits EventArgs
    Private m_actionRequest As ActionRequest

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Action Request Instance
    ' ----------  ARPATECH  ----------
    Property ActionRequest() As ActionRequest
        Get
            Return m_actionRequest
        End Get
        Set(ByVal Value As ActionRequest)
            m_actionRequest = Value
        End Set
    End Property

End Class

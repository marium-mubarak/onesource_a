' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is inherited by multiple classes for using its several
'                       functionalities.
' Inherits          :   ---------
' Implements        :   IAction
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text


<XmlInclude(GetType(GSSupport.ActionUpdateOrder)), _
XmlInclude(GetType(GSSupport.ActionUpdateTicket)), _
XmlInclude(GetType(GSSupport.ActionCXMLOrderRequest)), _
XmlInclude(GetType(GSSupport.ActionServiceResponseOutbound)), _
XmlInclude(GetType(GSSupport.ActionEmail)), _
XmlInclude(GetType(GSSupport.ActionEmpty)), _
Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public MustInherit Class Action
    Implements IAction
    Protected m_actionRequest As ActionRequest
    Private m_actionFields As ArrayList
    Private m_actionName As String = ""
    Private m_actionTypeID As Integer = 0
    Private m_actionTypeName As String = ""
    Private m_actionID As Int32 = 0
    Protected m_gsSupportManager As GSSupportManager
    Private m_retryCountMax As Integer = 1
    Protected m_actionInstanceID As Int32 = 0
    Private m_actionMonitor As ActionMonitor
    Private m_defaultFieldValues As New Hashtable()
    Protected m_isComplete As Boolean = False
    Protected m_isError As Boolean = False
    Protected m_errString As New String("")
    Protected m_returnField As Field
    Protected m_returnsValue As Boolean = False
    Protected m_loadFromAR As Boolean = False
    Protected m_gsListener As GSEventListener

    ' ----------  ARPATECH  ----------
    '       SAME CLASS INTERFACE
    ' ----------  ARPATECH  ----------
    Public Interface IAction

        Function GetFieldDefaultTextValue(ByVal FieldName As String) As String
        Function GetFieldDefaultTextValue(ByVal FieldSeqNum As Integer) As String
        Function GetFieldCount() As Integer
        Property ActionID() As Integer
        Property ActionName() As String
        Property ActionRequest() As ActionRequest
        Property ActionMonitor() As ActionMonitor
        Property ReturnField() As Field
        Property ReturnsValue() As Boolean

        Property ActionTypeID() As Integer
        Property ActionInstanceID() As Integer
        Property ActionTypeName() As String
        Property RetryCountMax() As Integer
        Sub Execute()
        Property Manager() As GSSupportManager

        Property IsError() As Boolean
        Property IsComplete() As Boolean
        Property ErrString() As String


    End Interface

    ' ----------  ARPATECH  ----------
    ' Property for Setting and Retrieving
    ' Action Name
    ' ----------  ARPATECH  ----------
    Public Property ActionName() As String Implements IAction.ActionName
        Get
            Return m_actionName
        End Get
        Set(ByVal Value As String)
            m_actionName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' Property for Setting and Retrieving
    ' Error String
    ' ----------  ARPATECH  ----------
    Public Property ErrString() As String Implements IAction.ErrString
        Get
            Return m_errString
        End Get
        Set(ByVal Value As String)
            m_errString = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' Property for Setting and Retrieving
    ' Action Complete Status
    ' ----------  ARPATECH  ----------
    Public Property IsComplete() As Boolean Implements IAction.IsComplete
        Get
            Return m_isComplete
        End Get
        Set(ByVal Value As Boolean)
            m_isComplete = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' of Support Manager type data
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
 Public Property Manager() As GSSupportManager Implements IAction.Manager
        Get
            Return m_gsSupportManager
        End Get
        Set(ByVal Value As GSSupportManager)
            m_gsSupportManager = Value
            If Not IsNothing(Value.GSListener) And IsNothing(m_gsListener) Then
                m_gsListener = Value.GSListener
            End If
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' of Field Type data
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
 Public Property ReturnField() As Field Implements IAction.ReturnField
        Get
            Return m_returnField
        End Get
        Set(ByVal Value As Field)
            m_returnField = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Field Value
    ' ----------  ARPATECH  ----------
    Public Property ReturnsValue() As Boolean Implements IAction.ReturnsValue
        Get
            Return m_returnsValue
        End Get
        Set(ByVal Value As Boolean)
            m_returnsValue = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Error Status
    ' ----------  ARPATECH  ----------
    Public Property IsError() As Boolean Implements IAction.IsError
        Get
            Return m_isError
        End Get
        Set(ByVal Value As Boolean)
            m_isError = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' of Action Monitor type data
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
     Public Property ActionMonitor() As ActionMonitor Implements IAction.ActionMonitor
        Get
            Return m_actionMonitor
        End Get
        Set(ByVal Value As ActionMonitor)
            m_actionMonitor = ActionMonitor
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Action ID Value
    ' ----------  ARPATECH  ----------
    Public Property ActionID() As Int32 Implements IAction.ActionID
        Get
            Return m_actionID
        End Get
        Set(ByVal Value As Int32)
            m_actionID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ActionInstance ID value
    ' ----------  ARPATECH  ----------
    Public Property ActionInstanceID() As Int32 Implements IAction.ActionInstanceID
        Get
            Return m_actionInstanceID
        End Get
        Set(ByVal Value As Int32)
            m_actionInstanceID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Actiontype ID value
    ' ----------  ARPATECH  ----------
    Public Property ActionTypeID() As Integer Implements IAction.ActionTypeID
        Get
            Return m_actionTypeID
        End Get
        Set(ByVal Value As Integer)
            m_actionTypeID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' of Action Request Type
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
       Public Property ActionRequest() As ActionRequest Implements IAction.ActionRequest
        Get
            Return m_actionRequest
        End Get
        Set(ByVal Value As ActionRequest)
            m_actionRequest = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Retry Counting Value
    ' ----------  ARPATECH  ----------
    Public Property RetryCountMax() As Integer Implements IAction.RetryCountMax
        Get
            Return m_retryCountMax
        End Get
        Set(ByVal Value As Integer)
            m_retryCountMax = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Action Type Name
    ' ----------  ARPATECH  ----------
    Public Property ActionTypeName() As String Implements IAction.ActionTypeName
        Get
            Return m_actionTypeName
        End Get
        Set(ByVal Value As String)
            m_actionTypeName = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR for setting Action ID to 0
    ' ----------  ARPATECH  ----------
    Public Sub New()
        m_actionID = 0

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR for setting Action ID to n
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal actionid As Int32)
        m_actionID = actionid

    End Sub

    ' ----------  ARPATECH  ----------
    ' Empty Function
    ' ----------  ARPATECH  ----------
    Public Function GetFieldDefaultTextValue(ByVal FieldName As String) As String Implements IAction.GetFieldDefaultTextValue

    End Function

    ' ----------  ARPATECH  ----------
    ' Empty Function
    ' ----------  ARPATECH  ----------
    Public Function GetFieldDefaultTextValue(ByVal FieldID As Integer) As String Implements IAction.GetFieldDefaultTextValue

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving field counts
    ' ----------  ARPATECH  ----------
    Public Function GetFieldCount() As Integer Implements IAction.GetFieldCount
        GetFieldCount = m_actionFields.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' executable sub routine for this class
    ' ----------  ARPATECH  ----------
    Overridable Sub Execute() Implements IAction.Execute

        EventLog.WriteEntry("Application", "Abstract execute")

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for loading dafault values
    ' ----------  ARPATECH  ----------
    Sub LoadDefaultFieldValues()

        If IsNothing(m_actionID) Or Not m_actionID > 0 Then
            Exit Sub
        End If

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString)



        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try


            Dim dr As SqlDataReader
            Cmd.CommandText = "select IsNull(f.fieldname,'') as fieldname, IsNull(f.fielddisplay,'') as fielddisplay, isnull(f.fielddatatypeid, '') as fielddatatypeid, isnull(afdv.fieldid, '') as fieldid, isnull(afdv.fieldTxtValue, '') as fieldTxtValue " & _
            " from tblActionFieldDefaultValues afdv, tblFields f where f.fieldid = afdv.fieldid and afdv.actionid=" & m_actionID


            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                Dim f As New Field()
                Dim tmpTxtValue As New String("")
                f.FieldID = dr.GetInt32(dr.GetOrdinal("fieldid"))
                f.DataType = dr.GetInt32(dr.GetOrdinal("fieldDataTypeID"))
                tmpTxtValue = dr.GetString(dr.GetOrdinal("fieldTxtValue"))
                f.FieldDisplayName = dr.GetString(dr.GetOrdinal("fieldDisplay"))
                f.FieldName = dr.GetString(dr.GetOrdinal("fieldName"))

                m_defaultFieldValues.Add(f.FieldID, f)

                If InStr(tmpTxtValue, FieldReplaceDelim) > 0 Then
                    f.FieldTextValue = GSSupportManager.ParseTextAR(m_actionRequest, tmpTxtValue)
                Else
                    f.FieldTextValue = tmpTxtValue
                End If

                Dim myPropertyInfo As PropertyInfo()
                myPropertyInfo = Me.GetType().GetProperties()

                Dim i As Integer

                ' Here I set any property values of the action matching the field name...

                For i = 0 To myPropertyInfo.Length - 1
                    '  EventLog.WriteEntry("Application", "Property name is " & UCase(myPropertyInfo(i).Name) & ", db fieldname is " & UCase(f.FieldName))
                    If UCase(myPropertyInfo(i).Name) = UCase(f.FieldName) Then
                        '   EventLog.WriteEntry("Application", "Is a match, setting value to " & GetValText(f.FieldNativeValue))
                        CallByName(Me, myPropertyInfo(i).Name, CallType.Set, f.FieldNativeValue)
                        Exit For
                    End If



                Next



            End While

            dr.Close()



        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading default action fields: " & e.Message)
        Finally

            Conn.Close()

            Conn = Nothing

        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' Empty Sub Routine
    ' ----------  ARPATECH  ----------
    Overridable Sub FireActionEvent()

    End Sub


End Class

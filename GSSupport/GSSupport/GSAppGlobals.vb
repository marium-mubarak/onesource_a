' ****************** ARPATECH (General Description) ******************
' Program Type      :   Module
' Description       :   This module contains all common functions which are being used by other
'                       object like opening data connection, validating data type and numeric
'                       values etc.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System
Imports System.Data
Imports System.Windows.Forms
Imports System.Messaging
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Diagnostics
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text


<System.Runtime.InteropServices.ComVisible(True)> Public Interface IMQUtils

    Function CreateMQMessage() As MSMQMessage


End Interface


Module GSAppGlobals

    ' Public Const DBString = "user id=sa;password=marvin2;database=globalserve;server=10.10.20.148"
    'Public Const QMachine = "gsnetpweb4"

    ' Public Const DBString = "user id=sa;password=marvin2;database=globalserve;server=10.10.20.146"
    ' Public Const QMachine = "gstlcmwebqa1"

    ' Public Const DBString = "user id=sa;password=marvin2;database=globalserve;server=10.0.0.42"
    ' Public Const QMachine = "gstlpweb1"

    Public Const RegistryPath = "Software\GlobalServe\GSSupport"
    Public Const IncomingARQ = "\private$\IncomingAR"
    Public Const IncomingWorkflowQ = "\private$\IncomingWorkflow"
    Public Const IncomingSLAQ = "\private$\IncomingSLA"
    Public Const ARPostProcQ = "\private$\ARPostProc"
    Public Const TicketEventQ = "\private$\TicketEvent"
    Public Const QualificationQ = "\private$\Qualification"
    Public Const ActionPreProcQ = "\private$\ActionPreProc"
    Public Const ActionPostProcQ = "\private$\ActionPostProc"
    Public Const ActionCompleteQ = "\private$\ActionComplete"
    Public Const WorkflowQ = "\private$\Workflow"
    Public Const WorkflowInProgressQ = "\private$\WorkflowInProgress"
    Public Const WorkflowPostProcQ = "\private$\WorkflowPostProc"
    Public Const SLAQ = "\private$\SLA"
    Public Const SLAInProgressQ = "\private$\SLAInProgress"
    Public Const OrderEventQ = "\private$\OrderEvent"
    Public Const ScheduleQ = "\private$\Schedule"
    Public Const MQQualifyMax = 1000
    Public Const IsDebug = True
    Public Const QualifyString = "&GSQUAL=1"
    Public Const TicketEventString = "&TICKETEVENT=1"
    Public Const ActionCompleteString = "&ACTIONCOMPLETE=1"
    Public Const OrderEventString = "&ORDEREVENT=1"
    Public Const vbString = 8
    Public Const vbGSExpression = 44
    ' Public Const GSDefaultSMTP = "mail.global-serve.com"
    ' Public Const GSDefaultSMTP = "10.0.0.81"

    Public Const FieldReplaceDelim = "##"
    Public Const MinYear As Integer = 1970
    Public Const ServerTimeZone As Integer = 10
    Public Const vbJanuary = 1
    Public Const vbFebruary = 2
    Public Const vbMarch = 3
    Public Const vbApril = 4
    Public Const vbMay = 5
    Public Const vbJune = 6
    Public Const vbJuly = 7
    Public Const vbAugust = 8
    Public Const vbSeptember = 9
    Public Const vbOctober = 10
    Public Const vbNovember = 11
    Public Const vbDecember = 12
    Public Const DefaultEmail As String = "Automailer@global-serve.com"

    ' ----------  ARPATECH  ----------
    ' function to open a record set 
    ' according to the provided sql query
    ' and return data reader type
    ' ----------  ARPATECH  ----------
    Function c_OpenRecordset(ByVal sql As String) As SqlDataReader
        Dim Conn As SqlConnection = New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim catCMD As SqlCommand = Conn.CreateCommand()
        catCMD.CommandText = sql

        Dim myReader As SqlDataReader = catCMD.ExecuteReader()
        Do While myReader.Read()
            MessageBox.Show(myReader.GetString(0))
        Loop

        myReader.Close()
        Conn.Close()

        c_OpenRecordset = myReader
        Conn = Nothing
        catCMD = Nothing

    End Function

    ' ----------  ARPATECH  ----------
    ' function to send the message to
    ' queue and returns success status
    ' ----------  ARPATECH  ----------
    Function SendToQ(ByVal qpath As String, ByVal mes As MSMQMessage) As Boolean

        Try
            Dim strFormatName As String
            Dim qinfo As New MSMQQueueInfo()
            Dim qDest As MSMQQueue

            ' Create a direct format name.
            strFormatName = "DIRECT=OS:" & GSSupportManager.GetQHost() & qpath
            '  EventLog.WriteEntry("Application", strFormatName)

            ' Set the FormatName property of the MSMQQueueInfo object.

            qinfo.FormatName = strFormatName

            ' Open the queue.
            qDest = qinfo.Open(MQ_SEND_ACCESS, MQ_DENY_NONE)

            ' Send the message and close the queue.
            mes.Send(DestinationQueue:=qDest)

            qDest.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Sending to Q:" & e.Message)
            Return False
        End Try

        Return True

    End Function

    ' ----------  ARPATECH  ----------
    ' subroutine that does nothing
    ' ----------  ARPATECH  ----------
    Sub LogEvent(ByVal src As String, ByVal msg As String)
        '     Dim el As New EventLog()

        '   If (Not EventLog.SourceExists(src)) Then
        '   EventLog.CreateEventSource(src, "GSSupport")
        '  End If

        ' el.Source = "Application"


        ' el.WriteEntry(msg)
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for logging events
    ' to database
    ' ----------  ARPATECH  ----------
    Sub LogEventDB(ByVal src As String, ByVal msg As String)
        Dim el As New EventLog()

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        '   Cmd.Transaction = myTrans


        Cmd.CommandText = "insert into tblEventLog (sourcename, comments) values ('" & src & _
        "', " & msg & ")"

        Cmd.ExecuteNonQuery()

    End Sub

    ' ----------  ARPATECH  ----------
    ' function to retrive datatype of 
    ' any passed object
    ' ----------  ARPATECH  ----------
    Function GetDataType(ByVal o As Object) As Integer
        If o.GetType.ToString = "System.Int32" Or o.GetType.ToString = "System.Int16" Or o.GetType.ToString = "System.Int64" Or o.GetType.Equals(New Integer()) Then
            Return vbInteger
        ElseIf o.GetType.ToString = "System.String" Or o.GetType.Equals(New String("")) Then
            Return vbString
        ElseIf o.GetType.ToString = "System.Boolean" Or o.GetType.Equals(New Boolean()) Then
            Return vbBoolean
        ElseIf o.GetType.ToString = "System.Date" Or UCase(o.GetType.ToString) = "SYSTEM.DATETIME" Or o.GetType.Equals(New Date()) Or o.GetType.Equals(New DateTime()) Then
            Return vbDate
        ElseIf o.GetType.ToString = "GSSupport.Expression" Or o.GetType.Equals(New Expression()) Then

            Return vbGSExpression
        Else
            Return 0
        End If
    End Function

    ' ----------  ARPATECH  ----------
    ' function to retrive the string 
    ' value of any passed object
    ' ----------  ARPATECH  ----------
    Function GetValText(ByVal o As Object) As String
        Dim i As Integer
        Dim n As Integer
        i = GetDataType(o)
        '   MessageBox.Show("GetValText with " & o.GetType.ToString & " of type " & i)

        If IsNumber(i) Then
            Return CDbl(o).ToString
        ElseIf i = vbDate Then
            Return CDate(o).ToString
        ElseIf i = vbBoolean Then
            Return CBool(o).ToString
        ElseIf i = vbGSExpression Then
            Dim e As New Expression()
            e = o
            Dim s As New String("")
            For n = 0 To e.SubExpressions.Count - 1
                Dim eSub As New Expression()
                If Not IsNothing(e.SubExpressions.Item(n)) Then
                    eSub = e.SubExpressions.Item(n)
                    s = s & eSub.GetText
                End If

            Next
            Return s
        End If
        Return CStr(o)
    End Function

    ' ----------  ARPATECH  ----------
    ' function to varify a datatype as number
    ' ----------  ARPATECH  ----------
    Function IsNumber(ByVal dataType As Integer) As Boolean
        If dataType = vbInteger Or dataType = vbLong Or dataType = vbDouble Then
            Return True
        Else
            Return False
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' function to convert passed object
    ' of Action Request type to XML
    ' ----------  ARPATECH  ----------
    Public Function ObjToXML(ByVal obj As ActionRequest) As String
        '  EventLog.WriteEntry("Application", "Type is " & obj.GetType.ToString)
        Dim serializer As New XmlSerializer(obj.GetType())
        Dim objxml As New String("")
        Try
            Dim ns As New XmlSerializerNamespaces()


            ns.Add("GSSupport", "https://onesource.global-serve.com")

            Dim writer As New XmlTextWriter(objxml, New UTF8Encoding())
            ' Serialize using the XmlTextWriter.
            serializer.Serialize(writer, obj, ns)
            writer.Close()
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error transorming object to xml:" & e.Message)
        End Try
        Return objxml

    End Function



End Module

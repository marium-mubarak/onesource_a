' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains all arguments for Ticketing Event within its properties
'                       and it contains a function also for creating Action Request on basis of
'                       these arguments.
' Inherits          :   EventArgs
' Implements        :   ITicketEventArgs
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System
Imports System.ComponentModel
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface ITicketEventArgs
    Property IsStatusChange() As Boolean
    Property IsPriorityChange() As Boolean
    Property IsEscalationChange() As Boolean
    Property IsTypeChange() As Boolean
    Property IsFSCAssigned() As Boolean
    Property IsCSRAssigned() As Boolean
    Property GSTicketID() As Integer
    Property NewStatusID() As Integer
    Property NewPriorityID() As Integer
    Property NewFSCID() As Integer
    Property TicketContactPhone() As String
    Property TicketContactName() As String
    Property NewEscalationLevel() As Integer
    Property AssignedCSRID() As Integer
    Property AssignedFSCID() As Integer
    Property AssignedCSRName() As String
    Property AssignedFSCName() As String
    Property NewCSRID() As Integer
    Function CreateAR() As ActionRequest
    Property ActionRequestID() As Integer
    Property NewTypeID() As Integer
    Property TimerStopOverride() As Boolean
    Property TimerStartOverride() As Boolean
    Property TimerStopOverrideTACID() As Integer
    Property TimerStartOverrideTACID() As Integer
    Property UpdateUserName() As String
    Property UpdateUserID() As Integer
    Property ActionInstanceID() As Integer
    Property IsException() As Boolean
    Property IsBreach() As Boolean
    Property IsClosedWithinSLA() As Boolean
    Property MinutesToBreach() As Integer
    Property OpenSLA() As Integer
    Property TimeZoneID() As Integer
    Property Statuses() As ArrayList
    Property StatusTargets() As ArrayList
    Property StatusActuals() As ArrayList
    Property IsAddComment() As Boolean
    Property StatusTargetChangedDates() As ArrayList
    Property StatusActualChangedDates() As ArrayList
    Property IsNewAppt() As Boolean
    Property ApptDate() As Date
    Property CustomerTicketID() As String
    Property SFATicketID() As String
    Property AssetSerial() As String
    Property AddressText() As String
    Property CSREmail() As String
    Property SubmittingUserPhone() As String
    Property AffectedContactPhone() As String








End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class TicketEventArgs
    Inherits EventArgs
    Implements ITicketEventArgs

    Private m_emailAssignedCSR As String
    Private m_updateUserName As String
    Private m_statusChange As Boolean = False
    Private m_priorityChange As Boolean = False
    Private m_escalationChange As Boolean = False
    Private m_typeChange As Boolean = False
    Private m_fscAssigned As Boolean = False
    Private m_csrAssigned As Boolean = False
    Private m_GSTicketID As Integer
    Private m_newStatusID As Integer
    Private m_newPriorityID As Integer
    Private m_newEscalationLevel As Integer
    Private m_newFSCID As Integer
    Private m_newCSRID As Integer
    Private m_companyID As Integer
    Private m_companyName As String
    Private m_affectedContactID As Integer
    Private m_affectedContactName As String
    Private m_csrID As Integer
    Private m_csrName As String
    Private m_csrEmail As String
    Private m_sfaid As Integer
    Private m_sfaname As String
    Private m_submittedByUserID As Integer
    Private m_submittedByUserName As String
    Private m_requestDateLocal As Date
    Private m_timeZoneName As String = ""
    Private m_statusID As Integer
    Private m_statusName As String
    Private m_priorityID As Integer
    Private m_priorityName As String
    Private m_escalationLevel As Integer
    Private m_actionRequestID As Integer
    Private m_addressID As Integer
    Private m_countryID As Integer
    Private m_typeID As Integer
    Private m_typeName As String
    Private m_timerStopOverride As Boolean = False
    Private m_timerStartOverride As Boolean = False
    Private m_timerStopOverrideTACID As Integer = 0
    Private m_timerStartOverrideTACID As Integer = 0
    Private m_actionInstanceID As Integer = 0
    Private m_isBreach As Boolean = False
    Private m_isClosedWithinSLA As Boolean = False
    Private m_isException As Boolean = False
    Private m_minutesToBreach As Integer = -1
    Private m_openSLA As Integer = -1
    Private m_timeZoneID As Integer = -1
    Private m_summary As String = ""
    Private m_desc As String = ""
    Private m_statuses As New ArrayList()
    Private m_statusTargets As New ArrayList()
    Private m_statusActuals As New ArrayList()
    Private m_isAddComment As Boolean = False
    Private m_statusTargetChangedDates As New ArrayList()
    Private m_statusActualChangedDates As New ArrayList()
    Private m_isNewAppt As Boolean = False
    Private m_apptDate As Date
    Private m_sfaTicketID As String = New String("")
    Private m_customerTicketID As String = New String("")
    Private m_assetSerial As New String("")
    Private m_addressText As New String("")
    Private m_submittingUserPhone As New String("")
    Private m_affectedContactPhone As New String("")
    Private m_updateUserID As Integer

    Private m_ticketContactPhone As New String("")
    Private m_ticketContactName As New String("")



    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with ticket ID
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal ticketid As Integer)
        Me.GSTicketID = ticketid
    End Sub

    ' ----------  ARPATECH  ----------
    ' EMAPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' custoemr's ticket ID
    ' ----------  ARPATECH  ----------
    Property CustomerTicketID() As String Implements ITicketEventArgs.CustomerTicketID
        Get
            Return m_customerTicketID
        End Get
        Set(ByVal Value As String)
            m_customerTicketID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' asset serial as string
    ' ----------  ARPATECH  ----------
    Property AssetSerial() As String Implements ITicketEventArgs.AssetSerial
        Get
            Return m_assetSerial
        End Get
        Set(ByVal Value As String)
            m_assetSerial = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' contacting phone of ticket
    ' ----------  ARPATECH  ----------
    Property TicketContactPhone() As String Implements ITicketEventArgs.TicketContactPhone
        Get
            Return m_ticketContactPhone
        End Get
        Set(ByVal Value As String)
            m_ticketContactPhone = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' contacting name of ticket
    ' ----------  ARPATECH  ----------
    Property TicketContactName() As String Implements ITicketEventArgs.TicketContactName
        Get
            Return m_ticketContactName
        End Get
        Set(ByVal Value As String)
            m_ticketContactName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' address
    ' ----------  ARPATECH  ----------
    Property AddressText() As String Implements ITicketEventArgs.AddressText
        Get
            Return m_addressText
        End Get
        Set(ByVal Value As String)
            m_addressText = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' submitting user's phone number
    ' ----------  ARPATECH  ----------
    Property SubmittingUserPhone() As String Implements ITicketEventArgs.SubmittingUserPhone
        Get
            Return m_submittingUserPhone
        End Get
        Set(ByVal Value As String)
            m_submittingUserPhone = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' affected user phone number
    ' ----------  ARPATECH  ----------
    Property AffectedContactPhone() As String Implements ITicketEventArgs.AffectedContactPhone
        Get
            Return m_affectedContactPhone
        End Get
        Set(ByVal Value As String)
            m_affectedContactPhone = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SFA ID
    ' ----------  ARPATECH  ----------
    Property SFATicketID() As String Implements ITicketEventArgs.SFATicketID
        Get
            Return m_sfaTicketID
        End Get
        Set(ByVal Value As String)
            m_sfaTicketID = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS ticket ID and also loads
    ' defaul value based on that ticket
    ' ----------  ARPATECH  ----------
    Public Property GSTicketID() As Integer Implements ITicketEventArgs.GSTicketID
        Get
            Return m_GSTicketID
        End Get
        Set(ByVal Value As Integer)
            m_GSTicketID = Value
            ' Get ticket details from DB
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

            Conn.Open()

            Dim Cmd As New SqlCommand()


            Try
                Dim dr As SqlDataReader
                Cmd.Connection = Conn
                Cmd.CommandText = "select Isnull(t.sfaid, 0) as sfaid, t.AppointmentDate, art.arTypeName, ar.requestdatelocal, tz.timezonename, ar.arname, ar.ardesc, t.tkttypeid, t.arid, t.escalationLevel, t.tktstatusid, ts.tktStatusName, pr.priorityName, t.priorityid, IsNull(s.sfaname, '') as sfaname, isnull(t.csrid, 0) as csrid, IsNull(c.lastname + ' ' + c.firstname, '') as csrname, " & _
                " IsNull(c.email, '') as csremail, ar.affectedusercontactid, ar.requesttimezoneid, IsNull(c2.firstname + ' ' + c2.lastname, '') as affectedusercontactname, " & _
                " ar.datereceived, IsNull(ar.addressid, 0) as addressid, Isnull(ca.countryid, 0) as countryid, ar.submittedByUserID, u.username as 'submittedbyusername'," & _
                " ar.companyid, pc.parentcompanyname from tblActionRequest ar, tblParentCompanies pc, tblPriority pr, tblTicketStatus ts, " & _
                " tblContact c, tblContact c2, tblUsers u, tblSFAs s, tblTicket t, tblCustomerAddresses ca, tblTimeZones tz, tblActionRequestType art " & _
                " where t.arid = ar.arid and ar.affectedusercontactid *= c2.contactid and t.csrid *= c.contactid " & _
                " and t.sfaid *= s.sfaid and ar.addressid *= ca.customeraddressid and ar.requesttimezoneid *= tz.timezoneid and t.tkttypeid = art.arTypeID " & _
                " and ar.companyid = pc.parentcompanyid and ts.tktstatusid = t.tktstatusid and t.priorityid = pr.priorityid and ar.submittedbyuserid = u.userid and t.ticketid = " & m_GSTicketID
                ' EventLog.WriteEntry("Application", Cmd.CommandText)

                dr = Cmd.ExecuteReader

                While dr.Read()
                    If Not dr.IsDBNull(dr.GetOrdinal("parentcompanyname")) Then
                        m_companyName = dr.GetString(dr.GetOrdinal("parentcompanyname"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("affectedusercontactname")) Then
                        m_affectedContactName = dr.GetString(dr.GetOrdinal("affectedusercontactname"))
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("affectedUserContactID")) Then
                        m_affectedContactID = dr.GetInt32(dr.GetOrdinal("affectedUserContactID"))
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("companyid")) Then
                        m_companyID = dr.GetInt32(dr.GetOrdinal("companyid"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("csrid")) Then
                        m_csrID = dr.GetInt32(dr.GetOrdinal("csrid"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("sfaid")) Then
                        m_sfaid = dr.GetInt32(dr.GetOrdinal("sfaid"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("submittedbyuserid")) Then
                        m_submittedByUserID = dr.GetInt32(dr.GetOrdinal("submittedbyuserid"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("sfaname")) Then
                        m_sfaname = dr.GetString(dr.GetOrdinal("sfaname"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("csrname")) Then
                        m_csrName = dr.GetString(dr.GetOrdinal("csrname"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("tktstatusName")) Then
                        m_statusName = dr.GetString(dr.GetOrdinal("tktstatusName"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("priorityName")) Then
                        m_priorityName = dr.GetString(dr.GetOrdinal("priorityName"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("submittedByUserName")) Then
                        m_submittedByUserName = dr.GetString(dr.GetOrdinal("submittedByUserName"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("priorityID")) Then
                        m_priorityID = dr.GetInt32(dr.GetOrdinal("priorityID"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("tktstatusID")) Then
                        m_statusID = dr.GetInt32(dr.GetOrdinal("tktstatusID"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("escalationLevel")) Then
                        m_escalationLevel = dr.GetInt32(dr.GetOrdinal("escalationLevel"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("arid")) Then
                        m_actionRequestID = dr.GetInt32(dr.GetOrdinal("arid"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("tkttypeid")) Then
                        m_typeID = dr.GetInt32(dr.GetOrdinal("tkttypeid"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("arTypeName")) Then
                        m_typeName = dr.GetString(dr.GetOrdinal("arTypeName"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("addressid")) Then
                        m_addressID = dr.GetInt32(dr.GetOrdinal("addressid"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("countryid")) Then
                        m_countryID = dr.GetInt32(dr.GetOrdinal("countryid"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("requesttimezoneid")) Then
                        m_timeZoneID = dr.GetInt32(dr.GetOrdinal("requesttimezoneid"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("csremail")) Then
                        m_csrEmail = dr.GetString(dr.GetOrdinal("csremail"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("arname")) Then
                        m_summary = dr.GetString(dr.GetOrdinal("arname"))
                    End If


                    If Not dr.IsDBNull(dr.GetOrdinal("arDesc")) Then
                        m_desc = dr.GetString(dr.GetOrdinal("arDesc"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("requestdatelocal")) Then
                        m_requestDateLocal = dr.GetDateTime(dr.GetOrdinal("requestdatelocal"))
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("timezonename")) Then
                        m_timeZoneName = dr.GetString(dr.GetOrdinal("timezonename"))
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("AppointmentDate")) AndAlso Year(dr.GetDateTime(dr.GetOrdinal("AppointmentDate"))) > 2000 Then
                        m_apptDate = dr.GetDateTime(dr.GetOrdinal("AppointmentDate"))
                    End If


                End While
                dr.Close()
            Catch e As Exception
                EventLog.WriteEntry("Application", "Error getting ticketdetails:" & e.Message)

            Finally
                Conn.Close()
            End Try


        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action instance ID
    ' ----------  ARPATECH  ----------
    Public Property ActionInstanceID() As Integer Implements ITicketEventArgs.ActionInstanceID
        Get
            Return m_actionInstanceID
        End Get
        Set(ByVal Value As Integer)
            m_actionInstanceID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' open SLA as integer
    ' ----------  ARPATECH  ----------
    Public Property OpenSLA() As Integer Implements ITicketEventArgs.OpenSLA
        Get
            Return m_openSLA
        End Get
        Set(ByVal Value As Integer)
            m_openSLA = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new appointment YES / NO
    ' ----------  ARPATECH  ----------
    Public Property IsNewAppt() As Boolean Implements ITicketEventArgs.IsNewAppt
        Get
            Return m_isNewAppt
        End Get
        Set(ByVal Value As Boolean)
            m_isNewAppt = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' appointment date
    ' ----------  ARPATECH  ----------
    Public Property ApptDate() As Date Implements ITicketEventArgs.ApptDate
        Get
            Return m_apptDate
        End Get
        Set(ByVal Value As Date)
            m_apptDate = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' stauses as arraylist
    ' ----------  ARPATECH  ----------
    Public Property Statuses() As ArrayList Implements ITicketEventArgs.Statuses
        Get
            Return m_statuses
        End Get
        Set(ByVal Value As ArrayList)
            m_statuses = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' actual staus as arraylist
    ' ----------  ARPATECH  ----------
    Public Property StatusActuals() As ArrayList Implements ITicketEventArgs.StatusActuals
        Get
            Return m_statusActuals
        End Get
        Set(ByVal Value As ArrayList)
            m_statusActuals = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' staus targets as arraylist
    ' ----------  ARPATECH  ----------
    Public Property StatusTargets() As ArrayList Implements ITicketEventArgs.StatusTargets
        Get
            Return m_statusTargets
        End Get
        Set(ByVal Value As ArrayList)
            m_statusTargets = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new priority ID
    ' ----------  ARPATECH  ----------
    Public Property NewPriorityID() As Integer Implements ITicketEventArgs.NewPriorityID
        Get
            Return m_newPriorityID
        End Get
        Set(ByVal Value As Integer)
            m_newPriorityID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' timer stop override TAC ID
    ' ----------  ARPATECH  ----------
    Public Property TimerStopOverrideTACID() As Integer Implements ITicketEventArgs.TimerStopOverrideTACID
        Get
            Return m_timerStopOverrideTACID
        End Get
        Set(ByVal Value As Integer)
            m_timerStopOverrideTACID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' timer start override TAC ID
    ' ----------  ARPATECH  ----------
    Public Property TimerStartOverrideTACID() As Integer Implements ITicketEventArgs.TimerStartOverrideTACID
        Get
            Return m_timerStartOverrideTACID
        End Get
        Set(ByVal Value As Integer)
            m_timerStartOverrideTACID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' timer stop override status
    ' ----------  ARPATECH  ----------
    Public Property TimerStopOverride() As Boolean Implements ITicketEventArgs.TimerStopOverride
        Get
            Return m_timerStopOverride
        End Get
        Set(ByVal Value As Boolean)
            m_timerStopOverride = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' timer start override status
    ' ----------  ARPATECH  ----------
    Public Property TimerStartOverride() As Boolean Implements ITicketEventArgs.TimerStartOverride
        Get
            Return m_timerStartOverride
        End Get
        Set(ByVal Value As Boolean)
            m_timerStartOverride = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action ID request
    ' ----------  ARPATECH  ----------
    Public Property ActionRequestID() As Integer Implements ITicketEventArgs.ActionRequestID
        Get
            Return m_actionRequestID
        End Get
        Set(ByVal Value As Integer)
            m_actionRequestID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' update user ID
    ' ----------  ARPATECH  ----------
    Public Property UpdateUserID() As Integer Implements ITicketEventArgs.UpdateUserID
        Get
            Return m_updateUserID
        End Get
        Set(ByVal Value As Integer)
            m_updateUserID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' assigned CSR name
    ' ----------  ARPATECH  ----------
    Public Property AssignedCSRName() As String Implements ITicketEventArgs.AssignedCSRName
        Get
            Return m_csrName
        End Get
        Set(ByVal Value As String)
            m_csrName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' assigned FSC name
    ' ----------  ARPATECH  ----------
    Public Property AssignedFSCName() As String Implements ITicketEventArgs.AssignedFSCName
        Get
            Return m_sfaname
        End Get
        Set(ByVal Value As String)
            m_sfaname = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' update user name
    ' ----------  ARPATECH  ----------
    Public Property UpdateUserName() As String Implements ITicketEventArgs.UpdateUserName
        Get
            Return m_updateUserName
        End Get
        Set(ByVal Value As String)
            m_updateUserName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' CSR email
    ' ----------  ARPATECH  ----------
    Public Property CSREmail() As String Implements ITicketEventArgs.CSREmail
        Get
            Return m_csrEmail
        End Get
        Set(ByVal Value As String)
            m_csrEmail = Value
        End Set
    End Property



    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' assisgned CSR ID
    ' ----------  ARPATECH  ----------
    Public Property AssignedCSRID() As Integer Implements ITicketEventArgs.AssignedCSRID
        Get
            Return m_csrID
        End Get
        Set(ByVal Value As Integer)
            m_csrID = Value
            Me.CSREmail = GSSupportManager.GetSingleDBValueString("select email from tblcontact where contactid=" & m_csrID)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' type new ID
    ' ----------  ARPATECH  ----------
    Public Property NewTypeID() As Integer Implements ITicketEventArgs.NewTypeID
        Get
            Return m_typeID
        End Get
        Set(ByVal Value As Integer)
            m_typeID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' assigned FSC ID
    ' ----------  ARPATECH  ----------
    Public Property AssignedFSCID() As Integer Implements ITicketEventArgs.AssignedFSCID
        Get
            Return m_sfaid
        End Get
        Set(ByVal Value As Integer)
            m_sfaid = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new esclation level
    ' ----------  ARPATECH  ----------
    Public Property NewEscalationLevel() As Integer Implements ITicketEventArgs.NewEscalationLevel
        Get
            Return m_newEscalationLevel
        End Get
        Set(ByVal Value As Integer)
            m_newEscalationLevel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' status's new ID
    ' ----------  ARPATECH  ----------
    Public Property NewStatusID() As Integer Implements ITicketEventArgs.NewStatusID
        Get
            Return m_newStatusID
        End Get
        Set(ByVal Value As Integer)
            m_newStatusID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' time zone ID
    ' ----------  ARPATECH  ----------
    Public Property TimeZoneID() As Integer Implements ITicketEventArgs.TimeZoneID
        Get
            Return m_timeZoneID
        End Get
        Set(ByVal Value As Integer)
            m_timeZoneID = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new FSC Id as integer
    ' ----------  ARPATECH  ----------
    Public Property NewFSCID() As Integer Implements ITicketEventArgs.NewFSCID
        Get
            Return m_newFSCID
        End Get
        Set(ByVal Value As Integer)
            m_newFSCID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new CSR ID
    ' ----------  ARPATECH  ----------
    Public Property NewCSRID() As Integer Implements ITicketEventArgs.NewCSRID
        Get
            Return m_newCSRID
        End Get
        Set(ByVal Value As Integer)
            m_newCSRID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' minutes to breach as integer
    ' ----------  ARPATECH  ----------
    Public Property MinutesToBreach() As Integer Implements ITicketEventArgs.MinutesToBreach
        Get
            Return m_minutesToBreach
        End Get
        Set(ByVal Value As Integer)
            m_minutesToBreach = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' esclation change or not status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsEscalationChange() As Boolean Implements ITicketEventArgs.IsEscalationChange
        Get
            Return m_escalationChange
        End Get
        Set(ByVal Value As Boolean)
            m_escalationChange = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' FSC assigned or not status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsFscAssigned() As Boolean Implements ITicketEventArgs.IsFSCAssigned
        Get

            Return m_fscAssigned
        End Get
        Set(ByVal Value As Boolean)
            Try
                '   EventLog.WriteEntry("Application", "Changing fsc assigned to " & Value)

                m_fscAssigned = Value

            Catch e As Exception
                EventLog.WriteEntry("Application", "Error fscassigned: " & e.Message)
            End Try

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' CSR assigned or not status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsCsrAssigned() As Boolean Implements ITicketEventArgs.IsCSRAssigned
        Get

            Return m_csrAssigned
        End Get
        Set(ByVal Value As Boolean)
            '   EventLog.WriteEntry("Application", "seeting csrassigned to " & Value)

            m_csrAssigned = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' type change status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsTypeChange() As Boolean Implements ITicketEventArgs.IsTypeChange
        Get
            Return m_typeChange
        End Get
        Set(ByVal Value As Boolean)
            EventLog.WriteEntry("Application", "Setting type change to " & Value)
            m_typeChange = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' priority change status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsPriorityChange() As Boolean Implements ITicketEventArgs.IsPriorityChange
        Get
            Return m_priorityChange
        End Get
        Set(ByVal Value As Boolean)
            m_priorityChange = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' "status" change status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsStatusChange() As Boolean Implements ITicketEventArgs.IsStatusChange
        Get
            Return m_statusChange
        End Get
        Set(ByVal Value As Boolean)
            '   EventLog.WriteEntry("Application", "seeting statuschange to " & Value)

            m_statusChange = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' comments adding status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Public Property IsAddComment() As Boolean Implements ITicketEventArgs.IsAddComment
        Get
            Return m_isAddComment
        End Get
        Set(ByVal Value As Boolean)
            m_isAddComment = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for creating action request
    ' in database
    ' ----------  ARPATECH  ----------
    Public Function CreateAR() As ActionRequest Implements ITicketEventArgs.CreateAR
        Dim ar As New ActionRequest()
        ar.AddFieldByDisplay("Ticket Change (Any)", "True", "True")
        ar.AddFieldByDisplay("Ticket Summary", m_summary)
        ar.AddFieldByDisplay("Ticket Description", m_desc)
        ar.AddFieldByDisplay("Serial Number", m_assetSerial)
        ar.AddFieldByDisplay("Submitting User Phone", m_submittingUserPhone)
        ar.AddFieldByDisplay("Affected Contact Phone", m_affectedContactPhone)
        ar.AddFieldByDisplay("Customer Ticket ID", m_customerTicketID)
        ar.AddFieldByDisplay("GSP Ticket ID", m_sfaTicketID)

        ar.AddFieldByDisplay("Ticket Contact Name", m_ticketContactName)
        ar.AddFieldByDisplay("Ticket Contact Phone", m_ticketContactPhone)



        ar.CategoryName = "Ticket Change"

        ar.GSTicketID = m_GSTicketID

        ar.Qualify = False
        ar.GenerateTicket = False
        ar.ActionRequestID = m_actionRequestID



        If Not IsNothing(m_companyID) Then
            ar.SetCompanyID(m_companyID, m_companyName)
        End If


        If Not IsNothing(m_typeID) Then
            ar.SetTypeID(m_typeID, m_typeName)
            ar.AddFieldByDisplay("Request Type", m_typeID, m_typeName)
        End If


        If Not IsNothing(m_affectedContactID) Then
            ar.SetAffectedContactID(m_affectedContactID, m_affectedContactName)
            ar.AddFieldByDisplay("Ticket Affected User", m_affectedContactID, m_affectedContactName)
            Dim affectedContactEmail As New String("")
            affectedContactEmail = GSSupportManager.GetSingleDBValueString("select isnull(email, '') as email from tblContact where contactid = " & m_affectedContactID)
            If Len(affectedContactEmail) > 0 Then

                ar.AddFieldByDisplay("Email of Affected User", affectedContactEmail, affectedContactEmail)

            End If
        End If

        If Not IsNothing(m_statusID) Then

            ar.AddFieldByDisplay("Ticket Status", m_statusID, m_statusName)
        End If

        If Not IsNothing(m_priorityID) Then

            ar.AddFieldByDisplay("Ticket Priority", m_priorityID, m_priorityName)
        End If

        If Not IsNothing(m_submittedByUserID) Then
            ar.SubmitUserID = m_submittedByUserID
            ar.SubmitUserName = m_submittedByUserName
            ar.AddFieldByDisplay("Ticket Submitted By", m_submittedByUserID, m_submittedByUserName)
            Dim submittingUserEmail As New String("")
            submittingUserEmail = GSSupportManager.GetSingleDBValueString("select isnull(email, '') from tblContact where userID = " & m_submittedByUserID)
            If Len(submittingUserEmail) > 0 Then

                ar.AddFieldByDisplay("Email of Submitting User", submittingUserEmail, submittingUserEmail)

            End If

        End If

        If Not IsNothing(m_csrID And m_csrID > 0) Then
            ar.AddFieldByDisplay("CSR", m_csrID, m_csrName)
            ar.AddFieldByDisplay("Email of Assigned CSR", m_csrEmail)

        End If

        If Not IsNothing(m_requestDateLocal) Then
            ar.AddFieldByDisplay("Ticket Submitted Date", m_requestDateLocal, m_requestDateLocal + " " + m_timeZoneName)
            ar.AddFieldByDisplay("Request Date (Local Time)", m_requestDateLocal, m_requestDateLocal + " " + m_timeZoneName)
        End If

        If Not IsNothing(m_sfaid And m_sfaid > 0) Then
            ar.AddFieldByDisplay("FSC", m_sfaid, m_sfaid)
            'If m_addressID > 0 Then
            'ar.AddFieldByDisplay("Email of Assigned FSC", GSSupportManager.GetSFAEmails(m_sfaid, m_addressID))
            'End If
            ar.AddFieldByDisplay("Email of Assigned FSC", GSSupportManager.GetSFAEmails(m_sfaid))
        End If

        If Not IsNothing(m_countryID) AndAlso m_countryID > 0 Then
            Dim countryname As String = GSSupportManager.GetSingleDBValueString("select countryname from tblcountries where countryid=" & m_countryID)
            ar.AddFieldByDisplay("Country of Affected Asset", m_countryID, countryname)
            ar.AddFieldByDisplay("Country of Affected User", m_countryID, countryname)

        End If

        If Not IsNothing(m_addressID) AndAlso m_addressID > 0 Then
            ar.AddFieldByDisplay("Address of Affected Asset", m_addressID, m_addressText)
            ar.AddFieldByDisplay("Address of Affected User", m_addressID, m_addressText)
            Dim coverageID As Integer = GSSupportManager.GetSingleDBValueInt("select isNull(coverageid, 0) from tblCustomerAddresses where customeraddressid=" & m_addressID)
            If Not IsNothing(coverageID) AndAlso IsNumeric(coverageID) AndAlso CInt(coverageID) > 0 Then
                ar.AddFieldByDisplay("Coverage Type", CStr(coverageID))
            Else
                ar.AddFieldByDisplay("Coverage Type", "0")

            End If

        End If

        If IsStatusChange Then
            ar.AddFieldByDisplay("Ticket Status", CStr(m_newStatusID), m_statusName)
            ar.AddFieldByDisplay("Ticket Status Changed", "True", "True")

        End If

        If IsEscalationChange Then
            ar.AddFieldByDisplay("Ticket Escalation Level", CStr(m_newEscalationLevel))
            ar.AddFieldByDisplay("Ticket Escalation Changed", "True", "True")
        End If

        If IsPriorityChange Then
            ar.AddFieldByDisplay("Ticket Priority", CStr(m_newPriorityID), m_priorityName)
            ar.AddFieldByDisplay("Ticket Priority Changed", "True", "True")
        End If

        '       EventLog.WriteEntry("Application", "newappt is " & m_isNewAppt)
        If Me.IsNewAppt Then
            ar.AddFieldByDisplay("Appointment Date Changed", "True", "True")
        End If

        If Not IsNothing(m_apptDate) AndAlso IsDate(m_apptDate) Then
            ar.AddFieldByDisplay("Appointment Date", CStr(m_apptDate), CStr(m_apptDate))
        End If

        If Me.IsFscAssigned Then

            ar.AddFieldByDisplay("Ticket GSP Changed", "True", "True")
        End If

        If IsCsrAssigned Then
            ar.AddFieldByDisplay("Ticket CSM Changed", "True", "True")
        End If

        If IsAddComment Then
            ar.AddFieldByDisplay("Ticket Comment Added", "True", "True")
        End If

        If m_statusActualChangedDates.Count > 0 Then
            ar.AddFieldByDisplay("Ticket Status Actual  Date Changed", "True", "True")
            Dim s As New String("")
            Dim i As Integer



            For i = 0 To m_statusActualChangedDates.Count - 1
                s = s & CStr(m_statusActualChangedDates(i))
            Next



            ar.AddFieldByDisplay("Ticket Status Actual Change Text", s, s)

        End If

        If m_statusTargetChangedDates.Count > 0 Then
            ar.AddFieldByDisplay("Ticket Status Target Date Changed", "True", "True")
            Dim s As New String("")
            Dim i As Integer


            For i = 0 To m_statusTargetChangedDates.Count - 1
                s = s & CStr(m_statusTargetChangedDates(i))
            Next

            ar.AddFieldByDisplay("Ticket Status Target Change Text", s, s)

        End If

        Return ar
    End Function


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' exception status
    ' ----------  ARPATECH  ----------
    Property IsException() As Boolean Implements ITicketEventArgs.IsException
        Get
            Return m_isException
        End Get
        Set(ByVal Value As Boolean)
            m_isException = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' closing SLA status
    ' ----------  ARPATECH  ----------
    Property IsClosedWithinSLA() As Boolean Implements ITicketEventArgs.IsClosedWithinSLA
        Get
            Return m_isClosedWithinSLA
        End Get
        Set(ByVal Value As Boolean)
            m_isClosedWithinSLA = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA breaching status
    ' ----------  ARPATECH  ----------
    Property IsBreach() As Boolean Implements ITicketEventArgs.IsBreach
        Get
            Return m_isBreach
        End Get
        Set(ByVal Value As Boolean)
            m_isBreach = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' target changing dates
    ' ----------  ARPATECH  ----------
    Property StatusTargetChangedDates() As ArrayList Implements ITicketEventArgs.StatusTargetChangedDates
        Get
            Return m_statusTargetChangedDates
        End Get
        Set(ByVal Value As ArrayList)
            m_statusTargetChangedDates = Value
        End Set
    End Property
    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' actual changes dates
    ' ----------  ARPATECH  ----------
    Property StatusActualChangedDates() As ArrayList Implements ITicketEventArgs.StatusActualChangedDates
        Get
            Return m_statusActualChangedDates
        End Get
        Set(ByVal Value As ArrayList)
            m_statusActualChangedDates = Value
        End Set
    End Property
End Class

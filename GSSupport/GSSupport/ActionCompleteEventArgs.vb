' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class has only one property for setting and returning execute any
'                       given process of type ActionMonitor.
' Inherits          :   EventArgs
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionCompleteEventArgs
    Inherits EventArgs

    Private m_actionMonitor As ActionMonitor

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' of Action Monitor type
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
       Property ActionMonitor() As ActionMonitor
        Get
            Return m_actionMonitor
        End Get
        Set(ByVal Value As ActionMonitor)
            m_actionMonitor = Value
        End Set
    End Property

End Class

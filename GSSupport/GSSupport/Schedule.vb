' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains properties and functions for scheduling like calculating
'                       when to execute next, when executed first when executed last, sending
'                       Action Request etc.
' Inherits          :   ---------
' Implements        :   ScheduledEvent, IMQUtils, IGSEventListener
' Friends           :   GSSupportManager
' ****************** ARPATECH (General Description) ******************


Imports GSSupport.GSAppGlobals
Imports System.Messaging
Imports System.Data.SqlClient
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE



' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface ScheduledEvent

    Property ExecuteNextDate() As Date
    Property LastExecuteDate() As Date
    Property StartDate() As Date
    Property ScheduleID() As Integer
    Property ActionID() As Integer
    Property IsDaily() As Boolean
    Property IsMonthly() As Boolean
    Property IsWeekly() As Boolean
    Property IsOnce() As Boolean
    Property IsActive() As Boolean
    Property StartDateTZ() As Integer
    Property RepeatInterval() As Integer
    Property DayOfMonth() As Integer
    Property Months() As ArrayList
    Property DaysOfWeek() As ArrayList
    Property ScheduleName() As String
    Property MQLabel() As String
    Property GSManager() As GSSupportManager
    Property TZFieldID() As Integer
    Sub LoadFromDB(ByVal sid As Integer)



End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class Schedule
    Implements ScheduledEvent, IMQUtils, IGSEventListener
    Friend GSSupportManager


    Private m_scheduleName As String
    Private m_actionID As Integer
    Private m_scheduleID As Integer = 0
    Private m_executeNextDate As Date
    Private m_startDate As Date
    Private m_months As New ArrayList()
    Private m_daysOfWeek As New ArrayList()
    Private m_repeatInterval As Integer
    Private m_dayOfMonth As Integer
    Private m_startDateTZ As Integer
    Private m_isActive As Boolean
    Private m_isWeekly As Boolean = False
    Private m_isDaily As Boolean = False
    Private m_isMonthly As Boolean = False
    Private m_isOnce As Boolean = False
    Private m_lastExecuteDate As Date
    Private m_mqLabel As String
    Private m_evaluatedARs As New SortedList()
    Private m_tzFieldID As Integer
    Private m_gsListener As GSEventListener
    Private m_gsManager As GSSupportManager

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule next execution date
    ' ----------  ARPATECH  ----------
    Public Property ExecuteNextDate() As Date Implements ScheduledEvent.ExecuteNextDate
        Get
            Return m_executeNextDate
        End Get
        Set(ByVal Value As Date)
            m_executeNextDate = Value
            UpdateNextExecuteDB(m_executeNextDate)
        End Set
    End Property



    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule last execution date
    ' ----------  ARPATECH  ----------
    Public Property LastExecuteDate() As Date Implements ScheduledEvent.LastExecuteDate
        Get
            Return m_lastExecuteDate
        End Get
        Set(ByVal Value As Date)
            ' Convert to local time
            m_lastExecuteDate = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Manager
    ' ----------  ARPATECH  ----------
    Public Property GSManager() As GSSupportManager Implements ScheduledEvent.GSManager
        Get
            Return m_gsManager
        End Get
        Set(ByVal Value As GSSupportManager)
            ' Convert to local time
            m_gsManager = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule name
    ' ----------  ARPATECH  ----------
    Public Property ScheduleName() As String Implements ScheduledEvent.ScheduleName
        Get
            Return m_scheduleName
        End Get
        Set(ByVal Value As String)
            m_scheduleName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' MQ label
    ' ----------  ARPATECH  ----------
    Public Property MQLabel() As String Implements ScheduledEvent.MQLabel
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action ID
    ' ----------  ARPATECH  ----------
    Public Property ActionID() As Integer Implements ScheduledEvent.ActionID
        Get
            Return m_actionID
        End Get
        Set(ByVal Value As Integer)
            m_actionID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' time zone field ID
    ' ----------  ARPATECH  ----------
    Public Property TZFieldID() As Integer Implements ScheduledEvent.TZFieldID
        Get
            Return m_tzFieldID
        End Get
        Set(ByVal Value As Integer)
            m_tzFieldID = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule event start date
    ' ----------  ARPATECH  ----------
    Public Property StartDate() As Date Implements ScheduledEvent.StartDate
        Get
            Return m_startDate
        End Get
        Set(ByVal Value As Date)
            '   EventLog.WriteEntry("Application", "Setting startdate to " & Value.ToString())
            m_startDate = Value
            Me.ExecuteNextDate = CalculateNextExecute(Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' time zone start date
    ' ----------  ARPATECH  ----------
    Public Property StartDateTZ() As Integer Implements ScheduledEvent.StartDateTZ
        Get
            Return m_startDateTZ
        End Get
        Set(ByVal Value As Integer)
            m_startDateTZ = Value
            '         If Not IsNothing(m_startDate) Then
            '     Me.ExecuteNextDate = CalculateNextExecute((New GSSupportManager()).ConvertTime(Now(), ServerTimeZone, m_startDateTZ))
            '     End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule ID
    ' ----------  ARPATECH  ----------
    Public Property ScheduleID() As Integer Implements ScheduledEvent.ScheduleID
        Get
            Return m_scheduleID
        End Get
        Set(ByVal Value As Integer)
            m_scheduleID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' day of the month
    ' ----------  ARPATECH  ----------
    Public Property DayOfMonth() As Integer Implements ScheduledEvent.DayOfMonth
        Get
            Return m_dayOfMonth
        End Get
        Set(ByVal Value As Integer)
            m_dayOfMonth = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' repeatation interval
    ' ----------  ARPATECH  ----------
    Public Property RepeatInterval() As Integer Implements ScheduledEvent.RepeatInterval
        Get
            Return m_repeatInterval
        End Get
        Set(ByVal Value As Integer)
            m_repeatInterval = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' day of the week
    ' ----------  ARPATECH  ----------
    Public Property DaysOfWeek() As ArrayList Implements ScheduledEvent.DaysOfWeek
        Get
            Return m_daysOfWeek
        End Get
        Set(ByVal Value As ArrayList)
            m_daysOfWeek = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' months in array list
    ' ----------  ARPATECH  ----------
    Public Property Months() As ArrayList Implements ScheduledEvent.Months
        Get
            Return m_months
        End Get
        Set(ByVal Value As ArrayList)
            m_months = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule active status
    ' ----------  ARPATECH  ----------
    Public Property IsActive() As Boolean Implements ScheduledEvent.IsActive
        Get
            Return m_isActive
        End Get
        Set(ByVal Value As Boolean)
            m_isActive = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule is once status
    ' ----------  ARPATECH  ----------
    Public Property IsOnce() As Boolean Implements ScheduledEvent.IsOnce
        Get
            Return m_isOnce
        End Get
        Set(ByVal Value As Boolean)
            m_isOnce = Value
            If m_isOnce Then
                m_isDaily = False
                m_isWeekly = False
                m_isMonthly = False
            End If
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' monthly schedule
    ' ----------  ARPATECH  ----------
    Public Property IsMonthly() As Boolean Implements ScheduledEvent.IsMonthly
        Get
            Return m_isMonthly
        End Get
        Set(ByVal Value As Boolean)
            m_isMonthly = Value
            If m_isMonthly Then
                m_isDaily = False
                m_isWeekly = False
                m_isOnce = False
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' weekly schedule
    ' ----------  ARPATECH  ----------
    Public Property IsWeekly() As Boolean Implements ScheduledEvent.IsWeekly
        Get
            Return m_isWeekly
        End Get
        Set(ByVal Value As Boolean)
            m_isWeekly = Value
            If (m_isWeekly) Then
                m_isDaily = False
                m_isMonthly = False
                m_isOnce = False
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' daily schedule
    ' ----------  ARPATECH  ----------
    Public Property IsDaily() As Boolean Implements ScheduledEvent.IsDaily
        Get
            Return m_isDaily
        End Get
        Set(ByVal Value As Boolean)
            m_isDaily = Value
            If (m_isDaily) Then
                m_isWeekly = False
                m_isMonthly = False
                m_isOnce = False
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for calculating next execute 
    ' date and return
    ' ----------  ARPATECH  ----------
    Function CalculateNextExecute(ByVal LocalTime As Date) As Date
        ' EventLog.WriteEntry("Application", "Calculating next execution for " & m_scheduleID & " starting " & LocalTime.ToString())
        Dim nd As Date

        ' Really easy if we only execute once
        If m_isOnce Then
            EventLog.WriteEntry("Application", "Is Once; returning start date")
            Return m_startDate

            ' If it's daily...
        ElseIf m_isDaily Then
            If m_repeatInterval <= 2 OrElse IsNothing(m_lastExecuteDate) Then
                'Going to be executed today

                nd = New Date(Year(Now()), Month(Now()), Day(Now()), Hour(m_startDate), Minute(m_startDate), 0)
                Return nd
            Else
                nd = New Date()
                nd = DateAdd(DateInterval.Day, m_repeatInterval, m_lastExecuteDate)
                Return nd

            End If
        ElseIf m_isWeekly Then
            ' Get the current day of the week in local time
            Dim tday As Integer = GetNextDay(Day(LocalTime))
            If tday > 0 Then
                Dim ddiff = Math.Abs(Day(LocalTime) - tday)
                nd = DateAdd(DateInterval.Day, ddiff, LocalTime)
                If nd < LocalTime Then
                    ' If it's in the past then go to tomorrow
                    Return CalculateNextExecute(DateAdd(DateInterval.Day, 1, LocalTime))
                Else
                    Return nd
                End If

            ElseIf m_isMonthly Then

                Dim tmonth As Integer = GetNextMonth(Month(LocalTime))
                If tmonth > 0 Then
                    nd = New Date(Year(LocalTime), tmonth, m_dayOfMonth, Hour(m_startDate), Minute(m_startDate), Second(StartDate))
                    ' If it's in the past then go to first day of next month...
                    If nd < LocalTime Then
                        Dim anotherdate = DateAdd(DateInterval.Month, 1, LocalTime)
                        Return CalculateNextExecute(New Date(Year(anotherdate), Month(anotherdate), 1, 0, 0, 0))
                    Else
                        Return nd

                    End If

                End If


            End If

        End If
        ' If I'm here there's no next execute date, so let's remove me from the Q.

        GSSupportManager.ScheduleComplete(m_mqLabel)

    End Function

    ' ----------  ARPATECH  ----------
    ' function for checking execute date
    ' and return status
    ' ----------  ARPATECH  ----------
    Function CheckExecuteTime(ByVal d As Date) As Boolean
        If Not IsNothing(m_executeNextDate) AndAlso d > m_executeNextDate Then
            '   EventLog.WriteEntry("Application", "Comparing " & d.ToString() & " to " & m_executeNextDate.ToString())

            Return True
        Else
            Return False
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' function for  Retrieving next day to come
    ' ----------  ARPATECH  ----------
    Function GetNextDay(ByVal sd As Integer)
        Dim i As Integer = 0

        For i = sd To 7
            If DaysOfWeek.Contains(i) Then
                Return i
            End If
        Next
        If sd > 1 Then

        End If
        For i = 1 To sd - 1
            If DaysOfWeek.Contains(i) Then
                Return i
            End If
        Next
        Return 0

    End Function

    ' ----------  ARPATECH  ----------
    ' function for  Retrieving next month to come
    ' ----------  ARPATECH  ----------
    Function GetNextMonth(ByVal sm As Integer)
        Dim i As Integer = 0

        For i = sm To 12
            If Months.Contains(i) Then
                Return i
            End If
        Next
        If sm > 1 Then

        End If
        For i = 1 To sm - 1
            If Months.Contains(i) Then
                Return i
            End If
        Next
        Return 0

    End Function


    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR 
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with schedule ID argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal scheduleID As Integer)
        m_scheduleID = scheduleID

    End Sub

    ' ----------  ARPATECH  ----------
    ' Function for creating message queue
    ' ----------  ARPATECH  ----------
    Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage

        Dim fb As New System.Messaging.BinaryMessageFormatter()

        Dim m As New System.Messaging.Message()

        fb.Write(m, Me)

        m.Label = "SID=" & CStr(m_scheduleID) & "&NAME=" & m_scheduleName & "&AID=" & CStr(m_actionID)
        m_mqLabel = m.Label

        CreateMQMessage = m

    End Function

    ' ----------  ARPATECH  ----------
    ' routine to restart listner for AR
    ' ----------  ARPATECH  ----------
    Public Sub RestartListener()
        ' Listen for incoming action requests...
        Dim mq As MessageQueue = New MessageQueue(GSSupportManager.GetQHost() & QualificationQ)

        mq.Formatter = New BinaryMessageFormatter()

        ' AddHandler mq.PeekCompleted, New PeekCompletedEventHandler(AddressOf OnPeekCompleted)
        mq.BeginPeek()
        ' EventLog.WriteEntry("Application", "Sched #" & m_scheduleID & " Listening to Qual Q")
    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for submiting Action request
    ' ----------  ARPATECH  ----------
    Sub SendAR()
        Try
            Dim ar As New ActionRequest()
            ar.ScheduleID = m_scheduleID
            ar.GenerateTicket = False
            ar.Qualify = False
            ar.RequestedActionID = m_actionID
            ar.Submit()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error sending AR from schedule:" & e.Message)

        Finally
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to update next schedule event 
    ' in database
    ' ----------  ARPATECH  ----------
    Sub UpdateNextExecuteDB(ByVal nextDate As Date)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        '   EventLog.WriteEntry("Application", "Updating db with date " & nextDate.ToString())
        Dim sql As New String("")

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Try

            sql = "update tblSchedule set nextExecuteDate = '" & nextDate.ToString() & "' where scheduleid=" & m_scheduleID
            Cmd.CommandText = sql
            EventLog.WriteEntry("Application", sql)

            Cmd.Connection = Conn

            Cmd.ExecuteNonQuery()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to update db next execute date: " & e.Message)

        Finally
            Conn.Close()

            Conn = Nothing
            Cmd = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to load schedule values from DB
    ' ----------  ARPATECH  ----------
    Sub LoadFromDB(ByVal sid As Integer) Implements ScheduledEvent.LoadFromDB
        Dim m As New GSSupportManager()
        m.LoadSchedules(sid)
    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to be executed on Action 
    ' request completion event
    ' ----------  ARPATECH  ----------
    Public Sub OnActionCompleteEvent(ByVal sender As Object, ByVal e As ActionCompleteEventArgs) Implements IGSEventListener.OnActionCompleteEvent
        EventLog.WriteEntry("Application", "RECEIVED AC EVENT")
    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to be executed on action request event
    ' ----------  ARPATECH  ----------
    Public Sub OnActionRequestEvent(ByVal sender As Object, ByVal e As ActionRequestEventArgs) Implements IGSEventListener.OnActionRequestEvent
        EventLog.WriteEntry("Application", "RECEIVED AR EVENT")

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to be executed on timer event
    ' ----------  ARPATECH  ----------
    Public Sub OnGSTimerEvent(ByVal sender As Object, ByVal e As GSTimerEventArgs) Implements IGSEventListener.OnGSTimerEvent
        EventLog.WriteEntry("Application", "RECEIVED GSTimer EVENT")
        Try


            Dim ar As New ActionRequest()

            ar = e.ActionRequest


            If Not m_evaluatedARs.ContainsKey(ar.ActionRequestID) Then

                m_evaluatedARs.Add(ar.ActionRequestID, True)
                EventLog.WriteEntry("Application", "scheduleid is " & m_scheduleID)

                ' Dim manager As New GSSupportManager()

                ' GSSupportManager.AddProcessedARSchedule(m_scheduleID, ar)

                If ar.ContainsField(m_tzFieldID) Then

                    If IsDate(ar.GetFieldNativeValue(m_tzFieldID)) Then

                        If CheckExecuteTime(CDate(ar.GetFieldNativeValue(m_tzFieldID))) Then
                            Dim localtime As Date = m_gsManager.ConvertTime(Now(), ServerTimeZone, m_startDateTZ)

                            ' EventLog.WriteEntry("Application", "Schedule " & m_scheduleID & "returned true; executing action...")
                            m_lastExecuteDate = localtime
                            SendAR()
                            m_executeNextDate = CalculateNextExecute(localtime)

                        End If


                    End If


                Else
                    '     EventLog.WriteEntry("Application", "wff " & m_workflowID & " already processed " & ar.ActionRequestID)
                End If
            End If



        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error Schedule receiving AR:" & ex.Message)
        Finally
            '     RestartListener()
        End Try
    End Sub


    ' ----------  ARPATECH  ----------
    ' EMPTY routine
    ' ----------  ARPATECH  ----------
    Public Sub OnTicketEvent(ByVal sender As Object, ByVal e As TicketEventArgs) Implements IGSEventListener.OnTicketEvent

    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY routine
    ' ----------  ARPATECH  ----------
    Public Sub OnOrderEvent(ByVal sender As Object, ByVal e As OrderEventArgs) Implements IGSEventListener.OnOrderEvent

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Listner 
    ' ----------  ARPATECH  ----------
    Public Property GSListener() As GSEventListener Implements IGSEventListener.GSListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            m_gsListener = Value
            AddHandler m_gsListener.GSTimerEvent, New GSTimerEventHandler(AddressOf Me.OnGSTimerEvent)

        End Set
    End Property

End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is contains only one function and several properties. The
'                       function returns a string consists of formula with multiple operators and
'                       operands.
' Inherits          :   ---------
' Implements        :   IFormulaDetail
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Windows.Forms

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IFormulaDetail

    Property BeginClause() As Integer
    Property EndClause() As Integer
    Property OperatorStr() As String
    Property ConjunctionStr() As String
    Property ConjunctionID() As Integer
    Property Var1() As Object
    Property Var2() As Object

End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class FormulaDetail
    Implements IFormulaDetail

    Private m_beginClause As Integer = 0
    Private m_endClause As Integer = 0
    Private m_operator As [Operator]
    Private m_conjunction As Conjunction
    Private m_returnType As Integer
    Private m_var1 As Object
    Private m_var2 As Object
    Private m_formulaDetailID As Integer = 0

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' formula detail ID
    ' ----------  ARPATECH  ----------
    Public Property FormulaDetailID() As Integer
        Get
            Return m_formulaDetailID
        End Get
        Set(ByVal Value As Integer)
            m_formulaDetailID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' begin clause of formula detail
    ' ----------  ARPATECH  ----------
    Public Property BeginClause() As Integer Implements IFormulaDetail.BeginClause
        Get
            Return m_beginClause
        End Get
        Set(ByVal Value As Integer)
            m_beginClause = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' end clause of formula detail
    ' ----------  ARPATECH  ----------
    Public Property EndClause() As Integer Implements IFormulaDetail.EndClause
        Get
            Return m_endClause
        End Get
        Set(ByVal Value As Integer)
            m_endClause = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' conjunction ID as integer
    ' ----------  ARPATECH  ----------
    Public Property ConjunctionID() As Integer Implements IFormulaDetail.ConjunctionID
        Get
            Return m_conjunction.ConjID
        End Get
        Set(ByVal Value As Integer)
            Console.WriteLine("1")
            m_conjunction = New Conjunction(Value)
            Console.WriteLine("2")
        End Set
    End Property



    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator as operator type
    ' ----------  ARPATECH  ----------
    Public Property [Operator]() As [Operator]
        Get
            Return m_operator
        End Get
        Set(ByVal Value As [Operator])
            m_operator = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator as string
    ' ----------  ARPATECH  ----------
    Public Property OperatorStr() As String Implements IFormulaDetail.OperatorStr
        Get
            Return m_operator.OperatorSymbol
        End Get
        Set(ByVal Value As String)
            m_operator = New [Operator](Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' conjunction
    ' ----------  ARPATECH  ----------
    Public Property Conjunction() As Conjunction
        Get
            Return m_conjunction
        End Get
        Set(ByVal Value As Conjunction)
            m_conjunction = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' con junction string
    ' ----------  ARPATECH  ----------
    Public Property ConjunctionStr() As String Implements IFormulaDetail.ConjunctionStr
        Get
            Return m_conjunction.Text
        End Get
        Set(ByVal Value As String)
            m_conjunction = New Conjunction(Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' variable 1
    ' ----------  ARPATECH  ----------
    Public Property Var1() As Object Implements IFormulaDetail.Var1
        Get
            Return m_var1
        End Get
        Set(ByVal Value As Object)
            m_var1 = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' variable 2
    ' ----------  ARPATECH  ----------
    Public Property Var2() As Object Implements IFormulaDetail.Var2
        Get
            Return m_var2
        End Get
        Set(ByVal Value As Object)
            m_var2 = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR 
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with multiple arguments 
    ' for setting defaul values
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal beginClause As Integer, ByVal endClause As Integer, ByVal operatorStr As String, ByVal var1 As Object, ByVal var2 As Object, ByVal conjunctionStr As String)
        m_beginClause = beginClause
        m_endClause = endClause
        m_operator = New [Operator](operatorStr)
        m_var1 = var1
        m_var2 = var2
        m_conjunction = New Conjunction(conjunctionStr)
    End Sub

    ' ----------  ARPATECH  ----------
    ' function for retriving formula details
    ' as text
    ' ----------  ARPATECH  ----------
    Function GetText() As String
        Dim i As Integer
        Dim s As New String("")
        For i = 1 To m_beginClause
            s = s & "("
        Next
        '       MessageBox.Show("in gettext")
        If Not IsNothing(m_var1) Then
            s = s & GetValText(m_var1)
        End If
        '     MessageBox.Show("gotvar1")
        If Not IsNothing(m_operator) Then
            s = s & m_operator.OperatorSymbol
        End If
        If Not IsNothing(m_var2) Then
            s = s & GetValText(m_var2)
        End If
        '      MessageBox.Show("gotvar2")

        For i = 1 To m_endClause
            s = s & ")"
        Next

        If Not IsNothing(m_conjunction) Then
            s = s & m_conjunction.Text
        End If
        '   MessageBox.Show("leaving gettext")
        Return s
    End Function



End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class creates an Action for every Action Request that user inputs to
'                       the system.
' Inherits          :   ---------
' Implements        :   IActionFactory inherits Action
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
Imports System.Data
' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IActionFactory
    Function CreateActionFromAR(ByVal ar As ActionRequest) As Action
    Function CreateAction(ByVal actionID As Integer) As Action

End Interface
<System.Runtime.InteropServices.ComVisible(True)> Public Class ActionFactory
    Implements IActionFactory

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that takes a ActionRequest
    ' and creates a action instance on that AR
    ' and returns that Action instance
    ' ----------  ARPATECH  ----------
    Public Function CreateActionFromAR(ByVal ar As ActionRequest) As Action Implements IActionFactory.CreateActionFromAR
        Dim a As Action

        '   If Not IsNothing(ar.RequestedActionID) AndAlso ar.RequestedActionID > 0 Then
        '  a = CreateAction(ar.RequestedActionID)
        '  a.ActionRequest = ar
        '  a.LoadDefaultFieldValues()
        ' EventLog.WriteEntry("Application", "arreqid is " & ar.RequestedActionID)

        ' Else
        Dim actionfieldid As Integer = 0
        actionfieldid = GSSupportManager.GetSingleDBValueInt("select min(fieldid) from tblFields where isactive=1 and fieldname='actionid'")
        Dim s As String = ar.GetFieldTextValue(actionfieldid)
        '   EventLog.WriteEntry("Application", "actionfieldid is " & s)
        If IsNumeric(s) Then
            If CInt(s) > 0 Then
                a = CreateAction(CInt(s))
                a.ActionRequest = ar
                a.LoadDefaultFieldValues()
            End If


        End If

        '   End If
        Return a
    End Function

    ' ----------  ARPATECH  ----------
    ' function that takes a actionID and
    ' retrieves some basic values of that Action
    ' from DB and returns Action instance
    ' after setting all the values
    ' ----------  ARPATECH  ----------
    Public Function CreateAction(ByVal actionID As Integer) As Action Implements IActionFactory.CreateAction

        Dim a As Action

        If Not actionID > 0 Then
            EventLog.WriteEntry("Application", "ERROR: Attempted to create action without valid actionid")
        End If

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try


            Dim dr As SqlDataReader
            Cmd.CommandText = "select IsNull(actionname, '') as actionname, IsNull(tblActions.actiontypeid, 0)  as actiontypeid, " & _
            "IsNull(actiontypename, '') as actiontypename, IsNull(actionclass, '') as actionclass " & _
            "from tblActions left join tblActionType on tblActions.actiontypeid = tblActionType.actiontypeid where tblActions.isActive = 1 and actionid = " & actionID


            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()


                If Not IsNothing(dr.GetString(dr.GetOrdinal("actionclass"))) Then

                    a = CreateObject(dr.GetString(dr.GetOrdinal("actionclass")))

                    a.ActionID = actionID
                    a.ActionName = dr.GetString(dr.GetOrdinal("actionname"))
                    a.ActionTypeName = dr.GetString(dr.GetOrdinal("actiontypename"))
                    a.ActionTypeID = dr.GetInt32(dr.GetOrdinal("actiontypeid"))

                End If

            End While
            dr.Close()



        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to create action: " & e.Message & ", source=" & e.Source)


        Finally
            Conn.Close()
            Conn = Nothing

        End Try

        Return a
    End Function



End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class acts like a support module manager for multiple functions like
'                       Loading SLAs, Fields, Workflows, and Schedules etc.
' Inherits          :   ---------
' Implements        :   IGSSupportManager
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Messaging
Imports System.Data.SqlClient
Imports System.Data
Imports System.Diagnostics
Imports System.Runtime.InteropServices
Imports System.Resources
Imports Microsoft.Win32
Imports System
Imports System.ServiceProcess

Imports System.Timers

Imports GSSupport
Imports System.Collections

Imports System.DateTime

Imports System.Runtime.Remoting.Lifetime
Imports System.Threading
Imports System.Xml


Imports System.IO
Imports System.Text
Imports System.Runtime.Remoting
Imports System.Security.Policy

Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports MSMQ




' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IGSSupportManager

    Function CreateARQs() As Integer
    Function GetSLA(ByVal slaid As Integer) As SLA
    Function GetSLAMonitor(ByVal slaiid As Integer) As SLAMonitor
    Function GetTAC(ByVal tac As Integer) As TermsAndConditions
    Function DispatchARQual(ByVal arid As String) As Boolean
    Function DispatchAR(ByVal arid As String) As Boolean
    ReadOnly Property ARQCount() As Integer
    Sub TestWF(ByVal str As String)
    Sub SendActionEvent(ByVal am As ActionMonitor)
    Function LoadSchedule(ByVal sid As Integer) As Boolean
    Function Base64Encode(ByVal enc As String) As String
    Function Base64Decode(ByVal enc As String) As String



End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class GSSupportManager
    Implements IGSSupportManager

    Private ReadOnly m_arQCount As Integer
    '   Public Shared m_workflowsProcessed As New Hashtable()
    '   Public Shared m_schedulesProcessed As New Hashtable()
    '   Public Shared m_slasProcessed As New Hashtable()
    '   Public Shared m_amworkflowsProcessed As New Hashtable()
    Public Shared m_fieldsByDisplay As New Hashtable()
    Public Shared m_fieldsByName As New Hashtable()

    Public Event TicketEvent As TicketEventHandler
    Public Shared GSListener As GSEventListener
    Private Shared DBString As String = ""
    Private Shared SMTPHost As String = ""
    Private Shared QHost As String = ""
    Private Shared AlertEmail As String = ""
    Private Shared AgentPath As String = ""
    Private Shared eConnLogPath As String = ""


    ' ----------  ARPATECH  ----------
    ' A Property for Retrieving
    ' action request counts
    ' ----------  ARPATECH  ----------
    Public ReadOnly Property ARQCount() As Integer Implements IGSSupportManager.ARQCount
        Get
            Return m_arQCount
        End Get

    End Property
    ' ----------  ARPATECH  ----------
    ' Function for getting alert email value
    ' from registry
    ' ----------  ARPATECH  ----------
    Public Shared Function GetAlertEmail() As String
        If QHost.Length < 1 Then
            Dim k As RegistryKey = Registry.LocalMachine.OpenSubKey(RegistryPath)
            AlertEmail = k.GetValue("AlertEmail")
        End If
        Return AlertEmail
    End Function
    ' ----------  ARPATECH  ----------
    ' Function for getting SMTP host name
    ' ----------  ARPATECH  ----------
    Public Shared Function GetSMTPHost() As String
        If SMTPHost.Length < 1 Then
            Dim k As RegistryKey = Registry.LocalMachine.OpenSubKey(RegistryPath)
            SMTPHost = k.GetValue("SMTPHost")
        End If
        Return SMTPHost
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting queue host name
    ' ----------  ARPATECH  ----------
    Public Shared Function GetQHost() As String
        If QHost.Length < 1 Then
            Dim k As RegistryKey = Registry.LocalMachine.OpenSubKey(RegistryPath)
            QHost = k.GetValue("QHost")
        End If
        Return QHost
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for loading fields
    ' from DB
    ' ----------  ARPATECH  ----------
    Public Shared Function LoadFields() As Integer
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        m_fieldsByName.Clear()
        m_fieldsByDisplay.Clear()
        Conn.Open()
        Dim sql As String
        sql = "Select IsNull(fieldid, 0) as fieldid, fieldName, IsNull(fieldDisplay, '') as fieldDisplay, IsNull(fieldDataTypeID, 0) as fieldDataTypeID from tblFields where isActive=1"
        Dim f As Field


        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Dim dr As SqlDataReader
        Cmd.CommandText = sql
        dr = Cmd.ExecuteReader
        Try

            While dr.Read()
                f = New Field()
                f.FieldID = dr.GetInt32(dr.GetOrdinal("fieldID"))
                f.FieldName = dr.GetString(dr.GetOrdinal("fieldName"))
                f.FieldDisplayName = dr.GetString(dr.GetOrdinal("fieldDisplay"))
                f.DataType = dr.GetInt32(dr.GetOrdinal("fieldDataTypeID"))
                If Not m_fieldsByDisplay.ContainsKey(f.FieldDisplayName) Then
                    m_fieldsByDisplay.Add(f.FieldDisplayName, f)
                End If

                If Not m_fieldsByName.ContainsKey(f.FieldName) Then
                    m_fieldsByName.Add(f.FieldName, f)
                End If



            End While
        Catch e As Exception

            EventLog.WriteEntry("Application", "Error loading fields: " & e.Message)

        Finally
            Conn.Close()
            EventLog.WriteEntry("Application", "Loaded: " & m_fieldsByDisplay.Count & " fields")

        End Try
        Return m_fieldsByDisplay.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting filed 
    ' by its display name as string
    ' ----------  ARPATECH  ----------
    Public Shared Function GetFieldByDisplay(ByVal displayName As String) As Field
        Dim f As New Field()
        If m_fieldsByDisplay.ContainsKey(displayName) Then
            '    f = m_fieldsByDisplay.Item(displayName)
            f.FieldID = m_fieldsByDisplay.Item(displayName).FieldID
            f.FieldDisplayName = m_fieldsByDisplay.Item(displayName).FieldDisplayName
            f.FieldName = m_fieldsByDisplay.Item(displayName).FieldName
            f.DataType = m_fieldsByDisplay.Item(displayName).DataType



        End If
        ' EventLog.WriteEntry("Application", "returning field " & f.FieldID & " from display " & displayName)
        Return f
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting filed 
    ' by its name as string
    ' ----------  ARPATECH  ----------
    Public Shared Function GetFieldByName(ByVal fieldName As String) As Field
        Dim f As New Field()
        If m_fieldsByName.ContainsKey(fieldName) Then
            f = m_fieldsByName.Item(fieldName)
        End If
        Return f
    End Function


    ' ----------  ARPATECH  ----------
    ' Function for getting TAC by 
    ' provided ID
    ' ----------  ARPATECH  ----------
    Public Function GetTAC(ByVal tacid As Integer) As TermsAndConditions Implements IGSSupportManager.GetTAC
        Dim tac As New TermsAndConditions(tacid)
        Return tac
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for retriving DB string
    ' ----------  ARPATECH  ----------
    Public Shared Function GetDBString() As String
        If DBString.Length < 1 Then

            Dim k As RegistryKey = Registry.LocalMachine.OpenSubKey(RegistryPath)
            DBString = New GSSupportManager().Base64Decode(k.GetValue("DBString"))
        End If
        Return DBString
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for retriving registry
    ' path from agent 
    ' ----------  ARPATECH  ----------
    Public Shared Function GetAgentPath() As String
        If AgentPath.Length < 1 Then
            Dim k As RegistryKey = Registry.LocalMachine.OpenSubKey(RegistryPath)
            AgentPath = k.GetValue("AgentPath")
        End If
        Return AgentPath
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for retriving connection
    ' log file server path
    ' ----------  ARPATECH  ----------
    Public Shared Function GeteConnLogPath() As String
        If eConnLogPath.Length < 1 Then
            Dim k As RegistryKey = Registry.LocalMachine.OpenSubKey(RegistryPath)
            eConnLogPath = k.GetValue("eConnData")
        End If
        Return eConnLogPath
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for loading workflows by defaul activated
    ' ----------  ARPATECH  ----------
    Overloads Function LoadWorkflows() As Integer
        Dim el As New EventLog()
        el.Source = "GSSupportManager.LoadWorkflows"
        Dim Conn As New SqlConnection(DBString)
        Dim WFCount As Integer

        WFCount = 0
        Dim wf As Workflow
        Conn.Open()

        Dim Cmd As New SqlCommand()
        ' Cmd.Transaction = myTrans
        Try

            Dim dr As SqlDataReader
            Cmd.CommandText = "select * from tblWorkflow where isActive=1"
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader


            While dr.Read()

                ' First check if wf exists and update date

                ' Create a new wf object
                wf = New Workflow( _
                dr.GetInt32(dr.GetOrdinal("workflowid")), _
                dr.GetString(dr.GetOrdinal("workflowname")), _
                dr.GetBoolean(dr.GetOrdinal("cancelonerror")), _
                   dr.GetBoolean(dr.GetOrdinal("oncepersla")), _
                dr.GetBoolean(dr.GetOrdinal("makeinactiveoncompletion")), _
                dr.GetDateTime(dr.GetOrdinal("lastUpdate")))

                wf.GSListener = GSListener
                wf.GSManager = Me


                SendToQ(WorkflowQ, wf.CreateMQMessage())

                WFCount = WFCount + 1

            End While

            dr.Close()

            '   el.WriteEntry(CStr(WFCount) & " active workflows sent to " & WorkflowQ)

        Catch e As Exception
            el.WriteEntry("Failed to LoadWorkflows:" & e.Message)
            '   myTrans.Rollback()

        Finally

            Conn.Close()
            Cmd = Nothing
            el = Nothing
            Conn = Nothing
            wf.Destroy()
            wf = Nothing

        End Try

        Return WFCount

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for loading workflows by 
    ' sorted list only 
    ' ----------  ARPATECH  ----------
    Overloads Function LoadWorkflows(ByRef sl As SortedList)
        Dim el As New EventLog()
        el.Source = "GSSupportManager.LoadWorkflows"
        Dim Conn As New SqlConnection(DBString)
        Dim WFCount As Integer

        WFCount = 0
        Dim wf As Workflow
        Conn.Open()

        Dim Cmd As New SqlCommand()
        ' Cmd.Transaction = myTrans
        Try

            Dim dr As SqlDataReader
            Cmd.CommandText = "select * from tblWorkflow where isActive=1"
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader


            While dr.Read()

                ' First check if wf exists and update date

                ' Create a new wf object
                wf = New Workflow( _
                dr.GetInt32(dr.GetOrdinal("workflowid")), _
                dr.GetString(dr.GetOrdinal("workflowname")), _
                dr.GetBoolean(dr.GetOrdinal("cancelonerror")), _
                   dr.GetBoolean(dr.GetOrdinal("oncepersla")), _
                dr.GetBoolean(dr.GetOrdinal("makeinactiveoncompletion")), _
                dr.GetDateTime(dr.GetOrdinal("lastUpdate")))

                wf.GSListener = GSListener
                wf.GSManager = Me

                sl.Add(wf.WorkflowID, wf)
                SendToQ(WorkflowQ, wf.CreateMQMessage())

                WFCount = WFCount + 1

            End While

            dr.Close()

        Catch e As Exception
            el.WriteEntry("Failed to LoadWorkflows:" & e.Message)
            '   myTrans.Rollback()

        Finally

            Conn.Close()
            Cmd = Nothing
            el = Nothing
            Conn = Nothing

        End Try

        Return WFCount

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for loading workflows by 
    ' provided ID
    ' ----------  ARPATECH  ----------
    Function LoadWorkflow(ByVal wfid As Integer) As Integer
        Dim el As New EventLog()
        el.Source = "GSSupportManager.LoadWorkflows"
        Dim Conn As New SqlConnection(DBString)
        Dim WFCount As Integer

        WFCount = 0

        Conn.Open()

        Dim Cmd As New SqlCommand()
        ' Cmd.Transaction = myTrans
        Try

            Dim dr As SqlDataReader
            Cmd.CommandText = "select * from tblWorkflow where workflowid=" & wfid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                ' First check if wf exists and update date

                ' Create a new wf object
                Dim wf As New Workflow( _
                dr.GetInt32(dr.GetOrdinal("workflowid")), _
                dr.GetString(dr.GetOrdinal("workflowname")), _
                dr.GetBoolean(dr.GetOrdinal("cancelonerror")), _
                  dr.GetBoolean(dr.GetOrdinal("oncepersla")), _
                dr.GetBoolean(dr.GetOrdinal("makeinactiveoncompletion")), _
                dr.GetDateTime(dr.GetOrdinal("lastUpdate")))

                wf.GSListener = GSListener
                wf.GSManager = Me

                SendToQ(WorkflowQ, wf.CreateMQMessage())
                WFCount = WFCount + 1

            End While

            dr.Close()

        Catch e As Exception
            el.WriteEntry("Failed to LoadWorkflows:" & e.Message)
            '   myTrans.Rollback()

        Finally

            Conn.Close()
            Cmd = Nothing
            el = Nothing
            Conn = Nothing

        End Try

        Return WFCount

    End Function



    ' ----------  ARPATECH  ----------
    ' Function for loading workflows by 
    ' sorted list and ID
    ' ----------  ARPATECH  ----------
    Function LoadWorkflow(ByVal wfid As Integer, ByRef sl As SortedList) As Integer
        Dim el As New EventLog()
        el.Source = "GSSupportManager.LoadWorkflows"
        Dim Conn As New SqlConnection(DBString)
        Dim WFCount As Integer

        WFCount = 0

        Conn.Open()

        Dim Cmd As New SqlCommand()
        ' Cmd.Transaction = myTrans
        Try

            Dim dr As SqlDataReader
            Cmd.CommandText = "select * from tblWorkflow where workflowid=" & wfid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                ' First check if wf exists and update date

                ' Create a new wf object
                Dim wf As New Workflow( _
                dr.GetInt32(dr.GetOrdinal("workflowid")), _
                dr.GetString(dr.GetOrdinal("workflowname")), _
                dr.GetBoolean(dr.GetOrdinal("cancelonerror")), _
                   dr.GetBoolean(dr.GetOrdinal("oncepersla")), _
                dr.GetBoolean(dr.GetOrdinal("makeinactiveoncompletion")), _
                dr.GetDateTime(dr.GetOrdinal("lastUpdate")))

                wf.GSListener = GSListener
                wf.GSManager = Me

                sl.Add(wf.WorkflowID, wf)

                SendToQ(WorkflowQ, wf.CreateMQMessage())
                WFCount = WFCount + 1

            End While

            dr.Close()


        Catch e As Exception
            el.WriteEntry("Failed to LoadWorkflows:" & e.Message)
            '   myTrans.Rollback()

        Finally

            Conn.Close()
            Cmd = Nothing
            el = Nothing
            Conn = Nothing

        End Try

        Return WFCount

    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Function CreateARQs() As Integer Implements IGSSupportManager.CreateARQs

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for dispating action
    ' request by string
    ' ----------  ARPATECH  ----------
    Function DispatchAR(ByVal ar As String) As Boolean Implements IGSSupportManager.DispatchAR
        Dim b As Boolean
        b = True

        DispatchAR = b

    End Function



    ' ----------  ARPATECH  ----------
    ' Function for dispating action
    ' request by queue
    ' ----------  ARPATECH  ----------
    Function DispatchARQual(ByVal ar As String) As Boolean Implements IGSSupportManager.DispatchARQual
        Dim b As Boolean
        Dim qualQ As New System.Messaging.MessageQueue(QualificationQ)
        Dim arQ As New System.Messaging.MessageQueue(IncomingARQ)
        Dim mes As New System.Messaging.Message()

        If Not arQ.CanRead Then
            LogEvent("Application", "Unable to read " & ar & " from Incoming AR Q")
        Else

        End If


        b = True

        Return b

    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY subroutine 
    ' ----------  ARPATECH  ----------
    Sub TestWF(ByVal str As String) Implements IGSSupportManager.TestWF


    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' Function for getting SLA 
    ' by provided SLA ID
    ' ----------  ARPATECH  ----------
    Function GetSLA(ByVal slaid As Integer) As SLA Implements IGSSupportManager.GetSLA
        Dim s As New SLA(slaid)

        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting SLA monitor 
    ' by provided SLA monitor ID
    ' ----------  ARPATECH  ----------
    Function GetSLAMonitor(ByVal slaiid As Integer) As SLAMonitor Implements IGSSupportManager.GetSLAMonitor
        Dim s As New SLAMonitor(slaiid)
        s.Manager = Me

        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for loading SLA 
    ' and returns loaded count
    ' ----------  ARPATECH  ----------
    Function LoadSLAs(Optional ByVal slaid As Integer = 0) As Integer
        Dim Conn As New SqlConnection(DBString)
        Dim Conn2 As New SqlConnection(DBString)
        Dim SLACount As Integer = 0
        Dim sql As New String("")

        Conn.Open()
        Conn2.Open()

        Dim Cmd As New SqlCommand()

        Try
            Dim dr As SqlDataReader

            sql = "select slaid from tblSLA where IsActive=1"

            If slaid > 0 Then
                sql = sql & " and tblSLA.slaid=" & slaid & " "
            End If

            Cmd.CommandText = sql
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                ' Create new Schedule Object
                '   EventLog.WriteEntry("Application", "Creating SLA with Conn" & dr.GetInt32(dr.GetOrdinal("slaid")))

                Dim s As New SLA(dr.GetInt32(dr.GetOrdinal("slaid")), Conn2)
                s.Manager = Me
                s.GSListener = GSListener

                ' s.RestartListener()

                SendToQ(SLAQ, s.CreateMQMessage())
                SLACount = SLACount + 1

            End While
            dr.Close()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to load SLAs: " & e.Message)
            '   myTrans.Rollback()

        Finally
            Conn.Close()
            Conn2.Close()
            '  Conn.Dispose()
            '  Conn2.Dispose()
            '  Conn2 = Nothing
            '  Conn = Nothing
            '  Cmd = Nothing

        End Try

        Return SLACount

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for loading SLA already 
    ' in progress and returns loaded count
    ' ----------  ARPATECH  ----------
    Function LoadSLAsInProgress() As Integer
        Dim Conn As New SqlConnection(DBString)
        Dim SLACount As Integer = 0
        Dim sql As New String("")


        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try
            Dim dr As SqlDataReader

            sql = "select slainstanceid from tblSLATracking slat where InProgress=1 and " & _
            " ticketid not in (select ticketid from tblTicket where tktstatusid in (" & _
            " select tktstatusid from tblTicketStatus where isClosed=1 or isCancelled=1))"

            Cmd.CommandText = sql
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                ' Create new Schedule Object


                Dim slam As New SLAMonitor(dr.GetInt32(dr.GetOrdinal("slainstanceid")))

                slam.Manager = Me
                slam.GSListener = Me.GSListener

                SendToQ(SLAInProgressQ, slam.CreateMQMessage())


                SLACount = SLACount + 1

            End While
            dr.Close()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to load SLAs In Progress: " & e.Message)
            '   myTrans.Rollback()

        Finally
            Conn.Close()

            Conn = Nothing
            Cmd = Nothing

        End Try

        Return SLACount

    End Function


    ' ----------  ARPATECH  ----------
    ' Function for loading schedules as per
    ' provided ID
    ' ----------  ARPATECH  ----------
    Function LoadSchedules(Optional ByVal sid As Integer = 0) As Integer

        Dim Conn As New SqlConnection(DBString)
        Dim SkedCount As Integer = 0
        Dim sql As New String("")


        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try
            Dim dr As SqlDataReader

            sql = "select tblFields.fieldid, IsNull(tblFields.vbConst, 0) as 'vbconst', tblSchedule.ScheduleID, tblSchedule.ActionID, tblSchedule.IsActive, tblSchedule.IsDaily, " & _
            "tblSchedule.ScheduleName, tblSchedule.IsWeekly,  tblSchedule.IsMonthly, tblSchedule.isOnce,  " & _
            "Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, January, February, March, April, " & _
            "May, June, July, August, September, October, November, December, " & _
            "tblSchedule.repeatInterval, tblSchedule.StartDateTZ, tblSchedule.StartDate, tblSchedule.DayOfMonth, " & _
            "max(actionstart) as lastexecuted, actionname, timezonename from tblSchedule, tblScheduleDays, tblScheduleMonths, tblActions, tblTimeZones, tblActionTracking, tblFields where " & _
            "tblSchedule.actionid = tblActions.actionid and tblScheduleDays.scheduleid =* tblSchedule.scheduleid  " & _
            "and tblScheduleMonths.scheduleid =* tblSchedule.scheduleid and tblSchedule.startdatetz = tblTimeZones.TimeZoneID and " & _
            "tblActionTracking.scheduleid =* tblSchedule.scheduleid and tblFields.fieldid =* tblTimeZones.fieldid "

            If sid > 0 Then
                sql = sql & " and tblSchedule.scheduleid=" & sid & " "
            End If

            sql = sql & "group by tblSchedule.ScheduleID, tblSchedule.ActionID, tblSchedule.ScheduleName,  " & _
            "tblSchedule.repeatInterval, tblSchedule.IsActive, tblSchedule.IsDaily, " & _
            "tblSchedule.IsWeekly, tblSchedule.IsMonthly, tblSchedule.isOnce, tblSchedule.StartDateTZ, tblSchedule.StartDate, " & _
            "tblSchedule.DayOfMonth, tblScheduleDays.ScheduleID, tblScheduleDays.Monday, tblScheduleDays.Tuesday, tblScheduleDays.Wednesday, " & _
            "tblScheduleDays.Thursday,  tblScheduleDays.Friday,  tblScheduleDays.Saturday, tblScheduleDays.Sunday, " & _
            "tblScheduleDays.IsActive, tblScheduleDays.lastUpdate, tblScheduleDays.lastUpdateUserID,  " & _
            "tblScheduleMonths.January, tblScheduleMonths.February, tblScheduleMonths.March, tblScheduleMonths.April, " & _
            "tblScheduleMonths.May, tblScheduleMonths.June, tblScheduleMonths.July, tblScheduleMonths.August, " & _
            "tblScheduleMonths.September, tblScheduleMonths.October, tblScheduleMonths.November, tblScheduleMonths.December, " & _
            "tblScheduleMonths.isActive, tblScheduleMonths.lastUpdate, tblScheduleMonths.lastUpdateUserID, " & _
            "tblScheduleMonths.ScheduleID, tblActions.ActionName, tblTimeZones.TimeZoneName, tblFields.fieldid, tblFields.vbConst"

            Cmd.CommandText = sql
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                ' Create new Schedule Object
                Dim s As New Schedule(dr.GetInt32(dr.GetOrdinal("scheduleid")))
                Dim tz As Integer = dr.GetInt32(dr.GetOrdinal("vbConst"))

                s.ActionID = dr.GetInt32(dr.GetOrdinal("actionid"))

                s.IsDaily = dr.GetBoolean(dr.GetOrdinal("isDaily"))

                s.IsWeekly = dr.GetBoolean(dr.GetOrdinal("isWeekly"))

                s.IsMonthly = dr.GetBoolean(dr.GetOrdinal("isMonthly"))
                s.IsOnce = dr.GetBoolean(dr.GetOrdinal("isOnce"))

                s.StartDateTZ = dr.GetInt32(dr.GetOrdinal("startDateTZ"))

                '  If tz <> ServerTimeZone Then
                ' s.StartDate = ConvertTime(dr.GetDateTime(dr.GetOrdinal("startDate")), ServerTimeZone, s.StartDateTZ)

                '   Else
                s.StartDate = dr.GetDateTime(dr.GetOrdinal("startDate"))

                '  End If

                s.RepeatInterval = dr.GetInt32(dr.GetOrdinal("repeatInterval"))
                s.ScheduleName = dr.GetString(dr.GetOrdinal("schedulename"))
                s.IsActive = dr.GetBoolean(dr.GetOrdinal("isActive"))

                If Not dr.IsDBNull(dr.GetOrdinal("lastexecuted")) Then
                    If tz <> ServerTimeZone Then
                        s.LastExecuteDate = ConvertTime(dr.GetInt32(dr.GetOrdinal("lastexecuted")), ServerTimeZone, s.StartDateTZ)
                    Else
                        s.LastExecuteDate = dr.GetDateTime(dr.GetOrdinal("lastexecuted"))
                    End If
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("fieldid")) Then

                    s.TZFieldID = dr.GetInt32(dr.GetOrdinal("fieldid"))

                End If

                If s.IsMonthly Then
                    s.DayOfMonth = dr.GetInt32(dr.GetOrdinal("dayofmonth"))
                    If Not dr.IsDBNull(dr.GetOrdinal("January")) AndAlso dr.GetBoolean(dr.GetOrdinal("January")) Then
                        s.Months.Add(vbJanuary)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("February")) AndAlso dr.GetBoolean(dr.GetOrdinal("February")) Then
                        s.Months.Add(vbFebruary)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("March")) AndAlso dr.GetBoolean(dr.GetOrdinal("March")) Then
                        s.Months.Add(vbMarch)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("April")) AndAlso dr.GetBoolean(dr.GetOrdinal("April")) Then
                        s.Months.Add(vbApril)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("May")) AndAlso dr.GetBoolean(dr.GetOrdinal("May")) Then
                        s.Months.Add(vbMay)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("June")) AndAlso dr.GetBoolean(dr.GetOrdinal("June")) Then
                        s.Months.Add(vbJune)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("July")) AndAlso dr.GetBoolean(dr.GetOrdinal("July")) Then
                        s.Months.Add(vbJuly)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("August")) AndAlso dr.GetBoolean(dr.GetOrdinal("August")) Then
                        s.Months.Add(vbAugust)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("September")) AndAlso dr.GetBoolean(dr.GetOrdinal("September")) Then
                        s.Months.Add(vbSeptember)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("October")) AndAlso dr.GetBoolean(dr.GetOrdinal("October")) Then
                        s.Months.Add(vbOctober)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("November")) AndAlso dr.GetBoolean(dr.GetOrdinal("November")) Then
                        s.Months.Add(vbNovember)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("December")) AndAlso dr.GetBoolean(dr.GetOrdinal("December")) Then
                        s.Months.Add(vbDecember)
                    End If

                ElseIf s.IsWeekly Then
                    If Not dr.IsDBNull(dr.GetOrdinal("Sunday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Sunday")) Then
                        s.Months.Add(vbSunday)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("Monday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Monday")) Then
                        s.Months.Add(vbMonday)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("Tuesday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Tuesday")) Then
                        s.Months.Add(vbTuesday)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("Wednesday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Wednesday")) Then
                        s.Months.Add(vbWednesday)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("Thursday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Thursday")) Then
                        s.Months.Add(vbThursday)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("Friday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Friday")) Then
                        s.Months.Add(vbFriday)
                    End If
                    If Not dr.IsDBNull(dr.GetOrdinal("Saturday")) AndAlso dr.GetBoolean(dr.GetOrdinal("Saturday")) Then
                        s.Months.Add(vbSaturday)
                    End If

                End If
                '  s.RestartListener()

                s.GSListener = GSListener
                s.GSManager = Me

                SendToQ(ScheduleQ, s.CreateMQMessage())
                SkedCount = SkedCount + 1

            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to load schedules: " & e.Message)
            '   myTrans.Rollback()

        Finally
            Conn.Close()

            Conn = Nothing
            Cmd = Nothing

        End Try

        Return SkedCount

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for retriving activated schedule count
    ' ----------  ARPATECH  ----------
    Shared Function ActiveScheduleCount() As Integer
        Dim i As Integer = 0
        Dim sQ As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & WorkflowQ)
        i = UBound(sQ.GetAllMessages()) + 1
        Return i

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for retriving activated workflow count
    ' ----------  ARPATECH  ----------
    Shared Function ActiveWorkflowCount() As Integer
        Dim i As Integer = 0
        Dim wfQ As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & WorkflowQ)
        i = UBound(wfQ.GetAllMessages()) + 1
        Return i

    End Function



    ' ----------  ARPATECH  ----------
    ' Function for retriving work flow in progress count
    ' ----------  ARPATECH  ----------
    Shared Function WorkflowsInProgressCount() As Integer
        Dim i As Integer = 0
        Dim wfQ As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & WorkflowInProgressQ)
        i = UBound(wfQ.GetAllMessages()) + 1
        Return i

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for retriving activated SLA count
    ' ----------  ARPATECH  ----------
    Shared Function ActiveSLACount() As Integer
        Dim i As Integer = 0
        Dim sQ As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & SLAQ)
        i = UBound(sQ.GetAllMessages()) + 1
        Return i

    End Function

    ' Remove AR from Qualification Q and
    Shared Sub ARPostProc(ByVal ar As ActionRequest)
        Dim s As New String("")
        Dim qPath As New String("")
        If ar.Qualify = False Then
            qPath = ActionPreProcQ
        Else
            qPath = QualificationQ

        End If
        If Not IsNothing(ar.MQLabel) Then

            Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & qPath)
            Dim m As New System.Messaging.Message()
            Dim menum As System.Messaging.MessageEnumerator
            menum = mqq.GetMessageEnumerator()
            While menum.MoveNext()
                If menum.Current.Label = ar.MQLabel Then
                    m = menum.Current
                    menum.RemoveCurrent()
                    SendToQ(ARPostProcQ, m)
                    Exit While

                End If
            End While

        End If

    End Sub

    ' Remove WF from WFInProgress Q to WFPostProcQ
    Shared Sub WFPostProc(ByVal label As String, Optional ByVal MakeInactive As Boolean = False, Optional ByVal wfLabel As String = "")

        Try
            Dim strFormatName As String
            Dim qinfo As New MSMQQueueInfo()
            Dim qDest As MSMQQueue

            ' Create a direct format name.
            strFormatName = "DIRECT=OS:" & GSSupportManager.GetQHost() & WorkflowInProgressQ

            ' Set the FormatName property of the MSMQQueueInfo object.

            qinfo.FormatName = strFormatName

            ' Open the queue.
            qDest = qinfo.Open(MQ_RECEIVE_ACCESS, MQ_DENY_NONE)
            qDest.Reset()


            Dim m As MSMQMessage = qDest.PeekCurrent(ReceiveTimeout:=500)

            ' Peek at messages in the queue using the cursor.
            Do While Not m Is Nothing
                If m.Label = label Then
                    qDest.ReceiveCurrent()
                    Exit Do
                End If
                m = qDest.PeekNext(ReceiveTimeout:=500)
            Loop

            qDest.Close()

            If MakeInactive Then
                ' Create a direct format name.
                strFormatName = "DIRECT=OS:" & GSSupportManager.GetQHost() & WorkflowQ

                ' Set the FormatName property of the MSMQQueueInfo object.

                qinfo.FormatName = strFormatName

                ' Open the queue.
                qDest = qinfo.Open(MQ_RECEIVE_ACCESS, MQ_DENY_NONE)
                qDest.Reset()
                m = qDest.Peek()

                While Not IsNothing(m)
                    If m.Label = label Then
                        qDest.ReceiveCurrent()
                        Exit While
                    End If
                    m = qDest.PeekNext
                End While

                qDest.Close()


            End If



        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Postprocessing " & label & ": " & e.Message)

        End Try


    End Sub

    ' Delete AR from ActionPreproc and put ActionMonitor in ActionPostProc
    Shared Sub ActionPostProc(ByVal arlabel As String, ByVal arm As MSMQMessage)

        SendToQ(ActionPostProcQ, arm)


    End Sub

    ' Remove AR from Qualification Q and
    ' ----------  ARPATECH  ----------
    ' subroutine for empting message queue
    ' ----------  ARPATECH  ----------
    Shared Sub EmptyQ(ByVal QName As String)

        Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & QName)
        mqq.Purge()

    End Sub

    ' Remove AR from Qualification Q and
    ' ----------  ARPATECH  ----------
    ' subroutine for completing action
    ' provided by lable
    ' ----------  ARPATECH  ----------
    Shared Sub ActionComplete(ByVal label As String)

        Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & ActionPostProcQ)
        Dim m As New System.Messaging.Message()
        Dim menum As System.Messaging.MessageEnumerator
        menum = mqq.GetMessageEnumerator()
        While menum.MoveNext()
            If menum.Current.Label = label Then
                m = menum.Current
                menum.RemoveCurrent()

                SendToQ(ActionCompleteQ, m)

                Exit While

            End If
        End While

    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for completing schedule
    ' provided by lable
    ' ----------  ARPATECH  ----------
    Shared Sub ScheduleComplete(ByVal label As String)

        Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & ScheduleQ)
        Dim m As New System.Messaging.Message()
        Dim menum As System.Messaging.MessageEnumerator
        menum = mqq.GetMessageEnumerator()
        While menum.MoveNext()
            If menum.Current.Label = label Then
                m = menum.Current
                menum.RemoveCurrent()

                Exit While

            End If
        End While

    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for completing SLA 
    ' provided by lable
    ' ----------  ARPATECH  ----------
    Shared Sub SLAComplete(ByVal label As String)

        Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & SLAInProgressQ)
        Dim m As New System.Messaging.Message()
        Dim menum As System.Messaging.MessageEnumerator
        menum = mqq.GetMessageEnumerator()
        While menum.MoveNext()
            If menum.Current.Label = label Then
                m = menum.Current
                menum.RemoveCurrent()
                m.Dispose()

                Exit While

            End If
        End While

    End Sub





    ' ----------  ARPATECH  ----------
    ' subroutine for restarting support service
    ' ----------  ARPATECH  ----------
    Public Shared Sub RestartService()

        Dim i
        Dim p As Process()
        Dim p2 As New Process()
        p = Process.GetProcessesByName("GSSupportSvc.ex")
        For i = 0 To UBound(p) - 1
            p(i).Kill()
        Next

        p2.StartInfo.UseShellExecute = True
        p2.StartInfo.CreateNoWindow = True
        p2.StartInfo.FileName = "net"
        p2.StartInfo.Arguments = "start gssupportsvc"
        p2.Start()
    End Sub







    ' ----------  ARPATECH  ----------
    ' Function for parsing action request text
    ' by provided string value
    ' ----------  ARPATECH  ----------
    Public Shared Function ParseTextAR(ByVal ar As ActionRequest, ByVal s As String) As String

        Dim i, n
        Dim arChunks As String()
        Dim ns As New String(s)

        ' iterate through fields and see if there's any text
        For n = 0 To ar.Fields.Count - 1


            If Not IsNothing(ar.Fields.GetByIndex(n)) Then

                Dim f As Field = ar.Fields.GetByIndex(n)
                '   EventLog.WriteEntry("Application", "Testing " & FieldReplaceDelim & UCase(f.FieldName) & FieldReplaceDelim)
                Dim tests As New String(FieldReplaceDelim & UCase(f.FieldName) & FieldReplaceDelim)
                ns = ns.Replace(tests, f.FieldDisplayValue)

            End If
        Next


        Return ns
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for replacing provided date
    ' from current date
    ' ----------  ARPATECH  ----------
    Public Shared Function ReplaceDate(ByVal d As Date) As Date
        Dim dt As New Date(d.Ticks)

        If IsDate(d) Then
            Dim y As Integer = CInt(d.Year)
            If y < MinYear Then
                dt = New Date(Now().Year, Now.Month, Now.Day, d.Hour, d.Minute, d.Second)
            End If

        End If
        Return dt

    End Function

    ' ----------  ARPATECH  ----------
    ' subroutine for logging action request
    ' details to log file
    ' ----------  ARPATECH  ----------
    Public Shared Sub LogARFields(ByVal ar As ActionRequest)
        Dim i As Integer
        Try
            For i = 0 To ar.Fields.Count - 1
                Dim f As New Field()
                If Not IsNothing(ar.Fields.GetByIndex(i)) Then
                    f = ar.Fields.GetByIndex(i)
                    EventLog.WriteEntry("Application", CStr(ar.Fields.GetKey(i)) & ", " & f.FieldName & ", " & f.FieldTextValue & ", " & f.FieldDisplayValue)
                End If
            Next


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error logging ar fields:" & e.Message)
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for loading action request
    ' details to memory variables
    ' ----------  ARPATECH  ----------
    Public Shared Sub LogARDetails(ByVal ar As ActionRequest)
        Dim i As Integer

        Dim sql As String

        Try
            Dim Conn As New SqlConnection(DBString)

            Conn.Open()


            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Cmd.CommandTimeout = 200000





            For i = 0 To ar.Fields.Count - 1
                Dim f As New Field()
                If Not IsNothing(ar.Fields.GetByIndex(i)) Then


                    f = ar.Fields.GetByIndex(i)
                    sql = "insert into tblARDetailLog (arid, fieldID, fieldName, fieldTextValue, fieldDisplayValue) " & _
                    " values (" & ar.ActionRequestID & "," & f.FieldID & ",'" & f.FieldName & "','" & Replace(f.FieldTextValue, "'", "''") & "','" & Replace(f.FieldDisplayValue, "'", "''") & "')"
                    Cmd.CommandText = sql
                    Cmd.ExecuteNonQuery()

                End If
            Next


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error logging ar fields (" & sql & "):" & e.Message)
        End Try

    End Sub



    ' ----------  ARPATECH  ----------
    ' Function for converting time 
    ' from one TZ to another
    ' ----------  ARPATECH  ----------
    Public Function ConvertTime(ByVal date1, ByVal tz1, ByVal tz2) As Date
        'EventLog.WriteEntry("Application", "Converting " & date1 & " from " & tz1 & " to " & tz2)
        ' Dim TimeConverter = New GSDates.GSTimeZoneClass()
        Dim TimeConverter = New GSTime.TimeConverter()
        Return TimeConverter.ConvertTime(date1, tz1, tz2)

    End Function





    ' ----------  ARPATECH  ----------
    ' Function for loading schedule by
    ' provided schedule ID
    ' ----------  ARPATECH  ----------
    Function LoadSchedule(ByVal sid As Integer) As Boolean Implements IGSSupportManager.LoadSchedule
        Dim b As Boolean = True
        Try
            LoadSchedules(sid)
        Catch e As Exception
            b = False
            EventLog.WriteEntry("Application", "Error loading schedule " & sid & ": " & e.Message)

        End Try
        Return b


    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting VB constant
    ' by customer table
    ' ----------  ARPATECH  ----------
    Shared Function GetTimeZoneFromCustomerAddress(ByVal addressid As Integer)
        Dim countryid, timezoneid
        Dim Conn As New SqlConnection(DBString)
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        sql = "select IsNull(timezoneid, 0) as timezoneid, isnull(countryid, 0) as countryid from tblCustomerAddresses where customeraddressid=" & addressid


        Cmd.Connection = Conn
        Try

            Cmd.CommandText = sql

            Dim dr As SqlDataReader
            dr = Cmd.ExecuteReader
            While dr.Read()

                countryid = dr.GetInt32(dr.GetOrdinal("countryid"))
                timezoneid = dr.GetInt32(dr.GetOrdinal("timezoneid"))


            End While
            dr.Close()

            If timezoneid = 0 And countryid <> 0 Then
                sql = "select isnull(timezoneid, 0) as timezoneid from tblCountryDefaultTimeZone where countryid=" & countryid

                Cmd.CommandText = sql

                dr = Cmd.ExecuteReader

                While dr.Read()
                    timezoneid = dr.GetInt32(dr.GetOrdinal("timezoneid"))
                End While
            End If


            dr.Close()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error gettimg time zone: " & e.Message & ", " & sql)


        Finally
            Conn.Close()

        End Try
        Return timezoneid
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting VB constant
    ' from Time Zone table
    ' ----------  ARPATECH  ----------
    Shared Function GetTimeZoneVBConst(ByVal timezoneid As Integer)
        Dim fieldid
        Dim Conn As New SqlConnection(DBString)
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader

            sql = "select vbconst from tblfields where fieldid= (select isnull(fieldid, 0) from tblTimeZones where timezoneid=" & timezoneid & ")"

            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            While dr.Read()
                fieldid = dr.GetInt32(dr.GetOrdinal("vbconst"))
            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error gettimg time zone vbconst: " & e.Message)


        Finally
            Conn.Close()

        End Try
        Return fieldid

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting VB constant
    ' from Time Zone table
    ' ----------  ARPATECH  ----------
    Shared Function GetVBConstTimeZone(ByVal vbconst As Integer)
        Dim fieldid
        Dim Conn As New SqlConnection(DBString)
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader


            sql = "select timezoneid from tblTimeZones where fieldid= (select isnull(fieldid, 0) from tblFields where vbConst=" & vbconst & ")"

            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            While dr.Read()
                fieldid = dr.GetInt32(dr.GetOrdinal("timezoneid"))
            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error gettimg time zone id from vbconst: " & e.Message)


        Finally
            Conn.Close()

        End Try

        Return fieldid

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting time zone 
    ' field ID from database
    ' ----------  ARPATECH  ----------
    Shared Function GetTimeZoneFieldID(ByVal timezoneid As Integer)
        Dim fieldid
        Dim Conn As New SqlConnection(DBString)
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader


            sql = "select fieldid from tblTimeZones where timezoneid=" & timezoneid

            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            While dr.Read()
                fieldid = dr.GetInt32(dr.GetOrdinal("fieldid"))
            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error gettimg time zone: " & e.Message)


        Finally
            Conn.Close()

        End Try
        Return fieldid

    End Function

    ' ----------  ARPATECH  ----------
    ' subroutine for raising ticketing event
    ' ----------  ARPATECH  ----------
    Public Sub RaiseTicketEvent(ByVal o As Object, ByVal e As TicketEventArgs)
        Try



            RaiseEvent TicketEvent(o, e)

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Unable to raise ticket event:" & ex.Message())

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for sending action event
    ' to event log
    ' ----------  ARPATECH  ----------
    Public Sub SendActionEvent(ByVal am As ActionMonitor) Implements IGSSupportManager.SendActionEvent
        Dim m As System.Messaging.Message
        If IsNothing(am.Action) Then
            EventLog.WriteEntry("Application", "Action is nothing")
            Return
        End If
        m = am.CreateMQMessage
        m.Label = Now() & ActionCompleteString
        SendToQ(IncomingARQ, m)
    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for removing message from queue
    ' ----------  ARPATECH  ----------
    Shared Sub RemoveFromQ(ByVal qname As String, ByVal label As String)


        Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & qname)
        Dim m As New System.Messaging.Message()
        Dim menum As System.Messaging.MessageEnumerator
        menum = mqq.GetMessageEnumerator()
        While menum.MoveNext()
            If menum.Current.Label = label Then
                m = menum.Current
                menum.RemoveCurrent()

                Exit While

            End If
        End While

    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for destrying SLA monitor event
    ' ----------  ARPATECH  ----------
    Shared Sub DestroySLAMonitor(ByRef slam As SLAMonitor)
        Try
            Dim label = slam.MQLabel

            Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & SLAInProgressQ)
            Dim m As New System.Messaging.Message()
            Dim menum As System.Messaging.MessageEnumerator
            menum = mqq.GetMessageEnumerator()
            While menum.MoveNext()
                If menum.Current.Label = label Then
                    m = menum.Current
                    menum.RemoveCurrent()

                    Exit While

                End If
            End While

            slam.RemoveHandlers()
            slam = Nothing
            If IsNothing(slam) Then
                EventLog.WriteEntry("Application", "Is nothing")

            End If
        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error destroying slamonitor: " & ex.Message)

        End Try

    End Sub
    ' ----------  ARPATECH  ----------
    ' Function for checking workflow's
    ' timer event relation
    ' ----------  ARPATECH  ----------
    Shared Function IsWorkFlowWithTimer(ByVal WorkFlowid As Integer) As Boolean
        Dim SQL As String
        Dim conn As New SqlConnection(DBString)

        SQL &= "if exists(select isTimerEvent from tblWorkFlow "
        SQL &= " where WorkFlowid = " & WorkFlowid & " And isTimerEvent = 1)"
        SQL &= "          select 1 as extant "
        SQL &= "else "
        SQL &= "         select 0 as extant "
        Try
            Dim cmd As New SqlCommand(SQL, conn)
            cmd.Connection.Open()
            Dim i As Integer = cmd.ExecuteScalar()
            conn.Close()
            Return i > 0
        Catch ex As System.Exception
            MsgBox(ex.ToString)
            Return False
        Finally
            Try
                If Not conn Is Nothing AndAlso conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            Catch
            End Try
        End Try
    End Function

    ' ----------  ARPATECH  ----------
    ' Function for getting string value
    ' from database of passed SQL
    ' ----------  ARPATECH  ----------
    Shared Function GetSingleDBValueString(ByVal sql As String) As String
        Dim s As New String("")

        If IsNothing(sql) OrElse Len(sql) < 1 Then
            Return s
        End If
        Dim Conn As New SqlConnection(DBString)
        Conn.Open()

        Dim Cmd As New SqlCommand()


        Cmd.Connection = Conn
        Try

            Dim dr As SqlDataReader


            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            While dr.Read()
                If Not IsDBNull(dr.GetString(0)) Then
                    s = "" & dr.GetString(0)
                End If

            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error single DB string value: " & e.Message & sql)


        Finally
            Conn.Close()

        End Try
        If Not IsNothing(s) Then
            Return s
        Else
            Return New String("")
        End If


    End Function


    ' ----------  ARPATECH  ----------
    ' Function for getting integer value
    ' from database of passed SQL
    ' ----------  ARPATECH  ----------
    Shared Function GetSingleDBValueInt(ByVal sql As String) As Integer
        Dim i As New Integer()
        i = -1
        If IsNothing(sql) Then
            Return i
        End If

        Dim Conn As New SqlConnection(DBString)
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Try
            Dim dr As SqlDataReader

            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            If Not IsNothing(dr) Then
                While dr.Read()
                    If Not IsDBNull(dr.GetInt32(0)) AndAlso Not IsNothing(dr.GetInt32(0)) Then
                        i = dr.GetInt32(0)
                    End If

                End While
                dr.Close()

            End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error single DB int value with sql " & sql & ": " & e.Message)
            i = -1

        Finally
            Conn.Close()

        End Try
        If IsNothing(i) OrElse Not IsNumeric(i) Then
            i = -1
        End If
        Return i

    End Function

    'Public Shared Function GetSFAEmails(ByVal sfaid As Integer, ByVal customerAddressID As Integer)
    '    Dim s As New String("")
    '    Dim sql As String = "select email from tblContact where contactid in (select sfacontactid from tblSFACustomerContact where sfaid=" & sfaid & "  And customerdestinationid = " & customerAddressID & " and offeringid in (select offeringid from tblOfferings where offeringshort='SUPPORT'))"
    '    'EventLog.WriteEntry("Application", sql)

    '    Dim Conn As New SqlConnection(DBString)
    '    Conn.Open()

    '    Dim Cmd As New SqlCommand()


    '    Cmd.Connection = Conn
    '    Try

    '        Dim dr As SqlDataReader

    '        Cmd.CommandText = sql
    '        dr = Cmd.ExecuteReader
    '        While dr.Read()
    '            Dim thisEmail As String = dr.GetString(0)
    '            s += thisEmail.Trim() + ";"

    '        End While
    '        dr.Close()

    '        If s.Length > 0 Then
    '            s = Mid(s, 1, s.Length - 1)
    '        End If

    '    Catch e As Exception
    '        EventLog.WriteEntry("Application", "Error getting SFA emails: " & e.Message)

    '    Finally
    '        Conn.Close()

    '    End Try
    '    Return s
    'End Function


    'Public Shared Function GetSFAEmails(ByVal sfaid As Integer, ByVal customerAddressID As Integer, ByVal ARTypeID As Integer)
    '    Dim s As New String("")

    '    Dim i As Integer = GetSingleDBValueInt("select count(sfacustomerid) from tblSFACustomerContact where artypeid=" & ARTypeID & " and customeraddressid=" & customerAddressID & " and sfaid = " & sfaid)
    '    Dim sql As String
    '    If i = 0 Then
    '        sql = "select email from tblContact where contactid in (select sfacontactid from tblSFACustomerContact where sfaid=" & sfaid & "  And customerdestinationid = " & customerAddressID & " and offeringid in (select offeringid from tblOfferings where offeringshort='SUPPORT'))"
    '    Else
    '        sql = "select email from tblContact where contactid in (select sfacontactid from tblSFACustomerContact where sfaid=" & sfaid & "  And customerdestinationid = " & customerAddressID & " and arTypeID=" & ARTypeID & " and  offeringid in (select offeringid from tblOfferings where offeringshort='SUPPORT'))"
    '    End If


    '    'EventLog.WriteEntry("Application", sql)

    '    Dim Conn As New SqlConnection(DBString)
    '    Conn.Open()

    '    Dim Cmd As New SqlCommand()


    '    Cmd.Connection = Conn
    '    Try

    '        Dim dr As SqlDataReader

    '        Cmd.CommandText = sql
    '        dr = Cmd.ExecuteReader
    '        While dr.Read()
    '            Dim thisEmail As String = dr.GetString(0)
    '            s += thisEmail.Trim() + ";"

    '        End While
    '        dr.Close()

    '        If s.Length > 0 Then
    '            s = Mid(s, 1, s.Length - 1)
    '        End If

    '    Catch e As Exception
    '        EventLog.WriteEntry("Application", "Error getting SFA emails: " & e.Message)

    '    Finally
    '        Conn.Close()

    '    End Try
    '    Return s
    'End Function

    ' ----------  ARPATECH  ----------
    ' Function for returnig SFA email address
    ' with SFA id
    ' ----------  ARPATECH  ----------
    Public Shared Function GetSFAEmails(ByVal SFAID As Integer) As String
        Dim s As New String("")
        Dim sql As String
        Dim Cmd As SqlCommand
        Dim dr As SqlDataReader
        Dim thisEmail As String
        Dim MissingEmail As New String("")
        Dim Conn As SqlConnection

        Conn = New SqlConnection(DBString)
        Try
            Conn.Open()
            Cmd = Conn.CreateCommand

            sql = "select ticketemail from tblprimecontractors where primecontractorname='GlobalServe' and isactive=1"
            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            If dr.HasRows Then
                If dr.Read() Then
                    MissingEmail = dr.GetString(0)
                End If
            End If
            If Not dr.IsClosed Then
                dr.Close()
            End If

            sql = "select distinct c.email from tblcontact c " & _
                " inner join tblrole r on r.rolename='Support Contact' and r.isactive=1" & _
                " inner join tblpartyrole pr on pr.partyid=c.partyid and pr.roleid=r.roleid and pr.isactive=1" & _
                " inner join tblsfas sfa on sfa.parentcompanyid=c.companyid" & _
                " where c.isactive=1 and c.email is not null and sfa.sfaid=" & SFAID
            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read()
                    thisEmail = dr.GetString(0)
                    If thisEmail.Trim().Length > 0 Then
                        s += thisEmail.Trim() + ";"
                    End If
                End While
            End If
            If Not dr.IsClosed Then
                dr.Close()
            End If

            If s.Length > 0 Then
                s = Mid(s, 1, s.Length - 1)
            Else
                s = MissingEmail
            End If
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error getting SFA emails: " & e.Message)
        Finally
            If Conn.State <> ConnectionState.Closed Then
                Conn.Close()
            End If
        End Try

        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for encoding decrypted text
    ' ----------  ARPATECH  ----------
    Public Function Base64Encode(ByVal plaintext As String) As String Implements IGSSupportManager.Base64Encode
        Dim binaryData As Byte()
        ReDim binaryData(plaintext.Length())
        Dim arS As Char()
        ReDim arS(plaintext.Length)
        arS = plaintext.ToCharArray()


        Dim i As Integer = 0


        For i = 0 To UBound(arS)
            Dim b As Byte = Microsoft.VisualBasic.AscW(arS(i))
            binaryData(i) = b
        Next

        Return System.Convert.ToBase64String(binaryData)

    End Function

    ' ----------  ARPATECH  ----------
    ' Function for decoding encrypted text
    ' ----------  ARPATECH  ----------
    Public Function Base64Decode(ByVal crypttext As String) As String Implements IGSSupportManager.Base64Decode
        Dim decoded As Byte() = System.Convert.FromBase64String(crypttext)

        Dim decodedStr = ""
        Dim i As Integer = 0
        For i = 0 To UBound(decoded)
            Dim c As Char = Microsoft.VisualBasic.ChrW(decoded(i))
            decodedStr = decodedStr & c
        Next

        Return decodedStr
    End Function


    ' ----------  ARPATECH  ----------
    ' subroutine for logging fields
    ' to event log file
    ' ----------  ARPATECH  ----------
    Public Shared Sub LogFieldsToEventLog()
        Dim i As Integer = 0
        Dim s As String = ""
        Dim sl As ArrayList = m_fieldsByDisplay.Values

        For i = 0 To sl.Count - 1
            Dim f As Field = sl.Item(i)
            If Not IsNothing(f) Then
                If Not IsNothing(f.FieldID) Then
                    s = s & f.FieldID & "="
                    If Not IsNothing(f.FieldTextValue) Then
                        s = s & f.FieldTextValue & vbCrLf
                    End If

                End If
            End If

        Next
        s = s & "END FIELDS"


        EventLog.WriteEntry("Application", s)

    End Sub

End Class

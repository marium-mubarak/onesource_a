' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains all arguments in properties for passing to Order Event.
' Inherits          :   ---------
' Implements        :   IOrderEventArgs
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System
Imports System.ComponentModel
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IOrderEventArgs
    Property SessionTrackingNumber() As Integer
    Property OrderTrackingNumber() As Integer
    Property DestinationTrackingNumber() As Integer
    Property BuyerUserID() As Integer
    Property BuyerUserName() As String
    Property SFAID() As Integer
    Property ChangeShipped() As Boolean
    Property ChangeAccepted() As Boolean
    Property ChangeDelivered() As Boolean
    Property ChangeETA() As Boolean
    Property ETA() As Date
    Property ActionInstanceID() As Integer
    Property ChangeItemShipped() As Boolean
    Property OrderItemID() As Integer
    Property ActionRequest() As ActionRequest


End Interface


<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class OrderEventArgs
    Implements IOrderEventArgs
    Private m_sessionTrackingNumber As Integer
    Private m_orderTrackingNumber As Integer
    Private m_destinationTrackingNumber As Integer
    Private m_buyerUserID As Integer
    Private m_buyerUserName As String
    Private m_buyerCustomerID As Integer
    Private m_sfaid As Integer
    Private m_changeShipped As Boolean = False
    Private m_changeItemShipped As Boolean = False
    Private m_changeDelivered As Boolean = False
    Private m_changeAccepted As Boolean = False
    Private m_chageDelivered As Boolean = False
    Private m_changeETA As Boolean = False
    Private m_eta As Date
    Private m_actionInstanceID As Integer
    Private m_orderItemID As Integer
    Private m_actionRequest As ActionRequest


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA order tracking number
    ' ----------  ARPATECH  ----------
    Property OrderTrackingNumber() As Integer Implements IOrderEventArgs.OrderTrackingNumber
        Get
            Return m_orderTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_orderTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action request
    ' ----------  ARPATECH  ----------
    Property ActionRequest() As ActionRequest Implements IOrderEventArgs.ActionRequest
        Get
            Return m_actionRequest
        End Get
        Set(ByVal Value As ActionRequest)
            m_actionRequest = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA session tracking number 
    ' ----------  ARPATECH  ----------
    Property SessionTrackingNumber() As Integer Implements IOrderEventArgs.SessionTrackingNumber
        Get
            Return m_sessionTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_sessionTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA destination tracking number
    ' ----------  ARPATECH  ----------
    Property DestinationTrackingNumber() As Integer Implements IOrderEventArgs.DestinationTrackingNumber
        Get
            Return m_destinationTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_destinationTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA buyer user ID
    ' ----------  ARPATECH  ----------
    Property BuyerUserID() As Integer Implements IOrderEventArgs.BuyerUserID
        Get
            Return m_buyerUserID
        End Get
        Set(ByVal Value As Integer)
            m_buyerUserID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA action instance ID
    ' ----------  ARPATECH  ----------
    Property ActionInstanceID() As Integer Implements IOrderEventArgs.ActionInstanceID
        Get
            Return m_actionInstanceID
        End Get
        Set(ByVal Value As Integer)
            m_actionInstanceID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA order item ID
    ' ----------  ARPATECH  ----------
    Property OrderItemID() As Integer Implements IOrderEventArgs.OrderItemID
        Get
            Return m_orderItemID
        End Get
        Set(ByVal Value As Integer)
            m_orderItemID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA's SFA ID
    ' ----------  ARPATECH  ----------
    Property SFAID() As Integer Implements IOrderEventArgs.SFAID
        Get
            Return m_sfaid
        End Get
        Set(ByVal Value As Integer)
            m_sfaid = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA buyer name
    ' ----------  ARPATECH  ----------
    Property BuyerUserName() As String Implements IOrderEventArgs.BuyerUserName
        Get
            Return m_buyerUserName
        End Get
        Set(ByVal Value As String)
            m_buyerUserName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA change shipped status
    ' ----------  ARPATECH  ----------
    Property ChangeShipped() As Boolean Implements IOrderEventArgs.ChangeShipped
        Get
            Return m_changeShipped
        End Get
        Set(ByVal Value As Boolean)
            m_changeShipped = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ETA item shipped status
    ' ----------  ARPATECH  ----------
    Property ChangeItemShipped() As Boolean Implements IOrderEventArgs.ChangeItemShipped
        Get
            Return m_changeItemShipped
        End Get
        Set(ByVal Value As Boolean)
            m_changeItemShipped = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Estimated time of arrival change accepted status
    ' ----------  ARPATECH  ----------
    Property ChangeAccepted() As Boolean Implements IOrderEventArgs.ChangeAccepted
        Get
            Return m_changeAccepted
        End Get
        Set(ByVal Value As Boolean)
            m_changeAccepted = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Estimated time of arrival delivered change status
    ' ----------  ARPATECH  ----------
    Property ChangeDelivered() As Boolean Implements IOrderEventArgs.ChangeDelivered
        Get
            Return m_changeDelivered
        End Get
        Set(ByVal Value As Boolean)
            m_changeDelivered = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Estimated time of arrival change status
    ' ----------  ARPATECH  ----------
    Property ChangeETA() As Boolean Implements IOrderEventArgs.ChangeETA
        Get
            Return m_changeETA
        End Get
        Set(ByVal Value As Boolean)
            m_changeETA = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Estimated time of arrival date
    ' ----------  ARPATECH  ----------
    Property ETA() As Date Implements IOrderEventArgs.ETA
        Get
            Return m_eta
        End Get
        Set(ByVal Value As Date)
            m_eta = Value
        End Set

    End Property





    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub
End Class

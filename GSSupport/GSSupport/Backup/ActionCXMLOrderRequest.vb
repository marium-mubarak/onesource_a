' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class basically generates XML file for order request with its complete
'                       information in a given file. This file can be later retrieved for any future
'                       usage. The file will be named like �OrderRequest_(session traccking
'                       number)_(current date & time).xml�.
' Inherits          :   Action
' Implements        :   IActionCXMLOrderRequest
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Xml.Schema       'Need this for cXML Validation
Imports System.Data
Imports System.Xml
Imports System.IO
Imports System.Collections
Imports System.Data.SqlClient

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IActionCXMLOrderRequest
    Property SessionTrackingNumber() As Integer
    Property OrderTrackingNumber() As Integer
    Property DestinationTrackingNumber() As Integer
    Property SFAID() As Integer
End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionCXMLOrderRequest
    Inherits Action
    Implements IActionCXMLOrderRequest
    Private m_orderTrackingNumber As Integer
    Private m_sessionTrackingNumber As Integer
    Private m_destinationTrackingNumber As Integer
    Private m_sfaID As Integer

    ' ----------  ARPATECH  ----------
    ' sub routine for executine this
    ' class instance
    ' ----------  ARPATECH  ----------
    Public Overrides Sub Execute()
        Try
            EventLog.WriteEntry("Application", "Execute 1")
            EventLog.WriteEntry("Application", "Session " & Me.SessionTrackingNumber)
            EventLog.WriteEntry("Application", "Order " & Me.OrderTrackingNumber)
            EventLog.WriteEntry("Application", "Destination " & Me.DestinationTrackingNumber)

            If m_sessionTrackingNumber < 1 Then
                EventLog.WriteEntry("Application", "Error excuting CXML Order Request: Session Tracking Number not set.")
                Exit Sub
            End If
            If m_orderTrackingNumber < 1 Then
                EventLog.WriteEntry("Application", "Error excuting CXML Order Request: Order Tracking Number not set.")
                Exit Sub
            End If
            If m_destinationTrackingNumber < 1 Then
                EventLog.WriteEntry("Application", "Error excuting CXML Order Request: Destination Tracking Number not set.")
                Exit Sub
            End If
            If m_sfaID < 1 Then
                EventLog.WriteEntry("Application", "Error excuting CXML Order Request: SFA ID not set.")
                Exit Sub
            End If

            cXMLOrderRequest(m_sessionTrackingNumber, m_orderTrackingNumber, m_destinationTrackingNumber, m_sfaID)


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error executing CXML Order Request: " & e.Message)
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Order Tracking Number
    ' ----------  ARPATECH  ----------
    Property OrderTrackingNumber() As Integer Implements IActionCXMLOrderRequest.OrderTrackingNumber
        Get
            Return m_orderTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_orderTrackingNumber = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Session Tracking Number
    ' ----------  ARPATECH  ----------
    Property SessionTrackingNumber() As Integer Implements IActionCXMLOrderRequest.SessionTrackingNumber
        Get
            Return m_sessionTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_sessionTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Destination Tracking Number
    ' ----------  ARPATECH  ----------
    Property DestinationTrackingNumber() As Integer Implements IActionCXMLOrderRequest.DestinationTrackingNumber
        Get
            Return m_destinationTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_destinationTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SFA Number
    ' ----------  ARPATECH  ----------
    Property SFAID() As Integer Implements IActionCXMLOrderRequest.SFAID
        Get
            Return m_sfaID
        End Get
        Set(ByVal Value As Integer)
            m_sfaID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine that converts Order request
    ' to XML and writes to an testing 
    ' Server File named args
    ' ----------  ARPATECH  ----------
    Public Sub cXMLOrderRequest(ByVal STN As Int32, ByVal OTN As Int32, ByVal DTN As Int32, ByVal SFAID As Int32)
        'Before starting to build XML message, output the input paramerters to a file. Helps to debug
        Dim InputParams = " SessionTrackingNumber: " & STN & _
              " OrderTrackingNumber: " & OTN & _
              " DestinationTrackingNumber: " & DTN & _
              " SFAID: " & SFAID
        Dim filename = "InputParameters_" & STN & "_" & Now() & ".txt"
        Dim FilePath
        FilePath = GSSupportManager.GeteConnLogPath()
        'Dim filename = "InputParameters_" & STN & ".txt"
        filename = SaturateFileNameString(filename)
        filename = FilePath & filename
        WriteFile(InputParams, filename)

        'Now create a filename to output the OrderRequest XML
        Dim args As String = "OrderRequest_" & STN & "_" & Now() & ".xml" 'With dateTime on the file name
        'Dim args As String = "OrderRequest_" & STN & ".xml"
        args = SaturateFileNameString(args)
        args = FilePath & args
        Dim GSOrderNumber
        Dim CustomerId
        Dim OrderedByUserID
        Dim OrderDate
        Dim OrderPO
        Dim BuyerCookie
        Dim BuyerCode
        Dim TotalOrderValueUSD
        Dim OrderCreatedByUserID
        Dim CustomerPartID
        Dim OrderItemID
        Dim OrderDeliveryDetailsID
        Dim CatalogStandardID
        Dim StdDescription
        Dim PartDescription
        Dim ManufacturerName
        Dim ManufacturerSKU
        Dim LineItemPrice
        Dim Quantity
        Dim LineItemTotal
        Dim MfgTrackingID
        Dim SupplierSKU
        Dim LineItemSeqNum
        Dim GroupIdentifier
        Dim GroupItemSeqNum
        Dim UNSPSC
        Dim CustomerName
        Dim UserName
        Dim DestinationPO
        Dim ShippingCustomerAddressName
        Dim ShippingCustomerAddressID
        Dim ShippingAddress1
        Dim ShippingAddress2
        Dim ShippingCity
        Dim ShippingStateProvinceRegion
        Dim ShippingPostalCode
        Dim ShippingCountryCodeID
        Dim ShippingCountryName
        Dim ShippingContact
        Dim ShippingPhone
        Dim BilltoCountryCodeID
        Dim BillingCustomerAddressID
        Dim BillingCustomerAddressName
        Dim BillingAddress1
        Dim BillingAddress2
        Dim BillingCity
        Dim BillingStateProvinceRegion
        Dim BillingPostalCode
        Dim BillingCountryName
        Dim BillingContact
        Dim BillingPhone
        Dim Total
        Dim Currency
        Dim OrderCountry
        Dim Comments
        Dim XMLLang = "en"
        Dim ParentCompanyID
        GSOrderNumber = STN & "-" & OTN & "-" & DTN

        Dim SFAName As String
        Dim OrderFormatID
        Dim PartnerId
        Dim ToDomain
        Dim FromDomain
        Dim SenderDomain
        Dim SharedSecret
        Dim version
        Dim TransactionSet
        Dim Flag
        Dim SenderIdentity
        Dim mydt As DataTable
        Dim mydr As DataRow
        Dim myds As DataSet
        Dim ISOCountryCodeShipTo
        Dim ISOCountryCodeBillTo
        Dim CurrencyCodeID
        Dim ISOCurrencyCode
        Dim ToCredentialIdentity
        Dim FromCredentialIdentity
        Dim UserAgent
        Dim SenderCredentialIdentity
        Dim PostURL

        Dim sqlConn As SqlConnection = New SqlConnection(GSSupportManager.GetDBString())
        sqlConn.Open()

        'Navigate TblOrders
        Dim SqlOrd As String = "SELECT CustomerID as CustomerID, OrderedByUserID as OrderedByUserID,  " & _
                                        " BuyerCookie as BuyerCookie, BuyerCode as BuyerCode, TotalOrderValueUSD as TotalOrderValueUSD, OrderCreatedByUserID as OrderCreatedByUserID FROM  tblOrders WHERE" & _
                                        " OrderTrackingNumber = " & OTN & " AND SessionTrackingNumber = " & STN

        myds = GetDataTable(SqlOrd)
        For Each mydr In myds.Tables("dt").Rows
            CustomerId = mydr("CustomerId").ToString
            OrderedByUserID = mydr("OrderedByUserID").ToString
            BuyerCookie = mydr("BuyerCookie").ToString
            BuyerCode = mydr("BuyerCode").ToString
            TotalOrderValueUSD = mydr("TotalOrderValueUSD").ToString
            OrderCreatedByUserID = mydr("OrderCreatedByUserID").ToString
        Next
        myds.Clear()
        mydr = Nothing

        'Navigate TblSFAs
        Dim SqlSFA As String = "SELECT * FROM  tblSFAs WHERE SFAID =" & SFAID
        myds = GetDataTable(SqlSFA)
        For Each mydr In myds.Tables("dt").Rows
            SFAName = mydr("Sfaname").ToString
            ParentCompanyID = mydr("ParentCompanyID").ToString
            OrderFormatID = mydr("OrderFormatID").ToString
        Next
        myds.Clear()
        mydr = Nothing
        'Navigate TblEconnectionParams
        Dim SqlEconnParam As String = "SELECT *  FROM  tblEconnectionParams where CompanyId = " & ParentCompanyID & " and eConnectionFormatId in (select eConnectionFormatId from tbleconnectionformats where econnectionformatname = 'XML') and Econnectiontypeid in (select Econnectiontypeid from tblEconnectionTypes where eConnectionTypeName = 'Order Outbound')"
        myds = GetDataTable(SqlEconnParam)
        For Each mydr In myds.Tables("dt").Rows
            PartnerId = mydr("PartnerId").ToString
            ToDomain = mydr("ToDomain").ToString
            ToCredentialIdentity = mydr("ToCredentialIdentity").ToString
            FromDomain = mydr("FromDomain").ToString
            FromCredentialIdentity = mydr("FromCredentialIdentity").ToString
            UserAgent = mydr("UserAgent").ToString
            SenderDomain = mydr("SenderDomain").ToString
            SenderCredentialIdentity = mydr("SenderCredentialIdentity").ToString
            SharedSecret = mydr("SharedSecret").ToString
            version = mydr("version").ToString
            Flag = mydr("Flag").ToString
            PostURL = mydr("HTTPPostURL").ToString
        Next
        myds.Clear()
        mydr = Nothing

        If Flag <> "" Then
            If UCase(Flag) = "P" Then
                Flag = "production"
            Else
                Flag = "test"
            End If
        End If
        'Navigate different Tbl to get order related info
        Dim SqlOrderSQL As String = "SELECT cu.CustomerName as CustomerName, u.UserName as UserName, " & _
        " o.OrderDate as OrderDate, o.OrderPO as OrderPO, od.DestinationPO as DestinationPO, " & _
        " sa.CustomerAddressName As ShippingCustomerAddressName, od.CustomerDestinationID as ShippingCustomerAddressID, " & _
        " sa.Address1 As ShippingAddress1, sa.Address2 As ShippingAddress2, sa.City As ShippingCity,  " & _
        " sa.StateProvinceRegion As ShippingStateProvinceRegion, sa.PostalCode As ShippingPostalCode, " & _
        " shco.CountryCodeId As ShippingCountryCodeID, shco.CountryName As ShippingCountryName, cd.Contact As ShippingContact, " & _
        " cd.Phone As ShippingPhone,  bico.CountryCodeID as BilltoCountryCodeID,od.CustomerInvoiceLocationID As BillingCustomerAddressID, " & _
        " ba.CustomerAddressName As BillingCustomerAddressName, ba.Address1 As BillingAddress1, ba.Address2 As BillingAddress2, ba.City As BillingCity," & _
        " ba.StateProvinceRegion As BillingStateProvinceRegion, ba.PostalCode As BillingPostalCode, bico.CountryName As BillingCountryName, " & _
        " cil.Contact As BillingContact, cil.Phone As BillingPhone, SUM(oi.LineItemTotal) As Total, c.CurrencyName As Currency, c.CurrencyCodeID As CurrencyCodeID,  " & _
        " co.CountryName As OrderCountry, o.Comments as Comments, oi.SessionTrackingNumber," & _
        " oi.OrderTrackingNumber,oi.DestinationTrackingNumber, os.OrderStatusCode As Status FROM  tblOrders o, tblOrderDestinations od, tblCustomerAddresses sa, " & _
        " tblCustomerAddresses ba, tblCurrencies c, tblOrderItems oi, tblOrderStatus os, tblCustomers cu, tblUsers u, tblCountries shco, tblCountries bico," & _
        " tblCustomerDestinations cd, tblCustomerInvoiceLocations cil, tblCountries co WHERE sa.CustomerID=o.CustomerID AND ba.CustomerID=o.CustomerID AND " & _
        " o.SessionTrackingNumber=" & STN & " AND o.OrderTrackingNumber= " & OTN & " AND od.DestinationTrackingNumber= " & DTN & " AND cu.CustomerID=o.CustomerID AND u.UserID=o.OrderedByUserID AND " & _
        " oi.SessionTrackingNumber=o.SessionTrackingNumber AND oi.OrderTrackingNumber=o.OrderTrackingNumber AND od.SessionTrackingNumber=oi.SessionTrackingNumber AND " & _
        " od.OrderTrackingNumber=oi.OrderTrackingNumber AND od.DestinationTrackingNumber=oi.DestinationTrackingNumber AND od.CustomerDestinationID=sa.CustomerAddressID AND" & _
        " od.CustomerInvoiceLocationID=ba.CustomerAddressID AND shco.CountryID=sa.CountryID AND bico.CountryID=ba.CountryID AND cd.CustomerDestinationID=sa.CustomerAddressID AND " & _
        " cil.CustomerInvoiceLocationID=ba.CustomerAddressID AND co.CountryID=o.OrderDestinationCountryID AND c.CurrencyID=o.OrderDestinationCurrencyID AND os.OrderStatusID=o.OrderStatusID AND " & _
        " oi.SFAID=" & SFAID & " GROUP BY cu.CustomerName, u.UserName, o.OrderDate, o.OrderPO, od.DestinationPO, od.CustomerDestinationID, sa.CustomerAddressName, " & _
        " sa.Address1, sa.Address2, sa.City, sa.StateProvinceRegion, sa.PostalCode, shco.CountryName, shco.CountryCodeID, od.CustomerInvoiceLocationID, bico.CountryCodeid, cd.Contact," & _
        " cd.Phone, ba.CustomerAddressName, ba.CustomerAddressID,ba.Address1, ba.Address2, ba.City, ba.StateProvinceRegion, ba.PostalCode, bico.CountryName, cil.Contact, cil.Phone," & _
        " c.CurrencyName, c.CurrencyCodeId, co.CountryName, od.DateAcceptedBySupplier, od.VAT, od.ShippingAmount, od.InvoiceNumber, o.Comments, oi.SessionTrackingNumber, oi.OrderTrackingNumber, oi.DestinationTrackingNumber, os.OrderStatusCode"

        myds = GetDataTable(SqlOrderSQL)
        For Each mydr In myds.Tables("dt").Rows
            CustomerName = mydr("CustomerName").ToString
            UserName = mydr("UserName").ToString
            OrderDate = mydr("OrderDate").ToString
            OrderPO = mydr("OrderPO").ToString
            DestinationPO = mydr("DestinationPO").ToString
            ShippingCustomerAddressName = mydr("ShippingCustomerAddressName").ToString
            ShippingCustomerAddressID = mydr("ShippingCustomerAddressID").ToString
            ShippingAddress1 = mydr("ShippingAddress1").ToString
            ShippingAddress2 = mydr("ShippingAddress2").ToString
            ShippingCity = mydr("ShippingCity").ToString
            ShippingStateProvinceRegion = mydr("ShippingStateProvinceRegion").ToString
            ShippingPostalCode = mydr("ShippingPostalCode").ToString
            ShippingCountryCodeID = mydr("ShippingCountryCodeID").ToString
            ShippingContact = mydr("ShippingContact").ToString
            ShippingPhone = mydr("ShippingPhone").ToString
            BilltoCountryCodeID = mydr("BilltoCountryCodeID").ToString
            BillingCustomerAddressID = mydr("BillingCustomerAddressID").ToString
            BillingCustomerAddressName = mydr("BillingCustomerAddressName").ToString
            BillingAddress1 = mydr("BillingAddress1").ToString
            BillingAddress2 = mydr("BillingAddress2").ToString
            BillingCity = mydr("BillingCity").ToString
            BillingStateProvinceRegion = mydr("BillingStateProvinceRegion").ToString
            BillingPostalCode = mydr("BillingPostalCode").ToString
            BillingCountryName = mydr("BillingCountryName").ToString
            BillingContact = mydr("BillingContact").ToString
            BillingPhone = mydr("BillingPhone").ToString
            Total = mydr("Total").ToString
            Currency = mydr("Currency").ToString
            CurrencyCodeID = mydr("CurrencyCodeID").ToString
            OrderCountry = mydr("OrderCountry").ToString
            Comments = mydr("Comments").ToString

        Next
        myds.Clear()
        mydr = Nothing

        'get value for From, Identity.... look inside the XREF tbls first 
        Dim XrefCustCode = GetStringValue("SELECT XrefCode FROM tblXrefCustomers WHERE CustomerID = " & CustomerId & " AND XrefTypeID = 3 AND XrefTableID = " & SFAID)
        If XrefCustCode = "" Then
            XrefCustCode = GetStringValue("SELECT XrefCode FROM tblXrefCustomers WHERE CustomerID = " & CustomerId & " AND XrefTypeID = 5 AND XrefTableID = " & ParentCompanyID)
            If XrefCustCode = "" Then
                XrefCustCode = CustomerName
            End If
        End If

        'Get Xref Bill to Code if there is one
        Dim XrefBillToCode = GetStringValue("SELECT XrefCode  FROM tblXrefCustomerAddresses WHERE CustomerAddressID = " & BillingCustomerAddressID & " AND XrefTypeID = 3 AND XrefTableID = " & SFAID)
        If XrefBillToCode = "" Then
            XrefBillToCode = GetStringValue("SELECT XrefCode, XrefCodeAdditional FROM tblXrefCustomerAddresses WHERE CustomerAddressID =" & BillingCustomerAddressID & " AND XrefTypeID = 5 AND XrefTableID = " & ParentCompanyID)
            If XrefBillToCode = "" Then
                XrefBillToCode = BillingCustomerAddressID
            End If
        End If

        'Get Xref Bill to Code if there is one
        Dim XrefShipToCode = GetStringValue("SELECT XrefCode  FROM tblXrefCustomerAddresses WHERE CustomerAddressID = " & ShippingCustomerAddressID & " AND XrefTypeID = 3 AND XrefTableID = " & SFAID)
        If XrefShipToCode = "" Then
            XrefShipToCode = GetStringValue("SELECT XrefCode, XrefCodeAdditional FROM tblXrefCustomerAddresses WHERE CustomerAddressID =" & ShippingCustomerAddressID & " AND XrefTypeID = 5 AND XrefTableID = " & ParentCompanyID)
            If XrefShipToCode = "" Then
                XrefShipToCode = ShippingCustomerAddressID
            End If
        End If

        If ShippingCountryCodeID Then
            ISOCountryCodeShipTo = GetStringValue("SELECT  ISOCountryCodeA2 FROM  tblCountryCodes WHERE CountryCodeID =" & ShippingCountryCodeID)
        End If

        If BilltoCountryCodeID Then
            ISOCountryCodeBillTo = GetStringValue("SELECT  ISOCountryCodeA2 FROM  tblCountryCodes WHERE CountryCodeID =" & BilltoCountryCodeID)
        End If

        If CurrencyCodeID Then
            ISOCurrencyCode = GetStringValue("Select ISOCurrencyCode from tblCurrencyCodes where CurrencyCodeID = " & CurrencyCodeID)
        End If

        'Start building XML message
        Dim myXmlTextReader As XmlTextReader = Nothing
        Dim myXmlTextWriter As XmlTextWriter = Nothing

        Try
            myXmlTextWriter = New XmlTextWriter(args, Nothing)


            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented
            myXmlTextWriter.WriteStartDocument(False)
            'XML declaration Part
            myXmlTextWriter.WriteDocType("cXML", Nothing, FilePath & "cXML.dtd", Nothing)
            ' myXmlTextWriter.WriteComment("This file represents sample OrderRequest")
            myXmlTextWriter.WriteStartElement("cXML")
            myXmlTextWriter.WriteAttributeString("payloadID", GSOrderNumber)
            myXmlTextWriter.WriteAttributeString("timestamp", Now())
            myXmlTextWriter.WriteAttributeString("version", version)
            myXmlTextWriter.WriteAttributeString("xml:lang", XMLLang)

            'Header Begins here
            'From Part
            myXmlTextWriter.WriteStartElement("Header", Nothing)
            myXmlTextWriter.WriteStartElement("From", Nothing)
            myXmlTextWriter.WriteStartElement("Credential", Nothing)
            myXmlTextWriter.WriteAttributeString("domain", FromDomain)
            myXmlTextWriter.WriteElementString("Identity", XrefCustCode)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()

            'To Part

            myXmlTextWriter.WriteStartElement("To", Nothing)
            myXmlTextWriter.WriteStartElement("Credential", Nothing)
            myXmlTextWriter.WriteAttributeString("domain", ToDomain)
            myXmlTextWriter.WriteElementString("Identity", PartnerId)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()


            'Sender Part

            myXmlTextWriter.WriteStartElement("Sender", Nothing)
            myXmlTextWriter.WriteStartElement("Credential", Nothing)
            myXmlTextWriter.WriteAttributeString("domain", SenderDomain)
            myXmlTextWriter.WriteElementString("Identity", SenderIdentity)
            myXmlTextWriter.WriteElementString("SharedSecret", SharedSecret)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("UserAgent", UserAgent)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'OrderRequest Body here

            myXmlTextWriter.WriteStartElement("Request", Nothing)
            myXmlTextWriter.WriteAttributeString("deploymentMode", Flag)
            myXmlTextWriter.WriteStartElement("OrderRequest", Nothing)
            myXmlTextWriter.WriteStartElement("OrderRequestHeader", Nothing)
            myXmlTextWriter.WriteAttributeString("orderID", OrderPO)
            myXmlTextWriter.WriteAttributeString("type", "new")
            myXmlTextWriter.WriteAttributeString("requisitionID", GSOrderNumber)
            myXmlTextWriter.WriteAttributeString("orderDate", OrderDate)
            'Total Order Money Value
            myXmlTextWriter.WriteStartElement("Total", Nothing)
            myXmlTextWriter.WriteStartElement("Money", Nothing)
            myXmlTextWriter.WriteAttributeString("currency", ISOCurrencyCode)
            myXmlTextWriter.WriteString("1000.00")
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            'Ship To Part
            '
            '
            myXmlTextWriter.WriteStartElement("ShipTo", Nothing)
            myXmlTextWriter.WriteStartElement("Address", Nothing)
            myXmlTextWriter.WriteAttributeString("isoCountryCode", ISOCountryCodeShipTo)
            myXmlTextWriter.WriteAttributeString("addressID", XrefShipToCode)
            myXmlTextWriter.WriteStartElement("Name", Nothing)
            myXmlTextWriter.WriteAttributeString("xml:lang", XMLLang)
            myXmlTextWriter.WriteString(CustomerName)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("PostalAddress", Nothing)
            myXmlTextWriter.WriteAttributeString("name", ShippingCustomerAddressName)
            myXmlTextWriter.WriteElementString("Street", ShippingAddress1)
            myXmlTextWriter.WriteElementString("Street", ShippingAddress2)
            myXmlTextWriter.WriteElementString("City", ShippingCity)
            myXmlTextWriter.WriteElementString("State", ShippingStateProvinceRegion)
            myXmlTextWriter.WriteElementString("PostalCode", ShippingPostalCode)

            myXmlTextWriter.WriteStartElement("Country", Nothing)

            myXmlTextWriter.WriteAttributeString("isoCountryCode", ISOCountryCodeShipTo)
            myXmlTextWriter.WriteString(ShippingCountryName)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()

            'Bill To Part
            myXmlTextWriter.WriteStartElement("BillTo", Nothing)
            myXmlTextWriter.WriteStartElement("Address", Nothing)
            myXmlTextWriter.WriteAttributeString("isoCountryCode", ISOCountryCodeBillTo)
            myXmlTextWriter.WriteAttributeString("addressID", XrefBillToCode)
            myXmlTextWriter.WriteStartElement("Name", Nothing)
            myXmlTextWriter.WriteAttributeString("xml:lang", XMLLang)
            myXmlTextWriter.WriteString(CustomerName)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("PostalAddress", Nothing)
            myXmlTextWriter.WriteAttributeString("name", BillingCustomerAddressName)
            myXmlTextWriter.WriteElementString("Street", BillingAddress1)
            myXmlTextWriter.WriteElementString("Street", BillingAddress2)
            myXmlTextWriter.WriteElementString("City", BillingCity)
            myXmlTextWriter.WriteElementString("State", BillingStateProvinceRegion)
            myXmlTextWriter.WriteElementString("PostalCode", BillingPostalCode)

            myXmlTextWriter.WriteStartElement("Country", Nothing)
            myXmlTextWriter.WriteAttributeString("isoCountryCode", ISOCountryCodeBillTo)
            myXmlTextWriter.WriteString(BillingCountryName)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()

            'Contact Part
            myXmlTextWriter.WriteStartElement("Contact", Nothing)
            myXmlTextWriter.WriteStartElement("Name", Nothing)
            myXmlTextWriter.WriteAttributeString("xml:lang", XMLLang)
            myXmlTextWriter.WriteString(ShippingContact)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteStartElement("Phone", Nothing)
            myXmlTextWriter.WriteStartElement("TelephoneNumber", Nothing)
            myXmlTextWriter.WriteStartElement("CountryCode", Nothing)
            myXmlTextWriter.WriteAttributeString("isoCountryCode", "na")
            myXmlTextWriter.WriteString(CustomerName)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("AreaOrCityCode", "na")
            myXmlTextWriter.WriteElementString("Number", ShippingPhone)
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteElementString("Comments", Comments)
            myXmlTextWriter.WriteEndElement()

            Dim SqlOrdersItem As String = "SELECT oi.CustomerPartID as CustomerPartID, oi.OrderItemID as OrderItemID, od.OrderDeliveryDetailsID as OrderDeliveryDetailsID," & _
                                                             " oi.CatalogStandardID as CatalogStandardID, oi.StdDescription as StdDescription, oi.PartDescription as PartDescription, oi.ManufacturerSKU as ManufacturerSKU, m.ManufacturerName as ManufacturerName," & _
                                                             " oi.LineItemPrice as LineItemPrice, oi.Quantity as Quantity, oi.LineItemTotal as LineItemTotal, oi.MfgTrackingID as MfgTrackingID,  oi.SupplierSKU as SupplierSKU," & _
                                                             " oi.LineItemSeqNum as LineItemSeqNum, oi.GroupIdentifier as GroupIdentifier, oi.GroupItemSeqNum as GroupItemSeqNum, mp.UNSPSC as UNSPSC FROM  tblOrderItems oi " & _
                                                             " LEFT OUTER JOIN  tblOrderDeliveryDetails od ON od.OrderDeliveryItemID=oi.OrderItemID  " & _
                                                             " LEFT OUTER JOIN dbo.tblCustomerCountryParts ccp ON ccp.CustomerPartID=oi.CustomerPartID " & _
                                                             " LEFT OUTER JOIN dbo.tblManufacturerParts mp ON mp.ManufacturerPartID=ccp.ManufacturerPartID " & _
                                                             " LEFT OUTER JOIN dbo.tblManufacturers m ON m.ManufacturerID=mp.ManufacturerID " & _
                                                             " WHERE oi.SessionTrackingNumber=" & STN & " AND oi.OrderTrackingNumber=" & OTN & " AND oi.DestinationTrackingNumber=  " & DTN & _
                                                             " AND  oi.SFAID= " & SFAID & " AND (oi.ItemStatusID NOT IN (SELECT OrderItemStatusID FROM tblOrderItemStatus WHERE OrderItemStatusCode='X') OR oi.ItemStatusID IS NULL) ORDER BY oi.CatalogStandardID DESC, oi.PartDescription"


            myds = GetDataTable(SqlOrdersItem)
            For Each mydr In myds.Tables("dt").Rows
                CustomerPartID = mydr("CustomerPartID").ToString
                OrderItemID = mydr("OrderItemID").ToString
                OrderDeliveryDetailsID = mydr("OrderDeliveryDetailsID").ToString
                CatalogStandardID = mydr("CatalogStandardID").ToString
                StdDescription = mydr("StdDescription").ToString
                PartDescription = mydr("PartDescription").ToString
                ManufacturerName = mydr("ManufacturerName").ToString
                ManufacturerSKU = mydr("ManufacturerSKU").ToString
                LineItemPrice = mydr("LineItemPrice").ToString
                Quantity = mydr("Quantity").ToString
                LineItemTotal = mydr("LineItemTotal").ToString
                MfgTrackingID = mydr("MfgTrackingID").ToString
                SupplierSKU = mydr("SupplierSKU").ToString
                LineItemSeqNum = mydr("LineItemSeqNum").ToString
                GroupIdentifier = mydr("GroupIdentifier").ToString
                GroupItemSeqNum = mydr("GroupItemSeqNum").ToString
                UNSPSC = mydr("UNSPSC").ToString

                ' MsgBox("ManufacturerName: " & ManufacturerName)
                'Item level data
                myXmlTextWriter.WriteStartElement("ItemOut", Nothing)
                myXmlTextWriter.WriteAttributeString("quantity", Quantity)
                myXmlTextWriter.WriteAttributeString("lineNumber", LineItemSeqNum)
                myXmlTextWriter.WriteStartElement("ItemID", Nothing)
                myXmlTextWriter.WriteElementString("SupplierPartID", SupplierSKU)
                myXmlTextWriter.WriteEndElement()

                myXmlTextWriter.WriteStartElement("ItemDetail", Nothing)
                myXmlTextWriter.WriteStartElement("UnitPrice", Nothing)
                myXmlTextWriter.WriteStartElement("Money", Nothing)
                myXmlTextWriter.WriteAttributeString("currency", ISOCurrencyCode)
                myXmlTextWriter.WriteString(LineItemPrice)
                myXmlTextWriter.WriteEndElement()
                myXmlTextWriter.WriteEndElement()

                myXmlTextWriter.WriteStartElement("Description", Nothing)
                myXmlTextWriter.WriteAttributeString("xml:lang", XMLLang)
                myXmlTextWriter.WriteString(PartDescription)
                myXmlTextWriter.WriteEndElement()
                myXmlTextWriter.WriteElementString("UnitOfMeasure", "EA")
                myXmlTextWriter.WriteStartElement("Classification", Nothing)
                myXmlTextWriter.WriteAttributeString("domain", "UNSPSC")
                myXmlTextWriter.WriteString(UNSPSC)
                myXmlTextWriter.WriteEndElement()
                myXmlTextWriter.WriteElementString("ManufacturerName", ManufacturerName)
                myXmlTextWriter.WriteStartElement("Extrinsic", Nothing)
                myXmlTextWriter.WriteAttributeString("name", "AggregationCode")
                myXmlTextWriter.WriteString(GroupItemSeqNum)
                myXmlTextWriter.WriteEndElement()
                myXmlTextWriter.WriteEndElement()
                myXmlTextWriter.WriteEndElement()
                'Now loop for all line Item level information

            Next
            myds.Clear()
            mydr = Nothing

            myXmlTextWriter.WriteEndElement()
            myXmlTextWriter.WriteEndElement()

            'Write the XML to file and close the writer
            myXmlTextWriter.Flush()
            myXmlTextWriter.Close()

            ' Read the file back in and parse to ensure well formed XML
            myXmlTextReader = New XmlTextReader(args)
        Catch e As Exception
            Console.WriteLine("Exception: {0}", e.ToString())
        Finally
            Console.WriteLine()
            Console.WriteLine("Processing of the file {0} complete.", args)

            If Not myXmlTextReader Is Nothing Then
                Dim StringToPostValue = myXmlTextReader.ReadString
                myXmlTextReader.Close()
            End If
            'Close the writer
            If Not myXmlTextWriter Is Nothing Then
                myXmlTextWriter.Close()
            End If
        End Try

        'Now Validate the XML with corresponding DTD
        Dim tr As XmlTextReader = New XmlTextReader(args)
        Dim vr As XmlValidatingReader = New XmlValidatingReader(tr)
        vr.ValidationType = ValidationType.DTD
        AddHandler vr.ValidationEventHandler, AddressOf ValidationCallBack
        While (vr.Read())
        End While
        EventLog.WriteEntry("Application", "DTD Validation finished")

        'Now Posting the XML Document to the according URL
        HTTPPost(args, PostURL)
        EventLog.WriteEntry("Application", "Http Posting finished")
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that Retrieves data from
    ' given SQL and returns in Dataset Type
    ' ----------  ARPATECH  ----------
    Public Function GetDataTable(ByVal PassSql As String) As DataSet

        Dim ds As DataSet = New DataSet()
        Dim dt As DataTable = New DataTable()
        Dim sqlConn As SqlConnection = New SqlConnection(GSSupportManager.GetDBString())
        sqlConn.Open()
        Dim SqlDataAdaptertblOrders As SqlDataAdapter = New SqlDataAdapter(PassSql, sqlConn)
        Try
            SqlDataAdaptertblOrders.Fill(ds, "dt")
        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to Load while trying to process " & PassSql & e.Message)
            ds.Clear()
            sqlConn.Close()
            sqlConn = Nothing
        End Try
        Return ds

    End Function

    ' ----------  ARPATECH  ----------
    ' function that retrieves first 
    ' field data from given SQL and returns as String
    ' ----------  ARPATECH  ----------
    Public Function GetStringValue(ByVal PassSql As String) As String

        Dim ds As DataSet = New DataSet()
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        Dim ReturnSTRValue
        Dim sqlConn As SqlConnection = New SqlConnection(GSSupportManager.GetDBString())
        sqlConn.Open()
        Dim SqlDataAdaptertblOrders As SqlDataAdapter = New SqlDataAdapter(PassSql, sqlConn)
        Try
            SqlDataAdaptertblOrders.Fill(ds, "dt")
            For Each dr In ds.Tables("dt").Rows
                ReturnSTRValue = dr(0)
            Next
        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to Load while trying to process " & PassSql & e.Message)
            ds.Clear()
            sqlConn.Close()
            sqlConn = Nothing
        End Try
        Return ReturnSTRValue

    End Function

    ' ----------  ARPATECH  ----------
    ' function that writes any given data 
    ' to given Filename
    ' ----------  ARPATECH  ----------
    Public Function WriteFile(ByVal InputString As String, ByVal FileName As String) As String

        FileName = SaturateFileNameString(FileName)
        Dim fs As FileStream = New FileStream(FileName, FileMode.Create)
        Dim w As BinaryWriter = New BinaryWriter(fs)
        Dim strOutput As String = ""

        w.BaseStream.Seek(0, SeekOrigin.End)        'set the file pointer to the end

        Try
            w.Write(InputString)

        Catch E As Exception
            ' let the user know that the information could not be written
            strOutput = "WRITE EXCEPTION: " & Chr(13) & _
             "The information you want to write cannot be written in the format requested. " & _
             Chr(13) & "Please enter a valid value for the datatype you are trying to write"
        End Try

        fs.Close()

        Return strOutput
    End Function

    ' ----------  ARPATECH  ----------
    ' function that retrieves replaces
    '  / \ space : to -
    ' ----------  ARPATECH  ----------
    Public Function SaturateFileNameString(ByVal passString As String) As String
        passString = Replace(passString, "/", "_")
        passString = Replace(passString, "\", "_")
        passString = Replace(passString, " ", "_")
        passString = Replace(passString, ":", "_")
        Return passString
    End Function

    ' ----------  ARPATECH  ----------
    ' function that retrieves first 
    ' field data from given SQL and returns as String
    ' ----------  ARPATECH  ----------
    Public Shared Sub ValidationCallBack(ByVal sender As Object, ByVal args As ValidationEventArgs)
        Console.WriteLine("***Validation error")
        Console.WriteLine("Severity:{0}", args.Severity)
        Console.WriteLine("Message:{0}", args.Message)
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that writes generated XML by 
    ' cXMLOrderRequest function to a given file at specific URL
    ' ----------  ARPATECH  ----------
    Public Function HTTPPost(ByVal FileName As String, ByVal URLToPost As String)
        EventLog.WriteEntry("Application", "FileName with cXML is " & FileName)
        EventLog.WriteEntry("Application", "URLToPost " & URLToPost)
        'for testing purpose, use this address to post
        URLToPost = "http://10.10.20.152/eCommerce/CompaqCAFE/inboundorder.asp"
        Dim StringToPost
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(FileName)
            Dim line As String
            ' Read the lines from the file until the end 
            ' of the file is reached.
            Do
                line = sr.ReadLine()
                StringToPost = StringToPost & line
            Loop Until line Is Nothing
            sr.Close()
        Catch E As Exception
            ' Let the user know what went wrong.
            EventLog.WriteEntry("Application", "The file " & FileName & " could not be read:")
            EventLog.WriteEntry("Application", E.Message)
        End Try
        'this is how you do the HTTP Posting of any string
        StringToPost = """" & StringToPost & """"
        'StringToPost = Replace(StringToPost, vbTab, "")
        'Console.WriteLine("stringtoPost " & StringToPost)
        Dim web As New System.Net.WebClient()
        web.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
        Dim dataString As Byte() = System.Text.Encoding.ASCII.GetBytes(StringToPost)
        Dim res As Byte() = web.UploadData(URLToPost, "POST", dataString)
        ' Dim res As Byte() = web.UploadData("http://b2bintegration.global-serve.com:7001/xml", "POST", dataString)

    End Function
End Class

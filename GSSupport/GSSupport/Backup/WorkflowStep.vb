' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class looks like a small class for some small steps of assigning and
'                       fetching values.
' Inherits          :   ---------
' Implements        :   IWorkflowStep
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class WorkflowStep
    Implements IWorkflowStep

    Private m_action As Action
    Private m_qualification As Qualification
    Private m_actionConjunction As Conjunction


    ' ----------  ARPATECH  ----------
    '       SAME CLASS INTERFACE
    ' ----------  ARPATECH  ----------
    <System.Runtime.InteropServices.ComVisible(True)> Public Interface IWorkflowStep
        Property Action() As Action
        Property Qualification() As Qualification
        Function GetActionText() As String
        Function GetQualificationText() As String
        Property ActionConjunction() As Conjunction

    End Interface

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action 
    ' ----------  ARPATECH  ----------
    Public Property Action() As Action Implements IWorkflowStep.Action
        Get
            Return m_action
        End Get
        Set(ByVal Value As Action)
            m_action = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' cojunction's action
    ' ----------  ARPATECH  ----------
    Public Property ActionConjunction() As Conjunction Implements IWorkflowStep.ActionConjunction
        Get
            Return m_actionConjunction
        End Get
        Set(ByVal Value As Conjunction)
            m_actionConjunction = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket qualification 
    ' ----------  ARPATECH  ----------
    Public Property Qualification() As Qualification Implements IWorkflowStep.Qualification
        Get
            Return m_qualification
        End Get
        Set(ByVal Value As Qualification)
            m_qualification = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with qualification and action arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal myQualification As Qualification, ByVal myAction As Action)
        m_action = myAction
        m_qualification = myQualification
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with qualification action and co junction arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal myQualification As Qualification, ByVal myAction As Action, ByVal myActionConjunction As Conjunction)
        m_action = myAction
        m_qualification = myQualification
        m_actionConjunction = myActionConjunction
    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR 
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with qualification arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal myQualification As Qualification)
        m_qualification = myQualification
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with action arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal myAction As Action)
        m_action = myAction
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with multiple arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal myAction As Action, ByVal myActionConjunction As Conjunction)
        m_action = myAction
        m_actionConjunction = myActionConjunction
    End Sub

    ' ----------  ARPATECH  ----------
    ' function for Retrieving action 
    ' ----------  ARPATECH  ----------
    Function GetActionText() As String Implements IWorkflowStep.GetActionText
        Dim s As New String("")

        If Not IsNothing(m_action) Then
            s = m_action.ActionName
        End If
        If Not IsNothing(m_actionConjunction) Then
            s = s & " " & LCase(m_actionConjunction.DisplayText)
        End If
        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving Qualification 
    ' ----------  ARPATECH  ----------
    Function GetQualificationText() As String Implements IWorkflowStep.GetQualificationText
        Dim s As New String("")

        If Not IsNothing(m_qualification) Then
            s = m_qualification.GetText
        End If
        Return s
    End Function

End Class

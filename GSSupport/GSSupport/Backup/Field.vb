' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class purpose is to returns native properties of given field from DB.
' Inherits          :   ---------
' Implements        :   IField
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient

<Serializable()> Public Class Field
    Implements IField

    Dim FieldTypes As ArrayList
    Private m_fieldDisplayValue As New String("")
    Private m_fieldNativeValue As Object
    Private m_fieldDisplayName As New String("")
    Private m_fieldID As Integer
    Private m_aspDisplayCmd As String
    Private m_fieldTextValue As New String("")
    Private m_dataType As Integer
    Private m_fieldName As New String("")
    Private m_vbConst As Integer


    ' ----------  ARPATECH  ----------
    '       SAME CLASS INTERFACE
    ' ----------  ARPATECH  ----------
    <System.Runtime.InteropServices.ComVisible(True)> Public Interface IField
        Property FieldDisplayValue() As String
        Property FieldID() As Integer
        Property VBConst() As Integer
        Property AspDisplayCmd() As String
        Property DataType() As Integer
        Property FieldTextValue() As String
        Property FieldNativeValue() As Object
        Property FieldDisplayName() As String
        Property FieldName() As String


    End Interface

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed data type
    ' ----------  ARPATECH  ----------
    Public Property DataType() As Integer Implements IField.DataType
        Get
            Return m_dataType
        End Get
        Set(ByVal Value As Integer)
            m_dataType = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed display value
    ' ----------  ARPATECH  ----------
    Public Property FieldDisplayValue() As String Implements IField.FieldDisplayValue
        Get
            Return m_fieldDisplayValue
        End Get
        Set(ByVal Value As String)
            m_fieldDisplayValue = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed name
    ' ----------  ARPATECH  ----------
    Public Property FieldName() As String Implements IField.FieldName
        Get
            Return m_fieldName
        End Get
        Set(ByVal Value As String)
            m_fieldName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed native value
    ' ----------  ARPATECH  ----------
    Public Property FieldNativeValue() As Object Implements IField.FieldNativeValue
        Get
            Dim manager = New GSSupportManager()
            If m_dataType = vbDate Then
                ' If it's a date convert to correct time zone if a tz exists
                If Not IsDate(m_fieldTextValue) Then
                    Dim n As Integer = m_fieldTextValue.LastIndexOf(" ")
                    If n > 0 Then
                        Dim tz, d
                        tz = Trim(Mid(m_fieldTextValue, n + 1))
                        d = Trim(Mid(m_fieldTextValue, 1, n))
                        ' Always convert to server time zone for evaluation purposes
                        If IsDate(d) Then
                            If Year(d) < MinYear Then

                                ' Just a time value... set day to present
                                ' Convert time value to current day server(time)
                                Dim nd As New Date(Year(Now()), Month(Now()), Day(Now()), Hour(d), Minute(d), Second(d))

                                ' Now convert time Server Time to time zone
                                m_fieldNativeValue = manager.ConvertTime(nd, ServerTimeZone, manager.GetTimeZoneVBConst(tz))

                            Else

                                Dim oldtz As Integer = manager.GetTimeZoneVBConst(tz)
                                m_fieldNativeValue = manager.ConvertTime(d, oldtz, ServerTimeZone)
                            End If
                        End If

                    End If
                End If

            End If
            Return m_fieldNativeValue
        End Get
        Set(ByVal Value As Object)
            m_fieldNativeValue = Value
            If IsNothing(m_fieldTextValue) OrElse Len(m_fieldTextValue) < 1 Then
                m_fieldTextValue = GetValText(Value)
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed display name
    ' ----------  ARPATECH  ----------
    Public Property FieldDisplayName() As String Implements IField.FieldDisplayName
        Get
            Return m_fieldDisplayName
        End Get
        Set(ByVal Value As String)
            m_fieldDisplayName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' field ID
    ' ----------  ARPATECH  ----------
    Public Property FieldID() As Integer Implements IField.FieldID
        Get
            Return m_fieldID
        End Get
        Set(ByVal Value As Integer)
            m_fieldID = Value
            If IsNothing(m_dataType) OrElse m_dataType = 0 Then
                m_dataType = GetFieldDataType(m_fieldID)
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' vb constant
    ' ----------  ARPATECH  ----------
    Public Property VBConst() As Integer Implements IField.VBConst
        Get
            Return m_vbConst
        End Get
        Set(ByVal Value As Integer)
            m_vbConst = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' display command
    ' ----------  ARPATECH  ----------
    Public Property AspDisplayCmd() As String Implements IField.AspDisplayCmd
        Get
            Return m_aspDisplayCmd
        End Get
        Set(ByVal Value As String)
            m_aspDisplayCmd = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' field's text value
    ' ----------  ARPATECH  ----------
    Public Property FieldTextValue() As String Implements IField.FieldTextValue
        Get
            Return m_fieldTextValue
        End Get
        Set(ByVal Value As String)
            m_fieldTextValue = Value

            Try

                If Not IsNothing(m_dataType) AndAlso m_dataType > 0 Then
                    Select Case (m_dataType)
                        Case (vbBoolean)
                            If UCase(Value) = "TRUE" Or _
                              UCase(Value) = "1" Or _
                              UCase(Value) = "YES" Then
                                m_fieldNativeValue = True

                            Else
                                m_fieldNativeValue = False
                            End If
                            '    m_fieldNativeValue = CBool(Value)
                        Case (vbInteger)
                            If IsNumeric(Value) Then
                                m_fieldNativeValue = CInt(Value)
                            Else
                                m_fieldNativeValue = 0
                            End If

                        Case (vbDouble)
                            If IsNumeric(Value) Then
                                m_fieldNativeValue = CDbl(Value)
                            Else
                                m_fieldNativeValue = 0
                            End If

                        Case (vbDate)
                            If Not IsDate(Value) Then
                                'Probably have timezoneid attached to the end
                                ' Try lopping off last integer
                                Dim n = Value.LastIndexOf(" ")
                                If (n > 0) Then
                                    Dim ns As String = Mid(Value, 1, n)
                                    If IsDate(ns) Then
                                        '          m_fieldNativeValue = CDate(ns)
                                    End If
                                End If

                            Else
                                '   m_fieldNativeValue = CDate(Value)
                            End If


                    End Select
                Else
                    If IsNumeric(Value) Then
                        m_fieldNativeValue = CDbl(Value)
                    End If
                End If
                If IsNothing(m_fieldNativeValue) Then
                    m_fieldNativeValue = New String(Value)
                End If

            Catch e As Exception
                EventLog.WriteEntry("Application", "Unable to convert " & Value & " to data type " & m_dataType & " for field " & Me.FieldName & "(" & Me.FieldID & ")")

            End Try


        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for Retrieving field data type
    ' ----------  ARPATECH  ----------
    Function GetFieldDataType(ByVal fieldid As Integer) As Integer

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim fdt As Integer = 0

        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try


            Dim dr As SqlDataReader
            Cmd.CommandText = "select IsNull(fieldDataTypeID, 0) as fielddatatypeid from tblFields where fieldid=" & fieldid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                fdt = dr.GetInt32(dr.GetOrdinal("fielddatatypeid"))



            End While

            dr.Close()



        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to get " & m_fieldDisplayName & " field data type:" & e.Message)


        Finally

            Conn.Close()

            Conn = Nothing

        End Try

        Return fdt

    End Function

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with multiple arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal fieldid As Integer, ByVal fieldName As String, ByVal displayName As String, ByVal dataTypeID As Integer)
        m_fieldID = fieldid
        m_fieldName = fieldName
        m_dataType = dataTypeID
        m_fieldDisplayName = displayName
    End Sub
    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with multiple arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal fieldid As Integer, ByVal fieldName As String, ByVal displayName As String, ByVal dataTypeID As Integer, ByVal vbConst As Integer)
        m_fieldID = fieldid
        m_fieldName = fieldName
        m_dataType = dataTypeID
        m_fieldDisplayName = displayName
        m_vbConst = vbConst
    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for clearing values
    ' ----------  ARPATECH  ----------
    Public Sub ClearValues()
        m_fieldNativeValue = Nothing
        m_fieldTextValue = ""
        m_fieldDisplayValue = ""
    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub


    ' ----------  ARPATECH  ----------
    ' function to convert object in string
    ' ----------  ARPATECH  ----------
    Overrides Function toString() As String
        Dim s As New String("")
        If Not IsNothing(m_fieldTextValue) AndAlso Len(m_fieldTextValue) > 0 Then
            Return m_fieldTextValue
        ElseIf Not IsNothing(m_fieldNativeValue) Then
            Return GetValText(m_fieldTextValue)
        End If
        Return s
    End Function

End Class

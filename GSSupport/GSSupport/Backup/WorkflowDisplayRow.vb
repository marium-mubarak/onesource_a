' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains only properties for workflow display in a table form.
' Inherits          :   ---------
' Implements        :   IWorkflowDispayRow
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IWorkflowDispayRow
    Property IsAction() As Boolean
    Property IsQualification() As Boolean
    Property ActionID() As Integer
    Property BeginClause() As Integer
    Property EndClause() As Integer
    Property FieldID() As Integer
    Property FieldDisplayValue() As String
    Property FieldTextValue() As String
    Property OperatorStr() As String
    Property ConjunctionStr() As String
    Property ActionConjunctionStr() As String
    Property CategoryID() As Integer



End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class WorkflowDisplayRow
    Implements IWorkflowDispayRow
    Private m_isAction As Boolean = False
    Private m_isQualification As Boolean = False
    Private m_actionID As Integer = 0
    Private m_beginClause As Integer = 0
    Private m_endClause As Integer = 0
    Private m_fieldID As Integer = 0
    Private m_fieldDisplayValue As New String("")
    Private m_fieldTextValue As New String("")
    Private m_operatorStr As New String("")
    Private m_conjunctionStr As New String("")
    Private m_actionConjunctionStr As New String("")
    Private m_categoryID As Integer = 0


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed's display value
    ' ----------  ARPATECH  ----------
    Public Property FieldDislayValue() As String Implements IWorkflowDispayRow.FieldDisplayValue
        Get
            Return m_fieldDisplayValue
        End Get
        Set(ByVal Value As String)
            m_fieldDisplayValue = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' co junction string
    ' ----------  ARPATECH  ----------
    Public Property ConjunctionStr() As String Implements IWorkflowDispayRow.ConjunctionStr
        Get
            Return m_conjunctionStr
        End Get
        Set(ByVal Value As String)
            m_conjunctionStr = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action co junction string
    ' ----------  ARPATECH  ----------
    Public Property actionConjunctionStr() As String Implements IWorkflowDispayRow.ActionConjunctionStr
        Get
            Return m_actionConjunctionStr
        End Get
        Set(ByVal Value As String)
            m_actionConjunctionStr = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator String
    ' ----------  ARPATECH  ----------
    Public Property OperatorStr() As String Implements IWorkflowDispayRow.OperatorStr
        Get
            Return m_operatorStr
        End Get
        Set(ByVal Value As String)
            m_operatorStr = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Action ID
    ' ----------  ARPATECH  ----------
    Public Property ActionID() As Integer Implements IWorkflowDispayRow.ActionID
        Get
            Return m_actionID
        End Get
        Set(ByVal Value As Integer)
            m_actionID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' category ID
    ' ----------  ARPATECH  ----------
    Public Property CategoryID() As Integer Implements IWorkflowDispayRow.CategoryID
        Get
            Return m_categoryID
        End Get
        Set(ByVal Value As Integer)
            m_categoryID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' field ID
    ' ----------  ARPATECH  ----------
    Public Property FieldID() As Integer Implements IWorkflowDispayRow.FieldID
        Get
            Return m_fieldID
        End Get
        Set(ByVal Value As Integer)
            m_fieldID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' field's text value
    ' ----------  ARPATECH  ----------
    Public Property FieldTextValue() As String Implements IWorkflowDispayRow.FieldTextValue
        Get
            Return m_fieldTextValue
        End Get
        Set(ByVal Value As String)
            m_fieldTextValue = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action confirmation yes / no
    ' ----------  ARPATECH  ----------
    Public Property IsAction() As Boolean Implements IWorkflowDispayRow.IsAction
        Get
            Return m_isAction
        End Get
        Set(ByVal Value As Boolean)
            m_isAction = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' qualification status
    ' ----------  ARPATECH  ----------
    Public Property IsQualification() As Boolean Implements IWorkflowDispayRow.IsQualification
        Get
            Return m_isQualification
        End Get
        Set(ByVal Value As Boolean)
            m_isQualification = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' begin clause
    ' ----------  ARPATECH  ----------
    Public Property BeginClause() As Integer Implements IWorkflowDispayRow.BeginClause
        Get
            Return m_beginClause
        End Get
        Set(ByVal Value As Integer)
            m_beginClause = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' end clause 
    ' ----------  ARPATECH  ----------
    Public Property EndClause() As Integer Implements IWorkflowDispayRow.EndClause
        Get
            Return m_endClause
        End Get
        Set(ByVal Value As Integer)
            m_endClause = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub
End Class

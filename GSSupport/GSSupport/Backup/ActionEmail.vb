' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is used for sending email to specified addresses.
' Inherits          :   Action
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Web.Mail


' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionEmail
    Inherits Action

    Private m_body As New String("")
    Private m_to As New String("")
    Private m_from As New String("")
    Private m_Cc As New String("")
    Private m_Bcc As New String("")
    Private m_subject As New String("")
    Private m_priority As Integer

    Private m_SMTPHost As String

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' body name variable
    ' ----------  ARPATECH  ----------
    Property Body() As String
        Get
            Return m_body
        End Get
        Set(ByVal Value As String)
            m_body = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SmtpMail name variable
    ' ----------  ARPATECH  ----------
    Property SMTPHost() As String
        Get
            If IsNothing(m_SMTPHost) Then
                Return SmtpMail.SmtpServer
            Else
                Return m_SMTPHost

            End If

        End Get
        Set(ByVal Value As String)
            SmtpMail.SmtpServer = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From name variable
    ' ----------  ARPATECH  ----------
    Property From() As String
        Get
            Return m_from
        End Get
        Set(ByVal Value As String)
            m_from = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From subject variable
    ' ----------  ARPATECH  ----------
    Property Subject() As String
        Get
            Return m_subject
        End Get
        Set(ByVal Value As String)
            m_subject = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From priority variable
    ' ----------  ARPATECH  ----------
    Property Priority() As Integer
        Get
            Return m_priority
        End Get
        Set(ByVal Value As Integer)
            m_priority = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From sendto variable
    ' ----------  ARPATECH  ----------
    Property SendTo() As String
        Get
            Return m_to
        End Get
        Set(ByVal Value As String)
            m_to = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From CC variable
    ' ----------  ARPATECH  ----------
    Property Cc() As String
        Get
            Return m_Cc
        End Get
        Set(ByVal Value As String)
            m_Cc = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' From BCC variable
    ' ----------  ARPATECH  ----------
    Property Bcc() As String
        Get
            Return m_Bcc
        End Get
        Set(ByVal Value As String)
            m_Bcc = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for executing this 
    ' class object
    ' it sends mail to specified address
    ' after some basic checks and works
    ' ----------  ARPATECH  ----------
    Public Overrides Sub Execute()

        Try

            If Not IsNothing(m_SMTPHost) Then
                SmtpMail.SmtpServer = m_SMTPHost
                'SmtpMail.SmtpServer = "localhost"
                ' SmtpMail.SmtpServer.Insert(0, m_SMTPHost)

            Else
                SmtpMail.SmtpServer = GSSupportManager.GetSMTPHost
                'SmtpMail.SmtpServer.Insert(0, GSSupportManager.GetSMTPHost)
                ' SmtpMail.SmtpServer = "localhost"
            End If
            Dim mes As New MailMessage()

            mes.Subject = m_subject
            mes.Body = m_body
            mes.Cc = m_Cc
            mes.Bcc = m_Bcc
            mes.From = m_from
            mes.Priority = m_priority

            If Len(m_priority) < 1 Then
                mes.Priority = MailPriority.Normal
            Else
                mes.Priority = m_priority
            End If
            If Len(m_from) < 1 Then
                mes.From = DefaultEmail
            End If

            mes.BodyFormat = MailFormat.Text
            mes.To = m_to



            SmtpMail.Send(mes)


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Sending Email:" & e.Message & ", " & e.Source)
            EventLog.WriteEntry("Application", "Error Email is " & m_to & ", " & m_from & ", " & m_subject & ", " & m_body & ", " & SmtpMail.SmtpServer)

            m_isError = True
            m_errString = e.Message()
        Finally
            m_isComplete = True

        End Try

    End Sub

End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains all operations for Qualification process like saving,
'                       fetching, and adding formula detail etc.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports GSSupport
Imports System.Data.SqlClient
Imports System.Data

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<Serializable()> Public Class Qualification

    Private m_formula As New Formula()
    Private m_formulaID As Integer
    Private m_qualificationID As Integer
    Private m_qualificationName As String
    Private m_arCategoryFieldName As String
    Private m_arCategoryFieldID As Integer = 0


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' qualification ID
    ' ----------  ARPATECH  ----------
    Public Property QualificationID() As Integer
        Get
            Return m_qualificationID
        End Get
        Set(ByVal Value As Integer)
            m_qualificationID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' formula 
    ' ----------  ARPATECH  ----------
    Public Property Formula() As Formula
        Get
            Return m_formula
        End Get
        Set(ByVal Value As Formula)
            m_formula = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' EMPTY constructor 
    ' ----------  ARPATECH  ----------
    Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' constructor with qualification ID
    ' and sql connection
    ' ----------  ARPATECH  ----------
    Sub New(ByVal myQualificationID As Integer, ByRef Conn As SqlConnection)
        ' Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        '   EventLog.WriteEntry("Application", "Loading qualification " & myQualificationID)
        ' Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try

            Cmd.CommandText = "select IsNull(qualificationname, '') as qualificationname, " & _
            " formulaid from tblQualifications where qualificationID =" & myQualificationID
            Dim dr As SqlDataReader


            dr = Cmd.ExecuteReader
            While dr.Read()
                Dim qid As Integer
                Dim aid As Integer


                m_qualificationName = dr.GetString(dr.GetOrdinal("qualificationname"))
                m_qualificationID = myQualificationID


                m_formulaID = dr.GetInt32(dr.GetOrdinal("formulaid"))


            End While
            dr.Close()
            ' EventLog.WriteEntry("Application", "About to create formula " & m_formulaID)
            m_formula = New Formula(m_formulaID, Conn)




        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading qualification " & myQualificationID & " from DB: " & e.Message)

        Finally
            '  Conn.Close()
            '    EventLog.WriteEntry("Application", "Loaded qualification " & myQualificationID)

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' constructor with qualification ID
    ' ----------  ARPATECH  ----------
    Sub New(ByVal myQualificationID As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        '   EventLog.WriteEntry("Application", "Loading qualification " & myQualificationID)
        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try

            Cmd.CommandText = "select IsNull(qualificationname, '') as qualificationname, " & _
            " formulaid from tblQualifications where qualificationID =" & myQualificationID
            Dim dr As SqlDataReader


            dr = Cmd.ExecuteReader
            While dr.Read()
                Dim qid As Integer
                Dim aid As Integer


                m_qualificationName = dr.GetString(dr.GetOrdinal("qualificationname"))
                m_qualificationID = myQualificationID


                m_formula = New Formula(dr.GetInt32(dr.GetOrdinal("formulaid")))


            End While
            dr.Close()





        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading qualification " & myQualificationID & " from DB: " & e.Message)

        Finally
            Conn.Close()
            '    EventLog.WriteEntry("Application", "Loaded qualification " & myQualificationID)

        End Try

    End Sub


    ' ----------  ARPATECH  ----------
    ' function for Retrieving text 
    ' from formula details
    ' ----------  ARPATECH  ----------
    Function GetText() As String
        Dim i As Integer = 0
        Dim n As Integer = 0
        Dim s As New String("")
        Dim sTmp As New String("")
        If Not IsNothing(m_formula) And Not IsNothing(m_formula.FormulaDetails) Then
            For i = 0 To m_formula.FormulaDetails.Count - 1
                sTmp = ""
                If Not IsNothing(m_formula.FormulaDetails.Item(i)) Then
                    Dim fd As New FormulaDetail()
                    fd = m_formula.FormulaDetails.Item(i)
                    For n = 1 To fd.BeginClause
                        sTmp = sTmp & " ("
                    Next


                    If Not IsNothing(fd.Var1) Then
                        Dim f As Field

                        f = fd.Var1
                        '      EventLog.WriteEntry("Application", "fieldID is " & f.FieldID)
                        sTmp = sTmp & f.FieldDisplayName & " "
                    End If

                    If Not IsNothing(fd.[Operator]) Then
                        sTmp = sTmp & fd.[Operator].OperatorText & " "
                    End If

                    If Not IsNothing(fd.Var2) Then
                        Dim f As Field
                        f = fd.Var2
                        sTmp = sTmp & f.FieldDisplayValue
                        If fd.EndClause < 1 Then
                            sTmp = sTmp & " "
                        End If

                    End If

                    For n = 1 To fd.EndClause
                        sTmp = sTmp & ") "
                    Next

                    If Not IsNothing(fd.Conjunction) AndAlso fd.Conjunction.DisplayText <> "-1" AndAlso Len(Trim(sTmp)) > 0 Then

                        sTmp = sTmp & fd.Conjunction.DisplayText & " "
                    End If
                End If
                If Len(Trim(sTmp)) > 0 Then
                    s = s & sTmp
                End If
            Next

        End If

        If Len(s) > 0 Then
            s = "If " & s
        End If
        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' function for adding formula details only
    ' ----------  ARPATECH  ----------
    Function AddFormulaDetail(ByVal beginClause As Integer, ByVal endClause As Integer, ByVal operatorStr As String, ByVal fieldID As Integer, ByVal fieldDisplayName As String, ByVal fieldTextValue As String, ByVal fieldDisplayValue As String, ByVal conjunctionStr As String) As Integer
        Dim fd As New FormulaDetail()
        Dim field1 As New Field()
        Dim field2 As New Field()


        fd.BeginClause = beginClause
        fd.EndClause = endClause
        fd.[Operator] = New [Operator](operatorStr)

        fd.Conjunction = New Conjunction(conjunctionStr)


        field1.FieldID = fieldID
        field2.FieldID = fieldID



        field1.FieldDisplayName = fieldDisplayName

        field2.FieldTextValue = fieldTextValue
        field2.FieldDisplayValue = fieldDisplayValue

        fd.Var1 = field1
        fd.Var2 = field2


        If Not IsNothing(m_formula) Then
            m_formula.AddFormulaDetail(fd)
            '      EventLog.WriteEntry("Application", "detail added")
        End If


        Return m_formula.FormulaDetails.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' function for adding category's formula details
    ' ----------  ARPATECH  ----------
    Function AddCategoryFormulaDetail(ByVal beginClause As Integer, ByVal endClause As Integer, ByVal operatorStr As String, ByVal fieldID As Integer, ByVal fieldDisplayName As String, ByVal fieldTextValue As String, ByVal fieldDisplayValue As String, ByVal conjunctionStr As String, ByVal ARCategoryID As Integer) As Integer
        Dim fd As New FormulaDetail()
        Dim field1 As New Field()
        Dim field2 As New Field()

        ' Add the ARCategory detail first
        AddFormulaDetail(beginClause + 1, 0, "==", GetARCategoryFieldID(), "Event", CStr(ARCategoryID), GetARCategoryDisplayName(ARCategoryID), "and")
        endClause = endClause + 1

        ' Now add the rest

        fd.BeginClause = 0
        fd.EndClause = endClause
        fd.[Operator] = New [Operator](operatorStr)

        fd.Conjunction = New Conjunction(conjunctionStr)

        field1.FieldID = fieldID
        field2.FieldID = fieldID

        field1.FieldDisplayName = fieldDisplayName

        field2.FieldTextValue = fieldTextValue
        field2.FieldDisplayValue = fieldDisplayValue

        fd.Var1 = field1
        fd.Var2 = field2


        If Not IsNothing(m_formula) Then
            m_formula.AddFormulaDetail(fd)
            '    EventLog.WriteEntry("Application", "detail added")
        End If

        Return m_formula.FormulaDetails.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving parenthesis status
    ' ----------  ARPATECH  ----------
    Function CheckParens() As Boolean
        If GetLParens() <> GetRParens() Then
            Return False
        Else
            Return True
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving right parens
    ' ----------  ARPATECH  ----------
    Function GetRParens() As Integer
        Dim i As Integer = 0
        Dim rcount As Integer = 0
        If Not IsNothing(m_formula) Then
            rcount = m_formula.GetRCount
        End If
        Return rcount

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving left parens
    ' ----------  ARPATECH  ----------
    Function GetLParens() As Integer
        Dim i As Integer = 0
        Dim lcount As Integer = 0
        If Not IsNothing(m_formula) Then
            lcount = m_formula.GetLCount
        End If
        Return lcount

    End Function

    ' ----------  ARPATECH  ----------
    ' function for saving qualification 
    ' and return saving status
    ' ----------  ARPATECH  ----------
    Function Save() As Boolean

        If Not SaveFormula() Then
            Return False
        End If
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())




        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.CommandType = CommandType.StoredProcedure
        Cmd.Connection = Conn
        Try

            Cmd.CommandText = "sp_AddQualification"

            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@qualname").Value = m_qualificationName
            Cmd.Parameters("@formulaid").Value = m_formula.FormulaID
            Cmd.Parameters("@qualificationID").Value = m_qualificationID

            Cmd.ExecuteNonQuery()
            m_qualificationID = Cmd.Parameters("@qualificationID").Value

            '      End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to save qualification: " & e.Message)
            Return False
        Finally
            Conn.Close()
        End Try

        Return True
    End Function

    ' ----------  ARPATECH  ----------
    ' function for saving formula to qualification
    ' ----------  ARPATECH  ----------
    Function SaveFormula() As Boolean

        Return m_formula.Save()

    End Function

    ' ----------  ARPATECH  ----------
    ' function for evaluating Action request 
    ' return success status
    ' ----------  ARPATECH  ----------
    Function Evaluate(ByVal ar As ActionRequest, Optional ByRef expr As String = "") As Boolean
        Dim b As Boolean = False
        Dim i As Integer
        Dim tmpf As New Formula()
        Dim completelynew As New Formula()

        '  EventLog.WriteEntry("Application", "Qualification " & Me.QualificationID & " starting evaluate")
        Try


            If Not IsNothing((m_formula)) Then
                completelynew = m_formula


                For i = 0 To m_formula.FormulaDetails.Count - 1
                    Dim fd As New FormulaDetail()
                    If Not IsNothing(completelynew.FormulaDetails.Item(i)) Then
                        fd = completelynew.FormulaDetails.Item(i)


                        Dim o As Object
                        If Not IsNothing(fd.Var2) Then
                            o = fd.Var2


                            If o.GetType.ToString = "GSSupport.Field" Then
                                Dim f As New Field()
                                f = o

                                ' If the field is part of the formula then set val1 to request value
                                If ar.ContainsField(f.FieldID) Then
                                    '          EventLog.WriteEntry("Application", "Contains field " & f.FieldDisplayName)
                                    If Not IsNothing(ar.GetFieldNativeValue(f.FieldID)) Then

                                        fd.Var1 = ar.GetFieldNativeValue(f.FieldID)
                                        '      EventLog.WriteEntry("Application", "at 2 m_formula =" & m_formula.GetText())
                                        '    EventLog.WriteEntry("Application", "at 2 completelynew is =" & completelynew.GetText())

                                    Else
                                        '        EventLog.WriteEntry("Application", "No AR nativefieldvalue for " & f.FieldDisplayName & "; text value is " & f.FieldTextValue)
                                    End If

                                    If Not IsNothing(f.FieldNativeValue) Then
                                        fd.Var2 = f.FieldNativeValue
                                        '       EventLog.WriteEntry("Application", "at 3 m_formula =" & m_formula.GetText())
                                        '        EventLog.WriteEntry("Application", "at 3 completelynew is =" & completelynew.GetText())

                                    Else
                                        '       EventLog.WriteEntry("Application", "No formula nativefieldvalue for " & f.FieldDisplayName)
                                    End If



                                    '    EventLog.WriteEntry("Application", "var1 = " & GetValText(fd.Var1) & ", var2=" & GetValText(fd.Var2))


                                    ' If field is not in the AR make it return false
                                Else
                                    '   EventLog.WriteEntry("Application", "Does not contain field " & f.FieldID & f.FieldDisplayName)


                                    fd.Var1 = 1
                                    fd.Var2 = 0
                                    fd.[Operator] = New [Operator]("==")

                                End If


                            End If

                        End If
                        tmpf.AddFormulaDetail(fd)
                        '        EventLog.WriteEntry("Application", "at 4 m_formula =" & m_formula.GetText())
                        '        EventLog.WriteEntry("Application", "at 4 completelynew is =" & completelynew.GetText())

                    End If
                Next


            End If


            expr = tmpf.GetText()
            '     EventLog.WriteEntry("Application", "Temp formula is " & expr)
            b = CBool(tmpf.Evaluate)
            m_formula = New Formula(m_formula.FormulaID)

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error evaluating qualification " & Me.QualificationID & ":" & e.Message)
            Return False
        End Try
        '     EventLog.WriteEntry("Application", "tmpf formuladetailscount is " & tmpf.FormulaDetails.Count)


        '  EventLog.WriteEntry("Application", "Qualification " & Me.QualificationID & " ending evaluate")
        Return b
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving action request
    ' category ID
    ' ----------  ARPATECH  ----------
    Function GetARCategoryFieldID() As Integer
        If m_arCategoryFieldID = 0 Then
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Conn.Open()

            Dim Cmd As New SqlCommand()

            Cmd.Connection = Conn
            Try
                Cmd.CommandText = "select fieldid from tblFields where upper(fieldname) = 'ARCATEGORYID'"

                Dim dr As SqlDataReader
                dr = Cmd.ExecuteReader
                While dr.Read()

                    m_arCategoryFieldID = dr.GetInt32(dr.GetOrdinal("fieldid"))

                End While
                dr.Close()

            Catch e As Exception
                EventLog.WriteEntry("Application", "Error getting ar from DB: " & e.Message)
            Finally
                Conn.Close()

            End Try
        End If

        Return m_arCategoryFieldID

    End Function



    ' ----------  ARPATECH  ----------
    ' function for Retrieving action request
    ' category display name
    ' ----------  ARPATECH  ----------
    Function GetARCategoryDisplayName(ByVal arCategoryID As Integer) As String
        Dim catName As New String("")

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try
            Cmd.CommandText = "select arcatname from tblARCategories where arCatID=" & arCategoryID

            Dim dr As SqlDataReader
            dr = Cmd.ExecuteReader
            While dr.Read()

                catName = dr.GetString(dr.GetOrdinal("arcatname"))

            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error getting arcatname from DB: " & e.Message)
        Finally
            Conn.Close()

        End Try

        Return catName

    End Function


End Class

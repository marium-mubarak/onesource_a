' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class purpose is to create multiple expressions and sub-expressions
'                       for or to create a ConJunction. Function.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Windows.Forms

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class Expression
    Private m_rValue As Object
    Private m_lValue As Object
    Private m_operator As [Operator]
    Private m_conjunction As Conjunction
    Private m_level As Integer = 0
    Private m_value As Object
    Private m_lValueDataType As Integer
    Private m_rValueDataType As Integer
    Private m_returnDataType As Integer
    Protected m_subExpressions As New SortedList()
    Public m_subExps As New ArrayList()
    Public m_parentExp As Expression = Nothing
    Public m_siblingCount As Integer = 0

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' data types as integer
    ' ----------  ARPATECH  ----------
    Public Property ReturnDataType() As Integer
        Get
            Dim dt As Integer = vbBoolean
            If Not IsNothing(m_operator.OperatorSymbol) Then
                Dim s As String = m_operator.OperatorSymbol
                If s = "+" Or s = "-" Or s = "*" Or s = "/" Then
                    dt = vbDouble
                End If
            End If
            Return dt
        End Get
        Set(ByVal Value As Integer)
            m_returnDataType = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' sub level of expression
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
    Public Property SubExpressions() As SortedList
        Get
            Return m_subExpressions
        End Get
        Set(ByVal Value As SortedList)
            m_subExpressions = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' level of expression
    ' ----------  ARPATECH  ----------
    Public Property Level() As Integer
        Get
            Return m_level
        End Get
        Set(ByVal Value As Integer)
            m_level = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operators of expression
    ' ----------  ARPATECH  ----------
    Public Property [Operator]() As [Operator]
        Get
            Return m_operator
        End Get
        Set(ByVal Value As [Operator])
            m_operator = Value
            If m_operator.OperatorSymbol = "==" Or m_operator.OperatorSymbol = "<" Or m_operator.OperatorSymbol = ">" Or m_operator.OperatorSymbol = ">=" Or m_operator.OperatorSymbol = "<=" Or m_operator.OperatorSymbol = "!=" Then
                m_returnDataType = vbBoolean
            Else
                m_returnDataType = vbDouble
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' R value
    ' ----------  ARPATECH  ----------
    Public Property RValue() As Object
        Get
            Return m_rValue
        End Get
        Set(ByVal Value As Object)
            m_rValue = Value
            m_rValueDataType = GetDataType(m_rValue)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' least value
    ' ----------  ARPATECH  ----------
    Public Property LValue() As Object
        Get
            Return m_lValue
        End Get
        Set(ByVal Value As Object)
            m_lValue = Value
            m_lValueDataType = GetDataType(m_lValue)
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' value
    ' ----------  ARPATECH  ----------
    Public Property Value() As Object
        Get
            Return m_value
        End Get
        Set(ByVal Value As Object)
            m_value = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' con junction
    ' ----------  ARPATECH  ----------
    Public Property Conjunction() As Conjunction
        Get
            Return m_conjunction
        End Get
        Set(ByVal Value As Conjunction)
            m_conjunction = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' function for geting values out of expressions
    ' ----------  ARPATECH  ----------
    Function GetValue() As Object

        Select Case (m_operator.OperatorSymbol)
            Case ("==")
                If IsNumber(m_lValueDataType) And IsNumber(m_rValueDataType) Then
                    If CDbl(m_lValue) = CDbl(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf m_lValueDataType = vbBoolean And m_lValueDataType = vbBoolean Then
                    If CBool(m_lValue) = CBool(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf m_lValueDataType = vbString And m_lValueDataType = vbString Then
                    If CStr(UCase(Trim(m_lValue))) = CStr(UCase(Trim(m_rValue))) Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Case ("!=")
                If IsNumber(m_lValueDataType) And IsNumber(m_rValueDataType) Then
                    If CDbl(m_lValue) <> CDbl(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                ElseIf m_lValueDataType = vbBoolean And m_lValueDataType = vbBoolean Then
                    If CBool(m_lValue) = CBool(m_rValue) Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf m_lValueDataType = vbString And m_lValueDataType = vbString Then
                    If CStr(UCase(Trim(m_lValue))) = CStr(UCase(Trim(m_rValue))) Then
                        Return False
                    Else
                        Return True
                    End If
                End If
            Case (">")
                If IsNumber(m_lValueDataType) And IsNumber(m_rValueDataType) Then
                    If CDbl(m_lValue) > CDbl(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    EventLog.WriteEntry("Application", "ERROR: Tried to use > with non-numerics)")
                End If


            Case ("<")
                If IsNumber(m_lValueDataType) And IsNumber(m_rValueDataType) Then
                    If CDbl(m_lValue) < CDbl(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    EventLog.WriteEntry("Application", "ERROR: Tried to use < with non-numerics)")
                End If

            Case (">=")
                If IsNumber(m_lValueDataType) And IsNumber(m_rValueDataType) Then
                    If CDbl(m_lValue) >= CDbl(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    EventLog.WriteEntry("Application", "ERROR: Tried to use >= with non-numerics)")
                End If

            Case ("<=")
                If IsNumber(m_lValueDataType) And IsNumber(m_rValueDataType) Then
                    If CDbl(m_lValue) <= CDbl(m_rValue) Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    EventLog.WriteEntry("Application", "ERROR: Tried to use <+ with non-numerics)")
                End If


        End Select


    End Function


    ' Create new simple expression
    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal fd As FormulaDetail)
        ' Dim e As New Expression()
        '   If Not IsNothing(fd.Operator) Then

        ' Console.WriteLine("Creating Expression with formuladetail" & fd.GetText)

        m_lValue = fd.Var1
        m_rValue = fd.Var2
        m_operator = fd.[Operator]
        If Not IsNothing(fd.Conjunction) Then
            m_conjunction = fd.Conjunction
        End If

        m_level = 0
        '    Console.WriteLine("Created Expression" & GetText() & " and isSimple=" & Me.isSimple())

        '  End If
    End Sub


    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with arguments
    ' ----------  ARPATECH  ----------
    ' Create an expression with subexpressions of n formula details or expressions on an equal level.
    Public Sub New(ByVal fds As ArrayList, Optional ByVal level As Integer = 0)
        Dim i As Integer = 0
        Dim ex As New Expression()
        Dim lastConj As Conjunction
        For i = 0 To fds.Count - 1
            If Not IsNothing(fds.Item(i)) Then
                '  MessageBox.Show("fds " & i & " is not nothing")
                If fds.Item(i).GetType().ToString() = "GSSupport.FormulaDetail" Then
                    ' Create simple expression for each formula detail
                    Dim fd As New FormulaDetail()
                    fd = fds.Item(i)
                    If Not IsNothing(fd.Conjunction) Then
                        lastConj = fd.Conjunction
                    End If


                    ' If it's the last fd in the batch set expression conjunction to the fd conjunction 
                    ' and the fd conjunction to nothing
                    '           If i = fds.Count - 1 Then
                    '        Dim c As New Conjunction()
                    '    c = fd.Conjunction
                    '     fd.Conjunction = Nothing
                    '     ex.Conjunction = c

                    '  End If
                    Dim n As New Expression(fd)
                    n.Level = level

                    ex.AddSubExpression(n)
                    '             MessageBox.Show("Added subex for Formuladetail" & i)
                ElseIf fds.Item(i).GetType.ToString = "GSSupport.Expression" Then
                    Dim n As New Expression()
                    n = fds.Item(i)
                    If Not IsNothing(n.Conjunction) Then
                        lastConj = n.Conjunction
                    End If
                    ex.AddSubExpression(n)
                End If

            End If
        Next
        If ex.m_subExpressions.Count > 0 Then
            If Not IsNothing(lastConj) Then
                ex.Conjunction = lastConj
            End If

            '         MessageBox.Show("Adding subex " & ex.GetText)
            AddSubExpression(ex)
        End If

    End Sub


    ' ----------  ARPATECH  ----------
    ' function that returns text (old or obsolote)
    ' ----------  ARPATECH  ----------
    Function GetTextOld() As String
        Dim s As New String("")
        Dim i As Integer
        Dim n As Integer
        Dim z As Integer
        Dim level As Integer = 0

        ' If it's a simple expression display values
        If isSimple() Then
            '          MessageBox.Show("In GetText for Simple Expression of level" & m_level)
            If Not IsNothing(m_level) Then
                For level = 1 To level
                    s = s & vbTab
                Next
            End If

            s = s & " LEVEL" & m_level
            If Not IsNothing(m_lValue) Then
                s = s & GetValText(m_lValue)
            End If

            If Not IsNothing(m_operator) Then

                s = s & " " & m_operator.OperatorSymbol
            End If

            If Not IsNothing(m_rValue) Then
                s = s & " " & GetValText(m_rValue) & " "
            End If

            If Not IsNothing(m_conjunction) Then
                s = s & m_conjunction.Text
            End If
            s = s & vbCrLf
            '    MessageBox.Show("Simple ex is " & s)
        ElseIf isComplex() Then
            '       MessageBox.Show("In GetText for Complex Expression")
            For i = 0 To m_subExpressions.Count - 1



                Dim l As Integer = 0


                '       MessageBox.Show("Getting subex level " & m_subExpressions.GetKey(i))
                Dim al As ArrayList
                If Not IsNothing(m_subExpressions.GetByIndex(i)) Then
                    '        MessageBox.Show(m_subExpressions.Item(n).GetType.ToString())
                    al = m_subExpressions.GetByIndex(i)
                    ' MessageBox.Show("there are " & al.Count & "expressions for level " & CInt(myEnumerator.Key))
                    ' Create begin parens for this level
                    ' Chop off first two
                    '    If n > 0 Then
                    If al.Count > 0 Then
                        '     s = s & "LEVEL" & m_subExpressions.GetKey(i) & " ( "
                        '    End If

                        For z = 0 To al.Count - 1
                            Dim etmp As Expression
                            If Not IsNothing(al.Item(z)) Then
                                etmp = al.Item(z)
                                s = s & etmp.GetText
                                If Not IsNothing(etmp.Conjunction) Then
                                    s = s & " " & etmp.Conjunction.Text
                                End If

                            End If
                        Next

                        ' Create end parens
                        ' Chop off last two
                        '    If n < m_subExpressions.Count - 1 Then
                        '      s = s & " ) "
                        s = s & vbCrLf
                        'End If

                    End If

                End If


                s = s & vbCrLf
                n = n + 1
            Next

        End If


        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns simple text 
    ' ----------  ARPATECH  ----------
    Function GetSimpleText() As String
        Dim s As String = ""
        If Not IsNothing(Me.LValue) Then
            s = s & GetValText(Me.LValue)
        End If
        If Not IsNothing(Me.[Operator]) Then
            s = s & " " & Me.[Operator].OperatorSymbol & " "
        End If
        If Not IsNothing(Me.RValue) Then
            s = s & GetValText(Me.RValue)
        End If
        If Not IsNothing(Me.Conjunction) Then
            s = s & " " & Me.Conjunction.Text & " "
        End If
        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns text 
    ' ----------  ARPATECH  ----------
    Function GetText(Optional ByVal level As Integer = 0) As String
        Dim s As New String("")
        Dim i As Integer
        Dim n As Integer
        Dim lastLevel As Integer = 1
        Dim deepestLevel As Integer = 0
        Dim lCount As Integer = 0
        '    Console.WriteLine("IN GETTEXT() and issimple is " & Me.isSimple & " and subcount is " & m_subExps.Count & " and level is " & Me.Level)

        If Me.isSimple Then
            '    Console.WriteLine("Getting simeple text...")
            s = s & Me.GetSimpleText  '& "level" & Me.Level
        Else


            For i = 0 To (m_subExps.Count - 1)
                '      Console.WriteLine("Not simeple, getting " & i)
                Dim e As Expression = m_subExps.Item(i)
                ' If e.Level <> level Then
                s = s & "(" & e.GetText(level + 1) & ")"
                '  Else
                '   s = s & e.GetText(level + 1)
                ' End If


            Next
            If Not IsNothing(Me.Conjunction) Then
                s = s & " " & Me.Conjunction.Text
            End If
        End If

        Return s
        '    MessageBox.Show("i")


    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns text (old or obsolote)
    ' ----------  ARPATECH  ----------
    Function GetTextOldOld() As String
        Dim s As New String("")
        Dim i As Integer
        Dim n As Integer
        Dim lastLevel As Integer = 1
        Dim deepestLevel As Integer = 0
        Dim lCount As Integer = 0


        If m_subExpressions.Count = 0 Then
            Return s
        End If

        deepestLevel = m_subExpressions.GetKey(m_subExpressions.Count - 1)

        '    MessageBox.Show("i")
        For i = 0 To deepestLevel

            s = s & " ("
            lCount = lCount + 1

            If m_subExpressions.ContainsKey(i) Then

                Dim level As Integer
                level = i
                '       MessageBox.Show("iii")
                If Not IsNothing(m_subExpressions.Item(level)) Then
                    Dim al As ArrayList
                    al = m_subExpressions.Item(level)
                    '         MessageBox.Show("iv")

                    For n = 0 To al.Count - 1
                        '               MessageBox.Show("Alcount is " & al.Count & " and n is " & n)
                        If Not IsNothing(al.Item(n)) Then
                            '         MessageBox.Show("v")
                            Dim e As New Expression()
                            e = al.Item(n)
                            '        MessageBox.Show("vi")
                            If e.isSimple() Then
                                '         MessageBox.Show("vii")
                                '   If al.Count > 1 Then
                                '      s = s & " ("
                                ' End If
                                If Not IsNothing(e.LValue) Then
                                    s = s & GetValText(e.LValue)
                                End If
                                If Not IsNothing(e.[Operator]) Then
                                    s = s & " " & e.[Operator].OperatorSymbol & " "
                                End If
                                If Not IsNothing(e.RValue) Then
                                    s = s & GetValText(e.RValue)
                                End If
                                If Not IsNothing(e.Conjunction) And Not (i = deepestLevel And n = (al.Count - 1)) Then
                                    s = s & " " & e.Conjunction.Text & " "
                                End If

                                '  If al.Count > 1 Then
                                '    s = s & ") "
                                '   End If

                            End If
                        End If
                    Next
                End If

            End If
        Next
        '      MessageBox.Show("lCount is " & lCount)
        For n = 1 To lCount
            s = s & ") "
        Next

        If Not IsNothing(m_conjunction) Then
            s = s & m_conjunction.Text
        End If
        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' simplicity status
    ' ----------  ARPATECH  ----------
    Public Function isSimple() As Boolean
        If m_subExps.Count = 0 And Not IsNothing(m_lValue) And Not IsNothing(m_rValue) Then
            Return True
        End If
        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' complexity status
    ' ----------  ARPATECH  ----------
    Public Function isComplex() As Boolean
        If m_subExpressions.Count > 0 Then
            Return True
        End If
        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' sub for making a dump graph
    ' ----------  ARPATECH  ----------
    Sub DumpGraph(Optional ByVal level As Integer = 0)
        Dim i As Integer = 0

        '  Console.WriteLine(vbCrLf)
        If Me.Level <> level Then
            Console.Write(vbCrLf & "LEVEL " & Me.Level & vbTab)
        End If
        level = Me.Level
        If Me.isSimple Then
            Console.Write("[SIMPLE (" & Me.GetText() & ")]")
        Else
            Console.Write("[COMPLEX]")
        End If
        For i = 0 To m_subExps.Count - 1
            Dim ex As Expression = m_subExps.Item(i)
            ex.DumpGraph(level)
        Next


    End Sub

    ' ----------  ARPATECH  ----------
    ' function for adding sub expression
    ' ----------  ARPATECH  ----------
    Public Function AddSubExp(ByVal e As Expression, Optional ByVal topIndex As Integer = 0) As Integer
        Dim i As Integer = 0
        Dim ex As Expression = New Expression()
        If Not IsNothing(e.Level) Then
            i = e.Level
        End If
        Dim lastConj As Conjunction
        If Not IsNothing(e.Conjunction) Then
            lastConj = e.Conjunction
        End If
        Console.WriteLine("Trying to add exp " & e.GetText() & " level " & e.Level & " topIndex " & topIndex & " and have " & m_subExps.Count & " children and sibcount is " & e.m_siblingCount)


        ' Create Toplevel expressions if needed
        If Not topIndex < m_subExps.Count Then
            i = m_subExps.Count - topIndex
            If i <= 0 Then
                While i <= 0
                    ' If the level is zero then make it a sibling by creating a parent and pushing everything down
                    If e.Level = 0 And i < 1 Then
                        Console.WriteLine("1")
                        '     Dim newParent As New Expression()
                        '     Me.PushDownLevel(1)
                        '    e.PushDownLevel(1)
                        '    Me.m_siblingCount = 0
                        '   e.m_siblingCount = 1
                        '  newParent.m_subExps.Add(Me)
                        '  newParent.m_subExps.Add(e)
                        Console.WriteLine("PUSHING DOWN")
                        e.PushDownLevel(1)
                        m_subExps.Add(e)


                        '          Console.WriteLine("Added  new top exp level" & e.Level)
                        Return m_subExps.Count
                    Else
                        ' otherwise create blank top expr
                        ex = New Expression()
                        ex.Level = 0
                        ex.m_parentExp = Me
                        ex.m_siblingCount = m_subExps.Count
                        m_subExps.Add(ex)
                        '        Console.WriteLine("Adding blank exp level" & ex.Level)
                    End If

                    i = m_subExps.Count - topIndex
                End While
            End If
        End If
        Console.WriteLine("Checking Levels...")
        If e.Level = 0 Then
            m_subExps.Insert(topIndex, e)
            Return m_subExps.Count
        End If


        ' Now go down however many levels we need...
        Dim topex As Expression = m_subExps.Item(topIndex)
        '  Dim topex As Expression = New Expression()
        '  topex.Level = 0
        '  m_subExps.Insert(topIndex, topex)
        '  e.m_siblingCount = topex.m_subExps.Count
        '  topex.m_subExps.Add(e)
        ' If the subex is same level as me add it as a parent and push everything else down a level
        If e.Level = topex.Level Then

            m_subExps.Add(e)
            '    If Not IsNothing(e.Conjunction) Then
            '       Me.Conjunction = e.Conjunction
            '   End If
            Return m_subExps.Count
        Else

            For i = 0 To e.Level
                Console.WriteLine("eElvel is " & e.Level & " topex level is " & topex.Level)
                If e.Level = topex.Level + 1 Then
                    e.m_siblingCount = topex.m_subExps.Count
                    e.m_parentExp = topex

                    topex.m_subExps.Add(e)
                    If Not IsNothing(e.Conjunction) Then
                        topex.Conjunction = e.Conjunction
                    End If
                    Console.WriteLine("ADDED" & e.GetText() & " to " & topex.GetText() & " and topIndex is " & topIndex & " and level is " & e.Level & " and siblingCOunt is " & e.m_siblingCount)
                    Return topex.m_subExps.Count()
                    '    Return m_subExps.Count
                ElseIf topex.m_subExps.Count > 0 Then
                    ' If this subexpressions has more subexpressions dig deeper
                    Console.WriteLine("Digging deeper in existing children")
                    ' Add to last subexp
                    Return topex.m_subExps.Item(topex.m_subExps.Count - 1).AddSubExp(e, topIndex)
                    '     Return topex.AddSubExp(e, topIndex)

                Else
                    ' Otherwise add empty subexpression with expression as sub-expression
                    ex = New Expression()
                    ex.Level = e.Level - 1
                    '  If Not IsNothing(lastConj) Then
                    '     ex.Conjunction = lastConj
                    '  End If
                    '      ex.m_siblingCount = topex.m_subExps.Count
                    ex.m_subExps.Add(e)
                    e.m_parentExp = ex
                    e.m_siblingCount = 0
                    '    ex.m_parentExp = topex
                    Console.WriteLine("Creating new parent")
                    Return topex.AddSubExp(ex, topIndex)

                    '         Console.WriteLine("ADDED EXPR col" & topIndex & " level" & ex.Level & " " & ex.GetText())


                End If
                '    End If
                '    i = i + 1

            Next
        End If


    End Function

    ' ----------  ARPATECH  ----------
    ' function to Add an array of formula 
    ' details as simple sub-expressions
    ' ----------  ARPATECH  ----------
    Public Function AddSubExpression(ByVal e As Expression, Optional ByVal topIndex As Integer = 0) As Integer
        Dim i As Integer = 0
        If Not IsNothing(e.Level) Then
            i = e.Level
        End If
        If i >= DeepestLevel() Then
            If Not IsNothing(e.Conjunction) Then
                m_conjunction = e.Conjunction
            End If

        End If
        '  Console.WriteLine("Adding SUBEX " & e.GetText)
        ' Add a key-pair
        If m_subExpressions.ContainsKey(i) Then
            '      Console.WriteLine("Adding array value to subex level " & i & " expr " & e.GetText)
            Dim al As New ArrayList()
            '       MessageBox.Show(m_subExpressions.Item(i).GetType.ToString)
            al = m_subExpressions.Item(i)
            '    MessageBox.Show("10")
            If IsNothing(al) Then
                '        MessageBox.Show("al is nothing")
            End If
            If IsNothing(e) Then
                '        MessageBox.Show("e is nothing")
            End If
            al.Add(e)
            '        MessageBox.Show("11")
        Else
            '  MessageBox.Show("Creating array value to subex level " & i & " expr  " & e.GetText)
            Dim al As New ArrayList()
            al.Add(e)
            m_subExpressions.Add(i, al)
            '   OrderSubExpressions()

        End If
        Return m_subExpressions.Count

    End Function

    ' Add an array of formula details as simple sub-expressions
    Public Function AddSubExpression(ByVal fds As ArrayList, Optional ByVal level As Integer = 0, Optional ByVal topcount As Integer = 0) As Integer
        '      MessageBox.Show("In Addsuex with fd count " & fds.Count)
        '  If Not level = 0 Then
        Dim i As Integer = 0
        For i = 0 To fds.Count
            Dim fd As FormulaDetail = fds.Item(i)
            Dim e As New Expression(fd)
            e.Level = level
            AddSubExp(e, topcount)
        Next



        '  End If
        Return m_subExps.Count

    End Function


    ' Add an array of formula details as simple sub-expressions
    Public Function AddSubExpressionThroughTop(ByVal fds As ArrayList, Optional ByVal level As Integer = 0, Optional ByVal topIndex As Integer = 0) As Integer
        '      MessageBox.Show("In Addsuex with fd count " & fds.Count)
        '  If Not level = 0 Then
        Dim i As Integer = 0
        Try

            If fds.Count > 1 Then
                ' If there's more than one fd then make each one a subExpression of a parent expression of the specified level
                Dim ex As New Expression()
                ex.Level = level
                For i = 0 To fds.Count - 1
                    Dim fd As FormulaDetail = fds.Item(i)
                    Dim e As New Expression(fd)
                    e.Level = level + 1
                    e.m_parentExp = ex
                    ' parent conjunction is conjunction of last subexpression
                    If Not IsNothing(e.Conjunction) Then
                        ex.Conjunction = e.Conjunction
                        ' e.Conjunction = Nothing
                    End If
                    Console.WriteLine("Created expression " & e.GetText() & " from formula details...")
                    '    If IsNothing(e.m_parentExp) Then
                    '       Console.WriteLine("Parent exp nothing")
                    '   Else
                    '       Console.WriteLine("Parent exp not nothing")
                    '  End If

                    ex.m_subExps.Add(e)


                    '  Console.WriteLine("About to add " & e.GetText())

                Next
                AddSubExp(ex, topIndex)
            Else
                ' Otherwise just create the expression and add it
                Dim fd As FormulaDetail = fds.Item(0)
                Dim e As New Expression(fd)
                e.Level = level
                ' Console.WriteLine("level is " & level)
                AddSubExp(e, topIndex)
            End If



        Catch e As Exception
            EventLog.WriteEntry("Application", "Error adding subex through top:" & e.Message)
        End Try


        Return m_subExps.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' subroutine for order sub expressions 
    ' ----------  ARPATECH  ----------
    Sub OrderSubExpressions()

        Dim n As Integer = 0
        Dim al As New ArrayList()
        Dim sl As New SortedList()

        sl = m_subExpressions
        m_subExpressions = New SortedList(sl.Count)

        '  MessageBox.Show("2")
        Dim myEnumerator As IDictionaryEnumerator = sl.GetEnumerator()
        ' MessageBox.Show("3")
        While myEnumerator.MoveNext()
            ' Create an ArrayList of ascending ints representing exp levels
            Dim i As New Integer()
            '      MessageBox.Show("4")
            i = CInt(myEnumerator.Key())


            ' Iterate through array of levels and insert this level.
            For n = 0 To al.Count - 1
                If CInt(al.Item(n)) = i Then
                    Exit For
                ElseIf CInt(al.Item(n)) > i Then
                    al.Insert(n - 1, i)
                    Exit For
                End If
            Next
            '       MessageBox.Show("5")
            If Not al.Contains(i) Then
                al.Add(i)
            End If

        End While

        For n = 0 To al.Count - 1
            '    MessageBox.Show("Readding arraylist for level " & al(n))
            '    If Not CInt(al(n)) = 0 Then
            m_subExpressions.Add(al(n), sl.Item(al(n)))
            '    End If
        Next
        '   MessageBox.Show("m_sub size is " & m_subExpressions.Count)

    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for cleaning expressions
    ' ----------  ARPATECH  ----------
    Sub CleanExpression()
        Dim ar As New ArrayList()
        '   Console.WriteLine("ceaning subs with " & m_subExpressions.Count)
        ar = GetSimpleExpressions()
        Dim i As Integer
        m_subExpressions = New SortedList()
        For i = 0 To ar.Count - 1
            Dim e As New Expression()
            e = ar.Item(i)

            AddSubExpression(e)
        Next
    End Sub

    ' ----------  ARPATECH  ----------
    ' subroutine for cleaning expressions (OLD or obsolete may be)
    ' ----------  ARPATECH  ----------
    Sub CleanExpressionOld()

        '       MessageBox.Show("BEFORE CLEAN IS " & GetText())

        Dim i As Integer
        Dim n As Integer
        Dim z As Integer
        Dim level As Integer = 0



        ' If it's a simple expression display values
        If isComplex() Then
            '       MessageBox.Show("BEFORE CLEAN IS " & GetText())
            Dim sl As New SortedList()
            sl = m_subExpressions
            m_subExpressions = New SortedList(sl.Count)
            '       MessageBox.Show("a")
            For i = 0 To sl.Count - 1
                '         MessageBox.Show("b")
                Dim l As Integer = 0

                If Not IsNothing(sl.GetByIndex(i)) Then
                    '           MessageBox.Show("c")
                    Dim al As New ArrayList()
                    al = sl.GetByIndex(i)

                    If al.Count > 0 Then

                        '             MessageBox.Show("e")
                        For z = 0 To al.Count - 1
                            Dim etmp As New Expression()
                            If Not IsNothing(al.Item(z)) Then
                                etmp = al.Item(z)

                                If etmp.isSimple Then
                                    AddSubExpression(etmp)
                                Else
                                    etmp.CleanExpression()
                                End If
                            End If

                        Next

                    End If

                End If

                n = n + 1
            Next

        End If
        OrderSubExpressions()
        '   MessageBox.Show("AFTER CLEAN IS " & GetText())
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that retrieves simple expression in array list
    ' ----------  ARPATECH  ----------
    Function GetSimpleExpressions() As ArrayList
        Dim a As New ArrayList()

        If isComplex() Then
            Dim i As Integer
            Dim z As Integer
            For i = 0 To m_subExpressions.Count - 1

                If Not IsNothing(m_subExpressions.GetByIndex(i)) Then
                    Dim al As New ArrayList()
                    al = m_subExpressions.GetByIndex(i)
                    For z = 0 To al.Count - 1
                        Dim e As New Expression()
                        If Not IsNothing(al.Item(z)) Then
                            e = al.Item(z)
                            If e.isSimple Then
                                a.Add(e)
                            Else
                                a.AddRange(e.GetSimpleExpressions)
                            End If
                        End If

                    Next
                End If

            Next
        End If
        Return a
    End Function

    ' ----------  ARPATECH  ----------
    ' function for checking deepest level
    ' ----------  ARPATECH  ----------
    Function DeepestLevel() As Integer
        Dim i As Integer
        Dim n As Integer
        Dim dl As Integer = 0

        For i = 0 To m_subExpressions.Count - 1
            n = CInt(m_subExpressions.GetKey(i))
            If n > dl Then
                dl = n
            End If
        Next
        Return dl
    End Function

    ' ----------  ARPATECH  ----------
    ' subroutine for simple expression or tests
    ' ----------  ARPATECH  ----------
    Sub ReallySimpleOrTest(ByVal e As Expression, ByVal index As Integer)

        Dim i As Integer
        If Not IsNothing(e.Conjunction) Then
            Console.WriteLine(e.GetText() & "=" & e.Conjunction.Text)

        End If
        For i = index To e.m_subExps.Count - 1
            ReallySimpleOrTest(e.m_subExps.Item(i), index)
        Next

    End Sub




    ' ----------  ARPATECH  ----------
    ' function that checks for more ORS
    '  and returns true / flase 
    ' ----------  ARPATECH  ----------
    Function HasMoreAnds(ByVal e As Expression, Optional ByVal andcount As Integer = 0, Optional ByVal startX As Integer = 0, Optional ByVal startY As Integer = 0, Optional ByVal currentX As Integer = 0, Optional ByVal currentY As Integer = 0) As Boolean
        ' Dim andcount As Integer = 0
        Dim i As Integer
        Dim siblings As Integer = 0

        Console.WriteLine("****** Starting HasMoreAnds FOR " & e.GetText() & "=" & andcount & "level=" & e.Level & " startX=" & startX & " startY=" & startY & " currentX=" & currentX & "currentY=" & currentY)
        ' Don't go backward
        If e.Level = 0 And currentX < startX Then
            Return False
        End If


        siblings = e.m_siblingCount + 1
        '  If Not IsNothing(e.m_parentExp) Then
        '         Console.WriteLine("parent is " & e.m_parentExp.GetText())
        '  End If
        If Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "AND" Then
            Console.WriteLine("Has more ands for " & e.GetText() & " is true")
            Return True
        End If

        If Not IsNothing(e.m_parentExp) Then
            '  If e.Level > Level Then
            ' Don't go down below start level unless we're at 0 level
            If currentY = 0 Or ((currentY + 1) <= startY) Then
                ' Don't care about the last expression
                For i = currentX To e.m_subExps.Count - 2
                    '    Console.WriteLine("****** Going over to " & e.m_subExps.Item(i).GetText())
                    If HasMoreAnds(e.m_subExps.Item(i), andcount, startX, startY, currentX + 1, currentY + 1) Then
                        '       Console.WriteLine("has more ands for " & e.m_subExps.Item(i).GetText() & " is true")
                        Return True
                    End If
                    i = i + 1
                Next
            End If

            If currentY > 0 Then
                '        Console.WriteLine("****** Going up to " & e.m_parentExp.GetText())
                ' andcount = andcount + AndsOnUp(e.m_parentExp, andcount)
                If HasMoreAnds(e.m_parentExp, andcount, startX, startY, currentX, currentY - 1) Then
                    Return True
                End If
            End If


        End If


        '   For i = siblings To e.m_parentExp.m_subExps.Count - 1
        '        Dim sib As Expression = e.m_parentExp.m_subExps.Item(i)
        '        Console.WriteLine("Checking child " & i & " " & sib.GetText())
        '        orcount = orcount + OrsOnUp(e.m_parentExp.m_subExps.Item(i), orcount, i)
        '        i = i + 1
        '     Next

        ' Console.WriteLine("****** Has More Ands FOR " & e.GetText() & "= false")

        Return False

    End Function

    ' ----------  ARPATECH  ----------
    ' function that checks for more ORS
    '  and returns true / flase 
    ' ----------  ARPATECH  ----------
    Function HasMoreOrs(ByVal e As Expression, Optional ByVal orcount As Integer = 0, Optional ByVal startX As Integer = 0, Optional ByVal startY As Integer = 0, Optional ByVal currentX As Integer = 0, Optional ByVal currentY As Integer = 0) As Boolean
        ' Dim andcount As Integer = 0
        Dim i As Integer
        Dim siblings As Integer = 0

        Console.WriteLine(" ****** Starting HasMoreOrs FOR " & e.GetText() & "=" & orcount & "level=" & e.Level & " startX=" & startX & " startY=" & startY & " currentX=" & currentX & "currentY=" & currentY)

        Try


            If Not IsNothing(e.Conjunction) Then
                Console.WriteLine("COnjunciton is " & e.Conjunction.Text)
                ' Else
                '     Console.WriteLine("Parent is nothing.")


            End If
            If e.Level = 0 And currentX < startX Then
                Console.WriteLine("Has more ors for " & e.GetText() & " is FALSE")
                Return False
            End If

            siblings = e.m_siblingCount + 1
            '   If Not IsNothing(e.m_parentExp) Then
            '         Console.WriteLine("parent is " & e.m_parentExp.GetText())
            '  End If
            If Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "OR" Then
                Console.WriteLine("Has more ors for " & e.GetText() & " is TRUE")
                Return True

            End If

            '  If e.Level > Level Then
            If Not IsNothing(e.m_parentExp) Then
                If (currentY + 1) <= startY Then
                    For i = currentX To e.m_subExps.Count - 2
                        Console.WriteLine("****** Going over to " & e.m_subExps.Item(i).GetText())
                        Console.WriteLine("i is " & i & " of " & e.m_subExps.Count - 1)
                        If HasMoreOrs(e.m_subExps.Item(i), orcount, startX, startY, currentX + 1, currentY + 1) Then
                            '                   Console.WriteLine("****** Has More Ors FOR " & e.m_subExps.Item(i).GetText() & "= true")
                            Console.WriteLine("Has more ors for " & e.GetText() & " is TRUE")
                            Return True
                        End If
                        i = i + 1
                    Next
                End If

                If currentY > 0 Then
                    '     Console.WriteLine("****** Going up to " & e.m_parentExp.GetText())
                    ' andcount = andcount + AndsOnUp(e.m_parentExp, andcount)
                    '      If HasMoreOrs(e.m_parentExp, orcount, startX, startY, currentX, currentY - 1) Then
                    '       Console.WriteLine("****** Has More Ors FOR " & e.m_parentExp.GetText() & "= true")

                    '      Return True
                    '     End If
                End If
            End If


            '   For i = siblings To e.m_parentExp.m_subExps.Count - 1
            '        Dim sib As Expression = e.m_parentExp.m_subExps.Item(i)
            '        Console.WriteLine("Checking child " & i & " " & sib.GetText())
            '        orcount = orcount + OrsOnUp(e.m_parentExp.m_subExps.Item(i), orcount, i)
            '        i = i + 1
            '     Next

            '  Console.WriteLine("****** Has More Ors FOR " & e.GetText() & "= false")

        Catch exc As Exception
            EventLog.WriteEntry("Application", "Error getting HasMoreOrs" & exc.Message)
        End Try
        Console.WriteLine("Has more ors finish for " & e.GetText() & " is FALSE")
        Return False

    End Function





    ' ----------  ARPATECH  ----------
    ' function for evaluate expression
    ' ----------  ARPATECH  ----------
    Function Evaluate(Optional ByVal topXCount As Integer = 0, Optional ByVal level As Integer = 0) As Object
        Dim b As New Boolean()
        Dim d As New Double()
        Dim i As Integer = 0
        Dim n As Integer = 0
        Dim lval As Object
        Dim rval As Object
        Dim prevConj As Conjunction
        Dim conjtext As String = ""
        Dim prevconjtext As String = ""

        Try

            '    If Me.isSimple Then
            '       Return EvaluateSimple(Me)
            '  End If

            If IsNothing(m_subExps) Then
                '       EventLog.WriteEntry("Application", "sub exps null for " & Me.GetText())
                '            Return False
            End If

            Console.WriteLine("Evaluating as object " & GetText())

            '  EventLog.WriteEntry("Application", "Evaluating as object " & GetText())
            Console.WriteLine("I am level" & Me.Level & " and have " & m_subExps.Count & " subexpressions and am child " & m_siblingCount)
            ' DumpGraph(-1)
            '  If level <= Me.Level + 1 Then
            For i = 0 To (m_subExps.Count - 1)
                ' If Not IsNothing(m_subExpressions.GetByIndex(i)) Then
                Dim e As New Expression()


                If Not IsNothing(m_subExps.Item(i)) Then
                    e = m_subExps.Item(i)
                    '            EventLog.WriteEntry("Application", "Evaling subx  " & i & " level" & e.Level & " " & e.GetText())

                    ' ConjOnUp(e)
                    '   Console.WriteLine("ands remaining is " & RemainingAnds(e, i))
                    '   Console.WriteLine("ors remaining is " & RemainingOrs(e, i))
                    If e.Level = 0 Then
                        '     TopExpression = e
                        '       Console.WriteLine("TOP EXPRESSION = " & e.GetText())
                    End If
                    If e.Level = 1 Then
                        topXCount = i
                    End If


                    If e.isSimple() Then
                        '   Console.WriteLine("This ex count is " & n)
                        '  Console.WriteLine("ands remaining is " & RemainingAnds(al, n))
                        '  Console.WriteLine("ors remaining is " & RemainingOrs(al, n))
                        '   If Not IsNothing(e.LValue) Then
                        '       Console.WriteLine(e.LValue.ToString)
                        '    End If
                        '    If Not IsNothing(e.RValue) Then
                        '        Console.WriteLine(e.RValue.ToString)
                        '   End If

                        '    If IsNothing(lval) Then
                        ' MessageBox.Show("lval is nothing")
                        lval = EvaluateSimple(e)
                        ' Return EvaluateSimple(e)
                    Else
                        lval = e.Evaluate(topXCount, level)
                        '   Return e.Evaluate(topXCount)

                    End If

                    Console.WriteLine("**************************** Expression col " & i & " level " & e.Level & " is " & GetValText(lval) & " " & conjtext)
                    '       If Not IsNothing(prevConj) Then
                    '         Console.WriteLine("prevConj is " & UCase(prevConj.Text))
                    '      End If

                    If Not IsNothing(e.Conjunction) Then
                        '      Console.WriteLine("conj is " & e.Conjunction.Text)
                        conjtext = UCase(e.Conjunction.Text)
                    End If
                    ' Since we're on the same level if we're already true and its an OR we bail
                    ' Use next conjunction...
                    '  If Not IsNothing(prevConj) AndAlso UCase(prevConj.Text) = "OR" AndAlso GetDataType(lval) = vbBoolean AndAlso UCase(e.Conjunction.Text) <> "AND" Then
                    '    If Not IsNothing(prevConj) AndAlso GetDataType(lval) = vbBoolean AndAlso Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "OR" AndAlso RemainingAnds(al, n) = 0 Then

                    '    If CBool(lval) = True Then
                    '     b = True
                    '   Console.WriteLine("A True")
                    '   Return b
                    '    End If
                    ' Else
                    ' If Not IsNothing(prevConj) AndAlso UCase(prevConj.Text) = "AND" AndAlso GetDataType(lval) = vbBoolean Then
                    '  If CBool(lval) = False AndAlso RemainingOrs(al, n) = 0 Then
                    '    MessageBox.Show("PPPPPPPP")
                    '   b = False
                    '   Console.WriteLine("B False")
                    '   Return b
                    ' End If

                    '  Else
                    ' If we're true and my conjunction is or then return true
                    If CBool(lval) = True AndAlso UCase(conjtext) = "OR" Then
                        Return True
                    End If

                    If CBool(lval) = False AndAlso UCase(conjtext) = "AND" Then
                        Return False
                    End If
                    ' If we're false and no more ORs on this level or above us then return false


                    '  MessageBox.Show("IIIIIIIIIII")
                    '  lval = EvaluateSimple(e)
                    '    MessageBox.Show("rval is " & GetValText(rval))
                    '   If GetDataType(rval) = vbBoolean Then
                    '      b = CBool(rval)
                    '       Return b
                    '  End If


                    '   End If

                    '   If n = al.Count - 1 Then
                    'Return lval
                    '   End If
                    If i = (m_subExps.Count - 1) Then
                        level = level + 1
                    End If


                    If Not IsNothing(e.Conjunction) Then
                        prevConj = e.Conjunction
                    End If
                End If

            Next

            '     End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error evaluating expression: " & e.Message)
        End Try

        Return lval
    End Function

    ' ----------  ARPATECH  ----------
    ' function for evaluate sample expression
    ' ----------  ARPATECH  ----------
    Function EvaluateSimple(ByVal e As Expression) As Object
        Dim b As New Boolean()
        Dim d As New Double()
        Dim dt As New Date()


        Dim lval As Object = e.m_lValue
        Dim rval As Object = e.m_rValue
        If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then
            lval = GSSupportManager.ReplaceDate(e.m_lValue)
            rval = GSSupportManager.ReplaceDate(e.m_rValue)

            ' EventLog.WriteEntry("Application", "lval=" & lval.ToString() & ", rval=" & rval.ToString())

        End If

        '  Console.WriteLine("in evaluatesimple with " & e.GetText())

        If Not IsNothing(e.m_operator) Then
            Select Case (e.m_operator.OperatorSymbol)
                Case ("==")
                    If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then
                        b = CDate(lval) = CDate(rval)
                    ElseIf IsNumeric(lval.ToString()) AndAlso IsNumeric(rval.ToString()) Then
                        b = CDbl(lval) = CDbl(rval)
                    Else
                        b = CStr(lval.ToString()) = CStr(rval.ToString())
                    End If
                    Return b
                Case ("!=")
                    If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then
                        b = CDate(lval) <> CDate(rval)
                    ElseIf IsNumeric(lval.ToString()) AndAlso IsNumeric(rval.ToString()) Then
                        b = CDbl(lval) <> CDbl(rval)
                    Else
                        b = CStr(lval.ToString()) <> CStr(rval.ToString())
                    End If
                    Return b
                Case (">")
                    If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then
                        b = CDate(lval) > CDate(rval)
                    ElseIf IsNumeric(lval.ToString()) AndAlso IsNumeric(rval.ToString()) Then
                        b = CDbl(lval) > CDbl(rval)
                    Else
                        b = False
                    End If
                    Return b
                Case (">=")
                    If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then
                        b = CDate(lval) >= CDate(rval)
                    ElseIf IsNumeric(lval.ToString()) AndAlso IsNumeric(rval.ToString()) Then

                        b = CDbl(lval) >= CDbl(rval)
                    Else
                        b = False
                    End If
                    Return b
                Case ("<")
                    If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then

                        b = CDate(lval) < CDate(rval)
                    ElseIf IsNumeric(lval.ToString()) AndAlso IsNumeric(rval.ToString()) Then

                        b = CDbl(lval) < CDbl(rval)
                    Else
                        b = False
                    End If

                    Return b
                Case ("<=")
                    If IsDate(lval.ToString()) AndAlso IsDate(rval.ToString()) Then
                        b = CDate(lval) >= CDate(rval)
                    ElseIf IsNumeric(lval.ToString()) AndAlso IsNumeric(rval.ToString()) Then

                        b = CDbl(lval) >= CDbl(rval)
                    Else
                        b = False
                    End If
                    Return b
                Case ("+")
                    d = CDbl(lval) + CDbl(rval)
                    Return d
                Case ("-")
                    d = CDbl(lval) - CDbl(rval)
                    Return d
                Case ("/")
                    d = CDbl(lval) / CDbl(rval)
                    Return d
                Case ("*")
                    d = (CDbl(lval) * CDbl(rval))
                    Return d
                Case ("LIKE")
                    If InStr(UCase(CStr(lval)), UCase(CStr(rval))) > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                Case ("NOT LIKE")
                    If InStr(UCase(CStr(lval)), UCase(CStr(rval))) = 0 Then
                        Return True
                    Else
                        Return False
                    End If
            End Select
        End If

    End Function


    ' ----------  ARPATECH  ----------
    ' function for evaluate string 
    ' ----------  ARPATECH  ----------
    Function EvaluateToString() As String
        Dim s As New String("")
        Dim i As Integer

        '    MessageBox.Show("Evaluating " & GetText())

        Dim o As Object = Evaluate()
        If Not IsNothing(o) Then
            s = GetValText(o)
        End If

        '   MessageBox.Show("Evaluated to " & s)
        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' function for counting remaning ORs
    ' ----------  ARPATECH  ----------
    Function RemainingOrs(ByVal al As ArrayList, ByVal i As Integer) As Integer
        Dim orcount As Integer = 0
        ' For i = i To al.Count - 1
        If Not IsNothing(al(i)) Then
            Dim e As Expression
            e = al(i)

            '  Console.WriteLine("rvalue is " & e.RValue.ToString())
            If Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "OR" Then
                If (i <> al.Count - 1) Then
                    orcount = orcount + 1
                End If

            End If


        End If
        ' Next
        Return orcount
    End Function


    ' See how many Ors we have left in an expression
    ' ----------  ARPATECH  ----------
    ' function for counting remaning ORs
    ' ----------  ARPATECH  ----------
    Function RemainingOrs(ByVal e As Expression, ByVal i As Integer, Optional ByVal OrCount As Integer = 0) As Integer
        ' Console.WriteLine("COUNTING ORS FOR " & e.GetText() & " and subexs is " & e.m_subExps.Count)
        ' If it's simple take the last conjunction
        '   If e.isSimple Then
        If Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "OR" Then

            OrCount = OrCount + 1
        End If

        '      Else
        ' If it's complex take the last conjunction of each subexpression

        ' If we're the last subexpression return 0
        '   If i >= e.m_subExps.Count - 1 Then
        '   Return orcount
        '   End If
        Dim ex As Expression
        Dim x As Integer = i
        '  Console.WriteLine("going from " & x & " to " & e.m_subExps.Count - 1)
        For x = 0 To e.m_subExps.Count - 1
            '     Console.WriteLine("X is " & x)
            ex = e.m_subExps.Item(x)
            OrCount = OrCount + RemainingOrs(ex, x, OrCount)
            x = x + 1
        Next
        '     End If
        '       Console.WriteLine(e.GetText & " has " & OrCount & " remaining ors")
        Return OrCount

    End Function

    ' ----------  ARPATECH  ----------
    ' function for counting levels
    ' ----------  ARPATECH  ----------
    Function CountLevels(ByVal e As Expression, Optional ByVal i As Integer = 0) As Integer
        Dim subcount As Integer = 0
        If e.m_subExps.Count > 0 Then
            i = i + i
            Dim ex As Expression = e.m_subExps.Item(0)
            Return CountLevels(ex, i)

        End If
    End Function



    ' See how many ANDS we have left in an expression
    ' ----------  ARPATECH  ----------
    ' function for  counting ANDs OVERLOADED
    ' ----------  ARPATECH  ----------
    Function RemainingAnds(ByVal e As Expression, ByVal i As Integer) As Integer
        '  Console.WriteLine("getting remaining ands for " & e.GetText())


        Dim andcount As Integer = 0
        ' If it's simple take the last conjunction
        '    If e.isSimple Then
        If Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "AND" Then
            andcount = andcount + 1
        End If

        '  Else
        '    Console.WriteLine("Not Simple" & e.m_subExps.Count & " countlevels=" & CountLevels(e))
        ' If it's complex take the last conjunction of each subexpression

        ' If we're the last subexpression return 0
        If i >= e.m_subExps.Count - 1 Then
            '      Return andcount
        End If
        Dim ex As Expression
        Dim x As Integer = i
        For x = x To e.m_subExps.Count - 1
            ex = e.m_subExps.Item(x)
            andcount = andcount + RemainingAnds(ex, x)
            x = x + 1

        Next
        '   End If
        ' Console.WriteLine(e.GetText & " has " & andcount & " remaining ands")

        Return andcount

    End Function

    ' ----------  ARPATECH  ----------
    ' function  for counting ANDs
    ' ----------  ARPATECH  ----------
    Function RemainingAnds(ByVal al As ArrayList, ByVal i As Integer) As Integer
        Dim andcount As Integer = 0
        '  For i = i To al.Count - 1
        If Not IsNothing(al(i)) Then
            Dim e As Expression
            e = al(i)
            If Not IsNothing(e.Conjunction) AndAlso UCase(e.Conjunction.Text) = "AND" Then
                If i <> al.Count - 1 Then
                    andcount = andcount + 1
                End If

            End If

        End If
        '      Next
        Return andcount

    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine for 
    ' removing trailing con junctions
    ' ----------  ARPATECH  ----------
    Public Sub RemoveTrailingConj()
        If Me.isSimple Then
            If Not IsNothing(Me.Conjunction) Then
                Me.Conjunction = Nothing
                Exit Sub
            End If
        Else
            ' Take the last subexpression from top
            Dim e As Expression = m_subExps.Item(m_subExps.Count - 1)
            If Not IsNothing(e) Then
                e.RemoveTrailingConj()
            End If
        End If
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for 
    ' pussing donw exp levels
    ' ----------  ARPATECH  ----------
    Sub PushDownLevel(ByVal levels As Integer)
        Dim i As Integer = 0
        Me.Level = Me.Level + levels
        For i = 0 To m_subExps.Count - 1
            Dim e As Expression = m_subExps.Item(i)
            e.PushDownLevel(levels)
        Next

    End Sub

End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains all operations for SLA process like saving, fetching,
'                       and saving TAC etc.
' Inherits          :   ---------
' Implements        :   ISLA, IMQUtils, IGSEventListener
' Friends           :   GSSupportManager
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
Imports System.Messaging
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports MSMQ
Imports MSMQ.MQACCESS

Imports MSMQ.MQSHARE
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface ISLA

    Property SLAName() As String
    Property SLADesc() As String
    Property MQLabel() As String
    Property Manager() As GSSupportManager
    Property SLAID() As Integer

    Function RemoveTAC(ByVal index As Integer) As Integer

    Function AddTAC(ByVal tac As TermsAndConditions) As Integer
    Function GetTAC(ByVal index As Integer) As TermsAndConditions
    Function GetTACCount() As Integer
    Property CreateByUserID() As Integer
    Function Save() As Boolean


End Interface


<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class SLA
    Implements ISLA
    Implements IMQUtils
    Implements IGSEventListener

    Protected Friend m_tacs As New ArrayList()
    Private m_mqLabel As String
    Private m_slaID As Integer = 0
    Private m_createByUserID As Integer = 0
    Private m_slaName As String
    Private m_isActive As Boolean = True
    Private m_isListening As Boolean = False
    Private m_slaDesc As String
    Private m_evaluatedARs As New Hashtable()
    Private m_activeInstances As New ArrayList()
    Private m_gsSupportManager As GSSupportManager
    Private m_gsListener As GSEventListener

    ' ----------  ARPATECH  ----------
    ' FUNCTION for adding terms and conds
    ' to counting and array list
    ' ----------  ARPATECH  ----------
    Function AddTAC(ByVal tac As TermsAndConditions) As Integer Implements ISLA.AddTAC
        Dim i As Integer
        tac.ParentSLA = Me

        For i = 0 To m_tacs.Count - 1
            Dim t As TermsAndConditions = m_tacs(i)
            If t.Rank > tac.Rank Then
                m_tacs.Insert(i, tac)
                Return m_tacs.Count
            End If
        Next

        m_tacs.Add(tac)
        Return m_tacs.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA ID
    ' ----------  ARPATECH  ----------
    Public Property SLAID() As Integer Implements ISLA.SLAID
        Get
            Return m_slaID
        End Get
        Set(ByVal Value As Integer)
            m_slaID = Value
        End Set
    End Property

  

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS support manager
    ' ----------  ARPATECH  ----------
    Public Property Manager() As GSSupportManager Implements ISLA.Manager
        Get
            Return m_gsSupportManager
        End Get
        Set(ByVal Value As GSSupportManager)
            m_gsSupportManager = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA created by user ID
    ' ----------  ARPATECH  ----------
    Public Property CreateByUserID() As Integer Implements ISLA.CreateByUserID
        Get
            Return m_createByUserID
        End Get
        Set(ByVal Value As Integer)
            m_createByUserID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA name
    ' ----------  ARPATECH  ----------
    Public Property SLAName() As String Implements ISLA.SLAName
        Get
            Return m_slaName
        End Get
        Set(ByVal Value As String)
            m_slaName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' MQ label
    ' ----------  ARPATECH  ----------
    Public Property MQLabel() As String Implements ISLA.MQLabel
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA description
    ' ----------  ARPATECH  ----------
    Public Property SLADesc() As String Implements ISLA.SLADesc
        Get
            Return m_slaDesc
        End Get
        Set(ByVal Value As String)
            m_slaDesc = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' FUNCTION for retrieving terms and conds counts
    ' ----------  ARPATECH  ----------
    Function GetTACCount() As Integer Implements ISLA.GetTACCount
        Return m_tacs.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' FUNCTION for retrieving terms and conds
    ' ----------  ARPATECH  ----------
    Function GetTAC(ByVal index As Integer) As TermsAndConditions Implements ISLA.GetTAC
        If index < m_tacs.Count Then
            Return m_tacs.Item(index)
        End If
    End Function

    ' ----------  ARPATECH  ----------
    ' FUNCTION for saving SLA in database
    ' ----------  ARPATECH  ----------
    Function Save() As Boolean Implements ISLA.Save
        Dim i As Integer = 0
        Dim b As Boolean = True
        Try



            ' FIRST delete existing TACs if we're updating an existing SLA
            If m_slaID > 0 Then
                If Not RemoveTACsFromDB() Then
                    EventLog.WriteEntry("Application", "Unable to inactive old TACs")
                    Return False
                End If
            End If

            ' Next update DB and get a slaid ID if I don't have one already...
            If Not SLAUpdateDB() Then
                EventLog.WriteEntry("Application", "Unable to save workflow to DB")
                Return False
            End If

            ' Next let's make sure each TAC is saved...
            If Not SaveTACs() Then
                EventLog.WriteEntry("Application", "Unable to save TACs.")
                Return False
            End If
            '  EventLog.WriteEntry("Application", "About to send to Q")
            If Not UpdateQ() Then
                EventLog.WriteEntry("Application", "Unable to send to SLA Q.")
                Return False
            End If



            '    RestartListener()


        Catch e As Exception
            b = False
            EventLog.WriteEntry("Application", "Error saving SLA: " & e.Message)
        End Try

        Return b

    End Function

    ' ----------  ARPATECH  ----------
    ' FUNCTION for updating message queue
    ' and return status
    ' ----------  ARPATECH  ----------
    Function UpdateQ() As Boolean
        Dim b As Boolean = False

        Try
            Dim mes As MSMQMessage = CreateMQMessage()
            Dim label As String = mes.Label

            ' First remove any SLA of the same name...

            Dim strFormatName As String
            Dim qinfo As New MSMQQueueInfo()
            Dim qDest As MSMQQueue

            ' Create a direct format name.
            strFormatName = "DIRECT=OS:" & GSSupportManager.GetQHost() & SLAQ


            ' Set the FormatName property of the MSMQQueueInfo object.

            qinfo.FormatName = strFormatName

            ' Open the queue.
            qDest = qinfo.Open(MQ_RECEIVE_ACCESS, MQ_DENY_NONE)
            qDest.Reset()

            Dim m As MSMQMessage = qDest.PeekCurrent(ReceiveTimeout:=500)

            ' Peek at messages in the queue using the cursor.
            Do While Not m Is Nothing
                If m.Label = label Then
                    qDest.ReceiveCurrent()
                    Exit Do
                End If
                m = qDest.PeekNext(ReceiveTimeout:=500)
            Loop
            qDest.Close()

            ' Now send the current SLA
            b = SendToQ(IncomingSLAQ, CreateMQMessage)
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error sending SLA to Q: " & e.Message)

        End Try

        Return b

    End Function


    ' ----------  ARPATECH  ----------
    ' FUNCTION for creating message queue
    ' ----------  ARPATECH  ----------
    Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage
        Dim l As String
        Dim m As New MSMQMessage()

        Try
            Dim t As Type = GetType(GSSupport.SLA)
            Dim serializer As New XmlSerializer(t)

            Dim objxml As New String("")

            Dim ns As New XmlSerializerNamespaces()

            ns.Add("GSSupport", "https://onesource.global-serve.com")

            Dim ms As MemoryStream = New MemoryStream()

            Dim writer As New XmlTextWriter(ms, New UTF8Encoding())

            ' Serialize using the XmlTextWriter.

            serializer.Serialize(writer, Me, ns)

            Dim arChars As Char()
            ReDim arChars(ms.Length)

            ms.ToArray.CopyTo(arChars, 0)

            objxml = New String(arChars)


            writer.Close()
            'Dim mexml As String = ObjToXML(Me)

            m.Label = "SLA=" & CStr(m_slaID) & "&NAME=" & m_slaName
            m_mqLabel = m.Label
            m.Body = objxml


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error converting SLA to message: " & e.Message & "," & e.Source & "," & e.StackTrace)
        End Try

        Return m

    End Function

    ' ----------  ARPATECH  ----------
    ' FUNCTION for updating SLA in DB
    ' ----------  ARPATECH  ----------
    Function SLAUpdateDB() As Boolean
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim isSubmitted As Boolean = True

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn

        Cmd.CommandType = CommandType.StoredProcedure

        Try

            Cmd.CommandText = "sp_updateSLA"

            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@slaname").Value = m_slaName

            Cmd.Parameters("@sladesc").Value = m_slaDesc

            Cmd.Parameters("@isactive").Value = m_isActive
            Cmd.Parameters("@slaid").Value = m_slaID

            Cmd.ExecuteNonQuery()

            If m_slaID = 0 Then
                m_slaID = Cmd.Parameters("@slaid").Value
            End If



        Catch e As Exception

            isSubmitted = False
            EventLog.WriteEntry("Application", "Failed to save SLA:" & e.ToString)
        Finally
            'Cmd = Nothing
            Conn.Close()
        End Try

        Return isSubmitted

    End Function

    ' ----------  ARPATECH  ----------
    ' FUNCTION for saving terms and conds
    ' ----------  ARPATECH  ----------
    Function SaveTACs() As Boolean
        Dim i
        For i = 0 To m_tacs.Count - 1
            Dim tac As TermsAndConditions = m_tacs(i)

            If Not tac.Save Then
                EventLog.WriteEntry("Application", "Unable to save TAC" & tac.TACID)
                Return False
            End If
        Next
        Return True
    End Function




    ' ----------  ARPATECH  ----------
    ' FUNCTION for removing term and conds
    ' ----------  ARPATECH  ----------
    Function RemoveTAC(ByVal index As Integer) As Integer Implements ISLA.RemoveTAC
        '       If index < m_tacs.Count - 1 Then
        m_tacs.RemoveAt(index)

        '     End If
        Return m_tacs.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with SLA ID argument
    ' and sql connection 
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal slaid As Integer, ByRef Conn As SqlConnection)
        '   Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim tn As New String("")
        m_slaID = slaid
        '  Conn.Open()

        Dim Cmd As New SqlCommand()


        Try
            Dim dr As SqlDataReader
            Cmd.CommandText = "select slaName, isNull(slaDescription, '') as slaDesc, isNUll(CreateByUserID, 0) as createbyuserid, isActive from tblSLA where slaid=" & slaid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                m_slaName = dr.GetString(dr.GetOrdinal("slaName"))

                m_slaDesc = dr.GetString(dr.GetOrdinal("slaDesc"))
                m_isActive = dr.GetBoolean(dr.GetOrdinal("isActive"))
                m_createByUserID = dr.GetInt32(dr.GetOrdinal("CreateByUserID"))

            End While
            dr.Close()

            Cmd.CommandText = "select tacid from tblTermsAndConditions where isActive=1 and slaid=" & slaid
            dr = Cmd.ExecuteReader
            Dim arTacs As New ArrayList()

            While dr.Read()
                arTacs.Add(dr.GetInt32(dr.GetOrdinal("tacid")))
                '  AddTAC(New TermsAndConditions(dr.GetInt32(dr.GetOrdinal("tacid"))))


            End While

            dr.Close()

            Dim i As Integer
            For i = 0 To arTacs.Count - 1

                AddTAC(New TermsAndConditions(arTacs(i), Conn))

            Next

        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to get sla:" & e.Message)

        Finally

            '  Conn.Close()
            '  Cmd = Nothing

            ' Conn = Nothing
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with SLA ID argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal slaid As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim tn As New String("")
        m_slaID = slaid
        Conn.Open()

        Dim Cmd As New SqlCommand()


        Try
            Dim dr As SqlDataReader
            Cmd.CommandText = "select slaName, isNull(slaDescription, '') as slaDesc, isNUll(CreateByUserID, 0) as createbyuserid, isActive from tblSLA where slaid=" & slaid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                m_slaName = dr.GetString(dr.GetOrdinal("slaName"))
                m_slaDesc = dr.GetString(dr.GetOrdinal("slaDesc"))
                m_isActive = dr.GetBoolean(dr.GetOrdinal("isActive"))
                m_createByUserID = dr.GetInt32(dr.GetOrdinal("CreateByUserID"))

            End While
            dr.Close()

            Cmd.CommandText = "select tacid from tblTermsAndConditions where isActive=1 and slaid=" & slaid
            dr = Cmd.ExecuteReader

            While dr.Read()

                AddTAC(New TermsAndConditions(dr.GetInt32(dr.GetOrdinal("tacid"))))


            End While

            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to get sla:" & e.Message)

        Finally

            Conn.Close()
            Cmd = Nothing

            Conn = Nothing
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' FUNCTION for removing term and conds
    ' from DB and return status
    ' ----------  ARPATECH  ----------
    Function RemoveTACsFromDB() As Boolean
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim isSubmitted As Boolean = True

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn

        Cmd.CommandType = CommandType.StoredProcedure

        Try

            Cmd.CommandText = "sp_removeTACs"
            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@slaid").Value = m_slaID

            Cmd.ExecuteNonQuery()

        Catch e As Exception

            isSubmitted = False
            EventLog.WriteEntry("Application", "Failed to remove TACs:" & e.ToString)
        Finally
            'Cmd = Nothing
            Conn.Close()
        End Try

        Return isSubmitted


    End Function

    ' ----------  ARPATECH  ----------
    ' FUNCTION for matching action request
    ' with expression and return status
    ' ----------  ARPATECH  ----------
    Function MatchAR(ByVal ar As ActionRequest, Optional ByRef expr As String = "") As Boolean
        Dim i As Integer
        Dim bRet As Boolean = False

        Try

            ' EventLog.WriteEntry("Application", "In SLA ismatch and number of tacs is " & m_tacs.Count)

            ' Evaluate see if this launches an SLA instance
            For i = 0 To m_tacs.Count - 1
                Dim t As TermsAndConditions = m_tacs(i)
                ' EventLog.WriteEntry("Application", "evaluating " & t.GetText & " against ar " & ar.ActionRequestID)

                If t.Qualification.Evaluate(ar, expr) Then
                    ' EventLog.WriteEntry("Application", "ar is true for slaid " & SLAID & ", tacid " & t.TACID)
                    Dim slam As New SLAMonitor(ar, m_tacs(i))
                    slam.Manager = m_gsSupportManager
                    slam.GSListener = m_gsListener
                    '  slam.GSListener = ar.GSListener
                    '  slam.Manager = ar.Manager
                    slam.GSTicketID = ar.GSTicketID
                    '    EventLog.WriteEntry("Application", "SLA montior created by sla with hash " & slam.GetType.GetHashCode)

                    SendToQ(SLAInProgressQ, slam.CreateMQMessage())

                    bRet = True
                Else
                    ' EventLog.WriteEntry("Application", "ar is false for slaid " & SLAID & ", tacid " & t.TACID & ", expr" & expr)

                End If

            Next
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error checking sla.ismatch: " & e.Message)
        End Try
        Return bRet
    End Function



    ' ----------  ARPATECH  ----------
    ' sub routine for on action request event
    ' for logging in event log file
    ' ----------  ARPATECH  ----------
    Public Sub OnActionRequestEvent(ByVal sender As Object, ByVal e As ActionRequestEventArgs) Implements IGSEventListener.OnActionRequestEvent
        '   EventLog.WriteEntry("Application", "RECEIVED AR EVENT")
        Dim ar As ActionRequest = e.ActionRequest
        Dim expr As New String("")

        '  EventLog.WriteEntry("Application", "sla" & m_slaID & " got at " & ar.ActionRequestID)

        Dim isMatch As Boolean = MatchAR(ar, expr)
        LogQualificationResult(ar.ActionRequestID, isMatch, expr)

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for on timer event
    ' for logging in event log file
    ' ----------  ARPATECH  ----------
    Public Sub OnGSTimerEvent(ByVal sender As Object, ByVal e As GSTimerEventArgs) Implements IGSEventListener.OnGSTimerEvent
        EventLog.WriteEntry("Application", "RECEIVED GSTimer EVENT")
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for on action completion event
    ' for logging in event log file
    ' ----------  ARPATECH  ----------
    Public Sub OnActionCompleteEvent(ByVal sender As Object, ByVal e As ActionCompleteEventArgs) Implements IGSEventListener.OnActionCompleteEvent
        EventLog.WriteEntry("Application", "RECEIVED AC EVENT")
    End Sub


    ' ----------  ARPATECH  ----------
    ' sub routine to be executed on
    ' ticketing event 
    ' ----------  ARPATECH  ----------
    Public Sub OnTicketEvent(ByVal sender As Object, ByVal e As TicketEventArgs) Implements IGSEventListener.OnTicketEvent
        '  EventLog.WriteEntry("Application", "RECEIVED TICKET EVENT")
        ' Was the request type changed?
        Try
            If e.IsTypeChange Then
                Dim oldARID As Integer = Manager.GetSingleDBValueInt("select arid from tblTicket where ticketid=" & e.GSTicketID)

                ' EventLog.WriteEntry("Application", "Rechecking SLAs with AR " & oldARID & " type " & e.NewTypeID)
                Dim newAR As New ActionRequest(oldARID)
                Dim args As New ActionRequestEventArgs()
                newAR.GSTicketID = e.GSTicketID
                newAR.TypeID = e.NewTypeID
                Dim expr As New String("")
                newAR.AddFieldWithDisplay("Event", "1", "New Request")
                newAR.AddFieldWithDisplay("Request Type", e.NewTypeID, GSSupportManager.GetSingleDBValueString("Select arTypeName from tblActionRequestType where arTypeID=" & e.NewTypeID))

                args.ActionRequest = newAR
                Dim isMatch As Boolean = MatchAR(newAR, expr)
                LogQualificationResult(newAR.ActionRequestID, isMatch, expr)
                '   sla.MatchAR(newAR)
                ' EventLog.WriteEntry("Application", "sla " & SLAID & "," & isMatch & "," & expr)


            End If
        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error SLA handling ticket event: " & ex.Message)
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine to be executed on order event
    ' ----------  ARPATECH  ----------
    Public Sub OnOrderEvent(ByVal sender As Object, ByVal e As OrderEventArgs) Implements IGSEventListener.OnOrderEvent
        EventLog.WriteEntry("Application", "SLA received order event")
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Listenner
    ' ----------  ARPATECH  ----------
    Public Property GSListener() As GSEventListener Implements IGSEventListener.GSListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            m_gsListener = Value
            AddHandler m_gsListener.ActionRequestEvent, New ActionRequestEventHandler(AddressOf Me.OnActionRequestEvent)
            AddHandler m_gsListener.TicketEvent, New TicketEventHandler(AddressOf Me.OnTicketEvent)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for logging qualifiation
    ' result in Lof file
    ' ----------  ARPATECH  ----------
    Sub LogQualificationResult(ByVal arid As Integer, ByVal isMatch As Boolean, Optional ByRef expr As String = "")
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()

        Dim Cmd As New SqlCommand()
        Dim sql As String

        Dim m As Integer
        If isMatch Then
            m = 1
        Else
            m = 0
        End If

        Try
            sql = "insert into tblQualificationLog (actionRequestID, slaid, isMatch, expression) values (" & arid & "," & m_slaID & "," & m & ",'" & expr & "')"

            Cmd.Connection = Conn
            Cmd.CommandText = sql

            Cmd.ExecuteNonQuery()

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Unable to update sla qualificaltion log: " & ex.Message)
        Finally
            Conn.Close()

        End Try
    End Sub



End Class

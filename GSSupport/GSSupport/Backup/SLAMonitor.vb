' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is a monitor for SLA and consists of functions to check several
'                       issues like checking minutes to breach SLA, updating status, getting start
'                       and stop date, creating holidays / work shifts.
' Inherits          :   ---------
' Implements        :   IMQUtils, ISLAMonitor, IGSEventListener
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
Imports System.Timers
Imports System.Threading
Imports System.Messaging
Imports GSSupport
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text


' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface ISLAMonitor
    Property Manager() As GSSupportManager
    Property Test() As String
    Property TAC() As TermsAndConditions
    Property IsBreach() As Boolean
    Property ClosedWithinSLA() As Boolean
    Property IsException() As Boolean
    Function GetTACString() As String
    Function GetLocalTime() As Date
    Sub CreateWorkshifts()
    Sub CreateHolidays()
    Sub UpdateSLAInstanceDB(ByVal comments As String, ByVal isClosed As Boolean, ByVal localdate As Date)


    Sub RemoveHandlers()


End Interface


<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class SLAMonitor
    Implements IMQUtils
    Implements ISLAMonitor
    Implements IGSEventListener

    Private m_SLA As SLA
    Private m_openTAC As TermsAndConditions
    Private m_actionRequest As ActionRequest
    Private m_slaInstanceID As Integer = 0
    Private m_mqLabel As New String("")
    Private m_timerTypeName As New String("")
    Private m_gsTicketID As Integer
    Private m_minutesToBreach As Integer
    Private m_isBreach As Boolean = False
    Private m_closedWithinSLA As Boolean = False
    Private m_isException As Boolean = False
    Private m_timeZoneID As Integer
    Private m_timeZoneFieldID As Integer
    Private m_timerStartDate As Date
    Private m_isRunning = False
    Private m_waitingForStatusChange = False
    Private m_ticketStatusID As Integer = 0
    Private m_isPaused As Boolean = False
    Private m_gsSupportManager As GSSupportManager
    Private m_test As String
    Private m_gsListener As GSEventListener
    ' Next stop date using SLA timezone
    Private m_nextStopDate As Date
    Private m_nextStartDate As New Date(Now.Ticks)
    Private m_workShifts As New SortedList()
    Private m_holidays As New ArrayList()
    Private m_exceptionStopTime As Date
    Private m_exceptionMinutes As Integer = 0
    Private m_testCounter As Integer = 0
    Private m_stopOverride As Boolean = False
    Private m_waitingForTargetChange As Boolean = False
    Private m_targetStatusID As Integer = 0
    Private m_targetStatusDate As DateTime

    Private m_teh As New TicketEventHandler(AddressOf Me.OnTicketEvent)
    Private m_gteh As New GSTimerEventHandler(AddressOf Me.OnGSTimerEvent)
    Private m_aceh As New ActionCompleteEventHandler(AddressOf Me.OnActionCompleteEvent)









    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

        '   AddHandler (New GSSupportManager()).TicketEvent, New TicketEventHandler(AddressOf Me.OnTicketEvent)

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' MQ label as string
    ' ----------  ARPATECH  ----------
    Public Property MQLabel() As String
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' terms and conds
    ' ----------  ARPATECH  ----------
    Public Property TAC() As TermsAndConditions Implements ISLAMonitor.TAC
        Get
            Return m_openTAC
        End Get
        Set(ByVal Value As TermsAndConditions)
            m_openTAC = Value
            m_timerTypeName = m_openTAC.TimerTypeName

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA paused status
    ' ----------  ARPATECH  ----------
    Public Property IsPaused() As Boolean
        Get
            Return m_isPaused
        End Get
        Set(ByVal Value As Boolean)
            m_isPaused = Value
            If m_isPaused Then
                m_isRunning = False
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' sla closed status
    ' ----------  ARPATECH  ----------
    Public Property ClosedWithinSLA() As Boolean Implements ISLAMonitor.ClosedWithinSLA
        Get
            Return m_closedWithinSLA
        End Get
        Set(ByVal Value As Boolean)
            m_closedWithinSLA = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' breach status
    ' ----------  ARPATECH  ----------
    Public Property IsBreach() As Boolean Implements ISLAMonitor.IsBreach
        Get
            Return m_isBreach
        End Get
        Set(ByVal Value As Boolean)
            m_isBreach = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' exception status
    ' ----------  ARPATECH  ----------
    Public Property IsException() As Boolean Implements ISLAMonitor.IsException
        Get
            Return m_isException
        End Get
        Set(ByVal Value As Boolean)
            m_isException = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' gs listener
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
    Public Property GSListener() As GSEventListener Implements IGSEventListener.GSListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            If IsNothing(m_gsListener) Then
                m_gsListener = Value
                AddHandler m_gsListener.TicketEvent, m_teh
                AddHandler m_gsListener.GSTimerEvent, m_gteh
                AddHandler m_gsListener.ActionCompleteEvent, m_aceh

            End If

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' constructor with SLA ID argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal slaiid As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim SLACount As Integer = 0
        Dim sql As New String("")
        m_slaInstanceID = slaiid


        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try
            Dim dr As SqlDataReader
            Dim ar As New ActionRequest()


            sql = "select ar.arid, slat.tacid, slat.ticketid, slat.assetid, slat.begindate, sla.slaid, slat.exceptionStopTime, slat.exceptionMinutes, " & _
            " slat.minutestobreach, slat.timezoneid from tblSLA sla, tblTermsAndConditions tac, tblSLATracking slat, tblTicket t, tblActionRequest ar where " & _
            " slat.ticketid = t.ticketid and t.arid = ar.arid and slat.tacid = tac.tacid and tac.slaid = sla.slaid and slat.slainstanceid=" & slaiid
            ' EventLog.WriteEntry("Application", sql)

            Cmd.CommandText = sql
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                ar = New ActionRequest(dr.GetInt32(dr.GetOrdinal("arid")))

                Dim tac As New TermsAndConditions(dr.GetInt32(dr.GetOrdinal("tacid")))
                Dim sla As New SLA(dr.GetInt32(dr.GetOrdinal("slaid")))

                m_actionRequest = ar
                m_openTAC = tac
                m_SLA = sla
                m_timerTypeName = tac.TimerTypeName

                Me.GSTicketID = dr.GetInt32(dr.GetOrdinal("ticketid"))

                If Not dr.IsDBNull(dr.GetOrdinal("minutesToBreach")) Then
                    m_minutesToBreach = dr.GetInt32(dr.GetOrdinal("minutesToBreach"))
                End If

                m_timeZoneID = dr.GetInt32(dr.GetOrdinal("timezoneid"))
                m_timeZoneFieldID = GSSupportManager.GetTimeZoneFieldID(m_timeZoneID)

                If Not dr.IsDBNull(dr.GetOrdinal("begindate")) Then
                    m_timerStartDate = dr.GetDateTime(dr.GetOrdinal("begindate"))
                End If


                If Not dr.IsDBNull(dr.GetOrdinal("exceptionStopTime")) Then
                    m_exceptionStopTime = dr.GetDateTime(dr.GetOrdinal("exceptionStopTime"))
                    m_stopOverride = True
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("exceptionMinutes")) Then
                    m_exceptionMinutes = dr.GetInt32(dr.GetOrdinal("exceptionMinutes"))
                End If




            End While
            dr.Close()

            CreateWorkshifts()

            If m_workShifts.Count = 0 Then
                ' No workshifts set

                RemoveHandler m_gsListener.TicketEvent, m_teh
                RemoveHandler m_gsListener.GSTimerEvent, m_gteh
                RemoveHandler m_gsListener.ActionCompleteEvent, m_aceh
                EventLog.WriteEntry("Application", "Error: no workshifts set for SLAIID " & m_slaInstanceID)


            End If
            CreateHolidays()
            m_isPaused = True
            Dim d As Date = SetTimerStartDate()
            ' If it's business days set target dates
            If (m_openTAC.BizDays >= 0 AndAlso m_openTAC.IsBizDays) Then
                ' EventLog.WriteEntry("Application", "Is Business Days, getting COB date")

                Dim statdate As Date = GetCloseOfBusinessDate(ar.RequestDateLocal)
                '   EventLog.WriteEntry("Application", "stat date is " & statdate.ToString)

                m_minutesToBreach = DateDiff(DateInterval.Minute, ar.RequestDateLocal, statdate)
                m_openTAC.MaxMinutes = m_minutesToBreach
                Me.UpdateMinutesToBreachDB(m_minutesToBreach)

            End If



        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to load sla from instance id " & slaiid & ": " & e.Message)

        Finally
            Conn.Close()

            ' Conn = Nothing
            ' Cmd = Nothing

        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' constructor with action request 
    ' and terms and conds argument
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal ar As ActionRequest, ByVal tac As TermsAndConditions)

        m_openTAC = tac
        m_SLA = tac.ParentSLA
        m_timerTypeName = tac.TimerTypeName
        m_actionRequest = ar
        '  m_gsSupportManager = m_SLA.Manager
        '  m_gsListener = m_SLA.GSListener
        m_minutesToBreach = m_openTAC.MaxMinutes
        m_timeZoneID = m_actionRequest.RequestDateTimeZoneID
        m_timeZoneFieldID = GSSupportManager.GetTimeZoneFieldID(m_timeZoneID)

        ' Create workshifts
        CreateWorkshifts()

        If m_workShifts.Count = 0 Then
            ' No workshifts set
            EventLog.WriteEntry("Application", "Error: no workshifts set.")
            RemoveHandler m_gsListener.TicketEvent, m_teh
            RemoveHandler m_gsListener.GSTimerEvent, m_gteh
            RemoveHandler m_gsListener.ActionCompleteEvent, m_aceh

        End If

        CreateHolidays()
        m_isPaused = True


        ' If it's business days set target dates
        If (m_openTAC.BizDays >= 0 AndAlso m_openTAC.IsBizDays) Then

            Dim statdate As Date = GetCloseOfBusinessDate(ar.RequestDateLocal)

            m_minutesToBreach = DateDiff(DateInterval.Minute, ar.RequestDateLocal, statdate)
            m_openTAC.MaxMinutes = m_minutesToBreach

            Me.UpdateMinutesToBreachDB(m_minutesToBreach)

        End If

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()


        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_openSLA"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_openTAC.TACID
            Cmd.Parameters("@ticketid").Value = m_actionRequest.GSTicketID
            Cmd.Parameters("@minutesToBreach").Value = m_minutesToBreach


            Dim d As Date = SetTimerStartDate()
            If Not IsNothing(d) AndAlso Year(d) > 2000 Then

                Cmd.Parameters("@begindate").Value = d
            End If

            Cmd.Parameters("@assetid").Value = m_actionRequest.GSAssetID
            Cmd.Parameters("@timezoneid").Value = m_actionRequest.RequestDateTimeZoneID

            Cmd.Parameters("@slainstanceid").Value = 0

            Cmd.ExecuteNonQuery()

            m_slaInstanceID = Cmd.Parameters("@slainstanceid").Value



        Catch e As Exception
            EventLog.WriteEntry("Application", "Error Creating SLAMonitor: " & e.Message)


        Finally

            Conn.Close()

            '     Conn = Nothing
            '      Cmd = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA monitor
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
    Public Property Manager() As GSSupportManager Implements ISLAMonitor.Manager
        Get
            Return m_gsSupportManager
        End Get
        Set(ByVal Value As GSSupportManager)
            m_gsSupportManager = Value

        End Set
    End Property
    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' terms and conds string
    ' ----------  ARPATECH  ----------
    Function GetTACString() As String Implements ISLAMonitor.GetTACString
        Return m_openTAC.ParentSLA.SLAName & "." & m_openTAC.TACName
    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket status
    ' ----------  ARPATECH  ----------
    Property TicketStatusID() As Integer
        Get
            Return m_ticketStatusID
        End Get
        Set(ByVal Value As Integer)
            m_ticketStatusID = Value
            Dim i As Integer
            Dim localdatetime As Date = GetLocalTime()

            ' First see if it's cancelled; if so delete SLA Monitor
            i = GSSupportManager.GetSingleDBValueInt("select count(tktstatusid) from tblTicketStatus where tktstatusid=" & m_ticketStatusID & " and isCancelled=1")
            If i > 0 Then
                m_isRunning = False
                m_isPaused = False
                DeleteFromDB()
                AddTicketComment("Ticket cancelled; SLAs removed.", m_ticketStatusID, localdatetime)
                Me.RemoveHandlers()

                Return
            End If

            ' Next see if it's an exception; if so end SLA Monitor
            i = GSSupportManager.GetSingleDBValueInt("select count(tktstatusid) from tblTicketStatus where tktstatusid=" & m_ticketStatusID & " and isException=1")
            If i > 0 Then
                m_isRunning = False
                m_isPaused = False
                UpdateSLAInstanceDB("SLA Exception", True, localdatetime)
                Me.RemoveHandlers()

                Return
            End If



            Dim arStatus As ArrayList
            '   If m_isRunning Then
            ' EventLog.WriteEntry("Application", "New Status is " & Value & " and isStatusEnd is " & IsStatusEnd(Value) & " and slaaid is " & m_slaInstanceID)
            If IsStatusEnd(Value) AndAlso Not m_isBreach AndAlso Not m_isException AndAlso Not m_closedWithinSLA Then
                m_isRunning = False
                m_isPaused = False
                m_closedWithinSLA = True
                UpdateSLAInstanceDB("Closed within SLA", True, localdatetime)
                Me.RemoveHandlers()

                Return
            End If


            If IsStatusException(Value) AndAlso Not m_isBreach AndAlso Not m_isException AndAlso Not m_closedWithinSLA Then
                m_isPaused = True
                m_isRunning = False
                m_waitingForStatusChange = True
                UpdateSLAInstanceDB("SLA timer suspended due to change in ticket status.", False)
                m_exceptionStopTime = Now()
                ToggleExceptionTimer(m_exceptionStopTime, 0)

            End If
            '   End If

            ' Timer resumed due to change in status
            If Not m_isRunning AndAlso m_waitingForStatusChange AndAlso Not IsStatusException(Value) AndAlso Not m_isBreach AndAlso Not m_isException AndAlso Not m_closedWithinSLA Then
                m_isPaused = False
                m_isRunning = True
                m_waitingForStatusChange = False
                ' EventLog.WriteEntry("Application", "Exception stop time is ")
                '  EventLog.WriteEntry("Application", " " & m_exceptionStopTime)
                If Not IsNothing(m_exceptionStopTime) AndAlso Year(m_exceptionStopTime) > 2000 Then
                    Dim m As Integer = DateDiff(DateInterval.Minute, m_exceptionStopTime, Now())

                    m_exceptionMinutes = m_exceptionMinutes + m
                    ToggleExceptionTimer(Nothing, m)
                    m_exceptionStopTime = Nothing

                End If

                UpdateSLAInstanceDB("SLA timer resumed due to change in ticket status.", False)
                If IsNothing(Me.m_timerStartDate) OrElse Year(m_timerStartDate) < 2000 Then
                    m_timerStartDate = New GSSupportManager().ConvertTime(Now(), ServerTimeZone, GSSupportManager.GetTimeZoneVBConst(m_timeZoneID))
                    Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
                    Dim SLACount As Integer = 0

                    Conn.Open()

                    Try
                        Dim Cmd As New SqlCommand()
                        Cmd.Connection = Conn
                        Dim sql As String
                        sql = "update tblSLATracking set begindate='" & m_timerStartDate & "' where slainstanceid=" & m_slaInstanceID
                        Cmd.CommandText = sql

                        Cmd.ExecuteNonQuery()

                    Catch e As Exception
                        EventLog.WriteEntry("Application", "Error updating SLA Timer Start: " & e.Message)

                    Finally
                        Conn.Close()

                    End Try

                End If
            End If

            ' Timer started for the first time due to change in status
            If Not m_isRunning AndAlso UCase(m_openTAC.StartTypeName) = "STATUS CHANGE" AndAlso Not m_isBreach AndAlso Not m_isException AndAlso Not m_closedWithinSLA Then
                If IsStatusStart(Value) Then
                    m_timerStartDate = Now
                    m_isPaused = False
                    m_isRunning = True
                    m_waitingForStatusChange = False
                    UpdateSLAInstanceDB("SLA timer started due to change in ticket status.", False)

                End If

            End If



        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function to create message queue
    ' ----------  ARPATECH  ----------
    Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage

        Dim l As String
        Dim m As New MSMQMessage()

        Try
            Dim t As Type = GetType(GSSupport.SLAMonitor)


            Dim serializer As New XmlSerializer(t)

            Dim objxml As New String("")

            Dim ns As New XmlSerializerNamespaces()


            Dim ms As MemoryStream = New MemoryStream()

            Dim writer As New XmlTextWriter(ms, New UTF8Encoding())

            ' Serialize using the XmlTextWriter.

            serializer.Serialize(writer, Me, ns)

            Dim arChars As Char()
            ReDim arChars(ms.Length)

            ms.ToArray.CopyTo(arChars, 0)

            objxml = New String(arChars)


            writer.Close()
            'Dim mexml As String = ObjToXML(Me)

            l = Now() & "&SLAIID=" & CStr(m_slaInstanceID) & "&SLAID=" & CStr(m_SLA.SLAID) & "&ARID=" & m_actionRequest.ActionRequestID

            m.Label = l
            m_mqLabel = l

            m.Body = objxml


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error converting SLA Monitor to message: " & e.Message & "," & e.InnerException.Message & e.Source & "," & e.StackTrace)
        End Try
        Return m


    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns local time of server
    ' ----------  ARPATECH  ----------
    Function GetLocalTime() As Date Implements ISLAMonitor.GetLocalTime
        Dim localdatetime As Date = Manager.ConvertTime(Now(), ServerTimeZone, Manager.GetTimeZoneVBConst(m_timeZoneID))
        Return localdatetime
    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns business
    ' closer date
    ' ----------  ARPATECH  ----------
    Function GetCloseOfBusinessDate(ByVal dRequestDate As Date) As Date
        Dim d As Date = GetLastEndDateToday(dRequestDate)
        '  EventLog.WriteEntry("Application", "last date today is with " & d)
        If m_openTAC.BizDays >= 0 Then
            ' Get first business day...
            Dim i As Integer = 0

            While i < m_openTAC.BizDays
                d = DateAdd(DateInterval.Day, 1, d)
                If IsBusinessDay(dRequestDate) Then
                    d = GetLastEndDateToday(d)

                End If
                '      EventLog.WriteEntry("Application", "d is " & d.ToString)
                i = i + 1


            End While



        End If
        Return d
    End Function



    ' ----------  ARPATECH  ----------
    ' function that returns SLA's minutes 
    ' to breach SLA as integer
    ' ----------  ARPATECH  ----------
    Function CalculateMinutesToBreach(ByVal localDateTime As Date) As Integer
        Dim m As Integer
        m = m_openTAC.MaxMinutes
        Console.WriteLine("Start Date:")
        Console.WriteLine(m_timerStartDate & " - " & localDateTime)

        Try


            ' If the timer hasn't started return maxminutes
            If IsNothing(m_timerStartDate) OrElse localDateTime < m_timerStartDate Then

                Return m
            ElseIf (IsNothing(m_timerStartDate) = False AndAlso UCase(m_openTAC.TimerTypeName) = "24X7" AndAlso m_openTAC.IncludeHolidays) Then
                ' If we're 24x7 just return the difference
                '   EventLog.WriteEntry("Application", "timer start date is " & m_timerStartDate & " and localDateTime is " & localDateTime & " and m is " & m & " and datefidd is " & DateDiff(DateInterval.Minute, m_timerStartDate, localDateTime))
                Return m - DateDiff(DateInterval.Minute, m_timerStartDate, localDateTime)
            Else

                'Otherwise calculate the number of in-scope minutes between timer start date and now
                Dim d As New Date()
                d = m_timerStartDate
                ' While Day(d) < Day(localDateTime)
                While DateDiff(DateInterval.Minute, d, localDateTime) > 0 AndAlso m > 0
                    Console.WriteLine("dddday is " & d & " and m is is " & m)

                    '      If m <= 0 Then
                    '      Return m
                    '      End If


                    ' Go to the next day
                    d = DateAdd(DateInterval.Day, 1, d)

                    ' Set time for 12:00:01 AM
                    d = New Date(d.Year, d.Month, d.Day, 0, 0, 1)


                    ' m = m - CalculateSLAMinutesPassedToday(d, localDateTime)
                    Dim m4 As Integer = CalculateSLAMinutesPassedToday(d, localDateTime)

                    Console.WriteLine("m is " & m & " and m4 today is " & m4)



                    m = m - m4



                End While

                ' Then subtract number of minutes for today
                Console.WriteLine("Calculating today...")
                Dim m3 As Integer = CalculateSLAMinutesPassedToday(localDateTime, localDateTime)
                Console.WriteLine("m is " & m & " and Minutes today is " & m3)
                m = m - m3

                Console.WriteLine("and now m is now " & m3)

            End If

            ' Finally add any exception minutes
            m = m + m_exceptionMinutes
        Catch e As Exception
            Console.WriteLine(e.Message)
            EventLog.WriteEntry("Application", e.Message)
        End Try


        Return m

    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns SLA's minutes 
    ' left today as integer
    ' ----------  ARPATECH  ----------
    Function CalculateSLAMinutesLeftToday(ByVal localdate As Date) As Integer
        Dim i As Integer
        Dim n As Integer
        Dim totalMinutes As Integer = 0
        i = localdate.DayOfWeek + 1
        Console.WriteLine("Calucating mltb for " & localdate)

        If IsHoliday(localdate) Then
            Return totalMinutes
        End If
        If Not m_workShifts.ContainsKey(i) Then
            Return totalMinutes
        Else

            Dim arShifts As ArrayList
            arShifts = m_workShifts.Item(i)
            For n = 0 To arShifts.Count - 1
                Dim ws As Workshift = arShifts(n)
                Dim sd As Date = ws.GetStartDateTime(localdate)
                Dim ed As Date = ws.GetEndDateTime(localdate)
                Console.WriteLine("Shift start is " & sd)
                Console.WriteLine("Shift end is " & ed)

                ' If current hour is less than start hour then add entire shift
                If localdate < sd Then
                    totalMinutes = totalMinutes + ws.GetTotalMinutes

                ElseIf localdate > sd AndAlso localdate < ed Then
                    ' If it's in the middle of a shift add number of remaining minutes.
                    totalMinutes = totalMinutes + DateDiff(DateInterval.Minute, localdate, ws.GetEndDateTime(localdate))
                End If

            Next
        End If

        Return totalMinutes
    End Function


    ' ----------  ARPATECH  ----------
    ' function that returns SLA's minutes 
    ' passed today as integer
    ' ----------  ARPATECH  ----------
    Function CalculateSLAMinutesPassedToday(ByVal localdate As Date, ByVal originalLocalDate As Date) As Integer
        Dim i As Integer
        Dim n As Integer
        Dim totalMinutes As Integer = 0
        Try
            i = localdate.DayOfWeek + 1

            Console.WriteLine("Checking cslmpt with " & localdate & " startdate is " & m_timerStartDate)

            If IsHoliday(localdate) Then
                Return totalMinutes
            End If

            ' If we're 24x7 with Holidays just return the difference..
            If (UCase(m_openTAC.TimerTypeName) = "24X7") Then
                Return (localdate.Hour * 60) + localdate.Minute
            End If


            If Not m_workShifts.ContainsKey(i) Then


                Return totalMinutes
            Else

                Dim arShifts As ArrayList
                arShifts = m_workShifts.Item(i)


                For n = 0 To arShifts.Count - 1


                    Dim ws As Workshift = arShifts(n)
                    Dim sd As Date = ws.GetStartDateTime(localdate)
                    Dim ed As Date = ws.GetEndDateTime(localdate)
                    Console.WriteLine("Shift start is " & sd)
                    Console.WriteLine("Shift end is " & ed)
                    '   Console.WriteLine("timer start is " & m_timerStartDate)
                    '    Console.WriteLine("localdate is " & localdate)
                    ' If current hour is greater than end hour then add entire shift
                    If localdate > ed Then
                        ' if the timer start is before the shift start
                        ' add difference between localdate and startdate
                        If m_timerStartDate < sd Then
                            totalMinutes = totalMinutes + ws.GetTotalMinutes
                            '  totalMinutes = totalMinutes + DateDiff(DateInterval.Minute, sd, localdate)
                        Else
                            ' If timerstart is in the middle of the shift add difference between timer start and shift end
                            totalMinutes = totalMinutes + Math.Abs(DateDiff(DateInterval.Minute, m_timerStartDate, ed))

                        End If
                        ' if localdate is before startdate and origaldate is after end date add whole shift

                    ElseIf localdate < sd AndAlso originalLocalDate > ed Then

                        totalMinutes = totalMinutes + ws.GetTotalMinutes



                    ElseIf localdate > sd AndAlso localdate < ed Then
                        ' If it's in the middle of a shift add number of expired minutes.

                        ' If the timer started today and timer start date is between the localdate
                        ' and the shift start (i.e. it didn't start during non-covered SLA times
                        ' then get number of minutes since timer start

                        If Day(m_timerStartDate) = Day(localdate) AndAlso Month(m_timerStartDate) = Month(localdate) AndAlso Year(m_timerStartDate) = Year(localdate) Then
                            ' Timer started today

                            ' Timer started AFTER the beginneing of the shift 
                            ' and the localdate is before the shift end...

                            ' Did timer start after localdate?
                            If m_timerStartDate >= localdate Then
                                ' If the original date is a later day then take timerStart -> ed
                                '  If DateDiff(DateInterval.Day, localdate, originalLocalDate) >= 1 Then
                                If Day(localdate) <> Day(originalLocalDate) Or Month(localdate) <> Month(originalLocalDate) Then
                                    totalMinutes = totalMinutes + Math.Abs(DateDiff(DateInterval.Minute, m_timerStartDate, ed))

                                End If
                                ' Otherwise if the timerstartdate is AFTER localdate then totalMinutes is 0
                                ' since the timer hasn't started

                            Else
                                ' If timer started BEFORE localdate then minutespassed = the difference
                                ' between the timer start and the localdate
                                If sd < m_timerStartDate Then
                                    totalMinutes = totalMinutes + Math.Abs(DateDiff(DateInterval.Minute, m_timerStartDate, localdate))
                                Else
                                    totalMinutes = totalMinutes + Math.Abs(DateDiff(DateInterval.Minute, ws.GetStartDateTime(localdate), localdate))

                                End If
                            End If


                        Else

                            'otherwise get number of minutes since start of shift
                            totalMinutes = totalMinutes + Math.Abs(DateDiff(DateInterval.Minute, ws.GetStartDateTime(localdate), localdate))

                        End If



                    End If
                    '        Console.WriteLine("totalMinutes is " & totalMinutes)
                Next
            End If
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error calculating SLA minutes passed today: " & e.Message)
        End Try
        '     Console.WriteLine("totalMinutes returning " & totalMinutes)
        Return totalMinutes
    End Function


    ' ----------  ARPATECH  ----------
    ' EMPTY
    ' ----------  ARPATECH  ----------
    Sub ProcessAR(ByVal ar As ActionRequest)


    End Sub

    ' ----------  ARPATECH  ----------
    ' function that returns SLA's start date
    ' ----------  ARPATECH  ----------
    Function SetTimerStartDate() As Date
        Dim d As Date
        Try


            If UCase(m_openTAC.StartTypeName) = "REQUEST DATE" Then

                d = m_actionRequest.RequestDateLocal

                m_timerStartDate = d

                '     m_isRunning = True
                m_nextStopDate = GetNextStopDate(d)


                '    ElseIf UCase(m_openTAC.StartTypeName) = "SUBMIT DATE" Then
                '       m_timerStartDate = m_actionRequest.SubmitDate
                '       m_isRunning = True
                '       m_nextStopDate = GetNextStopDate(m_actionRequest.SubmitDate)

            ElseIf UCase(m_openTAC.StartTypeName) = "TARGET DATE" Then
                m_isPaused = True
                '  m_waitingForTargetChange = True
                m_isRunning = False
            ElseIf UCase(m_openTAC.StartTypeName) = "STATUS CHANGE" Then
                m_isPaused = True
                '   m_waitingForStatusChange = True
                m_isRunning = False
            End If

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error gettingStartTime:" & ex.Message)
        End Try

        Return d
    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA test 
    ' ----------  ARPATECH  ----------
    Property Test() As String Implements ISLAMonitor.Test
        Get
            Return m_test
        End Get
        Set(ByVal Value As String)
            m_test = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS ticket ID
    ' ----------  ARPATECH  ----------
    Property GSTicketID() As Integer
        Get
            Return m_gsTicketID
        End Get
        Set(ByVal Value As Integer)
            m_gsTicketID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' routine for restarting listner
    ' ----------  ARPATECH  ----------
    Private Sub RestartListener()
        ' Listen for incoming action requests...
        Dim mq As MessageQueue = New MessageQueue(GSSupportManager.GetQHost() & TicketEventQ)

        mq.Formatter = New BinaryMessageFormatter()

        AddHandler mq.PeekCompleted, New PeekCompletedEventHandler(AddressOf OnPeekCompleted)
        mq.BeginPeek()

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to be executed on peek completion
    ' of any SLA
    ' ----------  ARPATECH  ----------
    Private Sub OnPeekCompleted(ByVal sender As System.Object, ByVal e As System.Messaging.PeekCompletedEventArgs)
        Try

            Dim mq1 As System.Messaging.MessageQueue = CType(sender, System.Messaging.MessageQueue)
            Dim m As System.Messaging.Message = mq1.EndPeek(e.AsyncResult)
            Dim ar As ActionRequest
            ar = CType(m.Body, GSSupport.ActionRequest)

            If ar.GSTicketID = m_actionRequest.GSTicketID Then
                UpdateFromDB()
            End If

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error SLA receiving postproc AR:" & ex.Message)
        Finally
            ' RestartListener()
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine to update db for ticket status
    ' ----------  ARPATECH  ----------
    Sub UpdateFromDB()
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim tn As New String("")
        Dim tktstatusid As Integer = 0

        Conn.Open()

        Dim Cmd As New SqlCommand()


        Try
            Dim dr As SqlDataReader
            Cmd.CommandText = "select t.ticketStatusid from tblTicket t, tblSLATracking slat where slat.ticketid = t.ticketid and slat.slainstanceid = " & m_slaInstanceID
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                tktstatusid = dr.GetInt32(dr.GetOrdinal("ticketstatusid"))


            End While
            dr.Close()

            If tktstatusid <> m_ticketStatusID Then
                Me.TicketStatusID = tktstatusid
            End If



        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to get timer type name:" & e.Message)

        Finally

            Conn.Close()
            '    Cmd = Nothing

            '    Conn = Nothing
        End Try
    End Sub



    ' ----------  ARPATECH  ----------
    ' routine to be executed on timer event
    ' ----------  ARPATECH  ----------
    Public Sub OnGSTimerEvent(ByVal sender As Object, ByVal e As GSTimerEventArgs) Implements IGSEventListener.OnGSTimerEvent

        ' If we've been manuually told to stop then return.

        EventLog.WriteEntry("Application", "SLA InstanceId: " & m_slaInstanceID & " received timer event")

        If (m_stopOverride) Then
            Exit Sub
        End If

        If m_isBreach OrElse m_closedWithinSLA Then
            Exit Sub
        End If

        Try


            Dim ar As ActionRequest = e.ActionRequest
            m_gsListener = Nothing

            m_gsListener = ar.GSListener

            Dim localdate As Date

            ' See if the request contains the local time for this SLA
            If ar.ContainsField(m_timeZoneFieldID) Then

                localdate = ar.GetFieldNativeValue(m_timeZoneFieldID)

                ' Target date SLAs only
                If UCase(m_openTAC.StartTypeName) = "TARGET DATE" Then
                    If Not IsNothing(m_nextStopDate) And Year(m_nextStopDate) > 2000 Then
                        Dim td As Integer = 0

                        td = DateDiff(DateInterval.Minute, localdate, m_nextStopDate)
                        ' EventLog.WriteEntry("Application", "TD and localdate is " & localdate & ", nextstop is " & m_nextStopDate & ", td is " & td)

                        If td <= 0 Then
                            ' No minutes left... must be busted SLA
                            m_isBreach = True
                            UpdateSLAInstanceDB("SLA timer expired at " & localdate & ". An SLA breach has occured.", True, localdate)
                            m_isRunning = False
                            ' Update DB with new minutes to breach
                            UpdateMinutesToBreachDB(td)

                        Else
                            m_isRunning = True
                            UpdateMinutesToBreachDB(td)

                        End If
                    Else
                        ' Is target date but no stop date means status doesn't have a target date...

                    End If
                    Exit Sub

                End If


                ' Otherwise get next stop time if one doesn't exist already...
                If IsNothing(m_nextStopDate) OrElse Year(m_nextStopDate) < MinYear Then

                    m_nextStopDate = GetNextStopDate(localdate)
                End If

                '   EventLog.WriteEntry("Application", m_slaInstanceID & " has timer type " & m_openTAC.TimerTypeName & " and include holidays is " & m_openTAC.IncludeHolidays)


                ' If I'm 24x7 with no holidays, or timer is running, calculate mins to breach
                If ((UCase(m_openTAC.TimerTypeName) = "24X7" AndAlso m_openTAC.IncludeHolidays)) OrElse m_openTAC.IsBizDays OrElse ShouldBeTicking(localdate) Then

                    Dim m As Integer = CalculateMinutesToBreach(localdate)
                    EventLog.WriteEntry("Application", m_slaInstanceID & " has " & m & " minutes left at " & localdate)

                    If m <= 0 Then
                        ' No minutes left... must be busted SLA
                        m_isBreach = True
                        UpdateSLAInstanceDB("SLA timer expired at " & localdate & ". An SLA breach has occured.", True, localdate)
                        m_isRunning = False
                    Else
                        ' Update DB with new minutes to breach
                        UpdateMinutesToBreachDB(m)

                    End If



                End If
            Else
                EventLog.WriteEntry("Application", "SLA Instance " & m_slaInstanceID & " couldn't find timezoneid " & m_timeZoneID & " in timer event")
            End If
            'AddHandler m_gsListener.GSTimerEvent, New GSTimerEventHandler(AddressOf Me.OnGSTimerEvent)
            '    RemoveHandlers()

        Catch ex As Exception
            EventLog.WriteEntry("Application", "SLA Monitor receive timer event error:" & ex.Message)

        End Try

        m_testCounter = m_testCounter + 1

    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY ROUTINE
    ' ----------  ARPATECH  ----------
    Public Sub OnActionCompleteEvent(ByVal sender As Object, ByVal e As ActionCompleteEventArgs) Implements IGSEventListener.OnActionCompleteEvent

    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY ROUTINE
    ' ----------  ARPATECH  ----------
    Public Sub OnActionRequestEvent(ByVal sender As Object, ByVal e As ActionRequestEventArgs) Implements IGSEventListener.OnActionRequestEvent
        '  EventLog.WriteEntry("Application", "RECEIVED AR EVENT")
    End Sub


    ' ----------  ARPATECH  ----------
    ' routine to be executed on ticket event
    ' ----------  ARPATECH  ----------
    Public Sub OnTicketEvent(ByVal sender As Object, ByVal e As TicketEventArgs) Implements IGSEventListener.OnTicketEvent
        ' EventLog.WriteEntry("Application", m_slaInstanceID & " GOT TICKET EVENT" & m_isRunning)
        ' EventLog.WriteEntry("Application", e.OpenSLA & "=" & m_openTAC.TACID & ", " & e.GSTicketID & "=" & Me.GSTicketID & "stoptimer=" & e.TimerStopOverride & " overrideid=" & e.TimerStartOverrideTACID & ", m_stopOvveride=" & m_stopOverride)

        Try


            '     EventLog.WriteEntry("Application", "me running start is " & m_isRunning)
            '     EventLog.WriteEntry("Application", "me status start is " & m_ticketStatusID & " " & e.MinutesToBreach)
            If e.GSTicketID = m_gsTicketID Then



                ' Did the ticket change status?
                If e.IsStatusChange Then
                    '      EventLog.WriteEntry("Application", "is status change with new ID " & e.NewStatusID)

                    Me.TicketStatusID = e.NewStatusID
                End If

                ' Was the request type changed?
                If e.IsTypeChange Then
                    Dim tmpstatus As Integer = GSSupportManager.GetSingleDBValueInt("select tktstatusid from tblTicket where ticketid=" & Me.GSTicketID)
                    Dim slafullname As String = GSSupportManager.GetSingleDBValueString("select sla.slaName + '.' + tac.tacName from tblSLA sla, tblTermsAndConditions tac, tblSLATracking slat where slat.SLAInstanceID = " & m_slaInstanceID & " and slat.tacid = tac.tacid and tac.slaID = sla.slaid")
                    EventLog.WriteEntry("Application", "Request Type Changed! " & tmpstatus & m_slaInstanceID)
                    m_isRunning = False
                    m_isPaused = False

                    DeleteFromDB()

                    '    If e.NewStatusID > 0 Then
                    '       tmpstatus = e.NewStatusID
                    '    End If
                    AddTicketComment("Ticket request type changed; SLA " & slafullname & " (#" & m_slaInstanceID & ") removed.", tmpstatus, GetLocalTime())
                    Me.RemoveHandlers()

                    Return

                End If

                ' Do we need to update the status target dates?
                If e.Statuses.Count > 0 Then
                    Call UpdateStatuses(e.Statuses, e.StatusTargets, e.StatusActuals, New GSSupportManager().ConvertTime(Now(), ServerTimeZone, GSSupportManager.GetTimeZoneVBConst(m_timeZoneID)))

                End If

                ' If it's SLA information make sure it's the same TAC
                If e.OpenSLA = Me.m_openTAC.TACID Then


                    ' Did someone start/stop the timer?
                    If e.TimerStartOverride Then
                        '            EventLog.WriteEntry("Application", "is timer start change with " & e.TimerStartOverrideTACID)
                        Me.TimerStartOverride()
                        UpdateSLAInstanceDB("SLA Timer Override: SLA Timer Started by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", False)
                    End If

                    If e.TimerStopOverride AndAlso Not m_stopOverride Then
                        '           EventLog.WriteEntry("Application", "is timer stop change with " & e.TimerStopOverrideTACID)
                        Me.TimerStopOverride()
                        UpdateSLAInstanceDB("SLA Timer Override: SLA Timer Stopped by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", False)

                    End If

                    ' Any other info overridden?

                    If e.MinutesToBreach > 0 Then
                        If e.MinutesToBreach <> m_minutesToBreach Then
                            Dim oldMinutesToBreach As Integer = m_minutesToBreach
                            m_minutesToBreach = e.MinutesToBreach
                            UpdateMinutesToBreachDB(m_minutesToBreach)
                            UpdateSLAInstanceDB("SLA Override: Minutes to Breach manually changed from " & oldMinutesToBreach & " to " & m_minutesToBreach & " by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", False)

                        End If
                    End If

                    If e.IsException <> m_isException Then
                        Dim oldException As Boolean = m_isException
                        m_isException = e.IsException
                        If m_isException Then
                            m_isRunning = False
                            m_isPaused = True
                            UpdateSLAInstanceDB("SLA Override: SLA resolution set to Exception by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", True)
                            RemoveHandlers()
                        Else
                            UpdateSLAInstanceDB("SLA Override: SLA Exception status changed from " & oldException & " to " & m_isException & " by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", False)
                        End If
                    End If

                    If e.IsBreach <> m_isBreach Then
                        Dim oldIsBreach As Boolean = m_isBreach
                        m_isBreach = e.IsBreach
                        If m_isBreach Then
                            m_isRunning = False
                            m_isPaused = True
                            UpdateSLAInstanceDB("SLA Override: SLA resolution set to Breach by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", True)
                            RemoveHandlers()
                        Else
                            UpdateSLAInstanceDB("SLA Override: SLA Breach status changed from " & oldIsBreach & " to " & m_isBreach & " by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", False)
                        End If
                    End If
                    If e.IsClosedWithinSLA <> m_closedWithinSLA Then
                        Dim oldClosedWithinSLA As Boolean = m_closedWithinSLA
                        m_closedWithinSLA = e.IsClosedWithinSLA
                        If m_closedWithinSLA Then
                            m_isRunning = False
                            m_isPaused = True
                            UpdateSLAInstanceDB("SLA Override: SLA resolution set to Closed Within SLA by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", True)
                            RemoveHandlers()
                        Else
                            UpdateSLAInstanceDB("SLA Override: SLA Closed Within SLA status changed from " & oldClosedWithinSLA & " to " & m_closedWithinSLA & " by <a href=/support/actions/action.asp?aiid=" & e.ActionInstanceID & ">Action " & e.ActionInstanceID & "</a>", False)
                        End If
                    End If

                End If



            End If

        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error SLAMonitor handling ticket event: " & ex.Message)

        End Try

        '   SaveToQ()
    End Sub


    ' ----------  ARPATECH  ----------
    ' routine for updating 
    ' SLA instance DB
    ' ----------  ARPATECH  ----------
    Public Sub UpdateSLAInstanceDB(ByVal comments As String, ByVal isClosed As Boolean)

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim SLACount As Integer = 0

        Conn.Open()


        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_openSLA"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_openTAC.TACID
            Cmd.Parameters("@ticketid").Value = m_gsTicketID


            If isClosed Then
                Cmd.Parameters("@enddate").Value = Now()
                Cmd.Parameters("@minutesToBreach").Value = 0
                If m_isBreach Then
                    Cmd.Parameters("@isbreach").Value = True
                End If
                If m_isException Then
                    Cmd.Parameters("@isexception").Value = True
                End If
                If m_closedWithinSLA Then
                    Cmd.Parameters("@closedwithinsla").Value = True
                End If
            End If

            Cmd.Parameters("@slainstanceid").Value = m_slaInstanceID
            Cmd.Parameters("@comments").Value = comments

            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error updating sla monitor: " & e.Message)

        Finally

            Conn.Close()

            '    Conn = Nothing
            '   Cmd = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for updating 
    ' SLA instance DB
    ' ----------  ARPATECH  ----------
    Public Sub UpdateSLAInstanceDB(ByVal comments As String, ByVal isClosed As Boolean, ByVal localdate As Date) Implements ISLAMonitor.UpdateSLAInstanceDB

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim SLACount As Integer = 0

        Conn.Open()


        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_openSLA"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_openTAC.TACID
            Cmd.Parameters("@ticketid").Value = m_gsTicketID
            Cmd.Parameters("@localdate").Value = localdate
            ' EventLog.WriteEntry("Application", "localdate is " & localdate)


            If isClosed Then
                Cmd.Parameters("@enddate").Value = localdate
                Cmd.Parameters("@minutesToBreach").Value = 0
                If m_isBreach Then
                    Cmd.Parameters("@isbreach").Value = True
                End If
                If m_isException Then
                    Cmd.Parameters("@isexception").Value = True
                End If
                If m_closedWithinSLA Then
                    Cmd.Parameters("@closedwithinsla").Value = True
                End If
            End If

            Cmd.Parameters("@slainstanceid").Value = m_slaInstanceID
            Cmd.Parameters("@comments").Value = comments

            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error updating sla monitor: " & e.Message)

        Finally

            Conn.Close()

            '    Conn = Nothing
            '   Cmd = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for updating and closing 
    ' SLA instance DB
    ' ----------  ARPATECH  ----------
    Sub CloseSLAInstance(ByVal comments As String)
        UpdateSLAInstanceDB(comments, True)

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that returns exception raising status
    ' ----------  ARPATECH  ----------
    Function IsExceptionStatus() As Boolean
        Dim i As Integer
        Dim arEx = m_openTAC.GetExceptionStatusID

        For i = 0 To m_openTAC.GetExceptionStatusID.Count - 1
            If m_ticketStatusID = arEx(i) Then
                Return True
            End If
        Next
        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' function that confrims that ticking
    ' status of timer
    ' ----------  ARPATECH  ----------
    Function ShouldBeTicking(ByVal localDateTime) As Boolean

        Dim shouldTick As Boolean = False
        '  EventLog.WriteEntry("Application", m_slaInstanceID & " checking ticking and paused is " & m_isPaused & " and running is " & m_isRunning & localDateTime)
        Try


            ' If the ticket is an exception status return false immediately
            If IsExceptionStatus() OrElse m_waitingForStatusChange OrElse IsNothing(m_timerStartDate) OrElse Year(m_timerStartDate) < 2000 Then
                Return shouldTick
            End If

            If m_isRunning Then

                ' If we passed the stop time then stop timer, log stop, set start time
                If localDateTime >= m_nextStopDate Then
                    m_isPaused = True
                    m_isRunning = False
                    m_nextStartDate = GetNextStartDate(localDateTime)

                    Dim m As Integer = CalculateMinutesToBreach(localDateTime)
                    If m <= 0 Then
                        ' No minutes left... must be busted SLA
                        m_isBreach = True
                        UpdateSLAInstanceDB("SLA timer expired at " & localDateTime & ". An SLA breach has occured.", True, localDateTime)
                    Else
                        UpdateSLAInstanceDB("Local time is " & localDateTime.ToString() & "; SLA timer suspended.  Scheduled to restart at " & m_nextStartDate, False, localDateTime)

                    End If


                    shouldTick = False
                Else
                    shouldTick = True
                End If

            ElseIf m_isPaused Then
                ' Dim lastStartDate As Date = GetNextStartDate(New Date(Year(localDateTime), Month(localDateTime), Day(localDateTime), 0, 0, 1))
                Dim lastStartDate As Date = GetLastStartDate(localDateTime)
                If Year(lastStartDate) < MinYear Then
                    EventLog.WriteEntry("Application", "Error getting last start date for " & localDateTime)
                    Return False
                End If

                Dim lastStopDate = GetNextStopDate(lastStartDate)
                ' Dim nextStartDate = GetNextStartDate(localDateTime)
                '       EventLog.WriteEntry("Application", "slaiid=" & m_slaInstanceID & ", localdatetime = " & localDateTime & ", lastStartDate=" & lastStartDate & ", laststopdate=" & lastStopDate)
                ' If the timer is stopped and the current time is after the last start date
                ' but before the last stop date
                If localDateTime >= lastStartDate AndAlso localDateTime < lastStopDate Then
                    m_isPaused = False
                    m_isRunning = True
                    m_nextStopDate = lastStopDate
                    Dim updatestring As String = "Local time is " & localDateTime.ToString() & "; SLA timer started."
                    If (UCase(m_openTAC.TimerTypeName) = "24X7" AndAlso m_openTAC.IncludeHolidays) AndAlso Not m_openTAC.IsBizDays Then
                    Else
                        updatestring = updatestring & " Scheduled to stop at " & m_nextStopDate & "."
                    End If

                    UpdateSLAInstanceDB(updatestring, False)

                    shouldTick = True
                Else
                    shouldTick = False
                End If

            End If
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error checking ShouldBeTIcking: " & e.Message)
        End Try
        Return shouldTick


    End Function

    ' ----------  ARPATECH  ----------
    ' function that gets last start date
    ' from the local supplied date
    ' ----------  ARPATECH  ----------
    Function GetLastStartDate(ByVal localdate As Date) As Date

        ' Make sure there's at least one workshift... otherwise we loop forever.
        Dim d As New Date(1900, 1, 1)
        If m_workShifts.Count < 1 Then
            EventLog.WriteEntry("Application", "Error: no workshifts set for ticket " & Me.GSTicketID)
            Return d
        End If

        Dim myday As Integer = localdate.DayOfWeek + 1
        '   EventLog.WriteEntry("Application", "day is " & myday)

        If Not IsHoliday(localdate) AndAlso m_workShifts.ContainsKey(myday) Then
            Dim al As ArrayList
            al = m_workShifts.Item(myday)

            Dim i As Integer
            Dim maxstart As Date
            For i = al.Count - 1 To 0 Step -1
                Dim ws As Workshift = al(i)
                Dim td As Date = ws.GetStartDateTime(localdate)
                '   EventLog.WriteEntry("Application", "td is " & td & " slaiid=" & m_slaInstanceID)

                ' if the startdate is less than localdate and more than previous start date...
                If td < localdate AndAlso td > d Then
                    Return td
                End If


            Next


        End If
        ' If there are no shifts today or it's a holiday go to previous day
        localdate = DateAdd(DateInterval.Day, -1, localdate)
        localdate = New Date(Year(localdate), Month(localdate), Day(localdate), 23, 59, 59)
        Return GetLastStartDate(localdate)

    End Function


    ' ----------  ARPATECH  ----------
    ' function that returns that last ending
    ' date for todays local date
    ' ----------  ARPATECH  ----------
    Function GetLastEndDateToday(ByVal localdate As Date) As Date
        '   EventLog.WriteEntry("Application", "Getting Last End Date With " & localdate.ToString)

        ' Make sure there's at least one workshift... otherwise we loop forever.
        Dim d As New Date(1900, 1, 1)
        If m_workShifts.Count < 1 Then
            EventLog.WriteEntry("Application", "Error: no workshifts set for ticket " & Me.GSTicketID)
            Return d
        End If

        Dim myday As Integer = localdate.DayOfWeek + 1
        ' EventLog.WriteEntry("Application", "day is " & myday)

        If Not IsHoliday(localdate) AndAlso m_workShifts.ContainsKey(myday) Then
            Dim al As ArrayList
            al = m_workShifts.Item(myday)

            Dim i As Integer
            Dim maxend As Date = localdate
            For i = al.Count - 1 To 0 Step -1
                Dim ws As Workshift = al(i)
                Dim td As Date = ws.GetEndDateTime(localdate)
                '        EventLog.WriteEntry("Application", "Get End Date Time for " & localdate.ToString & " is " & td.ToString)

                If IsNothing(maxend) OrElse Year(maxend) < 1970 Then
                    maxend = td
                Else
                    If (td > maxend) Then
                        maxend = td
                    End If
                End If

                '   EventLog.WriteEntry("Application", "td is " & td & " slaiid=" & m_slaInstanceID)




            Next
            ' EventLog.WriteEntry("Application", "maxend is " & maxend.ToString())

            Return maxend

        Else
            Return GetLastEndDateToday(localdate.AddDays(1))
        End If


    End Function



    ' ----------  ARPATECH  ----------
    ' routine for creating work shifts in DB
    ' ----------  ARPATECH  ----------
    Sub CreateWorkshifts() Implements ISLAMonitor.CreateWorkshifts

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim i As Integer
        Dim sql As New String("")

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Try
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            ' First see if there's a custom workshift for this TAC

            '   EventLog.WriteEntry("Application", "" & IsNothing(m_openTAC))
            i = GSSupportManager.GetSingleDBValueInt("select count(shiftid) from tblTACWorkshifts where isActive=1 and tacid=" & m_openTAC.TACID)



            If i > 0 Then
                sql = "select * from tblTACWorkshifts where isActive=1 and tacid=" & m_openTAC.TACID & " and countryid in (select countryid from tblCustomerAddresses where customeraddressid=" & m_actionRequest.AddressID & ")"
            Else

                ' Next let's see if this address has a custom workshift.
                i = GSSupportManager.GetSingleDBValueInt("select count(shiftid) from tblCustomerAddressWorkshifts where isActive=1 and customeraddressid=" & m_actionRequest.AddressID)


                If i > 0 Then
                    sql = "select * from tblCustomerAddressWorkshifts where isActive = 1 and customeraddressid = " & m_actionRequest.AddressID
                Else
                    sql = "select * from tblCountryDefaultWorkshifts where isActive=1 and countryid in (select countryid from tblCustomerAddresses where customeraddressid=" & m_actionRequest.AddressID & ")"
                End If

            End If


            Cmd.CommandText = sql

            dr = Cmd.ExecuteReader

            While dr.Read()

                ' First check if wf exists and update date

                ' Create a new wf object
                Dim ws As New Workshift()
                ws.StartTime = dr.GetDateTime(dr.GetOrdinal("starttime"))
                ws.EndTime = dr.GetDateTime(dr.GetOrdinal("endtime"))
                Dim day As Integer = dr.GetInt32(dr.GetOrdinal("dayOfWeek"))
                Dim al As New ArrayList()

                If m_workShifts.ContainsKey(day) Then
                    al = m_workShifts.Item(day)

                    Dim xyz As Integer = 0

                    Dim inserted As Boolean = False

                    For xyz = 0 To al.Count - 1

                        Dim tmpws As Workshift = al(xyz)


                        If ws.StartTime < tmpws.StartTime Then


                            Exit For
                        Else
                            If xyz = al.Count - 1 Then
                                xyz = al.Count
                                Exit For
                            End If
                        End If

                    Next

                    al.Insert(xyz, ws)

                    m_workShifts.Remove(day)
                Else
                    al.Add(ws)
                End If
                Dim x As Integer
                '   If m_slaInstanceID = 1291 Then
                '    EventLog.WriteEntry("Application", "For day " & day & " slaiid " & m_slaInstanceID & " shifts are: ")
                '   For x = 0 To al.Count - 1
                '       Dim wsp As Workshift = al(x)
                '       EventLog.WriteEntry("Application", wsp.StartTime & " - " & wsp.EndTime)


                '     Next
                '    End If

                m_workShifts.Add(day, al)

            End While
            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to create workshifts:" & e.Message & ", and the sql=" & Cmd.CommandText)

        Finally
            Conn.Close()
            Conn = Nothing
            '  EventLog.WriteEntry("Application", "number of days for " & m_slaInstanceID & "=" & m_workShifts.Count)


        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for creating holidays in DB
    ' ----------  ARPATECH  ----------
    Sub CreateHolidays() Implements ISLAMonitor.CreateHolidays

        If m_openTAC.IncludeHolidays Then
            Exit Sub
        End If

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim i As Integer
        Dim sql As New String("")


        Conn.Open()

        Dim Cmd As New SqlCommand()

        Try
            Cmd.Connection = Conn
            Dim dr As SqlDataReader


            sql = "select holidaydate from tblHolidays, tblTACHolidays where tblHolidays.isActive=1 " & _
            " and tblHolidays.holidayid = tblTACHolidays.tacid and tblTACHolidays.tacid = " & m_openTAC.TACID


            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader

            While dr.Read()
                Dim d As New Date()
                d = dr.GetDateTime(0)
                m_holidays.Add(d)

            End While
            dr.Close()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to get holidays:" & e.Message & ", and the sql=" & Cmd.CommandText)

        Finally
            Conn.Close()
            Conn = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that returns true
    ' if supplied date is a holiday
    ' ----------  ARPATECH  ----------
    Function IsHoliday(ByVal localdate As Date) As Boolean
        Dim i As Integer
        For i = 0 To m_holidays.Count - 1
            Dim d As Date = m_holidays(i)
            If d.Day = localdate.Day AndAlso d.Month = localdate.Month Then
                Return True
            End If
        Next
        Return False
    End Function

    ' Find the next localdatetime when SLA ticker should resume
    Function GetNextStartDate(ByVal localdate As Date) As Date

        Dim d As New Date()
        Dim n As Integer
        d = localdate
        Dim i As Integer
        i = d.DayOfWeek + 1
        Dim arSD As New ArrayList()

        ' Find next shift start greater than current datetime
        If m_workShifts.ContainsKey(i) Then
            Dim arWS As New ArrayList()
            ' Get all the workshifts for this day
            arWS = m_workShifts(i)


            For n = 0 To arWS.Count - 1
                Dim ws As Workshift = arWS(n)
                If localdate < ws.GetStartDateTime(localdate) Then
                    ' Add to array of startdate if it's after localdate
                    arSD.Add(ws.GetStartDateTime(localdate))
                End If
            Next


        End If


        If arSD.Count > 0 AndAlso Not IsHoliday(d) Then
            If arSD.Count = 1 Then
                ' Only one start date return it
                Dim dd As Date = arSD(0)
                Return dd
            Else
                ' Otherwise find the smallest
                Dim smallestDate As Date = arSD(0)
                For n = 1 To arSD.Count - 1
                    Dim dd As Date = arSD(n)
                    If dd < smallestDate Then
                        smallestDate = arSD(n)
                    End If
                Next
                Return smallestDate
            End If


        Else
            ' If today's a holiday or there aren't any shifts starting after localdate
            ' try 12:00:01 AM tomorrow

            Dim nd As New Date()
            nd = DateAdd(DateInterval.Day, 1, d)
            ' Change time to 12:00:01 AM
            nd = New Date(nd.Year, nd.Month, nd.Day, 0, 0, 1)
            Return GetNextStartDate(nd)

        End If



    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Public Sub OnOrderEvent(ByVal sender As Object, ByVal e As OrderEventArgs) Implements IGSEventListener.OnOrderEvent

    End Sub

    ' Find the end of the current workshift
    Function GetNextStopDate(ByVal localdate As Date) As Date

        Dim d As New Date()
        Dim n As Integer
        d = localdate
        Dim i As Integer
        i = d.DayOfWeek + 1
        Dim arED As New ArrayList()

        ' Find next shift start greater than current datetime
        If m_workShifts.ContainsKey(i) Then
            Dim arWS As New ArrayList()
            ' Get all the workshifts for this day
            arWS = m_workShifts(i)


            For n = 0 To arWS.Count - 1
                Dim ws As Workshift = arWS(n)
                '        EventLog.WriteEntry("Application", "Checking against workshift  " & ws.GetStartDateTime(localdate) & " - " & ws.GetEndDateTime(localdate))
                ' If localdate > ws.GetStartDateTime(localdate) Then
                ' Add to array of startdate if it's after localdate
                If ws.GetEndDateTime(localdate) > localdate Then
                    arED.Add(ws.GetEndDateTime(localdate))
                    Exit For
                    'End If
                End If
            Next


        End If

        ' Get Smallest end date


        If arED.Count > 0 AndAlso Not IsHoliday(d) Then


            If arED.Count = 1 Then
                ' Only one end date return it
                Dim dd As Date = arED(0)
                Return dd
            Else
                ' Otherwise find the smallest
                Dim smallestDate As Date = arED(0)
                For n = 1 To arED.Count - 1
                    Dim dd As Date = arED(n)
                    If dd < smallestDate Then
                        smallestDate = arED(n)
                    End If
                Next
                Return smallestDate
            End If


        Else
            ' If today's a holiday or there aren't any shifts starting after localdate
            ' try 12:00:01 AM tomorrow

            Dim nd As New Date()
            nd = DateAdd(DateInterval.Day, 1, d)
            ' Change time to 12:00:01 AM
            nd = New Date(nd.Year, nd.Month, nd.Day, 0, 0, 1)
            Return GetNextStopDate(nd)

        End If

    End Function



    ' ----------  ARPATECH  ----------
    ' routine for updating minutes to breach
    ' of SLA to database
    ' ----------  ARPATECH  ----------
    Sub UpdateMinutesToBreachDB(ByVal min As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim SLACount As Integer = 0

        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()
        Try

            Cmd.Connection = Conn

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_updateSLATimer"
            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@minutes").Value = min
            Cmd.Parameters("@slainstanceid").Value = m_slaInstanceID

            Cmd.ExecuteNonQuery()


        Catch ex As Exception
            EventLog.WriteEntry("GS Schedule Service", "Error updating sla timer db:" & ex.Message)

        Finally
            Conn.Close()
            '   Cmd = Nothing
            '   Conn = Nothing
        End Try

    End Sub

    ' Save to SLAInProgress Q after state change
    Sub SaveToQ()
        Dim mqq As New System.Messaging.MessageQueue(GSSupportManager.GetQHost() & SLAInProgressQ)
        Dim m As New System.Messaging.Message()
        Dim menum As System.Messaging.MessageEnumerator
        menum = mqq.GetMessageEnumerator()
        While menum.MoveNext()
            If menum.Current.Label = m_mqLabel Then
                m = menum.Current
                menum.RemoveCurrent()

                SendToQ(SLAInProgressQ, CreateMQMessage)

                Exit While

            End If
        End While

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for removine handlers
    ' from gs listner
    ' ----------  ARPATECH  ----------
    Public Sub RemoveHandlers() Implements ISLAMonitor.RemoveHandlers

        RemoveHandler m_gsListener.TicketEvent, m_teh
        RemoveHandler m_gsListener.GSTimerEvent, m_gteh
        RemoveHandler m_gsListener.ActionCompleteEvent, m_aceh
        m_gsListener = Nothing
        m_gsSupportManager = Nothing

        GSSupportManager.RemoveFromQ(SLAInProgressQ, m_mqLabel)

        Me.Finalize()

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that returns SLA monitor 
    ' start status of TAC 
    ' ----------  ARPATECH  ----------
    Function IsStatusStart(ByVal statusid As Integer) As Boolean
        Dim arStatus As New ArrayList()
        Dim i As Integer

        arStatus = m_openTAC.GetStartStatusID
        For i = 0 To arStatus.Count - 1
            If statusid = arStatus(i) Then
                Return True
            End If
        Next
        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns SLA monitor 
    ' Exception status of TAC 
    ' ----------  ARPATECH  ----------
    Function IsStatusException(ByVal statusid As Integer) As Boolean
        Dim arStatus As New ArrayList()
        Dim i As Integer

        arStatus = m_openTAC.GetExceptionStatusID
        For i = 0 To arStatus.Count - 1
            If statusid = arStatus(i) Then
                Return True
            End If
        Next
        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns SLA monitor 
    ' end status of TAC 
    ' ----------  ARPATECH  ----------
    Function IsStatusEnd(ByVal statusid As Integer) As Boolean
        Dim arStatus As New ArrayList()
        Dim i As Integer

        arStatus = m_openTAC.GetEndStatusID
        For i = 0 To arStatus.Count - 1

            If statusid = arStatus(i) Then
                Return True
            End If
        Next
        Return False
    End Function

    ' ----------  ARPATECH  ----------
    ' routine for toggle timer at certain time
    ' ----------  ARPATECH  ----------
    Sub ToggleExceptionTimer(ByVal stopTime As Date, ByVal minutes As Int32)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim SLACount As Integer = 0

        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_AddSLAExceptionMinutes"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@slaiid").Value = m_slaInstanceID

            If Not IsNothing(stopTime) AndAlso DateDiff(DateInterval.Year, stopTime, Now()) < 5 Then
                Cmd.Parameters("@stoptime").Value = stopTime

            End If

            If Not IsNothing(minutes) AndAlso minutes > 0 Then
                Cmd.Parameters("@minutes").Value = minutes
            End If

            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error updating sla exception minutes: " & e.Message)

        Finally

            Conn.Close()

            Conn = Nothing
            Cmd = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that stops timer for SLA
    ' ----------  ARPATECH  ----------
    Public Function TimerStopOverride() As Boolean
        If m_isRunning Then
            m_exceptionStopTime = Now()
            ToggleExceptionTimer(m_exceptionStopTime, 0)
        End If

        m_stopOverride = True
        m_isPaused = True
        m_isRunning = False


    End Function

    ' ----------  ARPATECH  ----------
    ' function that starts timer for SLA
    ' ----------  ARPATECH  ----------
    Public Function TimerStartOverride() As Boolean
        Dim b As Boolean

        ' If I'm running just return
        If m_isRunning Then
            Return b
        End If

        Try



            Dim m As Integer = DateDiff(DateInterval.Minute, m_exceptionStopTime, Now())

            m_exceptionMinutes = m_exceptionMinutes + m
            ToggleExceptionTimer(Nothing, m)
            m_exceptionStopTime = Nothing
            m_stopOverride = False
            m_isPaused = False
            m_isRunning = True
        Catch e As Exception
            b = False
            EventLog.WriteEntry("Application", "Error executing SLA Timer start override: " & e.Message)
        End Try

        Return b

    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY routine
    ' ----------  ARPATECH  ----------
    Private Sub UpdateStatusesFromDB()

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for updating statuses
    ' from database
    ' ----------  ARPATECH  ----------
    Private Sub UpdateStatuses(ByVal statuses As ArrayList, ByVal targets As ArrayList, ByVal actuals As ArrayList, ByVal localdate As DateTime)
        '  EventLog.WriteEntry("Application", "In Updatestatuses with localdate " & localdate)


        If UCase(m_openTAC.StartTypeName) = "TARGET DATE" AndAlso Not m_isBreach AndAlso Not m_isException AndAlso Not m_closedWithinSLA Then
            Dim i
            '   EventLog.WriteEntry("Application", "In checking status...")

            For i = 0 To statuses.Count - 1
                Dim thisstatus As Integer = CInt(statuses(i))
                '     EventLog.WriteEntry("Application", "found status is " & thisstatus)
                Dim als As ArrayList = m_openTAC.GetEndStatusID
                If als.Count > 0 Then
                    Dim thisint = CInt(als(0))
                    If thisint = thisstatus Then

                        ' There'a a status.. check the values
                        '    EventLog.WriteEntry("Application", "status is " & thisint)

                        Dim target As DateTime = targets(i)
                        Dim actual As DateTime = actuals(i)
                        '    EventLog.WriteEntry("Application", "target is " & target)
                        '   EventLog.WriteEntry("Application", "actual is " & actual)
                        If IsDate(target) And Year(target) > 2000 Then
                            ' We have a target...
                            '  EventLog.WriteEntry("Application", "Setting nextStopDate to " & target)
                            m_nextStopDate = target
                            'm_timerStartDate = localdate
                            If Not m_isRunning Then
                                UpdateSLAInstanceDB("Status target date set to " & target.ToShortDateString & " " & target.ToShortTimeString & "; SLA timer started.", False)
                            End If

                            If IsDate(actual) And Year(actual) > 2000 Then
                                ' if there'a an actual too we're not running
                                m_isRunning = False
                                If actual > target Then
                                    m_isBreach = True
                                    UpdateSLAInstanceDB("Status changed after target date; an SLA breach has occurred.", True)

                                Else
                                    m_closedWithinSLA = True
                                    UpdateSLAInstanceDB("Status changed at or before target date; closed within SLA.", True)
                                End If
                                Exit Sub
                            Else
                                ' No actual date yet...
                                ' If the target date is in the past then it's a breach
                                If target < localdate Then
                                    m_isBreach = True
                                    m_isRunning = False
                                    Exit Sub

                                Else
                                    m_isRunning = True
                                    '   EventLog.WriteEntry("Application", "Minutes to breach is " & DateDiff(DateInterval.Minute, localdate, target))
                                    m_minutesToBreach = DateDiff(DateInterval.Minute, localdate, target)
                                    Me.UpdateMinutesToBreachDB(m_minutesToBreach)

                                End If


                            End If
                        Else
                            ' If there's no target and we were running, stop
                            m_isRunning = False
                        End If

                    End If
                End If

            Next
        End If
    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for adding ticket comments
    ' to database
    ' ----------  ARPATECH  ----------
    Sub AddTicketComment(ByVal comment As String, ByVal statusid As Integer, ByVal logdate As Date)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Try
            Dim actionid As Integer
            Dim userid As Integer
            userid = GSSupportManager.GetSingleDBValueInt("select min(userid) from tblUsers where username='System'")

            actionid = GSSupportManager.GetSingleDBValueInt("select actionid from tblActions where actionname='Add a Comment to a Ticket'")

            Dim sql As String = "insert into tblTicketTracking (ticketid, actionid, comment, statusid, logdate, isActive, lastUpdateUserID) " & _
            " values (" & m_gsTicketID & ", " & actionid & ", '" & comment & "'," & statusid & ",'" & logdate & "',1," & userid & ")"

            Cmd.CommandText = sql
            Cmd.ExecuteNonQuery()
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error adding ticket comments from SLAMonitor: " & e.Message & " and status is " & statusid & " and comment is " & comment)
        Finally

            Conn.Close()

            Conn = Nothing
            Cmd = Nothing

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' delete SLA tracking from database
    ' ----------  ARPATECH  ----------
    Sub DeleteFromDB()
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Try

            Dim sql As String = "delete from tblSLATracking where slainstanceid=" & m_slaInstanceID
            EventLog.WriteEntry("Application", sql)
            Cmd.CommandText = sql
            Cmd.ExecuteNonQuery()
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error deleting SLATimer from database: " & e.Message)
        Finally

            Conn.Close()

            Conn = Nothing
            Cmd = Nothing

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' function that checks is provided
    ' date a valid business
    ' ----------  ARPATECH  ----------
    Function IsBusinessDay(ByVal localdate As Date) As Boolean
        If IsHoliday(localdate) Then
            Return False
        End If
        Dim i As Integer = i = localdate.DayOfWeek + 1
        If Not m_workShifts.ContainsKey(i) Then
            Return False
        Else
            Return True
        End If

    End Function




End Class

' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is used for every ActionRequest user send to server for
'                       processing. It contains the complete code from serialization till updating
'                       of that particular record.
' Inherits          :   Object
' Implements        :   IActionRequest and IMQUtils
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System
Imports System.Data
Imports System.Reflection
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports GSSupport.GSAppGlobals
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text



' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IActionRequest
    Sub Test()
    Function Submit() As Boolean
    Property SubmitUserName() As String
    Property SubmitUserID() As Integer
    Property AffectedContactID() As Integer
    Property GSTicketID() As Integer
    Property AddressName() As String
    Property GSAssetID() As Integer
    Property SubmitDate() As Date
    Property RequestDate() As Date
    Property WorkflowInstance() As WorkflowMonitor
    Property ScheduleID() As Integer
    Property ActionRequestID() As Integer
    Property Summary() As String
    Property Description() As String
    Property GSComments() As String
    Property TypeID() As Integer
    Property PriorityID() As Integer
    Property CompanyID() As Integer
    Property AddressID() As Integer
    Property SerialNum() As String
    Property MQLabel() As String
    Property AssetTag() As String
    Property RequestedActionID() As Integer
    Property MQAppCodeID() As Integer
    Function IsValid() As Boolean
    Function GetWorkflowInstanceID() As Integer
    Property Qualify() As Boolean
    Property GenerateTicket() As Boolean
    Property Fields() As SortedList
    Property CustomerID() As Integer
    Function ARUpdateDB() As Boolean
    Property CategoryID() As Integer
    Property CategoryName() As String
    Property IsChild() As Boolean
    Property RequestDateLocal() As Date
    Property RequestDateTimeZoneID() As Integer
    Property RequestDateTimeZoneVBConst() As Integer
    Property RequestDateLocalGMTOffset() As Integer
    Property RequestDateLocalGMTOffsetString() As String
    Property TicketCreateLocal() As Date
    Property FieldNames() As ArrayList
    Property FieldValues() As ArrayList
    Property FieldDisplays() As ArrayList
    Property FieldIDs() As ArrayList
    Property CustomerRequestID() As String
    Property SupplierRequestID() As String
    Property TicketContactName() As String
    Property TicketContactPhone() As String


    Property Manager() As GSSupportManager
    Property CompletedAction() As ActionMonitor
    Property IsCompletedAction() As Boolean
    Sub AddFieldWithDisplay(ByVal fieldName As String, ByVal fieldTextValue As String, ByVal fieldDisplayValue As String)
    Sub AddFieldByDisplay(ByVal fieldName As String, ByVal fieldTextValue As String, Optional ByVal fieldDisplayValue As String = "")








End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionRequest
    Inherits Object
    Implements IActionRequest, IMQUtils

    Private m_submitUserName As String
    Private m_summary As New String("")
    Private m_description As New String("")
    Private m_typeID As Integer = 0
    Private m_priorityID As Integer = 0
    Private m_companyID As Integer = 0
    Private m_addressID As Integer = 0
    Private m_submitUserID As Integer = 0
    Private m_serialNum As New String("")
    Private m_assetTag As String
    Private m_affectedContactID As Integer = 0
    Private m_GSTicketID As Integer = 0
    Private m_GSAssetID As Integer = 0
    Private m_submitDate As Date
    Private m_requestDate As Date
    Private m_requestDateTimeZoneID As Integer
    Private m_requestDateTimeZoneVBConst As Integer
    Private m_workflowInstance As WorkflowMonitor
    Private m_scheduleID As Integer
    Private m_actionRequestID As Integer = 0
    Private m_generateTicket As Boolean = False
    Private m_requestedActionID As Integer
    Private m_mqAppCodeID As Integer = 0
    Private m_isValid As Boolean = True
    Private m_qualify As Boolean = False
    Private m_mqLabel As New String("")
    Private m_fields As New SortedList()

    Private m_GSComments As String
    Private m_customerID As Integer = 0
    Private m_requestDateLocal As Date
    Private m_localTimeZoneID As Integer
    Private m_gsSupportManager As GSSupportManager
    Private m_completedAction As New ActionMonitor()
    Private m_gsListener As GSEventListener
    Private m_requestDateLocalGMTOffset As Integer
    Private m_requestDateLocalGMTOffsetString As String
    Private m_categoryName As String
    Private m_categoryID As String
    Private m_isChild As Boolean = False
    Private m_ticketCreateLocal As Date
    Private m_fieldNames As New ArrayList()
    Private m_fieldValues As New ArrayList()
    Private m_fieldDisplays As New ArrayList()
    Private m_fieldIDs As New ArrayList()
    Private m_isCompletedAction As Boolean = False
    Private m_supplierRequestID As String
    Private m_customerRequestID As String
    Private m_addressName As New String("")
    Private m_ticketContactName As New String("")
    Private m_ticketContactPhone As New String("")


    ' Private m_tzConverter As GSDates.GSTimeZoneClass

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for field name array
    ' ----------  ARPATECH  ----------
    Public Property FieldIDs() As ArrayList Implements IActionRequest.FieldIDs
        Get
            Return m_fieldIDs
        End Get
        Set(ByVal Value As ArrayList)
            m_fieldIDs = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for action instance value
    ' ----------  ARPATECH  ----------
    Public Property FieldNames() As ArrayList Implements IActionRequest.FieldNames
        Get
            Return m_fieldNames
        End Get
        Set(ByVal Value As ArrayList)
            m_fieldNames = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for variable determining action completion status
    ' ----------  ARPATECH  ----------
    Public Property IsCompletedAction() As Boolean Implements IActionRequest.IsCompletedAction
        Get
            Return m_isCompletedAction
        End Get
        Set(ByVal Value As Boolean)
            m_isCompletedAction = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for field display name array
    ' ----------  ARPATECH  ----------
    Public Property FieldDisplays() As ArrayList Implements IActionRequest.FieldDisplays
        Get
            Return m_fieldDisplays
        End Get
        Set(ByVal Value As ArrayList)
            m_fieldDisplays = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for field value array
    ' ----------  ARPATECH  ----------
    Public Property FieldValues() As ArrayList Implements IActionRequest.FieldValues
        Get
            Return m_fieldValues
        End Get
        Set(ByVal Value As ArrayList)
            m_fieldValues = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' submitted user name
    ' ----------  ARPATECH  ----------
    Public Property SubmitUserName() As String Implements IActionRequest.SubmitUserName
        Get
            Return m_submitUserName
        End Get
        Set(ByVal Value As String)
            m_submitUserName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine that logs all fields to
    ' default event logging file
    ' ----------  ARPATECH  ----------
    Public Sub LogFieldsToEventLog()
        Dim i As Integer = 0
        Dim s As String = ""
        Dim sl As SortedList = m_fields
        For i = 0 To sl.Count - 1
            Dim f As Field = sl.GetByIndex(i)
            If Not IsNothing(f) Then
                If Not IsNothing(f.FieldID) Then
                    s = s & f.FieldID & "="
                    If Not IsNothing(f.FieldTextValue) Then
                        s = s & f.FieldTextValue & vbCrLf
                    End If

                End If
            End If

        Next
        s = s & "END FIELDS"
        If Not IsNothing(Me.ActionRequestID) Then
            s = "AR " & Me.ActionRequestID & ":" & vbCrLf & s

        End If

        EventLog.WriteEntry("Application", s)

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' variable that determines class type (is child or not)
    ' ----------  ARPATECH  ----------
    Public Property IsChild() As Boolean Implements IActionRequest.IsChild
        Get
            Return m_isChild
        End Get
        Set(ByVal Value As Boolean)
            m_isChild = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' variable determining action complete/incomplete status
    ' ----------  ARPATECH  ----------
    ' <System.Xml.Serialization.XmlIgnore()> _
    <XmlElement(IsNullable:=False)> _
    Public Property CompletedAction() As ActionMonitor Implements IActionRequest.CompletedAction
        Get

            Return m_completedAction


        End Get
        Set(ByVal Value As ActionMonitor)

            m_completedAction = Value
            m_isCompletedAction = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' varible of GSListener type
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
      Public Property GSListener() As GSEventListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            m_gsListener = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' MQlabel variable
    ' ----------  ARPATECH  ----------
    Public Property MQLabel() As String Implements IActionRequest.MQLabel
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' category name but it retrieves from DB for value assigning
    ' ----------  ARPATECH  ----------
    Public Property CategoryName() As String Implements IActionRequest.CategoryName
        Get
            Return m_categoryName
        End Get
        Set(ByVal Value As String)
            m_categoryName = Value
            If IsNothing(m_categoryID) OrElse m_categoryID = 0 Then


                Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
                Conn.Open()

                Dim Cmd As New SqlCommand()

                Cmd.Connection = Conn
                Try

                    Cmd.CommandText = "select arcatid from tblarcategories where upper(arcatname)='" & UCase(m_categoryName) & "'"
                    Dim dr As SqlDataReader


                    dr = Cmd.ExecuteReader
                    While dr.Read()
                        m_categoryID = dr.GetInt32(dr.GetOrdinal("arcatid"))
                    End While

                    dr.Close()

                Catch e As Exception
                    EventLog.WriteEntry("Application", "Error getting arcatid: " & e.Message)

                Finally
                    Conn.Close()

                End Try
            End If

            Dim dname As New String(New Qualification().GetARCategoryDisplayName(m_categoryID))
            AddFieldByDisplay("Event", CStr(m_categoryID), dname)

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' category ID but it retrieves from DB for value assigning
    ' ----------  ARPATECH  ----------
    Public Property CategoryID() As Integer Implements IActionRequest.CategoryID
        Get
            Return m_categoryID
        End Get
        Set(ByVal Value As Integer)
            m_categoryID = Value
            If IsNothing(m_categoryName) OrElse Len(m_categoryName) < 1 Then

                Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
                Conn.Open()

                Dim Cmd As New SqlCommand()

                Cmd.Connection = Conn
                Try

                    Cmd.CommandText = "select arcatname from tblarcategories where arcatid=" & m_categoryID
                    Dim dr As SqlDataReader


                    dr = Cmd.ExecuteReader
                    While dr.Read()
                        m_categoryName = dr.GetString(dr.GetOrdinal("arcatname"))
                    End While

                    dr.Close()

                Catch e As Exception
                    EventLog.WriteEntry("Application", "Error getting arcatname for arcatid " & m_categoryID & ": " & e.Message)

                Finally
                    Conn.Close()

                End Try
            End If

            AddFieldByDisplay("Event", CStr(m_categoryID), New Qualification().GetARCategoryDisplayName(m_categoryID))

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' local date value
    ' ----------  ARPATECH  ----------
    Public Property RequestDateLocal() As Date Implements IActionRequest.RequestDateLocal
        Get
            Return m_requestDateLocal
        End Get
        Set(ByVal Value As Date)
            m_requestDateLocal = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' fields as action request field type
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
  Public Property Fields() As SortedList Implements IActionRequest.Fields
        Get
            Return m_fields
        End Get
        Set(ByVal Value As SortedList)
            m_fields = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' comments variable 
    ' ----------  ARPATECH  ----------
    Public Property GSComments() As String Implements IActionRequest.GSComments
        Get
            Return m_GSComments
        End Get
        Set(ByVal Value As String)
            m_GSComments = Value
            AddFieldByDisplay("GS Comments", m_GSComments)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' varible that determines either a ticket is generated or not
    ' ----------  ARPATECH  ----------
    Public Property GenerateTicket() As Boolean Implements IActionRequest.GenerateTicket
        Get
            Return m_generateTicket
        End Get
        Set(ByVal Value As Boolean)
            m_generateTicket = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' varible that determines either ticket qualify or not
    ' ----------  ARPATECH  ----------
    Public Property Qualify() As Boolean Implements IActionRequest.Qualify
        Get
            Return m_qualify
        End Get
        Set(ByVal Value As Boolean)
            m_qualify = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' action request ID
    ' ----------  ARPATECH  ----------
    Public Property ActionRequestID() As Integer Implements IActionRequest.ActionRequestID
        Get
            Return m_actionRequestID
        End Get
        Set(ByVal Value As Integer)
            m_actionRequestID = Value
            AddFieldByDisplay("Request ID", CStr(Value))
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request date's TZ id
    ' ----------  ARPATECH  ----------
    Public Property RequestDateTimeZoneID() As Integer Implements IActionRequest.RequestDateTimeZoneID
        Get
            Return m_requestDateTimeZoneID
        End Get
        Set(ByVal Value As Integer)
            m_requestDateTimeZoneID = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' TZ constant previously defined publically
    ' ----------  ARPATECH  ----------
    Public Property RequestDateTimeZoneVBConst() As Integer Implements IActionRequest.RequestDateTimeZoneVBConst
        Get
            Return m_requestDateTimeZoneVBConst
        End Get
        Set(ByVal Value As Integer)
            m_requestDateTimeZoneVBConst = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' customer ID
    ' ----------  ARPATECH  ----------
    Public Property CustomerID() As Integer Implements IActionRequest.CustomerID
        Get
            Return m_customerID
        End Get
        Set(ByVal Value As Integer)
            m_customerID = Value
            AddFieldByDisplay("Customer", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Manager and if its nothing then Lister is assiged to that
    ' ----------  ARPATECH  ----------
    Public Property Manager() As GSSupportManager Implements IActionRequest.Manager
        Get
            Return m_gsSupportManager
        End Get
        Set(ByVal Value As GSSupportManager)
            m_gsSupportManager = Value
            If Not IsNothing(Value.GSListener) And IsNothing(m_gsListener) Then
                m_gsListener = Value.GSListener
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' application code ID
    ' ----------  ARPATECH  ----------
    Public Property MQAppCodeID() As Integer Implements IActionRequest.MQAppCodeID
        Get
            Return m_mqAppCodeID
        End Get
        Set(ByVal Value As Integer)
            m_mqAppCodeID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' reuested action ID
    ' ----------  ARPATECH  ----------
    Public Property RequestedActionID() As Integer Implements IActionRequest.RequestedActionID
        Get
            Return m_requestedActionID
        End Get
        Set(ByVal Value As Integer)
            m_requestedActionID = Value
            AddFieldByDisplay("Action", CStr(Value), GSSupportManager.GetSingleDBValueString("select actionName from tblActions where actionid=" & m_requestedActionID))
            '  EventLog.WriteEntry("Application", "adding field " & GSSupportManager.GetSingleDBValueString("select actionName from tblActions where actionid=" & m_requestedActionID))

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' varible that determines either a submit is valid or not
    ' ----------  ARPATECH  ----------
    Function IsValid() As Boolean Implements IActionRequest.IsValid
        Dim errMsg
        errMsg = ""
        IsValid = True
        '      If Not m_affectedContactID > 0 Then
        '         errMsg = errMsg & "No affected contact ID" & vbCrLf
        '    End If
        If Not m_submitUserID > 0 Then
            errMsg = errMsg & "No submit by user ID" & vbCrLf
        End If

        If Len(errMsg) > 0 Then
            IsValid = False
            Err.Raise(vbObjectError + 1000, "ActionRequest Class", "Action Request Failed Validation")
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request summary
    ' ----------  ARPATECH  ----------
    Public Property Summary() As String Implements IActionRequest.Summary
        Get
            Return m_summary
        End Get
        Set(ByVal Value As String)
            m_summary = Value
            AddFieldByDisplay("Request Summary", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' reuest description
    ' ----------  ARPATECH  ----------
    Public Property Description() As String Implements IActionRequest.Description
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
            AddFieldByDisplay("Request Description", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request address name
    ' ----------  ARPATECH  ----------
    Public Property AddressName() As String Implements IActionRequest.AddressName
        Get
            Return m_addressName
        End Get
        Set(ByVal Value As String)
            m_addressName = Value
            AddFieldByDisplay("Address Name", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request ID of customer
    ' ----------  ARPATECH  ----------
    Public Property CustomerRequestID() As String Implements IActionRequest.CustomerRequestID
        Get
            Return m_customerRequestID
        End Get
        Set(ByVal Value As String)
            m_customerRequestID = Value
            AddFieldByDisplay("Customer Ticket ID", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request ID of supplier
    ' ----------  ARPATECH  ----------
    Public Property SupplierRequestID() As String Implements IActionRequest.SupplierRequestID
        Get
            Return m_supplierRequestID
        End Get
        Set(ByVal Value As String)
            m_supplierRequestID = Value
            AddFieldByDisplay("GSP Ticket ID", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket contact phone number
    ' ----------  ARPATECH  ----------
    Public Property TicketContactPhone() As String Implements IActionRequest.TicketContactPhone
        Get
            Return m_ticketContactPhone
        End Get
        Set(ByVal Value As String)
            m_ticketContactPhone = Value
            AddFieldByDisplay("Ticket Contact Phone", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket contact name
    ' ----------  ARPATECH  ----------
    Public Property TicketContactName() As String Implements IActionRequest.TicketContactName
        Get
            Return m_ticketContactName
        End Get
        Set(ByVal Value As String)
            m_ticketContactName = Value
            AddFieldByDisplay("Ticket Contact Name", Value)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' address id of affected user by request
    ' ----------  ARPATECH  ----------
    Public Property AddressID() As Integer Implements IActionRequest.AddressID
        Get
            Return m_addressID
        End Get
        Set(ByVal Value As Integer)
            m_addressID = Value
            ' AddField("addressid", CStr(Value))
            AddFieldByDisplay("Address of Affected User", CStr(Value))


            m_localTimeZoneID = GSSupportManager.GetTimeZoneFromCustomerAddress(m_addressID)


            ' lets assume for now all addresses are customer addresses
            If m_companyID > 0 Then
                Dim countryid As Integer = GetCustomerAddressCountry(m_addressID)
                Dim countryname As String = GSSupportManager.GetSingleDBValueString("select countryname from tblcountries where countryid=" & countryid)

                AddFieldByDisplay("Country Of Affected User", CStr(countryid), countryname)
                AddFieldByDisplay("Country Of Affected Asset", CStr(countryid), countryname)
            End If

            Dim coverageid
            ' see if there's special coverage for this address

            coverageid = GSSupportManager.GetSingleDBValueInt("select isnull(coverageid, 0) from tblCustomerAddresses where customeraddressid=" & m_addressID)


            If Not IsNothing(coverageid) And Len(coverageid) > 0 Then
                AddFieldByDisplay("Coverage Type", coverageid)
            Else
                AddFieldByDisplay("Coverage Type", "0")
            End If

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for assigning addressid to memory variable
    ' ----------  ARPATECH  ----------
    Sub SetAddressID(ByVal addressid As Integer, ByVal displayText As String)
        m_addressID = addressid
        '  AddField("addressid", CStr(addressid), displayText)
        AddFieldByDisplay("Address of Affected User", displayText)
        If Len(m_addressName) < 1 Then
            Me.AddressName = displayText
        End If
        If m_companyID > 0 Then
            Dim countryid As Integer = GetCustomerAddressCountry(m_addressID)
            Dim countryname As String = GSSupportManager.GetSingleDBValueString("select countryname from tblcountries where countryid=" & countryid)

            AddFieldByDisplay("Country Of Affected User", CStr(countryid), countryname)
            AddFieldByDisplay("Country Of Affected Asset", CStr(countryid), countryname)
        End If
        m_localTimeZoneID = GSSupportManager.GetTimeZoneFromCustomerAddress(m_addressID)

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request submit date
    ' ----------  ARPATECH  ----------
    Public Property SubmitDate() As Date Implements IActionRequest.SubmitDate
        Get
            Return m_submitDate
        End Get
        Set(ByVal Value As Date)
            m_submitDate = Value
            AddFieldByDisplay("Submit Date", Value.ToString())

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' date of ticket creation
    ' ----------  ARPATECH  ----------
    Public Property TicketCreateLocal() As Date Implements IActionRequest.TicketCreateLocal
        Get
            Return m_ticketCreateLocal
        End Get
        Set(ByVal Value As Date)
            m_ticketCreateLocal = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' varible that determines ticket priority
    ' ----------  ARPATECH  ----------
    Public Property PriorityID() As Integer Implements IActionRequest.PriorityID
        Get
            Return m_priorityID
        End Get
        Set(ByVal Value As Integer)
            m_priorityID = Value
            AddFieldByDisplay("Ticket Priority", CStr(Value))
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for assigning priorityid
    ' ----------  ARPATECH  ----------
    Sub SetPriorityID(ByVal p As Integer, ByVal displayText As String)
        m_priorityID = p
        ' AddField("tktpriorityid", CStr(p), displayText)
        AddFieldByDisplay("Ticket Priority", CStr(p), displayText)
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' type ID of ticket 
    ' ----------  ARPATECH  ----------
    Public Property TypeID() As Integer Implements IActionRequest.TypeID
        Get
            Return m_typeID
        End Get
        Set(ByVal Value As Integer)
            m_typeID = Value
            AddFieldByDisplay("Request Type", CStr(Value))
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for assisgning
    ' type Id to memory variable
    ' ----------  ARPATECH  ----------
    Sub SetTypeID(ByVal artype As Integer, ByVal displayText As String)
        m_typeID = artype
        ' AddField("requesttype", CStr(artype), displayText)
        AddFieldByDisplay("Request Type", CStr(artype), displayText)
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' varible that determines either a ticket is generated or not
    ' ----------  ARPATECH  ----------
    Public Property SubmitUserID() As Integer Implements IActionRequest.SubmitUserID
        Get
            Return m_submitUserID
        End Get
        Set(ByVal Value As Integer)
            m_submitUserID = Value

            AddFieldByDisplay("Submitting User", CStr(Value))

            ' Add contact email field
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Conn.Open()
            Dim sql As String
            Dim myEmail As New String("")
            sql = "Select IsNull(email, '" & DefaultEmail & "') as email from tblContact where userid =" & m_submitUserID

            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader

            Try

                While dr.Read()

                    myEmail = dr.GetString(dr.GetOrdinal("email"))

                End While

                AddFieldByDisplay("Email of Submitting User", myEmail, myEmail)


            Catch e As Exception

                EventLog.WriteEntry("Application", "Error getting email address: " & e.Message)

            Finally
                Conn.Close()
            End Try
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket request date as string
    ' ----------  ARPATECH  ----------
    Public Property RequestDateLocalGMTOffsetString() As String Implements IActionRequest.RequestDateLocalGMTOffsetString
        Get
            Return m_requestDateLocalGMTOffsetString
        End Get
        Set(ByVal Value As String)
            m_requestDateLocalGMTOffsetString = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket request date as ineger
    ' ----------  ARPATECH  ----------
    Public Property RequestDateLocalGMTOffset() As Integer Implements IActionRequest.RequestDateLocalGMTOffset
        Get
            Return m_requestDateLocalGMTOffset
        End Get
        Set(ByVal Value As Integer)
            m_requestDateLocalGMTOffset = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' company ID 
    ' ----------  ARPATECH  ----------
    Public Property CompanyID() As Integer Implements IActionRequest.CompanyID
        Get
            Return m_companyID
        End Get
        Set(ByVal Value As Integer)
            m_companyID = Value

            SetCompanyID(m_companyID, CStr(m_companyID))

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for assisgning company ID
    ' to local variable
    ' ----------  ARPATECH  ----------
    Sub SetCompanyID(ByVal companyid As Integer, ByVal displayText As String)
        m_companyID = companyid
        ' AddFieldByDisplay("Company", CStr(companyid), displayText)
        AddFieldByDisplay("Ticket Company of Affected User", CStr(companyid), displayText)
        AddFieldByDisplay("Customer", CStr(companyid), displayText)
        AddFieldByDisplay("Request Customer", CStr(companyid), displayText)
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' affected user contact ID and also returns
    ' name and email of that user
    ' ----------  ARPATECH  ----------
    Public Property AffectedContactID() As Integer Implements IActionRequest.AffectedContactID
        Get
            Return m_affectedContactID
        End Get
        Set(ByVal Value As Integer)
            m_affectedContactID = Value
            Dim username As String


            ' Add contact email field
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Conn.Open()
            Dim sql As String
            Dim myEmail As New String("")
            Dim myContactName As New String("")
            sql = "Select IsNull(email, '" & DefaultEmail & "') as email, isNull(u.username, '') as username, isnull(firstname + ' ' + lastname, '') as fullname from tblContact left join tblusers u on u.userid = tblcontact.userid where contactid =" & m_affectedContactID


            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader
            Try

                While dr.Read()

                    myEmail = dr.GetString(dr.GetOrdinal("email"))
                    myContactName = dr.GetString(dr.GetOrdinal("fullname"))
                    If myContactName.Length < 1 Then
                        myContactName = dr.GetString(dr.GetOrdinal("username"))
                    End If


                End While
                AddFieldByDisplay("Email of Affected User", myEmail, myEmail)
                AddFieldByDisplay("Affected User", CStr(Value), myContactName)
            Catch e As Exception

                EventLog.WriteEntry("Application", "Error getting email address: " & e.Message)

            Finally
                Conn.Close()
            End Try
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for assigning affedted user ID
    ' to local variable
    ' ----------  ARPATECH  ----------
    Sub SetAffectedContactID(ByVal contactid As Integer, ByVal displayText As String)
        m_affectedContactID = contactid
        AddFieldByDisplay("Affected User", CStr(contactid), displayText)
        ' Add contact email field
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim sql As String
        Dim myEmail As New String("")
        sql = "Select IsNull(email, '" & DefaultEmail & "') as email from tblContact where contactid =" & m_affectedContactID


        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Dim dr As SqlDataReader
        Cmd.CommandText = sql
        dr = Cmd.ExecuteReader
        Try

            While dr.Read()

                myEmail = dr.GetString(dr.GetOrdinal("email"))

            End While
            AddFieldByDisplay("Email of Affected User", myEmail, myEmail)
        Catch e As Exception

            EventLog.WriteEntry("Application", "Error getting email address: " & e.Message)

        Finally
            Conn.Close()
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Ticket ID
    ' ----------  ARPATECH  ----------
    Public Property GSTicketID() As Integer Implements IActionRequest.GSTicketID
        Get
            Return m_GSTicketID
        End Get
        Set(ByVal Value As Integer)
            m_GSTicketID = Value
            AddFieldByDisplay("GS Ticket Number", CStr(Value))
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' asset ID and also retrieves other
    ' local variable related to assets
    ' ----------  ARPATECH  ----------
    Public Property GSAssetID() As Integer Implements IActionRequest.GSAssetID
        Get
            Return m_GSAssetID
        End Get
        Set(ByVal Value As Integer)
            m_GSAssetID = Value
            ' Add asset fields
            Dim sql
            Dim countryid As Integer
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Conn.Open()
            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            sql = "select a.serialnum, a.assettagnum, a.supportedbysfaid, sfa.sfaname, ao.assettypeid, ao.manufacturerid, ao.manufacturer, " & _
            " ao.customerpartcategoryid, pc.partcategoryname, awa.countryid, awa.customeraddressid, co.countryname, " & _
            " awa.condensedaddress from GSNetP_tbl_assets a " & _
            " left join GSNetP_tbl_AssetsOwned ao on a.assetsownedid = ao.assetsownedid and ao.assettype in ('P', 'C') " & _
            " left join GSNetP_tbl_AddrWithAssets awa on awa.addrwithassetsid = a.addrwithassetsid " & _
            " left join tblCountries co on co.countryid = awa.countryid " & _
            " left join tblPartCategories pc on ao.customerpartcategoryid = pc.partcategoryid " & _
            " left join tblSFAs sfa on sfa.sfaid = a.supportedbysfaid " & _
            " where a.assetid = " & m_GSAssetID

            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader

            Try

                While dr.Read()
                    If Not dr.IsDBNull(dr.GetOrdinal("supportedBySFAID")) Then
                        AddFieldByDisplay("Assigned FSC", CStr(dr.GetInt32(dr.GetOrdinal("supportedBySFAID"))), dr.GetString(dr.GetOrdinal("sfaname")))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("countryid")) Then
                        AddFieldByDisplay("Country of Affected Asset", CStr(dr.GetInt32(dr.GetOrdinal("countryid"))), dr.GetString(dr.GetOrdinal("countryname")))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("serialnum")) Then
                        m_serialNum = dr.GetString(dr.GetOrdinal("serialnum"))
                    End If

                    If Not dr.IsDBNull(dr.GetOrdinal("assettagnum")) Then
                        m_assetTag = dr.GetString(dr.GetOrdinal("assettagnum"))
                    End If

                End While
            Catch e As Exception

                EventLog.WriteEntry("Application", "Error getting CountryID for customer address" & AddressID & ": " & e.Message)

            Finally
                Conn.Close()
            End Try

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket request date
    ' ----------  ARPATECH  ----------
    Public Property RequestDate() As Date Implements IActionRequest.RequestDate
        Get
            Return m_requestDate
        End Get
        Set(ByVal Value As Date)
            m_requestDate = Value
            AddFieldByDisplay("Request Date (Local Time)", CStr(Value))
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' asset Tag
    ' ----------  ARPATECH  ----------
    Public Property AssetTag() As String Implements IActionRequest.AssetTag
        Get
            Return m_assetTag
        End Get
        Set(ByVal Value As String)
            m_assetTag = Value
            AddFieldByDisplay("Asset Tag", m_assetTag, m_assetTag)
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' serial number gererated for asset
    ' ----------  ARPATECH  ----------
    Public Property SerialNum() As String Implements IActionRequest.SerialNum
        Get
            Return m_serialNum
        End Get
        Set(ByVal Value As String)
            m_serialNum = Value
            AddFieldByDisplay("Serial Number", m_serialNum, m_serialNum)
            If Me.GSAssetID < 1 Then
                Dim assetid As Integer = GSSupportManager.GetSingleDBValueInt("select assetid from gsnetp_tbl_assets where serialnum='" & Value & "'")
                If assetid > 0 Then
                    Me.GSAssetID = assetid
                End If
            End If

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' workflow instance
    ' ----------  ARPATECH  ----------
    Public Property WorkflowInstance() As WorkflowMonitor Implements IActionRequest.WorkflowInstance
        Get
            Return m_workflowInstance
        End Get
        Set(ByVal Value As WorkflowMonitor)
            m_workflowInstance = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' schedule ID
    ' ----------  ARPATECH  ----------
    Public Property ScheduleID() As Integer Implements IActionRequest.ScheduleID
        Get
            Return m_scheduleID
        End Get
        Set(ByVal Value As Integer)
            m_scheduleID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for only Retrieving
    ' get workflow instance ID as integer
    ' ----------  ARPATECH  ----------
    Public Function GetWorkflowInstanceID() As Integer Implements IActionRequest.GetWorkflowInstanceID

        Dim wiid = 0
        If Not IsNothing(m_workflowInstance) Then
            wiid = m_workflowInstance.WorkflowInstanceID
        End If
        GetWorkflowInstanceID = wiid

    End Function

    ' ----------  ARPATECH  ----------
    ' testing sub routine
    ' ----------  ARPATECH  ----------
    Public Sub Test() Implements IActionRequest.Test
        MessageBox.Show("Hi!")
    End Sub

    ' ----------  ARPATECH  ----------
    ' function for submitting AR to DB queue
    ' and then returns submission status
    ' ----------  ARPATECH  ----------
    Public Function Submit() As Boolean Implements IActionRequest.Submit
        Dim isSubmitted As Boolean = False

        ' Submit date is always Server time
        m_submitDate = Now()

        ' Local request date time zone is always in local time timezone of request customer address
        Dim tz As Integer = 0

        If Not IsNothing(m_addressID) AndAlso m_addressID > 0 AndAlso (IsNothing(m_requestDateTimeZoneID) Or m_requestDateTimeZoneID < 1) Then

            tz = Manager.GetTimeZoneFromCustomerAddress(m_addressID)

            If m_requestDateTimeZoneID <> tz Then
                m_requestDateTimeZoneID = tz
            End If

        End If

        ' Check for local request date
        ' If we don't have one it's probably submitted by ISD on behalf of customer
        If IsNothing(m_requestDateLocal) Or Year(m_requestDateLocal) < MinYear Then
            m_requestDate = Now()
            m_requestDateLocal = Now()
            m_requestDateTimeZoneVBConst = ServerTimeZone
            m_requestDateTimeZoneID = Manager.GetVBConstTimeZone(ServerTimeZone)


        End If

        ' Add all request time-related fields...

        AddFieldByDisplay("Request Date (Local Time)", Month(m_requestDateLocal) & "/" & Microsoft.VisualBasic.Day(m_requestDateLocal) & "/" & Year(m_requestDateLocal) & " " & Hour(m_requestDateLocal) & ":" & Minute(m_requestDateLocal) & " " & m_requestDateTimeZoneID)

        AddFieldByDisplay("Request Rec'd Date (Server Time)", Now(), Now())


        Try

            If Not ARUpdateDB() Or Not SendToQ(IncomingARQ, CreateMQMessage()) Then
                EventLog.WriteEntry("Application", "Unable to Send AR to queue.")
            Else
                isSubmitted = True
            End If

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error sending AR to queue:" & e.Message)
        End Try


        Return isSubmitted


    End Function

    ' ----------  ARPATECH  ----------
    ' function that updates Action Request to
    ' DB and returns updation status
    ' ----------  ARPATECH  ----------
    Public Function ARUpdateDB() As Boolean Implements IActionRequest.ARUpdateDB
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim isSubmitted As Boolean = True

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn

        Try

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_SubmitAR"


            SqlCommandBuilder.DeriveParameters(Cmd)
            ' EventLog.WriteEntry("Application", " " & m_submitDate & m_requestDate & m_requestDateLocal & m_ticketCreateLocal)
            Cmd.Parameters("@submitdate").Value = m_submitDate

            Cmd.Parameters("@ticketcontactname").Value = m_ticketContactName
            Cmd.Parameters("@ticketcontactphone").Value = m_ticketContactPhone

            Cmd.Parameters("@requestdate").Value = m_requestDate
            Cmd.Parameters("@requestdatelocal").Value = m_requestDateLocal
            Cmd.Parameters("@requesttimezoneid").Value = m_requestDateTimeZoneID
            Cmd.Parameters("@gmtoffset").Value = m_requestDateLocalGMTOffset
            Cmd.Parameters("@gmtoffsetstring").Value = m_requestDateLocalGMTOffsetString
            If m_generateTicket Then
                If IsNothing(m_ticketCreateLocal) OrElse Year(m_ticketCreateLocal) < 2000 OrElse CStr(m_ticketCreateLocal) = "12:00:00 AM" Then

                    Cmd.Parameters("@logdatelocal").Value = Now()
                Else
                    Cmd.Parameters("@logdatelocal").Value = m_ticketCreateLocal

                End If


            End If

            Cmd.Parameters("@userid").Value = m_submitUserID

            If Not IsNothing(m_GSAssetID) AndAlso m_GSAssetID > 0 Then
                Cmd.Parameters("@assetid").Value = m_GSAssetID
            End If

            If Not IsNothing(m_assetTag) AndAlso Len(m_assetTag) > 0 Then
                Cmd.Parameters("@assettag").Value = m_assetTag
            End If

            If Not IsNothing(m_serialNum) AndAlso Len(m_serialNum) > 0 Then
                Cmd.Parameters("@serialnumber").Value = m_serialNum
            End If

            If Not IsNothing(m_categoryID) Then
                Cmd.Parameters("@categoryid").Value = m_categoryID
            End If

            Cmd.Parameters("@gscomments").Value = m_GSComments
            Cmd.Parameters("@priorityid").Value = m_priorityID
            Cmd.Parameters("@artypeid").Value = m_typeID
            Cmd.Parameters("@companyid").Value = m_companyID
            If Not IsNothing(m_addressID) AndAlso m_addressID > 0 Then
                Cmd.Parameters("@addressid").Value = m_addressID
            End If

            Cmd.Parameters("@description").Value = m_description
            Cmd.Parameters("@summary").Value = m_summary
            Cmd.Parameters("@affectedcontactid").Value = m_affectedContactID
            If Not IsNothing(m_scheduleID) AndAlso m_scheduleID > 0 Then
                Cmd.Parameters("@scheduleid").Value = m_scheduleID
            End If

            If Not IsNothing(m_workflowInstance) AndAlso Not IsNothing(m_workflowInstance.WorkflowInstanceID) Then
                Cmd.Parameters("@workflowinstanceid").Value = GetWorkflowInstanceID()
            End If
            Cmd.Parameters("@generateticket").Value = m_generateTicket

            If Not IsNothing(m_supplierRequestID) AndAlso Len(m_supplierRequestID) > 0 Then
                Cmd.Parameters("@sfaticketid").Value = m_supplierRequestID
            End If

            ' EventLog.WriteEntry("Application", "Customer req id is " & m_customerRequestID)
            If Not IsNothing(m_customerRequestID) AndAlso Len(m_customerRequestID) > 0 Then
                Cmd.Parameters("@customerticketid").Value = m_customerRequestID
            End If


            ' Output Parameters
            Cmd.Parameters("@arid").Value = m_actionRequestID
            Cmd.Parameters("@ticketid").Value = m_GSTicketID


            Cmd.ExecuteNonQuery()

            Me.ActionRequestID = Cmd.Parameters("@arid").Value

            If m_generateTicket Then
                Me.GSTicketID = Cmd.Parameters("@ticketid").Value
            End If


        Catch e As Exception

            isSubmitted = False

            EventLog.WriteEntry("Application", "Failed to update DB:" & e.Message)
        Finally
            'Cmd = Nothing
            Conn.Close()
        End Try

        Return isSubmitted

    End Function

    ' ----------  ARPATECH  ----------
    ' function that returns a XML serializer
    ' ----------  ARPATECH  ----------
    Public Function GetXmlSerializer() As XmlSerializer

        Dim t As Type = GetType(GSSupport.ActionRequest)

        Dim serializer As New XmlSerializer(t)

        Return serializer
    End Function

    ' ----------  ARPATECH  ----------
    ' function that converts AR to Message queue
    ' ----------  ARPATECH  ----------
    Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage
        Dim l As String
        Dim m As New MSMQMessage()

        Try
            Me.SerializeFields()

            Dim n As Integer = 0
            Dim t As Type = GetType(GSSupport.ActionRequest)

            Dim serializer As New XmlSerializer(t)
            Dim ar As New ActionRequest()

            ' Dim serializer As XmlSerializer = ar.GetXmlSerializer()

            Dim ns As New XmlSerializerNamespaces()

            ' ns.Add("inventory", "http://www.cpandl.com")
            ' ns.Add("GSSupport", "https://onesource.global-serve.com")

            Dim ms As MemoryStream = New MemoryStream()

            Dim writer As New XmlTextWriter(ms, New ASCIIEncoding())

            ' Serialize using the XmlTextWriter.

            serializer.Serialize(writer, Me, ns)

            Dim arChars As Char()

            ReDim arChars(ms.Length)


            arChars = Encoding.ASCII.GetChars(ms.ToArray)

            Dim objxml As New String(arChars)
            '  EventLog.WriteEntry("Application", "AR " & Me.ActionRequestID & " sending " & objxml)

            writer.Close()


            l = Now() & "&AR=" & CStr(m_actionRequestID) & "&CO=" & CStr(m_companyID) & "&UID=" & CStr(m_submitUserID)
            If m_qualify Then
                l = l & "&GSQUAL=1"
            ElseIf (m_isCompletedAction) Then
                l = l & ActionCompleteString
            End If

            m.Label = l
            m_mqLabel = l
            m.Body = objxml


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error converting AR to message: " & e.Message & "," & e.InnerException.Message & "," & e.InnerException.InnerException.Message & "," & e.InnerException.InnerException.InnerException.Message & "," & e.InnerException.InnerException.InnerException.InnerException.Message & ",")
        End Try
        Return m
    End Function

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR (Empty Argument)
    ' ----------  ARPATECH  ----------
    Public Sub New()

        m_requestDate = Now()
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR (with Action Request ID argument)
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal arid As Integer)

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim SLACount As Integer = 0
        Dim sql As New String("")

        Me.ActionRequestID = arid

        Conn.Open()
        ' Start a local transaction.

        Dim Cmd As New SqlCommand()

        Try
            Dim dr As SqlDataReader

            sql = "select * from tblActionRequest where arid = " & arid

            Cmd.CommandText = sql
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                Me.AddressID = dr.GetInt32(dr.GetOrdinal("addressid"))
                Me.CompanyID = dr.GetInt32(dr.GetOrdinal("companyid"))
                Me.AffectedContactID = dr.GetInt32(dr.GetOrdinal("affectedusercontactid"))
                Me.CategoryID = dr.GetInt32(dr.GetOrdinal("arCatID"))
                Me.TypeID = dr.GetInt32(dr.GetOrdinal("arTypeID"))
                Me.PriorityID = dr.GetInt32(dr.GetOrdinal("priorityid"))

                If Not dr.IsDBNull(dr.GetOrdinal("assettagnum")) Then
                    Me.AssetTag = dr.GetString(dr.GetOrdinal("assettagnum"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("serialnum")) Then
                    Me.SerialNum = dr.GetString(dr.GetOrdinal("serialnum"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("arDesc")) Then
                    Me.Description = dr.GetString(dr.GetOrdinal("arDesc"))
                End If

                Me.Summary = dr.GetString(dr.GetOrdinal("arName"))

                Me.RequestDate = dr.GetDateTime(dr.GetOrdinal("requestdate"))
                Me.RequestDateLocal = dr.GetDateTime(dr.GetOrdinal("requestdatelocal"))
                Me.RequestDateTimeZoneID = dr.GetInt32(dr.GetOrdinal("requesttimezoneid"))

                Me.GenerateTicket = False
                Me.Qualify = False

            End While

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error creating ar from arid " & arid & ": " & e.Message)

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that confirms fieldID
    ' existance in memoryfields
    ' ----------  ARPATECH  ----------
    Function ContainsField(ByVal fieldID As Integer) As Boolean
        Return m_fields.ContainsKey(fieldID)
    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine for adding field to memeory
    ' variable by display
    ' ----------  ARPATECH  ----------
    Sub AddFieldByDisplay(ByVal fieldDisplayName As String, ByVal fieldTextValue As String, Optional ByVal fieldDisplayValue As String = "") Implements IActionRequest.AddFieldByDisplay

        Dim f As New Field()

        Try

            ' Load fields if they haven't been already
            If GSSupportManager.m_fieldsByDisplay.Count = 0 Then
                GSSupportManager.LoadFields()
            End If

            f = GSSupportManager.GetFieldByDisplay(fieldDisplayName)

            f.FieldTextValue = fieldTextValue

            ' If there's a displayvalue supplied use it...
            If Len(fieldDisplayValue) > 0 Then
                f.FieldDisplayValue = fieldDisplayValue
                ' otherwise go ahead and set display value to text value
            Else
                f.FieldDisplayValue = fieldTextValue

            End If




            If Not IsNothing(f) AndAlso f.FieldID > 0 Then
                AddField(f)
                '        EventLog.WriteEntry("Application", "Adding Field " & f.FieldName & f.FieldID & ", " & f.FieldTextValue)

            End If


        Catch e As Exception

            EventLog.WriteEntry("Application", "Error getting fieldID for AddFieldByDisplay " & fieldDisplayName & ": " & e.Message)

        Finally

        End Try



    End Sub

    ' This function is here primarily for passing field values through ASP
    Sub AddFieldWithDisplay(ByVal fieldDisplayName As String, ByVal fieldTextValue As String, ByVal fieldDisplayValue As String) Implements IActionRequest.AddFieldWithDisplay
        AddFieldByDisplay(fieldDisplayName, fieldTextValue, fieldDisplayValue)
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for adding fields to memory variable
    ' ----------  ARPATECH  ----------
    Sub AddField(ByVal f As Field)
        Dim i As Integer = f.FieldID
        If m_fields.ContainsKey(i) Then
            m_fields.Remove(i)
            '   EventLog.WriteEntry("Application", "Removing " & i)
        End If
        '  EventLog.WriteEntry("Application", "Adding to AR field " & f.FieldID & "=" & f.FieldTextValue)
        m_fields.Add(f.FieldID, f)

    End Sub

    ' ----------  ARPATECH  ----------
    ' function that Retrieves fields native value
    ' ----------  ARPATECH  ----------
    Function GetFieldNativeValue(ByVal fieldid As Integer) As Object
        Dim o As New Object()
        If m_fields.ContainsKey(fieldid) Then
            Dim f As New Field()
            f = m_fields.Item(fieldid)
            o = f.FieldNativeValue

        End If
        Return o
    End Function

    ' ----------  ARPATECH  ----------
    ' function that Retrieves fields text value
    ' ----------  ARPATECH  ----------
    Function GetFieldTextValue(ByVal fieldid As Integer) As String
        Dim s As New String("")
        If m_fields.ContainsKey(fieldid) Then
            Dim f As New Field()
            f = m_fields.Item(fieldid)
            s = f.FieldTextValue
        End If
        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function that Retrieves customer country ID
    ' ----------  ARPATECH  ----------
    Function GetCustomerAddressCountry(ByVal addressid As Integer) As Integer
        Dim sql
        Dim countryid As Integer
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Dim dr As SqlDataReader
        sql = "select countryid from tblCustomerAddresses where customeraddressid=" & addressid

        Cmd.CommandText = sql
        dr = Cmd.ExecuteReader

        Try

            While dr.Read()
                countryid = dr.GetInt32(dr.GetOrdinal("countryid"))

            End While
        Catch e As Exception

            EventLog.WriteEntry("Application", "Error getting CountryID for customer address" & addressid & ": " & e.Message)

        Finally
            Conn.Close()
        End Try

        Return countryid
    End Function

    ' ----------  ARPATECH  ----------
    ' sub routine for serializing fields
    ' ----------  ARPATECH  ----------
    Public Sub SerializeFields()
        Try
            Dim i As Integer
            m_fieldIDs = New ArrayList()
            m_fieldNames = New ArrayList()
            m_fieldValues = New ArrayList()
            m_fieldDisplays = New ArrayList()

            If m_fields.Count = 0 Then
                Exit Sub
            End If

            For i = 0 To m_fields.Count - 1
                Dim f As Field = m_fields.GetByIndex(i)
                Dim name, value, display As String
                Dim fieldid As Integer
                name = f.FieldName
                If Len(name) > 0 Then
                    value = ""
                    display = ""
                    If Not IsNothing(f.FieldTextValue) AndAlso Len(f.FieldTextValue) > 0 Then
                        value = f.FieldTextValue
                    End If
                    If Not IsNothing(f.FieldDisplayValue) AndAlso Len(f.FieldDisplayValue) > 0 Then
                        display = f.FieldDisplayValue
                    End If
                    If Not IsNothing(f.FieldID) Then
                        fieldid = f.FieldID
                    End If
                    m_fieldNames.Add(name)
                    m_fieldValues.Add(value)
                    m_fieldDisplays.Add(display)
                    m_fieldIDs.Add(fieldid)

                End If

            Next
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error serializing AR fields: " & e.Message)
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for Deserializing fields
    ' ----------  ARPATECH  ----------
    Public Sub DeserializeFields()
        Dim i As Integer
        m_fields = New SortedList()
        For i = 0 To m_fieldIDs.Count - 1
            Dim f As New Field()
            f.FieldID = m_fieldIDs(i)
            f.FieldName = m_fieldNames(i)
            f.FieldTextValue = m_fieldValues(i)
            f.FieldDisplayValue = m_fieldDisplays(i)
            Me.AddField(f)

            ' Me.AddField(m_fieldNames(i), m_fieldValues(i), m_fieldDisplays(i))

        Next

    End Sub
End Class

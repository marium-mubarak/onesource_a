' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is for Terms and Conditions processing like saving, fetching,
'                       and some other functions for other processes. 
' Inherits          :   ---------
' Implements        :   ITermsAndConditions
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface ITermsAndConditions
    Property Qualification() As Qualification
    Property StartTypeID() As Integer

    Function HasEndStatus(ByVal statusid As Integer) As Boolean
    Property TimerTypeID() As Integer
    Property MaxMinutes() As Integer
    Property BizDays() As Integer
    Property IsBizDays() As Boolean
    Property TimerTypeName() As String
    Function GetText() As String
    Function GetStartStatusCount() As Integer
    Function GetEndStatusCount() As Integer
    Function GetExceptionStatusCount() As Integer
    Function GetHolidayExceptionCount() As Integer
    Function GetHolidaysText() As String
    Function GetQualificationText() As String
    Function GetEndStatusText(ByVal index As Integer) As String
    Function GetStartStatusText(ByVal index As Integer) As String
    Function GetExceptionStatusText(ByVal index As Integer) As String
    Property StartTypeName() As String
    Function AddEndStatusID(ByVal statusid As Integer) As Integer
    Function GetEndStatusIDByIndex(ByVal index As Integer) As Integer
    Function AddStartStatusID(ByVal statusid As Integer) As Integer
    Function AddExceptionStatusID(ByVal statusid As Integer) As Integer
    Property TACID() As Integer
    Property PctCommitment() As Decimal
    Property Rank() As Integer
    Property IncludeHolidays() As Boolean
    Property ParentSLA() As SLA
    Property ParentSLAID() As Integer
    Property TACName() As String
    Function Save() As Boolean
    Function GetStartStatusID() As ArrayList
    Function GetEndStatusID() As ArrayList
    Function GetExceptionStatusID() As ArrayList

End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class TermsAndConditions
    Implements ITermsAndConditions

    Private m_qualification As Qualification
    Private m_bizdays As Integer = -1
    Private m_isBizDays As Boolean = False
    Private m_qualificationID As Integer
    Private m_parentSLA As SLA
    Private m_parentSLAID As Integer
    Private m_tacID As Integer
    Private m_endStatusID As New ArrayList()
    Private m_startTypeID As Integer
    Private m_startStatusID As New ArrayList()
    Private m_exceptionStatusID As New ArrayList()
    Private m_timerTypeID As Integer
    Private m_timerTypeName As New String("")
    Private m_startTypeName As New String("")
    Private m_maxMinutes As Integer = 0
    Private m_pctCommitment As Decimal = 1.0
    Private m_rank As Integer = 1
    Private m_tacName As New String("")
    Private m_includeHolidays As Boolean = True
    Private m_holidayExceptions As New SortedList()



    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' operator type ID
    ' ----------  ARPATECH  ----------
    Public Property StartTypeID() As Integer Implements ITermsAndConditions.StartTypeID
        Get
            Return m_startTypeID
        End Get
        Set(ByVal Value As Integer)
            m_startTypeID = Value
            If Len(m_startTypeName) < 1 Then
                Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
                Dim sn As New String("")
                Conn.Open()

                Dim Cmd As New SqlCommand()

                Try
                    Dim dr As SqlDataReader
                    Cmd.CommandText = "select starttypename from tblTACStartTypes where startTypeID=" & m_startTypeID
                    Cmd.Connection = Conn

                    dr = Cmd.ExecuteReader

                    While dr.Read()

                        sn = dr.GetString(dr.GetOrdinal("startTypeName"))


                    End While

                    m_startTypeName = sn
                    If (IsNothing(m_timerTypeID) OrElse m_timerTypeID < 1) AndAlso UCase(m_startTypeName) = "TARGET DATE" Then
                        Dim ttid As Integer
                        ttid = GSSupportManager.GetSingleDBValueInt("select timerTypeID from tblTACTimerTypes where timerTypeName='24x7'")
                        If ttid > 0 Then
                            m_timerTypeID = ttid
                            m_timerTypeName = "24x7"
                        End If
                    End If

                    dr.Close()

                Catch e As Exception
                    EventLog.WriteEntry("Application", "Failed to get start type name:" & e.Message)

                Finally

                    Conn.Close()
                    Cmd = Nothing
                    Conn = Nothing
                End Try
            End If
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' parent SLA
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
        Public Property ParentSLA() As SLA Implements ITermsAndConditions.ParentSLA
        Get
            If IsNothing(m_parentSLA) AndAlso m_parentSLAID > 0 Then
                m_parentSLA = New SLA(m_parentSLAID)
            End If
            Return m_parentSLA
        End Get
        Set(ByVal Value As SLA)
            m_parentSLA = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' TAC ranking
    ' ----------  ARPATECH  ----------
    Public Property Rank() As Integer Implements ITermsAndConditions.Rank
        Get
            Return m_rank
        End Get
        Set(ByVal Value As Integer)
            m_rank = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' end status ID
    ' ----------  ARPATECH  ----------
    Public Function GetEndStatusID() As ArrayList Implements ITermsAndConditions.GetEndStatusID
        Return m_endStatusID
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' start status ID
    ' ----------  ARPATECH  ----------
    Public Function GetStartStatusID() As ArrayList Implements ITermsAndConditions.GetStartStatusID
        Return m_startStatusID
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' exception status ID
    ' ----------  ARPATECH  ----------
    Public Function GetExceptionStatusID() As ArrayList Implements ITermsAndConditions.GetExceptionStatusID
        Return m_exceptionStatusID
    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' business day status
    ' ----------  ARPATECH  ----------
    Public Property IsBizDays() As Boolean Implements ITermsAndConditions.IsBizDays
        Get
            Return m_isBizDays
        End Get
        Set(ByVal Value As Boolean)
            m_isBizDays = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' business days as integer
    ' ----------  ARPATECH  ----------
    Public Property BizDays() As Integer Implements ITermsAndConditions.BizDays
        Get
            Return m_bizdays
        End Get
        Set(ByVal Value As Integer)
            m_bizdays = Value
            EventLog.WriteEntry("Application", "Bizdays is " & m_bizdays)
            If m_bizdays >= 0 Then
                Me.IsBizDays = True
                ' Me.StartTypeName = "Target Date"
                Me.TimerTypeName = "business-asset-24x5"
                EventLog.WriteEntry("Application", "IsBizdays is " & m_isBizDays)
            Else
                Me.IsBizDays = False
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' maximum terms and conds minutes
    ' ----------  ARPATECH  ----------
    Public Property MaxMinutes() As Integer Implements ITermsAndConditions.MaxMinutes
        Get
            Return m_maxMinutes
        End Get
        Set(ByVal Value As Integer)
            m_maxMinutes = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' commitment as decimal
    ' ----------  ARPATECH  ----------
    Public Property PctCommitment() As Decimal Implements ITermsAndConditions.PctCommitment
        Get

            Return m_pctCommitment
        End Get
        Set(ByVal Value As Decimal)
            m_pctCommitment = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' end status number
    ' ----------  ARPATECH  ----------
    Function GetEndStatusIDByIndex(ByVal index As Integer) As Integer Implements ITermsAndConditions.GetEndStatusIDByIndex
        Dim i As Integer = -1
        If index < m_endStatusID.Count Then
            i = m_endStatusID(index)
        End If
        Return i
    End Function



    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' include holidays status
    ' ----------  ARPATECH  ----------
    Public Property IncludeHolidays() As Boolean Implements ITermsAndConditions.IncludeHolidays
        Get
            Return m_includeHolidays
        End Get
        Set(ByVal Value As Boolean)
            m_includeHolidays = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' timer type ID
    ' ----------  ARPATECH  ----------
    Public Property TimerTypeID() As Integer Implements ITermsAndConditions.TimerTypeID
        Get
            Return m_timerTypeID
        End Get
        Set(ByVal Value As Integer)
            m_timerTypeID = Value
            If Len(m_timerTypeName) < 1 Then
                Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
                Dim tn As New String("")
                Conn.Open()

                Dim Cmd As New SqlCommand()


                Try
                    Dim dr As SqlDataReader
                    Cmd.CommandText = "select timertypename from tblTACTimerTypes where timertypeid=" & m_timerTypeID
                    Cmd.Connection = Conn

                    dr = Cmd.ExecuteReader

                    While dr.Read()

                        tn = dr.GetString(dr.GetOrdinal("timertypename"))


                    End While
                    m_timerTypeName = tn


                    dr.Close()

                Catch e As Exception
                    EventLog.WriteEntry("Application", "Failed to get timer type name:" & e.Message)

                Finally

                    Conn.Close()
                    Cmd = Nothing

                    Conn = Nothing
                End Try
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' TAC ID
    ' ----------  ARPATECH  ----------
    Public Property TACID() As Integer Implements ITermsAndConditions.TACID
        Get
            Return m_tacID
        End Get
        Set(ByVal Value As Integer)
            m_tacID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' TAC name
    ' ----------  ARPATECH  ----------
    Public Property TACName() As String Implements ITermsAndConditions.TACName
        Get
            Return m_tacName
        End Get
        Set(ByVal Value As String)
            m_tacName = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' qualification
    ' ----------  ARPATECH  ----------
    Public Property Qualification() As Qualification Implements ITermsAndConditions.Qualification
        Get
            Return m_qualification
        End Get
        Set(ByVal Value As Qualification)
            m_qualification = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' start type name
    ' ----------  ARPATECH  ----------
    Public Property StartTypeName() As String Implements ITermsAndConditions.StartTypeName
        Get
            Return m_startTypeName
        End Get
        Set(ByVal Value As String)
            m_startTypeName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' end  status of TAC
    ' ----------  ARPATECH  ----------
    Function HasEndStatus(ByVal statusid As Integer) As Boolean Implements ITermsAndConditions.HasEndStatus
        Dim i As Integer
        Dim b As Boolean = False
        For i = 0 To m_endStatusID.Count - 1
            If CInt(statusid) = CInt(m_endStatusID(i)) Then
                Return True
            End If
        Next
        Return b
    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' timer type name
    ' ----------  ARPATECH  ----------
    Public Property TimerTypeName() As String Implements ITermsAndConditions.TimerTypeName
        Get
            Return m_timerTypeName
        End Get
        Set(ByVal Value As String)
            m_timerTypeName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' parent SLA Id
    ' ----------  ARPATECH  ----------
    Public Property ParentSLAID() As Integer Implements ITermsAndConditions.ParentSLAID
        Get
            Return m_parentSLAID
        End Get
        Set(ByVal Value As Integer)
            m_parentSLAID = Value
        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' function for adding
    ' start status ID
    ' ----------  ARPATECH  ----------
    Function AddStartStatusID(ByVal statusid As Integer) As Integer Implements ITermsAndConditions.AddStartStatusID
        m_startStatusID.Add(statusid)
        Return m_startStatusID.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' function for adding
    ' end status exception ID
    ' ----------  ARPATECH  ----------
    Function AddEndStatusID(ByVal statusid As Integer) As Integer Implements ITermsAndConditions.AddEndStatusID
        m_endStatusID.Add(statusid)
        Return m_endStatusID.Count
    End Function


    ' ----------  ARPATECH  ----------
    ' function for adding
    ' add exception status ID
    ' ----------  ARPATECH  ----------
    Function AddExceptionStatusID(ByVal statusid As Integer) As Integer Implements ITermsAndConditions.AddExceptionStatusID
        m_exceptionStatusID.Add(statusid)

        Return m_exceptionStatusID.Count
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' end status count
    ' ----------  ARPATECH  ----------
    Public Function GetEndStatusCount() As Integer Implements ITermsAndConditions.GetEndStatusCount

        Return m_endStatusID.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' end exception text
    ' ----------  ARPATECH  ----------
    Public Function GetEndStatusText(ByVal index As Integer) As String Implements ITermsAndConditions.GetEndStatusText
        Dim al As ArrayList = GetStatusTextValues(m_endStatusID)
        Return al.Item(index)
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' start exception text
    ' ----------  ARPATECH  ----------
    Public Function GetStartStatusText(ByVal index As Integer) As String Implements ITermsAndConditions.GetStartStatusText
        Dim al As ArrayList = GetStatusTextValues(m_startStatusID)
        Return al(index)
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' exception status text
    ' ----------  ARPATECH  ----------
    Public Function GetExceptionStatusText(ByVal index As Integer) As String Implements ITermsAndConditions.GetExceptionStatusText
        Dim al As ArrayList = GetStatusTextValues(m_exceptionStatusID)
        Return al(index)
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' qualification text
    ' ----------  ARPATECH  ----------
    Public Function GetQualificationText() As String Implements ITermsAndConditions.GetQualificationText
        Return m_qualification.GetText()
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' holiday exception count
    ' ----------  ARPATECH  ----------
    Public Function GetHolidayExceptionCount() As Integer Implements ITermsAndConditions.GetHolidayExceptionCount
        If m_includeHolidays Then
            Return 0
        Else
            Return m_holidayExceptions.Count
        End If
    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' holidays text from DB
    ' ----------  ARPATECH  ----------
    Public Function GetHolidaysText() As String Implements ITermsAndConditions.GetHolidaysText
        Dim s As New String("")
        If m_includeHolidays Then
            Return s
        Else
            s = s & "<table class=holidays>"
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Dim sn As New String("")
            Conn.Open()

            Dim Cmd As New SqlCommand()

            Try
                Dim dr As SqlDataReader
                Cmd.CommandText = "select countryname, holidayname, holidaydate from tblTACHolidays, tblCountries, tblholidays where tblTACHolidays.holidayid = tblHolidays.holidayid and tblCountries.countryid= tblTACHolidays.countryid and tacid=" & m_tacID & " order by countryname, holidaydate"
                Cmd.Connection = Conn
                dr = Cmd.ExecuteReader
                Dim countryname As String
                Dim lastCountry As String

                While dr.Read()
                    Dim hd As Date
                    Dim hn As String


                    countryname = dr.GetString(dr.GetOrdinal("countryname"))



                    hn = dr.GetString(dr.GetOrdinal("holidayname"))
                    hd = dr.GetDateTime(dr.GetOrdinal("holidaydate"))
                    s = s & "<tr><td>"

                    If countryname <> lastCountry Then
                        s = s & countryname
                    End If
                    s = s & "</td>"
                    s = s & "<td>" & MonthName(Month(hd)) & " " & Day(hd)

                    If Len(hn) > 3 Then
                        s = s & " (" & hn & ") "
                    End If
                    s = s & "</td></tr>" & vbCrLf

                    lastCountry = countryname

                End While

                dr.Close()
                s = s & "</table>"

            Catch e As Exception
                EventLog.WriteEntry("Application", "Failed to get holiday name:" & e.Message)

            Finally
                Conn.Close()
                Cmd = Nothing
                Conn = Nothing
            End Try
        End If

        Return s
    End Function




    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' holiday text name from db
    ' ----------  ARPATECH  ----------
    Function GetHolidayNameText(ByVal holidayid As Integer) As String
        Dim s As New String("")

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim sn As New String("")
        Conn.Open()

        Dim Cmd As New SqlCommand()

        Try
            Dim dr As SqlDataReader
            Cmd.CommandText = "select holidaydate, holidayname from tblHolidays where holidayid=" & holidayid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                Dim hd As Date
                Dim hn As String

                hn = dr.GetString(dr.GetOrdinal("holidayname"))
                hd = dr.GetDateTime(dr.GetOrdinal("holidaydate"))
                s = MonthName(Month(hd)) & " " & Day(hd)
                If Len(hd) > 3 Then
                    s = s & " (" & hn & ") "
                End If

            End While

            dr.Close()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to get holiday name:" & e.Message)

        Finally

            Conn.Close()
            Cmd = Nothing
            Conn = Nothing
        End Try

        Return s

    End Function



    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' exception status count
    ' ----------  ARPATECH  ----------
    Public Function GetExceptionStatusCount() As Integer Implements ITermsAndConditions.GetExceptionStatusCount

        Return m_exceptionStatusID.Count

    End Function

    ' ----------  ARPATECH  ----------
    ' function for Retrieving
    ' start status count
    ' ----------  ARPATECH  ----------
    Public Function GetStartStatusCount() As Integer Implements ITermsAndConditions.GetStartStatusCount

        Return m_startStatusID.Count

    End Function



    ' ----------  ARPATECH  ----------
    ' function for getting text from 
    ' TAC arraylist
    ' ----------  ARPATECH  ----------
    Function GetText() As String Implements ITermsAndConditions.GetText
        Try
            EventLog.WriteEntry("Application", "Get TExt 1")

            Dim s As New String("")
            Dim i As Integer
            Dim al As New ArrayList()
            If Not IsNothing(m_qualification) Then
                s = s & m_qualification.GetText()
            End If
            s = s & " status must be "
            al = GetStatusTextValues(m_endStatusID)
            For i = 0 To al.Count - 1
                s = s & CStr(al(i))
                If i <> al.Count - 1 Then
                    s = s & " or "
                End If
            Next
            EventLog.WriteEntry("Application", "Get TExt 2")
            If UCase(m_startTypeName) <> "TARGET DATE" Then
                EventLog.WriteEntry("Application", "Get TExt 3")

                s = s & " within " & m_maxMinutes & " minutes ("



                s = s & m_timerTypeName
                If m_includeHolidays Then
                    s = s & ", local holidays included"
                Else
                    s = s & ", local holidays excluded"
                End If
                s = s & ")"
                s = s & " of " & m_startTypeName
            Else
                EventLog.WriteEntry("Application", "Get TExt 4")
                EventLog.WriteEntry("Application", "Biz Days " & m_bizdays & m_isBizDays)
                If Not m_isBizDays Then
                    EventLog.WriteEntry("Application", "Get TExt 5")
                    s = s & " by assigned Target Date "
                Else
                    EventLog.WriteEntry("Application", "Get TExt 6")
                    s = s & " by the end of " & m_bizdays & " business days (local holidays excluded)"
                End If

            End If

            If UCase(m_startTypeName) = "STATUS CHANGE" Then
                s = s & " to "
                al = GetStatusTextValues(m_startStatusID)
                For i = 0 To al.Count - 1
                    s = s & CStr(al(i))
                    If i <> al.Count - 1 Then
                        s = s & " or "
                    End If
                Next
            End If

            If m_exceptionStatusID.Count > 0 Then
                s = s & " excluding time when the status is "
                al = GetStatusTextValues(m_exceptionStatusID)

                For i = 0 To al.Count - 1
                    s = s & CStr(al(i))
                    If i <> al.Count - 1 Then
                        s = s & " or "
                    End If
                Next
            End If


            Return s
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error getting tac text: " & e.Message)

        End Try

    End Function

    ' ----------  ARPATECH  ----------
    ' function to return TAC text vlaues
    ' as array list
    ' ----------  ARPATECH  ----------
    Function GetStatusTextValues(ByVal arStatusIDs As ArrayList) As ArrayList
        Dim al As New ArrayList()
        Dim i As Integer
        Dim sql, rs
        If arStatusIDs.Count > 0 Then


            For i = 0 To arStatusIDs.Count - 1
                sql = sql & CStr(arStatusIDs(i))
                If i <> arStatusIDs.Count - 1 Then
                    sql = sql & ","
                End If

            Next
            sql = "select tktstatusname from tblTicketStatus where tktstatusid in (" & sql & ") order by tktstatusid"


            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

            Conn.Open()

            Dim Cmd As New SqlCommand()

            Try
                Dim dr As SqlDataReader
                Cmd.CommandText = sql
                Cmd.Connection = Conn

                dr = Cmd.ExecuteReader

                While dr.Read()

                    al.Add(dr.GetString(dr.GetOrdinal("tktstatusname")))


                End While

                dr.Close()

            Catch e As Exception
                EventLog.WriteEntry("Application", "Failed to get status names:" & e.Message)

            Finally

                Conn.Close()
                Cmd = Nothing

                Conn = Nothing
            End Try
        End If

        Return al


    End Function

    ' ----------  ARPATECH  ----------
    ' function to save terms and conds 
    ' and return status
    ' ----------  ARPATECH  ----------
    Function Save() As Boolean Implements ITermsAndConditions.Save
        Dim i As Integer
        Dim b As Boolean = True
        Try

            If Not m_qualification.Save() Then
                EventLog.WriteEntry("Application", "Unable to save TandC qualification.")
                Return False
            End If

            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
            Dim isSubmitted As Boolean = True
            Conn.Open()
            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Cmd.CommandType = CommandType.StoredProcedure

            ' Add TAC properties
            Cmd.CommandText = "sp_addTAC"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_tacID
            Cmd.Parameters("@qualificationid").Value = m_qualification.QualificationID
            Cmd.Parameters("@timertypeid").Value = m_timerTypeID
            Cmd.Parameters("@starttypeid").Value = m_startTypeID
            Cmd.Parameters("@tacname").Value = m_tacName
            Cmd.Parameters("@rank").Value = m_rank
            Cmd.Parameters("@pctcommitment").Value = m_pctCommitment
            Cmd.Parameters("@maxminutes").Value = m_maxMinutes
            Cmd.Parameters("@slaid").Value = m_parentSLA.SLAID
            If m_bizdays >= 0 Then
                Cmd.Parameters("@bizdays").Value = m_bizdays
            End If

            Cmd.Parameters("@includeholidays").Value = m_includeHolidays

            Cmd.ExecuteNonQuery()
            m_tacID = Cmd.Parameters("@tacid").Value

            ' Add Start Status Types
            Cmd.CommandText = "sp_addTACStartStatus"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_tacID
            For i = 0 To m_startStatusID.Count - 1
                Cmd.Parameters("@statusid").Value = m_startStatusID(i)
                Cmd.ExecuteNonQuery()
            Next

            ' Add End Status Types
            Cmd.CommandText = "sp_addTACEndStatus"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_tacID
            For i = 0 To m_endStatusID.Count - 1
                Cmd.Parameters("@statusid").Value = m_endStatusID(i)
                Cmd.ExecuteNonQuery()
            Next

            ' Add Exception Status Types
            Cmd.CommandText = "sp_addTACExceptionStatus"
            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@tacid").Value = m_tacID
            For i = 0 To m_exceptionStatusID.Count - 1
                Cmd.Parameters("@statusid").Value = m_exceptionStatusID(i)
                Cmd.ExecuteNonQuery()
            Next

            ' Add holidays
            If Not m_includeHolidays Then
                Cmd.CommandText = "sp_addTACDefaultHolidays"
                SqlCommandBuilder.DeriveParameters(Cmd)
                Cmd.Parameters("@tacid").Value = m_tacID

                Cmd.ExecuteNonQuery()

                ' Remove holiday exceptions
                For i = 0 To m_holidayExceptions.Count - 1

                    Cmd.CommandText = "sp_RemoveTACHoliday"
                    SqlCommandBuilder.DeriveParameters(Cmd)
                    Dim countryid As Integer = m_holidayExceptions.GetKey(i)
                    Dim al As ArrayList = m_holidayExceptions.GetByIndex(i)
                    Dim n As Integer = 0
                    For n = 0 To al.Count
                        Dim holidayid As Integer = al.Item(n)
                        Cmd.Parameters("@tacid").Value = m_tacID
                        Cmd.Parameters("@countryid").Value = countryid
                        Cmd.Parameters("@holidayid").Value = holidayid
                        Cmd.ExecuteNonQuery()

                    Next

                Next


            End If


        Catch e As Exception
            EventLog.WriteEntry("Application", "Unable to save TandC:" & e.Message)
            b = False
        End Try
        Return b
    End Function

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with terms and conds id and 
    ' arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal tacid As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim tn As New String("")
        m_tacID = tacid
        Conn.Open()

        Dim Cmd As New SqlCommand()


        Try
            Dim dr As SqlDataReader
            Cmd.CommandText = "select * from tblTermsAndConditions where tacid=" & tacid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()

                Me.TimerTypeID = dr.GetInt32(dr.GetOrdinal("timerTypeID"))
                Me.TACName = dr.GetString(dr.GetOrdinal("tacName"))
                Me.Rank = dr.GetInt32(dr.GetOrdinal("tacRank"))
                m_parentSLAID = dr.GetInt32(dr.GetOrdinal("slaid"))
                '   Me.ParentSLA = New SLA(dr.GetInt32(dr.GetOrdinal("slaid")))
                Me.StartTypeID = dr.GetInt32(dr.GetOrdinal("startTypeID"))
                Me.PctCommitment = dr.GetDecimal(dr.GetOrdinal("tacPctCommitment"))
                Me.MaxMinutes = dr.GetInt32(dr.GetOrdinal("maxMinutes"))
                Me.IncludeHolidays = dr.GetBoolean(dr.GetOrdinal("includeHolidays"))
                If Not dr.IsDBNull(dr.GetOrdinal("tacPctCommitment")) Then
                    m_pctCommitment = dr.GetDecimal(dr.GetOrdinal("tacPctCommitment"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("bizDays")) Then
                    Me.BizDays = dr.GetInt32(dr.GetOrdinal("bizDays"))

                End If

                m_qualification = New Qualification(dr.GetInt32(dr.GetOrdinal("qualificationID")))
            End While
            dr.Close()


            Cmd.CommandText = "select statusid from tblTACEndStatus where isActive=1 and tacid=" & tacid
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_endStatusID.Add(dr.GetInt32(dr.GetOrdinal("statusid")))
            End While
            dr.Close()


            Cmd.CommandText = "select statusid from tblTACStartStatus where isActive=1 and tacid=" & tacid
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_startStatusID.Add(dr.GetInt32(dr.GetOrdinal("statusid")))
            End While
            dr.Close()

            Cmd.CommandText = "select statusid from tblTACExceptionStatus where isActive=1 and tacid=" & tacid
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_exceptionStatusID.Add(dr.GetInt32(dr.GetOrdinal("statusid")))
            End While
            dr.Close()




        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to create TAC:" & e.Message)

        Finally

            Conn.Close()
            Cmd = Nothing

            Conn = Nothing
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with terms and conds id and 
    ' connection arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal tacid As Integer, ByRef Conn As SqlConnection)
        ' Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim tn As New String("")

        m_tacID = tacid
        '  Conn.Open()

        Dim Cmd As New SqlCommand()


        Try
            Dim dr As SqlDataReader
            Cmd.CommandText = "select tblTermsAndConditions.timerTypeID, tacName, tacRank, slaid, " & _
            " startTypeID, bizDays, tacPctCommitment, maxMinutes, includeHolidays, " & _
            " tacPctCommitment, qualificationID, timerTypeName from tblTermsAndConditions " & _
            " left join tblTACTimerTypes on tblTACTimerTypes.timerTypeID = tblTermsAndConditions.timerTypeID " & _
            " where tacid=" & tacid
            Cmd.Connection = Conn

            dr = Cmd.ExecuteReader

            While dr.Read()
                m_timerTypeName = dr.GetString(dr.GetOrdinal("timerTypeName"))

                m_timerTypeID = dr.GetInt32(dr.GetOrdinal("timerTypeID"))

                Me.TACName = dr.GetString(dr.GetOrdinal("tacName"))
                Me.Rank = dr.GetInt32(dr.GetOrdinal("tacRank"))
                Me.ParentSLAID = dr.GetInt32(dr.GetOrdinal("slaid"))
                ' Me.ParentSLA = New SLA(dr.GetInt32(dr.GetOrdinal("slaid")))
                Me.StartTypeID = dr.GetInt32(dr.GetOrdinal("startTypeID"))
                ' Me.PctCommitment = dr.GetDecimal(dr.GetOrdinal("tacPctCommitment"))
                Me.MaxMinutes = dr.GetInt32(dr.GetOrdinal("maxMinutes"))
                Me.IncludeHolidays = dr.GetBoolean(dr.GetOrdinal("includeHolidays"))
                If Not dr.IsDBNull(dr.GetOrdinal("tacPctCommitment")) Then
                    m_pctCommitment = dr.GetDecimal(dr.GetOrdinal("tacPctCommitment"))
                End If
                If Not dr.IsDBNull(dr.GetOrdinal("bizDays")) Then
                    Me.BizDays = dr.GetInt32(dr.GetOrdinal("bizDays"))

                End If
                m_qualificationID = dr.GetInt32(dr.GetOrdinal("qualificationID"))

            End While
            dr.Close()

            ' EventLog.WriteEntry("Application", "About to create qual  " & m_qualificationID)
            m_qualification = New Qualification(m_qualificationID, Conn)

            Cmd.CommandText = "select statusid from tblTACEndStatus where isActive=1 and tacid=" & tacid
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_endStatusID.Add(dr.GetInt32(dr.GetOrdinal("statusid")))
            End While
            dr.Close()


            Cmd.CommandText = "select statusid from tblTACStartStatus where isActive=1 and tacid=" & tacid
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_startStatusID.Add(dr.GetInt32(dr.GetOrdinal("statusid")))
            End While
            dr.Close()

            Cmd.CommandText = "select statusid from tblTACExceptionStatus where isActive=1 and tacid=" & tacid
            dr = Cmd.ExecuteReader
            While dr.Read()
                m_exceptionStatusID.Add(dr.GetInt32(dr.GetOrdinal("statusid")))
            End While
            dr.Close()




        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to get timer type name:" & e.Message)

        Finally

            '   Conn.Close()
            Cmd = Nothing

            ' Conn = Nothing
        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for adding holidays
    ' exceptions
    ' ----------  ARPATECH  ----------
    Public Sub AddHolidayException(ByVal countryid As Integer, ByVal holidayid As Integer)
        If m_holidayExceptions.ContainsKey(countryid) Then
            Dim al As ArrayList = m_holidayExceptions.Item(countryid)
            If Not al.Contains(holidayid) Then
                al.Add(holidayid)
                m_holidayExceptions.Remove(countryid)
                m_holidayExceptions.Add(countryid, al)
            End If
        Else
            Dim al As New ArrayList()
            al.Add(holidayid)
            m_holidayExceptions.Add(countryid, al)
        End If
    End Sub
End Class

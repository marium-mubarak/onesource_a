' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class only contains one property of ActionRequest type.
' Inherits          :   EventArgs
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class GSTimerEventArgs
    Inherits EventArgs
    Private m_actionRequest

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Action request type
    ' ----------  ARPATECH  ----------
    Property ActionRequest() As ActionRequest
        Get
            Return m_actionRequest
        End Get
        Set(ByVal Value As ActionRequest)
            m_actionRequest = Value
        End Set
    End Property

End Class

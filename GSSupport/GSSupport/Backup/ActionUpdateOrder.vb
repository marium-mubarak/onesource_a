' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class contains only one function and multiple properties. On calling
'                       that function it creates a instance of “OrderEventArgs” class and pass it to
'                       “m_ActionRequest.GSListener.RaiseOrderEvent” function after
'                       assisgning all the the values to specific variables.
' Inherits          :   Action
' Implements        :   IActionUpdateOrder
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IActionUpdateOrder
    Property SessionTrackingNumber() As Integer
    Property OrderTrackingNumber() As Integer
    Property DestinationTrackingNumber() As Integer
    Property BuyerUserID() As Integer
    Property BuyerUserName() As String

    Property SFAID() As Integer
    Property ChangeShipped() As Boolean
    Property ChangeAccepted() As Boolean
    Property ChangeDelivered() As Boolean
    Property ChangeETA() As Boolean
    Property ChangeItemShipped() As Boolean
    Property OrderItemID() As Integer
    Property ETA() As Date

End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionUpdateOrder
    Inherits Action
    Implements IActionUpdateOrder


    Private m_orderItemID As Integer
    Private m_changeItemShipped As Boolean = False
    Private m_sessionTrackingNumber As Integer
    Private m_orderTrackingNumber As Integer
    Private m_destinationTrackingNumber As Integer
    Private m_buyerUserID As Integer
    Private m_buyerUserName As String
    Private m_buyerCustomerID As Integer
    Private m_sfaid As Integer
    Private m_changeShipped As Boolean = False
    Private m_changeDelivered As Boolean = False
    Private m_changeAccepted As Boolean = False
    Private m_chageDelivered As Boolean = False
    Private m_changeETA As Boolean = False
    Private m_eta As Date
    Private m_customerID As Integer
    Private m_currencyID As Integer
    Private m_countryID As Integer
    Private m_orderEventArgs As New OrderEventArgs()

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' order tracking number
    ' ----------  ARPATECH  ----------
    Property OrderTrackingNumber() As Integer Implements IActionUpdateOrder.OrderTrackingNumber
        Get
            Return m_orderTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_orderTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' session tracking number
    ' ----------  ARPATECH  ----------
    Property SessionTrackingNumber() As Integer Implements IActionUpdateOrder.SessionTrackingNumber
        Get
            Return m_sessionTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_sessionTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' destination tracking number
    ' ----------  ARPATECH  ----------
    Property DestinationTrackingNumber() As Integer Implements IActionUpdateOrder.DestinationTrackingNumber
        Get
            Return m_destinationTrackingNumber
        End Get
        Set(ByVal Value As Integer)
            m_destinationTrackingNumber = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' buyer user id
    ' ----------  ARPATECH  ----------
    Property BuyerUserID() As Integer Implements IActionUpdateOrder.BuyerUserID
        Get
            Return m_buyerUserID
        End Get
        Set(ByVal Value As Integer)
            m_buyerUserID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' order item ID
    ' ----------  ARPATECH  ----------
    Property OrderItemID() As Integer Implements IActionUpdateOrder.OrderItemID
        Get
            Return m_orderItemID
        End Get
        Set(ByVal Value As Integer)
            m_orderItemID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SFA ID
    ' ----------  ARPATECH  ----------
    Property SFAID() As Integer Implements IActionUpdateOrder.SFAID
        Get
            Return m_sfaid
        End Get
        Set(ByVal Value As Integer)
            m_sfaid = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' buyer user name
    ' ----------  ARPATECH  ----------
    Property BuyerUserName() As String Implements IActionUpdateOrder.BuyerUserName
        Get
            Return m_buyerUserName
        End Get
        Set(ByVal Value As String)
            m_buyerUserName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' changed shipped
    ' ----------  ARPATECH  ----------
    Property ChangeShipped() As Boolean Implements IActionUpdateOrder.ChangeShipped
        Get
            Return m_changeShipped
        End Get
        Set(ByVal Value As Boolean)
            m_changeShipped = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' change item shipped
    ' ----------  ARPATECH  ----------
    Property ChangeItemShipped() As Boolean Implements IActionUpdateOrder.ChangeItemShipped
        Get
            Return m_changeItemShipped
        End Get
        Set(ByVal Value As Boolean)
            m_changeItemShipped = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' change either accepted or rejected
    ' ----------  ARPATECH  ----------
    Property ChangeAccepted() As Boolean Implements IActionUpdateOrder.ChangeAccepted
        Get
            Return m_changeAccepted
        End Get
        Set(ByVal Value As Boolean)
            m_changeAccepted = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' change delivered status
    ' ----------  ARPATECH  ----------
    Property ChangeDelivered() As Boolean Implements IActionUpdateOrder.ChangeDelivered
        Get
            Return m_changeDelivered
        End Get
        Set(ByVal Value As Boolean)
            m_changeDelivered = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' change Estimated Time Arrival change status
    ' ----------  ARPATECH  ----------
    Property ChangeETA() As Boolean Implements IActionUpdateOrder.ChangeETA
        Get
            Return m_changeETA
        End Get
        Set(ByVal Value As Boolean)
            m_changeETA = Value
        End Set

    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' change Estimated Time Arrival 
    ' ----------  ARPATECH  ----------
    Property ETA() As Date Implements IActionUpdateOrder.ETA
        Get
            Return m_eta
        End Get
        Set(ByVal Value As Date)
            m_eta = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine that executes on
    ' any event
    ' ----------  ARPATECH  ----------
    Public Overrides Sub FireActionEvent()
        Try

            If Not IsNothing(m_actionInstanceID) Then
                m_orderEventArgs.ActionInstanceID = m_actionInstanceID
            End If

            If Not IsNothing(m_actionRequest.GSListener) Then

                Dim oea As OrderEventArgs = New OrderEventArgs()

                oea = m_orderEventArgs
                oea.SessionTrackingNumber = Me.SessionTrackingNumber
                oea.OrderTrackingNumber = Me.OrderTrackingNumber
                oea.DestinationTrackingNumber = Me.DestinationTrackingNumber
                oea.ActionRequest = m_actionRequest
                oea.BuyerUserName = Me.BuyerUserName
                oea.BuyerUserID = Me.BuyerUserID
                oea.ChangeAccepted = Me.ChangeAccepted
                oea.ChangeShipped = Me.ChangeShipped
                oea.ChangeDelivered = Me.ChangeDelivered
                oea.ChangeETA = Me.ChangeETA

                m_actionRequest.GSListener.RaiseOrderEvent(Me, oea)

            Else
                EventLog.WriteEntry("Application", "Error firing OrderUpdateEvent: m_ActionRequest.GSListener is nothing")
            End If


        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error AUO.firevent:" & ex.Message)
        End Try

    End Sub



End Class

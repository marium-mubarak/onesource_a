' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class purpose is to execute all functions like Add, Update and retrieve
'                       formulas.
' Inherits          :   ---------
' Implements        :   IFormula
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IFormula

    Function AddFormulaDetail(ByVal fd As FormulaDetail) As Integer
    Function DumpParenMatch() As String
    Function GetText() As String
    Function EvaluateToString() As String
    Function GetExpressionText() As String
    Function GetTopExpressions() As String

    Function GetExpressionTextLevel(ByVal i As Integer) As String

End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class Formula
    Implements IFormula
    Private m_formulaDetails As New ArrayList()
    Private m_ParenMatch As New Hashtable()
    Private m_formulaID As Integer = 0
    Private m_returnDataType As Integer = vbBoolean
    Private m_formulaName As New String("")
    Private m_expression As New Expression()
    Private m_topExpressions As New ArrayList()
    Public Const MAX_EXPR_LEVELS As Integer = 100

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' formula details
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
        Property FormulaDetails() As ArrayList
        Get
            Return m_formulaDetails
        End Get
        Set(ByVal Value As ArrayList)
            m_formulaDetails = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' formula name
    ' ----------  ARPATECH  ----------
    Property FormulaName() As String
        Get
            Return m_formulaName
        End Get
        Set(ByVal Value As String)
            m_formulaName = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' formula ID
    ' ----------  ARPATECH  ----------
    Property FormulaID() As Integer
        Get
            Return m_formulaID
        End Get
        ' ----------  ARPATECH  ----------
        ' A Property for Setting and Retrieving
        ' filed data type
        ' ----------  ARPATECH  ----------
        Set(ByVal Value As Integer)
            m_formulaID = Value
        End Set
    End Property




    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Function Validate() As Boolean

    End Function


    ' ----------  ARPATECH  ----------
    ' function to return formula details
    ' in string form
    ' ----------  ARPATECH  ----------
    Function GetText() As String Implements IFormula.GetText
        Dim i As Integer = 0
        Dim n As Integer = 0
        Dim s As New String("")

        For i = 0 To m_formulaDetails.Count - 1
            If Not IsNothing(m_formulaDetails.Item(i)) Then
                Dim fd As New FormulaDetail()
                fd = m_formulaDetails.Item(i)
                For n = 1 To fd.BeginClause
                    s = s & " ("
                Next


                If Not IsNothing(fd.Var1) Then

                    s = s & fd.Var1.ToString() & " "
                End If

                If Not IsNothing(fd.[Operator]) Then
                    s = s & fd.[Operator].OperatorText & " "
                End If

                If Not IsNothing(fd.Var2) Then
                    s = s & fd.Var2.ToString() & " "
                End If

                For n = 1 To fd.EndClause
                    s = s & ") "
                Next

                If Not IsNothing(fd.Conjunction) Then
                    s = s & fd.Conjunction.DisplayText & " "
                End If
            End If
        Next



        If Len(s) > 0 Then
            s = "If " & s
        End If
        Return s

    End Function





    ' ----------  ARPATECH  ----------
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Function EvaluateDate() As Date

    End Function

    ' ----------  ARPATECH  ----------
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Function EvaluateInteger() As Integer

    End Function

    ' ----------  ARPATECH  ----------
    ' function that adds formula detail
    ' and returns the count for added details
    ' ----------  ARPATECH  ----------
    Function AddFormulaDetail(ByVal fd As FormulaDetail) As Integer Implements IFormula.AddFormulaDetail
        Dim dcount As Integer = 0
        If Not IsNothing(m_formulaDetails) Then
            m_formulaDetails.Add(fd)
            dcount = m_formulaDetails.Count
        End If

        Return dcount


    End Function

    ' ----------  ARPATECH  ----------
    ' function that dumps matching parens
    ' to a arraylist and puts with enter to a String
    ' ----------  ARPATECH  ----------
    Function DumpParenMatch() As String Implements IFormula.DumpParenMatch
        Dim i As Integer
        Dim s As New String("")

        '  EventLog.WriteEntry("Application", "About to match parens")
        MatchParens()
        '  EventLog.WriteEntry("Application", "Done matching parens")


        For i = 1 To m_ParenMatch.Count
            s = s & "rParen" & i & " matches lParen " & m_ParenMatch.Item(i) & vbCrLf
        Next
        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function that checks and returns
    ' either formula is saved / updated or NOT
    ' ----------  ARPATECH  ----------
    Function Save() As Boolean

        If Not FormulaUpdateDB() And Not SaveFormulaDetails() Then
            Return False
        Else
            Return True
        End If

    End Function

    ' ----------  ARPATECH  ----------
    ' function for saving formula detail
    ' in database
    ' ----------  ARPATECH  ----------
    Function SaveFormulaDetails() As Boolean
        Dim isSubmit As Boolean = True
        If Not IsNothing(m_formulaDetails) Then
            Dim i


            ' First Update DB for formula
            Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

            Conn.Open()


            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Try
                ' First delete all existing details
                Cmd.CommandText = "delete from tblFormulaDetail where formulaid=" & CStr(m_formulaID)
                Cmd.ExecuteNonQuery()

                Cmd.CommandType = CommandType.StoredProcedure
                Cmd.CommandText = "sp_AddFormulaDetail"

                For i = 0 To m_formulaDetails.Count - 1
                    Dim fd As New FormulaDetail()
                    If Not IsNothing(m_formulaDetails.Item(i)) Then
                        Dim errstr As New String("")
                        fd = m_formulaDetails.Item(i)

                        SqlCommandBuilder.DeriveParameters(Cmd)

                        Cmd.Parameters("@formulaid").Value = m_formulaID
                        Cmd.Parameters("@formulaSeqNum").Value = i
                        Cmd.Parameters("@beginClause").Value = fd.BeginClause
                        Cmd.Parameters("@endClause").Value = fd.EndClause
                        Cmd.Parameters("@conjunctionStr").Value = fd.Conjunction.Text
                        Cmd.Parameters("@operatorStr").Value = fd.[Operator].OperatorSymbol

                        Dim f As New Field()
                        If Not IsNothing(fd.Var1) Then
                            '  If fd.Var1.GetType.Equals(f) Then
                            If fd.Var1.GetType.ToString = "GSSupport.Field" Then


                                f = fd.Var1
                                '    EventLog.WriteEntry("Application", "fieldid is " & f.FieldID)
                                Cmd.Parameters("@fieldID").Value = f.FieldID
                            End If
                        End If

                        If Not IsNothing(fd.Var2) Then
                            ' If fd.Var2.GetType.Equals(f) Then
                            '   EventLog.WriteEntry("Application", fd.Var2.GetType.ToString)
                            If fd.Var2.GetType.ToString = "GSSupport.Field" Then
                                ' EventLog.WriteEntry("Application", "vari is a field")

                                f = fd.Var2
                                Cmd.Parameters("@fieldTextValue").Value = f.FieldTextValue
                                ' EventLog.WriteEntry("Application", "id value is " & f.FieldID)

                                ' EventLog.WriteEntry("Application", "text value is " & f.FieldTextValue)

                                Cmd.Parameters("@fieldDisplayValue").Value = f.FieldDisplayValue
                                ' EventLog.WriteEntry("Application", "display value is " & f.FieldDisplayValue)
                            End If
                        End If

                        ' Output parameters
                        Cmd.Parameters("@errstr").Value = errstr
                        Cmd.Parameters("@formuladetailid").Value = fd.FormulaDetailID
                        '  EventLog.WriteEntry("Application", "ABout to save formula details")
                        If Not IsNothing(f.FieldTextValue) Then
                            Cmd.ExecuteNonQuery()
                        End If

                        '  EventLog.WriteEntry("Application", "Saved formula details:" & Cmd.Parameters("@errstr").Value)


                    End If

                Next
            Catch e As Exception
                EventLog.WriteEntry("Application", "Error Saving FormulaDetail:" & e.Message)
                isSubmit = False

            Finally
                Conn.Close()
            End Try

        End If

        Return isSubmit

    End Function


    ' ----------  ARPATECH  ----------
    ' function for updating Database
    ' for formula table and return success status
    ' ----------  ARPATECH  ----------
    Function FormulaUpdateDB() As Boolean
        Dim isSubmit As Boolean = True

        ' First Update DB for formula
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = Conn
            Cmd.CommandText = "sp_AddFormula"

            SqlCommandBuilder.DeriveParameters(Cmd)



            Cmd.Parameters("@formulaname").Value = m_formulaName

            ' Output Parameters
            Cmd.Parameters("@formulaid").Value = m_formulaID

            Cmd.ExecuteNonQuery()
            m_formulaID = Cmd.Parameters("@formulaid").Value
        Catch e As Exception
            isSubmit = False
            EventLog.WriteEntry("Application", "Error Saving Formula:" & e.Message)
        Finally
            Conn.Close()

        End Try
        Return isSubmit

    End Function

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' filed data type
    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR 
    Public Sub New()

    End Sub


    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with Formula ID argument
    ' it reads data according to formula ID
    ' from supplied SQL connection
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal formulaID As Integer, ByRef Conn As SqlConnection)
        ' Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        '   EventLog.WriteEntry("Application", "Loading formula " & formulaID)
        ' Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try

            m_formulaID = formulaID
            Dim sql As String

            sql = "select *, conjName, opSymbol, fieldDisplay from tblFormulaDetail, tblConjunctions, tblOperators, tblFields " & _
            " where tblFormulaDetail.operatorID = tblOperators.opid and tblFormulaDetail.conjunctionID = tblConjunctions.conjid and formulaid =" & formulaID & _
            " and tblFormulaDetail.fieldid = tblFields.fieldID order by formulaDetailSeqNum"
            '   EventLog.WriteEntry("Application", sql)


            Cmd.CommandText = sql


            Dim dr As SqlDataReader

            dr = Cmd.ExecuteReader
            While dr.Read()
                Dim fd As New FormulaDetail()



                fd.BeginClause = dr.GetInt32(dr.GetOrdinal("beginSubClause"))
                fd.EndClause = dr.GetInt32(dr.GetOrdinal("endSubClause"))



                Dim field1 As New Field()
                Dim field2 As New Field()

                field1.DataType = dr.GetInt32(dr.GetOrdinal("fieldDataTypeID"))
                field1.FieldID = dr.GetInt32(dr.GetOrdinal("fieldID"))
                field1.FieldDisplayName = dr.GetString(dr.GetOrdinal("fieldDisplay"))

                field2.DataType = dr.GetInt32(dr.GetOrdinal("fieldDataTypeID"))
                field2.FieldID = dr.GetInt32(dr.GetOrdinal("fieldID"))
                field2.FieldDisplayValue = dr.GetString(dr.GetOrdinal("fieldDisplayValue"))
                field2.FieldTextValue = dr.GetString(dr.GetOrdinal("fieldTextValue"))


                fd.Conjunction = New Conjunction(dr.GetString(dr.GetOrdinal("conjname")))
                fd.[Operator] = New [Operator](dr.GetString(dr.GetOrdinal("opsymbol")))
                fd.Var1 = field1
                fd.Var2 = field2

                m_formulaDetails.Add(fd)

            End While
            dr.Close()





        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading formula details for formula " & formulaID & " from DB: " & e.Message)
            Return

        Finally
            ' Conn.Close()
            '   EventLog.WriteEntry("Application", "Loaded formula " & GetText())

        End Try

    End Sub



    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with Formula ID argument
    ' it reads data according to formula ID
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal formulaID As Integer)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        '   EventLog.WriteEntry("Application", "Loading formula " & formulaID)
        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try

            m_formulaID = formulaID
            Dim sql As String

            sql = "select *, conjName, opSymbol, fieldDisplay from tblFormulaDetail, tblConjunctions, tblOperators, tblFields " & _
            " where tblFormulaDetail.operatorID = tblOperators.opid and tblFormulaDetail.conjunctionID = tblConjunctions.conjid and formulaid =" & formulaID & _
            " and tblFormulaDetail.fieldid = tblFields.fieldID order by formulaDetailSeqNum"
            '   EventLog.WriteEntry("Application", sql)


            Cmd.CommandText = sql


            Dim dr As SqlDataReader

            dr = Cmd.ExecuteReader
            While dr.Read()
                Dim fd As New FormulaDetail()



                fd.BeginClause = dr.GetInt32(dr.GetOrdinal("beginSubClause"))
                fd.EndClause = dr.GetInt32(dr.GetOrdinal("endSubClause"))



                Dim field1 As New Field()
                Dim field2 As New Field()

                field1.DataType = dr.GetInt32(dr.GetOrdinal("fieldDataTypeID"))
                field1.FieldID = dr.GetInt32(dr.GetOrdinal("fieldID"))
                field1.FieldDisplayName = dr.GetString(dr.GetOrdinal("fieldDisplay"))

                field2.DataType = dr.GetInt32(dr.GetOrdinal("fieldDataTypeID"))
                field2.FieldID = dr.GetInt32(dr.GetOrdinal("fieldID"))
                field2.FieldDisplayValue = dr.GetString(dr.GetOrdinal("fieldDisplayValue"))
                field2.FieldTextValue = dr.GetString(dr.GetOrdinal("fieldTextValue"))


                fd.Conjunction = New Conjunction(dr.GetString(dr.GetOrdinal("conjname")))
                fd.[Operator] = New [Operator](dr.GetString(dr.GetOrdinal("opsymbol")))
                fd.Var1 = field1
                fd.Var2 = field2

                m_formulaDetails.Add(fd)

            End While
            dr.Close()





        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading formula details for formula " & formulaID & " from DB: " & e.Message)
            Return

        Finally
            Conn.Close()
            '   EventLog.WriteEntry("Application", "Loaded formula " & GetText())

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Function EvaluateBoolean() As Boolean
        Dim b As Boolean

    End Function

    ' Break up formula into expressions of matching parentheticals
    ' ----------  ARPATECH  ----------
    ' sub routine for matching parents
    ' and breakup formula according to expressions
    ' ----------  ARPATECH  ----------
    Sub MatchParens()
        CheckAllParens()
        m_ParenMatch.Clear()
        ' Find first R Paren
        Dim i As Integer
        Dim n As Integer
        Dim z As Integer
        Dim lCount As Integer
        Dim rCount As Integer
        Dim h As New Hashtable()
        m_ParenMatch = h
        Dim topexp As New Expression()
        m_expression = topexp

        Try

            For i = 0 To m_formulaDetails.Count - 1
                '       MessageBox.Show("i is NOW " & i)
                Dim fd As New FormulaDetail()
                If Not IsNothing(FormulaDetails.Item(i)) Then

                    fd = FormulaDetails.Item(i)
                    lCount = lCount + fd.BeginClause
                    rCount = rCount + fd.EndClause
                    '      MessageBox.Show("lcount is " & lCount)
                    '      MessageBox.Show("rcount is " & rCount)

                    If fd.EndClause > 0 Then


                        ' Break down into match r-l parens... get next n l-parens
                        Dim detailCount As Integer = 1
                        Dim lBack = 0
                        Dim rBack = 0

                        n = fd.EndClause

                        z = fd.BeginClause
                        '   MessageBox.Show("n BEFORE LOOP is " & n)
                        '   MessageBox.Show("z BEFORE is " & z)
                        ' Go backward to find matching lParen
                        '   Dim thisExp As New Expression(fd)
                        While n > 0

                            For z = z To 0 Step -1
                                '       MessageBox.Show("z in loop is " & z)
                                '       MessageBox.Show("Checking hash for lvalue " & lCount - lBack)
                                If Not h.ContainsValue(lCount - lBack) Then
                                    '    MessageBox.Show("Adding to hash is " & rCount - (n - 1) & ", " & lCount - lBack)
                                    m_ParenMatch.Add(rCount - (n - 1), lCount - lBack)
                                    n = n - 1
                                    rBack = rBack + 1
                                    '           MessageBox.Show("n is " & n)
                                    If n = 0 Then
                                        Exit While
                                    End If

                                End If

                                z = z - 1
                                lBack = lBack + 1
                                '    EventLog.WriteEntry("Application", "1")
                            Next

                            ' Out of lParens, go to previous detail
                            If i - detailCount >= 0 Then
                                Dim fdPrevious As New FormulaDetail()
                                fdPrevious = FormulaDetails.Item(i - detailCount)


                                z = fdPrevious.BeginClause

                                detailCount = detailCount + 1
                                '    MessageBox.Show("z is NOW " & z)
                            End If


                        End While

                        '   MessageBox.Show("m_expression is NOW " & m_expression.GetText)


                    End If
                End If
            Next
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error matching parens: " & e.Message)
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY FUNCTION
    ' ----------  ARPATECH  ----------
    Function GetFormulaSection(ByVal lParen As Integer, ByVal rParen As Integer) As ArrayList


    End Function

    ' ----------  ARPATECH  ----------
    ' function for getting expressions
    ' as array list
    ' ----------  ARPATECH  ----------
    Function GetExpressions() As ArrayList
        Dim a As New ArrayList()
        If GetLCount() <> GetRCount() Then
            '        EventLog.WriteEntry("Application", "Error: Cannot GetMatchingParens because there are " & GetLCount() & " left parens and " & GetRCount() & " right parens")
            Return a
        End If
        Dim e As New Expression()
        m_expression = e
        MatchParens()

        ' First for left parens

        Dim i As Integer = 0

        For i = 1 To m_ParenMatch.Count
            Dim rParen = i
            Dim lMatch = m_ParenMatch.Item(i)
            Dim thisExp As New Expression()

            ' thisExp = GetExpressionFromFormula(lMatch, rParen)

            '  Dim level As Integer = (ht.Count - lParen)



        Next





        Return a

    End Function

    ' ----------  ARPATECH  ----------
    ' function for getting R count
    ' but it dont have a return type
    ' ----------  ARPATECH  ----------
    Function GetRCount()

        Dim i As Integer = 0
        Dim rcount As Integer = 0

        For i = 0 To m_formulaDetails.Count - 1
            Dim fd As New FormulaDetail()
            If Not IsNothing(m_formulaDetails.Item(i)) Then
                fd = m_formulaDetails.Item(i)

                rcount = rcount + fd.EndClause

            End If

        Next

        Return rcount
    End Function

    ' ----------  ARPATECH  ----------
    ' function for getting least count
    ' but it dont have a return type
    ' ----------  ARPATECH  ----------
    Function GetLCount()

        Dim i As Integer = 0
        Dim lcount As Integer = 0

        For i = 0 To m_formulaDetails.Count - 1
            Dim fd As New FormulaDetail()
            If Not IsNothing(m_formulaDetails.Item(i)) Then
                fd = m_formulaDetails.Item(i)

                lcount = lcount + fd.BeginClause

            End If

        Next

        Return lcount
    End Function

    ' ----------  ARPATECH  ----------
    ' function for getting matching
    ' parent levels in formula details
    ' ----------  ARPATECH  ----------
    Function GetMatchingRParen(ByVal lparen As Integer) As Integer
        Dim lParenCount As Integer = 0
        Dim rParenCount As Integer = 0
        Dim i As Integer
        For i = 0 To m_formulaDetails.Count - 1
            Dim fd As New FormulaDetail()
            If Not IsNothing(m_formulaDetails.Item(i)) Then
                fd = m_formulaDetails.Item(i)
                lParenCount = lParenCount + fd.BeginClause
                rParenCount = rParenCount = fd.EndClause
                If lParenCount = lparen Then
                    Return rParenCount
                ElseIf lParenCount > lparen Then



                End If

            End If


        Next
    End Function

    ' ----------  ARPATECH  ----------
    ' function for getting expressions
    ' text only
    ' ----------  ARPATECH  ----------
    Function GetExpressionText() As String Implements IFormula.GetExpressionText
        ParseExpressions()

        '    Return m_expression.GetText
        Dim s As New String("")
        Dim i As Integer
        s = s & m_topExpressions.Count & " Top Expressions:" & vbCrLf

        For i = 0 To m_topExpressions.Count - 1

            If Not IsNothing(m_topExpressions(i)) Then
                Dim e As New Expression()
                e = m_topExpressions(i)

                s = s & e.GetText
            End If

        Next
        Return s

    End Function

    ' ----------  ARPATECH  ----------
    ' function for getting expressions
    ' text level
    ' ----------  ARPATECH  ----------
    Function GetExpressionTextLevel(ByVal i As Integer) As String Implements IFormula.GetExpressionTextLevel
        Dim s As New String("")
        Dim e As New Expression()
        ' e = GetExpressionFromFormula(e, i)
        '  e = ParseExpressions(e, i)
        'e = ParseExpressions()
        s = e.GetText()
        Return s
    End Function

    ' If the very top level is simple put parens around it
    ' ----------  ARPATECH  ----------
    ' sub routine for checking parent
    ' level and putting parent around
    ' on simple levels
    ' ----------  ARPATECH  ----------
    Sub CheckAllParens()
        If Not IsNothing(m_formulaDetails) Then
            If Not IsNothing(m_formulaDetails.Item(0)) Then
                Dim fd As FormulaDetail = m_formulaDetails.Item(0)

                Dim fd2 As FormulaDetail = m_formulaDetails.Item(m_formulaDetails.Count - 1)
                Console.WriteLine("begin is " & fd.BeginClause)
                If fd.BeginClause = 0 AndAlso Not IsNothing(fd2) Then
                    fd.BeginClause = 1
                    fd2.EndClause = fd2.EndClause + 1
                End If

            End If

        End If
    End Sub



    ' ----------  ARPATECH  ----------
    ' sub routine for parsing expressions
    ' ----------  ARPATECH  ----------
    Sub ParseExpressions(Optional ByVal l As Integer = 1)
        Dim rCount
        Dim lCount
        Dim exLevel As Integer = 0
        Dim n As Integer
        Dim i As Integer
        Dim lastConj As Conjunction
        Dim firstStep As Integer = -1
        Dim lastStep As Integer = -1
        Dim stepsInExpression As New Hashtable()

        Try


            ' Dim newEx As New Expression()
            Dim topcount As Integer = 0

            ' Dim thisTopExpression As New Expression()
            m_expression = New Expression()

            'Console.WriteLine("before loop l is " & l)
            ' If there are no parens it's all one big expression
            If m_ParenMatch.Count = 0 Then

                m_expression.AddSubExpressionThroughTop(m_formulaDetails, 0, topcount)
                '     m_expression.AddSubExpression(m_formulaDetails, 1, topcount)
                '    m_expression.CleanExpression()
                '     m_topExpressions.Add(newEx)
                Exit Sub
            End If

            ' Else go through formula details getting  each r - l Paren expression
            While l <= m_ParenMatch.Count
                If l > MAX_EXPR_LEVELS Then
                    Exit Sub
                End If

                '    Console.WriteLine("l is " & l)

                rCount = 0
                lCount = 0

                ' Get the formula steps these parens enclose
                For i = 0 To m_formulaDetails.Count - 1

                    lastConj = Nothing

                    '  Console.WriteLine("Looking for " & m_ParenMatch(l) & " lparens in " & i)
                    If Not IsNothing(m_formulaDetails.Item(i)) Then

                        Dim fd As New FormulaDetail()
                        fd = m_formulaDetails.Item(i)
                        rCount = rCount + fd.EndClause
                        '    MessageBox.Show("rCount is is " & rCount)
                        lCount = lCount + fd.BeginClause
                        '    MessageBox.Show("lCount is is " & lCount)
                        If lCount >= m_ParenMatch(l) And firstStep < 0 Then

                            firstStep = i
                            '     MessageBox.Show("Setting firststep to " & firstStep)
                        End If
                        If rCount >= l And lastStep < 0 Then
                            lastStep = i
                            '   MessageBox.Show("Setting lastStep to " & lastStep)
                            exLevel = lCount - (rCount - (rCount - l))
                        End If

                        If firstStep >= 0 And lastStep >= 0 Then

                            Exit For
                        End If
                    End If
                Next

                Dim fds As New ArrayList()

                ' Now create expressions from steps
                For i = (firstStep) To lastStep
                    ' Create an array of formula details that haven't been added

                    ' Only add steps to expression that haven't been added
                    If Not stepsInExpression.ContainsKey(i) Then

                        Dim fd As New FormulaDetail()
                        fd = m_formulaDetails.Item(i)
                        '    Console.WriteLine("fd is " & fd.GetText)
                        fds.Add(fd)
                        stepsInExpression.Add(i, fd)
                        If Not IsNothing(fd.Conjunction) Then
                            lastConj = fd.Conjunction
                        End If
                    End If

                Next

                If fds.Count > 0 Then

                    '       Console.WriteLine("Adding fd array of size " & fds.Count & " at level " & exLevel & " col " & topcount)
                    '  thisTopExpression.AddSubExpression(fds, exLevel)
                    '  m_expression.AddSubExpression(fds, exLevel)
                    m_expression.AddSubExpressionThroughTop(fds, exLevel, topcount)
                    If exLevel = 0 Then
                        topcount = topcount + 1
                    End If

                    '  Console.WriteLine("this top expression NOW has " & m_expression.m_subExps.Count & " subexpressions")

                End If


                If exLevel = 0 Then
                    '    If Not IsNothing(lastConj) And Not l >= m_ParenMatch.Count Then
                    '       thisTopExpression.Conjunction = lastConj
                    '    End If

                    '     m_topExpressions.Add(thisTopExpression)
                    '      thisTopExpression.CleanExpression()
                    '    MessageBox.Show("Adding to topex: " & thisTopExpression.GetText())
                    '      MessageBox.Show("5")
                    '     thisTopExpression = New Expression()
                End If
                '    MessageBox.Show("5a")
                l = l + 1
                ' Console.WriteLine("this top exprssion is now " & thisTopExpression.GetText())
                firstStep = -1
                lastStep = -1
            End While

            '   MessageBox.Show("6")
            ' Last top expression has no conjunction
            ' If Not IsNothing(m_topExpressions.Item(m_topExpressions.Count - 1)) Then
            ' Dim e As New Expression()
            '  e = m_topExpressions.Item(m_topExpressions.Count - 1)
            '  e.Conjunction = Nothing
            '  End If
            m_expression.RemoveTrailingConj()
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error parsing expressions:" & e.Message)
        End Try


        Console.WriteLine("******************************** Done parsinng Exps with " & m_expression.GetText())
        m_expression.DumpGraph()
    End Sub

    ' ----------  ARPATECH  ----------
    ' function to get TOP expressions
    ' ----------  ARPATECH  ----------
    Function GetTopExpressions() As String Implements IFormula.GetTopExpressions
        Dim i As Integer
        Dim s As New String("")
        For i = 0 To m_topExpressions.Count - 1
            Dim e As New Expression()
            If Not IsNothing(m_topExpressions(i)) Then
                e = m_topExpressions(i)

                s = s & e.GetText()
            End If

        Next

        Return s
    End Function

    ' ----------  ARPATECH  ----------
    ' function to evaluate Object type
    ' ----------  ARPATECH  ----------
    Function Evaluate() As Object

        '   EventLog.WriteEntry("Application", "Evaluating formula")
        MatchParens()
        '    EventLog.WriteEntry("Application", "Matched Parens")
        ParseExpressions()
        '    EventLog.WriteEntry("Application", "Parsed Expressions")
        Dim i As Integer
        Dim lastConj As Conjunction
        Dim o As Object
        If Not IsNothing(m_expression) Then
            ' EventLog.WriteEntry("Application", "Formula evaluating expression " & m_expression.GetText())
            o = m_expression.Evaluate()
            '   If m_formulaID = 577 Then
            '      EventLog.WriteEntry("Application", "Formula " & m_formulaID & " evaluating expression " & m_expression.GetText() & " is " & o.ToString())
            ' End If

            '     Console.WriteLine("Formula evaluating expression " & m_expression.GetText() & " is " & o.ToString())

            Return o
        Else
            EventLog.WriteEntry("Application", "m_expression IS NULL")
        End If



    End Function

    ' ----------  ARPATECH  ----------
    ' function to evaluate string
    ' ----------  ARPATECH  ----------
    Function EvaluateToString() As String Implements IFormula.EvaluateToString
        Dim s As New String("")
        Dim i As Integer


        Dim o As Object = Evaluate()
        If Not IsNothing(o) Then
            s = GetValText(o)
        End If

        Return s
    End Function


End Class

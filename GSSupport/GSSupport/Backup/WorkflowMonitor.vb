' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is a monitor for Workflow and consists of functions to process
'                       workflow, update status, on completion of action request etc.
' Inherits          :   ---------
' Implements        :   IMQUtils, IWorkflowMonitor, IGSEventListener
' Friends           :   Workflow
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
Imports GSSupport
Imports System.Messaging
Imports MSMQ
Imports MSMQ.MQACCESS
Imports MSMQ.MQSHARE
Imports GSSupport.GSAppGlobals
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text


<Serializable()> Public Class WorkflowMonitor
    Implements IMQUtils
    Implements IWorkflowMonitor
    Implements IGSEventListener

    Friend Workflow


    Private m_workflowInstanceID As Integer = 0
    Private m_workflow As Workflow
    Private m_actionRequest As New ActionRequest()
    Private m_currentStep As Integer = 0
    Private m_mqLabel As String
    Private m_gsListener As GSEventListener

    ' ----------  ARPATECH  ----------
    '       SAME CLASS INTERFACE
    ' ----------  ARPATECH  ----------
    Public Interface IWorkflowMonitor

        Property WorkflowInstanceID() As Integer

    End Interface


    ' ----------  ARPATECH  ----------
    ' CONSTRUCTOR with AR and WF arguments
    ' ----------  ARPATECH  ----------
    Public Sub New(ByVal ar As ActionRequest, ByVal wf As Workflow)
        '   EventLog.WriteEntry("Application", "Starting workflow " & wf.WorkflowID)

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Dim isSubmitted As Boolean = True
        m_workflow = wf

        m_actionRequest = ar

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try

            ' First update db to get wfinstanceid
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_LaunchWorkflow"


            SqlCommandBuilder.DeriveParameters(Cmd)

            Cmd.Parameters("@arid").Value = ar.ActionRequestID
            Cmd.Parameters("@workflowid").Value = wf.WorkflowID
            If Not IsNothing(ar.SubmitUserID) Then
                Cmd.Parameters("@userid").Value = ar.SubmitUserID
            End If

            Cmd.Parameters("@workflowInstanceID").Value = m_workflowInstanceID

            Cmd.ExecuteNonQuery()

            m_workflowInstanceID = Cmd.Parameters("@workflowInstanceID").Value

            ' Next send to queue
            Dim m As New MSMQMessage()

            m = CreateMQMessage()

            SendToQ(WorkflowInProgressQ, m)

            ProcessWorkflow()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Failed to create Workflow Monitor:" & e.Message)
        Finally
            'Cmd = Nothing
            Conn.Close()

            '  RestartListener()
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' workflow instance ID
    ' ----------  ARPATECH  ----------
    Property WorkflowInstanceID() As Integer Implements IWorkflowMonitor.WorkflowInstanceID
        Get
            Return m_workflowInstanceID
        End Get
        Set(ByVal Value As Integer)
            m_workflowInstanceID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' MQ Label
    ' ----------  ARPATECH  ----------
    Property MQLabel() As String
        Get
            Return m_mqLabel
        End Get
        Set(ByVal Value As String)
            m_mqLabel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' routine for processing workflow 
    ' ----------  ARPATECH  ----------
    Sub ProcessWorkflow()
        Dim i As Integer = 0
        Dim steps As Integer = 0


        For i = m_currentStep To m_workflow.GetStepsCount() - 1
            Dim a As Action
            Dim q As Qualification
            If Not IsNothing(m_workflow.m_workflowSequence(i)) Then
                Dim ws As WorkflowStep = m_workflow.m_workflowSequence(i)
                If Not IsNothing(ws.Qualification) AndAlso ws.Qualification.QualificationID > 0 Then
                    ' Evaluate the action request if a qualification exists
                    ' return number of executed steps if it isn't true.
                    If Not ws.Qualification.Evaluate(m_actionRequest) Then
                        UpdateWorkflowStatus(True, "Step " & i & " for workflow instance " & m_workflowInstanceID & "returned false")
                    End If
                End If
                If Not IsNothing(ws.Action) AndAlso ws.Action.ActionID > 0 Then

                    ' Create AR for action; copy all vals except a few...
                    Dim ar As New ActionRequest()
                    ar = m_actionRequest
                    ' ar.ActionRequestID = m_actionRequest.ActionRequestID
                    ar.WorkflowInstance = Me
                    ar.Qualify = False
                    ar.GenerateTicket = False
                    ar.CategoryName = "System"
                    ar.CompletedAction = Nothing
                    ar.IsCompletedAction = False
                    ar.AddFieldByDisplay("Ticket CSM Changed", "False", "False")
                    ar.AddFieldByDisplay("Ticket GSP Changed", "False", "False")
                    ar.AddFieldByDisplay("Ticket Status Changed", "False", "False")
                    ar.AddFieldByDisplay("Ticket Priority Changed", "False", "False")
                    ar.AddFieldByDisplay("Ticket Escalation Changed", "False", "False")
                    ar.AddFieldByDisplay("Ticket Comment Added", "False", "False")




                    If Not IsNothing(ws.Action.ActionID) Then
                        ar.RequestedActionID = ws.Action.ActionID
                    End If

                    ar.Summary = "System generated action request for workflow " & m_workflow.WorkflowID


                    If Not ar.Submit() Then
                        EventLog.WriteEntry("Application", "Error submitting AR from workflow")
                    Else


                        m_currentStep = m_currentStep + 1

                        ' If there's more steps 
                        If m_currentStep < m_workflow.GetStepsCount() - 1 Then

                            UpdateWorkflowStatus(False, "Qualification #" & i & " returned true; Action Request submitted.")
                            ' If cancel on error exit and wait for action to complete.
                            If m_workflow.CancelOnError Then
                                Exit Sub
                            End If
                        Else
                            ' Last step; we're done.
                            UpdateWorkflowStatus(True, "Qualification #" & i & " returned true; Action Request submitted.  Workflow complete.")
                            If (m_workflow.MakeInactiveOnCompletion) Then
                                m_workflow.IsActive = False
                            End If

                            GSSupportManager.WFPostProc(m_mqLabel, m_workflow.MakeInactiveOnCompletion, m_workflow.MQLabel)
                        End If

                    End If
                End If

            End If


        Next


    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for updating workflow status
    ' in database
    ' ----------  ARPATECH  ----------
    Sub UpdateWorkflowStatus(ByVal isEnd As Boolean, ByVal comments As String)
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn

        Try

            ' First update db to get wfinstanceid
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_UpdateWorkflowInstance"


            SqlCommandBuilder.DeriveParameters(Cmd)


            Cmd.Parameters("@userid").Value = m_actionRequest.SubmitUserID
            Cmd.Parameters("@workflowInstanceID").Value = m_workflowInstanceID
            Cmd.Parameters("@seqnum").Value = m_currentStep

            If isEnd Then
                Cmd.Parameters("@endDate").Value = Now()

            End If
            If Not IsNothing(comments) AndAlso Len(comments) > 0 Then
                Cmd.Parameters("@comments").Value = comments
            End If

            Cmd.ExecuteNonQuery()

        Catch e As Exception
            EventLog.WriteEntry("Application", "Error updating workflow status:" & e.Message)
        Finally
            Cmd = Nothing
            Conn.Close()

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' function for writing queue message
    ' ----------  ARPATECH  ----------
    Function CreateMQMessage() As MSMQMessage Implements IMQUtils.CreateMQMessage
        Dim l As String
        Dim m As New MSMQMessage()

        Try
            Dim t As Type = GetType(GSSupport.WorkflowMonitor)


            Dim serializer As New XmlSerializer(t)

            Dim objxml As New String("")

            Dim ns As New XmlSerializerNamespaces()

            ' ns.Add("GSSupport", "https://onesource.global-serve.com")

            Dim ms As MemoryStream = New MemoryStream()

            Dim writer As New XmlTextWriter(ms, New UTF8Encoding())

            ' Serialize using the XmlTextWriter.

            serializer.Serialize(writer, Me, ns)

            Dim arChars As Char()
            ReDim arChars(ms.Length)

            ms.ToArray.CopyTo(arChars, 0)

            objxml = New String(arChars)


            writer.Close()
            'Dim mexml As String = ObjToXML(Me)
            l = Now() & "&AR=" & CStr(m_actionRequest.ActionRequestID) & "&WF=" & CStr(m_workflow.WorkflowID & "&WFIID=" & m_workflowInstanceID)

            m.Label = l
            m_mqLabel = l

            m.Body = objxml


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error converting Workflow to message: " & e.Message & "," & e.Source & "," & e.StackTrace)
        End Try
        Return m


    End Function



    ' ----------  ARPATECH  ----------
    ' EMPTY Routine
    ' ----------  ARPATECH  ----------
    Public Sub OnActionRequestEvent(ByVal sender As Object, ByVal e As ActionRequestEventArgs) Implements IGSEventListener.OnActionRequestEvent
        '   EventLog.WriteEntry("Application", "RECEIVED AR EVENT")
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine the executes on 
    ' action complete event
    ' ----------  ARPATECH  ----------
    Public Sub OnActionCompleteEvent(ByVal sender As Object, ByVal e As ActionCompleteEventArgs) Implements IGSEventListener.OnActionCompleteEvent
        '   EventLog.WriteEntry("Application", "RECEIVED AC EVENT")
        Try
            Dim am As ActionMonitor
            am = e.ActionMonitor


            If Not IsNothing(am.Action.ActionRequest.WorkflowInstance.WorkflowInstanceID) AndAlso am.Action.ActionRequest.WorkflowInstance.WorkflowInstanceID = m_workflowInstanceID Then

                If am.Action.IsError Then
                    UpdateWorkflowStatus(True, "Action " & am.Action.ActionID & " returned error:" & am.Action.ErrString)

                    If m_workflow.CancelOnError Then
                        UpdateWorkflowStatus(True, "Workflow is configured to terminate on error.")
                        m_currentStep = m_workflow.GetStepsCount - 1

                    Else
                        UpdateWorkflowStatus(False, "Workflow is configured to continue on error.")
                    End If

                Else
                    Dim s As New String("successfully")
                    If am.Action.ReturnsValue AndAlso Not IsNothing(am.Action.ReturnField) Then
                        s = am.Action.ReturnField.FieldTextValue
                    End If


                    If m_currentStep < m_workflow.GetStepsCount - 1 Then
                        UpdateWorkflowStatus(True, "Action " & am.Action.ActionID & " returned " & s & "; workflow resuming.")
                        ProcessWorkflow()
                    Else
                        UpdateWorkflowStatus(True, "Action " & am.Action.ActionID & " returned " & s & "; workflow completed.")

                        If (m_workflow.MakeInactiveOnCompletion) Then
                            m_workflow.IsActive = False
                        End If

                    End If

                End If


            End If



        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error WF receiving postproc AM:" & ex.Message)

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for writeing log on
    ' GS timer event
    ' ----------  ARPATECH  ----------
    Public Sub OnGSTimerEvent(ByVal sender As Object, ByVal e As GSTimerEventArgs) Implements IGSEventListener.OnGSTimerEvent
        EventLog.WriteEntry("Application", "RECEIVED GSTimer EVENT")
    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for writeing log on
    ' ticketing event
    ' ----------  ARPATECH  ----------
    Public Sub OnTicketEvent(ByVal sender As Object, ByVal e As TicketEventArgs) Implements IGSEventListener.OnTicketEvent
        EventLog.WriteEntry("Application", "RECEIVED TICKET EVENT")
    End Sub


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GS Listener
    ' ----------  ARPATECH  ----------
    <System.Xml.Serialization.XmlIgnore()> _
        Public Property GSListener() As GSEventListener Implements IGSEventListener.GSListener
        Get
            Return m_gsListener
        End Get
        Set(ByVal Value As GSEventListener)
            m_gsListener = Value
            '     AddHandler m_gsListener.TicketEvent, New TicketEventHandler(AddressOf Me.OnTicketEvent)
            AddHandler m_gsListener.ActionCompleteEvent, New ActionCompleteEventHandler(AddressOf Me.OnActionCompleteEvent)

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' Empty sub routine
    ' ----------  ARPATECH  ----------
    Public Sub OnOrderEvent(ByVal sender As Object, ByVal e As OrderEventArgs) Implements IGSEventListener.OnOrderEvent

    End Sub


    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub
End Class

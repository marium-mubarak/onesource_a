' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class is almost same like ActionUpdateOrder class. Its function is
'                       also to update the Ticket information in DB with multiple properties. But
'                       this class also contains some other functions for loading default values,
'                       changing ticket status etc.
' Inherits          :   Action
' Implements        :   IActionUpdateTicket
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

Imports System.Data.SqlClient
Imports System
Imports System.ComponentModel
Imports Microsoft.VisualBasic

' ----------  ARPATECH  ----------
'       SAME CLASS INTERFACE
' ----------  ARPATECH  ----------
<System.Runtime.InteropServices.ComVisible(True)> Public Interface IActionUpdateTicket

    Property TicketEventArgs() As TicketEventArgs
    Property CSRID() As Integer
    Property TktEscalationLevel() As Integer
    Property PriorityID() As Integer
    Property RequestID() As Integer
    Property SFAID() As Integer
    Property StatusID() As Integer
    Property UserID() As Integer
    Property Comments() As String
    Property GSTicketID() As Integer
    Property TypeID() As Integer
    Property SuspendTACTimer() As Boolean
    Property StartTACTimer() As Boolean
    Property OpenSLA() As Integer
    Property MinsToVio() As Integer
    Property IsBreach() As Boolean
    Property IsException() As Boolean
    Property IsClosedWithinSLA() As Boolean
    Property IsNewStatus() As Boolean
    Property IsNewPriority() As Boolean
    Property IsNewEscalation() As Boolean
    Property IsNewCSR() As Boolean
    Property IsNewType() As Boolean
    Property IsNewGSP() As Boolean
    Property IsNewAppt() As Boolean
    Property IsAddComment() As Boolean
    Property ApptDate() As Date
    Property CustomerTicketID() As String
    Property SFATicketID() As String


    Sub AddStatusTarget(ByVal statusid As Integer, ByVal target As DateTime, ByVal actual As DateTime)

    Sub AddStatusTargetChange(ByVal statusid As Integer, ByVal oldtarget As DateTime, ByVal newtarget As DateTime)
    Sub AddStatusActualChange(ByVal statusid As Integer, ByVal oldactual As DateTime, ByVal newactual As DateTime)

    Sub LoadDefaultValues(ByVal ticketid As Integer)





End Interface

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionUpdateTicket
    Inherits Action
    Implements IActionUpdateTicket


    Private m_statusID As Integer = 0
    Private m_escalationLevel As Integer = 0
    Private m_typeID As Integer = 0
    Private m_priorityID As Integer = 0
    Private m_csrID As Integer = 0
    Private m_sfaID As Integer = 0
    Private m_ticketID As Integer = 0
    Private m_arID As Integer = 0
    Private m_comments As New String("")
    Private m_userID As Integer = 0
    Private m_ticketEventArgs As New TicketEventArgs()
    Private m_timeZoneID As Integer = -1
    Private m_suspendTACTimer As Boolean = False
    Private n_isNewType As Boolean = False
    Private m_startTACTimer As Boolean = False
    Private m_openSLA As Integer = 0
    Private m_isBreach As Boolean = False
    Private m_isException As Boolean = False
    Private m_isClosedWithinSLA As Boolean = False
    Private m_isInProgress As Boolean = False
    Private m_minutesToBreach As Integer = -1
    Private m_statuses As New ArrayList()
    Private m_statusTargets As New ArrayList()
    Private m_statusActuals As New ArrayList()
    ' Private m_statusTargetChanges As New Hashtable()
    ' Private m_statusActualChanges As New Hashtable()
    Private m_isNewSFA As Boolean = False
    Private m_isNewEscalation As Boolean = False
    Private m_isNewCSR As Boolean = False
    Private m_isNewStatus As Boolean = False
    Private m_isNewType As Boolean = False
    Private m_isNewPriority As Boolean = False
    Private m_isAddComment As Boolean = False
    Private m_apptDate As Date
    Private m_isNewAppt As Boolean = False
    Private m_sfaTicketID As New String("")
    Private m_customerTicketID As New String("")
    Private m_assetSerial As New String("")
    Private m_addressText As New String("")
    Private m_submittingUserPhone As New String("")
    Private m_affectedContactPhone As New String("")

    ' Private m_gsSupportManager As GSSupportManager

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' comments variable
    ' ----------  ARPATECH  ----------
    Property Comments() As String Implements IActionUpdateTicket.Comments
        Get
            Return m_comments
        End Get
        Set(ByVal Value As String)
            m_comments = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' customer ticket variable
    ' ----------  ARPATECH  ----------
    Property CustomerTicketID() As String Implements IActionUpdateTicket.CustomerTicketID
        Get
            Return m_customerTicketID
        End Get
        Set(ByVal Value As String)
            m_customerTicketID = Value
            m_ticketEventArgs.CustomerTicketID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SFA ticket ID
    ' ----------  ARPATECH  ----------
    Property SFATicketID() As String Implements IActionUpdateTicket.SFATicketID
        Get
            Return m_sfaTicketID
        End Get
        Set(ByVal Value As String)
            m_sfaTicketID = Value
            m_ticketEventArgs.SFATicketID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' appointment date variable
    ' ----------  ARPATECH  ----------
    Property ApptDate() As Date Implements IActionUpdateTicket.ApptDate
        Get
            Return m_apptDate
        End Get
        Set(ByVal Value As Date)
            If m_ticketID > 0 AndAlso ((IsNothing(m_apptDate) And Not IsNothing(Value)) OrElse m_apptDate <> Value) Then
                m_ticketEventArgs.ApptDate = Value
                m_ticketEventArgs.IsNewAppt = True

            End If
            m_apptDate = Value

        End Set
    End Property


    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' priority variable
    ' ----------  ARPATECH  ----------
    Property PriorityID() As Integer Implements IActionUpdateTicket.PriorityID
        Get
            Return m_priorityID
        End Get
        Set(ByVal Value As Integer)
            If m_ticketID > 0 AndAlso m_priorityID <> Value Then
                m_ticketEventArgs.NewPriorityID = Value
                m_ticketEventArgs.IsPriorityChange = True
            End If
            m_priorityID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Service Level Agreement ID variable
    ' ----------  ARPATECH  ----------
    Property OpenSLA() As Integer Implements IActionUpdateTicket.OpenSLA
        Get
            Return m_openSLA
        End Get
        Set(ByVal Value As Integer)
            m_openSLA = Value
            m_ticketEventArgs.OpenSLA = m_openSLA
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket Esclation ID variable
    ' ----------  ARPATECH  ----------
    Property TktEscalationLevel() As Integer Implements IActionUpdateTicket.TktEscalationLevel
        Get
            Return m_escalationLevel
        End Get
        Set(ByVal Value As Integer)
            If m_ticketID > 0 AndAlso m_escalationLevel <> Value Then
                m_ticketEventArgs.NewEscalationLevel = Value
                m_ticketEventArgs.IsEscalationChange = True
            End If
            m_escalationLevel = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' minutes to breach SLA
    ' ----------  ARPATECH  ----------
    Property MinsToVio() As Integer Implements IActionUpdateTicket.MinsToVio
        Get
            Return m_minutesToBreach
        End Get
        Set(ByVal Value As Integer)
            m_minutesToBreach = Value
            m_ticketEventArgs.MinutesToBreach = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new appointment status
    ' ----------  ARPATECH  ----------
    Property IsNewAppt() As Boolean Implements IActionUpdateTicket.IsNewAppt
        Get
            Return m_isNewAppt
        End Get
        Set(ByVal Value As Boolean)
            m_isNewAppt = Value
            m_ticketEventArgs.IsNewAppt = True
            '     EventLog.WriteEntry("Application", "AUT.m_ticetEvntArgs=" & m_ticketEventArgs.IsNewAppt)

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket type id
    ' ----------  ARPATECH  ----------
    Property TypeID() As Integer Implements IActionUpdateTicket.TypeID
        Get
            Return m_typeID
        End Get
        Set(ByVal Value As Integer)
            If Value > 0 Then
                Dim o As New Object()
                '   Dim e As New Exception()


                '    EventLog.WriteEntry("Application", "Setting TYPE ID" & e.StackTrace)
                If m_ticketID > 0 AndAlso m_typeID > 0 AndAlso m_typeID <> Value Then
                    EventLog.WriteEntry("Application", "old type was " & m_typeID & ", new type is " & Value)


                    ' Change ticket status if necessary

                    ' First see if there are different statuses for this request type
                    Dim tmpi As Integer = GSSupportManager.GetSingleDBValueInt("Select count(arTypeID) from tblTicketStatus where companyid in (select IsNull(companyid,0) from tblTicketStatus where tktStatusID=" & Me.StatusID & ")")
                    If (tmpi > 1) Then
                        ' see if there's a ticket status for this request type with the same name as the old status
                        Dim tmpstatus As Integer = GSSupportManager.GetSingleDBValueInt("Select tktStatusID from tblTicketStatus where upper(tktStatusName) in (select upper(tktStatusName) from tblTicketStatus where tktStatusID=" & Me.StatusID & ") and arTypeID=" & Value & " and companyid in (select companyid from tblTicketStatus where tktstatusid=" & Me.StatusID & ")")
                        If tmpstatus < 1 Then
                            ' otherwise set to minumum value for this request type
                            tmpstatus = GSSupportManager.GetSingleDBValueInt("select top 1 tktStatusID from tblTicketStatus where arTypeID=" & Value & " and companyid in (select IsNull(companyid,0) from tblTicketStatus where tktStatusID=" & Me.StatusID & ") order by statusIntValue")
                        End If
                        If (tmpstatus > 0) Then
                            Me.StatusID = tmpstatus
                        End If
                        '
                    End If


                    ' Check for new SLAs
                    Me.IsNewType = True
                    m_ticketEventArgs.NewTypeID = Value
                    m_ticketEventArgs.IsTypeChange = True
                    m_typeID = Value


                End If
            End If


            '    RecheckSLAs()
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' TAC timer status
    ' ----------  ARPATECH  ----------
    Property SuspendTACTimer() As Boolean Implements IActionUpdateTicket.SuspendTACTimer
        Get
            Return m_suspendTACTimer
        End Get
        Set(ByVal Value As Boolean)
            m_suspendTACTimer = Value
            If m_suspendTACTimer Then
                m_ticketEventArgs.TimerStopOverride = True
                m_ticketEventArgs.TimerStopOverrideTACID = m_openSLA
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' exception / error raised status
    ' ----------  ARPATECH  ----------
    Property IsException() As Boolean Implements IActionUpdateTicket.IsException
        Get
            Return m_isException
        End Get
        Set(ByVal Value As Boolean)
            m_isException = Value
            If m_isException Then
                m_ticketEventArgs.IsException = True
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine for adding
    ' target status 
    ' ----------  ARPATECH  ----------
    Public Sub AddStatusTarget(ByVal statusid As Integer, ByVal target As DateTime, ByVal actual As DateTime) Implements IActionUpdateTicket.AddStatusTarget
        Dim b As Boolean = False
        Dim i As Integer = 0
        For i = 0 To m_statuses.Count - 1
            If CInt(m_statuses(i)) = statusid Then
                m_statusTargets(i) = target
                m_statusActuals(i) = actual
                b = True
                Exit For
            End If

        Next
        If Not b Then
            m_statuses.Add(statusid)
            m_statusTargets.Add(target)
            m_statusActuals.Add(actual)

        End If

        m_ticketEventArgs.Statuses = m_statuses
        m_ticketEventArgs.StatusActuals = m_statusActuals
        m_ticketEventArgs.StatusTargets = m_statusTargets

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' customer service representative new / old status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsNewCSR() As Boolean Implements IActionUpdateTicket.IsNewCSR
        Get
            Return m_isNewCSR
        End Get
        Set(ByVal Value As Boolean)
            m_isNewCSR = Value
            m_ticketEventArgs.IsCsrAssigned = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' GSP new / old status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsNewGSP() As Boolean Implements IActionUpdateTicket.IsNewGSP
        Get
            Return m_isNewSFA
        End Get
        Set(ByVal Value As Boolean)
            m_isNewSFA = Value
            m_ticketEventArgs.IsFscAssigned = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' escalation new / old status 
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsNewEscalation() As Boolean Implements IActionUpdateTicket.IsNewEscalation
        Get
            Return m_isNewEscalation
        End Get
        Set(ByVal Value As Boolean)
            m_isNewEscalation = Value
            m_ticketEventArgs.IsEscalationChange = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' priority new / old status 
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsNewPriority() As Boolean Implements IActionUpdateTicket.IsNewPriority
        Get
            Return m_isNewPriority
        End Get
        Set(ByVal Value As Boolean)
            m_isNewPriority = Value
            m_ticketEventArgs.IsPriorityChange = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' add comments status 
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsAddComment() As Boolean Implements IActionUpdateTicket.IsAddComment
        Get
            Return m_isAddComment
        End Get
        Set(ByVal Value As Boolean)
            m_isAddComment = Value
            m_ticketEventArgs.IsAddComment = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' status new / old status 
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsNewStatus() As Boolean Implements IActionUpdateTicket.IsNewStatus
        Get
            Return m_isNewStatus
        End Get
        Set(ByVal Value As Boolean)
            m_isNewStatus = Value
            m_ticketEventArgs.IsStatusChange = True
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' new type new / old status
    ' ----------  ARPATECH  ----------
    <DefaultValueAttribute(False)> Property IsNewType() As Boolean Implements IActionUpdateTicket.IsNewType
        Get
            Return m_isNewType
        End Get
        Set(ByVal Value As Boolean)
            m_isNewType = Value
            m_ticketEventArgs.IsTypeChange = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA closed status 
    ' ----------  ARPATECH  ----------
    Property IsClosedWithinSLA() As Boolean Implements IActionUpdateTicket.IsClosedWithinSLA
        Get
            Return m_isClosedWithinSLA
        End Get
        Set(ByVal Value As Boolean)
            m_isClosedWithinSLA = Value
            If m_isClosedWithinSLA Then
                m_ticketEventArgs.IsClosedWithinSLA = True
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SLA breach status 
    ' ----------  ARPATECH  ----------
    Property IsBreach() As Boolean Implements IActionUpdateTicket.IsBreach
        Get
            Return m_isBreach
        End Get
        Set(ByVal Value As Boolean)
            m_isBreach = Value
            If m_isBreach Then
                m_ticketEventArgs.IsBreach = True
            End If
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' for stating stoping TAC time 
    ' ----------  ARPATECH  ----------
    Property StartTACTimer() As Boolean Implements IActionUpdateTicket.StartTACTimer
        Get
            Return m_startTACTimer
        End Get
        Set(ByVal Value As Boolean)
            m_startTACTimer = Value
            If m_startTACTimer Then
                m_ticketEventArgs.TimerStartOverride = True
                m_ticketEventArgs.TimerStartOverrideTACID = m_openSLA
            End If

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' arguments for ticketing process
    ' ----------  ARPATECH  ----------
    '  <System.Xml.Serialization.XmlIgnore()> _
    Property TicketEventArgs() As TicketEventArgs Implements IActionUpdateTicket.TicketEventArgs
        Get
            Return m_ticketEventArgs
        End Get
        Set(ByVal Value As TicketEventArgs)
            m_ticketEventArgs = Value

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket ID and loding its default values
    ' ----------  ARPATECH  ----------
    Property GSTicketID() As Integer Implements IActionUpdateTicket.GSTicketID
        Get
            Return m_ticketID
        End Get
        Set(ByVal Value As Integer)
            m_ticketID = Value
            m_ticketEventArgs.GSTicketID = Value
            m_timeZoneID = m_ticketEventArgs.TimeZoneID
            LoadDefaultValues(m_ticketID)

        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' user ID 
    ' ----------  ARPATECH  ----------
    Property UserID() As Integer Implements IActionUpdateTicket.UserID
        Get
            Return m_userID
        End Get
        Set(ByVal Value As Integer)
            m_userID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' request ID
    ' ----------  ARPATECH  ----------
    Property RequestID() As Integer Implements IActionUpdateTicket.RequestID
        Get
            Return m_arID
        End Get
        Set(ByVal Value As Integer)
            m_arID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' CSR ID
    ' ----------  ARPATECH  ----------
    Property CSRID() As Integer Implements IActionUpdateTicket.CSRID
        Get
            Return m_csrID
        End Get
        Set(ByVal Value As Integer)
            If m_ticketID > 0 AndAlso m_csrID <> Value Then
                m_ticketEventArgs.NewCSRID = Value
                m_ticketEventArgs.IsCsrAssigned = True
            End If
            m_csrID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' SFA ID
    ' ----------  ARPATECH  ----------
    Property SFAID() As Integer Implements IActionUpdateTicket.SFAID
        Get
            Return m_sfaID
        End Get
        Set(ByVal Value As Integer)
            If m_ticketID > 0 AndAlso m_sfaID <> Value Then

                Me.IsNewGSP = True

            End If
            m_sfaID = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' ticket status ID 
    ' ----------  ARPATECH  ----------
    Property StatusID() As Integer Implements IActionUpdateTicket.StatusID
        Get
            Return m_statusID
        End Get
        Set(ByVal Value As Integer)
            If m_ticketID > 0 And m_statusID <> Value Then
                m_ticketEventArgs.NewStatusID = Value
                m_ticketEventArgs.IsStatusChange = True
            End If
            m_statusID = Value
            ' Change actual dates for this status and all < statuses
            RefreshStatusTargetsFromDB()
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' sub routine that refreshes target ID
    ' from database
    ' ----------  ARPATECH  ----------
    Private Sub RefreshStatusTargetsFromDB()
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Dim sql As String
        Conn.Open()

        Try
            m_statuses = New ArrayList()
            m_statusTargets = New ArrayList()
            m_statusActuals = New ArrayList()



            sql = "select tktstatusid, targetdate, actualdate from tblTktStatusSchedule where  " & _
            "  ticketid =" & m_ticketID & " and (tktstatusid =  " & m_statusID & " or  " & _
          "  tktstatusid in (select tktstatusid from tblTicketStatus where statusIntValue  " & _
           "  <= (select statusIntValue from tblTicketStatus where tktStatusID =  " & m_statusID & " )))"

            '   EventLog.WriteEntry("Application", sql)

            Dim Cmd As New SqlCommand()
            Cmd.Connection = Conn
            Dim dr As SqlDataReader
            Cmd.CommandText = sql
            dr = Cmd.ExecuteReader


            While dr.Read()
                Dim target As Date
                Dim actual As Date
                Dim status As Integer

                target = New Date(1900, 1, 1)
                actual = New Date(1900, 1, 1)
                If Not dr.IsDBNull(dr.GetOrdinal("targetdate")) Then
                    target = dr.GetDateTime(dr.GetOrdinal("targetdate"))
                    ' Only care if there's a target date
                    If Not dr.IsDBNull(dr.GetOrdinal("actualdate")) Then
                        actual = dr.GetDateTime(dr.GetOrdinal("actualdate"))
                    End If
                    '     EventLog.WriteEntry("Application", "Adding status " & dr.GetInt32(dr.GetOrdinal("tktstatusid")) & ", actual " & actual)
                    AddStatusTarget(dr.GetInt32(dr.GetOrdinal("tktstatusid")), target, actual)
                End If

            End While
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error loading status targets from DB: " & e.Message)

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine that executes on this class
    ' instance execution
    ' ----------  ARPATECH  ----------
    Public Overrides Sub Execute()

        If m_ticketID = 0 And m_arID > 0 Then
            m_ticketID = GetTicketIDFromARID(m_arID)
        End If

        ' If we still don't have a ticket ID might as well bail...
        If m_ticketID = 0 Then
            m_isError = True
            m_errString = "Unable to update ticket without valid ticket ID."
            EventLog.WriteEntry("Application", "Unable to execute ticket update without ticket id")
            Exit Sub
        End If

        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())

        Conn.Open()

        Dim Cmd As New SqlCommand()

        Cmd.Connection = Conn
        Try
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "sp_UpdateTicketStatus"

            SqlCommandBuilder.DeriveParameters(Cmd)
            Cmd.Parameters("@ticketid").Value = m_ticketID


            Dim dd As Date = Me.ActionRequest.Manager.ConvertTime(Now(), ServerTimeZone, GSSupportManager.GetTimeZoneVBConst(Me.m_timeZoneID))

            Cmd.Parameters("@logdatelocal").Value = dd


            If m_csrID > 0 Then
                Cmd.Parameters("@csrid").Value = m_csrID
            End If

            If m_sfaID > 0 Then
                Cmd.Parameters("@sfaid").Value = m_sfaID
            End If

            If m_statusID > 0 Then
                Cmd.Parameters("@tktstatusid").Value = m_statusID
            End If

            If m_escalationLevel > 0 Then
                Cmd.Parameters("@escalationlevel").Value = m_escalationLevel
            End If

            If m_priorityID > 0 Then
                Cmd.Parameters("@priorityid").Value = m_priorityID
            End If

            If m_userID > 0 Then
                Cmd.Parameters("@userid").Value = m_userID
            End If

            If Len(m_comments) > 0 Then
                Cmd.Parameters("@comments").Value = m_comments
            End If

            Cmd.ExecuteNonQuery()

            ' Manually start/stop timer
            ' If m_suspendTACTimer > 0 Then
            '     Cmd.Parameters("@timerstopoverride").Value = True
            ' End If

            '  If m_startTACTimer > 0 Then
            '     Cmd.Parameters("@timerstartoverride").Value = True
            '  End If
            Me.FireActionEvent()


        Catch e As Exception
            EventLog.WriteEntry("Application", "Error deleting updating ticket: " & e.Message)


        Finally
            Conn.Close()

        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' funtion to retrieve Ticket ID from
    ' action request ID
    ' ----------  ARPATECH  ----------
    Function GetTicketIDFromARID(ByVal arid As Integer) As Integer
        Dim ticketid As Integer = 0
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim sql As String
        sql = "select ticketid from tblActionRequest where actionrequestid=" & arid
        Dim f As Field

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Dim dr As SqlDataReader
        Cmd.CommandText = sql
        dr = Cmd.ExecuteReader
        Try

            While dr.Read()

                ticketid = dr.GetInt32(dr.GetOrdinal("ticketid"))
            End While
        Catch e As Exception

            EventLog.WriteEntry("Application", "Error getting ticketID for AR " & arid & ": " & e.Message)

        Finally
            Conn.Close()
        End Try
        Return ticketid
    End Function

    ' ----------  ARPATECH  ----------
    ' routine that executes on any
    ' Action Event execution
    ' ----------  ARPATECH  ----------
    Public Overrides Sub FireActionEvent()
        Try
            ' EventLog.WriteEntry("Application", "AUT firing Action Event")
            If Not IsNothing(m_actionInstanceID) Then
                m_ticketEventArgs.ActionInstanceID = m_actionInstanceID
            End If



            If Not IsNothing(m_actionRequest.GSListener) Then
                ' m_ActionRequest.GSListener.RaiseTicketEvent(Me, m_ticketEventArgs)
                ' Create new ticket event args to reflect updated ticket

                Dim tea As TicketEventArgs = New TicketEventArgs()

                tea = m_ticketEventArgs

                tea.GSTicketID = m_ticketEventArgs.GSTicketID

                ' EventLog.WriteEntry("Application", "Ticket ID is " & tea.GSTicketID)
                m_actionRequest.GSListener.RaiseTicketEvent(Me, tea)

                '   m_ActionRequest.GSListener.RaiseTicketEvent(Me, m_ticketEventArgs)
            Else
                EventLog.WriteEntry("Application", "Error firing RaiseTicketEvent: m_ActionRequest.GSListener is nothing")
            End If


        Catch ex As Exception
            EventLog.WriteEntry("Application", "Error AUT.firevent:" & ex.Message)
        End Try

    End Sub

    ' ----------  ARPATECH  ----------
    ' routine for loading default values to 
    ' memory variables
    ' ----------  ARPATECH  ----------
    Sub LoadDefaultValues(ByVal ticketid As Integer) Implements IActionUpdateTicket.LoadDefaultValues

        m_ticketID = ticketid
        Dim Conn As New SqlConnection(GSSupportManager.GetDBString())
        Conn.Open()
        Dim sql As String
        sql = "select tktTypeID, isnull(TicketContactName, '') as ticketcontactname, isnull(TicketContactPhoneVoice, '') as ticketcontactphone, isNull(artypeid, 0) as artypeid, ca.address1, ca.address2, ca.city, ca.stateprovinceregion, ca.postalcode, cou.countryname, c1.workphonenumber as submittinguserphone, c2.workphonenumber as affectedcontactphone, ass.serialnum, sfaTicketID, customerTicketID, IsNull(tktStatusID, 0) as tktStatusID, AppointmentDate, IsNull(tblTicket.priorityID, 0) as priorityID, " & _
        " isNull(sfaid, 0) as sfaid, isNull(csrid, 0) as csrid, isNull(escalationLevel, 0) as escalationLevel " & _
        " from tblTicket left join tblActionRequest ar on ar.arid = tblTicket.arid " & _
        " left join gsnetp_tbl_assets ass on ass.assetid = ar.assetid " & _
        " left join tblContact c1 on c1.userid = ar.submittedByUserID left join tblContact c2 " & _
        " on c2.contactID = ar.affectedUserContactID left join tblCustomerAddresses ca on " & _
        " ca.customeraddressid = ar.addressid left join tblCountries cou on cou.countryid = ca.countryid where ticketid=" & ticketid

        Dim Cmd As New SqlCommand()
        Cmd.Connection = Conn
        Dim dr As SqlDataReader
        Cmd.CommandText = sql
        dr = Cmd.ExecuteReader
        Try

            While dr.Read()
                m_typeID = dr.GetInt32(dr.GetOrdinal("tktTypeID"))
                m_statusID = dr.GetInt32(dr.GetOrdinal("tktStatusID"))
                m_priorityID = dr.GetInt32(dr.GetOrdinal("priorityID"))
                m_sfaID = dr.GetInt32(dr.GetOrdinal("sfaid"))
                m_csrID = dr.GetInt32(dr.GetOrdinal("csrid"))
                m_escalationLevel = dr.GetInt32(dr.GetOrdinal("escalationLevel"))
                '   m_typeID = dr.GetInt32(dr.GetOrdinal("arTypeID"))
                If Not dr.IsDBNull(dr.GetOrdinal("AppointmentDate")) AndAlso Year(dr.GetDateTime(dr.GetOrdinal("AppointmentDate"))) > 2000 Then
                    m_apptDate = dr.GetDateTime(dr.GetOrdinal("AppointmentDate"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("serialnum")) Then
                    m_assetSerial = dr.GetString(dr.GetOrdinal("serialnum"))
                    m_ticketEventArgs.AssetSerial = m_assetSerial
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("submittinguserphone")) Then
                    m_submittingUserPhone = dr.GetString(dr.GetOrdinal("submittinguserphone"))
                    m_ticketEventArgs.SubmittingUserPhone = m_submittingUserPhone
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("affectedcontactphone")) Then
                    m_affectedContactPhone = dr.GetString(dr.GetOrdinal("affectedcontactphone"))
                    m_ticketEventArgs.AffectedContactPhone = m_affectedContactPhone
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("ticketcontactname")) Then
                    m_ticketEventArgs.TicketContactName = dr.GetString(dr.GetOrdinal("ticketcontactname"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("ticketcontactphone")) Then
                    m_ticketEventArgs.TicketContactPhone = dr.GetString(dr.GetOrdinal("ticketcontactphone"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("sfaTicketID")) Then
                    Me.SFATicketID = dr.GetString(dr.GetOrdinal("sfaTicketID"))
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("address1")) Then
                    m_addressText = dr.GetString(dr.GetOrdinal("address1")) & vbCrLf
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("address2")) Then
                    m_addressText = m_addressText & dr.GetString(dr.GetOrdinal("address2")) & vbCrLf
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("city")) Then
                    m_addressText = m_addressText & dr.GetString(dr.GetOrdinal("city")) & " "
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("stateprovinceregion")) Then
                    m_addressText = m_addressText & dr.GetString(dr.GetOrdinal("stateprovinceregion")) & " "
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("postalcode")) Then
                    m_addressText = m_addressText & dr.GetString(dr.GetOrdinal("postalcode")) & " "
                End If

                If Not dr.IsDBNull(dr.GetOrdinal("countryname")) Then
                    m_addressText = m_addressText & dr.GetString(dr.GetOrdinal("countryname"))
                End If

                m_ticketEventArgs.AddressText = m_addressText

                If Not dr.IsDBNull(dr.GetOrdinal("customerTicketID")) Then
                    Me.CustomerTicketID = dr.GetString(dr.GetOrdinal("customerTicketID"))
                End If

            End While
        Catch e As Exception

            EventLog.WriteEntry("Application", "Error loading default values for ticket" & ticketid & ": " & e.Message)

        Finally
            Conn.Close()
        End Try


    End Sub

    ' ----------  ARPATECH  ----------
    ' EMPTY CONSTRUCTOR
    ' ----------  ARPATECH  ----------
    Public Sub New()

    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' escalation new / old status 
    ' ----------  ARPATECH  ----------
    Public Sub AddStatusTargetChange(ByVal statusid As Integer, ByVal oldtarget As DateTime, ByVal newtarget As DateTime) Implements IActionUpdateTicket.AddStatusTargetChange
        Dim statusname As String
        Dim statuschange As String


        statusname = GSSupportManager.GetSingleDBValueString("select tktstatusname from tblTicketStatus where tktstatusid=" & statusid)

        statuschange = "Target date for status " & statusname & " changed from "

        If IsNothing(oldtarget) OrElse Year(oldtarget) < 2000 Then
            statuschange = statuschange & " (nothing) "
        Else
            statuschange = statuschange & CStr(oldtarget)
        End If

        statuschange = statuschange & " to "

        If IsNothing(newtarget) OrElse Year(newtarget) < 2000 Then
            statuschange = statuschange & " (nothing) "
        Else
            statuschange = statuschange & CStr(newtarget)
        End If
        statuschange = statuschange & "." & vbCrLf

        '  m_statusTargetChanges.Add(statusname, statuschange)
        m_ticketEventArgs.StatusTargetChangedDates.Add(statuschange)


    End Sub

    ' ----------  ARPATECH  ----------
    ' sub routine for changing status
    ' of any ticket
    ' ----------  ARPATECH  ----------
    Public Sub AddStatusActualChange(ByVal statusid As Integer, ByVal oldactual As DateTime, ByVal newactual As DateTime) Implements IActionUpdateTicket.AddStatusActualChange
        Dim statusname As String
        Dim statuschange As String

        statusname = GSSupportManager.GetSingleDBValueString("select tktstatusname from tblTicketStatus where tktstatusid=" & statusid)

        statuschange = "Actual date for status " & statusname & " changed from "

        If IsNothing(oldactual) OrElse Year(oldactual) < 2000 Then
            statuschange = statuschange & " (nothing) "
        Else
            statuschange = statuschange & CStr(oldactual)
        End If

        statuschange = statuschange & " to "

        If IsNothing(newactual) OrElse Year(newactual) < 2000 Then
            statuschange = statuschange & " (nothing) "
        Else
            statuschange = statuschange & CStr(newactual)
        End If

        statuschange = statuschange & "." & vbCrLf

        '  m_statusActualChanges.Add(statusname, statuschange)
        m_ticketEventArgs.StatusActualChangedDates.Add(statuschange)

    End Sub



End Class

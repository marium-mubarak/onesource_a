' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This small class contains few properties and 2-3 functions for workshift�s
'                       small processes.
' Inherits          :   ---------
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class Workshift

    Private m_startTime As Date
    Private m_endTime As Date

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' start time as date
    ' ----------  ARPATECH  ----------
    Public Property StartTime() As Date
        Get
            Dim d As New Date(Now.Year, Now.Month, Now.Day, m_startTime.Hour, m_startTime.Minute, 0)
            Return d
        End Get
        Set(ByVal Value As Date)
            m_startTime = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' end time as date
    ' ----------  ARPATECH  ----------
    Public Property EndTime() As Date
        Get
            Dim d As New Date(Now.Year, Now.Month, Now.Day, m_endTime.Hour, m_endTime.Minute, 0)
            Return d
        End Get
        Set(ByVal Value As Date)
            m_endTime = Value
        End Set
    End Property

    ' ----------  ARPATECH  ----------
    ' function for assigning start date time
    ' ----------  ARPATECH  ----------
    Function GetStartDateTime(ByVal localdate As Date)
        Dim d As New Date(localdate.Year, localdate.Month, localdate.Day, m_startTime.Hour, m_startTime.Minute, 0)
        Return d

    End Function

    ' ----------  ARPATECH  ----------
    ' function for assigning end date time
    ' ----------  ARPATECH  ----------
    Function GetEndDateTime(ByVal localdate As Date)
        Dim d As New Date(localdate.Year, localdate.Month, localdate.Day, m_endTime.Hour, m_endTime.Minute, 0)
        Return d

    End Function

    ' ----------  ARPATECH  ----------
    ' function for retrieving total minutes
    ' ----------  ARPATECH  ----------
    Function GetTotalMinutes() As Integer
        Dim i As Integer
        i = DateDiff(DateInterval.Minute, Me.StartTime, Me.EndTime)
        Return i
    End Function

End Class

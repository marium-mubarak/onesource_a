' ****************** ARPATECH (General Description) ******************
' Program Type      :   Class
' Description       :   This class can execute any given process and waits until that process is
'                       completed. It contains only one EXECUTE function and one String
'                       property for passing the process name to execute.
' Inherits          :   Action
' Implements        :   ---------
' Friends           :   ---------
' ****************** ARPATECH (General Description) ******************

<Serializable(), System.Runtime.InteropServices.ComVisible(True)> Public Class ActionCommandExec
    Inherits Action

    Private m_cmdString As String
    Private m_outputText As String

    ' ----------  ARPATECH  ----------
    ' sub routine for executing command
    ' ----------  ARPATECH  ----------
    Public Overrides Sub Execute()

        Try
            '     EventLog.WriteEntry("Application", "Starting execute")
            Dim psCmd As New ProcessStartInfo()
            psCmd.FileName = Environ("ComSpec")

            psCmd.Arguments = "/c " & m_cmdString

            psCmd.RedirectStandardInput = False
            psCmd.RedirectStandardOutput = True

            psCmd.UseShellExecute = False

            Dim procCmd = Process.Start(psCmd)  'Launch Process

            'Read in Results & Write to current cmd session
            m_outputText = procCmd.StandardOutput.ReadToEnd

            procCmd.WaitForExit()
            '  EventLog.WriteEntry("Application", m_cmdString & "=" & m_outputText)
            Me.IsError = False
            Me.ErrString = m_outputText
        Catch e As Exception
            EventLog.WriteEntry("Application", "Error executing ActionCommandExec: " & e.Message)
            Me.ErrString = e.Message
            Me.IsError = True
        Finally
            If m_actionInstanceID > 0 Then
                ActionMonitor.LogActionEvent(m_actionInstanceID, "COMPLETE", m_outputText, 0)
            End If

        End Try
    End Sub

    ' ----------  ARPATECH  ----------
    ' A Property for Setting and Retrieving
    ' Command as STRING
    ' ----------  ARPATECH  ----------
    Property CommandString() As String
        Get
            Return m_cmdString
        End Get
        Set(ByVal Value As String)
            m_cmdString = Value
        End Set
    End Property

End Class

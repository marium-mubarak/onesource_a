﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace OneSourceAdminCore.Helper
{
    public static class CollectionExtensions
    {
        public static DataTable ToDataTable<T>(this List<T> items)
        {
            var dataTable = new DataTable(typeof(T).Name);

            try
            {
                //Get all the properties
                var Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (var prop in Props)
                {
                    //Defining type of data column gives proper data table 
                    var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) 
                        ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name, type);
                }

                foreach (var item in items)
                {
                    var values = new object[Props.Length];

                    for (var i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "CollectionExtensions.ToDataTable");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "CollectionExtensions.ToDataTable");
                }
            }

            return dataTable;
        }


        public static List<T> ToList<T>(this DataTable dt)
        {
            List<T> data = null;

            try
            {
                data = new List<T>();

                foreach (DataRow row in dt.Rows)
                {
                    if (row[0].ConvertObjectToString() == string.Empty) continue;
                    var item = GetItem<T>(row);
                    data.Add(item);
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "CollectionExtensions.ToList");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "CollectionExtensions.ToList");
                }
            }

            return data;
        }


        public static T GetItem<T>(this DataRow dr)
        {
            var obj = Activator.CreateInstance<T>();

            try
            {
                var temp = typeof(T);

                foreach (DataColumn column in dr.Table.Columns)
                {
                    foreach (var pro in temp.GetProperties())
                    {

                        if (pro.Name == column.ColumnName)
                        {
                            pro.SetValue(obj, dr[column.ColumnName].ToString() != "" ? dr[column.ColumnName] : null,
                                null);
                        }
                        else
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "CollectionExtensions.GetItem");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "CollectionExtensions.GetItem");
                }
            }

            return obj;
        }

    }
}

﻿using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.Mvc;

namespace OneSourceAdminCore.Helper
{
    public static class StaticHelper
    {
        public static string RenderViewToString(System.Web.Mvc.ControllerContext context, string viewPath, object model = null, bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
            {
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            }
            else
            {
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);
            }

            if (viewEngineResult == null)
            {
                throw new FileNotFoundException("View cannot be found.");
            }

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view, context.Controller.ViewData, context.Controller.TempData, sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result.Trim();
        }
        public static void LogException(string errorMessage, string methodName)
        {
            var path = HttpContext.Current.Server.MapPath("~/Exception/AllExceptions.txt");

            using (var streamWriter = File.AppendText(path))
            {
                var currentDateTime = DateTime.Now;
                streamWriter.Write("\n \r Log Entry : ");
                streamWriter.WriteLine("{0} {1}", currentDateTime.ToLongTimeString(), currentDateTime.ToLongDateString() + ": ");
                streamWriter.WriteLine(string.Format("Exception From Service in Method : \"" + methodName) + "\"");
                streamWriter.WriteLine(errorMessage);
                streamWriter.WriteLine("<<------------------------------->>");
            }
        }


        public static string GetParameter(string url, string paramName)
        {
            var parameter = string.Empty;

            if (string.IsNullOrEmpty(url)) return parameter;

            var myUri = new Uri(url);
            parameter = HttpUtility.ParseQueryString(myUri.Query).Get(paramName);

            return parameter;
        }

        public static void SendEmailMessage(string mailto, string mailFrom, string mailCc, string mailBcc, string mailBody, string mailSubject)
        {
            try
            {

                var smtpHost = ConfigurationManager.AppSettings["mailServer"];
                //   int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
                //string userName = Convert.ToString(ConfigurationManager.AppSettings["UserName"]);
                //string password = Convert.ToString(ConfigurationManager.AppSettings["Password"]);

                var emailClient = new SmtpClient
                {
                    Host = smtpHost,
                    Port = 25,
                    UseDefaultCredentials = true
                    //,
                    // EnableSsl = true,
                    // DeliveryMethod = SmtpDeliveryMethod.Network,
                    // Credentials = new NetworkCredential(userName, password)
                };

                var mail = new MailMessage();

                // mail.To.Add(new MailAddress(mailto));

                if (!string.IsNullOrWhiteSpace(mailto))
                {
                    var emailTo = mailto.Trim().Split(';');

                    foreach (var item in emailTo.Distinct())
                    {
                        mail.To.Add(new MailAddress(item));
                    }
                }

                if (!string.IsNullOrWhiteSpace(mailBcc))
                {
                    var emailBcc = mailBcc.Trim().Split(';');

                    foreach (var item in emailBcc.Distinct())
                    {
                        mail.Bcc.Add(new MailAddress(item));
                    }
                }

                if (!string.IsNullOrWhiteSpace(mailCc))
                {
                    var emailCc = mailCc.Trim().Split(';');

                    foreach (var item in emailCc.Distinct())
                    {
                        if (mailto != null && !mailto.Contains(item.Trim()))
                        {
                            mail.CC.Add(new MailAddress(item));
                        }
                    }
                }


                mail.From = new MailAddress(mailFrom);
                mail.Body = mailBody;
                mail.Subject = mailSubject;
                mail.IsBodyHtml = false;

                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", string.Format("-->>>>>GLOBALSERVE SENDING EMAIL<<<<<--{0}", ex));

                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", string.Format("------INNER EX:{0}", ex.InnerException));
                }
            }
        }


        public static void SendEmailMessageWithAttachment(string mailto, string mailFrom, string mailCc, string mailBcc, string mailBody, string mailSubject, string fullPath, string fileName)
        {
            try
            {
                var smtpHost = ConfigurationManager.AppSettings["mailServer"];
                //   int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
                //string userName = Convert.ToString(ConfigurationManager.AppSettings["UserName"]);
                //string password = Convert.ToString(ConfigurationManager.AppSettings["Password"]);

                var emailClient = new SmtpClient
                {
                    Host = smtpHost,
                    Port = 25,
                    UseDefaultCredentials = true
                    //,
                    // EnableSsl = true,
                    // DeliveryMethod = SmtpDeliveryMethod.Network,
                    // Credentials = new NetworkCredential(userName, password)
                };

                var mail = new MailMessage();

                // mail.To.Add(new MailAddress(mailto));

                if (!string.IsNullOrWhiteSpace(mailto))
                {
                    var emailTo = mailto.Trim().Split(';');

                    foreach (var item in emailTo.Distinct())
                    {
                        mail.To.Add(new MailAddress(item));
                    }
                }

                if (!string.IsNullOrWhiteSpace(mailBcc))
                {
                    var emailBcc = mailBcc.Trim().Split(';');

                    foreach (var item in emailBcc.Distinct())
                    {
                        mail.Bcc.Add(new MailAddress(item));
                    }
                }

                if (!string.IsNullOrWhiteSpace(mailCc))
                {
                    var emailCc = mailCc.Trim().Split(';');

                    foreach (var item in emailCc.Distinct())
                    {
                        if (mailto != null && !mailto.Contains(item.Trim()))
                        {
                            mail.CC.Add(new MailAddress(item));
                        }
                    }
                }


                mail.From = new MailAddress(mailFrom);

                mail.Body = mailBody;
                mail.Subject = mailSubject;
                mail.IsBodyHtml = false;

                var contentType = new ContentType();
                contentType.MediaType = MediaTypeNames.Application.Octet;
                contentType.Name = fileName;
                mail.Attachments.Add(new Attachment(fullPath, contentType));

                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", string.Format("-->>>>>GLOBALSERVE SENDING EMAIL<<<<<--{0}", ex));

                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", string.Format("------INNER EX:{0}", ex.InnerException));
                }
            }
        }

        public static void DownloadExcel(this HttpResponseBase response, GridView gridView, string fileName, string contentType, string charSet = "")
        {
            response.ClearContent();
            response.Buffer = true;
            response.AddHeader("content-disposition", string.Format("attachment;filename = {0}", fileName));
            response.ContentType = contentType;
            response.Charset = charSet;

            using (var sw = new StringWriter())
            {
                using (var htw = new HtmlTextWriter(sw))
                {
                    gridView.RenderControl(htw);
                    response.Output.Write(sw.ToString());
                    response.Flush();
                    response.End();
                }
            }
        }
    
    }
}
﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace OneSourceAdminCore.Helper
{
    /// <summary>
    /// Aurangzeb Khan
    /// 6-11-2018
    /// Modified for part detail and bundle report
    /// </summary>
    public static class ReportHelper
    {

        public static void ExportToExcel(DataTable dt, string reportName, HttpResponseBase response, int expirationTime = 10)
        {
            try
            {
                var filename = reportName.Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");

                var gridView = new GridView();
                gridView.AllowPaging = false;
                gridView.DataSource = dt;
                gridView.DataBind();

                if (reportName == "Shipped Orders" || reportName == "Catalog Setup"
                    || reportName == "Customer Product by OEM" || reportName == "EOL Products"
                    || reportName == "Manufacturer Setup" || reportName == "SFA Address"
                    || reportName == "Catalog Category by Customer" || reportName == "Catalog by Country"
                    || reportName == "YTD Report" || reportName == "Raw Order Detail"
                    || reportName == "UnInvoiced Order Detail" || reportName == "Invoiced Order Detail"
                    || reportName == "Customer Buyer Address Association Detail" || reportName == "Quote Report"
                    || reportName == "Asset By Customer"
                    || reportName == "Revenue by Customer" || reportName == "Spend By supplier"
                    || reportName == "Projected Revenue by customer" || reportName == "Spend revenue by customer"
                    || reportName== "Old Quote Response" || reportName == "New Quote Response"
                    || reportName == "New Quote Order Conversion" || reportName == "Old Quote Order Conversion"
                    || reportName == "Saved Baskets" || reportName == "In Progress Shipped Revenue"
                    || reportName == "JDE Traxx Orders" || reportName == "Distribution Uplift by OEM"
                    || reportName == "Expiring Products" || reportName == "Pending Tickets" 
                    || reportName == "Cancelled Orders") 
                {
                    ExportGridViewToExcel(response, filename, expirationTime, gridView);
                }
                else
                {
                    ExportToFormattedExcelInBulk(response, filename, gridView);
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "ExportToExcel");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "ExportToExcel");
                }
            }
        }


        private static void ExportToFormattedExcelInBulk(HttpResponseBase response, string filename, GridView gridView)
        {
            try
            {
                FormatGridView(filename, gridView);

                // Clear all the content from the current response
                response.ClearContent();

                var cookie = new HttpCookie("excelDownloadStatus", "Complete");
                cookie.Expires = DateTime.Now.AddSeconds(10);
                response.Cookies.Add(cookie);

                // set the header
                response.AddHeader("content-disposition", string.Format("attachment;fileName={0}.xls", filename));
                response.Charset = "utf-8";
                response.ContentType = "application/vnd.xls";
                response.ContentEncoding = Encoding.UTF8;
                response.Buffer = false;

                using (var sw = new StreamWriter(response.OutputStream, Encoding.UTF8))
                {
                    using (var htw = new HtmlTextWriter(sw))
                    {
                        gridView.RenderControl(htw);
                        response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "ExportToFormattedExcelInBulk");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "ExportToFormattedExcelInBulk");
                }
            }
        }


        private static void ExportDataTableToExcel(HttpResponseBase response, string fileName, DataTable dt)
        {
            try
            {
                response.ClearContent();

                var cookie = new HttpCookie("excelDownloadStatus", "Complete");
                cookie.Expires = DateTime.Now.AddSeconds(10);
                response.Cookies.Add(cookie);

                response.AddHeader("content-disposition", string.Format("attachment;fileName={0}.xls", fileName));
                response.Charset = "utf-8";
                response.ContentType = "application/vnd.ms-excel";
                response.ContentEncoding = Encoding.UTF8;
                response.Buffer = false;

                var tab = string.Empty;

                foreach (DataColumn dc in dt.Columns)
                {
                    response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }

                response.Write("\n");

                foreach (DataRow dr in dt.Rows)
                {
                    tab = "";

                    for (var i = 0; i < dt.Columns.Count; i++)
                    {
                        response.Write(tab + dr[i]);
                        tab = "\t";
                    }

                    response.Write("\n");
                }

                response.End();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "ExportDataTableToExcel");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "ExportDataTableToExcel");
                }
            }
        }


        private static void ExportGridViewToExcel(HttpResponseBase response, string fileName, int expirationTime, GridView gridView)
        {
            try
            {
                response.ClearContent();

                var cookie = new HttpCookie("excelDownloadStatus", "Complete");
                cookie.Expires = DateTime.Now.AddSeconds(expirationTime);
                response.Cookies.Add(cookie);

                response.AddHeader("content-disposition", string.Format("attachment;fileName={0}.xls", fileName));
                response.Charset = "utf-8";
                response.ContentType = "application/vnd.ms-excel";
                response.ContentEncoding = Encoding.UTF8;
                response.Buffer = false;

                var stringWriter = new StringWriter();
                var hTextWriter = new HtmlTextWriter(stringWriter);
                gridView.RenderControl(hTextWriter);
                response.Write(stringWriter.ToString());
                response.End();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "ExportGridViewToExcel");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "ExportGridViewToExcel");
                }
            }
        }


        private static void FormatGridView(string filename, GridView gridView)
        {
            try
            {
                for (var i = 0; i < gridView.Rows.Count; i++)
                {
                    var row = gridView.Rows[i];

                    if (filename == "BundlesAndAssociatedParts")
                    {
                        gridview_RowDataBoundForBundleReport(gridView, new GridViewRowEventArgs(row));
                    }
                    else if (filename == "PartDetailReport")
                    {
                        gridview_RowDataBoundPartDetail(gridView, new GridViewRowEventArgs(row));
                    }
                    else
                    {
                        gridview_RowDataBound(gridView, new GridViewRowEventArgs(row));
                    }

                    foreach (TableCell cell in gridView.Columns)
                    {
                        cell.Attributes.CssStyle["text-align"] = "center";
                        cell.Wrap = true;
                    }
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "FormatGridView");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "FormatGridView");
                }
            }
        }


        public static void gridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Attributes.Add("style", "white-space:normal;word-break:break-all;word-wrap:break-word;");//

                    //Work here for comments
                    if (e.Row.Cells[i].Text.Contains("&nbsp;"))
                    {
                        e.Row.Cells[i].Text = " ";
                    }
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "gridview_RowDataBound");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "gridview_RowDataBound");
                }
            }
        }


        /// <summary>
        /// Aurangzeb Khan
        /// 5-11-2018
        /// Implementation for row data bound of bundle report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void gridview_RowDataBoundForBundleReport(object sender, GridViewRowEventArgs e)
        {
            try
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Attributes.Add("style", "white-space:normal;word-break:break-all;word-wrap:break-word;");

                    //Work here for comments
                    if (e.Row.Cells[i].Text.Contains("&nbsp;"))
                    {
                        e.Row.Cells[i].Text = " ";
                    }

                    if (e.Row.Cells[0].Text.Trim() != "")
                    {
                        e.Row.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#E6E6E6");
                    }
                    else
                    {
                        e.Row.Cells[i].Attributes.Add("style", "font-weight:bold;");
                    }
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "gridview_RowDataBoundForBundleReport");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "gridview_RowDataBoundForBundleReport");
                }
            }
        }


        /// <summary>
        /// Aurangzeb Khan
        /// 5-11-2018
        /// Changed the implementation for row data bound of part detail report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void gridview_RowDataBoundPartDetail(object sender, GridViewRowEventArgs e)
        {
            try
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Attributes.Add("style", "white-space:normal;word-break:break-all;word-wrap:break-word;");
                }

                if (e.Row.Cells[6].Text.Trim() != "&nbsp;")
                {
                    e.Row.Cells[6].Attributes.Add("style", "font-weight:bold;");
                    e.Row.Cells[6].Text = HttpUtility.HtmlDecode(e.Row.Cells[6].Text);
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "gridview_RowDataBoundPartDetail");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "gridview_RowDataBoundPartDetail");
                }
            }
        }


        public static void ExportToCSV(DataTable dt, string reportName, HttpResponseBase response)
        {
            try
            {
                var filename = reportName.Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");

                var gridview = new GridView();
                gridview.DataSource = dt;
                gridview.DataBind();

                for (var i = 0; i < gridview.Rows.Count; i++)
                {
                    var row = gridview.Rows[i];
                    gridview_RowDataBound(gridview, new GridViewRowEventArgs(row));

                    foreach (TableCell cell in gridview.Columns)
                    {
                        cell.Attributes.CssStyle["text-align"] = "center";
                        cell.Wrap = true;
                    }
                }

                // Clear all the content from the current response
                response.ClearContent();
                response.Buffer = true;
                response.AddHeader("content-disposition", string.Format("attachment;fileName={0}.csv", filename));
                response.Charset = "";
                response.ContentType = "application/text";
                response.ContentEncoding = Encoding.UTF8;

                var sb = new StringBuilder();

                // Hide/Remove columns from csv.
                for (var k = 0; k < dt.Columns.Count; k++)
                {
                    sb.Append(dt.Columns[k].ColumnName + ',');
                }

                sb.Append("\r\n");

                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    for (var k = 0; k < dt.Columns.Count; k++)
                    {
                        sb.Append(string.Format("{0}{1}", dt.Rows[i][k].ToString().Replace(",", ";"), ','));
                    }

                    sb.Append("\r\n");
                }

                response.Output.Write(HttpUtility.HtmlDecode(sb.ToString()));
                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "ExportToCSV");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "ExportToCSV");
                }
            }
        }


        public static void ExportToPdf(DataTable dt, PdfPTable pdfTable, string reportName, HttpResponseBase response, HttpServerUtilityBase server)
        {
            try
            {
                var logoImg = iTextSharp.text.Image.GetInstance(server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                logoImg.ScaleAbsolute(250, 45);

                using (var memoryStream = new MemoryStream())
                {
                    var document = new Document(PageSize.A4, 10, 10, 20, 10);
                    var writer = PdfWriter.GetInstance(document, memoryStream);

                    document.Open();
                    document.Add(logoImg);
                    document.Add(new Paragraph("\n"));
                    document.Add(new Paragraph("\n"));
                    document.Add(pdfTable);
                    document.Close();
                    writer.Close();

                    response.ClearContent();
                    response.ContentType = "application/pdf";
                    response.AddHeader("Content-Disposition", string.Format("attachment; fileName={0}.pdf", reportName));
                    response.Buffer = false;
                    response.Cache.SetCacheability(HttpCacheability.NoCache);
                    response.OutputStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
                    response.End();
                    memoryStream.Close();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "ExportToPdf");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "ExportToPdf");
                }
            }
        }
    }
}
﻿using System;

namespace OneSourceAdminCore.Helper
{
    public static class TypeExtensions
    {

        public static DateTime ConvertObjectToDateTime(this object valueFrom)
        {
            return valueFrom != null && !string.IsNullOrEmpty(valueFrom.ToString())
                ? Convert.ToDateTime(valueFrom)
                : DateTime.Now;
        }


        public static string ConvertObjectToString(this object valueFrom, string defaultValue = "")
        {
            return valueFrom != null ? valueFrom.ToString() : defaultValue;
        }


        public static int ConvertObjectToInt32(this object valueFrom, int defaultValue = 0)
        {
            return valueFrom != null ? Convert.ToInt32(valueFrom) : defaultValue;
        }


        public static string TrimSubString(this object valueFrom, int indexFrom, int indexTo, string defaultValue = "")
        {
            return valueFrom != null ? valueFrom.ToString().Substring(indexFrom, indexTo) : defaultValue;
        }


        public static bool IsNotEmpty(this string valueFrom)
        {
            return !string.IsNullOrEmpty(valueFrom);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company detail"
    /// </summary>
    public class CompanyModel
    {
        public List<sp_getIndustry_AdminR_Result> industries { get; set; }
        public List<sp_getContact_AdminR_Result> contacts { get; set; }
        public List<sp_getRole_AdminR_Result> roles { get; set; }
        public List<GenericList> parentCompanies { get; set; }
        public int CompanyId { get; set; }
        public string companyName { get; set; }
        public int? numEmployees { get; set; }
        public int? parentcompanyid { get; set; }       
        public int? yearInc { get; set; }
        public string companyURL { get; set; }
        public string taxID { get; set; }
        public string DUNS { get; set; }
        public decimal? marketCap { get; set; }
        public decimal? annualitspending { get; set; }
        public decimal? annualsvcsspending { get; set; }
        public bool isActive { get; set; }
        public int? gscontactId { get; set; }
        public int? docmgtvisible { get; set; }
        public string gscontactEmail { get; set; }
        public int? userID { get; set; }
        public string sicIds{ get; set; }
        public string roleIds{ get; set; }
        public int roleId{ get; set; }
        public string parentcompanyname { get; set; }
        public int? ParentParentCompanyID { get; set; }

        public int PartyId { get; set; }
        public CompanyRoleModel CompanyRoleModel{ get; set; }


    }
}

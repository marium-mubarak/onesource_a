﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class SoftwareQuotesSearchModel
    {
        public SoftwareQuotesSearchModel()
        {
            ListCustomers = new List<GenericList>();
            ListCustomers.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
            SoftwareQuotes = new List<SoftwareQuotesDetailModel>();
            SoftwareProgramsList = new List<GenericList>();
            SoftwareProgramsList.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
        }

        public int CustomerId { get; set; }
        public List<SoftwareQuotesDetailModel> SoftwareQuotes { get; set; }
        public List<GenericList> ListCustomers { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<SelectListItem> Manufacturers { get; set; }

        //Software Programs (SUNNY GOHAR: 7th Jan 2019)
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string ProgramType { get; set; }
        public List<GenericList> SoftwareProgramsList { get; set; }
    }

    public class SoftwareQuotesDetailModel
    {
        public SoftwareQuotesDetailModel()
        {
            RequestItems = new List<SoftwareQuoteItems>();
        }

        public int QuoteId { get; set; }
        public string CustomerReference { get; set; }
        public string Country { get; set; }
        public string Supplier { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Currency { get; set; }

        public string PriceLevel { get; set; }

        public List<SoftwareQuoteItems> RequestItems { get; set; }
        public List<SoftwareQuotesResponseLineItem> ResponseItems { get; set; }

        public List<SoftwareQuoteAttachments> QuoteAttachments { get; set; }

        public List<SoftwareQuoteComments> QuoteComments { get; set; }

        public List<SoftwareQuoteAttachments> Attachments { get; set; }
        public List<SoftwareQuoteComments> Comments { get; set; }

        //added more fields for program type (SUNNY GOHAR : 8th Jan 2018)
        public string ProgramName { get; set; }
        public string OrganizationName { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public Nullable<System.DateTime> ExpectedPurchaseDate { get; set; }
        public string AgreementType { get; set; }
        public string AgreementNumber { get; set; }
        public string InvoiceCountry { get; set; }
        public string AgreementLevel { get; set; }
        public string ProgramType { get; set; }
    }
    public class SoftwareQuoteItems
    {
        public int QuoteItemId { get; set; }
        public int SoftwareQuoteId { get; set; }
        public string Description { get; set; }
        public string Sku { get; set; }
        public string Manufacturer { get; set; }
        public int Quantity { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> SoftwareItemId { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public string PriceLevel { get; set; }
    }
    public class SoftwareQuoteComments
    {
        public int CommenId { get; set; }
        public string Comment { get; set; }
        public int SoftwareQuoteId { get; set; }
        public bool IsActive { get; set; }
        public string UpdateBy { get; set; }
        public string UpdatedDate { get; set; }
        public Nullable<bool> IsSupplierComment { get; set; }
        public Nullable<bool> IsAdminComment { get; set; }
        public string Supplier { get; set; }
    }

    public class SoftwareQuoteAttachments
    {
        public int AttachmentId { get; set; }
        public int SoftwareQuoteId { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentUniqueId { get; set; }
        public Nullable<int> MimeTypeExtensionId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsSupplierAttachment { get; set; }
        public Nullable<bool> IsAdminAttachment { get; set; }
        public string Supplier { get; set; }
    }

    public class SoftwareQuotesResponseLineItem
    {
        public int UserId { get; set; }
        public int ResponseItemId { get; set; }
        [Required]
        public int SoftwareQuoteId { get; set; }
        [Required]
        public string Manufacturer { get; set; }
        [Required]
        public string Sku { get; set; }
        public string Description { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public Nullable<decimal> Price { get; set; }
        [Required]
        public string Eol { get; set; }
        public Nullable<int> LeadTime { get; set; }

        public string ManufacturerName { get; set; }

        public string CreatedBy { get; set; }
        public string Supplier { get; set; }

        public bool isSupplier { get; set; }
    }

}

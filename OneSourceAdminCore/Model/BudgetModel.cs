﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of budget"
    /// </summary>
    public class BudgetModel
    {
        public List<GenericList> Customers { get; set; }
        public List<GenericList> Currencies { get; set; }
    }
}

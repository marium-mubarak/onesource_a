﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : August 1 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for Support -> Request Listing"
    /// </summary>
    public class SupportRequestListModel
    {
        public List<sp_getSupportRequest_AdminR_Result> Request{ get; set; }
        public List<GenericList> Filters { get; set; }
        public string FilterId{ get; set; }
    }
}

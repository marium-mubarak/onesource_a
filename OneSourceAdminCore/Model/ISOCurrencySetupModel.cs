﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;


namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 28 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for ISO Currency Setup detail"
    /// </summary>
    public class ISOCurrencySetupModel
    {
        public List<sp_getISOCurrencyList_R_Result> ISOCurrencyList { get; set; }
        public List<GenericList> DefaultISOCurrencyList { get; set; }
    }
}

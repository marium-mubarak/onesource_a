﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for Search Quote info"
    /// </summary>
    public class QuoteSearchModel
    {
        public List<sp_getCustomerQuoteSearch_AdminR_Result> Customers { get; set; }
        public List<sp_getCountryQuoteSearch_AdminR_Result> Countries { get; set; }
        public List<sp_getCategoryResponseQuoteSearch_AdminR_Result> CategoryResponses { get; set; }
        public List<sp_getRequestedByQuoteSearch_AdminR_Result> RequestedBy { get; set; }
        public List<sp_getManufacturerResponseQuoteSearch_AdminR_Result> ManufacturerResponses { get; set; }
        public List<sp_getSupplierQuoteSearch_AdminR_Result> Suppliers { get; set; }
        public List<GenericList> QuoteStatus { get; set; }      
        public string StrStatus { get; set; }
        public string StrSuppliers { get; set; }
        public string quoteType { get; set; }
        public string quoteNumber { get; set; }
        public int customerid { get; set; }
        public int countryid { get; set; }
        public string descriptionRequest { get; set; }
        public string skuResponse { get; set; }
        public int partcategoryid { get; set; }
        public string requestedAfterDate { get; set; }
        public string requestedBeforeDate { get; set; }
        public string quoteReference { get; set; }
        public string manufacturerRequest { get; set; }
        public int manufacturerid { get; set; }
        public string descriptionResponse { get; set; }
        public int contactid{ get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : July 25 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for having data of preferences"
    /// </summary>
    public class PreferenceModel
    {
        public List<GenericList> DateTimeList{ get; set; }
        public List<GenericList> CountryList{ get; set; }
        public string DHomePage{ get; set; }
        public string DSearchPage { get; set; }
        public int DDateFormatId { get; set; }
        public int DCountryId { get; set; }
        public string DReportViewer { get; set; }
    }
}

﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class SMBQuotesModel
    {
        public SMBQuotesModel()
        {
            ListSMBUsers = new List<SelectListItem>();
            ListQuoteTypes = new List<SelectListItem>();
            ListQuoteIds = new List<SelectListItem>();

            ListSMBUsers.Add(new SelectListItem { Value = "0", Text = "--Select--", Selected = true });
            ListQuoteTypes.Add(new SelectListItem { Value = "0", Text = "--Select--", Selected = true });
            ListQuoteIds.Add(new SelectListItem { Value = "0", Text = "--Select--", Selected = true });
        }

        public List<SelectListItem> ListSMBUsers { get; set; }

        public List<SelectListItem> ListQuoteTypes { get; set; }

        public List<SelectListItem> ListQuoteIds { get; set; }

        public List<tblResponseQuoteLineItem> ResponseQuoteLineItems { get; set; }

        public int AccountManagerId { get; set; }
        [Required(ErrorMessage = "Please select account manager")]
        public int AccountManagerName { get; set; }
        public string QuoteType { get; set; }
        public int QuoteId { get; set; }

        public class SMBUsers
        {
            public int UserID { get; set; }
            public string UserName { get; set; }
            public int UserGroupID { get; set; }
        }
        
    }

    public class SMBQuoteLineItems
    {
        public int ResponseLineItemId { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string SKU { get; set; }
        public string SerialNumber { get; set; }
        public string Category { get; set; }
        public string LeadTime { get; set; }
        public string EOL { get; set; }
        public string Quantity { get; set; }
        public string UnitPrice { get; set; }
        public string ServiceType { get; set; }
        public string SLAType { get; set; }
        public string Resources { get; set; }
        public string HourlyRate { get; set; }
        public string TravelCharges { get; set; }
        public string GSPMargin { get; set; }
        public string GSMargin { get; set; }
        public string FinalPrice { get; set; }
        public string TaxVat { get; set; }
        public string TotalTaxVat { get; set; }

    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company contact detail"
    /// </summary>
    public class CompanyContactModel
    {
        public List<GenericList> Referrals { get; set; }
        public List<GenericList> Users { get; set; }
        public List<GenericList> Companies { get; set; }
        public sp_getCompanyContactInfo_AdminR_Result ContactInfo { get; set; }
        public List<GenericList> ContactType { get; set; }
        public List<GenericList> Countries { get; set; }
        public string roleIds { get; set; }
        public List<sp_getContactAttachment_AdminR_Result> contactAttachments{ get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of Request Engineer setting to view"
    /// </summary>
    public class RequestEngineerLevelModel
    {
        public List<CustomerCompanyClass> Customers { get; set; }
        public List<sp_GetEngineerLevelMapping_R_Result> EngineeringLevelData { get; set; }
    }
    public class RequestsTypes
    {
        public int RequestId { get; set; }
        public string RequestTypeName { get; set; }
    }
    public class Priority
    {
        public int PriorityId { get; set; }
        public string PriorityName { get; set; }
    }
}

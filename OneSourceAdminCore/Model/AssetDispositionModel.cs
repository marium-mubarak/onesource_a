﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class AssetDispositionModel
    {
        public AssetDispositionModel()
        {
            ListCustomers = new List<SelectListItem>();
            ListCountries = new List<SelectListItem>();
            ListRegions = new List<SelectListItem>();
            ListAssetTypes = new List<SelectListItem>();
            ListProductCategories = new List<SelectListItem>();
            ListSuppliers = new List<SelectListItem>();
           
        }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CountryIds { get; set; }
        public string CountryNames { get; set; }

        public string AssetTypes { get; set; }
        public string AssetTypeNames { get; set; }

        public string PartCategoryIds { get; set; }
        public string CategoryNames { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string RegionCountry  { get; set; }
        public string SubType { get; set; }
        public string RegionIds { get; set; }
        public string RegionNames { get; set; }
        public string RegionName { get; set; }
        public List<SelectListItem> ListCustomers { get; set; }
        public List<SelectListItem> ListSuppliers { get; set; }
        public List<SelectListItem> ListCountries { get; set; }
        public List<SelectListItem> ListRegions { get; set; }
        public int TotalAssets { get; set; }
        public int TotalItems { get; set; }
        public List<SelectListItem> ListAssetTypes { get; set; }
        public List<SelectListItem> ListProductCategories { get; set; }

        public string DetailName { get; set; }
        public DataTable  dt { get; set; }

        #region remove session work because of opening multiple tab issue
        public int routeId { get; set; }
        public string reportName { get; set; }
        public string routeName { get; set; }
        public int parentRouteId { get; set; }

        public string parentName { get; set; }

        public string parentRoute { get; set; }

        public AssetDispositionModel TempAssetDispositionModel { get; set; }

        public DataTable TempAssetDispositionTable { get; set; }

        public string excelAction { get; set; }
        public string pdfAction { get; set; }
        public string MainAction { get; set; }
        public string CSVAction { get; set; }
        public double TotalFees { get; set; }
        public double TotalAr { get; set; }
        public double NetGainOrLoss { get; set; }
        public double TotalNetLossPerAsset { get; set; }
        #endregion

    }

    public class Region
    {
        public string regionName { get; set; }
    }
    public class RegionCountries
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ResultData
    {
        public string name { get; set; }
        public int[] data { get; set; }
    }

    public class FinancialResultData
    {
        public string name { get; set; }
        public object[] data { get; set; }
    }
}

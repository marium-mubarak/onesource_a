﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class AssetSerialNumberViewModel
    {
        public int? SessionTrackingNumber { get; set; }
        public string ManufacturerSKU { get; set; }
        public string SerialNum { get; set; }
        public string AssetTagNum { get; set; }
        public string MACAddress { get; set; }
    }
}

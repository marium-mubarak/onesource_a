﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class CatalogBundleDetailModel
    {
        public List<CatalogBundleItemList> ItemList { get; set; }
        public ProductSpecification ProductSpecification { get; set; }
        public string DefaultImage{ get; set; }
    }
    public class CatalogBundleItemList
    {
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string SKU { get; set; }
        public int Quantity { get; set; }
        public bool IsOption { get; set; }
    }
    public class ProductSpecification
    {
        public string Description{ get; set; }
        public string ImageFile{ get; set; }
        public string Comments{ get; set; }
    }

    //public class BundleCategorySortModel
    //{
    //    public List<BundleCategorySortTableModel> finalList { get; set; }
    //}

    //public class BundleCategorySortTableModel
    //{
    //    public int PartCategoryID { get; set; }
    //    public string PartCategoryName { get; set; }
    //    public int SortNumber { get; set; }
    //    public int BundleCategorySortID { get; set; }
    //    public int CustomerID { get; set; }
    //}

}

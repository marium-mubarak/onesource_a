﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : feb 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for service detail"
    /// </summary>
    public class SSM_ServiceModel
    {
        public List<tblService> lstService { get; set; }
        public tblService serviceInfo { get; set; }

        public string UserName{ get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class SoftwareSettingsModel
    {
        public List<TblConnectionCustomerSetting> CustomerSettings { get; set; }
        public List<GenericList> CustomersList { get; set; }
        public List<GenericList> ContractTypeList { get; set; }
        public List<GenericList> ProgramsList { get; set; }
        public List<GenericList> AgreementTypeList { get; set; }
        public List<GenericList> PoolList { get; set; }
        public List<TblPool_PriceLevelMapping> PoolSettingList { get; set; }
        public string AgreementNumber { get; set; }
        public string ConnectionSourceId { get; set; }
        public string LegalEntityName { get; set; }
        public string EnrollmentNumber { get; set; }
        public string PcnNumber { get; set; }
        public string PusrchasingAccountNumber { get; set; }
        public int? AnniversryMonth { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Source { get; set; }
        public bool IsActive { get; set; }
        public int ProgramId { get; set; }
        public int CustomerId { get; set; }
        public int AgreementTypeId { get; set; }
        public int ContractTypeId { get; set; }
        public int SettingId { get; set; }

    }
}

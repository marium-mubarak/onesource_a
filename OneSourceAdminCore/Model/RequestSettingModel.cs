﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of Request setting to view"
    /// </summary>
    public class RequestSettingModel
    {
        public List<CustomerCompanyClass> Customers { get; set; }
        public List<GenericList> LstGenericPriorities { get; set; }
        public List<Priority> LstPriorities { get; set; }
        public int ParentCompanyId{ get; set; }
        public string CustomerId { get; set; }
    }
    public class EngineerLevelModel
    {
        public List<string> PriorityIds { get; set; }
        public List<string> GenericPriorityIds { get; set; }
        public int CustomerId { get; set; }
        public int CompanyId { get; set; }
        public string GenericsPriorityNames { get; set; }
        public bool wasStandard { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class PrimeContractorModel
    {
        public List<tblPrimeContractor> PrimeContractorList { get; set; }
        public List<tblPrimeContractor> PrimeContractorListForDrpDown { get; set; }
        public List<GenericList> CountryList { get; set; }
        public List<GenericList> CompanyList { get; set; }
        public List<GenericList> OrderFormatList { get; set; }
        public List<GenericList> DateFormatList { get; set; }
        public tblPrimeContractor PrimeContractor { get; set; }
        public string LastUpdatedByUser { get; set; }
        public List<tblPrimeContractorserviceLocation> LocationsList { get; set; }
        public tblPrimeContractorserviceLocation PcLocation { get; set; }
        public List<GenericList> PcExchangeRatesCurrencyList { get; set; }
        public List<Sp_GetPrimeContractorUser_AdminR_Result> PcUsersList { get; set; }
        public PrimeContractorUserModel PrimeContractorUser { get; set; }
    }


    public class PrimeContractorUserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public byte IsActive { get; set; }
        public string Email { get; set; }
        public int PrimeContractorId { get; set; }
        public int PrimeContractorLocationId { get; set; }
        public int DateFormatId { get; set; }
        public int LastUpdateByUser { get; set; }
        public DateTime LastUpdated { get; set; }

    }


}

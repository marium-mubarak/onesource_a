﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{

    
    public partial class QuoteRequestModel
    {
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string SKU { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> CatalogStandardID { get; set; }
        public int BundleCategoryId { get; set; }
    }
    
    public partial class PartQuoteRequestModel
    {
        public int PartQuoteRequestID { get; set; }
        public int PartQuoteID { get; set; }
        public string Manufacturer { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }
        public Nullable<int> Quantity { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public int LastUpdatedByUserID { get; set; }

        public Nullable<int> CatalogStandardID { get; set; }
        public int BundleCategoryId { get; set; }
        public Nullable<int> CatalogID { get; set; }
    }

    public class BundleSettings
    {
        public Nullable<int> CustomerId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> CurrencyId { get; set; }
        public int DestinationId { get; set; }
        public int UserSecurityLevel { get; set; }
        public string CurrencySymbol { get; set; }
        public string CompanyId { get; set; }
        public string DateFormatID { get; set; }
        public string UserGroupName { get; set; }
        public int ContactId { get; set; }
        public int PartyId { get; set; }
        public bool IsProcurementCustomer { get; set; }
        public bool IsSupportCustomer { get; set; }
        public string CustomerName { get; set; }
        public bool OneStreetAccess { get; set; }
        public int UserSessionLogId { get; set; }
        public string UserName { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class PartInfoModel
    {
        public string StdnDesc { get; set; }
        public string SKU { get; set; }
        public string ImageID { get; set; }
        public string ProdLine { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public string MarketingDesc { get; set; }
    }
    public class PartMainSpecification
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
    public class PartExtendedSpecification
    {
        public string Section { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
    }
    public class PartDetailModel
    {
        public PartInfoModel partInfoModel { get; set; }
        public List<PartMainSpecification> partMainSpecification { get; set; }
        public List<PartExtendedSpecification> partExtendedSpecification { get; set; }
    }

}

﻿
using System.Collections.Generic;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class SFACatalogModel
    {
        public List<GenericList> CustomerList { get; set; }
        public List<GenericList> CurrencyList { get; set; }
        public List<GenericList> CountryList { get; set; }
        public List<GenericList> CategoryList { get; set; }
        public List<GenericList> ManufacturerList { get; set; }
        public List<usp_GetPartInfoAdjustedPrice_R_Result> SearchList { get; set; }
        public List<sp_getDomesticCatalog_AdminR_Result> SearchDomesticList{ get; set; }
        public List<sp_getCatalogBundles_AdminR_Result> SearchCatalogBundleList { get; set; }
        public int manufacturerId { get; set; }
        public int partCategoryId { get; set; }
        public string ProductSKUName { get; set; }
        public int countryId { get; set; }
        public int currencyId { get; set; }
        public int customerId{ get; set; }
        public string catalogTypeId { get; set; }
        public string productSearchBy { get; set; }
        public string CurrencyName{ get; set; }
    }
}


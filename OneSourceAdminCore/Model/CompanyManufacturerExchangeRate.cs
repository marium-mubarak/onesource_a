﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company manufacturer exchange rate"
    /// </summary>
    public class CompanyManufacturerExchangeRate
    {
        public List<GenericList> MfgExchangeRateCountries { get; set; }
        public List<sp_GetMfgExchangeRateInfo_AdminR_Result> MfgExchangeRateInfo { get; set; }
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class ProcurementReportModel
    {
        public ProcurementReportModel()
        {
            ListColumns = new List<SelectListItem>();
            ListColumns.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            ListPartDetailCustomers = new List<SelectListItem>();
            ListPartDetailCustomers.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            ListCurrencies = new List<GenericList>();
            ListRegions = new List<GenericList>();
            DataList = new List<object>();
            PartDetailColumns = new PartDetailColumns();
            ISDAmountSpendByUserReportModel = new ISDAmountSpendByUserReportModel();
            QuoteSummaryReportModel = new QuoteSummaryReportModel();
            QuoteSummaryReportOutput = new QuoteSummaryReportOutput();
            QuoteSummaryResponseOutput = new QuoteSummaryResponseOutput();
            CustomerOrderReportListModel = new CustomerOrderReportListModel();
            PreferredSupplierDataTable = new DataTable();
            ListSOCShifts = new List<GenericList>(); // added for open orders report with SOC shift

        }
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public string OrderStatus { get; set; }
        public List<GenericList> ListCurrencies { get; set; }
        public List<GenericList> ListRegions { get; set; }
        public List<SelectListItem> ListPartDetailCustomers { get; set; }
        public IList<SelectListItem> ListColumns { get; set; }
        public int CustomerId { get; set; }
        public int RegionId { get; set; }
        public string CustomerIds { get; set; }
        public string ColumnIds { get; set; }
        public string CurrencyId { get; set; }
        public string CountryIDs { get; set; }
        public string QuoteType { get; set; }
        public string CustomerName { get; set; }
        public IList<object> DataList { get; set; }
        public PartDetailColumns PartDetailColumns { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public ISDAmountSpendByUserReportModel ISDAmountSpendByUserReportModel { get; set; }
        public QuoteSummaryReportModel QuoteSummaryReportModel { get; set; }
        public QuoteSummaryReportOutput QuoteSummaryReportOutput { get; set; }
        public QuoteSummaryResponseOutput QuoteSummaryResponseOutput { get; set; }

        public CustomerOrderReportListModel CustomerOrderReportListModel { get; set; }

        public DataTable PreferredSupplierDataTable { get; set; }

        public List<GenericList> ListSOCShifts { get; set; } // added for open orders report with SOC shift
        public int SOCShiftId { get; set; }
        public int ParentCompanyID { get; set; } // Added by adnan on 1-7-2019 for YTD report
        public List<GenericList> ListSuppliers { get; set; }// Added by adnan on 10-7-2019 for Invoiced Order report

        #region remove session work because of opening multiple tab issue
        public int routeId { get; set; }
        public string reportName { get; set; }
        public string routeName { get; set; }
        public int parentRouteId { get; set; }

        public string parentName { get; set; }

        public string parentRoute { get; set; }

        public string excelAction { get; set; }
        public string pdfAction { get; set; }
        public string MainAction { get; set; }
        public string CSVAction { get; set; }
        #endregion
    }

    //public class ColumnsModel
    //{
    //    public string CustomerName { get; set; }
    //    public string CountryName { get; set; }
    //    public string SFAName { get; set; }
    //    public string SupplierCity { get; set; }
    //    public string PrimeContractorUplift { get; set; }
    //    public string SFAStandardUplift { get; set; }
    //    public bool IsPreferred { get; set; }
    //    public string LastUpdated { get; set; }
    //    public string LastUpdatedByUserID { get; set; }
    //}

    #region Sub Models

    public class ISDAmountSpendByUserReportModel
    {
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string TotalOrderValueCount { get; set; }
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public List<sp_ISDAmountSpendByUserRep_AdminR_Result> ListOrders { get; set; }
    }
    public class QuoteSummaryReportModel
    {
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public List<QuoteSummaryReportOutput> ListOutputData { get; set; }
        public int CustomerId { get; set; }
        public int CountryId { get; set; }

    }
    public class QuoteSummaryReportOutput
    {
        public int QuoteId { get; set; }
        public string CustomerReference { get; set; }
        public int CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
    public class QuoteSummaryResponseOutput
    {
        public int ManufacturerID { get; set; }
        public string Manufacturer { get; set; }
        public string SKU { get; set; }
        public int PartCategoryID { get; set; }
        public string PartCategory { get; set; }
        public string Description { get; set; }
        public int Qunatity { get; set; }
        public string Price { get; set; }
        public string EOL { get; set; }

    }
    public enum PartDetailColumns
    {
        [Description("Order Country")]
        Order_Country = 0,
        [Description("Destination PO")]
        Destination_PO = 1,
        [Description("Accepted Date")]
        Accepted_Date = 2,
        //[Description("Business Days Open")]
        //Business_Days_Open = 3,
        //[Description("Delivery SLA (Business Days)")]
        //Delivery_SLA_Business_Days = 4,
        [Description("Status")]
        Status = 5,
        [Description("Shipping Address")]
        Shipping_Address = 6,
        [Description("Billing Address")]
        Billing_Address = 7,
        [Description("Bundle")]
        Bundle = 8,
        [Description("Category")]
        Category = 9,
        [Description("Manufacturer")]
        Manufacturer = 10,
        [Description("Supplier")]
        Supplier = 11,
        [Description("MFG Tracking Info")]
        MFG_Tracking_Info = 12,
        [Description("Catalog Type")]
        Catalog_Type = 13,
        [Description("Buyer")]
        Buyer = 14,
        //Removed from optional (SUNNY GOHAR : 18th Dec 2018)
        //[Description("Order Currency")]
        //Order_Currency = 15,
        [Description("Item Price")]
        Item_Price = 16,
        [Description("Item Total")]
        Item_Total = 17,
        //[Description("Item Price USD")]
        //Item_Price = 18,
        [Description("Quantity")]
        Quantity = 19,
        //[Description("Item Total USD")]
        //Item_Total = 20,
        //[Description("Customer")]
        //Customer = 21,
        //[Description("Tracking #")]
        //Tracking_No = 22,
        //[Description("Order PO")]
        //Order_PO = 23,
        //[Description("Order Date")]
        //Order_Date = 24,
        //[Description("ETS")]
        //ETS = 25,
        //[Description("Ship Date")]
        //Ship_Date = 26,
        //[Description("Part Description")]
        //Part_Description = 27,
        //[Description("SKU")]
        //SKU = 28,
        //Added Date Delivered as optional column ( SUNNY GOHAR : 14th Jan 2019 )
        //[Description("Date Delivered")]
        //DateDelivered = 29,
        //Removed from optional (SUNNY GOHAR : 18th Dec 2018)
        //[Description("Currency")]
        //Currency = 29

    }
    public class CustomerOrderReportModel
    {
        public int SessionTrackingNumber { get; set; }
        public string OrderDate { get; set; }

        public string OrderPO { get; set; }
        public string OrderedByUser { get; set; }
        public string DateShipped { get; set; }

        public int OrderTrackingNumber { get; set; }
        public int DestinationTrackingNumber { get; set; }

    }
    public class CustomerOrderReportListModel
    {
        public CustomerOrderReportListModel()
        {
            CustomerOrderDetails = new List<CustomerOrderReportModel>();
            ListCustomers = new List<CustomerCompanyClass>();

        }

        public List<CustomerOrderReportModel> CustomerOrderDetails { get; set; }
        public IList<CustomerCompanyClass> ListCustomers { get; set; }

        public int CustomerId { get; set; }

    }
    public class CustomerOrderItems
    {
        public int OrderItemId { get; set; }
        public string ItemDescription { get; set; }
        public int Quantity { get; set; }

        public decimal? UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public string Sku { get; set; }
    }

    #endregion


    public enum PreferredSupplierColumns
    {
        [Description("Customer Name")]
        CustomerName = 0,
        [Description("Country Name")]
        CountryName = 1,
        [Description("Supplier Name")]
        SFAName = 2,
        [Description("Prime Contractor Uplift")]
        PrimeContractorUplift = 3,
        [Description("Supplier Standard Uplift")]
        SFAStandardUplift = 4,
        [Description("Uplift Preffered")]
        IsPreferred = 5,
        [Description("Uplift Last Updated")]
        LastUpdated = 6,
        [Description("Uplift Last Updated By")]
        LastUpdatedByUserID = 7

    }
}

//public class PartDetailColumnsModel
//{
//    public string Order_Country { get; set; }
//    public string Destination_PO { get; set; }
//    public string Accepted_Date { get; set; }
//    public string Business_Days_Open { get; set; }
//    public string Delivery_SLA_Business_Days { get; set; }
//    public string Status { get; set; }
//    public bool Shipping_Address { get; set; }
//    public string Billing_Address { get; set; }
//    public string Bundle { get; set; }
//    public string Category { get; set; }
//    public string Manufacturer { get; set; }
//    public string Supplier { get; set; }
//    public string MFG_Tracking_Info { get; set; }
//    public string Catalog_Type { get; set; }
//    public string Buyer { get; set; }
//    public bool Order_Currency { get; set; }
//    public string Item_Price { get; set; }
//    public string Item_Total { get; set; }
//    public string Item_Price_USD { get; set; }
//    public string Quantity { get; set; }
//    public string Item_Total_USD { get; set; }
//}
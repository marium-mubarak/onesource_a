﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for quote response model having updated information"
    /// </summary>
    public class QuoteResponseEditModel
    {
        public sp_getQuoteResponseById_AdminR_Result QuoteResponse { get; set; }
        public sp_getQuoteResponseById_AdminR_Result PartQuoteResponse { get; set; }
        public List<GenericList> Manufacturers { get; set; }
        public List<sp_getQuoteReqResCategories_AdminR_Result> Categories { get; set; }
        public int QuoteId{ get; set; }
    }
}

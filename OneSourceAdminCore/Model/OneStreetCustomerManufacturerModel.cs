﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Oct 10 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of Customer's manufacturer to view"
    /// </summary>
    public class OneStreetCustomerManufacturerModel
    {
        public List<GenericList> Manufacturers{ get; set; }
        public List<GenericList> Customers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for uploading data of quote response"
    /// </summary>
    public class UploadQuoteResponseModel
    {
        public int ManufacturerIds { get; set; }
        public string SKU{ get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public int? LeadTime{ get; set; }
        public DateTime EOL { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

    }
}

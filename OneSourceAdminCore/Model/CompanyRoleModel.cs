﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for managing company role like manufacturer and customer etc"
    /// </summary>
    public class CompanyRoleModel
    {
        public CompanyManufacturerModel CompanyManufacturerModel { get; set; }
        public CompanyCustomerModel CompanyCustomerModel { get; set; }
        public CompanySupplierModel CompanySupplierModel { get; set; }
    }


    public class CompanyCustomerModel
    {
        public tblCustomer Customer { get; set; }
        public List<tblOffering> OfferingList { get; set; }
        public List<tblPrimeContractor> PrimeContractorList { get; set; }
        public string CustomerOfferingIds { get; set; }
        public string UpdatedByUser { get; set; }
        public List<GenericList> CustomerCountryList { get; set; }
        public List<Sp_GetUserByCustomerId_AdminR_Result> CustomerUsersList { get; set; }
        public Sp_GetCustomerUserByUserId_AdminR_Result CustomerUser { get; set; }
        public List<GenericList> DateFormatList { get; set; }
        public List<GenericList> CountryList { get; set; }
        public List<GenericList> CurrencyList { get; set; }
        public List<GenericList> CustomerUserGroupList { get; set; }
        public decimal ExpectedMonthlySpend { get; set; }
        public int CompanyId { get; set; }
        public int RoleId { get; set; }
        public int PartyId { get; set; }
        public List<Sp_getCustomerAddress_AdminR_Result> CustomerAddressList { get; set; }
        public sp_GetAddressDetailByCustomer_AdminR_Result Address { get; set; }
        public List<GenericList> TimeZoneList { get; set; }
        public List<GenericList> RegionsList { get; set; }
        public List<GenericList> CarrierList { get; set; }
        public List<Sp_GetContactList_AdminR_Result> ContactList { get; set; }

        //Additional Setup start
        public List<GenericList> CustomerDestinationCountryList { get; set; }
        public List<GenericList> SetupManufacturersList { get; set; }
        public List<GenericList> CustomerDestinationAddressList { get; set; }
        public List<GenericList> CustomerUpliftsCountryList { get; set; }
        public List<GenericList> TbuManufacturerList { get; set; }

        //Additional Setup end

        // Catalog start 

        public List<Sp_GetCatalogPartsByCustomer_R_Result> CatalogList { get; set; }
        public List<tblCatalogStandard> BundlelList { get; set; }
        public CatalogSearchModel CatalogSearchModel { get; set; }

        //Catalog end

        //Customer Disclaimer Start
           public List<Sp_GetCustomerCountryDisclaimers_R_Result> CountryDisclaimerList { get; set; }

        //Customer Disclaimer Start
    }

    public class CatalogSearchModel
    {
        public int PriceCodeId { get; set; }
        public string SearchText { get; set; }
        public int CatalogType { get; set; }
    }

    public class CustomerCurrencyModel
    {
        public bool IsSelected { get; set; }
        public int CustomerId { get; set; }
        public string CurrencyName { get; set; }
        public int CountryId { get; set; }
        public int CurrencyId { get; set; }
        public bool IsDefaultCurrency { get; set; }
    }


    public class CustomerUserAddressPermissionModel
    {
        public bool IsSelected { get; set; }
        public int InvoiceAddressId { get; set; }
        public string InVoiceAddressName { get; set; }
        public bool IsGlobal { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public int CountryId { get; set; }
        public int DestinationId { get; set; }
    }

    public class UserCredentialModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CompanyName { get; set; }

    }

    public class CompanySearchModel
    {
        public List<GenericList> RolesList { get; set; }
        public List<sp_getCompanies_AdminR_Result> CompaniesList { get; set; }
    }

}

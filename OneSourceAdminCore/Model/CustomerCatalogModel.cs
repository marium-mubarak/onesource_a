﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class CustomerCatalogModel
    {
        public DateTime Eol { get; set; }
        public int CountrifyIn { get; set; }
        public int CompanyId { get; set; }
        public int CustomerId { get; set; }
        public int PartyId { get; set; }
        public tblCatalogPart CatalogPart { get; set; }
        public tblManufacturerPart ManfPart { get; set; }
        public List<GenericList> PartCategoryList { get; set; }
        public List<GenericList> CurrencyList { get; set; }
        public List<GenericStringList> UnspscList { get; set; }
        public List<GenericList> PriceCodeList { get; set; }
        public List<GenericList> SupplierList { get; set; }
        public List<GenericList> CountriFyList { get; set; }

        public List<GenericList> CountrifyCountryList { get; set; }
        public Sp_GetCustomerCountryPartDetail_AdminR_Result CountryPartInfo { get; set; }
        public tblCatalogStandard CatalogStandard { get; set; }
        public List<sp_GetBundlePartsById_AdminR_Result> BundleParts { get; set; }
    }


    public class QuickSettingModel
    {
        public DateTime Eol { get; set; }
        public int CustomerId { get; set; }
        public int CatalogId { get; set; }
        public int CountryId { get; set; }
        public bool IsActive { get; set; }
        public bool UpdateEol { get; set; }
        public int CurrencyId { get; set; }
        public decimal Price { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class SupplierAccreditationModel
    {
        public List<AccreditationMfg> Mfgs{ get; set; }
        public int custserviceId { get; set; }
    }
    public class AccreditationMfg
    {
        public int ManufacturerID { get; set; }
        public string ManufacturerName { get; set; }
        public int? SelectedMfg { get; set; }
        
    }

}

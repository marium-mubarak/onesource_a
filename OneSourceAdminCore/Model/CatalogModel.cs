﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Feb 09 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of catalog alert to view"
    /// </summary>
    public class CatalogModel
    {
        public List<CustomCatalogAlert> CatalogAlerts { get; set; }
        public List<sp_getSearchCatalogSKU_R_Result> CatalogSearchModel { get; set; }
        public string ManufacturerSku { get; set; }
        public string ManufacturerPartId { get; set; }
        public List<GenericList> CustomerList { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
    }
    public class CustomCatalogAlert
    {
        public int CatalogAlertId { get; set; }
        public int? CustomerId { get; set; }
        public int ManufacturerPartId { get; set; }
        public string SKU { get; set; }
        public string Alert { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public bool IsTopStory { get; set; }
        public string customerName { get; set; }


    }




}
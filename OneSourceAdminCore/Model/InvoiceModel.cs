﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class InvoiceModel
    {
        public string InvoiceNumber { get; set; }
        public string PoNumber { get; set; }
        public string OrderTrackingNumber { get; set; }
        public string OrderPoNumber { get; set; }
        public int SentToCompanyId { get; set; }
        public int OnBehlafCompanyId { get; set; }
        public int StatusId { get; set; }
        public int TypeId { get; set; }
        public string CreatedAfter { get; set; }
        public string CreatedBefore { get; set; }
        public string SentAfter { get; set; }
        public string SentBefore { get; set; }
        public string RecievedAfter { get; set; }
        public string RecievedBefore { get; set; }
        public List<GenericList> SentToList { get; set; }
        public List<GenericList> OnBehalfOfList { get; set; }
        public List<GenericList> StatusList { get; set; }
        public List<GenericList> TypeList { get; set; }
    }



    public class InvoicedetailsModel
    {
        //Sunny Gohar (13th August 2018)
        public InvoicedetailsModel()
        {
            //initialize select lists (Sunny Gohar)
            InvoiceOBOCompanies = new List<SelectListItem>();
            CurrencyList = new List<SelectListItem>();
            StatusList = new List<SelectListItem>();
            InvoiceOwnerList = new List<SelectListItem>();
            BillfromAddressList = new List<SelectListItem>();
            BillToAddressList = new List<SelectListItem>();
            PayToAddressList = new List<SelectListItem>();
            PaymentTermsList = new List<SelectListItem>();
            PaymentMethodList = new List<SelectListItem>();
        }

        public Sp_GetInvoiceDetails_AdminR_Result InvoiceInfo { get; set; }
        public List<SelectListItem> CurrencyList { get; set; } //Changed to select list item type (Sunny Gohar : 13th Aug 2018)
        public List<SelectListItem> StatusList { get; set; } //Changed to select list item type (Sunny Gohar : 13th Aug 2018)
        public List<SelectListItem> InvoiceOwnerList { get; set; }  //Changed to select list item type (Sunny Gohar : 15th Aug 2018)
        public List<SelectListItem> PaymentTermsList { get; set; }//Changed to select list item type (Sunny Gohar : 15th Aug 2018)
        public List<SelectListItem> PaymentMethodList { get; set; }//Changed to select list item type (Sunny Gohar : 15th Aug 2018)
        public List<SelectListItem> BillToAddressList { get; set; } //Changed to select list item type(Sunny Gohar : 15th Aug 2018)
        public List<SelectListItem> BillfromAddressList { get; set; } //Changed to select list item type(Sunny Gohar : 15th Aug 2018)
        public List<SelectListItem> PayToAddressList { get; set; }//Changed to select list item type(Sunny Gohar : 15th Aug 2018)

        //Sunny Gohar (13th August 2018)
        public List<SelectListItem> InvoiceOBOCompanies { get; set; }

        public bool EnableStatusDropDown { get; set; }

        public int GspMonthlyId { get; set; }

        public double InvoiceTotalPayment { get; set; }

        public int InvoiceId { get; set; }

        public string invoicePayToAddressListDrp { get; set; }
        public string invoiceCurrency { get; set; }
    }

    public class InvoiceDetailStatuses
    {
        public InvoiceDetailStatuses()
        {
            List = new List<SelectListItem>();
        }

        public bool EnableDropdown { get; set; }
        public List<SelectListItem> List { get; set; }
    }


    public class NewInvoiceModel
    {
        //Sunny Gohar (31st August 2018)
        public NewInvoiceModel()
        {
            InvoiceTypeList = new List<SelectListItem>();
            InvoiceTypeActiveOrInActiveList = new List<SelectListItem>();
            BillFromCompanyList = new List<SelectListItem>();
            BillToCompanyList = new List<SelectListItem>();
            InvoiceRecipientList = new List<SelectListItem>();
            InvoiceDetailColumnsList = new List<string>();
            ReOccursList = new List<SelectListItem>();
        }

        public tblInvoiceType InvoiceType { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public List<SelectListItem> InvoiceOwnerList { get; set; }
        public List<SelectListItem> PaymentTermsList { get; set; }
        public List<SelectListItem> PaymentMethodList { get; set; }
        public List<SelectListItem> BillToAddressList { get; set; }
        public List<SelectListItem> BillfromAddressList { get; set; }
        public List<SelectListItem> PayToAddressList { get; set; }

        public List<string> InvoiceDetailColumnsList { get; set; }
        //Sunny Gohar (31st August 2018)
        public List<SelectListItem> InvoiceOBOCompanies { get; set; }

        public List<SelectListItem> ReOccursList { get; set; }

        public bool EnableStatusDropDown { get; set; }

        public int GspMonthlyId { get; set; }

        public string InvoiceTotalPayment { get; set; }

        public int InvoiceId { get; set; }
        public int InvoiceTypeId { get; set; }

        public string invoiceCurrency { get; set; }

        public List<SelectListItem> InvoiceTypeList { get; set; }
        public List<SelectListItem> InvoiceTypeActiveOrInActiveList { get; set; }

        public List<SelectListItem> BillFromCompanyList { get; set; }
        public List<SelectListItem> BillToCompanyList { get; set; }
        public List<SelectListItem> InvoiceRecipientList { get; set; }
        public int BillFromCompany { get; set; }
        public string SelectedProcessType { get; set; }
        public string SelectedTab { get; set; }
        public string CreateDate { get; set; }
        public string StartDate { get; set; }
        public string PaymentInstructions { get; set; }
        public string Comments { get; set; }
        public string TaxDate { get; set; }
        public string DueDate { get; set; }
        public string EndDate { get; set; }
        public string InvoiceTotalUsd { get; set; }
        public tblInvoiceType InvoiceTypeinfo { get; set; }
        public string Description { get; set; }
        public string OrderNumber { get; set; }
        public string OrderTotal { get; set; }
        public string GSFee { get; set; }
        public string PO { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public string VAT { get; set; }
        public string Amount { get; set; }
        public string Ticket { get; set; }
        public string invoiceBillFromCompanyDrp { get; set; }
        public string invoiceBillToCompanyDrp { get; set; }

        public string invoicePayToAddressListDrp { get; set; }

        public string invoiceOBOCompanyDrp { get; set; }

        public string invoiceCurrencyDrp { get; set; }

        public string InvoiceVatRate { get; set; }

        public string invoiceOwnerDrp { get; set; }

        public string invoiceBillFromAddressDrp { get; set; }

        public string invoiceBillToAddressDrp { get; set; }

        public string invoicePaymentTermDrp { get; set; }

        public string invoicePaymentMethodListDrp { get; set; }

        public string VATReg { get; set; }

        public string invoiceRecipientListDrp { get; set; }
        public string ColumnNames { get; set; }
        public int RowCount { get; set; }

        public string InvoiceTemplateName { get; set; }
        public string InvoiceAmount { get; set; }
        public string InvoiceAmountUSD { get; set; }

    }

    public class InvoiceLineItemDetailsModel
    {
        public string Description { get; set; }
        public string OrderNumber { get; set; }
        public string OrderTotal { get; set; }
        public string GSFee { get; set; }
        public string PO { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public string VAT { get; set; }
        public string Amount { get; set; }
        public string Ticket { get; set; }
        
        public string TotalValue { get; set; }

        public string LineItemSeqNum { get; set; }
    }

}

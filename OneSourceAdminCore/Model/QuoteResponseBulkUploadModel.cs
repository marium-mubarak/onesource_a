﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for bulk upload responses"
    /// </summary>
    public class QuoteResponseBulkUploadModel
    {
        public List<UploadQuoteResponseModel> UploadQuoteResponse { get; set; }
        public string ErrorMsg { get; set; }
    }
}

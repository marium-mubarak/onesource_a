﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company manufacturer info"
    /// </summary>
    public class CompanyManufacturerInfoModel
    {
        public List<GenericList> DateFormates { get; set; }
        public List<GenericList> MfgExchangeRates { get; set; }
        public bool GivesAccreditation { get; set; }
        public bool HasExchangeRates { get; set; }
        public DateTime LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public int ExRateFromMFG { get; set; }
        public int DateFormatId { get; set; }
        public int ParentCompanyId { get; set; }
    }
}

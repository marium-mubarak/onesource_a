﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{

    /// <summary>   
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of currency budget"
    /// </summary>
    public class BudgetCurrencyModel
    {
        public int CurrencyID { get; set; }
        public decimal BudgetAmount { get; set; }
        public string Duration { get; set; }
    }
}

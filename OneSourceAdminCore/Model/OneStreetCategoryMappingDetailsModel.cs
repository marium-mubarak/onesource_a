﻿using OneSourceAdminCore.Entity;
using System.Collections.Generic;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : May 10 2019
    /// This class is responsible for taking data of one street category mapping details to view"
    /// </summary>
    public class OneStreetCategoryMappingDetailsModel
    {
        public List<tblPartCategory> Categories { get; set; }
        public List<Sp_OneStreetCategoryMappingDetails_AdminR_Result> MappingDetails { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : July 7 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of exchange rate alert to view"
    /// </summary>
    public class ExchageRateModel
    {
        public List<GenericList> Countries { get; set; }
        public List<CustomExchangeRateAlert> ExchangeRateAlerts { get; set; }
        public List<GenericList> Customers { get; set; }
    }
   public class CustomExchangeRateAlert
   {
       public int ExchangeRateAlertId { get; set; }
       public int CountryId { get; set; }
       public string CountryName { get; set; }
       public string Alert { get; set; }
       public string strStartDate { get; set; }
       public string strEndDate { get; set; }
       public System.DateTime StartDate { get; set; }
       public Nullable<System.DateTime> EndDate { get; set; }
       public string FileName { get; set; }
       public string FilePath { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}

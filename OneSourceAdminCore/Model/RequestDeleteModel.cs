﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for request data that need to delete"
    /// </summary>
    public class RequestDeleteModel
    {
        public int Count { get; set; }
        public string PriorityName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : july 12 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of customer alert to view"
    /// </summary>
    public class CustomerModel
    {
        public List<GenericList> Customers { get; set; }
        public List<CustomCustomerAlert> CustomerAlerts { get; set; }
    }
    public class CustomCustomerAlert
    {
        public int CustomerAlertId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Alert { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        public bool IsTopStory{ get; set; }
    }
}

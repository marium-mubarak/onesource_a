﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Feb 8 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of currency alert to view"
    /// </summary>
   public class CurrencyModel
    {
        public List<GenericList> Currencies { get; set; }
        public List<CustomCurrencyAlert> CurrencyAlerts { get; set; }
        public List<GenericList> Customers { get; set; }
    }
   public class CustomCurrencyAlert
   {
       public int CurrencyAlertId { get; set; }
       public int CurrencyId { get; set; }
       public string CurrencyName { get; set; }
       public string Alert { get; set; }
       public string strStartDate { get; set; }
       public string strEndDate { get; set; }
       public System.DateTime StartDate { get; set; }
       public Nullable<System.DateTime> EndDate { get; set; }
       public bool IsTopStory { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}

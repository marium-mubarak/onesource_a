﻿namespace OneSourceAdminCore.Model
{
    public class CatalogReportMainModel
    {
        public int CatalogStandardID { get; set; }
        public string CountryIDList { get; set; }
        public string Countries { get; set; }
        public string Description { get; set; }
        public string CatalogDescription { get; set; }
        public string CustomerName { get; set; }
        public int CatalogPartID { get; set; }
        public string ManufacturerSKU { get; set; }
        public string PartDescription { get; set; }
        public int Quantity { get; set; }
        public double GlobalPrice { get; set; }
        public string Currency { get; set; }
        public bool IsOption { get; set; }

    }
}

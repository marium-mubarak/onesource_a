﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class PaymentModel
    {
        public string PaymentId { get; set; }
        public int ParentCompanyId { get; set; }
        public string InvoiceId { get; set; }
        public string VendorPaymentId { get; set; }
        public int PaidBy { get; set; }
        public int PaidTo { get; set; }
        public int PaymentMethodSelected { get; set; }
        public int CurrencySelected { get; set; }
        public int PaymentTypeSelected { get; set; }
        public string PaidAfter { get; set; }
        public string PaidBefore { get; set; }
        public sp_SearchPayment_AdminR_Result SelectedRecord { get; set; }
        public tblPayment newPaymentModel { get; set; }

        public List<GenericList> PaidByCompanyList { get; set; }
        public List<GenericList> PaidToCompanyList { get; set; }
        public List<GenericList> PaymentMethod { get; set; }
        public List<GenericList> PaymentTxType { get; set; }
        public List<GenericList> CurrencyList { get; set; }

        public List<List<SelectListItem>> AllInvoices { get; set; }
        public List<SelectListItem> NewInvoices { get; set; }
        public List<InvoiceDetailModel> SelectedInvoices { get; set; }
        public decimal PaymentTotal { get; set; }
        public decimal Balance { get; set; }

        public List<sp_SearchPayment_AdminR_Result> SearchRecords { get; set; }
    }

    public class InvoiceDetailModel
    {
        public int InvoiceId { get; set; }
        public string CompanyInvoiceId { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }
}

﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class CustomerAddressReportModel
    {
        public CustomerAddressReportModel()
        {
            ListAddressType = new List<GenericList>();
            ListAddressType.Add(new GenericList { Key = 0, Value = "--Select--", Count = 1 });
            ListAddressType.Add(new GenericList { Key = 1, Value = "Destination", Count = 1 });
            ListAddressType.Add(new GenericList { Key = 2, Value = "Invoice", Count = 1 });
            ListAddressType.Add(new GenericList { Key = 3, Value = "Both", Count = 1 });
        }

        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public List<GenericList> ListCountries { get; set; }
        public List<GenericList> ListAddressType { get; set; }
        public List<CustomerAddressReportOutput> ListOutputData { get; set; }
        public int CustomerId { get; set; }
        public int CountryId { get; set; }
        public int AddressTypeId { get; set; }
    }

    public class CustomerAddressReportOutput
    {
        public string CondensedAddress { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }

    }
}

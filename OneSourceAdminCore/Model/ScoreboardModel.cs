﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class ScoreboardModel
    {
        public List<GenericStringList> FiltersList { get; set; }
        public bool InternalRequest { get; set; }
        public bool SystemRequest { get; set; }
        public string DefaultFilterValue { get; set; }
 
    }

    public class ScoreBoardListingModel
    {
        public int TicketId { get; set; }
        public string TypeName { get; set; }
        public string DateReceived { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
        public Nullable<int> SlaInstanceId { get; set; }
        public string SlaName { get; set; }
        public Nullable<int> MinutesToBreach { get; set; }
        public string CSR { get; set; }
        public string GSP { get; set; }
        public string Country { get; set; }
        public string Mfg { get; set; }
        public string Model { get; set; }
        public string Coverage { get; set; }
        public string LastUpdated { get; set; }
        public string LastUpdateBy { get; set; }
        public string Country2 { get; set; }
        public int CustomerId { get; set; }
    }
}

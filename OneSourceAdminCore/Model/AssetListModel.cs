﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : July 12 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing asset list detail"
    /// </summary>
    public class AssetListModel
    {
        public List<sp_getAssetSearch_AdminR_Result> LstAsset { get; set; }
        public int TotalRecord { get; set; }
    }
}

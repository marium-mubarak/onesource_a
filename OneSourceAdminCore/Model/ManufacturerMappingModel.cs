﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Oct 28 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of Manufacturer Mapping to view"
    /// </summary>
    public class ManufacturerMappingModel
    {
        public List<tblManufacturer> Manufacturers { get; set; }
        public List<sp_OneStreet_GetUnmappedManufacturers_Result> UnmappedManufacturer{ get; set; }
        public List<sp_OneStreet_GetManufacturerMappingDetail_Result> MappingDetail { get; set; }
    }
}

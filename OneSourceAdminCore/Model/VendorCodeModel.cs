﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : Feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for Vendor Code detail"
    /// </summary>
    public class VendorCodeModel
    {
        public List<GenericList> Countries{ get; set; }
        public List<GenericList> Suppliers { get; set; }
        public tblXrefSFA vendorCodeInfo { get; set; }
        public List<GenericList> XrefTypes { get; set; }
        public int VCode{ get; set; }
    }
}

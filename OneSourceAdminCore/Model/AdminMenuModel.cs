﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class AdminMenuModel
    {
        public List<tblAdminMenu> MenuList { get; set; }
        public tblAdminMenu AdminMenu { get; set; }
        public List<GenericList> UserGroupList { get; set; }
        public List<AdminRoleMenuMappingModel> RoleMenuList { get; set; }
        public List<GenericStringList> UserGroupType { get; set; }
        public List<GenericList> EntityTypeList { get; set; }
        public List<GenericList> EntityUsersList { get; set; }

    }


    public class AdminRoleMenuMappingModel
    {
        public int UserGroupId { get; set; }
        public string UserGroupName { get; set; }
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public string ParentMenu { get; set; }
        public bool IsDefaultMenu { get; set; }
        public bool ViewOnlyAccess { get; set; }
        public bool AddEditAccess { get; set; }

    }

    public class CheckAllMenuRights
    {
        public int MenuId { get; set; }
        public int RoleId { get; set; }
        public bool SelectAll { get; set; }

    }

    public class CheckAllUserRestrictions
    {
        public int MenuId { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public bool Accessiable { get; set; }
        public string UserType { get; set; }
        public int EntityTypeId { get; set; }

    }


    public class AdminMenuAndAccessModel
    {
        public List<tblAdminMenu> AdminMenu { get; set; }
        public List<UserAccessCheckModel> UserAccessCheck { get; set; }

    }

    public class UserAccessCheckModel
    {
        public int MenuId { get; set; }
        public string DataToken { get; set; }
        public bool ViewAccess { get; set; }
        public bool AddEditAccess { get; set; }
    }

}

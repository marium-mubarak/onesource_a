﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class AssetReportModel
    {
        public AssetReportModel()
        {
            ListAddresses = new List<SelectListItem>();
            ListCustomers = new List<SelectListItem>();

        }

        public List<SelectListItem> ListCustomers { get; set; }

        public List<SelectListItem> ListAddresses { get; set; }

        public List<SelectListItem> ListManufacturers { get; set; }

        public int CustomerId { get; set; }
        public int CompanyId { get; set; }
        public string CountryIDs { get; set; }
        public string AddressId { get; set; }
        public string AddressIDs { get; set; }
        public string ManufacturerIDs { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }


        public int routeId { get; set; }
        public string routeName { get; set; }
        public string ReportName { get; set; }
        public int parentRouteId { get; set; }
        public string parentName { get; set; }
        public string parentRoute { get; set; }
        public AssetReportModel TempAssetLocationModel { get; set; }
        public DataTable TempAssetLocationTable { get; set; }

        
    }

    public class AssetManufacturerClass
    {
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }

}
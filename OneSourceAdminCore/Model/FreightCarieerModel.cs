﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for Freight Carieer detail."
    /// </summary>
    public class FreightCarieerModel
    {
        public tblFreightCarrier FreightCarrier { get; set; }
        public string UserName{ get; set; }
    }
}

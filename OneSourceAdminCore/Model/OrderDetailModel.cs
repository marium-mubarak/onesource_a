﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing order detail"
    /// </summary>
    public class OrderDetailModel
    {
        public sp_getOrderDetail_AdminR_Result OrderDetail{ get; set; }//used for ISD
        //public sp_getOrderDetail_SFA_AdminR_Result OrderDetailSFA { get; set; }//used for SFA
        public List<GenericList> GSPList{ get; set; }
        public List<GenericList> OrderStatusList { get; set; }
        public string ShippingMethod { get; set; }
        public int SessionTrackingNumber { get; set; }
        public short OrderTrackingNumber { get; set; }
        public short DestinationTrackingNumber { get; set; }
        public DateTime? DateAcceptedBySupplier { get; set; }
        public int QuoteId{ get; set; }
        public int SMBQuoteId { get; set; }
        public string BasketName{ get; set; }
        public string CreatedBy{ get; set; }
        public string StatusCode { get; set; }

        #region this field is added for GS Buyer Info detail by adnan nooruddin 27-june-2019
        public string VAT { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public string CorporateName { get; set; }
        #endregion
    }
}

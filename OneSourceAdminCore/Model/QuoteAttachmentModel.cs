﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OneSourceAdminCore.Entity;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class QuoteAttachmentModel
    {
        public List<sp_getQuoteAttachments_AdminR_Result> Attachments { get; set; }
        public string quoteType { get; set; }
    }
}

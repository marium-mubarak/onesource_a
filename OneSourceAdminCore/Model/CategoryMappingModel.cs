﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Oct 28 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of Category Mapping to view"
    /// </summary>
    public class CategoryMappingModel
    {
        public List<sp_OneStreet_GetUnmappedCategories_Result> UnmappedCategories { get; set; }
        public List<tblPartCategory> Categories{ get; set; }
        public List<sp_OneStreet_GetCategoryMappingDetail_Result> MappingDetail { get; set; }
    }
}

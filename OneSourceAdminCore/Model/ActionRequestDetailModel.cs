﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : feb 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing action request detail"
    /// </summary>
    public class ActionRequestDetailModel
    {
        public tblActionRequestType ActionRequest { get; set; }
        public List<sp_getTicketStatus_AdminR_Result> TicketStatus{ get; set; }
        public string UserName{ get; set; }

        public int CompanyId{ get; set; }
    }
}

﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class SoftwareQuoteModel : tblSoftwareQuote
    {
        public string Currency { get; set; }
        public string Country { get; set; }
        public string HQOption { get; set; }
        public string Supplier { get; set; }
        public string SupplierEmail { get; set; }
        public int LineItems { get; set; }
    }
}

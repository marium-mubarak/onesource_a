﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
   public class CompanySupplierAddSetupModel
    {
        public int Id{ get; set; }
        public string Name{ get; set; }
        public decimal? DistributionUplift { get; set; }
        public int? TierId{ get; set; }
        public string Comments{ get; set; }
    }
}

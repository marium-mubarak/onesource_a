﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company as manufacturer"
    /// </summary>
    public class CompanyManufacturerModel
    {
        public CompanyManufacturerInfoModel MfgInfo { get; set; }
        public CompanyManufacturerAddressModel MfgAddress { get; set; }
        public List<sp_getManufacturerUsers_AdminR_Result> mfgUsers { get; set; }
        public CompanyManufacturerExchangeRate mfgExchangeRate { get; set; }
        public List<sp_getCompanyContacts_AdminR_Result> mfgContacts { get; set; }


    }
}

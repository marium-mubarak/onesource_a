﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Aug 28 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for getting data of SLA history to view"
    /// </summary>
    public class SLAHistoryModel
    {
        public sp_getSLAHistory_AdminR_Result SLAHistory  { get; set; }
        public List<GenericList> BreachCauses{ get; set; }
    }
}

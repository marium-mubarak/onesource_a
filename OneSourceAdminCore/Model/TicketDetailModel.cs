﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : June 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for ticket detail"
    /// </summary>

    public class TicketDetailModel
    {
        public int TicketId { get; set; }
        public sp_getTicketDetail_AdminR_Result TicketDetail { get; set; }
        public List<sp_getTimeZone_AdminR_Result> TimeZone { get; set; }
        public List<GenericList> CSRList { get; set; }
        public List<GenericList> RequestType { get; set; }
        public List<GenericList> Priorities { get; set; }
        public List<GenericList> TicketStatuses { get; set; }
        public List<GenericList> AssignSuppliers{ get; set; }
        public List<GenericList> AssetManufacturer { get; set; }
        public List<GenericList> Currencies{ get; set; }
        public tblWorldTimeServer WorldTimeServer{ get; set; }
        public List<ServiceLevel> ServiceLevel { get; set; }
        public List<int> CloseTicketStatusIds { get; set; }
        public List<SupplierContact> SupplierContacts{ get; set; }
        public string tmpmodel { get; set; }
        public int tmpmfgid { get; set; }
        public int IsCustomer{ get; set; }
        public bool ServerDST { get; set; }
        public int TimeZoneId{ get; set; }
    }
    public class ServiceLevel {
        public int slaInstanceID { get; set; }
        public string SLAName { get; set; }
        public string tacname { get; set; }
    }
    public class TicketAdditionalUpdateModel {
        public string tmpmodel { get; set; }
        public int tzbias { get; set; }
        public int requesthours { get; set; }
        public int requestminutes { get; set; }
        public int closehours { get; set; }
        public int closeminutes { get; set; }
        public int appointmenthours { get; set; }
        public int appointmentminutes { get; set; }
        public bool isactive { get; set; }
        public int tmpmfgid { get; set; }
        public int requesttimezoneid { get; set; }
        public int addressid { get; set; }
        public string comments{ get; set; }
    }
    public class TicketStatusScheduleModel
    {
        public int TicketId{ get; set; }
        public List<tblTicketStatu> TicketStatusList{ get; set; }
        public List<tblTktStatusSchedule> TktStatusScheduleList { get; set; }
    }
    public class AssetDataGridModel
    {
        public string SerialNumber { get; set; }
        public string AssetTag { get; set; }
        public string AssetId { get; set; }
        public string AssetLocation { get; set; }
        public string AssetDescription { get; set; }
        public int CompanyId{ get; set; }
        public List<Sp_GetAssetPopupGrid_AdminR_Result> AssetDataGridDataList { get; set; }
    }

    public class AssetTagAddress
    {
        public int CustomerAddressId { get; set; }
        public string CustomerAddress { get; set; }
        public int CountryId { get; set; }
    }
    public class AssetPopupModel
    {
        public int ParentCompanyId{ get; set; }
        public string SerialNumber { get; set; }
        public int AssetLocationId { get; set; }
        public int AssetStatusId { get; set; }
        public string AssetCategoryIds { get; set; }
        public string AssetManufactureIds { get; set; }
        public string AssetPartNumber { get; set; }
        public string AssetTag { get; set; }
        public List<GenericList> AssetStatuses { get; set; }
        public List<AssetTagAddress> AssetTagAddress { get; set; }
        public List<GenericList> AssetManufacturList { get; set; }
        public List<GenericList> AssetCategoryList { get; set; }
        
    }
    public class SupplierContact
    {
        public string Email { get; set; }
        public string ContactName { get; set; }
        public string SFAPhone { get; set; }
        public string Title { get; set; }
    }
    
}

﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of SLA setting to view"
    /// </summary>
    public class SLASettingModel
    {
        public List<CustomerCompanyClass> Customers { get; set; }
        public List<sp_GetUnselectedSLA_R_Result> LstUnselectedSLA { get; set; }
        public List<GenericList> LstSelectedSLA { get; set; }
        public string CustomerId{ get; set; }
        public bool Viewable { get; set; }

    }
    public class SLAModel
    {
        public List<string> selectedSLA { get; set; }
        public List<string> unselectedSLA { get; set; }
        public int CustomerId { get; set; }
        public bool Viewable { get; set; }
    }

}

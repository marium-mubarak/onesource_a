﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing quote request and response detail"
    /// </summary>
    public class QuoteRequestResponseModel
    {
        public QuoteRequestResponseModel()
        {
            Categories = new List<sp_getQuoteReqResCategories_AdminR_Result>();
        }
        public List<tblPartQuoteRequest> PartQuoteRequests { get; set; }
        public  List<tblQuoteRequest> QuoteRequests { get; set; }
        public List<QuoteRequestModel> QuoteRequestList { get; set; } //added (SUNNY GOHAR)
        public List<sp_getQuoteResponse_New_AdminR_Result>  NewQuoteResponse{ get; set; }
        public List<sp_getQuoteResponse_New_SFA_AdminR_Result> NewQuoteResponse_SFA { get; set; }
        public List<sp_getQuoteResponse_Old_AdminR_Result> OldQuoteResponse { get; set; }
        public string QuoteStatusCode{ get; set; }
        public string CurrencySymbol{ get; set; }
        public int CustomerId { get; set; }
        public List<GenericList> Manufacturers{ get; set; }
        public List<sp_getQuoteReqResCategories_AdminR_Result> Categories { get; set; }
        public string quoteType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing quote list detail"
    /// </summary>
    public class QuoteListModel
    {
        public List<sp_getQuoteSearch_AdminR_Result> LstQuotes{ get; set; }
        public int TotalRecord { get; set; }
        public string quoteType { get; set; }
    }
}

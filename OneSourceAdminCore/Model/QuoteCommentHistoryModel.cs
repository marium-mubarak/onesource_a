﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for quote comment history detail"
    /// </summary>
    public class QuoteCommentHistoryModel
    {
        public int QuoteId{ get; set; }
        public List<sp_getQuoteHistory_AdminR_Result> QuoteHistory{ get; set; }
        public List<sp_getQuoteGSComment_AdminR_Result> GSQuoteComment{ get; set; }
        public string quoteType { get; set; }
        public string JiraTicketNumber { get; set; }
    }
}

﻿using OneSourceAdminCore.Entity;
using System.Collections.Generic;

namespace OneSourceAdminCore.Model
{
    public class OneStreetManufacturerMappingDetailsModel
    {
        public List<tblManufacturer> Manufacturers { get; set; }
        public List<Sp_OneStreetManufacturerMappingDetails_AdminR_Result> MappingDetails { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : dec 22 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for national holiday detail"
    /// </summary>
    public class HolidayModel
    {
        public int CountryID { get; set; }
        public string holidayName { get; set; }
        public int month { get; set; }
        public int day { get; set; }
      

    }
}

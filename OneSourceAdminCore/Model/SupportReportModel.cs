﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class SupportReportModel
    {
        public SupportReportModel()
        {
            ListCustomers = new List<CustomerCompanyClass>();

        }
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public int CustomerId { get; set; }
        public int CompanyId { get; set; }
        public string CountryIDs { get; set; }
        public string RequestTypeIDs { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<GenericList> SFAsList { set; get; }
        public List<GenericList> ManufacturerList { get; set; }
    }
}

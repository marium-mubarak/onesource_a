﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 27 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for sfa uplifts detail"
    /// </summary>
    public class OneStreetSFAsUpliftsModel
    {
        public OneStreetSFAsUpliftsModel()
        {
            Customers = new List<SelectListItem>();
        }

        public int OneStreetUplift { get; set; }
        public List<GenericList> Countries{ get; set; }
        public List<GenericList> SFAsSuppliers { get; set; }

        public decimal? GSUplift { get; set; }
        public decimal? GSPUplift { get; set; }
        public int Country{ get; set; }
        public int SupplierID{ get; set; }

        public List<sp_getOneStreetSFAsUpliftsInfo_AdminR_Result> supplierDetail  { get; set; }

        public List<SelectListItem> Customers { get; set; }
        public int CustomerId { get; set; }


    }
}

﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : june 19 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing ticket list detail"
    /// </summary>
    public class TicketListModel
    {
        public List<sp_getTicketSearch_AdminR_Result> LstTicket { get; set; }
        public int TotalRecord { get; set; }
        public string TicketType { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : June 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for ticket history"
    /// </summary>
    public class TicketHistoryModel
    {
        public string resolution{ get; set; }
        public List<TicketHistory> LstTicketHistory { get; set; }
    }

    /// <summary>
    /// Date created : June 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for ticket history"
    /// </summary>
    public class TicketHistory
    {
        public DateTime Logdate { get; set; }
        public string UserName { get; set; }
        public string ActionName { get; set; }
        public string TktStatusName { get; set; }
        public bool IsPublic { get; set; }
        public int TicketLogID { get; set; }
        public string Comments{ get; set; }
    }
}

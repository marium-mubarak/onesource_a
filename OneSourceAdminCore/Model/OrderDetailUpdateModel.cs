﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for taking data that will be updated"
    /// </summary>
    public class OrderDetailUpdateModel
    {
        public int SessionTrackingNumber { get; set; }
        public short OrderTrackingNumber { get; set; }
        public short DestinationTrackingNumber { get; set; }
        public string OrderPO { get; set; }
        public System.DateTime OrderDate { get; set; }  
        public Nullable<decimal> ShippingAmount { get; set; }
        public string DestinationPO { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public Nullable<System.DateTime> DateAcceptedBySupplier { get; set; }
    }
}

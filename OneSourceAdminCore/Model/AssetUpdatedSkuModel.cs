﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class AssetUpdatedSkuModel
    {
        public int? SessionTrackingNumber { get; set; }
        public string Sku { get; set; }
    }

    public class ModelToUpdate
    {
        public List<AssetUpdatedSkuModel> ListOfModel { get; set; }

        public string NotUpdateMessage { get; set; }

        public string UpdateMessage { get; set; }

        public string FileError { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model class is responsible for part category detail"
    /// </summary>
    public class PartCategoryModel
    {
        public List<GenericList> Companies{ get; set; }
        public string PartCategoryName{ get; set; }
        public int? CompanyId{ get; set; }
        public bool IsActive{ get; set; }
        public DateTime LastUpdated { get; set; }
        public string UserName{ get; set; }
        public int PCId{ get; set; }

    }
}

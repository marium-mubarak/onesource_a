﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace OneSourceAdminCore.Model
{
    public class ADUploadModel
    {
        public string FileName { get; set; }
        public int VersionNumber { get; set; }
        public string CustomerId { get; set; }
        public string CategoryId { get; set; }
        public string CustomerName { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int TicketId { get; set; }
        public int CompanyId { get; set; }
        public string BusinessSegment { get; set; }
        public string ADPickUpDate { get; set; }
        public List<ADFileModel> listADUpload { get; set; }
        public List<ADFileDetailInfo> ListFileInfo { get; set; }
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public DataTable ErrorDataTable { get; set; }
        public DataTable UploadDataTable { get; set; }
        public List<GenericList> OSCategoriesList { get; set; }
        public List<GenericStringList> UnmappedCategoiriesList { get; set; }
    }

    public class ADFileModel
    {
        public int FileID { get; set; }
        public string CustomerName { get; set; }
        public string SFAName { get; set; }
        public string Representative { get; set; }
        public DateTime? Uploaded_Date { get; set; }
        public DateTime? ADPickup_Date { get; set; }
        public string Status { get; set; }
        public int TicketID { get; set; }
    }

    public class ADFileDetailInfo
    {
        public int? UnitID { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Category { get; set; }
        public string CPU { get; set; }
        public string Screen_Size { get; set; }
        public string Status { get; set; }
        public string RAM { get; set; }
        public string HDD { get; set; }
        public decimal? Processing_Fees { get; set; }
        public decimal? AR_Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : feb 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing action request ticket status detail"
    /// </summary>
    public class ActionRequestTicketStatusModel
    {
        public List<sp_getCompanyByParentCompanyAndRoleName_AdminR_Result> Companies{ get; set; }
        public List<sp_getTicketStatus_AdminR_Result> lstTicketStatus { get; set; }
        public List<sp_getActionRequests_AdminR_Result> lstActionRequests { get; set; }
        public int CompanyId { get; set; }
        public int editType { get; set; }
    }
}

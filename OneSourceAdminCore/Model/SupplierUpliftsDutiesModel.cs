﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : July 25 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for having data of supplier uplifts and duties"
    /// </summary>
    public class SupplierUpliftsDutiesModel
    {
        public string TradeBlockUplift { get; set; }
        public string Duties { get; set; }
        public string DistributionUplift { get; set; }
        public string Manufacturer{ get; set; }
        public string Country { get; set; }
    }
}

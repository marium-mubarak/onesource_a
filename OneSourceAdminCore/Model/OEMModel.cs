﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : july 12 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of oem alert to view"
    /// </summary>
    public class OEMModel
    {
        public List<GenericList> manufacturers { get; set; }
        public List<CustomOEMAlert> OEMAlerts { get; set; }
        public List<GenericList> customers { get; set;  }
    }
    public class CustomOEMAlert
    {
        public int OEMAlertId { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string Alert { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public bool IsTopStory { get; set; }
        public int? CustomerId{ get; set; }
        public string CustomerName {get;set;}

    }
}

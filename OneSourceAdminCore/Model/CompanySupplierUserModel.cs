﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class CompanySupplierUserModel
    {
        public List<GenericList> DateFormates { get; set; }
        public int ContactId { get; set; }
        public CompanySupplierUserInfo UserInfo { get; set; }
    }
    public class CompanySupplierUserInfo
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public int SFAID { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public string LastUpdatedBy { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int IsActive { get; set; }
        public Nullable<int> DateFormatID { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : June 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for ticket search view data"
    /// </summary>
    public class TicketSearchModel
    {
        public List<sp_getSubmittedByTicketSearch_AdminR_Result> SubmittedBy { get; set; }
        public List<sp_getAffectedUserTicketSearch_AdminR_Result> AffectedUsers { get; set; }
        public List<sp_getCompanyTicketSearch_AdminR_Result> Companies { get; set; }
        public List<sp_getPriorityTicketSearch_AdminR_Result> Priorities { get; set; }
        public List<sp_getRequestTypeTicketSearch_AdminR_Result> RequestTypes { get; set; }
        public List<sp_getLocationTicketSearch_AdminR_Result> Locations { get; set; }
        public List<sp_getStatusTicketSearch_AdminR_Result> Statuses { get; set; }
        public List<sp_getCountryTicketSearch_AdminR_Result> Countries { get; set; }
        public List<sp_getAssignedSupplierSearch_AdminR_Result> AssignedSuppliers { get; set; }
        public List<sp_getAssignedCSMSearch_AdminR_Result> AssignedCSM { get; set; }
        public string TicketType{ get; set; }
        public int SubmittedById { get; set; }
        public int PriorityId { get; set; }
        public int AddressId { get; set; }
        public int CountryId { get; set; }
        public int SFAId { get; set; }
        public int CompanyId { get; set; }
        public int AffectedUserContactId { get; set; }
        public int ArtypeId { get; set; }
        public int TktstatusId { get; set; }
        public int CSRId { get; set; }
        public string TicketNumber { get; set; }
        public string STicketNumber { get; set; }
        public string CTicketNumber { get; set; }
        public string SerialNumber { get; set; }
        public string AssetTag { get; set; }
        public string RequestedAfterDate { get; set; }
        public string RequestedBeforeDate { get; set; }
        public string ClosedAfterDate { get; set; }
        public string ClosedBeforeDate { get; set; }
        public string StrLocations { get; set; }
        public string StrStatuses { get; set; }
        public string StrCountries { get; set; }

    }

    public class ADAttachmentDetailModel
    {
        public string ConEmail { get; set; }
        public int CountryId { get; set; }
        public int CompanyId { get; set; }
    
    }
}

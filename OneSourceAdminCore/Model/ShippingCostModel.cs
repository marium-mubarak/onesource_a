﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class ShippingCostModel
    {
        public int ShippingCostID { get; set; }
        public int CustomerID { get; set; }
        public int SupplierID { get; set; }
        public int CategoryID { get; set; }
        public int MinQty { get; set; }
        public int MaxQty { get; set; }
        public decimal ShippingCostInUSD { get; set; }
        public bool IsActive { get; set; }
        public int LastUpdateByUserId { get; set; }
        public DateTime LastUpdated { get; set; }
        public string CustomerName { get; set; }
        public string SupplierName { get; set; }
        public string CategoryName { get; set; }


        public List<SelectListItem> ListCustomer { get; set; }
        public List<SelectListItem> ListSupplier { get; set; }
        public List<SelectListItem> ListPartCategory { get; set; }
        public List<ShippingCostModel> ListShipingCostModel { get; set; }
        public List<sp_getShippingCostInfo_R_Result> ShippingCostList { get; set; }
        public ShippingCostModel()
        {

            ListCustomer = new List<SelectListItem>();
            ListSupplier = new List<SelectListItem>();
            ListPartCategory = new List<SelectListItem>();
            ListShipingCostModel = new List<ShippingCostModel>();
            ShippingCostList = new List<sp_getShippingCostInfo_R_Result>();
        }
    }
}

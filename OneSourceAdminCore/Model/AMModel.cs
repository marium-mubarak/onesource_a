﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of AM Communication"
    /// </summary>
    public class AMModel
    {
        public List<GenericList> Customers { get; set; }
        public List<AMDetailModel> AMDataList { get; set; }
    }
    public class AMDetailModel
    {
        public int tblAMCommunicationId { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string Month { get; set; }

        public int Year { get; set; }

        public int LiveChats { get; set; }

        public int PhoneCalls { get; set; }

        public int Emails { get; set; }

        public System.DateTime LastUpdated { get; set; }

        public int LastUpdateByUser { get; set; }

        public Nullable<System.DateTime> DurationStartDate { get; set; }

        public Nullable<System.DateTime> DurationEndDate { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
    }
}

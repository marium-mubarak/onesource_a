﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Oct 14 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of customer's user to view"
    /// </summary>
    public class OneStreetCustomerUserModel
    {
       public List<sp_GetOneStreetUserInfo_R_Result> OneStreetUserInfo{get;set;}
       public List<GenericList> Customers { get; set; }
    }
}

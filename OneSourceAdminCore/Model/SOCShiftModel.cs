﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Oct 15 2018
    /// Created by : Sunny Gohar
    /// </summary>
    public class SOCShiftModel
    {

        public SOCShiftModel()
        {
            ListSOCShifts = new List<GenericList>();
            ListSOCShifts.Add(new GenericList() { Key = 0, Value = "--Select--" });
            ListCountries = new List<SelectListItem>();
            ListSOCShiftWiseCountry = new List<tblSOCShiftMapping>();
        }

        public List<SelectListItem> ListCountries { get; set; }
        public List<GenericList> ListSOCShifts { get; set; }
        public List<tblSOCShiftMapping> ListSOCShiftWiseCountry { get; set; }
        public int SOCShiftId { get; set; }
        public string SOCShiftName { get; set; }
        public int DefaultSOCShiftId { get; set; }
        public int CountryId { get; set; }

        public string CountryName { get; set; }
        public string CountryIds { get; set; }
        public string CountryNames { get; set; }

        public string TimeZone { get; set; }

    }
    /// <summary>   
    /// Date created : Feb 10 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of country region mapping to view"
    /// </summary>
    public class ParentCountrySOCShiftMapping
    {
        public tblSOCShiftMapping SOCShiftWiseCountry { get; set; }
        public int Count { get; set; }
    }

    public class SocShiftsListModel
    {
        public string SocShift { get; set; }
        public string Country { get; set; }
        public string TimeZone { get; set; }
    }

}

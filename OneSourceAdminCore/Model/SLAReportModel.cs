﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class SLAReportModel
    {
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public List<GenericList> ListManufacturers { get; set; }
        public List<GenericList> ListSubCompanies { get; set; }
        //public List<string> ListBusinessSegments { get; set; }
        public List<GenericList> ListCategories { get; set; }
        public List<GenericList> ListCountries { get; set; }
        public int CustomerId { get; set; }
        public string CustomerIds { get; set; }
        public string ManufacturerIds { get; set; }
        public string CategoryIds { get; set; }
        public string CountryIds { get; set; }
        public string BusinessSegments { get; set; }
        public string RegionIds { get; set; }
        public string CustomerName { get; set; }
        public string ManufacturerNames { get; set; }
        public string SubCompanyNames { get; set; }
        public string Categorynames { get; set; }
        public string CountryNames { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }


        #region removing session work by adnan 
        public int routeId { get; set; }
        public string routeName { get; set; }
        public string reportName { get; set; }
        public int parentRouteId { get; set; }
        public string parentName { get; set; }
        public string parentRoute { get; set; }
        public string excelAction { get; set; }
        public string ReportType { get; set; }
        #endregion


    }
}

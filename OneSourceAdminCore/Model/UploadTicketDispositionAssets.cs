﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class UploadTicketDispositionAssets
    {
        public int DispositionAssetID { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string AssetTag { get; set; }
        public string OEM { get; set; }
        public string Category { get; set; }
        public string ScreenSize { get; set; }
        public string Processor { get; set; }
        public string RAM { get; set; }
        public string HardDrive { get; set; }
        public string ServiceType { get; set; }
        public string GraphicsCardPartNumber { get; set; }
        public string GraphicsCardSerialNumber { get; set; }
        public string RouteSwitchProcessorPartNumber { get; set; }
        public string RouteSwitchProcessorSerialNumber { get; set; }
        public string OpticalDrivePartNumber { get; set; }
        public string OpticalDriveSerialNumber { get; set; }
        public string Comments { get; set; }
        public string RemarketingAmount { get; set; }

    }
}

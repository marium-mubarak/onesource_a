﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for quote detail"
    /// </summary>
    public class QuoteDetailModel
    {
        public sp_getNewQuoteDetail_AdminR_Result NewQuoteDetail{ get; set; }
        public sp_getOldQuoteDetail_AdminR_Result OldQuoteDetail{ get; set; }
        public List<sp_getSFAsByCustomerCountry_AdminR_Result> SFAs { get; set; }
        public string SupplierRef{ get; set; }
        public string QuoteRef{ get; set; }
        public string QuoteType{ get; set; }
        public int QuoteId{ get; set; }
        public decimal? TotalPrice { get; set; }
        public string Symbol{ get; set; }

        public Nullable<int> CustomerId { get; set; } //added for bundle (SUNNY GOHAR)
        public Nullable<int> CurrencyId { get; set; }//added for bundle (SUNNY GOHAR)
        public Nullable<int> CountryId { get; set; } //added for bundle (SUNNY GOHAR)


        //new added
        public List<sp_quoteComment_R_Result> lstQuoteComment { get; set; }
        public List<sp_quoteAttachement_Result> lstQuoteAttachments { get; set; }
        public List<sp_quoteRequest_R_Result> lstQuoteRequest { get; set; }
        public List<sp_quoteDetail_R_Result> lstQuoteDetail { get; set; }
        public List<sp_getQuoteResponses_R_Result> lstQuoteResponses { get; set; }
    
        //
    }
}

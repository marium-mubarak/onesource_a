﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : dec 28 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for managing currency setup"
    /// </summary>
    public class CurrencySetupModel
    {
        public List<sp_getAdminCurrencyList_R_Result> CurrenciesList { get; set; }
        public List<GenericList> CountriesList  { get; set; }
        public List<sp_getISOCurrencies_R_Result> ISOCurrenciesList { get; set; }
    }
}

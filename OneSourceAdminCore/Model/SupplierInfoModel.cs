﻿
using System.Collections.Generic;
using OneSourceAdminCore.Entity;
using System;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : june 1 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company supplier info"
    /// </summary>
    public class SupplierInfoModel
    {
        public int SFAId{ get; set; }
        public int CSROrderId { get; set; }
        public int CSRInvoiceId { get; set; }
        public List<GenericList> OrderFormat{ get; set; }
        public List<GenericList> CSRDataList{ get; set; }
        public List<GenericList> CountryList{ get; set; }
        public List<GenericList> DateTimeList{ get; set; }
        public List<GenericList> AddressList{ get; set; }
        public List<GenericList> ContactList{ get; set; }
        public List<sp_getSupplierCustomerBySFAId_AdminR_Result> CustomerList { get; set; }
        public string CountryName{ get; set; }
        public bool IsGlobal{ get; set; }
        public string Email { get; set; }
        public int ProcureId{ get; set; }
        public int SupportId{ get; set; }
        public int CustServiceId { get; set; }
        public int? DeliverySLADays { get; set; }
        public int? WAMonths { get; set; }
        public string WANumber { get; set; }
        public int? NetPaymentDays { get; set; }
        public int OrderFormatId { get; set; }
        public int DateTimeId { get; set; }
        public int AddressId { get; set; }
        public int ContactId{ get; set; }
        public int SelectedCustomerId { get; set; }
        public int DefaultCountryId{ get; set; }
        public DateTime? Fsadate { get; set; }
        public DateTime? Wadate { get; set; }
        public string ContactIds { get; set; }
        public string AddressIds { get; set; }

    }
}

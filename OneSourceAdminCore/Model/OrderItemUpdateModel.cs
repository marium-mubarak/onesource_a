﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for order item data that should be updated"
    /// </summary>
    public class OrderItemUpdateModel
    {
        public int GSP { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }

        public DateTime? DateShipped { get; set; }

        public DateTime? DateDelivered { get; set; }

        public DateTime? CustomClearanceDate { get; set; }

        public DateTime? EstimatedInstallationDate { get; set; }

        public DateTime? ActualInstallationDate { get; set; }

        public string OEMOrderNo { get; set; }

        public int DistributorId { get; set; }

        public string GSPtoDistriNo { get; set; }

        public string DistritoOEMNo { get; set; }

        public DateTime? ShipDateOEM { get; set; }

        public DateTime? ShipDateDistri { get; set; }

        public string ShipperTrackingNo { get; set; }

        public string ShipperTrackingName { get; set; }
        public string OrderItemIDs { get; set; }
        public int SessionTrackingNumber { get; set; }
        public int OrderTrackingNumber { get; set; }
        public int DestinationTrackingNumber { get; set; }

        public string ReasonForDelay{ get; set; }
        public string ReasonType { get; set; }

        public string TrackingReferenceUrl { get; set; } //added (SUNNY GOHAR)

    }
}

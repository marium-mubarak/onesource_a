﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing order List"
    /// </summary>
    public class OrderListModel
    {
        public List<sp_getOrderSearch_AdminR_Result> ISDOrderSearchlst{ get; set; }
        public List<sp_getOrderSearch_SFA_AdminR_Result> SFAOrderSearchlst { get; set; }
        public int TotalRecord { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : July 14 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing data to asset detail view"
    /// </summary>
    public class AssetDetailModel
    {
        public sp_getAssetDetail_AdminR_Result AssetDetail { get; set; }
        public List<GenericList> Manufacturers { get; set; }
        public List<GenericList> Categories { get; set; }
        public List<GenericList> Statuses { get; set; }
        public List<GenericList> Locations { get; set; }
        public List<GenericList> SupportedByList { get; set; }
        public List<GenericList> CurrentOwnerList { get; set; }
        public List<GenericList> WarrantyList { get; set; }
        public List<GenericList> Regions { get; set; }
        public int AssetId { get; set; }
        public string ImageUrl{ get; set; }
        public List<AssetPartsPeripherals> PartsPeripherals{ get; set; }      

    }

    /// <summary>
    /// Date created : July 17 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing data to asset detail view for parts/peripherals"
    /// </summary>
    public class AssetPartsPeripherals
    {
        public string Category { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
        public string Serial{ get; set; }
        public int AssetId{ get; set; }
        public string AssetTagNum { get; set; }
    }

    /// <summary>
    /// Date created : July 18 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing data to asset history view"
    /// </summary>
    public class AssetHistory
    {
        public DateTime Logdate { get; set; }
        public string UserName { get; set; }
        public string ActionName { get; set; }
        public string Status { get; set; }
        public bool IsPublic { get; set; }
        public int AssetLogID { get; set; }
        public string Comments { get; set; }
    }
}

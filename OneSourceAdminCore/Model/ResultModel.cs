﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
   public class ResultModel
    {
        public int ID { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OneSourceAdminCore.Presenter;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class CatalogReportModel
    {
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public List<GenericList> ListCatalog { get; set; }
        public string CustomerIds { get; set; }
        public string CatalogStandardId { get; set; }

        public List<GenericList> ListManufacturer { get; set; }// Adnan 19-june-2019
        public List<GenericList> ListPartCategories { get; set; }//Adnan 27-june-2019
        public List<GenericList> ListCountries { get; set; }//Adnan 28-june-2019
        public int CustomerID { get; set; }
        public int PartCategoryID { get; set; }
        public int CountryID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }


        #region removing session work by adnan 
        public int routeId { get; set; }
        public string routeName { get; set; }
        public string reportName { get; set; }
        public int parentRouteId { get; set; }
        public string parentName { get; set; }
        public string parentRoute { get; set; }
        public string excelAction { get; set; }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : june 8 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company supplier address model"
    /// </summary>
    public class CompanySupplierAddressModel
    {
        public List<GenericList> AddressTypes { get; set; }
        public List<GenericList> Countries { get; set; }
        public List<sp_GetAddressByPartyId_AdminR_Result> AddressList { get; set; }
        public sp_GetAddressInfo_AdminR_Result AddressInfo { get; set; }
        public int PartyId { get; set; }
    }
}

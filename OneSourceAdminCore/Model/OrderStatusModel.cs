﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing order status detail"
    /// </summary>
    public class OrderStatusModel
    {
        public int SessionTrackingNumber { get; set; }
        public int OrderTrackingNumber { get; set; }
        public int DestinationTrackingNumber { get; set; }
        public int OrderStatusId{ get; set; }
        public string Ostatus { get; set; }
        public string Reason { get; set; }
         
    }
}

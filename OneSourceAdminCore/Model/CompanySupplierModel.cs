﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class CompanySupplierModel
    {
        public CompanySupplierAddressModel SupplierAddress { get; set; }
        public List<sp_getManufacturerUsers_AdminR_Result> SupplierUsers { get; set; }
        public List<sp_getCompanyContacts_AdminR_Result> SupplierContacts { get; set; }
        public string ISLive{ get; set; }
        public bool IsSupplier { get; set; }
    }

}

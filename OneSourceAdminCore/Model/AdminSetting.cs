﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    public class AdminSetting
    {
        public int UserID { get; set; }
        public string UserName{ get; set; }
        public int UserGroupID { get; set; }
        public string UserGroupName { get; set; }
        public int UserSecurityLevel { get; set; }
        public int? LanguageID { get; set; }
        public int? DateFormatID { get; set; }
        public int? TimeZoneID { get; set; }
        public int CustomUserGroupID { get; set; }
        public int PrimeContractorID { get; set; }
        public string PrimeContractorName { get; set; }
        public int ParentCompanyID { get; set; }
        public int ContactId { get; set; }

    }
}

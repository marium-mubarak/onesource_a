﻿using System;
using System.Collections.Generic;
using OneSourceAdminCore.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Aug 15 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Domestic Catalog Operations"
    /// </summary>
    public class DomesticPartModel
    {
        public string CustomerName { get; set; }
        public string CountryName { get; set; }
        public string PartDescription { get; set; }
        public decimal PartPrice { get; set; }
        public int PartPriceCurrencyID { get; set; }
        public bool IsActive { get; set; }
        public string ManufacturerSKU { get; set; }
        public DateTime? EOL { get; set; }
        public int CustomerPartID { get; set; }
        public int CustomerID { get; set; }
        

    }
    public class DomesticCatalogPartModel
    {
        public DomesticPartModel Partdetail { get; set; }
        public List<sp_getPartCountries_AdminR_Result> Partcountry { get; set; }
        public List<GenericList> Currencies{ get; set; }
       
    }
    public class DomesticAddPartModel
    {
        public List<GenericList> CustomerList{ get; set; }
        public List<GenericList> CategoryList{ get; set; }
        public List<GenericList> CurrencyList{ get; set; }
        public List<sp_getPartCountries_AdminR_Result> CountryList { get; set; }
        public int CustomerId{ get; set; }
        public string ManufacturerPartNumber{ get; set; }
        public string Description{ get; set; }
        public int CategoryId{ get; set; }
        public decimal CostPrice{ get; set; }
        public int CurrencyId{ get; set; }
        public DateTime Eol{ get; set; }
        public string Countries{ get; set; }
        public string SKU{ get; set; }
        public int MfgPartId{ get; set; }
    }
    public class ManufacturerPartModel
    {
        public string ManufacturerSKU { get; set; }
        public int ManufacturerPartID { get; set; }
        public string Description { get; set; }
        public string PartCategoryName { get; set; }
        public string ManufacturerName { get; set; }

    }
    public class CatalogCustomerApprovalModel
    {
        public string CustomerName { get; set; }
        public string ManufacturerSKU { get; set; }
        public string ManufacturerName { get; set; }
        public string CurrencyName { get; set; }
        public string SFAName { get; set; }
        public string UserName { get; set; }

    }
}

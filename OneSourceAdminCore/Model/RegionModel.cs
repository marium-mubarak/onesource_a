﻿using System.Collections.Generic;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Feb 10 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of region to view"
    /// </summary>
    public class RegionModel
    {
        public List<GenericList> LstCountries{ get; set; }
        public List<GenericList> LstRegion { get; set; }
        public List<tblCountryRegionMapping> LstRegionWiseCountry { get; set; }
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public int RegionId{ get; set; }
        public string RegionName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int DefaultRegionId { get; set; }

    }
    /// <summary>   
    /// Date created : Feb 10 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of country region mapping to view"
    /// </summary>
    public class ParentCountryRegionMapping
    {
        public tblCountryRegionMapping RegionWiseCountry { get; set; }
        public int Count{ get; set; }
    }

}

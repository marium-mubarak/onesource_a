﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using System.Data;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for order comment"
    /// </summary>
    public class OrderCommentsModel
    {
        public string Comments{ get; set; }
        public List<sp_getGSOnlyComments_AdminR_Result> GSComments { get; set; }
        public DataTable tblComments{ get; set; }
        public List<GenericList> ListSuppliers { get; set; }
        public int SelectedSupplierID { get; set; }
        public List<sp_getCommentsBySupplier_AdminR_Result> OrderCommentsbySupplier { get; set; }
        public string JiraTicketNumber { get; set; }
    }

}

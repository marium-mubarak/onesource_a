﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : August 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for Support -> Request -> New Request Filter"
    /// </summary>
    public class NewRequestFilterModel
    {
        public List<GenericList> Companies{ get; set; }
        public List<GenericList> ActionTypes { get; set; }
        public List<GenericList> Statuses { get; set; }
        public List<GenericList> Priorities { get; set; }
        public string CompanyIds { get; set; }
        public string TypeIds { get; set; }
        public string PriorityIds { get; set; }
        public string StatusIds { get; set; }
        public string Name { get; set; }
        public int DateId{ get; set; }
        public string FilterType { get; set; }
    }
}

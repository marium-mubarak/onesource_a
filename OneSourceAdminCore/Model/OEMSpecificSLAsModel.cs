﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for OEM Specific SLA detail"
    /// </summary>
    public class OEMSpecificSLAsModel
    {
        public List<GenericList> Manufacturers { get; set; }
        public List<GenericList> Countries { get; set; }
        public decimal? OrderDeliverySLA { get; set; }
        public decimal? OrderAcceptanceSLA { get; set; }
        public decimal? QuoteResponseSLA { get; set; }
        public decimal? CatalogUploadSLA { get; set; }        
        public int countryId { get; set; }
        public int manufacturerId{ get; set; }


    }
}

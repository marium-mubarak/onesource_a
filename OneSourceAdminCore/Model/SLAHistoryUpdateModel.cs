﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Aug 28 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for having data of SLA history for update"
    /// </summary>
    public class SLAHistoryUpdateModel
    {
        public Nullable<int> breachCauseID { get; set; }
        public string breachResolutionText { get; set; }
        public int minutestobreach { get; set; }
        public int slainstanceid { get; set; }
        public bool oldisbreach { get; set; }
        public bool oldisexception { get; set; }
        public bool oldisclosedwithinsla { get; set; }
    }
}

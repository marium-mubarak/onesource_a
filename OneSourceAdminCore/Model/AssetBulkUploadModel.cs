﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class AssetBulkUploadModel
    {
        public int customerid { get; set; }
        public int parentassetid { get; set; }
        public string Mfg { get; set; }
        public string costcenter { get; set; }
        public Nullable<int> mfgpartid { get; set; }
        public Nullable<int> lineitemid { get; set; }
        public Nullable<int> parentcompanyid { get; set; }
        public string SKU { get; set; }
        public string Category { get; set; }
        public Nullable<int> customerpartcategoryid { get; set; }
        public Nullable<int> sessiontrackingnumber { get; set; }
        public Nullable<short> ordertrackingnumber { get; set; }
        public Nullable<short> destinationtrackingnumber { get; set; }
        public string ponumber { get; set; }
        public string sfaname { get; set; }
        public string description { get; set; }
        public string Owner { get; set; }
        public Nullable<int> currentownerid { get; set; }
        public string Address { get; set; }
        public string serialnum { get; set; }
        public Nullable<int> assetstatusid { get; set; }
        public string status { get; set; }
        public Nullable<System.DateTime> Purchase_Date { get; set; }
        public string assettagnum { get; set; }
        public Nullable<int> customeraddressid { get; set; }
        public Nullable<int> manufacturerid { get; set; }
        public string model { get; set; }
        public string modelnumber { get; set; }
        public int supportedbysfaid { get; set; }
        public int warrantytypeid { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> enddate { get; set; }
        public string warrantytypename { get; set; }
        public Nullable<System.DateTime> warrantystartdate { get; set; }
        public Nullable<System.DateTime> warrantyenddate { get; set; }
        public string customeraddressname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string stateprovinceregion { get; set; }
        public string postalcode { get; set; }
        public string countryname { get; set; }
        public Nullable<int> ticketid { get; set; }
        public string customername { get; set; }
        public string systemname { get; set; }
        public Nullable<decimal> price { get; set; }
        public string currencysymbol { get; set; }
        public string coveragename { get; set; }
        public string OldAssetName { get; set; }
        public string OldAssetTag { get; set; }
        public string OldLeasedorPurchased { get; set; }
        public string NewLeasedorPurchased { get; set; }
        public Nullable<bool> Imaging { get; set; }
        public Nullable<bool> WhiteGloveDelivery { get; set; }
        public string region { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<decimal> AssetRentalAmount { get; set; }
        public string OldAssetModel { get; set; }
        public string OldAssetModelNumber { get; set; }
        public string UserLoginName { get; set; }
        public string UserEmailAddress { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserPhoneNumber { get; set; }
        public Nullable<System.DateTime> ScheduledReturnDate { get; set; }
        public Nullable<System.DateTime> InstallDate { get; set; }
        public Nullable<System.DateTime> leaseStartDate { get; set; }
        public Nullable<System.DateTime> leaseEndDate { get; set; }
        public string PartNumber { get; set; }
        public string Location { get; set; }

        public string SupportedBy { get; set; }
        public string InstallationDate { get; set; }

        public string ErrorMessage { get; set; }
        public string UserLogonName { get; set; }
        public string Comments { get; set; }
        public string Errors { get; set; }
    }
}
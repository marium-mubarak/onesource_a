﻿using System;
using System.Collections.Generic;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Feb 4 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class is responsible for taking data of country alert to view"
    /// </summary>
    public class CountryModel
    {
        public List<GenericList> Countries { get; set; }
        public List<CustomCountryAlert> CountryAlerts { get; set; }
        public List<GenericList> Customers { get; set; }
    }

    public class CustomCountryAlert
    {
        public int CountryAlertId { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string Alert { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public bool IsTopStory { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
    }


    public class CountryCrudModal
    {
        public List<CountryListing> CountryListing { get; set; }
        public List<GenericList> DateFormatsList { get; set; }
        public List<GenericList> TimeZonesList { get; set; }
        public List<GenericList> IsoCountriesList { get; set; }

    }

    public class CountryListing
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string IsoCountryName { get; set; }
        public bool IsActive { get; set; }

    }

    public class CountryWorkShiftsModel
    {

        public int ShiftId { get; set; }
        public int WeekDayId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int CountryId { get; set; }

    }

    public class IsoCountryCrudModal
    {
        public List<IsoCountryListing> IsoCountryListing { get; set; }
        public List<GenericList> CurrencyCodesList { get; set; }

    }

    public class IsoCountryListing
    {
        public int IsoCountryId { get; set; }
        public string IsoCountryName { get; set; }
        public string Code { get; set; }
        public string DefaultCurrency { get; set; }
        public bool IsActive { get; set; }

    }
}

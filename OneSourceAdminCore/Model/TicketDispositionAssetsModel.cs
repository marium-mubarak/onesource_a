﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class TicketDispositionAssetsModel
    {
        public List<GenericList> CurrencyList{ get; set; }
        public int CurrencyId { get; set; }
        public decimal? PreTaxInvoiceAmount { get; set; }
        public List<tblDispositionAsset> DispositionAssets { get; set; }
        public List<TicketDispositionAssetsAttachment> DispositionAssetAttachments { get; set; }
    }
    public class TicketDispositionAssetsAttachment
    {
        public int daid { get; set; }
        public string docid { get; set; }
        public string daname { get; set; }
        public string daurl { get; set; }
        public string mimetypeicon { get; set; }
    }


}

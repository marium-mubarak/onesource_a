﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class PriceDeskReportModel
    {
        public PriceDeskReportModel()
        {
            SupplierQuotes = new List<QuotesBySuppiler>();
            Suppliers = new List<Suppliers>();
            AccountManagers = new List<AccountManager>();

            var pricedeskReportsPresenter = new PriceDeskReportsPresenter();

            var _accounts = pricedeskReportsPresenter.GetUserWithInfo().Where(x => x.UserGroupID == 20).OrderBy(x => x.UserName).ToList(); // added by sunny gohar
                        
            foreach (var item in _accounts)
            {
                AccountManagers.Add(new AccountManager { AccountManagerId = item.UserID, AccountManagerName = item.UserName });
            }
            AccountManagers.Add(new AccountManager { AccountManagerId = 0, AccountManagerName = "--Select--" });


        }
        public int QuoteTypeId { get; set; }
        public int SupplierId { get; set; }
        public List<QuotesBySuppiler> SupplierQuotes { get; set; }
        public string StartDate { get; set; }
        public string Status { get; set; }
        public string EndDate { get; set; }
        public List<Suppliers> Suppliers { get; set; }
        public List<AccountManager> AccountManagers { get; set; }
        public int AccountManagerId { get; set; }
        public string AccountManagerName { get; set; }

        public string Quoteids { get; set; }


        #region remove session work because of opening multiple tab issue
        public int routeId { get; set; }
        public string reportName { get; set; }
        public string routeName { get; set; }
        public int parentRouteId { get; set; }

        public string parentName { get; set; }

        public string parentRoute { get; set; }

        public string excelAction { get; set; }
        public string pdfAction { get; set; }
        public string MainAction { get; set; }
        public string CSVAction { get; set; }
        #endregion
    }

    public class AccountManager
    {
        public int AccountManagerId { get; set; }
        public string AccountManagerName { get; set; }
    }

    public class QuotesBySuppiler
    {
        public int QuoteId { get; set; }
        public string QuoteType { get; set; }
        public string AccountManagerName { get; set; }
        public string AccountManagerType { get; set; }
        public string CustomerName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }
    }

    public class QuoteResponseItems
    {
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string Sku { get; set; }
        public string Category { get; set; }
        public string LeadTime { get; set; }
        public string SerialNumber { get; set; }
        public string EOL { get; set; }
        public string Quantity { get; set; }
        public string UnitPrice { get; set; }
        public string ServiceType { get; set; }
        public string SLAType { get; set; }
        public string Resources { get; set; }
        public string HourlyRate { get; set; }
        public string TravelCharges { get; set; }

    }

    public class Suppliers
    {
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Model
{
    /// <summary>   
    /// Date created : Feb 04 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Class Made for getting list of key,value like country,currency dropdownlist
    /// </summary>
    public class GenericList
    {
        public int Key { get; set; }
        public string Value{ get; set; }
        public int Count { get; set; }

    }

    public class GenericStringList
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}

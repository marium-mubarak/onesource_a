﻿using OneSourceAdminCore.Entity;
using System.Collections.Generic;
namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : July 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing data to asset search view"
    /// </summary>
    public class AssetSearchModel
    {
        public List<sp_getManufacturerAssetSearch_AdminR_Result> Manufacturers { get; set; }
        public List<sp_getCategoryAssetSearch_AdminR_Result> Categories{ get; set; }
        public List<sp_getCustomerAssetSearch_AdminR_Result> Customers{ get; set; }
        public List<sp_getLocationAssetSearch_AdminR_Result> Locations{ get; set; }
        public List<GenericList> Statuses{ get; set; }
        public int CustomerId { get; set; }
        public int LocationId { get; set; }
        public int ManufacturerId { get; set; }
        public string StrManufacturers { get; set; }
        public int StatusId { get; set; }
        public int CategoryId { get; set; }
        public string StrCategories { get; set; }
        public string SerialNumber{ get; set; }
        public string AssetTag { get; set; }
        public string PartNumber { get; set; }
    }
}

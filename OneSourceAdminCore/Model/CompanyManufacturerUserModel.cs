﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for company manufacturer's user detail"
    /// </summary>
    public class CompanyManufacturerUserModel
    {
        public List<GenericList> DateFormates { get; set; }
        public sp_getManufacturerUserInfo_AdminR_Result userInfo { get; set; }
        public int ContactId{ get; set; }
    }
}

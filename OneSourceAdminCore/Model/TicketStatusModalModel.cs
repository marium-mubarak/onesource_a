﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : feb 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for ticket status detail"
    /// </summary>
    public class TicketStatusModalModel
    {
        public tblTicketStatu ticketStatus{ get; set; }

        public string XRefStatusName { get; set; }
        public string XRefStatusCode { get; set; }
        public string UserName{ get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    public class ManufacturerPartsModel
    {
        public string SearchTxt { get; set; }
        public string SearchType { get; set; }
        public int CategoryId { get; set; }
        public int ManfId { get; set; }
        public List<GenericList> CategoryList { get; set; }
        public List<GenericList> ManfList { get; set; }
        public List<GenericList> AvailabilityList { get; set; }
        public List<GenericStringList> UnSpsList { get; set; }
        public List<GenericList> PriceCodeList { get; set; }
        public List<GenericList> SupplierList { get; set; }
        public tblManufacturerPart ManfPart { get; set; }
        public string LastUpdateByUser { get; set; }
        public bool Posted { get; set; }
        public List<Sp_GetManuFacturerParts_AdminR_Result> ManfPartList { get; set; }
        
    }
}

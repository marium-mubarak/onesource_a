﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Model
{
    public class ReportSchedulerModel
    {
        public ReportSchedulerModel()
        {
            //Asset Disposition Report Fields
            AssetDispositionCustomers = new List<SelectListItem>();
            ListCountries = new List<SelectListItem>();
            ListRegions = new List<SelectListItem>();
            ListAssetTypes = new List<SelectListItem>();
            ListProductCategories = new List<SelectListItem>();
            ListLocations = new List<SelectListItem>();
            //===============================

            //Quotes By Supplier Report
            SupplierQuotes = new List<QuotesBySuppiler>();
            Suppliers = new List<Suppliers>();
            AccountManagers = new List<AccountManager>();

            //=================================
            ListPartDetailCustomers = new List<SelectListItem>();
            ReportSettingsList = new List<ReportSchedulerSettings>();
            ListCompanies = new List<SelectListItem>();
            SelectedCurrencies = new List<SelectListItem>();
            SelectedSuppliers = new List<SelectListItem>();
            SelectedManufacturers = new List<SelectListItem>();
        }

        public List<GenericList> ReportTypes { get; set; }
        public List<GenericList> ReportsList { get; set; }
        public List<CustomerCompanyClass> ListCustomers { get; set; }
        public List<SelectListItem> ListPartDetailCustomers { get; set; }
        public List<GenericList> ListManufacturers { get; set; }
        public List<SelectListItem> SelectedManufacturers { get; set; }
        public List<GenericList> ListCurrencies { get; set; }
        public List<SelectListItem> SelectedCurrencies { get; set; }
        //Asset Disposition Report Fields
        public List<SelectListItem> AssetDispositionCustomers { get; set; }
        public List<SelectListItem> ListAssetTypes { get; set; }
        public List<SelectListItem> ListProductCategories { get; set; }
        public List<SelectListItem> ListCountries { get; set; }
        public List<SelectListItem> ListRegions { get; set; }
        public List<SelectListItem> ListLocations { get; set; }
        public List<SelectListItem> ListCompanies { get; set; }
        //Quotes By Supplier Report
        public List<QuotesBySuppiler> SupplierQuotes { get; set; }
        public List<Suppliers> Suppliers { get; set; }
        public List<SelectListItem> SelectedSuppliers { get; set; }
        public List<AccountManager> AccountManagers { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public tblReportSchedulerSetting mainModel { get; set; }
        public List<ReportSchedulerSettings> ReportSettingsList { get; set; }
        public ReportSchedulerSettings ReportSettings { get; set; }
    }

    public partial class ReportSchedulerSettings
    {
        public int Id { get; set; }
        public string RecieverEmail { get; set; }
        public string ReportType { get; set; }
        public string Report { get; set; }
        public string WeekDay { get; set; }
        public string ReportTime { get; set; }
        public string Customers { get; set; }
        public string CustomerIds { get; set; }
        public string Users { get; set; }
        public string Currencies { get; set; }
        public string OrderStatus { get; set; }
        public string OrderStatuses { get; set; }
        public string Suppliers { get; set; }
        public string Manufacturers { get; set; }
        public string Locations { get; set; }
        public string RequestTypes { get; set; }
        public string Companies { get; set; }
        public string BusinessSegments { get; set; }
        public string AssetTypes { get; set; }
        public string AccountManager { get; set; }
        public string QuoteType { get; set; }
        public string Status { get; set; }
        public string Countries { get; set; }
        public string CountryIds { get; set; }
        public string Assets { get; set; }
        public string Categories { get; set; }
        public string Regions { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FilterType { get; set; }
    }

    public enum ReportDays
    {
        [Description("Monday")]
        Monday = 1,
        [Description("Tuesday")]
        Tuesday = 2,
        [Description("Wednesday")]
        Wednesday = 3,
        [Description("Thursday")]
        Thursday = 4,
        [Description("Friday")]
        Friday = 5,
        [Description("Saturday")]
        Saturday = 6,
        [Description("Sunday")]
        Sunday = 7

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing searched order detail"
    /// </summary>
    public class OrderSearchModel
    {
        public List<sp_getSupplierOrderSearch_AdminR_Result> Suppliers{ get; set; }
        public List<sp_getDeliveryAddressOrderSearch_AdminR_Result> DeliveryAddresses { get; set; }
        public List<sp_getManufacturerOrderSearch_AdminR_Result> Manufacturers { get; set; }
        public List<sp_getCustomerOrderSearch_AdminR_Result> Customers { get; set; }
        public List<sp_getUserOrderSearch_AdminR_Result> Users { get; set; }
        public List<sp_getCategoryOrderSearch_AdminR_Result> Categories{ get; set; }
        public List<sp_getCountryOrderSearch_AdminR_Result> Countries { get; set; }
        public List<sp_GetOrderStatuses_AdminR_Result> OrderStatuses{ get; set; }
        public List<sp_getPartsOrderSearch_AdminR_Result> LstParts{ get; set; }
        public List<sp_getBundlesOrderSearch_AdminR_Result> LstBundles{ get; set; }
        public int SessionTrackingNumber { get; set; }
        public int OrderTrackingNumber { get; set; }
        public int DestinationTrackingNumber { get; set; }
        public string OrderPO { get; set; }
        public string DestPO { get; set; }
        public string Statuses { get; set; }
        public int CountryId { get; set; }
        public int AddressId { get; set; }
        public int OrderedByUserID { get; set; }
        public int SupplierId { get; set; }
        public int ManufacturerId { get; set; }
        public int PartCategoryID { get; set; }
        public string SKU { get; set; }
        public string OrderStartDate { get; set; }
        public string OrderEndDate { get; set; }
        public string ShipStartDate { get; set; }
        public string ShipEndDate { get; set; }
        public int CustomerID { get; set; }

        //supplier sfa
        public string Parts{ get; set; }
        public string Bundles { get; set; }
    }
}

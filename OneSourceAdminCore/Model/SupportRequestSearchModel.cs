﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : July 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This  Class is responsible for providing request/ticket support search view detail"
    /// </summary>
    public class SupportRequestSearchModel
    {
        public List<GenericList> RequestType{ get; set; }
        public List<GenericList> Priorities { get; set; }
        public List<sp_getTimeZone_AdminR_Result> TimeZone { get; set; }
        public List<GenericList> Companies { get; set; }
        public List<GenericList> Locations{ get; set; }
        public List<GenericList> AffectedUser{ get; set; }
        public int requestTypeId{ get; set; }
        public int priorityId{ get; set; }
        public int companyId { get; set; }
        public int locationId { get; set; }
        public int affectedUserId { get; set; }
        public string CustomerTicket{ get; set; }
        public string SupplierTicket { get; set; }
        public string ContactName{ get; set; }
        public string ContactPhone { get; set; }
        public DateTime? RequestDateTime{ get; set; }
        public int Hour{ get; set; }
        public int Minute{ get; set; }
        public string GSOnlyComment{ get; set; }
        public string Summary{ get; set; }
        public string Description{ get; set; }
        public string TimeZoneId { get; set; }
        public string assetId { get; set; }
        public string serialNum { get; set; }
        public string assetTag { get; set; }
        public string gmtoffset { get; set; }
        public string gmtoffsetstring { get; set; }
        public string CompanyName{ get; set; }
        public string PriorityName { get; set; }
        public string RequestTypeName{ get; set; }
        public string AffectedUserName { get; set; }
        public string LocationName { get; set; }
    }
}

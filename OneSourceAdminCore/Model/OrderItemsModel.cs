﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using System.Data;

namespace OneSourceAdminCore.Model
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This model Class is responsible for providing order items"
    /// </summary>
    public class OrderItemsModel
    {
        public List<GenericList> GSPList { get; set; }
        public List<sp_getOrderItems_AdminR_Result> OrderItems { get; set; }
        public List<GenericList> Distributors { get; set; }
        public string CustomerName { get; set; }
        public string Status{ get; set; }
        public DateTime? DateAcceptedBySupplier { get; set; }
        public List<string> ViewFields{ get; set; }
        public string Reason { get; set; }
        public int?  NetPaymentDays{ get; set; }
        public DataTable tblReason{ get; set; }
        public List<int> DispositionQuoteIds { get; set; }
        public string CurrencyName{ get; set; }

    }
    public class DispositionQuoteDetail {
        public string pickupdate { get; set; }
        public DateTime pickupdate1 { get; set; }
        public string dispositionpaymenttypename { get; set; }
        public string locationname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string contactname { get; set; }
        public string contactphone { get; set; }
        public string contactemail { get; set; }
        public string specialinstructions { get; set; }
        public int quoteId{ get; set; }
    }


}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class sp_BundlesAssociatedParts_AdminR_Result
    {
        public Nullable<int> CatalogStandardID { get; set; }
        public string CatalogTitle { get; set; }
        public string CatalogDescription { get; set; }
        public string CustomerName { get; set; }
        public Nullable<int> CatalogPartID { get; set; }
        public string ManufacturerSKU { get; set; }
        public string PartDescription { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<double> GlobalPrice { get; set; }
        public string CurrencySymbol { get; set; }
        public Nullable<bool> IsOption { get; set; }
    }
}

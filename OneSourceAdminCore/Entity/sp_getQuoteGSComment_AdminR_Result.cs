//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class sp_getQuoteGSComment_AdminR_Result
    {
        public string logdate { get; set; }
        public string loggedby { get; set; }
        public string company { get; set; }
        public string @event { get; set; }
        public string status { get; set; }
        public string comments { get; set; }
    }
}

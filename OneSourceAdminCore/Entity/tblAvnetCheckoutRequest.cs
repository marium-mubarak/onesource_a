//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAvnetCheckoutRequest
    {
        public int AvnetCheckoutReqID { get; set; }
        public long BasketID { get; set; }
        public int UserID { get; set; }
        public int BasketOrderCountryID { get; set; }
        public int BasketOrderCurrencyID { get; set; }
        public int CustomerDestinationID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public decimal BasketAmount { get; set; }
        public Nullable<int> SessionTrackingNumber { get; set; }
        public string OgoneRefNo { get; set; }
    }
}

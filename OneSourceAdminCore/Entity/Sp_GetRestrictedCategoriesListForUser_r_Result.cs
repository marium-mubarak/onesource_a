//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class Sp_GetRestrictedCategoriesListForUser_r_Result
    {
        public int PartCategoryID { get; set; }
        public string PartCategoryName { get; set; }
        public Nullable<bool> IsRestricted { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOrderInquiryItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblOrderInquiryItem()
        {
            this.tblOrderInquiryDestinations = new HashSet<tblOrderInquiryDestination>();
        }
    
        public int orderInquiryItemID { get; set; }
        public int orderInquiryID { get; set; }
        public string manufacturerName { get; set; }
        public Nullable<int> manufacturerID { get; set; }
        public string partNumber { get; set; }
        public string partModel { get; set; }
        public string partMake { get; set; }
        public Nullable<int> quantity { get; set; }
        public string partDesc { get; set; }
        public bool isActive { get; set; }
        public System.DateTime lastUpdate { get; set; }
        public Nullable<int> lastUpdateUserID { get; set; }
    
        public virtual tblManufacturer tblManufacturer { get; set; }
        public virtual tblOrderInquiry tblOrderInquiry { get; set; }
        public virtual tblOrderInquiry tblOrderInquiry1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderInquiryDestination> tblOrderInquiryDestinations { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

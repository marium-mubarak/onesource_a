//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAMCommunication
    {
        public int tblAMCommunicationId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
        public int LiveChats { get; set; }
        public int PhoneCalls { get; set; }
        public int Emails { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public int LastUpdateByUser { get; set; }
        public Nullable<System.DateTime> DurationStartDate { get; set; }
        public Nullable<System.DateTime> DurationEndDate { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPartQuoteTrackRevalidate
    {
        public long PartQuoteID { get; set; }
        public long PartQuoteResponseId { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public int UpdatedByUser { get; set; }
    }
}

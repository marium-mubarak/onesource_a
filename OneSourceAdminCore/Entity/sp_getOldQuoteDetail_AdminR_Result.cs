//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class sp_getOldQuoteDetail_AdminR_Result
    {
        public Nullable<int> partquoteid { get; set; }
        public Nullable<int> customerid { get; set; }
        public string customername { get; set; }
        public string countryname { get; set; }
        public string currencyname { get; set; }
        public string currencysymbol { get; set; }
        public Nullable<int> SFAID { get; set; }
        public string sfaname { get; set; }
        public string customerref { get; set; }
        public string partquotestatus { get; set; }
        public Nullable<int> countryid { get; set; }
        public string requestedby { get; set; }
        public string partquotestatuscode { get; set; }
        public Nullable<int> currencyid { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblIntegrationSetup
    {
        public int IntegrationSetupId { get; set; }
        public int CustomerId { get; set; }
        public int PartnerCompanyId { get; set; }
        public int CountryId { get; set; }
        public int IntegrationTransactionSetId { get; set; }
        public int IntegrationDirectionId { get; set; }
        public int IntegrationFormatId { get; set; }
        public string HttpPostURL { get; set; }
        public string FromCredentialDomain { get; set; }
        public string FromCredentialIdentity { get; set; }
        public string ToCredentialDomain { get; set; }
        public string ToCredentialIdentity { get; set; }
        public string SenderCredentialDomain { get; set; }
        public string SenderCredentialIdentity { get; set; }
        public string SenderCredentialSharedSecret { get; set; }
        public string DeploymentMode { get; set; }
        public Nullable<int> DefaultETSDays { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public int LastUpdatedByUserId { get; set; }
    
        public virtual tblCountry tblCountry { get; set; }
        public virtual tblCustomer tblCustomer { get; set; }
        public virtual tblIntegrationDirection tblIntegrationDirection { get; set; }
        public virtual tblIntegrationFormat tblIntegrationFormat { get; set; }
        public virtual tblIntegrationTransactionSet tblIntegrationTransactionSet { get; set; }
        public virtual tblParentCompany tblParentCompany { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

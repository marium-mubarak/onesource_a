//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class sp_getISOCurrencyList_R_Result
    {
        public string isocurrencyname { get; set; }
        public int currencycodeid { get; set; }
        public int CountryCodeID { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public string ISOCurrencyCodeNumber { get; set; }
        public string UserName { get; set; }
        public string Code { get; set; }
        public string Default_Country { get; set; }
        public string Active { get; set; }
    }
}

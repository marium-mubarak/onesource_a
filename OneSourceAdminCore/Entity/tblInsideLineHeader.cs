//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblInsideLineHeader
    {
        public int SessionTrackingNumber { get; set; }
        public short OrderTrackingNumber { get; set; }
        public short DestinationTrackingNumber { get; set; }
        public string OrderNumber { get; set; }
        public string OrderBranch { get; set; }
        public string ResponseErrorFlag { get; set; }
        public string WarehouseShipFrom { get; set; }
        public string CarrierCode { get; set; }
        public string ShipVia { get; set; }
        public string TaxFlag { get; set; }
        public string ThirdPartyFreightAcctNum { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateRegionProvince { get; set; }
        public string PostalCode { get; set; }
        public string AddressErrorType { get; set; }
        public string Reserved { get; set; }
        public string ToIngramPO { get; set; }
    }
}

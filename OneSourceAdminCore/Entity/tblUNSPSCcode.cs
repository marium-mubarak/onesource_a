//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUNSPSCcode
    {
        public string UNSPSC { get; set; }
        public string Title { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> PartCategoryID { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public byte IsActive { get; set; }
    
        public virtual tblPartCategory tblPartCategory { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

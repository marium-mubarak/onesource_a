//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBasketOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblBasketOrder()
        {
            this.tblBasketOrderDestinations = new HashSet<tblBasketOrderDestination>();
            this.tblBuyerHeaders = new HashSet<tblBuyerHeader>();
        }
    
        public int BasketOrderID { get; set; }
        public int BasketID { get; set; }
        public int OrderDestinationCountryID { get; set; }
        public int OrderDestinationCurrencyID { get; set; }
        public string OrderPO { get; set; }
        public Nullable<int> ShippingMethodID { get; set; }
        public int OrderStatusID { get; set; }
        public string Comments { get; set; }
        public string BuyerCookie { get; set; }
        public string BuyerCode { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Custom6 { get; set; }
        public string Custom7 { get; set; }
        public string Custom8 { get; set; }
        public string Custom9 { get; set; }
        public string Custom10 { get; set; }
        public Nullable<int> OCISupplierID { get; set; }
        public Nullable<int> OCIID { get; set; }
        public Nullable<int> DestinationId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBasketOrderDestination> tblBasketOrderDestinations { get; set; }
        public virtual tblBasket tblBasket { get; set; }
        public virtual tblCountry tblCountry { get; set; }
        public virtual tblCurrency tblCurrency { get; set; }
        public virtual tblOrderStatu tblOrderStatu { get; set; }
        public virtual tblShippingMethod tblShippingMethod { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBuyerHeader> tblBuyerHeaders { get; set; }
    }
}

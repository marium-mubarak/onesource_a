//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCommInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblCommInfo()
        {
            this.tblCommInfoRoles = new HashSet<tblCommInfoRole>();
            this.tblCommInfoValues = new HashSet<tblCommInfoValue>();
        }
    
        public int CommInfoID { get; set; }
        public string CommInfoName { get; set; }
        public string CommInfoCode { get; set; }
        public int DataType { get; set; }
        public int DisplayPosition { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public int LastUpdatedByUserID { get; set; }
    
        public virtual tblDataType tblDataType { get; set; }
        public virtual tblUser tblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblCommInfoRole> tblCommInfoRoles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblCommInfoValue> tblCommInfoValues { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class Sp_GetInvoiceDetails_AdminR_Result
    {
        public string companyInvoiceID { get; set; }
        public System.DateTime dueDate { get; set; }
        public Nullable<System.DateTime> invoicePeriodStart { get; set; }
        public Nullable<int> billfrompartyid { get; set; }
        public Nullable<int> billtopartyid { get; set; }
        public int invoiceTypeID { get; set; }
        public System.DateTime createDate { get; set; }
        public int invoiceStatusID { get; set; }
        public string invoiceStatusName { get; set; }
        public string comments { get; set; }
        public int invoiceCurrencyID { get; set; }
        public Nullable<System.DateTime> invoicePeriodEnd { get; set; }
        public System.DateTime lastUpdate { get; set; }
        public string vatReg { get; set; }
        public Nullable<System.DateTime> taxDate { get; set; }
        public int billfromaddress { get; set; }
        public int billtoaddress { get; set; }
        public int paytoaddress { get; set; }
        public int preferredPaymentMethodID { get; set; }
        public Nullable<System.DateTime> recdDate { get; set; }
        public Nullable<System.DateTime> sendDate { get; set; }
        public string paymentInstr { get; set; }
        public int invoiceTermID { get; set; }
        public decimal invoicetotal { get; set; }
        public decimal vatTotal { get; set; }
        public int billfromcoid { get; set; }
        public int billtocoid { get; set; }
        public string billfromco { get; set; }
        public string billtoco { get; set; }
        public Nullable<decimal> vatrate { get; set; }
        public string po { get; set; }
        public string createdby { get; set; }
        public string updatedby { get; set; }
        public string oboco { get; set; }
    }
}

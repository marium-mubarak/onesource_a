//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOrderChangeIncoming
    {
        public int sessiontrackingnumber { get; set; }
        public int ordertrackingnumber { get; set; }
        public int destinationtrackingnumber { get; set; }
        public bool changeorderaccept { get; set; }
        public bool changeordership { get; set; }
        public bool changeeta { get; set; }
        public Nullable<System.DateTime> ordereta { get; set; }
        public Nullable<System.DateTime> ordershipdate { get; set; }
        public Nullable<System.DateTime> orderacceptdate { get; set; }
        public System.DateTime logdate { get; set; }
        public int ociid { get; set; }
    }
}

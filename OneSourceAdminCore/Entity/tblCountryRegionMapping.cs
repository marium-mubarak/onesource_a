//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCountryRegionMapping
    {
        public int CountryRegionMappingId { get; set; }
        public int RegionId { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public Nullable<int> UpdatedByUserID { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class sp_SearchPayment_AdminR_Result
    {
        public string Payment_ { get; set; }
        public string Payment_Date { get; set; }
        public string Vendor_ID { get; set; }
        public string Paid_By { get; set; }
        public Nullable<int> Paid_By_Id { get; set; }
        public string Paid_To { get; set; }
        public Nullable<int> Paid_To_Id { get; set; }
        public string Method { get; set; }
        public string TxType { get; set; }
        public string Comments { get; set; }
        public string Payment_Amount { get; set; }
        public string Payment_Amount_USD_ { get; set; }
        public string Last_Updated { get; set; }
        public string Updated_By { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySymbol { get; set; }
        public string Symbol { get; set; }
        public Nullable<int> CurrencyId { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCountryDefaultTimeZone
    {
        public int CountryID { get; set; }
        public int TimeZoneID { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        public virtual tblCountry tblCountry { get; set; }
        public virtual tblTimeZone tblTimeZone { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

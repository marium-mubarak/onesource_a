//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCM_Countries
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblCM_Countries()
        {
            this.tblsubIA_LifeCycleManageServicesData = new HashSet<tblsubIA_LifeCycleManageServicesData>();
        }
    
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        public virtual tblUser tblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblsubIA_LifeCycleManageServicesData> tblsubIA_LifeCycleManageServicesData { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblProjectDependency
    {
        public int taskID { get; set; }
        public int dependOnTaskID { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> lastUpdate { get; set; }
        public Nullable<int> lastUpdateUserID { get; set; }
    }
}

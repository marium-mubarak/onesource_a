//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSFAPart
    {
        public int ManufacturerPartID { get; set; }
        public int SFAID { get; set; }
        public int CountryID { get; set; }
        public string SFAPartID { get; set; }
        public string SFAPartCategory { get; set; }
        public string SFAPartSubCategory { get; set; }
        public string SFADescription { get; set; }
        public Nullable<bool> InStock { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        public virtual tblCountry tblCountry { get; set; }
        public virtual tblManufacturerPart tblManufacturerPart { get; set; }
        public virtual tblSFA tblSFA { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

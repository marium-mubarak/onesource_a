//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCommResult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblCommResult()
        {
            this.tblCommHistories = new HashSet<tblCommHistory>();
        }
    
        public int CommResultID { get; set; }
        public string CommResultName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public int LastUpdatedByUserID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblCommHistory> tblCommHistories { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

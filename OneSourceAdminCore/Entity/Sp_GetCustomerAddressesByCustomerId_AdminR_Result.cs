//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class Sp_GetCustomerAddressesByCustomerId_AdminR_Result
    {
        public int CustomerAddressId { get; set; }
        public string CondensedAddress { get; set; }
        public string CountryName { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblActionRequestType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblActionRequestType()
        {
            this.tblActionRequestType1 = new HashSet<tblActionRequestType>();
            this.tblActionRequestType11 = new HashSet<tblActionRequestType>();
            this.tblTicketStatus = new HashSet<tblTicketStatu>();
            this.tblXRefTicketStatus = new HashSet<tblXRefTicketStatu>();
            this.tblActionRequests = new HashSet<tblActionRequest>();
            this.tblTickets = new HashSet<tblTicket>();
            this.tblARTypeFields = new HashSet<tblARTypeField>();
            this.tblticket3 = new HashSet<tblticket3>();
            this.tblXrefActionRequestTypes = new HashSet<tblXrefActionRequestType>();
        }
    
        public int ARTypeID { get; set; }
        public string ARTypeName { get; set; }
        public string ARTypeDesc { get; set; }
        public Nullable<int> ARTypeParentID { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public bool IsDefault { get; set; }
        public bool IsAssetMandatory { get; set; }
        public bool IsSelectable { get; set; }
        public bool NoReport { get; set; }
        public bool IsDisposition { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblActionRequestType> tblActionRequestType1 { get; set; }
        public virtual tblActionRequestType tblActionRequestType2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblActionRequestType> tblActionRequestType11 { get; set; }
        public virtual tblActionRequestType tblActionRequestType3 { get; set; }
        public virtual tblParentCompany tblParentCompany { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblTicketStatu> tblTicketStatus { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblXRefTicketStatu> tblXRefTicketStatus { get; set; }
        public virtual tblUser tblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblActionRequest> tblActionRequests { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblTicket> tblTickets { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblARTypeField> tblARTypeFields { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblticket3> tblticket3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblXrefActionRequestType> tblXrefActionRequestTypes { get; set; }
    }
}

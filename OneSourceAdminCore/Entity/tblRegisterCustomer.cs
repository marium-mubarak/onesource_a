//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblRegisterCustomer
    {
        public int RequestId { get; set; }
        public string CustomerName { get; set; }
        public int ParentCompanyID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<int> DateFormatID { get; set; }
        public string Contact_LastName { get; set; }
        public string Contact_FirstName { get; set; }
        public Nullable<int> SecurityLevel { get; set; }
        public string AddressName { get; set; }
        public string CondensedAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public int CountryID { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int DefaultCountryID { get; set; }
        public Nullable<decimal> SpendingLimit { get; set; }
        public Nullable<int> SpendingLimit_CurrencyID { get; set; }
        public Nullable<System.DateTime> SpendingLimit_StartDate { get; set; }
        public Nullable<System.DateTime> SpendingLimit_EndDate { get; set; }
        public string SpendingLimit_PerOrder { get; set; }
        public Nullable<int> SpendingLimit_PerOrder_CurrencyID { get; set; }
        public Nullable<int> Order_SpendingLimit_ApprovalCode { get; set; }
        public string OrderApprovalCode { get; set; }
        public string Shipping_Phone { get; set; }
        public string Shipping_Fax { get; set; }
        public string Shipping_Email { get; set; }
        public string Shipping_Contact { get; set; }
        public int PreferredFreightCarrierID { get; set; }
        public bool Shipping_IsGlobal { get; set; }
        public string Billing_Phone { get; set; }
        public string Billing_Fax { get; set; }
        public string Billing_Email { get; set; }
        public string Billing_Contact { get; set; }
        public string AccountNumber { get; set; }
        public bool Billing_IsGlobal { get; set; }
        public string CurrentStatus { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOrdersRecvQueue
    {
        public decimal OrdersRecvQueueID { get; set; }
        public System.DateTime QueueDateTime { get; set; }
        public decimal SessionTrackingNumber { get; set; }
        public decimal OrderTrackingNumber { get; set; }
        public decimal DestinationTrackingNumber { get; set; }
        public short OrderFormatCode { get; set; }
        public short OrderSourceCode { get; set; }
        public string OrderImportFilename { get; set; }
        public string OrderItemsData { get; set; }
        public string OrderExportFilename { get; set; }
        public short OrderPriority { get; set; }
        public string SourceName { get; set; }
        public string Email { get; set; }
    }
}

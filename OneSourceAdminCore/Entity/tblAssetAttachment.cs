//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAssetAttachment
    {
        public int DAID { get; set; }
        public int AssetID { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        public virtual GSNetP_tbl_Assets GSNetP_tbl_Assets { get; set; }
        public virtual tblDigitalAsset tblDigitalAsset { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblIA_LifeCycleManageServices
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblIA_LifeCycleManageServices()
        {
            this.tblsubIA_LifeCycleManageServices = new HashSet<tblsubIA_LifeCycleManageServices>();
        }
    
        public int IALifeCycleManageID { get; set; }
        public string IALifeCycleManageTitle { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        public virtual tblUser tblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblsubIA_LifeCycleManageServices> tblsubIA_LifeCycleManageServices { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblMimeType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblMimeType()
        {
            this.tblDigitalAssets = new HashSet<tblDigitalAsset>();
            this.tblMimeTypeExtensions = new HashSet<tblMimeTypeExtension>();
        }
    
        public int MimeTypeID { get; set; }
        public string MimeTypeName { get; set; }
        public string MimeTypeDesc { get; set; }
        public string MimeTypeHeader { get; set; }
        public string MimeTypeIcon { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblDigitalAsset> tblDigitalAssets { get; set; }
        public virtual tblUser tblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblMimeTypeExtension> tblMimeTypeExtensions { get; set; }
    }
}

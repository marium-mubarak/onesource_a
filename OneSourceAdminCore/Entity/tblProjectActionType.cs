//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblProjectActionType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblProjectActionType()
        {
            this.tblProjectActions = new HashSet<tblProjectAction>();
        }
    
        public int patypeid { get; set; }
        public string patypename { get; set; }
        public Nullable<bool> isactive { get; set; }
        public System.DateTime lastupdate { get; set; }
        public Nullable<int> lastupdateuserid { get; set; }
        public Nullable<int> companyid { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblProjectAction> tblProjectActions { get; set; }
    }
}

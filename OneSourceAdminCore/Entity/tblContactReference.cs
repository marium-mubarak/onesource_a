//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblContactReference
    {
        public int refid { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string title { get; set; }
        public string companyname { get; set; }
        public string relation { get; set; }
        public Nullable<bool> isOK { get; set; }
        public string comments { get; set; }
        public bool isActive { get; set; }
        public System.DateTime lastUpdate { get; set; }
        public Nullable<int> lastUpdateUserID { get; set; }
        public int contactid { get; set; }
        public string checkedBy { get; set; }
        public Nullable<System.DateTime> checkDate { get; set; }
        public Nullable<int> yrsKnown { get; set; }
        public string workphone { get; set; }
        public string email { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBasketHistory
    {
        public int HistoryID { get; set; }
        public Nullable<int> BasketID { get; set; }
        public string BasketName { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public int OCIID { get; set; }
        public bool Sent { get; set; }
        public Nullable<System.DateTime> logDate { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblKPITracking
    {
        public int kpiid { get; set; }
        public string textValue { get; set; }
        public bool isActive { get; set; }
        public System.DateTime lastUpdate { get; set; }
        public Nullable<int> lastUpdateuserID { get; set; }
        public System.DateTime kpiDate { get; set; }
        public int kpiDetailID { get; set; }
        public Nullable<int> regionID { get; set; }
        public Nullable<int> offeringID { get; set; }
        public Nullable<int> companyID { get; set; }
        public Nullable<int> channelID { get; set; }
        public Nullable<int> sfaid { get; set; }
    
        public virtual tblOffering tblOffering { get; set; }
        public virtual tblRegion tblRegion { get; set; }
    }
}

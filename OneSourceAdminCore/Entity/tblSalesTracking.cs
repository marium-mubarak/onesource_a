//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSalesTracking
    {
        public int OPChangeID { get; set; }
        public int OpptyID { get; set; }
        public int OPActionID { get; set; }
        public System.DateTime CommentDate { get; set; }
        public int OPActionBy { get; set; }
        public string OPComment { get; set; }
        public Nullable<int> StepID { get; set; }
        public Nullable<double> Probability { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
    
        public virtual tblAction tblAction { get; set; }
        public virtual tblSalesPipeline tblSalesPipeline { get; set; }
        public virtual tblSalesStep tblSalesStep { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}

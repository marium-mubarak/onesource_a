//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblReportsMenu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblReportsMenu()
        {
            this.tblReportMenuRoleMappings = new HashSet<tblReportMenuRoleMapping>();
        }
    
        public int ReportMenuId { get; set; }
        public string ReportMenuName { get; set; }
        public string ReportMenuLink { get; set; }
        public int ParentMenuId { get; set; }
        public string DataTokenName { get; set; }
        public string IconClass { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> LastUpdatedByUserId { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblReportMenuRoleMapping> tblReportMenuRoleMappings { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblQualification
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblQualification()
        {
            this.tblTermsAndConditions = new HashSet<tblTermsAndCondition>();
            this.tblWorkflowDetails = new HashSet<tblWorkflowDetail>();
        }
    
        public int qualificationID { get; set; }
        public string qualificationName { get; set; }
        public string qualificationDescription { get; set; }
        public System.DateTime lastUpdate { get; set; }
        public bool isActive { get; set; }
        public int lastUpdateUserID { get; set; }
        public System.DateTime createDate { get; set; }
        public Nullable<int> createdByUserID { get; set; }
        public int formulaid { get; set; }
        public Nullable<int> qualificationTypeID { get; set; }
    
        public virtual tblUser tblUser { get; set; }
        public virtual tblUser tblUser1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblTermsAndCondition> tblTermsAndConditions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblWorkflowDetail> tblWorkflowDetails { get; set; }
        public virtual tblFormula tblFormula { get; set; }
        public virtual tblQualificationType tblQualificationType { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class Sp_GetFieldsByAactionId_AdminR_Result
    {
        public int FieldId { get; set; }
        public int ActionID { get; set; }
        public string FieldName { get; set; }
        public string ActionTypeName { get; set; }
        public string FieldLabel { get; set; }
        public string FieldMethod { get; set; }
        public string DefaultValue { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
    }
}

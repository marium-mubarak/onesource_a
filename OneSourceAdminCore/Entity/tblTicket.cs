//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblTicket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblTicket()
        {
            this.GSNetP_tbl_AssetsOwned = new HashSet<GSNetP_tbl_AssetsOwned>();
            this.tblADFileUploads_RepEng = new HashSet<tblADFileUploads_RepEng>();
            this.tblDispositionSummaries = new HashSet<tblDispositionSummary>();
            this.tblSLATrackings = new HashSet<tblSLATracking>();
            this.tblTicketAttachments = new HashSet<tblTicketAttachment>();
            this.tblTicketTrackings = new HashSet<tblTicketTracking>();
            this.tblActionTrackings = new HashSet<tblActionTracking>();
            this.tblARTrackings = new HashSet<tblARTracking>();
            this.tblSLAWFLogs = new HashSet<tblSLAWFLog>();
        }
    
        public int TicketID { get; set; }
        public string TktDesc { get; set; }
        public Nullable<int> TktStatusID { get; set; }
        public int TktTypeID { get; set; }
        public Nullable<int> PriorityID { get; set; }
        public Nullable<int> SeverityID { get; set; }
        public Nullable<int> EscalationLevel { get; set; }
        public Nullable<decimal> PctComplete { get; set; }
        public Nullable<int> ARID { get; set; }
        public Nullable<int> CSRID { get; set; }
        public Nullable<int> AssetID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<System.DateTime> NeedByDate { get; set; }
        public Nullable<System.DateTime> ETADate { get; set; }
        public Nullable<System.DateTime> AppointmentDate { get; set; }
        public Nullable<int> SFAID { get; set; }
        public string SFATicketID { get; set; }
        public Nullable<System.DateTime> SFAAssignDate { get; set; }
        public string SFAPO { get; set; }
        public Nullable<System.DateTime> ResolutionDate { get; set; }
        public string ResolutionText { get; set; }
        public bool IsBillable { get; set; }
        public Nullable<decimal> BillAmount { get; set; }
        public Nullable<int> BillAmountCurrencyID { get; set; }
        public string ReplaceSerial { get; set; }
        public string WorkedOnSerial { get; set; }
        public Nullable<int> HoursOnSite { get; set; }
        public Nullable<int> MinsOnSite { get; set; }
        public Nullable<int> HoursTravel { get; set; }
        public Nullable<int> MinsTravel { get; set; }
        public string CustomerTicketID { get; set; }
        public string CustomerPO { get; set; }
        public string TicketContactName { get; set; }
        public string TicketContactPhoneVoice { get; set; }
        public Nullable<int> Override_ManufacturerID { get; set; }
        public string Override_Model { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime LastUpdate { get; set; }
        public int LastUpdateUserID { get; set; }
        public bool ServerDST { get; set; }
        public string TicketSLA { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<int> OrderItemId { get; set; }
        public string InvoiceNo { get; set; }
    
        public virtual GSNetP_tbl_Assets GSNetP_tbl_Assets { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GSNetP_tbl_AssetsOwned> GSNetP_tbl_AssetsOwned { get; set; }
        public virtual tblActionRequest tblActionRequest { get; set; }
        public virtual tblActionRequestType tblActionRequestType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblADFileUploads_RepEng> tblADFileUploads_RepEng { get; set; }
        public virtual tblContact tblContact { get; set; }
        public virtual tblCurrency tblCurrency { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblDispositionSummary> tblDispositionSummaries { get; set; }
        public virtual tblManufacturer tblManufacturer { get; set; }
        public virtual tblPriority tblPriority { get; set; }
        public virtual tblSFA tblSFA { get; set; }
        public virtual tblSFA tblSFA1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblSLATracking> tblSLATrackings { get; set; }
        public virtual tblTicketStatu tblTicketStatu { get; set; }
        public virtual tblUser tblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblTicketAttachment> tblTicketAttachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblTicketTracking> tblTicketTrackings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblActionTracking> tblActionTrackings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblARTracking> tblARTrackings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblSLAWFLog> tblSLAWFLogs { get; set; }
    }
}

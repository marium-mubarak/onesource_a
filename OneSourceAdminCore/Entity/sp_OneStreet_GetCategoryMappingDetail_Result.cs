//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    
    public partial class sp_OneStreet_GetCategoryMappingDetail_Result
    {
        public int OneStreetCategoryMappingId { get; set; }
        public long OSCategoryID { get; set; }
        public long OneStreetCategoryID { get; set; }
        public string TreeClassClassificationName { get; set; }
        public string PartCategoryName { get; set; }
    }
}

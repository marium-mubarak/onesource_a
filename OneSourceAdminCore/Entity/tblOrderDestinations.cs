//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneSourceAdminCore.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOrderDestinations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblOrderDestinations()
        {
            this.tblOrderItems = new HashSet<tblOrderItems>();
            this.tblInvoiceOrders = new HashSet<tblInvoiceOrder>();
            this.tblInvoiceTypeOrders = new HashSet<tblInvoiceTypeOrder>();
            this.tblXrefSFAOrders = new HashSet<tblXrefSFAOrder>();
        }
    
        public int SessionTrackingNumber { get; set; }
        public short OrderTrackingNumber { get; set; }
        public short DestinationTrackingNumber { get; set; }
        public int CustomerDestinationID { get; set; }
        public Nullable<int> CustomerInvoiceLocationID { get; set; }
        public string DestinationPO { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> ShippingAmount { get; set; }
        public Nullable<decimal> TotalInvoiceAmount { get; set; }
        public string InvoiceNumber { get; set; }
        public string ShippingContact { get; set; }
        public string BillingContact { get; set; }
        public Nullable<int> DeliveryStatusID { get; set; }
        public Nullable<System.DateTime> DeliveryStatusLastUpdated { get; set; }
        public Nullable<int> DeliveryStatusLastUpdatedByUserID { get; set; }
        public Nullable<System.DateTime> DateAcceptedbySupplier { get; set; }
        public Nullable<System.DateTime> DateAcceptedLastUpdated { get; set; }
        public Nullable<int> DateAcceptedLastUpdateByUserID { get; set; }
        public Nullable<System.DateTime> ETA { get; set; }
        public Nullable<System.DateTime> ETALastUpdated { get; set; }
        public Nullable<int> ETALastUpdatedByUserID { get; set; }
        public Nullable<System.DateTime> OrderPlacedBySFA { get; set; }
        public Nullable<System.DateTime> OrderReceivedByMfg { get; set; }
        public Nullable<System.DateTime> DateReceived { get; set; }
        public Nullable<System.DateTime> DateReceivedLastUpdated { get; set; }
        public Nullable<int> DateReceivedLastUpdatedByUserID { get; set; }
        public Nullable<System.DateTime> DateShipped { get; set; }
        public Nullable<System.DateTime> DateShippedLastUpdated { get; set; }
        public Nullable<int> DateShippedLastUpdatedByUserID { get; set; }
        public string BuyerCookie { get; set; }
        public string BuyerCode { get; set; }
        public Nullable<int> OrderFormatID { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public string VAT_ID { get; set; }
    
        public virtual tblCustomerAddress tblCustomerAddresses { get; set; }
        public virtual tblCustomerAddress tblCustomerAddresses1 { get; set; }
        public virtual tblOrderFormat tblOrderFormats { get; set; }
        public virtual tblDeliveryStatus tblDeliveryStatus { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderItems> tblOrderItems { get; set; }
        public virtual tblUser tblUser { get; set; }
        public virtual tblUser tblUser1 { get; set; }
        public virtual tblUser tblUser2 { get; set; }
        public virtual tblUser tblUser3 { get; set; }
        public virtual tblUser tblUser4 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblInvoiceOrder> tblInvoiceOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblInvoiceTypeOrder> tblInvoiceTypeOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblXrefSFAOrder> tblXrefSFAOrders { get; set; }
        public virtual tblOrder tblOrder { get; set; }
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Am Communication data"
    /// </summary>
    public class AMPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public AMPresenter()
        {
            _dbCon = new Entity.globalserveEntities();
        }

        /// <summary>
        /// edit Am Communication data
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditAMData(tblAMCommunication model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblAMCommunication am =
                    _dbCon.tblAMCommunications.FirstOrDefault(x => x.tblAMCommunicationId == model.tblAMCommunicationId);
                if (am != null)
                {
                    am.CustomerName = model.CustomerName;
                    am.Month = "0";
                    am.Year = 0;
                    am.PhoneCalls = model.PhoneCalls;
                    am.Emails = model.Emails;
                    am.LiveChats = model.LiveChats;
                    am.LastUpdated = DateTime.Now;
                    am.LastUpdateByUser = userId;
                    am.DurationStartDate = model.DurationStartDate;
                    am.DurationEndDate = model.DurationEndDate;
                }
                _dbCon.Entry(am).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// add AM Communication data to database
        /// </summary>
        /// <param name="data"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddAMData(tblAMCommunication data, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                data.Month = "0";
                data.Year = 0;
                data.LastUpdated = DateTime.Now;
                data.LastUpdateByUser = userId;
                _dbCon.tblAMCommunications.Add(data);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// Get List of AM Communication from database
        /// </summary>
        /// <returns></returns>
        public List<AMDetailModel> GetAMData()
        {
            var data = _dbCon.tblAMCommunications.Select(x => new AMDetailModel
            {
                tblAMCommunicationId = x.tblAMCommunicationId,
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName,
                Month = x.Month,
                Year = x.Year,
                LiveChats = x.LiveChats,
                PhoneCalls = x.PhoneCalls,
                Emails = x.Emails,
                LastUpdated = x.LastUpdated,
                LastUpdateByUser = x.LastUpdateByUser,
                DurationStartDate = x.DurationStartDate,
                DurationEndDate = x.DurationEndDate
            }).OrderBy(x => x.CustomerName).ToList();
            data.ForEach(x =>
            {
                x.strStartDate = Convert.ToDateTime(x.DurationStartDate).Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.DurationEndDate != null ? Convert.ToDateTime(x.DurationEndDate).Date.ToString("MM/dd/yyy") : string.Empty);

            });
            return data;
        }
        /// <summary>
        /// delete the AM Communication record by its ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Delete(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblAMCommunications.FirstOrDefault(x => x.tblAMCommunicationId == id);
                if (data != null)
                {
                    _dbCon.tblAMCommunications.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }
    }
}

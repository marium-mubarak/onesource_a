﻿using System.Linq;
using OneSourceAdminCore.Entity;
using System.Collections.Generic;
using OneSourceAdminCore.Model;
using System;
using System.Data;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using OneSourceAdminCore.Helper;

namespace OneSourceAdminCore.Presenter
{
    public class ProcurementDashboardPresenter
    {
        globalserveEntities dbCon;
        public ProcurementDashboardPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public string GetProdCatIDs(int CustomerId)
        {
            //declare string to save comma seperated ID's
            string pcatlst = string.Empty;
            try
            {
                var categories = (from pc in dbCon.tblPartCategories
                                  join ccp in dbCon.tblCustomerCountryParts on pc.PartCategoryID equals ccp.PartCategoryID
                                  where (pc.IsActive == true) && (ccp.CustomerID == CustomerId) && (ccp.IsActive == true)
                                  select new GenericList { Key = pc.PartCategoryID, Value = pc.PartCategoryName }).Distinct().OrderBy(x => x.Value).ToList();
                //loop through the query items
                foreach (var p1 in categories)
                {
                    //make a comma seperated list of product category ID's
                    pcatlst += p1.Key + ",";

                }
                //Trim last comma from the ID list 
                pcatlst = pcatlst.TrimEnd(',');
            }
            // catch exception 
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetProdCatIDs");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProdCatIDs");
                }
                return null;
            }
            //return comma seperated list of product category ID's
            return pcatlst;
        }

        public string GetSLAForMetrics(int CustomerId, int country, int oem)
        {
            string result = string.Empty;
            try
            {
                var csla = (from c in dbCon.tblCountryDeliverySLAsByOEMs
                            where (c.CountryID == country) && (c.ManufacturerID == oem)
                            select new { c.OrderDeliverySLA }).FirstOrDefault();
                //if Query is not null 
                if (csla != null)
                {
                    if (csla.OrderDeliverySLA > 1)
                    {
                        result = "SLA: " + csla.OrderDeliverySLA.ToString() + " days";
                    }

                    else
                    {
                        result = "SLA: " + csla.OrderDeliverySLA.ToString() + " day";
                    }
                }
                else
                {
                    // Else set not specified value
                    result = "SLA: Not specified";
                }
            }
            // catch exception 
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSLAForMetrics");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSLAForMetrics");
                }
                return null;
            }
            return result;
        }

        public string GetSLAForOrderAcceptance(int country)
        {
            string result = string.Empty;
            try
            {
                var csla = (from c in dbCon.tblCountryDeliverySLAs
                            where (c.CountryID == country)
                            select new { c.AcceptanceDays }).FirstOrDefault();
                //if Query is not null 
                if (csla != null)
                {
                    if (csla.AcceptanceDays > 1)
                    {
                        result = "SLA: " + csla.AcceptanceDays.ToString() + " days";
                    }

                    else
                    {
                        result = "SLA: " + csla.AcceptanceDays.ToString() + " day";
                    }
                }
                else
                {
                    // Else set not specified value
                    result = "SLA: Not specified";
                }
            }
            // catch exception 
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSLAForOrderAcceptance");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSLAForOrderAcceptance");
                }
                return null;
            }
            return result;
        }

        public string GetSLAForQuoteResponse(int country)
        {
            string result = string.Empty;
            try
            {
                var csla = (from c in dbCon.tblCountryDeliverySLAs
                            where (c.CountryID == country)
                            select new { c.QuoteResponseDays }).FirstOrDefault();
                //if Query is not null 
                if (csla != null)
                {
                    if (csla.QuoteResponseDays > 1)
                    {
                        result = "SLA: " + csla.QuoteResponseDays.ToString() + " days";
                    }

                    else
                    {
                        result = "SLA: " + csla.QuoteResponseDays.ToString() + " day";
                    }
                }
                else
                {
                    // Else set not specified value
                    result = "SLA: Not specified";
                }
            }
            // catch exception 
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSLAForQuoteResponse");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSLAForQuoteResponse");
                }
                return null;
            }
            return result;
        }
    }
}

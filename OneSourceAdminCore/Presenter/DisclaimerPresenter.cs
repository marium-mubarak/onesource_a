﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Oct 5 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing of customer's disclaimer data"
    /// </summary>

    public class DisclaimerPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public DisclaimerPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// Get disclaimer by customerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public string GetDisclaimer(int customerId)
        {
            string value = string.Empty;
            var data = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerId);
            if (data != null)
            {
                value = data.Disclaimer;
            }
            return value;

        }

        /// <summary>
        /// update customer's disclaimer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="disclaimer"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateDisclaimer(int customerId, string disclaimer, int userId)
        {
            int count = 0;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblCustomer customer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerId);

                    if (customer != null)
                    {
                        customer.Disclaimer = disclaimer;
                        customer.LastUpdated = DateTime.Now;
                        customer.LastUpdatedByUserID = userId;
                    }

                    _dbCon.Entry(customer).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateDisclaimer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateDisclaimer");
                }
            }
            return count;
        }

        /// <summary>
        /// delete customer's disclaimer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int DeleteDisclaimer(int customerId, int userId)
        {
            int count = 0;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblCustomer customer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerId);

                    if (customer != null)
                    {
                        customer.Disclaimer = null;
                        customer.LastUpdated = DateTime.Now;
                        customer.LastUpdatedByUserID = userId;
                    }
                    _dbCon.Entry(customer).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateDisclaimer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateDisclaimer");
                }
            }
            return count;
        }

    }
}

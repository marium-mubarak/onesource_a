﻿using System.Linq;
using OneSourceAdminCore.Entity;
using System.Collections.Generic;
using OneSourceAdminCore.Model;
using System;
using System.Data;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Web.Mvc;
using OneSourceAdminCore.Helper;

namespace OneSourceAdminCore.Presenter
{
    public class SupportReportsPresenter
    {
        globalserveEntities dbCon;
        public SupportReportsPresenter()
        {
            dbCon = new globalserveEntities();
        }
        public List<GenericList> GetManufactures()//Adnan 26-july-2019
        {
            return dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.ManufacturerID, Value = x.ManufacturerName }).Distinct().OrderBy(x => x.Key).ToList();

        }
        public DataTable GetReportData(string reportId, string reportName, SupportReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_SupportReportMain_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 1000;

                    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@ReportId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CompanyID", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@RequestTypes", SqlDbType.Text));
                    command.Parameters[0].Value = reportName;
                    command.Parameters[1].Value = reportId;
                    command.Parameters[2].Value = model.StartDate;
                    command.Parameters[3].Value = model.EndDate;
                    command.Parameters[4].Value = (model.CompanyId != 0) ? model.CompanyId : (reportName == "Requests vs. SLA Summary (By Request)" || reportName == "Requests By CSM" || reportName == "Replacement Serial Requests" || reportName == "Requests Raw Data" || reportName == "Billable Requests") ? model.CompanyId = -1 : 0;
                    command.Parameters[5].Value = (model.CountryIDs != null) ? model.CountryIDs : (reportName == "Requests By Country") ? model.CountryIDs = "-1" : "";
                    command.Parameters[6].Value = (model.RequestTypeIDs != null) ? model.RequestTypeIDs : (reportName == "Requests vs. SLA Summary (By Request)") ? model.RequestTypeIDs = "-1" : "";

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportData");
                }
                return null;
            }
        }

        public List<sp_GetRequestTypeByCustomerId_AdminR_Result> GetRequestTypesByCustomerId(string customerId)
        {
            try
            {
                return dbCon.sp_GetRequestTypeByCustomerId_AdminR(customerId).ToList();

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRequestTypesByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRequestTypesByCustomerId");
                }
                return null;
            }
        }
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Aurangzeb Khan
    /// This class fetches all the data for Customer Address Report
    /// </summary>
    public class CustomerAddressReportPresenter
    {
        private readonly globalserveEntities _dbCon;

        public CustomerAddressReportPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public List<CustomerAddressReportOutput> getData(int customerID, int countryID, int addressTypeID)
        {
            List<CustomerAddressReportOutput> ListOutputData = new List<CustomerAddressReportOutput>();

            var customerAddresses = _dbCon.tblCustomerAddresses.Where(i => countryID != 0 ? (i.CountryID == countryID && i.CustomerID == customerID) : (i.CustomerID == customerID)).ToList();

            if (customerAddresses.Count >0)
            {
                foreach (var address in customerAddresses)
                {
                    CustomerAddressReportOutput Address = new CustomerAddressReportOutput();

                    Address.CondensedAddress = address.CondensedAddress;
                    Address.Address1 = address.Address1;
                    Address.PostalCode = address.PostalCode;
                    Address.Country = address.tblCountry.CountryName;
                    Address.City = address.City;

                    var destinationAddressData = _dbCon.tblCustomerDestinations.Where(i => i.CustomerDestinationID == address.CustomerAddressID).Count();
                    var invoiceAddressData = _dbCon.tblCustomerInvoiceLocations.Where(i => i.CustomerInvoiceLocationID == address.CustomerAddressID).Count();

                    if (addressTypeID == 0)
                    {
                        if (destinationAddressData > 0 && invoiceAddressData > 0)
                        {
                            Address.AddressType = "Both";
                        }
                        else if (destinationAddressData <= 0 && invoiceAddressData > 0)
                        {
                            Address.AddressType = "Bill To";
                        }
                        else if (destinationAddressData > 0 && invoiceAddressData <= 0)
                        {
                            Address.AddressType = "Ship To";
                        }

                        ListOutputData.Add(Address);
                    }
                    else if (addressTypeID == 1)
                    {
                        if (destinationAddressData > 0)
                        {
                            Address.AddressType = "Ship To";
                            ListOutputData.Add(Address);
                        }
                    }
                    else if (addressTypeID == 2)
                    {
                        if (invoiceAddressData > 0)
                        {
                            Address.AddressType = "Bill To";
                            ListOutputData.Add(Address);
                        }
                    }
                    else if (addressTypeID == 3)
                    {
                        if (destinationAddressData > 0 && invoiceAddressData > 0)
                        {
                            Address.AddressType = "Both";
                            ListOutputData.Add(Address);
                        }
                    }
                }
            }

            
            return ListOutputData;
        }
    }
}

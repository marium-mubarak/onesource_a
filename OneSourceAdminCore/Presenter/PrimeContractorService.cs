﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class PrimeContractorService
    {
        private globalserveEntities dbCon;
        public PrimeContractorService()
        {
            dbCon = new globalserveEntities();
        }


        /// <summary>
        /// Prime contractors list
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<tblPrimeContractor> GetPrimeContractorList(string name)
        {
            var list = dbCon.tblPrimeContractors.OrderBy(x => x.PrimeContractorName).ToList();

            if (!string.IsNullOrWhiteSpace(name) && name != "All")
            {
                list = list.Where(x => x.PrimeContractorName.ToLower().Contains(name.Trim().ToLower())).ToList();
            }
            return list;
        }

        /// <summary>
        /// Order format list 
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetOrderFormatList()
        {
            var list = dbCon.tblOrderFormats.Where(x => x.IsActive).Select(x => new GenericList()
            {
                Key = x.OrderFormatID,
                Value = x.OrderFormatName.Trim()

            }).OrderBy(x => x.Value).ToList();
            return list;
        }

        /// <summary>
        /// Get parent companies
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetParentCompanyList()
        {
            var list = dbCon.tblParentCompanies.Where(x => x.IsActive).Select(x => new GenericList()
            {
                Key = x.ParentCompanyID,
                Value = x.ParentCompanyName.Trim()

            }).OrderBy(x => x.Value).ToList();
            return list;
        }


        public tblPrimeContractor GetPrimeContractorById(int id)
        {
            var primeContractor = dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorID == id);
            return primeContractor;
        }

        public bool AddNewPrimeContractor(tblPrimeContractor primeContractor, int userId)
        {
            try
            {
                primeContractor.OrderEmail = primeContractor.Email ?? "";
                primeContractor.TicketEmail = primeContractor.Email ?? "";
                primeContractor.PartQuoteEmail = primeContractor.Email ?? "";
                primeContractor.LastUpdatedByUserID = userId;
                primeContractor.LastUpdated = DateTime.Now;
                primeContractor.IsActive = 1;
                dbCon.tblPrimeContractors.Add(primeContractor);
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewPrimeContractor");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewPrimeContractor");
                }
            }
            return false;
        }


        public bool UpdatePrimeContractor(tblPrimeContractor primeContractor, int userId)
        {
            try
            {
                //var getPrimeContractor =
                //    dbCon.tblPrimeContractors.FirstOrDefault(
                //        x => x.PrimeContractorID == primeContractor.PrimeContractorID);

                primeContractor.OrderEmail = primeContractor.Email ?? "";
                primeContractor.TicketEmail = primeContractor.Email ?? "";
                primeContractor.PartQuoteEmail = primeContractor.Email ?? "";
                primeContractor.LastUpdatedByUserID = userId;
                primeContractor.LastUpdated = DateTime.Now;

                //getPrimeContractor = primeContractor;
                dbCon.Entry(primeContractor).State = System.Data.Entity.EntityState.Modified;
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdatePrimeContractor");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdatePrimeContractor");
                }
            }
            return false;
        }

        public List<tblPrimeContractorserviceLocation> GetLocationsByPrimeContractorId(int primeContractorId)
        {
            try
            {

                var data =
                    dbCon.tblPrimeContractorserviceLocations.Where(x => x.PrimeContractorID == primeContractorId)
                        .ToList();
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetLocationsByPrimeContractorId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetLocationsByPrimeContractorId");
                }
                return new List<tblPrimeContractorserviceLocation>();
            }

        }

        public tblPrimeContractorserviceLocation GetPcLocationById(int id)
        {
            var location =
                dbCon.tblPrimeContractorserviceLocations.FirstOrDefault(x => x.PrimeContractorLocationID == id);
            return location;
        }

        public List<GenericList> GetDateFormatList()
        {
            var list =
                dbCon.tblDateFormats.Where(x => x.IsActive)
                    .OrderByDescending(x => x.IsDefaultFormat)
                    .Select(x => new GenericList()
                    {
                        Key = x.DateFormatID,
                        Value = x.DateFormatName.Trim()

                    }).OrderBy(x => x.Value).ToList();
            return list;
        }

        public bool UpdatePcLocation(tblPrimeContractorserviceLocation pcLocation, int userId)
        {
            try
            {

                pcLocation.LastUpdatedByUserID = userId;
                pcLocation.LastUpdated = DateTime.Now;

                dbCon.Entry(pcLocation).State = System.Data.Entity.EntityState.Modified;
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdatePcLocation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdatePcLocation");
                }
            }
            return false;
        }

        public bool AddNewPcLocation(tblPrimeContractorserviceLocation pcLocation, int userId)
        {
            try
            {
                pcLocation.LastUpdatedByUserID = userId;
                pcLocation.LastUpdated = DateTime.Now;
                pcLocation.IsActive = 1;
                dbCon.tblPrimeContractorserviceLocations.Add(pcLocation);
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewPcLocation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewPcLocation");
                }
            }
            return false;
        }

        public List<tblPrimeContractorExchangeRate> GetExhangeRatesByPrimeContractorId(int pcId)
        {
            var exchangeRates = dbCon.tblPrimeContractorExchangeRates.Where(x => x.PrimeContractorID == pcId).ToList();
            return exchangeRates;
        }

        public List<GenericList> GetPcExchangeRatesCurrencyList()
        {
            var list =
             dbCon.tblCurrencies.Where(x => x.IsActive)
                 .Select(x => new GenericList()
                 {
                     Key = x.CurrencyID,
                     Value = x.CurrencyName.Trim() + " (" + x.CurrencySymbol + ")",

                 }).OrderBy(x => x.Value).ToList();
            return list;
        }

        public tblPrimeContractorExchangeRate GetPcExchangeRatesByCurrencyIdAndPcId(int currencyId, int pcId)
        {
            var exRate =
                dbCon.tblPrimeContractorExchangeRates.FirstOrDefault(
                    x => x.PrimeContractorID == pcId && x.CurrencyID == currencyId);
            return exRate;
        }


        public bool AddUpdatePcExchangeRatesByCurrencyIdAndPcId(int currencyId, int pcId, string exRates, int userId)
        {
            try
            {
                var exRate =
                    dbCon.tblPrimeContractorExchangeRates.FirstOrDefault(
                        x => x.PrimeContractorID == pcId && x.CurrencyID == currencyId);

                var rates = Convert.ToDecimal(exRates);
                if (exRate != null)
                {
                    exRate.LastUpdated = DateTime.Now;
                    exRate.LastUpdatedByUserID = userId;
                    exRate.IsActive = 1;
                    exRate.ExchangeRateToUsDollars = rates;
                    dbCon.Entry(exRate).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();
                    return true;
                }
                else
                {
                    var newExtRates = new tblPrimeContractorExchangeRate()
                    {
                        CurrencyID = currencyId,
                        PrimeContractorID = pcId,
                        ExchangeRateToUsDollars = rates,
                        LastUpdated = DateTime.Now,
                        ExchangeRateDate = DateTime.Now,
                        IsActive = 1,
                        LastUpdatedByUserID = userId
                    };
                    dbCon.tblPrimeContractorExchangeRates.Add(newExtRates);
                    dbCon.SaveChanges();
                    return true;

                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdatePcExchangeRatesByCurrencyIdAndPcId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdatePcExchangeRatesByCurrencyIdAndPcId");
                }

            }
            return false;
        }

        public List<Sp_GetPrimeContractorUser_AdminR_Result> GetPcUsersByPcId(int pcId)
        {

            var getPcUsers = dbCon.Sp_GetPrimeContractorUser_AdminR(pcId).ToList();
            return getPcUsers;
        }


        public bool UpdateUserStatus(int userId, bool isActive, int updatedByUserId)
        {
            try
            {
                var tblUser = dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId);
                tblUser.IsActive = Convert.ToByte(isActive);
                tblUser.LastUpdated = DateTime.Now;
                tblUser.LastUpdatedByUserID = updatedByUserId;
                dbCon.Entry(tblUser).State = System.Data.Entity.EntityState.Modified;
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateUserStatus");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateUserStatus");
                }
            }
            return false;
        }

        public bool UpdateUserAccess(int userId, int securityLevel, int currentUserId, string userGroupName)
        {
            try
            {
                var getUserGroupId =
                    dbCon.tblUserGroups.FirstOrDefault(x => x.IsActive == 1 && x.UserSecurityLevel == securityLevel && x.UserGroupName.ToLower() == userGroupName);
                if (getUserGroupId != null)
                {
                    var pcUser = dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId);

                    pcUser.UserGroupID = getUserGroupId.UserGroupID;
                    pcUser.LastUpdated = DateTime.Now;
                    pcUser.LastUpdatedByUserID = currentUserId;
                    dbCon.Entry(pcUser).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateUserAccess");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateUserAccess");
                }
            }
            return false;
        }


        public PrimeContractorUserModel GetPcUserById(int userId)
        {

            var user = dbCon.tblUsers.Join
                (dbCon.tblISDUsers, a => a.UserID, b => b.UserID, (a, b) => new { a, b })
                .Where(@t => @t.a.UserID == userId)
                .Select(@t => new PrimeContractorUserModel
                {
                    UserId = @t.a.UserID,
                    UserName = @t.a.UserName,
                    Password = @t.a.UserPassword,
                    IsActive = @t.a.IsActive,
                    LastUpdated = @t.a.LastUpdated,
                    DateFormatId = @t.a.DateFormatID ?? 0,
                    Email = @t.b.Email,
                    PrimeContractorId = @t.b.PrimeContractorID,
                    PrimeContractorLocationId = @t.b.PrimeContractorLocationID,
                    LastUpdateByUser = @t.a.LastUpdatedByUserID
                }).FirstOrDefault();
            return user;
        }

        public string GetGsContactEmailByCustomerId(int customerId)
        {

            try
            {

                if (customerId != 0)
                {
                    var getData = dbCon.Sp_GetGsContactEmailByCustomerId_R(customerId).FirstOrDefault();
                    if (getData != null && !string.IsNullOrWhiteSpace(getData.email))
                    {
                        return getData.email;
                    }
                    else
                    {
                        return "automailer@global-serve.com";
                    }


                }
                return "";
            }

            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetGsContactEmailByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetGsContactEmailByCustomerId");
                }
                return "";
            }

        }

        public bool AddNewPcUser(PrimeContractorUserModel pcUser, AdminSetting currentSetting)
        {
            try
            {
                if (pcUser != null && pcUser.PrimeContractorId > 0)
                {
                    var userGroup =
                        dbCon.tblUserGroups.FirstOrDefault(x => x.UserGroupName.ToLower().Contains("isd"));

                    var primeContractor =
                        dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorID == pcUser.PrimeContractorId);

                    var splitName = pcUser.UserName.Split(' ');
                    var lastName = pcUser.UserName;
                    var firstName = "";
                    if (splitName.Length > 1)
                    {
                        lastName = splitName[1];
                        firstName = splitName[0];
                    }

                    var newUserId = dbCon.Sp_CreateOneSourceUser_AdminR(pcUser.UserName, pcUser.Password,
                    userGroup.UserGroupID, "",
                    pcUser.DateFormatId.ToString(), "", null, "INTERNAL", lastName, firstName, null,
                    null, null, null, null, null, null, primeContractor.ParentCompanyID, null, pcUser.Email,
                    null, null, null, null, null, null, null, currentSetting.ParentCompanyID, currentSetting.UserID).FirstOrDefault();

                    if (newUserId != null)
                    {
                        var isdUser = new tblISDUser()
                        {

                            UserID = Convert.ToInt32(newUserId),
                            PrimeContractorID = pcUser.PrimeContractorId,
                            PrimeContractorLocationID = pcUser.PrimeContractorLocationId,
                            IsActive = 1,
                            Email = pcUser.Email,
                            LastUpdated = DateTime.Now,
                            LastUpdatedByUserID = currentSetting.UserID,

                        };
                        dbCon.tblISDUsers.Add(isdUser);
                        dbCon.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewPcUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewPcUser");
                }
            }
            return false;
        }

        public bool EditPcUser(PrimeContractorUserModel pcUser, AdminSetting currentSetting)
        {
            try
            {
                if (pcUser != null && pcUser.PrimeContractorId > 0)
                {

                    var user = dbCon.tblUsers.FirstOrDefault(x => x.UserID == pcUser.UserId);

                    if (user != null)
                    {
                        user.UserName = pcUser.UserName.Trim();
                        user.UserPassword = pcUser.Password.Trim();
                        user.DateFormatID = pcUser.DateFormatId;
                        user.IsActive = pcUser.IsActive;
                        user.LastUpdated = DateTime.Now;
                        user.LastUpdatedByUserID = currentSetting.UserID;

                        dbCon.Entry(user).State = System.Data.Entity.EntityState.Modified;

                        var isdUser = dbCon.tblISDUsers.FirstOrDefault(x => x.UserID == pcUser.UserId);
                        if (isdUser != null)
                        {
                            isdUser.PrimeContractorID = pcUser.PrimeContractorId;
                            isdUser.PrimeContractorLocationID = pcUser.PrimeContractorLocationId;
                            isdUser.IsActive = pcUser.IsActive;
                            isdUser.Email = pcUser.Email;
                            isdUser.LastUpdated = DateTime.Now;
                            isdUser.LastUpdatedByUserID = currentSetting.UserID;
                            dbCon.Entry(isdUser).State = System.Data.Entity.EntityState.Modified;
                        }

                        dbCon.SaveChanges();

                        return true;

                    }
                    return false;

                }

                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "EditPcUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "EditPcUser");
                }
            }
            return false;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System.Data.Entity.Validation;

namespace OneSourceAdminCore.Presenter
{
    public class CustomerCatalogService
    {
        private globalserveEntities dbCon;

        public CustomerCatalogService()
        {
            dbCon = new globalserveEntities();
        }


        public tblCatalogPart GetCatalogPartById(int customerId, int partId)
        {
            try
            {
                var catalogPart =
                    dbCon.tblCatalogParts.FirstOrDefault(x => x.CatalogPartID == partId && x.CustomerID == customerId);
                return catalogPart;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCatalogPartById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCatalogPartById");
                }
                return new tblCatalogPart();
            }
        }

        public tblManufacturerPart GetManfPartById(int manfPartId)
        {
            try
            {
                var manfPart =
                    dbCon.tblManufacturerParts.FirstOrDefault(x => x.ManufacturerPartID == manfPartId);
                return manfPart;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCatalogPartById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCatalogPartById");
                }
                return new tblManufacturerPart();
            }
        }


        public List<Sp_GetManfParts_AdminR_Result> GetManfParts(int? page, int? limit, string sortBy, string direction,
            string searchString, out int total)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(searchString))
                {
                    searchString = "";
                }
                total = dbCon.Sp_GetManfParts_AdminR(searchString).Count();
                var records = dbCon.Sp_GetManfParts_AdminR(searchString).AsQueryable();

                //total = foreRecords.Count();
                if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
                {
                    if (direction.Trim().ToLower() == "asc")
                    {
                        records = SortHelper.OrderBy(records, sortBy);
                    }
                    else
                    {
                        records = SortHelper.OrderByDescending(records, sortBy);
                    }
                }

                if (page.HasValue && limit.HasValue)
                {
                    int start = (page.Value - 1) * limit.Value;
                    records = records.Skip(start).Take(limit.Value);
                }

                var finalList = records.ToList();



                return finalList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManfParts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManfParts");
                }
                total = 0;
                return new List<Sp_GetManfParts_AdminR_Result>();
            }
        }

        public List<GenericList> GetGsPartCategoriesListByCompany(int companyId)
        {
            try
            {
                var data =
                    dbCon.Sp_GetGsPartCategoryList_AdminR(companyId).Select(x => new GenericList()
                    {
                        Key = x.PartCategoryId,
                        Value = x.PartCategoryName
                    }).ToList();

                if (data == null || data.Count < 1)
                {
                    data.Insert(0, new Model.GenericList
                    {
                        Key = 0,
                        Value = "(None)"
                    });
                }

                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActiveSuppliers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActiveSuppliers");
                }

                return new List<GenericList>();
            }

        }


        public List<GenericList> GetActiveSuppliers()
        {
            try
            {
                var data =
                    dbCon.tblSFAs.Where(x => x.IsActive && x.IsComposite == true && x.ParentCompanyID != null)
                        .Select(x => new GenericList()
                        {
                            Key = x.SFAID,
                            Value = x.SFAName
                        }).OrderBy(x => x.Value).ToList();

                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActiveSuppliers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActiveSuppliers");
                }

                return new List<GenericList>();
            }
        }


        public bool IsDuplicate(int manfPartId, int sfaId, int priceCodeId, int customerId, int catalogPartId)
        {
            var chkDuplicate =
                dbCon.tblCatalogParts.Any(
                    x =>
                        x.CustomerID == customerId && x.ManufacturerPartID == manfPartId && x.PriceCodeID == priceCodeId && x.SFAID == sfaId && x.CatalogPartID != catalogPartId);
            return chkDuplicate;
        }


        public bool IsDuplicateBundleCheck(int sfaId, int priceCodeId, int customerId, int catalogStdId, string desc)
        {
            var chkDuplicate =
                dbCon.tblCatalogStandards.Any(
                    x =>
                        x.CustomerID == customerId && x.CatalogStandardID != catalogStdId && x.PriceCodeID == priceCodeId && x.SFAID == sfaId && x.Description.ToLower() == desc.ToLower().Trim());
            return chkDuplicate;
        }

        public string AddNewCatalogPart(CustomerCatalogModel model, AdminSetting currentSetting)
        {
            try
            {

                var catalogId = dbCon.sp_AddCustomerCatalogPart_AdminR("", model.CatalogPart.Description,
                    model.CatalogPart.GlobalPrice, model.CatalogPart.GlobalPriceCurrencyID,
                    model.CatalogPart.CustomerID, model.CatalogPart.UNSPSC, model.CatalogPart.SFAID
                    , model.CatalogPart.PartCategoryID, model.CatalogPart.PriceCodeID,
                    model.CatalogPart.ManufacturerPartID, currentSetting.UserID, 1).FirstOrDefault();

                if (catalogId != null && catalogId > 0)
                {

                    if (model.CountrifyIn > 0)
                    {
                        var countriFyPart = new tblCustomerCountryPart();
                        countriFyPart.ManufacturerPartID = model.CatalogPart.ManufacturerPartID;
                        countriFyPart.CustomerID = model.CatalogPart.CustomerID;
                        countriFyPart.CatalogPartID = Convert.ToInt32(catalogId);
                        countriFyPart.PartPrice = model.CatalogPart.GlobalPrice;
                        countriFyPart.PartPriceCurrencyID = model.CatalogPart.GlobalPriceCurrencyID;
                        countriFyPart.Discount = 0;
                        countriFyPart.NonDirectPurchaseUplift = 0;
                        countriFyPart.PartDescription = model.CatalogPart.Description;
                        countriFyPart.CatalogPartID = model.CatalogPart.PartCategoryID;
                        countriFyPart.DestinationCountryID = model.CountrifyIn;
                        countriFyPart.UNSPSC = model.CatalogPart.UNSPSC;
                        countriFyPart.PriceCodeID = model.CatalogPart.PriceCodeID;
                        countriFyPart.SFAID = model.CatalogPart.SFAID;
                        countriFyPart.LastUpdatedByUserID = currentSetting.UserID;
                        countriFyPart.LastUpdated = DateTime.Now;
                        countriFyPart.EOL = model.Eol;
                        countriFyPart.IsActive = true;

                        dbCon.tblCustomerCountryParts.Add(countriFyPart);
                        dbCon.SaveChanges();
                    }

                    return "CatalogId:" + catalogId;
                }
                else
                {
                    return "error";
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewCatalogPart");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewCatalogPart");
                }
                return "error";
            }
        }


        public string UpdateCatalogPart(CustomerCatalogModel model, AdminSetting currentSetting)
        {
            try
            {
                var catalogPart =
                    dbCon.tblCatalogParts.FirstOrDefault(x => x.CatalogPartID == model.CatalogPart.CatalogPartID);

                if (catalogPart != null)
                {

                    catalogPart.Description = model.CatalogPart.Description;
                    catalogPart.IsActive = model.CatalogPart.IsActive;
                    catalogPart.hide = model.CatalogPart.hide;
                    if (model.CatalogPart.GlobalPrice > 0)
                    {
                        catalogPart.GlobalPrice = model.CatalogPart.GlobalPrice;
                    }

                    catalogPart.SFAID = model.CatalogPart.SFAID;
                    catalogPart.LastUpdated = DateTime.Now;
                    catalogPart.LastUpdatedByUserID = currentSetting.UserID;
                    catalogPart.ManufacturerPartID = model.CatalogPart.ManufacturerPartID;
                    catalogPart.PartCategoryID = model.CatalogPart.PartCategoryID;
                    catalogPart.UNSPSC = model.CatalogPart.UNSPSC;

                    dbCon.Entry(catalogPart).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();

                    var countrifiedParts =
                        dbCon.tblCustomerCountryParts.Where(
                            x =>
                                x.CatalogPartID == model.CatalogPart.CatalogPartID &&
                                x.CustomerID == model.CatalogPart.CustomerID).ToList();

                    foreach (var item in countrifiedParts)
                    {
                        item.PartCategoryID = model.CatalogPart.PartCategoryID;
                        item.PriceCodeID = model.CatalogPart.PriceCodeID;
                        item.UNSPSC = model.CatalogPart.UNSPSC;
                        if (!model.CatalogPart.IsActive)
                        {
                            item.IsActive = false;
                        }

                    }
                    if (countrifiedParts.ToList().Count > 0)
                    {
                        // dbCon.Entry(countrifiedParts).State = System.Data.Entity.EntityState.Modified;
                        dbCon.SaveChanges();
                    }


                    if (!model.CatalogPart.IsActive)
                    {
                        var tblPartStandards =
                            dbCon.tblPartsInStandards.Where(x => x.CatalogPartID == model.CatalogPart.CatalogPartID)
                                .ToList();

                        dbCon.tblPartsInStandards.RemoveRange(tblPartStandards);
                        dbCon.SaveChanges();

                    }

                    return "true";
                }
                else
                {
                    return "error";
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCatalogPart");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCatalogPart");
                }
                return "error";
            }
        }


        public tblCatalogPart GetCatalogDetailsById(int customerId, int catalogPartId)
        {

            try
            {
                var catalog =
                    dbCon.tblCatalogParts.FirstOrDefault(
                        x => x.CatalogPartID == catalogPartId && x.CustomerID == customerId);
                return catalog ?? new tblCatalogPart();

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCatalogDetailsById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCatalogDetailsById");
                }
                return new tblCatalogPart();
            }


        }

        public List<GenericList> GetCatalogCountrifyCountries(int customerId, int catalogPartId, int priceCodeId)
        {
            try
            {
                var countries =
                    dbCon.Sp_GetCustomerCountriesToCountrify_AdminR(customerId, catalogPartId, priceCodeId).Select(x => new GenericList()
                    {
                        Value = x.CountryName + (x.CustomerPartID != null ? "(<b>Countrified</b>)" : ""),
                        Key = x.CountryID,
                        Count = Convert.ToInt32(x.CustomerPartID) //to check if it's countrified or not
                    }).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerCatalogCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerCatalogCountries");
                }
                return new List<GenericList>();
            }
        }


        public List<GenericList> GetQuickSettingCountryList(int customerId, int catalogPartId, int priceCodeId)
        {
            try
            {
                var countries =
                    dbCon.Sp_GetCustomerCountriesToCountrify_AdminR(customerId, catalogPartId, priceCodeId).Where(x => x.CustomerPartID != null && x.CustomerPartID > 0).Select(x => new GenericList()
                    {
                        Value = x.CountryName,
                        Key = x.CountryID,
                    }).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuickSettingCountryList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuickSettingCountryList");
                }
                return new List<GenericList>();
            }
        }


        public Sp_GetCustomerCountryPartDetail_AdminR_Result GetCountryPartInfo(int customerId, int customerCatalogPart, int countryId)
        {
            try
            {
                var chkPartExist =
                    dbCon.tblCustomerCountryParts.FirstOrDefault(
                        x =>
                            x.CatalogPartID == customerCatalogPart && x.CustomerID == customerId &&
                            x.DestinationCountryID == countryId);
                if (chkPartExist != null)
                {
                    var data =
                        dbCon.Sp_GetCustomerCountryPartDetail_AdminR(chkPartExist.CustomerPartID).FirstOrDefault();
                    return data;
                }


                return new Sp_GetCustomerCountryPartDetail_AdminR_Result();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCountryPartInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountryPartInfo");
                }
                return new Sp_GetCustomerCountryPartDetail_AdminR_Result();
            }
        }


        public string UpdateCustomerCountryPart(CustomerCatalogModel model, AdminSetting currentSetting)
        {
            try
            {
                var customerPart =
                    dbCon.tblCustomerCountryParts.FirstOrDefault(x => x.CustomerPartID == model.CountryPartInfo.CustomerPartID);

                if (customerPart != null)
                {

                    customerPart.PartDescription = model.CountryPartInfo.PartDescription;
                    customerPart.IsActive = model.CountryPartInfo.IsActive;

                    if (model.CountryPartInfo.PartPrice > 0)
                    {
                        customerPart.PartPrice = model.CountryPartInfo.PartPrice;
                    }

                    customerPart.SFAID = model.CountryPartInfo.SFAID;
                    customerPart.PriceCodeID = model.CountryPartInfo.PriceCodeID;
                    customerPart.LastUpdated = DateTime.Now;
                    customerPart.EOL = model.CountryPartInfo.EOL;
                    customerPart.LastUpdatedByUserID = currentSetting.UserID;
                    customerPart.ManufacturerPartID = model.CountryPartInfo.ManufacturerPartID;
                    customerPart.CustomerID = model.CustomerId;
                    customerPart.PartCategoryID = model.CountryPartInfo.PartCategoryID;
                    customerPart.CatalogPartID = model.CountryPartInfo.CatalogPartID;
                    customerPart.PartPriceCurrencyID = model.CountryPartInfo.PartPriceCurrencyID;
                    customerPart.IsActive = model.CountryPartInfo.IsActive;
                    customerPart.UNSPSC = model.CatalogPart.UNSPSC;

                    customerPart.PartTradeBlockUplift = Convert.ToDecimal(model.CountryPartInfo.PartTradeBlockUplift) / 100;
                    customerPart.SFASpecialUplift = Convert.ToDecimal(model.CountryPartInfo.SFASpecialUplift) / 100;

                    customerPart.NonDirectPurchaseUplift = Convert.ToDecimal(model.CountryPartInfo.NonDirectPurchaseUplift) / 100;
                    customerPart.Discount = Convert.ToDecimal(model.CountryPartInfo.Discount) / 100;

                    dbCon.Entry(customerPart).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();

                    return "true";
                }
                else
                {
                    return "error";
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerCountryPart");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerCountryPart");
                }
                return "error";
            }
        }

        public string AddCustomerCountryPart(CustomerCatalogModel model, AdminSetting currentSetting)
        {
            try
            {
                var customerPart = new tblCustomerCountryPart();

                customerPart.PartDescription = model.CountryPartInfo.PartDescription;
                customerPart.IsActive = model.CountryPartInfo.IsActive;

                if (model.CountryPartInfo.PartPrice > 0)
                {
                    customerPart.PartPrice = model.CountryPartInfo.PartPrice;
                }

                customerPart.SFAID = model.CountryPartInfo.SFAID;
                customerPart.PriceCodeID = model.CountryPartInfo.PriceCodeID;
                customerPart.LastUpdated = DateTime.Now;
                customerPart.EOL = model.CountryPartInfo.EOL;
                customerPart.LastUpdatedByUserID = currentSetting.UserID;
                customerPart.ManufacturerPartID = model.CountryPartInfo.ManufacturerPartID;
                customerPart.CustomerID = model.CustomerId;
                customerPart.PartCategoryID = model.CountryPartInfo.PartCategoryID;
                customerPart.CatalogPartID = model.CountryPartInfo.CatalogPartID;
                customerPart.PartPriceCurrencyID = model.CountryPartInfo.PartPriceCurrencyID;
                customerPart.DestinationCountryID = model.CountryPartInfo.DestinationCountryID;
                customerPart.UNSPSC = model.CatalogPart.UNSPSC;

                customerPart.PartTradeBlockUplift = Convert.ToDecimal(model.CountryPartInfo.PartTradeBlockUplift) / 100;
                customerPart.SFASpecialUplift = Convert.ToDecimal(model.CountryPartInfo.SFASpecialUplift) / 100;

                customerPart.NonDirectPurchaseUplift = Convert.ToDecimal(model.CountryPartInfo.NonDirectPurchaseUplift) / 100;
                customerPart.Discount = Convert.ToDecimal(model.CountryPartInfo.Discount) / 100;

                dbCon.tblCustomerCountryParts.Add(customerPart);
                dbCon.SaveChanges();

                return "CustomerPartId:" + customerPart.CustomerPartID;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerCountryPart");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerCountryPart");
                }
                return "error";
            }
        }


        public bool UpdateCatalogQuickSettings(List<QuickSettingModel> setups, AdminSetting currentSetting)
        {
            try
            {
                if (setups != null && setups.Count > 0)
                {

                    var selectCountryIds = setups.Select(x => x.CountryId).ToList();
                    var customerId = setups.FirstOrDefault().CustomerId;
                    var catalogId = setups.FirstOrDefault().CatalogId;
                    var isActive = setups.FirstOrDefault().IsActive;
                    var eol = setups.FirstOrDefault().Eol;
                    var price = setups.FirstOrDefault().Price;
                    var currencyId = setups.FirstOrDefault().CurrencyId;
                    var onlyEol = setups.FirstOrDefault().UpdateEol;

                    var customerCatParts =
                        dbCon.tblCustomerCountryParts.Where(
                            x =>
                                selectCountryIds.Contains(x.DestinationCountryID) && x.CustomerID == customerId &&
                                x.CatalogPartID == catalogId).ToList();
                    if (onlyEol)
                    {
                        customerCatParts.ForEach(x =>
                        {
                            x.EOL = eol;
                            x.LastUpdated = DateTime.Now;
                            x.LastUpdatedByUserID = currentSetting.UserID;
                        });
                    }
                    else
                    {
                        customerCatParts.ForEach(x =>
                        {
                            x.IsActive = isActive;
                            x.EOL = eol;
                            x.PartPrice = price;
                            x.PartPriceCurrencyID = currencyId;
                            x.LastUpdated = DateTime.Now;
                            x.LastUpdatedByUserID = currentSetting.UserID;
                        });
                    }

                    if (customerCatParts.Count > 0)
                    {
                        //dbCon.Entry(customerCatParts).State = System.Data.Entity.EntityState.Modified;
                        dbCon.SaveChanges();
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCatalogQuickSettings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCatalogQuickSettings");


                }
                return false;
            }
        }


        //Catalog Bundles start

        public List<tblCatalogStandard> GetBundlesByCustomerId(int customerId, string searchTxt = "")
        {
            try
            {
                var bundles = new List<tblCatalogStandard>();

                bundles = dbCon.tblCatalogStandards.Where(x=>x.CustomerID == customerId && x.Description.Contains(searchTxt)).ToList();

                return bundles;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetBundlesByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetBundlesByCustomerId");


                }
                return new List<tblCatalogStandard>();
            }

        }


        public tblCatalogStandard GetCatalogStandardById(int customerId, int catStdId)
        {
            try
            {
                var bundle = new tblCatalogStandard();

                bundle = dbCon.tblCatalogStandards.FirstOrDefault(x=> x.CustomerID == customerId && x.CatalogStandardID == catStdId);

                return bundle;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCatalogStandardById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCatalogStandardById");


                }
                return new tblCatalogStandard();
            }

        }

        public List<sp_GetBundlePartsById_AdminR_Result> GetBundlesPartsById(int partStdId)
        {
            try
            {
                var parts = dbCon.sp_GetBundlePartsById_AdminR(partStdId).ToList();

                return parts;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetBundlesByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetBundlesByCustomerId");
                }
                return new List<sp_GetBundlePartsById_AdminR_Result>();
            }

        }

        public List<Sp_GetCatalogStandardPartsByCustomer_AdminR_Result> GetCustomerCatalogPartsByCustomerId(int customerId, int? page, int? limit, string sortBy, string direction,
         string searchString, out int total)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(searchString))
                {
                    searchString = "";
                }
                total = dbCon.Sp_GetCatalogStandardPartsByCustomer_AdminR(customerId, searchString).Count();
                var records = dbCon.Sp_GetCatalogStandardPartsByCustomer_AdminR(customerId, searchString).AsQueryable();

                //total = foreRecords.Count();
                if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
                {
                    if (direction.Trim().ToLower() == "asc")
                    {
                        records = SortHelper.OrderBy(records, sortBy);
                    }
                    else
                    {
                        records = SortHelper.OrderByDescending(records, sortBy);
                    }
                }

                if (page.HasValue && limit.HasValue)
                {
                    int start = (page.Value - 1) * limit.Value;
                    records = records.Skip(start).Take(limit.Value);
                }

                var finalList = records.ToList();

                return finalList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerCatalogParts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerCatalogParts");
                }
                total = 0;
                return new List<Sp_GetCatalogStandardPartsByCustomer_AdminR_Result>();
            }
        }


        public string UpdateBundle(CustomerCatalogModel model, AdminSetting currentSetting)
        {
            try
            {
                var bundle =
                    dbCon.tblCatalogStandards.FirstOrDefault(x => x.CatalogStandardID == model.CatalogStandard.CatalogStandardID && x.CustomerID == model.CatalogStandard.CustomerID);

                if (bundle != null)
                {

                    bundle.KeyIdentifier = model.CatalogStandard.KeyIdentifier;
                    bundle.DisplayPosition = model.CatalogStandard.DisplayPosition;
                    bundle.Description = model.CatalogStandard.Description;
                    bundle.Comments = model.CatalogStandard.Comments;
                    bundle.ImageFile = model.CatalogStandard.ImageFile;
                    bundle.IsActive = model.CatalogStandard.IsActive;
                    bundle.PartCategoryID = model.CatalogStandard.PartCategoryID;
                    bundle.UNSPSC = "43000000";
                    bundle.CountryIDList = model.CatalogStandard.CountryIDList;
                    bundle.CustomerID = model.CatalogStandard.CustomerID;
                    bundle.SFAID= model.CatalogStandard.SFAID;
                    bundle.LastUpdated = DateTime.Now;
                    bundle.LastUpdatedByUserID = currentSetting.UserID;

                    dbCon.Entry(bundle).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();


                    if (model.BundleParts != null && model.BundleParts.Any(x => x.CatalogPartId > 0))
                    {

                        var getExistingParts =
                            dbCon.tblPartsInStandards.Where(x => x.CatalogStandardID == bundle.CatalogStandardID)
                                .ToList();
                        if (getExistingParts != null && getExistingParts.Count > 0)
                        {
                            dbCon.tblPartsInStandards.RemoveRange(getExistingParts);
                            dbCon.SaveChanges();
                        }

                        var catalogInStd = model.BundleParts.Where(x => x.CatalogPartId > 0 && x.Quantity > 0).Select(x => new tblPartsInStandard()
                        {
                            CatalogPartID = x.CatalogPartId,
                            CatalogStandardID = x.CatalogStdId,
                            IsOption = x.IsOptional,
                            DisplayPriority = x.Priority,
                            OptionalSelected = x.OptionalSelected,
                            IsActive = true,
                            Quantity = x.Quantity,
                            LastUpdated = DateTime.Now,
                            LastUpdatedByUserID = currentSetting.UserID,
                        }).ToList();

                        if (catalogInStd.Any(x => x.CatalogPartID > 0 && x.CatalogStandardID > 0))
                        {
                            dbCon.tblPartsInStandards.AddRange(catalogInStd);
                            dbCon.SaveChanges();
                        }
                    }

                    return "true";
                }
                else
                {
                    return "error";
                }

            }
            catch (DbEntityValidationException dbEx)
            {

                StaticHelper.LogException(dbEx.ToString(), "UpdateBundle");
                if (dbEx.InnerException != null)
                {
                    StaticHelper.LogException(dbEx.InnerException.ToString(), "UpdateBundle");
                }
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        StaticHelper.LogException("Property: "+ validationError.PropertyName + " Error: "+ validationError.ErrorMessage, "UpdateBundle");
                    }
                }
                return "error";
            }
            //catch (Exception ex)
            //{
            //    StaticHelper.LogException(ex.ToString(), "UpdateBundle");

            //    if (ex.InnerException != null)
            //    {
            //        StaticHelper.LogException(ex.InnerException.ToString(), "UpdateBundle");
            //    }
            //    return "error";
            //}

        }

        public string AddNewBundle(CustomerCatalogModel model, AdminSetting currentSetting)
        {
            try
            {
                var bundle = new tblCatalogStandard();


                if (model.CatalogStandard != null)
                {

                    bundle.KeyIdentifier = model.CatalogStandard.KeyIdentifier;
                    bundle.DisplayPosition = model.CatalogStandard.DisplayPosition;
                    bundle.Description = model.CatalogStandard.Description;
                    bundle.Comments = model.CatalogStandard.Comments;
                    bundle.ImageFile = model.CatalogStandard.ImageFile;
                    bundle.CustomerID = model.CatalogStandard.CustomerID;
                    bundle.UNSPSC = "43000000";
                    bundle.IsActive = true;
                    bundle.PartCategoryID = model.CatalogStandard.PartCategoryID;
                    bundle.CountryIDList = model.CatalogStandard.CountryIDList;
                    bundle.SFAID = model.CatalogStandard.SFAID;

                    bundle.PriceCodeID = 1;

                    bundle.LastUpdated = DateTime.Now;
                    bundle.LastUpdatedByUserID = currentSetting.UserID;


                    dbCon.tblCatalogStandards.Add(bundle);
                    dbCon.SaveChanges();

                    if (model.BundleParts != null && model.BundleParts.Any(x => x.CatalogPartId > 0) && bundle.CatalogStandardID > 0)
                    {
                        var catalogInStd = model.BundleParts.Where(x=>x.CatalogPartId> 0 && x.Quantity> 0).Select(x => new tblPartsInStandard()
                        {
                            CatalogPartID = x.CatalogPartId,
                            CatalogStandardID = bundle.CatalogStandardID,
                            IsOption = x.IsOptional,
                            IsActive = true,
                            Quantity = x.Quantity,
                            LastUpdated = DateTime.Now,
                            LastUpdatedByUserID = currentSetting.UserID,
                        }).ToList();

                        if (catalogInStd.Any(x => x.CatalogPartID > 0 && x.CatalogStandardID > 0))
                        {
                            dbCon.tblPartsInStandards.AddRange(catalogInStd);
                            dbCon.SaveChanges();
                        }
                    }
                    return "CatStdId:" + bundle.CatalogStandardID;
                }
                else
                {
                    return "error";
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewBundle");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewBundle");
                }
                return "error";
            }
        }


        //Catalog Bundles End

    }
}

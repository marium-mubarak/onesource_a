﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Am Communication data"
    /// </summary>
    public class RequestEngineerLevelPresenter
    {

        private readonly globalserveEntities _dbCon;
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public RequestEngineerLevelPresenter()
        {
            _dbCon = new globalserveEntities();
        }
        /// <summary>
        /// get Request types by companyid
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<RequestsTypes> GetRequestsTypesListByCompanyId(int companyId)
        {

            var chkRequestsCount =
                _dbCon.tblActionRequestTypes.Where(x => x.IsActive && x.IsSelectable && x.CompanyID == companyId)
                    .ToList().Count;

            if (chkRequestsCount == 0)
            {
                return _dbCon.tblActionRequestTypes.Where(x => x.IsActive && x.IsSelectable && x.CompanyID == null).Select(x => new RequestsTypes()
                {
                    RequestId = x.ARTypeID,
                    RequestTypeName = x.ARTypeName

                }).OrderByDescending(x => x.RequestTypeName).ToList();

            }
            else
            {

                return _dbCon.tblActionRequestTypes.Where(x => x.IsActive && x.IsSelectable && x.CompanyID == companyId).Select(x => new RequestsTypes()
                {
                    RequestId = x.ARTypeID,
                    RequestTypeName = x.ARTypeName

                }).OrderByDescending(x => x.RequestTypeName).ToList();


            }


        }

        /// <summary>
        /// get priorities by company Id
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Priority> GetPriorityListByCompanyId(int companyId)
        {

            if(companyId==0)
            {
                return _dbCon.tblPriorities.Where(x => x.IsActive == true).Select(x=>new Priority {
                    PriorityId=x.PriorityID,
                    PriorityName=x.PriorityName

                }).ToList();
            }

            var includeGs = true;
            var chkPriority = _dbCon.tblPriorities.Where(x => x.CompanyID == companyId).ToList().Count;
            if (chkPriority > 0)
            {
                includeGs = false;
            }

            List<Priority> prioritiesList;
            if (includeGs)
            {
                var chkParentCompanyGs = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName != null && x.ParentCompanyName == "GlobalServe").Select(s => s.ParentCompanyID).ToList();
                prioritiesList = _dbCon.tblPriorities.Join(_dbCon.tblParentCompanies,
                   priority => priority.CompanyID,
                   company => company.ParentCompanyID,
                   (priority, company) => new
                   {
                       priority = priority,
                       company = company

                   })
                   .Where(
                       s =>
                           s.priority.IsActive &&
                           (chkParentCompanyGs.Contains(s.priority.CompanyID) || s.priority.CompanyID == companyId))
                   .OrderBy(o => o.priority.PriorityIntValue).Select(p => new Priority()
                   {
                       PriorityId = p.priority.PriorityID,
                       PriorityName = p.priority.PriorityName

                   })
                   .ToList();
            }
            else
            {

                prioritiesList = _dbCon.tblPriorities.Join(_dbCon.tblParentCompanies,
                   priority => priority.CompanyID,
                   company => company.ParentCompanyID,
                   (priority, company) => new
                   {
                       priority = priority,
                       company = company

                   })
                   .Where(
                       s =>
                           s.priority.IsActive && (s.priority.CompanyID == companyId))
                   .OrderBy(o => o.priority.PriorityIntValue).Select(p => new Priority()
                   {
                       PriorityId = p.priority.PriorityID,
                       PriorityName = p.priority.PriorityName

                   })
                   .ToList();
            }
            return prioritiesList;
        }

        /// <summary>
        /// get engineer level mapping with customer
        /// </summary>
        /// <returns></returns>
        public List<sp_GetEngineerLevelMapping_R_Result> GetEngineerLevel()
        {
            return _dbCon.sp_GetEngineerLevelMapping_R().ToList();
        }

        /// <summary>
        /// add Request engineer level to the data
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddRequestEngineerLevel(tblEngineerLevelMapping model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (model.CustomerId == 0)
                {
                    var data = _dbCon.tblEngineerLevelMappings.Where(x => x.CustomerId == 0 && x.RequestId==model.RequestId).FirstOrDefault();
                    if (data != null)
                    {
                        _dbCon.tblEngineerLevelMappings.Remove(data);
                    }
                }
                model.CustomerName = (model.CustomerName== "--Select--" || string.IsNullOrEmpty(model.CustomerName))==true?null: model.CustomerName;

                model.UpdatedDate = DateTime.Now;
                model.UpdatedBy = userId;
                _dbCon.tblEngineerLevelMappings.Add(model);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }

        /// <summary>
        /// delete the engineer level mapping
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Delete(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblEngineerLevelMappings.FirstOrDefault(x => x.EngineerLevelMappingId == id);
                if (data != null)
                {
                    _dbCon.tblEngineerLevelMappings.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }
        /// <summary>
        /// edit/update the request engineer level mapping
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditRequestEngineerLevel(tblEngineerLevelMapping model, int userId)
        {
            int count=0;
            tblEngineerLevelMapping myModel;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                 myModel =
                    _dbCon.tblEngineerLevelMappings.FirstOrDefault(x => x.CustomerId==model.CustomerId && x.RequestId==model.RequestId);
                if (model != null && myModel!=null) 
                {
                    myModel.CustomerId = model.CustomerId;
                    myModel.CustomerName = model.CustomerName;
                    myModel.PriorityId = model.PriorityId;
                    myModel.RequestId = model.RequestId;
                    myModel.UpdatedBy = userId;
                    myModel.UpdatedDate = DateTime.Now;
                    _dbCon.Entry(myModel).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();
                }
              
                transaction.Commit();

            }
            if (myModel == null) AddRequestEngineerLevel(model,userId);
            return count;
        }
    }
}

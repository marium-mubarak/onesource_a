﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Request data"
    /// </summary>
    public class RequestSettingPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public RequestSettingPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// get the Request setting model from customer presenter
        /// </summary>
        /// <returns></returns>
        public RequestSettingModel GetRequestSettingModel()
        {
            CustomerPresenter cp = new CustomerPresenter();

            RequestSettingModel model = new RequestSettingModel
            {
                Customers = cp.GetCustomersWithPriority()
            };
            return model;
        }

        /// <summary>
        /// get priorities by the company Id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="parentcompanyId"></param>
        /// <returns></returns>
        public List<Priority> GetPriorityListByCompanyId(int companyId, out int parentcompanyId)
        {
            parentcompanyId = 0;
            if (companyId == 0)
            {
                return _dbCon.tblPriorities.Where(x => x.IsActive == true).Select(x => new Priority
                {
                    PriorityId = x.PriorityID,
                    PriorityName = x.PriorityName

                }).ToList();
            }

            var includeGs = true;
            var chkPriority = _dbCon.tblPriorities.Where(x => x.CompanyID == companyId).ToList().Count;
            if (chkPriority > 0)
            {
                includeGs = false;
            }

            List<Priority> prioritiesList;
            if (includeGs)
            {
                var chkParentCompanyGs = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName != null && x.ParentCompanyName == "GlobalServe").Select(s => s.ParentCompanyID).ToList();
                parentcompanyId = 15;
                prioritiesList = _dbCon.tblPriorities.Join(_dbCon.tblParentCompanies,
                   priority => priority.CompanyID,
                   company => company.ParentCompanyID,
                   (priority, company) => new
                   {
                       priority = priority,
                       company = company

                   })
                   .Where(
                       s =>
                           s.priority.IsActive &&
                           (chkParentCompanyGs.Contains(s.priority.CompanyID) || s.priority.CompanyID == companyId))
                   .OrderBy(o => o.priority.PriorityIntValue).Select(p => new Priority()
                   {
                       PriorityId = p.priority.PriorityID,
                       PriorityName = p.priority.PriorityName

                   })
                   .ToList();
            }
            else
            {
                parentcompanyId = 0;
                prioritiesList = _dbCon.tblPriorities.Join(_dbCon.tblParentCompanies,
                   priority => priority.CompanyID,
                   company => company.ParentCompanyID,
                   (priority, company) => new
                   {
                       priority = priority,
                       company = company

                   })
                   .Where(
                       s =>
                           s.priority.IsActive && (s.priority.CompanyID == companyId))
                   .OrderBy(o => o.priority.PriorityIntValue).Select(p => new Priority()
                   {
                       PriorityId = p.priority.PriorityID,
                       PriorityName = p.priority.PriorityName

                   })
                   .ToList();
            }
            return prioritiesList;
        }
        /// <summary>
        /// get the list of generic priorities by the company id
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<GenericList> GetGenericList(int companyId)
        {
            return _dbCon.sp_GetGenericPriority_R(companyId).Select(x => new GenericList
            {
                Key = x.PriorityID,
                Value = x.PriorityName
            }).ToList();
        }
        /// <summary>
        /// add engineer level to the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddEngineerLevel(tblGenericPriority model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblGenericPriorities.Where(x => x.PriorityName == model.PriorityName.Trim()).ToList();
                if (data.Count > 0)
                {
                    count = 0;
                    return count;
                }

                model.LastUpdate = DateTime.Now;
                model.LastUpdateByUserID = userId;
                _dbCon.tblGenericPriorities.Add(model);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// get the priorities by the customerId and companyId
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public RequestSettingModel GetPriorities(int customerId, int companyId)
        {
            int parentcompanyId;
            RequestSettingModel model = new RequestSettingModel
            {
                LstGenericPriorities = GetGenericList(companyId).OrderBy(x=>x.Value).ToList(),
                LstPriorities = GetPriorityListByCompanyId(companyId, out parentcompanyId).OrderBy(x => x.PriorityName).ToList()
            };
            model.ParentCompanyId = parentcompanyId;
            return model;
        }

        /// <summary>
        /// Add engineer level to customer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddEngineerLevelCustomerWise(EngineerLevelModel model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {

                for (int i = 0; i < model.GenericPriorityIds.Count; i++)
                {
                    var Priority = new Entity.tblPriority();
                    Priority.CompanyID = model.CompanyId;
                    Priority.PriorityName = model.GenericsPriorityNames.Split(',')[i];
                    Priority.PriorityMinInt = 1;
                    Priority.PriorityMaxInt = 1;
                    Priority.PriorityIntValue = 1;
                    Priority.PriorityDefaultInt = 1;
                    Priority.IsActive = true;
                    Priority.LastUpdate = DateTime.Now;
                    Priority.LastUpdateUserID = userId;
                    _dbCon.tblPriorities.Add(Priority);
                }
                if (model.wasStandard)
                {
                    int parentCompanyId;
                    var standardPriorities = GetPriorityListByCompanyId(model.CompanyId,out parentCompanyId).ToList();
                    if (standardPriorities.Count > 0)
                    {
                        foreach (var item in standardPriorities)
                        {
                            var Priority = new Entity.tblPriority();
                            Priority.CompanyID = model.CompanyId;
                            Priority.PriorityName = item.PriorityName;
                            Priority.PriorityMinInt = 1;
                            Priority.PriorityMaxInt = 1;
                            Priority.PriorityIntValue = 1;
                            Priority.PriorityDefaultInt = 1;
                            Priority.IsActive = true;
                            Priority.LastUpdate = DateTime.Now;
                            Priority.LastUpdateUserID = userId;
                            _dbCon.tblPriorities.Add(Priority);

                        }
                    }
                }
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        public RequestDeleteModel DeleteRequest(string companyId, string RequestIds)
        {
            RequestDeleteModel obj = new RequestDeleteModel();
           // int count = 0;
            var lst = RequestIds.Split(',');
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                int priorityId;
                int intCompanyId = Convert.ToInt32(companyId);

               
               
                    foreach (var item in lst)
                    {
                        priorityId = Convert.ToInt32(item);
                        var record = _dbCon.tblPriorities.FirstOrDefault(x => x.PriorityID == priorityId && x.CompanyID == intCompanyId);
                        if (record != null)
                        {

                        var tickets = _dbCon.tblTickets.Where(x => x.PriorityID == record.PriorityID).ToList();
                        if (tickets.Count > 0) {
                            obj.Count = -1;
                            obj.PriorityName = record.PriorityName;
                            return obj;
                        }

                            _dbCon.tblPriorities.Remove(record);
                        obj.Count = _dbCon.SaveChanges();
                        }
                    }
                
                transcation.Commit();
            }
            return obj;
        }


    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class PaymentPresenter
    {
        globalserveEntities dbCon;
        public PaymentPresenter()
        {
            dbCon = new globalserveEntities();
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used for new payment. Returns the created ID, 0 if failed.
        /// </summary>
        /// <param name="tblPayment"></param>
        /// <returns>int</returns>
        public int NewPayment(tblPayment model)
        {
            try
            {
                ObjectParameter paymentid = new ObjectParameter("paymentid", typeof(global::System.Int32));
                paymentid.Value = DBNull.Value;

                dbCon.sp_NewPayment(Convert.ToDecimal(model.PaymentAmount), model.PaymentCurrencyID, model.PaymentTypeID, model.CompanyID, model.PayToCompanyID, model.PaymentTxTypeID,
                    model.VendorPaymentID != null ? model.VendorPaymentID : null, model.PaymentDate, model.Comments, model.LastUpdateUserID, paymentid);
                return (int)paymentid.Value;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "NewPayment");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "NewPayment");
                }
            }
            return 0;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to search payments and consumes sp_SearchPayment_AdminR.
        /// </summary>
        /// <param name="PaymentModel"></param>
        /// <returns>List<sp_SearchPayment_AdminR_Result></returns>
        public List<sp_SearchPayment_AdminR_Result> SearchPayments(PaymentModel model)
        {
            try
            {
                var result = dbCon.sp_SearchPayment_AdminR(model.ParentCompanyId.ToString(), model.PaymentId, model.InvoiceId, model.VendorPaymentId, model.PaidBy == 0 ? null : model.PaidBy.ToString(), model.PaidTo == 0 ? null : model.PaidTo.ToString(), model.PaymentMethodSelected == 0 ? null : model.PaymentMethodSelected.ToString(), model.PaymentTypeSelected == 0 ? null : model.PaymentTypeSelected.ToString(), model.PaidAfter, model.PaidBefore).ToList();
                return result;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "SearchPayments");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "SearchPayments");
                }
            }
            return null;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to fetch paid by companies list.
        /// </summary>
        /// <returns>List<GenericList></returns>
        public List<GenericList> GetPaidByCompaniesList()
        {
            try
            {
                var companyListInPayments = dbCon.tblPayments.Select(y => y.CompanyID).Distinct();
                var list = dbCon.tblParentCompanies.Where(x => companyListInPayments.Contains(x.ParentCompanyID)).Select(x => new GenericList
                {
                    Key = x.ParentCompanyID,
                    Value = x.ParentCompanyName
                }).OrderBy(x => x.Value).ToList();
                list.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPaidByCompaniesList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPaidByCompaniesList");
                }
            }
            return null;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to fetch paid to companies list.
        /// </summary>
        /// <returns>List<GenericList></returns>
        public List<GenericList> GetPaidToCompaniesList()
        {
            try
            {
                var companyPayToListInPayments = dbCon.tblPayments.Select(y => y.PayToCompanyID).Distinct();
                var list = dbCon.tblParentCompanies.Where(x => companyPayToListInPayments.Contains(x.ParentCompanyID)).Select(x => new GenericList
                {
                    Key = x.ParentCompanyID,
                    Value = x.ParentCompanyName
                }).OrderBy(x => x.Value).ToList();
                list.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPaidToCompaniesList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPaidToCompaniesList");
                }
            }
            return null;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to fetch transaction types (credit/debit) list.
        /// </summary>
        /// <returns>List<GenericList></returns>
        public List<GenericList> GetTxTypeList()
        {
            try
            {
                var list = dbCon.tblPaymentTxTypes.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.PaymentTxTypeID,
                    Value = x.PaymentTxTypeName
                }).OrderBy(x => x.Value).ToList();
                list.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTxTypeList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTxTypeList");
                }
            }
            return null;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to fetch payment methods list (Check/Cash etc).
        /// </summary>
        /// <returns>List<GenericList></returns>
        public List<GenericList> GetPaymentMethodList()
        {
            try
            {
                var list = dbCon.tblPaymentMethods.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.PaymentMethodID,
                    Value = x.PaymentMethodName
                }).OrderBy(x => x.Value).ToList();
                list.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPaymentMethodList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPaymentMethodList");
                }
            }
            return null;
        }


        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to update the comments of a particular payment.
        /// </summary>
        /// <param name="sp_SearchPayment_AdminR_Result"></param>
        /// <returns>bool</returns>
        public bool UpdateComment(sp_SearchPayment_AdminR_Result model)
        {
            try
            {
                var recordId = Convert.ToInt32(model.Payment_);
                var oldRecord = dbCon.tblPayments.Where(x => x.PaymentID == recordId).FirstOrDefault();
                var currentRecord = oldRecord;
                currentRecord.Comments = model.Comments;
                currentRecord.LastUpdate = DateTime.Parse(model.Last_Updated);
                currentRecord.LastUpdateUserID = Convert.ToInt32(model.Updated_By);

                dbCon.Entry(oldRecord).CurrentValues.SetValues(currentRecord);
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateComment");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateComment");
                }
            }
            return false;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method fetches all the active, new, selected invoices for a particular payment ID including invoice total.
        /// </summary>
        /// <param name="paymentId"></param>
        /// <param name="currencyId"></param>
        /// <param name="paidBy"></param>
        /// <param name="paidTo"></param>
        /// <param name="paymentType"></param>
        /// <returns>PaymentModel</returns>
        public PaymentModel GetCurrentInvoices(int paymentId, int currencyId, int paidBy, int paidTo, string paymentType)
        {
            try
            {
                var model = new PaymentModel();
                model.AllInvoices = new List<List<SelectListItem>>();
                model.NewInvoices = new List<SelectListItem>();

                dynamic allInvoices = null;
                dynamic allNewInvoices = null;
                decimal totalAmountForPayment = 0.0M;

                //Completed invoices
                model.SelectedInvoices = new List<InvoiceDetailModel>();
                model.SelectedInvoices = (from i in dbCon.tblInvoices
                                          join ip in dbCon.tblInvoicePayments on i.InvoiceID equals ip.InvoiceID
                                          where ip.PaymentID == paymentId && ip.IsActive && i.IsActive
                                          select new InvoiceDetailModel { InvoiceId = i.InvoiceID, CompanyInvoiceId = i.CompanyInvoiceID, Amount = ip.Amount, Description = ip.Description }).ToList();

                totalAmountForPayment = dbCon.tblInvoicePayments.Where(x => x.PaymentID == paymentId && x.IsActive).Select(x => x.Amount).DefaultIfEmpty(0).Sum();
                model.PaymentTotal = totalAmountForPayment;

                //All active invoices
                allInvoices = (from i in dbCon.tblInvoices
                               join c in dbCon.tblCurrencies on i.InvoiceCurrencyID equals c.CurrencyID
                               where (i.InvoiceCurrencyID == currencyId &&
                              i.CompanyID == (paymentType == "Credit" ? paidTo : paidBy) &&
                              i.BillToCompanyID == (paymentType == "Credit" ? paidBy : paidTo) &&
                               i.IsActive)
                               select new { i.InvoiceID, i.CompanyInvoiceID, i.InvoiceTotal, c.CurrencySymbol }).ToList();

                //All remaining invoices, room for new invoice
                allNewInvoices = (from i in dbCon.tblInvoices
                                  join c in dbCon.tblCurrencies on i.InvoiceCurrencyID equals c.CurrencyID
                                  where (i.InvoiceCurrencyID == currencyId &&
                                  i.CompanyID == (paymentType == "Credit" ? paidTo : paidBy) &&
                                  i.BillToCompanyID == (paymentType == "Credit" ? paidBy : paidTo) &&
                                  i.IsActive && !(from st in dbCon.tblInvoiceStatus
                                                  where st.InvoiceStatusName == "Paid In Full"
                                                  select st.InvoiceStatusID).Contains(i.InvoiceStatusID))
                                  select new { i.InvoiceID, i.CompanyInvoiceID, i.InvoiceTotal, c.CurrencySymbol }).ToList();


                //Iteration through all the active invoices
                foreach (var activeInvoice in model.SelectedInvoices)
                {
                    List<SelectListItem> list = new List<SelectListItem>();
                    foreach (var invoice in allInvoices)
                    {
                        int invoiceId = invoice.InvoiceID;
                        var invoiceSum = dbCon.tblInvoicePayments.Where(x => x.InvoiceID == invoiceId).Select(l => l.Amount).DefaultIfEmpty(0).Sum();
                        if (invoice.InvoiceID == activeInvoice.InvoiceId)
                        {
                            list.Add(new SelectListItem
                            {

                                Value = invoice.InvoiceID.ToString(),
                                Text = invoice.CompanyInvoiceID + " - (" + (invoice.InvoiceTotal - invoiceSum) + " " + invoice.CurrencySymbol + ")",
                                Selected = true
                            });
                        }
                        else
                        {
                            list.Add(new SelectListItem
                            {
                                Value = invoice.InvoiceID.ToString(),
                                Text = invoice.CompanyInvoiceID + " - (" + (invoice.InvoiceTotal - invoiceSum) + " " + invoice.CurrencySymbol + ")",
                                Selected = false
                            });
                        }
                    }
                    model.AllInvoices.Add(list);
                }

                //Iteration through all the new invoices
                foreach (var invoice in allNewInvoices)
                {
                    int invoiceId = invoice.InvoiceID;
                    var invoiceSum = dbCon.tblInvoicePayments.Where(x => x.InvoiceID == invoiceId).Select(l => l.Amount).DefaultIfEmpty(0).Sum();

                    model.NewInvoices.Add(new SelectListItem
                    {
                        Value = invoice.InvoiceID.ToString(),
                        Text = invoice.CompanyInvoiceID + " - (" + (invoice.InvoiceTotal - invoiceSum) + " " + invoice.CurrencySymbol + ")",
                        Selected = false
                    });
                }

                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCurrentInvoices");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCurrentInvoices");
                }
            }
            return null;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 27th August 2018
        /// This method is used to add or update payment invoice info.
        /// </summary>
        /// <param name="invoiceid"></param>
        /// <param name="amount"></param>
        /// <param name="description"></param>
        /// <param name="paymentId"></param>
        /// <param name="userId"></param>
        /// <returns>bool</returns>
        public bool AddPayment(string invoiceid, string amount, string description, int paymentId, int userId)
        {
            try
            {
                //List<object> finalList = new List<object>();

                //get the invoice, amount and description and split it  with comma
                var invoiceIds = invoiceid.Split(',').ToList();
                var amounts = amount.Split(',').ToList();
                var descriptions = description.Split(',').ToList();

                //for (int i = 0; i < invoiceIds.Count(); i++)
                //{
                //    finalList.Add(new { InvoiceId = invoiceIds[i], Amount = amounts[i], Description = descriptions[i] });
                //}

                //First delete all other payments
                var otherPayments = dbCon.tblInvoicePayments.Where(x => x.PaymentID == paymentId && !invoiceIds.Contains(x.InvoiceID.ToString())).ToList();
                dbCon.tblInvoicePayments.RemoveRange(otherPayments);
                dbCon.SaveChanges();

                for (int i = 0; i < invoiceIds.Count(); i++)
                {
                    dbCon.sp_AddInvoicePayment(Convert.ToInt32(invoiceIds[i]), Convert.ToDecimal(amounts[i]), descriptions[i].ToString() != "" ? descriptions[i] : null, paymentId, userId);
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddPayment");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddPayment");
                }
            }
            return false;
        }

    }
}

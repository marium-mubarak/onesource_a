﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
   public class QuotePresenter
    {
        private readonly globalserveEntities _dbCon;
        public QuotePresenter()
        {
            _dbCon = new globalserveEntities();
          
        }
        public List<sp_quoteAttachement_Result> GetQuoteAttachments(int quoteNo)
        {
            try
            {
                return _dbCon.sp_quoteAttachement(quoteNo).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteAttachments");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteAttachments");
                }
                return null;
            }
           
        }

        public List<sp_quoteComment_R_Result> GetQuoteComments(int quoteNo)
        {
            try
            {
                return _dbCon.sp_quoteComment_R(quoteNo).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteComments");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteComments");
                }
                return null;
            }
           
        }

        public List<sp_quoteDetail_R_Result> GetQuoteDetail(int quoteNo, int customerId)
        {
            try
            {
                return _dbCon.sp_quoteDetail_R(quoteNo, customerId).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteDetail");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteDetail");
                }
                return null;
            }
           
        }

        public List<sp_quoteRequest_R_Result> GetQuoteRequest(int quoteNo)
        {
            try
            {
                var quoteItems = _dbCon.sp_quoteRequest_R(quoteNo).ToList();
                var nonBundleItems = quoteItems.Where(x => x.CatalogStandardID == null).ToList();
                var finalList = new List<sp_quoteRequest_R_Result>();
                var getBundleIds = quoteItems.Where(x => x.CatalogStandardID != null).Select(x => x.CatalogStandardID).Distinct().ToList();

                if (getBundleIds.Any())
                {
                    var count = 1;
                    foreach (var item in getBundleIds)
                    {
                        var getItemsByBundle = quoteItems.Where(x => x.CatalogStandardID == item).ToList();
                        getItemsByBundle.ForEach(x => x.CatalogStandardID = count);
                        finalList.AddRange(getItemsByBundle);
                        count += 1;
                    }
                }

                finalList = finalList.OrderBy(x => x.CatalogStandardID).ToList();
                finalList.AddRange(nonBundleItems);
                return finalList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteRequest");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteRequest");
                }
                return null;
            }
           
        }

        public List<sp_getQuoteResponses_R_Result> GetQuoteResponses(int quoteId, string quotetype)
        {
            try
            {
                List<sp_getQuoteResponses_R_Result> lst = new List<sp_getQuoteResponses_R_Result>();
                if (quotetype == "new")
                {
                    lst = _dbCon.sp_getQuoteResponses_R(quoteId, "new").ToList();
                }
                else
                {
                    lst = _dbCon.sp_getQuoteResponses_R(quoteId, "old").ToList();
                }
                return lst;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteResponses");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteResponses");
                }
                return null;
            }

        }
        public List<sp_quoteDetail_R_Result> GetOldQuoteDetail(int quoteNo, int customerId)
        {
            try
            {
                var query =
                "select sfa.SFAID,sfa.sfaname, con.countryid, con.countryname, cur.currencyname, cur.currencysymbol, pqs.partquotestatuscode as quotestatuscode ," +
                " pqs.partquotestatus quotestatus,null as SupplierReference,isNull(pq.CustomerRef,'') as CustomerReference " +
                " from tblpartquotes pq" +
                " inner join tblsfas sfa on sfa.sfaid=pq.sfaid inner join tblcountries con on con.countryid=pq.countryid" +
                " inner join tblcurrencies cur on cur.currencyid=pq.currencyid" +
                " inner join tblpartquotestatus pqs on pqs.partquotestatusid=pq.partquotestatusid" +
                " where pq.partquoteid=" + quoteNo + " and pq.CustomerID = " + customerId;

                var results = _dbCon.Database.SqlQuery<sp_quoteDetail_R_Result>(query).ToList();
                return results;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOldQuoteDetail");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOldQuoteDetail");
                }
                return null;
            }
            
        }
        public List<sp_quoteAttachement_Result> GetOldQuoteAttachments(int quoteNo)
        {
            try
            {
                var query =
              "select pqa.partquoteid as quoteid,da.daid, da.docid, da.daname, da.daurl, isnull(mt.mimetypeicon, 'blank.gif') as mimetypeicon " +
              " from tbldigitalassets da inner join tblpartquoteattachments pqa on pqa.daid=da.daid and " +
              " pqa.isactive=1 and pqa.partquoteid= " + quoteNo +
              " left outer join tblmimetypes mt on mt.mimetypeid=da.damimetypeid ";

                var results = _dbCon.Database.SqlQuery<sp_quoteAttachement_Result>(query).ToList();
                return results;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOldQuoteAttachments");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOldQuoteAttachments");
                }
                return null;
            }
          
        }

        public List<sp_quoteComment_R_Result> GetOldQuoteComments(int quoteNo)
        {
            try
            {
                var query =
               "SELECT pqt.lastupdated As 'Log_Date', isnull(con.firstname, '') + ' ' + isnull(con.lastname, '') as" +
               " 'Logged_By',pc.parentcompanyname as 'Company', replace(isnull(pqt.comments, ''), char(10),'<br/>') as " +
               " 'Comments' from tblpartquotetracking pqt left outer join tblcontact con on con.userid=pqt.lastupdatedbyuserid" +
               " left outer join tblparentcompanies pc on pc.parentcompanyid=con.companyid where pqt.partquoteid= " +
               quoteNo + " and " +
               " pqt.GSComments is Null order by pqt.lastupdated desc";
                var results = _dbCon.Database.SqlQuery<sp_quoteComment_R_Result>(query).ToList();
                return results;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOldQuoteComments");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOldQuoteComments");
                }
                return null;
            }
           

        }

        public List<sp_quoteRequest_R_Result> GetOldQuoteRequest(int quoteNo)
        {
            try
            {
                var query =
               "select description as [Part_Description], Manufacturer as [Manufacturer], sku as [SKU], " +
               "quantity as [Required_Quantity],CatalogStandardID,PartQuoteID as QuoteID,PartQuoteRequestID as QuoteRequestID,CatalogID from tblpartquoterequests where partquoteid=" + quoteNo;

                var results = _dbCon.Database.SqlQuery<sp_quoteRequest_R_Result>(query).ToList();
                return results;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOldQuoteRequest");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOldQuoteRequest");
                }
                return null;
            }

        }

        public decimal? GetQuoteTotalPrice(int quoteId)
        {
            try
            {
                return _dbCon.sp_getNewQuoteTotalPrice_R(quoteId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteTotalPrice");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteTotalPrice");
                }
                return 0;
            }
         
        }

        public int GetPaymentTermDaysForQuote(int customerId, int countryId, int supplierId)
        {
            try
            {
              

                var query =
                    "SELECT top(1) NetPaymentDays FROM tblSFACustomerWA sfc join tblCustomerAddresses" +
                    " ca on ca.CustomerAddressID=sfc.CustomerDestinationID WHERE SFAID=" + supplierId + " AND " +
                    " sfc.CustomerID=" + customerId + " and  sfc.isActive=1 and offeringid=2 and ca.CountryID=" + countryId;
                var results = _dbCon.Database.SqlQuery<int>(query).FirstOrDefault();

                return results;

            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetPaymentTermDaysForQuote");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPaymentTermDaysForQuote");
                }
                return 0;
            }
        }

        public string GetResellerAddress(int countryId, int supplierId, string customerName)
        {
            try
            {
               
                int partyId = 0;
                var query =
                    " select partyid From tblSFAs sfa join tblParentCompanies pc  on pc.ParentCompanyID=sfa.ParentCompanyID where sfa.SFAID =" + supplierId;
                var results = _dbCon.Database.SqlQuery<int>(query).FirstOrDefault();

                partyId = results;

                if (partyId > 0)
                {
                    var partyAddressType = "";
                    var countrycheck = " and a.CountryID = " + countryId; ;
                    if (customerName.ToLower() == "abbott")
                    {
                        partyAddressType = " and pa.AddressTypeID=7 ";
                        countrycheck = "";
                    }
                    var query1 =
                        "select  pa.addressname+', '+IsNull(a.address1, '-') +', '+IsNull(a.stateprovinceregion, '-') +', '+IsNull(a.postalcode ,'-')+', '+ISNULL( a.City,'-')as Address" +
                        " from tblpartyaddress pa join tblparty p on p.partyid = pa.partyid and " +
                        " pa.partyid = " + partyId + "" + partyAddressType +
                        " join tblAddress a on pa.addressid = a.addressid and a.isActive = 1 " + countrycheck;

                    var results1 = _dbCon.Database.SqlQuery<string>(query1).FirstOrDefault();
                    return results1 ?? "-";
                }
                else
                {
                    return "-";
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetResellerAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetResellerAddress");
                }
                return "-";


            }
        }
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Oct 20 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of customer's categories used for OneStreet"
    /// </summary>
    public class CustomerCategoriesPresenter
    {
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        globalserveEntities _dbCon;
        public CustomerCategoriesPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public List<GenericList> GetManufacturersCatalogSetup(int? customerId)
        {
            var manufacturerId = _dbCon.tblOneStreetCustomerManufacturers.Where(x => x.CustomerID == customerId).Select(x => x.ManufacturerID).ToList();
            var getManfCategorySetupCount = _dbCon.tblOnestreetCatalogMapping_R
                .Where(x => x.CustomerId == customerId).ToList();

            var temp = _dbCon.tblOneStreetManufacturers.Where(x => manufacturerId.Contains(x.ManufacturerID)).ToList();
            var manufacturers = temp.Select(x => new GenericList
            {
                Value = x.ManufacturerName + " (" + (getManfCategorySetupCount.Any(y => y.ManufacturerId == x.ManufacturerID && y.CategoryId == 0) ? "All" : getManfCategorySetupCount.Count(y => y.ManufacturerId == x.ManufacturerID).ToString()) + " mapped)",
                Key = x.ManufacturerID,
                Count = getManfCategorySetupCount.Count(y => y.ManufacturerId == x.ManufacturerID),
            }).OrderByDescending(x => x.Count).ThenBy(x => x.Value).ToList();

            return manufacturers;
        }

        public List<sp_OneStreet_GetCategoryByMfg_R_Result> GetCategories(int? manufacturerId, int? customerId)
        {
            var ret = _dbCon.sp_OneStreet_GetCategoryByMfg_R(manufacturerId, customerId).OrderBy(x => x.TreeClassClassificationName).ToList();
            return ret;
        }

        public int UpdateCustomerCategorySetting(int customerId, int manufacturerId, string categoriesIds, int userId)
        {
            int count = 0;

            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (manufacturerId > 0)
                {
                    if (customerId > 0)
                    {
                        if (categoriesIds.Length > 0)
                        {
                            string[] catArr = categoriesIds.Split(',');
                            long[] catIntArr = new long[catArr.Length];//new manufacturer
                            for (int i = 0; i < catArr.Length; i++)
                            {
                                catIntArr[i] = Convert.ToInt32(catArr[i]);
                            }
                            var allCat = _dbCon.tblOneStreetCustomerCategories.Where(x => x.CustomerID == customerId && x.ManufacturerId == manufacturerId).Select(x => x.CategoryTreeFamilyID).ToArray();//customer's all categories

                            var catItemToDelete = allCat.Where(x => !catIntArr.Contains(x)).ToArray();

                            var products = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == customerId && x.ManufacturerID == manufacturerId && catItemToDelete.Contains(x.CategoryTreeFamilyID)).ToList();
                            _dbCon.tblOneStreetCustomerProducts.RemoveRange(products);
                        }
                        else
                        {
                            var products = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == customerId && x.ManufacturerID == manufacturerId).ToList();
                            _dbCon.tblOneStreetCustomerProducts.RemoveRange(products);
                        }

                        var data = _dbCon.tblOneStreetCustomerCategories.Where(x => x.CustomerID == customerId && x.ManufacturerId == manufacturerId).ToList();
                        _dbCon.tblOneStreetCustomerCategories.RemoveRange(data);
                    }

                    if (categoriesIds.Length > 0)
                    {
                        tblOneStreetCustomerCategory cm;
                        foreach (var item in categoriesIds.Split(','))
                        {
                            cm = new tblOneStreetCustomerCategory();
                            cm.CustomerID = customerId;
                            cm.CategoryTreeFamilyID = Convert.ToInt32(item);
                            cm.Created = DateTime.Now;
                            cm.CreatedBy = userId;
                            cm.ManufacturerId = manufacturerId;
                            _dbCon.tblOneStreetCustomerCategories.Add(cm);
                        }
                    }
                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
                else
                {
                    var manufacturers = GetManufacturers(customerId);
                    foreach (var mfg in manufacturers)
                    {
                        var data = _dbCon.tblOneStreetCustomerCategories.Where(x => x.CustomerID == customerId && x.ManufacturerId == mfg.ManufacturerID).ToList();
                        _dbCon.tblOneStreetCustomerCategories.RemoveRange(data);

                        var categories = GetCategories(Convert.ToInt32(mfg.ManufacturerID), customerId);
                        tblOneStreetCustomerCategory cm;

                        foreach (var item in categoriesIds.Split(','))
                        {

                            //var products = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == customerId && x.ManufacturerID == manufacturerId && catItemToDelete.Contains(x.CategoryTreeFamilyID)).ToList();
                            //_dbCon.tblOneStreetCustomerProducts.RemoveRange(products);

                            var category = categories.Where(x => x.TreeClassClassificationID == Convert.ToInt32(item)).ToList();
                            if (category.Count > 0)
                            {
                                cm = new tblOneStreetCustomerCategory();
                                cm.CustomerID = customerId;
                                cm.CategoryTreeFamilyID = Convert.ToInt32(item);
                                cm.Created = DateTime.Now;
                                cm.CreatedBy = userId;
                                cm.ManufacturerId = Convert.ToInt32(mfg.ManufacturerID);
                                _dbCon.tblOneStreetCustomerCategories.Add(cm);
                            }
                            //else
                            //{
                            //    var catItemToDelete = categories.Where(x => x.TreeClassClassificationID != Convert.ToInt32(item)).Select(x=>x.TreeClassClassificationID).ToList();
                            //    var products = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == customerId && x.ManufacturerID == mfg.ManufacturerID 
                            //    && catItemToDelete.Contains(x.CategoryTreeFamilyID)).ToList();
                            //    _dbCon.tblOneStreetCustomerProducts.RemoveRange(products);
                            //}
                        }
                    }
                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
            }
            return count;
        }


        /// <summary>
        /// get customer's manufacturer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<sp_OneStreet_GetManufacturerByCust_R_Result> GetManufacturers(int customerId)
        {
            return _dbCon.sp_OneStreet_GetManufacturerByCust_R(customerId).OrderBy(x => x.ManufacturerName).ToList();
        }

        //public List<> GetCategories(int? manufacturerId, int? customerId)
        //{
        //    return _dbCon.sp_OneStreet_GetCategoryByMfg_R(manufacturerId, customerId).OrderBy(x => x.TreeClassClassificationName).ToList();
        //}
    }
}

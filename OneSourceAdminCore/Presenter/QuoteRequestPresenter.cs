﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class QuoteRequestPresenter
    {
        private globalserveEntities _dbCon;
        public QuoteRequestPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public void InsertPartQuoteRequest(List<PartQuoteRequestModel> list)
        {

            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                try
                {
                    foreach (var item in list)
                    {
                        var entity = new tblPartQuoteRequest();
                        entity.PartQuoteID = item.PartQuoteID;
                        entity.Manufacturer = item.Manufacturer;
                        entity.SKU = item.SKU;
                        entity.Description = item.Description;
                        entity.Quantity = item.Quantity;
                        entity.IsActive = item.IsActive;
                        entity.LastUpdatedByUserID = item.LastUpdatedByUserID;
                        entity.LastUpdated = item.LastUpdated;
                        _dbCon.tblPartQuoteRequests.Add(entity);
                        _dbCon.SaveChanges();
                    }
                    transaction.Commit();
                }

                catch (Exception)
                {

                    transaction.Rollback();
                }
            }

        }

        /// <summary>
        /// This method is responsible for providing prime contractor email
        /// </summary>
        /// <returns>string</returns>
        public string GetPrimeContractorEmail()
        {
            return
                _dbCon.tblPrimeContractors.FirstOrDefault(
                    x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1).PartQuoteEmail;
        }

        /// <summary>
        /// This method is responsible for providing Customer Info
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns>sp_GetCustomerInfo_R_Result</returns>
        public sp_GetCustomerInfo_R_Result GetCustomerInfo(int quoteId)
        {
            return _dbCon.sp_GetCustomerInfo_R(quoteId).FirstOrDefault();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public sp_GetCustomerInfo_R_Result GetCustomerInfo_OldQuote(int quoteId)
        {
            return _dbCon.sp_GetCustomerInfo_OldQuote_R(quoteId).Select(x => new sp_GetCustomerInfo_R_Result()
            {
                CountryID = x.CountryID,
                CountryName = x.CountryName,
                CreateDate = x.CreateDate,
                CustomerEmail = x.CustomerEmail,
                CustomerID = x.CustomerID,
                CustomerName = x.CustomerName,
                CustomerReference = x.CustomerRef,
                NetpQuoteEmail = x.NetpQuoteEmail,
                SFAID = x.SFAID,
                SFAName = x.SFAName,
                SupplierEmail = x.SupplierEmail,
                UserEmail = x.UserEmail,
                UserName = x.UserName

            }).FirstOrDefault();
        }
        public List<sp_GetSFACustomerContactEmails_R_Result> GetSFACustomerContactEmails(int sfaId, int customerId, int countryId)
        {
            return _dbCon.sp_GetSFACustomerContactEmails_R(customerId, sfaId, countryId).ToList();

        }

    }
}

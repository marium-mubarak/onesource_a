﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class ServiceQuotesService
    {

        private globalserveEntities dbCon;
        public ServiceQuotesService()
        {
            dbCon = new globalserveEntities();
        }



        public List<Sp_GetServicesList_AdminR_Result> GetServicesList()
        {
            try
            {

                var list = dbCon.Sp_GetServicesList_AdminR().ToList();

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetServicesList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetServicesList");
                }
                return new List<Sp_GetServicesList_AdminR_Result>();
            }
        }









    }
}

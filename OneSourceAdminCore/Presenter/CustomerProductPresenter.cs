﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Oct 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of customer's product used for OneStreet"
    /// </summary>
    public class CustomerProductPresenter
    {
        globalserveEntities _dbCon;
        public CustomerProductPresenter() //Constructor
        {
            _dbCon = new globalserveEntities();
        }

        public List<sp_OneStreet_GetCategoryByMfg_AdminR_Result> GetCategories(int manufacturerId, int customerId)
        {
            try
            {
                var result = _dbCon.sp_OneStreet_GetCategoryByMfg_AdminR(customerId, manufacturerId).ToList();
                return result;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCategories");
                if (ex.InnerException != null)
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCategories");
            }
            return null;
        }

        public string CheckSelectCriteria(int customerId, int manufacturerId)
        {
            try
            {
                var ccp = new CustomerCategoriesPresenter();
                var getCustomerManfIds = ccp.GetManufacturers(customerId).Select(x => Convert.ToInt32(x.ManufacturerID)).ToList();

                var chkExisting = _dbCon.tblOnestreetCatalogMapping_R.Where(x => x.CustomerId == customerId && getCustomerManfIds.Contains(x.ManufacturerId ?? 0) && x.CategoryId == 0)
                                                                                  .Select(x => x.ManufacturerId).ToList();

                var chkAllMatching = getCustomerManfIds.All(x => chkExisting.Contains(x));

                if (chkAllMatching == true)
                    return "SelectAll";

                var chkAllForManf = _dbCon.tblOnestreetCatalogMapping_R.Any(x => x.CustomerId == customerId && x.ManufacturerId == manufacturerId && x.CategoryId == 0);
                if (chkAllForManf && manufacturerId > 0)
                    return "SelectAll";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "CheckSelectCriteria");
                if (ex.InnerException != null)
                    StaticHelper.LogException(ex.InnerException.ToString(), "CheckSelectCriteria");
            }
            return "none";
        }

        public int UpdateCustomerProductSetting(int customerId, int manufacturerId, string categoryIds, int UserId, DateTime dateTime)
        {
            int count = 0;
            try
            {
                count = _dbCon.sp_MappingCustomerAllManufacturerAllCategory_AdminR(customerId, manufacturerId, categoryIds, dateTime, UserId);
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerProductSetting");
                if (ex.InnerException != null)
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerProductSetting");
            }
            return count;
        }

        public DataTable GetReportData(int customerId)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();
                using (SqlCommand command = new SqlCommand("sp_OneStreet_GetMappedCategoriesWithManufacturer_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters[0].Value = customerId;
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportData");
                if (ex.InnerException != null)
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportData");
                return null;
            }
        }

        //public int updatecustomerproductmanufacturerwise(int customerid, int manufacturerid, string categoryids)
        //{
        //    try
        //    {
        //        _dbcon.database.commandtimeout = 5000;
        //        return _dbcon.sp_onestreet_insertbulkproduct_r(convert.toint32(manufacturerid).tostring(), categoryids, customerid.tostring());
        //    }
        //    catch (exception ex)
        //    {
        //        statichelper.logexception(ex.tostring(), "checkselectcriteria");
        //        if (ex.innerexception != null)
        //            statichelper.logexception(ex.innerexception.tostring(), "checkselectcriteria");
        //    }
        //    return 0;
        //}

        //public List<sp_OneStreet_GetProductByCustCatMfg_R_Result> GetProducts(int customerId, int mfgId, int categoryId)
        //{
        //    return _dbCon.sp_OneStreet_GetProductByCustCatMfg_R(mfgId, customerId, categoryId).OrderBy(x => x.ManufacturerPartCode).ToList();
        //}

        /// <summary>
        /// update customer's product
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="manufacturerId"></param>
        /// <param name="categoryTreeFamilyID"></param>
        /// <param name="productIds"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        //public int UpdateCustomerProductSetting(int customerId, int manufacturerId, int categoryTreeFamilyID, string productIds, int userId)
        //{
        //    int count = 0;
        //    try
        //    {
        //        //using (var transaction = _dbCon.Database.BeginTransaction())
        //        //{
        //        //    if (customerId > 0)
        //        //    {
        //        //     var data = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == customerId && x.ManufacturerID == manufacturerId && x.CategoryTreeFamilyID == categoryTreeFamilyID).ToList();
        //        //        _dbCon.tblOneStreetCustomerProducts.RemoveRange(data);
        //        //    }

        //        //    if (productIds.Length > 0)
        //        //    {
        //        //        tblOneStreetCustomerProduct cp;
        //        //        foreach (var item in productIds.Split(','))
        //        //        {
        //        //            cp = new tblOneStreetCustomerProduct();
        //        //            cp.CustomerID = customerId;
        //        //            cp.CategoryTreeFamilyID = categoryTreeFamilyID;
        //        //            cp.ProductID = Convert.ToInt32(item.Split(';')[0]);
        //        //            cp.ManufacturerPartCode = item.Split(';')[1];
        //        //            cp.CountryID = 0;
        //        //            cp.IsActive = true;
        //        //            cp.ManufacturerID = manufacturerId;
        //        //            _dbCon.tblOneStreetCustomerProducts.Add(cp);
        //        //        }
        //        //    }
        //        //    count = _dbCon.SaveChanges();
        //        //    transaction.Commit();
        //        //}

        //        //-------------Added by Adnan 14-june-2019------------------sp_AddOneStreetCustomerCatalogMapping_AdminR
        //        //DataTable dt = new DataTable();
        //        //var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
        //        //var builder = new EntityConnectionStringBuilder(efConnectionString);
        //        //var regularConnectionString = builder.ProviderConnectionString;

        //        //SqlConnection con = new SqlConnection(regularConnectionString);
        //        //con.Open();

        //        //using (SqlCommand command = new SqlCommand("sp_UpdateCustomerCategorySetting_AdminR", con))
        //        //{
        //        //    command.CommandType = CommandType.StoredProcedure;

        //        //    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
        //        //    command.Parameters.Add(new SqlParameter("@ManufacturerID", SqlDbType.Int));
        //        //    command.Parameters.Add(new SqlParameter("@CategoriesID", SqlDbType.Int));
        //        //    command.Parameters.Add(new SqlParameter("@ProductID", SqlDbType.Text));
        //        //    command.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.Bit));
        //        //    command.Parameters.Add(new SqlParameter("@CountryID", SqlDbType.Int));


        //        //    command.Parameters[0].Value = customerId;
        //        //    command.Parameters[1].Value = manufacturerId;
        //        //    command.Parameters[2].Value = categoryTreeFamilyID;
        //        //    command.Parameters[3].Value = productIds;
        //        //    command.Parameters[4].Value = true;
        //        //    command.Parameters[5].Value = 0;

        //        //    using (SqlDataAdapter da = new SqlDataAdapter(command))
        //        //    {
        //        //        da.Fill(dt);
        //        //    }

        //        //}
        //        if(customerId > 0 && (manufacturerId == 0 || categoryTreeFamilyID == 0))
        //        {
        //            _dbCon.sp_MappingCustomerManufacturerCategory_WR(manufacturerId, customerId, categoryTreeFamilyID);
        //            count = 1;
        //        }
        //        else if (string.IsNullOrWhiteSpace(productIds))
        //        {
        //            productIds = "0";
        //        }
        //        else if (customerId > 0 && !string.IsNullOrWhiteSpace(productIds) && productIds.Split(',').ToArray().Length > 0)
        //        {
        //            _dbCon.sp_AddOneStreetCustomerCatalogMapping_AdminR(customerId, manufacturerId, categoryTreeFamilyID, productIds, true, 0);
        //            count = 1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StaticHelper.LogException(ex.ToString(), "UpdateCustomerProductSetting");
        //        if (ex.InnerException != null)
        //        {
        //            StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerProductSetting");
        //        }
        //    }
        //    //----------------------------------------------------------
        //    return count;
        //}
    }
}

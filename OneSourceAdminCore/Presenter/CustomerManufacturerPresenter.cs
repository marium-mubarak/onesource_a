﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Oct 20 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of customer's manufacturer used for OneStreet"
    /// </summary>
    public class CustomerManufacturerPresenter
    {
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        globalserveEntities _dbCon;
        public CustomerManufacturerPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// get customer manufacturers
        /// </summary>
        /// <returns></returns>
        public OneStreetCustomerManufacturerModel GetCustomerManufacturers()
        {

            OneStreetCustomerManufacturerModel model = new OneStreetCustomerManufacturerModel();
            CustomerPresenter cp = new CustomerPresenter();
            model.Manufacturers = _dbCon.tblOneStreetManufacturers.Select(x => new GenericList
            {
                Key = x.ManufacturerID,
                Value = x.ManufacturerName
            }).OrderBy(x => x.Value).ToList();
            model.Customers = cp.GetCustomers();
            return model;
        }

        /// <summary>
        /// update the customer's manufacturer record
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="manufacturerIds"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateCustomerManufacturerRecord(string customerId, string manufacturerIds, int userId)
        {
            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                int cId = Convert.ToInt32(customerId);
                if (cId > 0)
                {
                    if (manufacturerIds.Length > 0)
                    {
                    string[] manArr = manufacturerIds.Split(',');
                    long[] manIntArr = new long[manArr.Length];//new manufacturer
                    for (int i = 0; i < manArr.Length; i++)
                    {
                        manIntArr[i] = Convert.ToInt32(manArr[i]);
                    }

                    var allMan = _dbCon.tblOneStreetCustomerManufacturers.Where(x => x.CustomerID == cId).Select(x => x.ManufacturerID).ToArray();//customer's all manufacturer

                    var manItemToDelete = allMan.Where(x=>!manIntArr.Contains(x)).ToArray();
                    
                    var categories = _dbCon.tblOneStreetCustomerCategories.Where(x => x.CustomerID == cId && manItemToDelete.Contains(x.ManufacturerId)).ToList();
                    _dbCon.tblOneStreetCustomerCategories.RemoveRange(categories);

                    var products = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == cId && manItemToDelete.Contains(x.ManufacturerID)).ToList();
                    _dbCon.tblOneStreetCustomerProducts.RemoveRange(products);


                    }
                    else
                    {
                        var categories = _dbCon.tblOneStreetCustomerCategories.Where(x => x.CustomerID == cId ).ToList();
                        _dbCon.tblOneStreetCustomerCategories.RemoveRange(categories);

                        var products = _dbCon.tblOneStreetCustomerProducts.Where(x => x.CustomerID == cId).ToList();
                        _dbCon.tblOneStreetCustomerProducts.RemoveRange(products);
                    }

                    var data = _dbCon.tblOneStreetCustomerManufacturers.Where(x => x.CustomerID == cId).ToList();
                    _dbCon.tblOneStreetCustomerManufacturers.RemoveRange(data);
                }
                if (manufacturerIds.Length > 0)
                {
                    tblOneStreetCustomerManufacturer cm;
                    foreach (var item in manufacturerIds.Split(','))
                    {
                        cm = new tblOneStreetCustomerManufacturer();
                        cm.CustomerID = cId;
                        cm.ManufacturerID = Convert.ToInt32(item);
                        cm.created = DateTime.Now;
                        cm.createdBy = userId;
                        _dbCon.tblOneStreetCustomerManufacturers.Add(cm);
                      
                    }
                }
                count = _dbCon.SaveChanges();
                transaction.Commit();
            }
            return count;

        }
        /// <summary>
        /// get all manufacturers of customer already exists
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<long> GetCustomerManufacturer(int customerId)
        {
            if (customerId > 0)
            {
                var data = _dbCon.tblOneStreetCustomerManufacturers.Where(x => x.CustomerID == customerId).Select(x => x.ManufacturerID).ToList();
                return data;
            }
            return null;
        }
    }
}

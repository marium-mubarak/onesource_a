﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class SMBQuotesPresenter
    {
        private globalserveEntities dbCon;

        HardwareResponsePresenter _hardwareresponse;

        public SMBQuotesPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public List<SelectListItem> GetSMBUsers(string quoteType)
        {
            try
            {
                List<SelectListItem> getUserList = null;
                if (quoteType == "Asset Disposal Quotes")
                {
                    var _users = (from U in dbCon.tblUsers
                                  join C in dbCon.tblContacts on U.UserID equals C.UserID
                                  join a in dbCon.tblAssetDisposalQuotes on U.UserID equals a.AccountManagerId
                                  where U.IsActive == 1 && (C.FirstName != null && C.FirstName != "") || (C.LastName != null && C.LastName != "")
                                  select new { UserName = C.FirstName + " " + C.LastName, UserID = U.UserID, UserGroupID = U.UserGroupID }).Where(x => x.UserGroupID == 20).Distinct().ToList();

                    getUserList = _users.Select(s => new SelectListItem
                    {
                        Text = s.UserName,
                        Value = s.UserID.ToString()
                    }).OrderBy(x => x.Value).ToList();
                }
                else if (quoteType == "Hardware Quotes")
                {
                    var _users = (from U in dbCon.tblUsers
                                  join C in dbCon.tblContacts on U.UserID equals C.UserID
                                  join h in dbCon.tblHardwareQuotes on U.UserID equals h.AccountManagerId
                                  where U.IsActive == 1 && (C.FirstName != null && C.FirstName != "") || (C.LastName != null && C.LastName != "")
                                  select new { UserName = C.FirstName + " " + C.LastName, UserID = U.UserID, UserGroupID = U.UserGroupID }).Where(x => x.UserGroupID == 20).Distinct().ToList();

                    getUserList = _users.Select(s => new SelectListItem
                    {
                        Text = s.UserName,
                        Value = s.UserID.ToString()
                    }).OrderBy(x => x.Value).ToList();
                }
                else if (quoteType == "Software Quotes")
                {
                    var _users = (from U in dbCon.tblUsers
                                  join C in dbCon.tblContacts on U.UserID equals C.UserID
                                  join s in dbCon.tblSoftwareQuotes on U.UserID equals s.AccountManagerId
                                  where U.IsActive == 1 && (C.FirstName != null && C.FirstName != "") || (C.LastName != null && C.LastName != "")
                                  select new { UserName = C.FirstName + " " + C.LastName, UserID = U.UserID, UserGroupID = U.UserGroupID }).Where(x => x.UserGroupID == 20).Distinct().ToList();

                    getUserList = _users.Select(s => new SelectListItem
                    {
                        Text = s.UserName,
                        Value = s.UserID.ToString()
                    }).OrderBy(x => x.Value).ToList();
                }
                else if (quoteType == "IT Service Quotes")
                {
                    var _users = (from U in dbCon.tblUsers
                                  join C in dbCon.tblContacts on U.UserID equals C.UserID
                                  join it in dbCon.tblITServiceQuotes on U.UserID equals it.AccountManagerId
                                  where U.IsActive == 1 && (C.FirstName != null && C.FirstName != "") || (C.LastName != null && C.LastName != "")
                                  select new { UserName = C.FirstName + " " + C.LastName, UserID = U.UserID, UserGroupID = U.UserGroupID }).Where(x => x.UserGroupID == 20).Distinct().ToList();

                    getUserList = _users.Select(s => new SelectListItem
                    {
                        Text = s.UserName,
                        Value = s.UserID.ToString()
                    }).OrderBy(x => x.Value).ToList();
                }

                return getUserList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSMBUsers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSMBUsers");
                }
                return null;
            }
        }

        public List<SelectListItem> GetQuoteIdByAccountManagerandQuoteType(int AccountManagerId, string QuoteType)
        {
            try
            {
                if (QuoteType == "Asset Disposal Quotes")
                {
                    var quotes = dbCon.tblAssetDisposalQuotes.Where(x => x.CraetedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    var quoteIds = new List<SelectListItem>();
                    foreach (var item in quotes)
                    {
                        var responseQuote = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.AssertDisposalId && x.QuoteType == 3);
                        if (responseQuote != null)
                        {
                            var items = dbCon.tblResponseQuoteLineItems.Where(x => x.ResponseQuoteId == responseQuote.ResponseQuoteId && x.QuoteTypeId == 3).ToList();
                            if (items.Count > 0)
                            {
                                var value = responseQuote.ResponseQuoteId.ToString();
                                quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.AssertDisposalId.ToString() });
                            }
                        }
                        
                    }

                    return quoteIds;
                }
                else if (QuoteType == "Hardware Quotes")
                {

                    var quotes = dbCon.tblHardwareQuotes.Where(x => x.CreatedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    var quoteIds = new List<SelectListItem>();
                    foreach (var item in quotes)
                    {
                        var responseQuote = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.HardwareQuoteId && x.QuoteType == 1);
                        if (responseQuote != null)
                        {
                            var items = dbCon.tblResponseQuoteLineItems.Where(x => x.ResponseQuoteId == responseQuote.ResponseQuoteId && x.QuoteTypeId == 1).ToList();
                            if (items.Count > 0)
                            {
                                var value = responseQuote.ResponseQuoteId.ToString();
                                quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.HardwareQuoteId.ToString() });
                            }
                        }

                    }

                    //var quotes = dbCon.tblHardwareQuotes.Where(x => x.CreatedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    //var quoteIds = new List<SelectListItem>();
                    //foreach (var item in quotes)
                    //{
                    //    var value = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.HardwareQuoteId && x.QuoteType == 1) != null ? dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.HardwareQuoteId && x.QuoteType == 1).ResponseQuoteId.ToString() : "Quote # " + item.HardwareQuoteId.ToString();
                    //    quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.HardwareQuoteId.ToString() });
                    //}
                    return quoteIds;
                }
                else if (QuoteType == "Software Quotes")
                {
                    var quotes = dbCon.tblSoftwareQuotes.Where(x => x.CreatedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    var quoteIds = new List<SelectListItem>();
                    foreach (var item in quotes)
                    {
                        var responseQuote = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.SoftwareQuoteId && x.QuoteType == 2);
                        if (responseQuote != null)
                        {
                            var items = dbCon.tblResponseQuoteLineItems.Where(x => x.ResponseQuoteId == responseQuote.ResponseQuoteId && x.QuoteTypeId == 2).ToList();
                            if (items.Count > 0)
                            {
                                var value = responseQuote.ResponseQuoteId.ToString();
                                quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.SoftwareQuoteId.ToString() });
                            }
                        }

                    }
                    //var quotes = dbCon.tblSoftwareQuotes.Where(x => x.CreatedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    //var quoteIds = new List<SelectListItem>();
                    //foreach (var item in quotes)
                    //{
                    //    var value = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.SoftwareQuoteId && x.QuoteType == 2) != null ? dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.SoftwareQuoteId && x.QuoteType == 2).ResponseQuoteId.ToString() : "Quote # " + item.SoftwareQuoteId.ToString();
                    //    quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.SoftwareQuoteId.ToString() });
                    //}

                    return quoteIds;
                }
                else if (QuoteType == "IT Service Quotes")
                {

                    var quotes = dbCon.tblITServiceQuotes.Where(x => x.CreatedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    var quoteIds = new List<SelectListItem>();
                    foreach (var item in quotes)
                    {
                        var responseQuote = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.ISQuoteId && x.QuoteType == 4);
                        if (responseQuote != null)
                        {
                            var items = dbCon.tblResponseQuoteLineItems.Where(x => x.ResponseQuoteId == responseQuote.ResponseQuoteId && x.QuoteTypeId == 4).ToList();
                            if (items.Count > 0)
                            {
                                var value = responseQuote.ResponseQuoteId.ToString();
                                quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.ISQuoteId.ToString() });
                            }
                        }

                    }

                    //var quotes = dbCon.tblITServiceQuotes.Where(x => x.CreatedBy == AccountManagerId || x.CreatedFor == AccountManagerId).ToList();
                    //var quoteIds = new List<SelectListItem>();
                    //foreach (var item in quotes)
                    //{
                    //    var value = dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.ISQuoteId && x.QuoteType == 4) != null ? dbCon.tblResponseQuotes.FirstOrDefault(x => x.QuoteId == item.ISQuoteId && x.QuoteType == 4).ResponseQuoteId.ToString() : "Quote # " + item.ISQuoteId.ToString();
                    //    quoteIds.Add(new SelectListItem { Value = value, Text = "Quote # " + item.ISQuoteId.ToString() });
                    //}

                    return quoteIds;
                }
                return null;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteIdByAccountManagerandQuoteType");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteIdByAccountManagerandQuoteType");
                }
                return null;
            }
        }


        public List<SMBQuoteLineItems> GetQuoteResponseLineItems(int QuoteId, int QuoteTypeId)
        {
            try
            {
                var items = dbCon.tblResponseQuoteLineItems.Where(x => x.ResponseQuoteId == QuoteId && x.QuoteTypeId == QuoteTypeId).ToList();

                List<SMBQuoteLineItems> responseItems = new List<SMBQuoteLineItems>();

                if (QuoteTypeId == 3) //asset
                {
                    foreach (var s in items)
                    {
                        var model = new SMBQuoteLineItems();
                        model.ResponseLineItemId = s.ResponseLineItemId;
                        model.SKU = s.SKU;
                        model.Description = s.Description;
                        model.Category = dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == s.CategoryId).PartCategoryName;
                        model.LeadTime = s.LeadTime != null ? s.LeadTime + " (days)" : "-";
                        model.EOL = s.EOL != null ? Convert.ToDateTime(s.EOL).ToShortDateString() : "-";
                        model.Quantity = s.Quantity.ToString();
                        model.UnitPrice = s.UnitCostPrice.ToString();
                        model.GSPMargin = s.GSPMargin != null ? Convert.ToDouble(s.GSPMargin).ToString("0.#####") : "0";
                        model.GSMargin = (s.UnitCostPrice != null && s.GSMargin != null) ? Convert.ToDouble(((double)s.UnitCostPrice * ((double)s.GSMargin / 100))).ToString("0.#####") : "0";
                        model.FinalPrice = (s.FinalPriceWithoutTax != null && s.ExchangeRate != null) ? Convert.ToDouble((s.FinalPriceWithoutTax * (s.ExchangeRate != null ? (double)s.ExchangeRate : 0))).ToString("0.#####") : "0";
                        model.TaxVat = Convert.ToDouble(s.Tax).ToString("0.#####");
                        model.TotalTaxVat = Convert.ToDouble(s.FinalPriceWithTax).ToString("0.#####");
                        responseItems.Add(model);
                    }

                }
                else if (QuoteTypeId == 1) //hardware
                {
                    foreach (var s in items)
                    {
                        var model = new SMBQuoteLineItems();
                        model.ResponseLineItemId = s.ResponseLineItemId;
                        model.Manufacturer = dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == s.ManufacturerId).ManufacturerName;
                        model.Description = s.Description;
                        model.SKU = s.SKU;
                        model.Category = dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == s.CategoryId).PartCategoryName;
                        model.LeadTime = s.LeadTime != null ? s.LeadTime + " (days)" : "-";
                        model.EOL = s.EOL != null ? Convert.ToDateTime(s.EOL).ToShortDateString() : "-";
                        model.Quantity = s.Quantity.ToString();
                        model.UnitPrice = s.UnitCostPrice.ToString();
                        model.GSPMargin = s.GSPMargin != null ? Convert.ToDouble(s.GSPMargin).ToString("0.#####") : "0";
                        model.GSMargin = (s.UnitCostPrice != null && s.GSMargin != null) ? Convert.ToDouble(((double)s.UnitCostPrice * ((double)s.GSMargin / 100))).ToString("0.#####") : "0";
                        model.FinalPrice = (s.FinalPriceWithoutTax != null && s.ExchangeRate != null) ? Convert.ToDouble((s.FinalPriceWithoutTax * (s.ExchangeRate != null ? (double)s.ExchangeRate : 0))).ToString("0.#####") : "0";
                        model.TaxVat = Convert.ToDouble(s.Tax).ToString("0.#####");
                        model.TotalTaxVat = Convert.ToDouble(s.FinalPriceWithTax).ToString("0.#####");
                        responseItems.Add(model);
                    }
                }
                else if (QuoteTypeId == 4)// IT Service
                {
                    foreach (var s in items)
                    {
                        var model = new SMBQuoteLineItems();
                        model.ResponseLineItemId = s.ResponseLineItemId;
                        model.SKU = s.SKU;
                        model.ServiceType = dbCon.tblServiceTypes.FirstOrDefault(x => x.ServiceTypeId == s.ServiceTypeId)!=null? dbCon.tblServiceTypes.FirstOrDefault(x => x.ServiceTypeId == s.ServiceTypeId).ServiceTypeName:"";
                        model.SLAType = dbCon.tblSLATypes.FirstOrDefault(x => x.SLATypeId == s.SLATypeId)!=null? dbCon.tblSLATypes.FirstOrDefault(x => x.SLATypeId == s.SLATypeId).SLAName:"";
                        model.Resources = s.NoOfResources != null ? s.NoOfResources.ToString() : "0";
                        model.HourlyRate = s.HourlyRate != null ? s.HourlyRate.ToString() : "0";
                        model.TravelCharges = s.TravelCharge != null ? s.TravelCharge.ToString() : "0";
                        model.GSMargin = s.GSMargin != null ? Convert.ToDouble(s.GSMargin).ToString("0.#####") : "-";
                        model.FinalPrice = s.FinalPriceWithoutTax != null ? Convert.ToDouble(s.FinalPriceWithoutTax).ToString("0.#####") : "-";
                        model.TaxVat = s.Tax != null ? Convert.ToDouble(s.Tax).ToString("0.#####") : "0";
                        model.TotalTaxVat = s.FinalPriceWithTax != null ? Convert.ToDouble(s.FinalPriceWithTax).ToString("0.#####") : "0";
                        responseItems.Add(model);
                    }
                }
                else if (QuoteTypeId == 2)// Software
                {
                    foreach (var s in items)
                    {
                        var model = new SMBQuoteLineItems();
                        model.ResponseLineItemId = s.ResponseLineItemId;
                        model.SKU = s.SKU;
                        model.Manufacturer = dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == s.ManufacturerId).ManufacturerName;
                        model.Description = s.Description;
                        model.Category = dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == s.CategoryId).PartCategoryName;
                        model.LeadTime = s.LeadTime != null ? s.LeadTime + " (days)" : "-";
                        model.EOL = s.EOL != null ? Convert.ToDateTime(s.EOL).ToShortDateString() : "-";
                        model.Quantity = s.Quantity != null ? s.Quantity.ToString() : "0";
                        model.UnitPrice = s.UnitCostPrice != null ? s.UnitCostPrice.ToString() : "0";
                        model.GSPMargin = s.GSPMargin != null ? Convert.ToDouble(s.GSPMargin).ToString("0.#####") : "0";
                        model.GSMargin = (s.UnitCostPrice != null && s.GSMargin != null) ? Convert.ToDouble(((double)s.UnitCostPrice * ((double)s.GSMargin / 100))).ToString("0.#####") : "0";
                        model.FinalPrice = (s.FinalPriceWithoutTax != null && s.ExchangeRate != null) ? Convert.ToDouble((s.FinalPriceWithoutTax * (s.ExchangeRate != null ? (double)s.ExchangeRate : 0))).ToString("0.#####") : "0";
                        model.TaxVat = Convert.ToDouble(s.Tax).ToString("0.#####");
                        model.TotalTaxVat = Convert.ToDouble(s.FinalPriceWithTax).ToString("0.#####");
                        responseItems.Add(model);
                    }
                }

                return responseItems;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteResponseLineItems");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteResponseLineItems");
                }
                return null;
            }
        }

        public int DeleteResponseLineItems(int[] responseLineItemIds)
        {
            int count = 0;
            try
            {
                using (var transaction = dbCon.Database.BeginTransaction())
                {
                    var lineItems =
                        dbCon.tblResponseQuoteLineItems.Where(x => responseLineItemIds.Contains(x.ResponseLineItemId)).ToList();
                    if (lineItems.Count > 0)
                    {
                        dbCon.tblResponseQuoteLineItems.RemoveRange(lineItems);
                        count = dbCon.SaveChanges();
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "DeleteResponseLineItems");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteResponseLineItems");
                }
                return count;
            }

            return count;
        }
    }
}

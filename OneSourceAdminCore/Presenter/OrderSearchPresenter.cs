﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This presenter Class is responsible for providing order tracking detail"
    /// </summary>
    public class OrderSearchPresenter
    {
        private readonly globalserveEntities _dbCon;
        public OrderSearchPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// provide order search model
        /// </summary>
        /// <param name="userGroupName"></param>
        /// <param name="countryId"></param>
        /// <param name="customerId"></param>
        /// <param name="SFAID"></param>
        /// <returns></returns>
        public OrderSearchModel GetOrderSearchModel(string userGroupName, int countryId, int customerId, string SFAID)
        {
            try
            {
                OrderSearchModel obj = new OrderSearchModel();
                obj.Categories = _dbCon.sp_getCategoryOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID).ToList();
                obj.Categories.Insert(0, new Entity.sp_getCategoryOrderSearch_AdminR_Result { partcategoryid = 0, partcategoryname = "(None)" });

                obj.Countries = _dbCon.sp_getCountryOrderSearch_AdminR(userGroupName, customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID).ToList();
                obj.Countries.Insert(0, new Entity.sp_getCountryOrderSearch_AdminR_Result { orderdestinationcountryid = 0, countryname = "(None)" });

                obj.Customers = _dbCon.sp_getCustomerOrderSearch_AdminR(userGroupName, SFAID).ToList();
                obj.Customers.Insert(0, new Entity.sp_getCustomerOrderSearch_AdminR_Result { customerid = 0, customername = "(None)" });

                var tempStatus = _dbCon.sp_GetOrderStatuses_AdminR().ToList();
                obj.OrderStatuses = new List<Entity.sp_GetOrderStatuses_AdminR_Result>();
                obj.OrderStatuses.Insert(0, tempStatus.FirstOrDefault(x => x.OrderStatusId == "1,2,3"));
                obj.OrderStatuses.Insert(1, tempStatus.FirstOrDefault(x => x.OrderStatusId == "4"));
                obj.OrderStatuses.Insert(2, tempStatus.FirstOrDefault(x => x.OrderStatusId == "9"));
                obj.OrderStatuses.Insert(3, tempStatus.FirstOrDefault(x => x.OrderStatusId == "8"));
                obj.OrderStatuses.Insert(4, tempStatus.FirstOrDefault(x => x.OrderStatusId == "7"));
                obj.OrderStatuses.Insert(5, tempStatus.FirstOrDefault(x => x.OrderStatusId == "6"));
                obj.OrderStatuses.Insert(6, tempStatus.FirstOrDefault(x => x.OrderStatusId == "13"));


                obj.Manufacturers = _dbCon.sp_getManufacturerOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID).ToList();
                obj.Manufacturers.Insert(0, new Entity.sp_getManufacturerOrderSearch_AdminR_Result { manufacturerid = 0, manufacturername = "(None)" });

                if (userGroupName == "ISD")
                {
                    obj.DeliveryAddresses = _dbCon.sp_getDeliveryAddressOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId)).ToList();
                    obj.DeliveryAddresses.Insert(0, new Entity.sp_getDeliveryAddressOrderSearch_AdminR_Result { customerdestinationid = 0, condensedaddress = "(None)" });

                    obj.Suppliers = _dbCon.sp_getSupplierOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId)).ToList();
                    obj.Suppliers.Insert(0, new Entity.sp_getSupplierOrderSearch_AdminR_Result { sfaid = 0, sfaname = "(None)" });

                    obj.Users = _dbCon.sp_getUserOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId)).ToList();
                    obj.Users.Insert(0, new Entity.sp_getUserOrderSearch_AdminR_Result { userid = 0, username = "(None)" });
                }


                if (!string.IsNullOrEmpty(SFAID))
                {
                    //if Group is SFA then SFAID wont be null
                    obj.LstParts = _dbCon.sp_getPartsOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID).ToList();
                    obj.LstBundles = _dbCon.sp_getBundlesOrderSearch_AdminR(userGroupName, countryId == 0 ? string.Empty : Convert.ToString(countryId), customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID).ToList();
                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => GetOrderSearchModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => GetOrderSearchModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrderSearchModel");
                }
                return new OrderSearchModel();
            }
        }

        /// <summary>
        /// provide searched result for ISD
        /// </summary>
        /// <param name="model"></param>
        /// <param name="dateFormatId"></param>
        /// <param name="primeContractorId"></param>
        /// <param name="requiredRecord"></param>
        /// <returns></returns>
        public OrderListModel GetSearchedOrders_ISD(OrderSearchModel model, string dateFormatId, string primeContractorId, int requiredRecord)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                OrderListModel oslmodel = new Model.OrderListModel();
                ObjectParameter totalCount = new ObjectParameter("TotalCount", typeof(Int32));

                List<sp_getOrderSearch_AdminR_Result> obj = _dbCon.sp_getOrderSearch_AdminR(Convert.ToInt32(dateFormatId), primeContractorId, model.CustomerID == 0 ? "" : model.CustomerID.ToString(),
                   model.SessionTrackingNumber == 0 ? "" : model.SessionTrackingNumber.ToString(), model.OrderTrackingNumber == 0 ? "" : model.OrderTrackingNumber.ToString(),
                   model.DestinationTrackingNumber == 0 ? "" : model.DestinationTrackingNumber.ToString(), model.OrderPO, model.DestPO, model.Statuses,
                   model.CountryId == 0 ? "" : model.CountryId.ToString(), model.AddressId == 0 ? "" : model.AddressId.ToString(), model.OrderedByUserID == 0 ? "" : model.OrderedByUserID.ToString(),
                   model.SKU, model.SupplierId == 0 ? "" : model.SupplierId.ToString(), model.ManufacturerId == 0 ? "" : model.ManufacturerId.ToString(), model.PartCategoryID == 0 ? "" : model.PartCategoryID.ToString()
                    , model.OrderStartDate, model.OrderEndDate, model.ShipStartDate, model.ShipEndDate, requiredRecord.ToString(), totalCount).ToList();

                oslmodel.ISDOrderSearchlst = obj;
                if (obj.Count == 0)
                {
                    oslmodel.TotalRecord = 0;
                }
                else { oslmodel.TotalRecord = Convert.ToInt32(totalCount.Value); }
                return oslmodel;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>GetSearchedOrders_ISD<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>GetSearchedOrders_ISD");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSearchedOrders_ISD");
                }
                return new OrderListModel();
            }
        }

        /// <summary>
        /// provide searched result of SFA
        /// </summary>
        /// <param name="model"></param>
        /// <param name="dateFormatId"></param>
        /// <param name="requiredRecord"></param>
        /// <returns></returns>
        public OrderListModel GetSearchedOrders_SFA(OrderSearchModel model, string dateFormatId, string sfaId, int requiredRecord)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                OrderListModel oslmodel = new Model.OrderListModel();
                ObjectParameter totalCount = new ObjectParameter("TotalCount", typeof(Int32));

                List<sp_getOrderSearch_SFA_AdminR_Result> obj = _dbCon.sp_getOrderSearch_SFA_AdminR(Convert.ToInt32(dateFormatId), model.CustomerID == 0 ? "" : model.CustomerID.ToString(),
                   model.SessionTrackingNumber == 0 ? "" : model.SessionTrackingNumber.ToString(), model.OrderTrackingNumber == 0 ? "" : model.OrderTrackingNumber.ToString(),
                   model.DestinationTrackingNumber == 0 ? "" : model.DestinationTrackingNumber.ToString(), model.OrderPO, model.DestPO, model.Statuses,
                   model.CountryId == 0 ? "" : model.CountryId.ToString(), model.Parts, model.Bundles, model.AddressId == 0 ? "" : model.AddressId.ToString(), model.OrderedByUserID == 0 ? "" : model.OrderedByUserID.ToString(),
                   model.SKU, model.SupplierId == 0 ? "" : model.SupplierId.ToString(), model.ManufacturerId == 0 ? "" : model.ManufacturerId.ToString(), model.PartCategoryID == 0 ? "" : model.PartCategoryID.ToString()
                    , model.OrderStartDate, model.OrderEndDate, model.ShipStartDate, model.ShipEndDate, sfaId, requiredRecord.ToString(), totalCount).ToList();


                oslmodel.SFAOrderSearchlst = obj;
                if (obj.Count == 0)
                {
                    oslmodel.TotalRecord = 0;
                }
                else { oslmodel.TotalRecord = Convert.ToInt32(totalCount.Value); }
                return oslmodel;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>GetSearchedOrders_SFA<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>GetSearchedOrders_SFA");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSearchedOrders_SFA");
                }
                return new OrderListModel();
            }
        }

        /// <summary>
        /// provide order status list
        /// </summary>
        /// <param name="OrderStatusID"></param>
        /// <returns></returns>
        public List<GenericList> GetOrderStatusList(int OrderStatusID)
        {
            List<int> OrderStatusList = new List<int>();
            switch (OrderStatusID)
            {
                case 1:
                    OrderStatusList = new List<int> { 1, 2, 9, 13 };
                    break;
                case 2:
                    OrderStatusList = new List<int> { 1, 2, 3, 5, 9, 13 };
                    break;
                case 3:
                    OrderStatusList = new List<int> { 3, 9, 13 };
                    break;
                case 4:
                    OrderStatusList = new List<int> { 4, 5, 9, 13 };
                    break;
                case 5:
                    OrderStatusList = new List<int> { 5, 9, 13 };
                    break;
                case 6:
                    OrderStatusList = new List<int> { 6, 13 };
                    break;
                case 7:
                    OrderStatusList = new List<int> { 7, 9, 13 };
                    break;
                case 8:
                    OrderStatusList = new List<int> { 8, 9, 13 };
                    break;
                case 9:
                    OrderStatusList = new List<int> { 6, 7, 8, 9, 13 };
                    break;

            }

            return _dbCon.tblOrderStatus.Where(x => x.IsActive == true && OrderStatusList.Contains(x.OrderStatusID)).Select(x => new GenericList
            {
                Key = x.OrderStatusID,
                Value = x.OrderStatusName
            }).OrderBy(x => x.Value).ToList();
        }

        /// <summary>
        /// provide order detail
        /// </summary>
        /// <param name="strTrackingNumber"></param>
        /// <param name="userGroupName"></param>
        /// <param name="SFAID"></param>
        /// <returns></returns>
        public OrderDetailModel GetOrderDetail(string strTrackingNumber, string userGroupName, int SFAID)
        {
            try
            {
                int orderTrackingNumber, destinationTrackingNumber, sessionTrackingNumber;
                sessionTrackingNumber = Convert.ToInt32(strTrackingNumber.Split('-')[0]);
                orderTrackingNumber = Convert.ToInt32(strTrackingNumber.Split('-')[1]);
                destinationTrackingNumber = Convert.ToInt32(strTrackingNumber.Split('-')[2]);

                OrderDetailModel obj = new Model.OrderDetailModel();

                //if (userGroupName == "ISD")
                //{
                var orderDetailData = _dbCon.sp_getOrderDetail_AdminR(sessionTrackingNumber, orderTrackingNumber, destinationTrackingNumber).ToList();
                var totalAmount = 0.0M;
                foreach (var item in orderDetailData)
                {
                    totalAmount = totalAmount + Convert.ToDecimal(item.Total);
                }
                obj.OrderDetail = orderDetailData.FirstOrDefault();
                obj.OrderDetail.Total = totalAmount;

                if (userGroupName == "SFA")
                {
                    if (obj.OrderDetail.SFAID != SFAID)
                    {
                        return null;
                    }
                }

                if (orderDetailData != null && orderDetailData.FirstOrDefault().CustomerName == "EIPC(subway)")//by Adnan 27-june-2019
                {
                    var UserID = orderDetailData.FirstOrDefault().UserID;
                    var GSbuyerInforDetail = _dbCon.tblGSBuyerInfoes.Where(x => x.UserID == UserID).FirstOrDefault();
                    if (GSbuyerInforDetail != null)
                    {
                        obj.VAT = string.IsNullOrEmpty(GSbuyerInforDetail.VATNumberofFranchisee) ? "-" : GSbuyerInforDetail.VATNumberofFranchisee;
                        obj.Email = string.IsNullOrEmpty(GSbuyerInforDetail.BuyerEmail) ? "-" : GSbuyerInforDetail.BuyerEmail;
                        obj.TelephoneNumber = string.IsNullOrEmpty(GSbuyerInforDetail.BuyerPhone) ? "-" : GSbuyerInforDetail.BuyerPhone;
                        obj.CorporateName = string.IsNullOrEmpty(GSbuyerInforDetail.CorpNameofCompany) ? "-" : GSbuyerInforDetail.CorpNameofCompany;
                    }
                }

                if (obj.OrderDetail != null)
                {
                    obj.OrderStatusList = GetOrderStatusList(obj.OrderDetail.OrderStatusID);
                    obj.SessionTrackingNumber = obj.OrderDetail.SessionTrackingNumber;
                    obj.OrderTrackingNumber = obj.OrderDetail.OrderTrackingNumber;
                    obj.DestinationTrackingNumber = obj.OrderDetail.DestinationTrackingNumber;
                    obj.DateAcceptedBySupplier = obj.OrderDetail.DateAcceptedBySupplier;

                }
                else
                {
                    obj.OrderStatusList = new List<Model.GenericList>();
                    obj.OrderDetail = new Entity.sp_getOrderDetail_AdminR_Result();
                }
                obj.ShippingMethod = _dbCon.sp_GetShippingMethods_R(sessionTrackingNumber, orderTrackingNumber).FirstOrDefault() ?? "";

                var OrderItems = _dbCon.sp_GetOrderItem_R(sessionTrackingNumber).ToList();

                var QuoteId = 0;
                var smbQuoteId = 0;
                if (OrderItems.ToList().Count > 0 && OrderItems.Any(x => x.QuoteResponseId > 0))
                {
                    var quoteResponseId = OrderItems.FirstOrDefault(x => x.QuoteResponseId > 0).QuoteResponseId;
                    var getQuoteId =
                        _dbCon.tblQuoteResponses.FirstOrDefault(x => x.QuoteResponseID == quoteResponseId);

                    if (getQuoteId != null) QuoteId = getQuoteId.QuoteID;
                }
                if (OrderItems.ToList().Count > 0 && OrderItems.Any(x => x.SmbQuoteLineItemId > 0))
                {
                    var responseLineItemId = OrderItems.FirstOrDefault(x => x.SmbQuoteLineItemId > 0).SmbQuoteLineItemId;

                    var getResponseDetails =
                         _dbCon.tblResponseQuoteLineItems.FirstOrDefault(x => x.ResponseLineItemId == responseLineItemId);
                    if (getResponseDetails != null)
                    {
                        var getQuote =
                           _dbCon.tblResponseQuotes.FirstOrDefault(
                                x => x.ResponseQuoteId == getResponseDetails.ResponseQuoteId);
                        if (getQuote != null)
                        {
                            smbQuoteId = Convert.ToInt32(getQuote.QuoteId);
                        }
                    }
                }
                obj.QuoteId = QuoteId;
                obj.SMBQuoteId = smbQuoteId;

                var orderData =
                    _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == sessionTrackingNumber);
                if (orderData != null && !string.IsNullOrWhiteSpace(orderData.BasketName))
                {
                    obj.BasketName = orderData.BasketName;
                }
                // }
                //else if (userGroupName == "SFA")
                //{
                //    obj.OrderDetailSFA = _dbCon.sp_getOrderDetail_SFA_AdminR(sessionTrackingNumber, orderTrackingNumber, destinationTrackingNumber, SFAID).FirstOrDefault();
                //    if (obj.OrderDetailSFA != null)
                //    {
                //        obj.SessionTrackingNumber = obj.OrderDetailSFA.SessionTrackingNumber;
                //        obj.OrderTrackingNumber = obj.OrderDetailSFA.OrderTrackingNumber;
                //        obj.DestinationTrackingNumber = obj.OrderDetailSFA.DestinationTrackingNumber;
                //        obj.DateAcceptedBySupplier = obj.OrderDetailSFA.DateAcceptedBySupplier;
                //    }
                //    else
                //    {
                //        obj.OrderDetailSFA = new sp_getOrderDetail_SFA_AdminR_Result();
                //    }
                //    obj.ShippingMethod = _dbCon.sp_GetShippingMethods_R(sessionTrackingNumber, orderTrackingNumber).FirstOrDefault() ?? "";
                //}

                var forCreateBy = (from o in _dbCon.tblOrders
                                   join
                                     u in _dbCon.tblUsers on o.OrderCreatedByUserID equals u.UserID
                                   where o.SessionTrackingNumber == sessionTrackingNumber && o.OrderTrackingNumber == orderTrackingNumber
                                   select new { u.UserName }).FirstOrDefault();
                if (forCreateBy != null)
                {
                    obj.CreatedBy = forCreateBy.UserName ?? string.Empty;
                }


                //QL = "SELECT ds.DeliveryStatusCode FROM tblOrderDestinations od, tblDeliveryStatus ds" & _
                //    " WHERE od.SessionTrackingNumber=" & SessionTNumber & _
                //    " AND od.OrderTrackingNumber=" & OrderTNumber & _
                //    " AND od.DestinationTrackingNumber=" & DestinationTNumber & _
                //    " AND ds.DeliveryStatusID=od.DeliveryStatusID"

                var statusCode = (from od in _dbCon.tblOrderDestinations
                                  join ds in _dbCon.tblDeliveryStatus on od.DeliveryStatusID equals ds.DeliveryStatusID
                                  where od.SessionTrackingNumber == sessionTrackingNumber &&
                                  od.OrderTrackingNumber == orderTrackingNumber && od.DestinationTrackingNumber == destinationTrackingNumber
                                  select new { ds.DeliveryStatusCode }).FirstOrDefault();
                if (statusCode != null)
                {
                    obj.StatusCode = statusCode.DeliveryStatusCode;
                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>GetOrderDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>GetOrderDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrderDetail");
                }
                return new OrderDetailModel();
            }
        }

        /// <summary>
        /// update order status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string UpdateOrderStatus(OrderStatusModel model, int userId)
        {
            try
            {

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblOrder order =
                       _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber && x.OrderTrackingNumber == model.OrderTrackingNumber);
                    if (order != null)
                    {
                        order.OrderStatusID = model.OrderStatusId;
                        order.OrderStatusUpdatedByUserID = userId;
                        order.OrderStatusLastUpdated = DateTime.Now;

                        var status = _dbCon.tblOrderStatus.FirstOrDefault(x => x.OrderStatusID == model.OrderStatusId); // Cancelled;
                        if (status != null && status.OrderStatusName == "Cancelled")
                        {
                            order.CanceledBy = userId;
                            order.CanceledDate = DateTime.Now;
                        }
                    }
                    _dbCon.Entry(order).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                    transaction.Commit();

                }
                CheckOrderDeliveryStatus(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber, userId);
                UpdateOrderStatus(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber);
                SendOrderUpdateNotification(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>UpdateOrderStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>UpdateOrderStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateOrderStatus");
                }
            }
            return model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber;
        }

        public string GetOrderStatusCode(int orderStatusId)
        {
            var data = _dbCon.tblOrderStatus.FirstOrDefault(x => x.OrderStatusID == orderStatusId);
            if (data != null)
            {
                return data.OrderStatusCode;
            }
            return string.Empty;
        }

        /// <summary>
        /// check order delivery status
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="orderTrackingNumber"></param>
        /// <param name="destinationTrackingNumber"></param>
        /// <param name="userId"></param>
        public void CheckOrderDeliveryStatus(int sessionTrackingNumber, int orderTrackingNumber, int destinationTrackingNumber, int userId)
        {
            try
            {
                int dStatus = 0;
                bool isNew = true;
                bool isInProgress = true;
                bool isShipped = true;
                bool isDelivered = true;
                int nStatus = -1;

                var data = _dbCon.sp_getCurrentOrderDeliveryStatus_AdminR(sessionTrackingNumber, orderTrackingNumber, destinationTrackingNumber).ToList();
                if (data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (item.DeliveryStatusID == null)
                        {
                            dStatus = -1;
                        }
                        else
                        {
                            dStatus = Convert.ToInt32(item.DeliveryStatusID);
                        }
                    }
                }
                else
                {
                    return;
                }
                if (dStatus == 3)
                {
                    return;
                }

                foreach (var item in data)
                {
                    if (item.oddDateShipped != null || item.datedelivered != null && item.dateacceptedBySupplier != null)
                    {
                        isNew = false;
                    }
                    if (item.oddDateShipped == null && item.datedelivered == null && item.dateacceptedBySupplier == null)
                    {
                        isInProgress = false;
                    }
                    if (item.oddDateShipped == null)
                    {
                        isShipped = false;
                    }
                    if (item.deliveredquantity != null)
                    {
                        if (item.deliveredquantity != item.quantity)
                        {
                            isShipped = false;
                        }
                    }
                    if (item.datedelivered == null)
                    {
                        isDelivered = false;
                    }
                }

                if (isDelivered)
                {
                    nStatus = 3;
                }
                else if (isShipped)
                {
                    nStatus = 4;
                }
                else if (isInProgress)
                {
                    nStatus = 2;
                }
                else if (isNew)
                {
                    nStatus = 1;
                }
                else if (dStatus == -1)
                {
                    nStatus = 1;
                }

                if (nStatus > 0)
                {
                    using (var transaction = _dbCon.Database.BeginTransaction())
                    {
                        tblOrderDestinations orderDes =
                            _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == sessionTrackingNumber && x.OrderTrackingNumber == orderTrackingNumber && x.DestinationTrackingNumber == destinationTrackingNumber);
                        if (orderDes != null)
                        {
                            orderDes.DeliveryStatusID = nStatus;
                            orderDes.DeliveryStatusLastUpdatedByUserID = userId;
                            orderDes.DeliveryStatusLastUpdated = DateTime.Now;

                            _dbCon.Entry(orderDes).State = System.Data.Entity.EntityState.Modified;
                            _dbCon.SaveChanges();
                            transaction.Commit();
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>CheckOrderDeliveryStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>CheckOrderDeliveryStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "CheckOrderDeliveryStatus");
                }
            }

        }

        /// <summary>
        /// update order status
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="orderTrackingNumber"></param>
        /// <param name="destinationTrackingNumber"></param>
        public void UpdateOrderStatus(int sessionTrackingNumber, int orderTrackingNumber, int destinationTrackingNumber)
        {
            try
            {
                _dbCon.sp_UpdateOrderStatus_AdminR(sessionTrackingNumber, orderTrackingNumber, destinationTrackingNumber);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>UpdateOrderStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>UpdateOrderStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateOrderStatus");
                }
            }
        }

        /// <summary>
        /// change order status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object ChangeOrderStatus(OrderStatusModel model, int userId)
        {
            string message = string.Empty;
            try
            {

                string statusName = string.Empty, fromStatus = string.Empty;
                int OrderStatusID = 0;
                if (model.Ostatus == "N")
                {
                    statusName = "Released By Customer";
                }
                else if (model.Ostatus == "A")
                {
                    statusName = "Accepted By Supplier";
                }
                if (!string.IsNullOrEmpty(statusName))
                {
                    var OrderStatus = _dbCon.tblOrderStatus.FirstOrDefault(x => x.OrderStatusName == statusName);
                    if (OrderStatus != null)
                    {
                        OrderStatusID = OrderStatus.OrderStatusID;
                    }
                    var order = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber);
                    OrderStatus = _dbCon.tblOrderStatus.FirstOrDefault(x => x.OrderStatusID == order.OrderStatusID);
                    if (OrderStatus != null)
                    {
                        fromStatus = OrderStatus.OrderStatusName;
                    }

                    if (OrderStatusID > 0)
                    {
                        if (statusName == "Released By Customer")
                        {
                            var orderItem = _dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == model.SessionTrackingNumber).Select(x => x.OrderItemId).ToList();

                            //
                            var tblorderdeliverydetails = _dbCon.tblOrderDeliveryDetails.Where(x => orderItem.Contains(x.OrderDeliveryItemID)).ToList();
                            if (tblorderdeliverydetails != null)
                            {
                                _dbCon.tblOrderDeliveryDetails.RemoveRange(tblorderdeliverydetails);
                                _dbCon.SaveChanges();
                            }
                            //
                            tblOrderDestinations orderDes =
                            _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber);
                            if (orderDes != null)
                            {
                                orderDes.DateAcceptedbySupplier = null;
                                orderDes.ETA = null;
                                orderDes.DateShipped = null;
                                _dbCon.Entry(orderDes).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }

                            //

                            var orderItemToUpdate = _dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == model.SessionTrackingNumber).ToList();
                            foreach (var item in orderItemToUpdate)
                            {
                                item.ETA = null;
                                _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                            //

                            var deliveryStatus = _dbCon.tblDeliveryStatus.FirstOrDefault(x => x.DeliverystatusName == "New");
                            int deliveryStatusId = 0;
                            if (deliveryStatus != null)
                            {
                                deliveryStatusId = deliveryStatus.DeliveryStatusID;
                            }

                            if (deliveryStatusId > 0)
                            {
                                foreach (var item in orderItemToUpdate)
                                {
                                    item.DeliveryStatusID = deliveryStatusId;
                                    _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                                    _dbCon.SaveChanges();
                                }
                                if (orderDes != null)
                                {
                                    orderDes.DeliveryStatusID = deliveryStatusId;
                                    _dbCon.Entry(orderDes).State = System.Data.Entity.EntityState.Modified;
                                    _dbCon.SaveChanges();
                                }
                            }

                        }
                        else if (statusName == "Accepted By Supplier")
                        {
                            var orderItem = _dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == model.SessionTrackingNumber).Select(x => x.OrderItemId).ToList();

                            //
                            var tblorderdeliverydetails = _dbCon.tblOrderDeliveryDetails.Where(x => orderItem.Contains(x.OrderDeliveryItemID)).ToList();
                            if (tblorderdeliverydetails != null)
                            {
                                _dbCon.tblOrderDeliveryDetails.RemoveRange(tblorderdeliverydetails);
                                _dbCon.SaveChanges();
                            }
                            //
                            tblOrderDestinations orderDes =
                            _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber);
                            if (orderDes != null)
                            {
                                orderDes.DateShipped = null;
                                _dbCon.Entry(orderDes).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }

                            //

                            var deliveryStatus = _dbCon.tblDeliveryStatus.FirstOrDefault(x => x.DeliverystatusName == "In Progress");
                            int deliveryStatusId = 0;
                            if (deliveryStatus != null)
                            {
                                deliveryStatusId = deliveryStatus.DeliveryStatusID;
                            }

                            if (deliveryStatusId > 0)
                            {

                                var orderItemToUpdate = _dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == model.SessionTrackingNumber).ToList();

                                foreach (var item in orderItemToUpdate)
                                {
                                    item.DeliveryStatusID = deliveryStatusId;
                                    _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                                    _dbCon.SaveChanges();
                                }
                                if (orderDes != null)
                                {
                                    orderDes.DeliveryStatusID = deliveryStatusId;
                                    _dbCon.Entry(orderDes).State = System.Data.Entity.EntityState.Modified;
                                    _dbCon.SaveChanges();
                                }
                            }
                        }

                        if (order != null)
                        {
                            order.OrderStatusID = OrderStatusID;
                            _dbCon.Entry(order).State = System.Data.Entity.EntityState.Modified;
                            _dbCon.SaveChanges();
                        }

                        tblOrderTracking otracking = new tblOrderTracking();
                        otracking.SessionTrackingNumber = model.SessionTrackingNumber;
                        otracking.OrderTrackingNumber = Convert.ToInt16(model.OrderTrackingNumber);
                        otracking.DestinationTrackingNumber = Convert.ToInt16(model.DestinationTrackingNumber);
                        otracking.OrderStatusID = OrderStatusID;
                        otracking.OrderTrackingTypeID = 1;
                        otracking.OrderActionID = 1;
                        otracking.Comment = model.Reason.Trim();
                        otracking.IsActive = true;
                        otracking.LastUpdatedByUserID = userId;
                        otracking.LastUpdated = DateTime.Now;
                        otracking.QuoteActionName = null;

                        _dbCon.tblOrderTracking.Add(otracking);
                        _dbCon.SaveChanges();


                        SendOrderStatusOverrideNotification(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber, fromStatus, statusName, model.Reason);

                        var orderupdate = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber);
                        if (orderupdate != null)
                        {
                            if (orderupdate.OrderStatusID == OrderStatusID)
                            {
                                message = "Order Status is changed to " + statusName;
                            }
                        }

                    }

                }
            }

            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>ChangeOrderStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>ChangeOrderStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ChangeOrderStatus");
                }
            }
            return new { strTrackingNo = model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber, message = message };
        }


        /// <summary>
        /// Aurangzeb Khan
        /// Send order status update notification
        /// 3rd September 2018
        /// </summary>
        /// <param name="StrackNo"></param>
        /// <param name="OtrackNo"></param>
        /// <param name="ODestNo"></param>
        public void SendOrderUpdateNotification(int StrackNo, int OtrackNo, int ODestNo)
        {
            try
            {
                string sessionTrackingNumber = StrackNo + "-" + OtrackNo + "-" + ODestNo;
                string OrderTNumber = OtrackNo.ToString();
                string DestinationTNumber = ODestNo.ToString();

                string MissingEmail = null;
                string OrderedBy = null;
                string CreatedBy = null;
                string OrderStatusID = null;
                string OrderPO = null;
                string DestinationPO = null;
                string CustomerEmail = null;
                string OrderEmail = null;
                string CustomerName = null;
                string OrderStatus = null;
                string sMailTo = null;
                string sMailCC = null;
                string sMailBody = null;
                string sMailSubject = null;
                string sMailFrom = null;

                var obj = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);

                if (obj != null)
                {

                    MissingEmail = obj.OrderEmail;
                }
                else
                {
                    MissingEmail = "";
                }

                // var orderInfo = GetOrderDetail(sessionTrackingNumber, "ISD", 0);

                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(StrackNo, OtrackNo, ODestNo).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID.ToString() : "";
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                    DestinationPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.DestinationPO) ? orderAndCustomerDetailForEmail.DestinationPO : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = "0";
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                    DestinationPO = "";
                }

                //if (orderInfo != null)
                // {

                if (string.IsNullOrEmpty(OrderedBy))
                {

                    OrderedBy = "";
                }
                else
                {

                    OrderedBy = OrderedBy.Trim();
                }


                if (string.IsNullOrEmpty(CreatedBy))
                {

                    CreatedBy = "";
                }


                if (OrderStatusID == null || OrderStatusID == "0")
                {
                    OrderStatusID = "0";
                }
                else
                {

                    OrderStatusID = Convert.ToString(OrderStatusID);
                }



                if (string.IsNullOrEmpty(OrderPO))
                {
                    OrderPO = "";
                }


                if (string.IsNullOrEmpty(DestinationPO))
                {

                    DestinationPO = "";
                }


                if (string.IsNullOrEmpty(CustomerEmail))
                {

                    CustomerEmail = "";
                }

                if (string.IsNullOrEmpty(OrderEmail))
                {

                    OrderEmail = "";
                }


                if (string.IsNullOrEmpty(CustomerName))
                {

                    CustomerName = "";
                }

                if ((OrderStatusID == "1") | (OrderStatusID == "2") | (OrderStatusID == "3"))
                {
                    OrderStatus = "New Order";
                }
                else if (OrderStatusID == "4")
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if (OrderStatusID == "8")
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if (OrderStatusID == "7")
                {
                    OrderStatus = "Shipped Partial";
                }
                else if (OrderStatusID == "6")
                {
                    OrderStatus = "Shipped Complete";
                }
                else if (OrderStatusID == "13")
                {
                    OrderStatus = "Cancelled";
                }
                else if (OrderStatusID == "9")
                {
                    OrderStatus = "Under Review";
                }
                else
                {
                    OrderStatus = "";
                }


                if (CustomerEmail.Length > 0)
                {

                    sMailFrom = CustomerEmail;
                }
                else
                {

                    sMailFrom = MissingEmail;
                }


                if (OrderedBy.Length > 0)
                {

                    sMailCC = OrderedBy.Trim();
                }


                if (CreatedBy.Length > 0)
                {

                    if (!string.IsNullOrEmpty(sMailCC))
                    {

                        sMailCC = sMailCC + "; " + CreatedBy;
                    }
                    else
                    {

                        sMailCC = CreatedBy;
                    }
                }


                if (OrderEmail.Length > 0)
                {

                    if (!string.IsNullOrEmpty(sMailCC))
                    {

                        sMailCC = sMailCC + ";" + OrderEmail.Trim();
                    }
                    else
                    {

                        sMailCC = OrderEmail.Trim();
                    }
                }

                if (string.IsNullOrEmpty(sMailCC))
                {

                    sMailCC = MissingEmail.Trim();
                }

                var supplierToEmail = "";

                var supplierInfo = _dbCon.sp_GetSupplierNotificationInfo_R(StrackNo).FirstOrDefault();


                if ((supplierInfo != null))
                {
                    if (string.IsNullOrEmpty(supplierInfo.Email))
                    {
                        supplierToEmail = "";
                    }
                    else
                    {
                        supplierToEmail = supplierInfo.Email;
                    }

                    if (supplierToEmail.Length == 0)
                    {
                        supplierToEmail = MissingEmail;
                    }


                    //----------------------
                    List<string> contactEmails = _dbCon.sp_GetContactEmail_R(StrackNo, supplierInfo.SFAID).Distinct().ToList();

                    if (contactEmails.Count > 0)
                    {
                        foreach (var email in contactEmails)
                        {
                            if (string.IsNullOrEmpty(sMailTo))
                            {
                                sMailTo = email;
                            }
                            else
                            {
                                if (!sMailTo.Contains(email))
                                {
                                    sMailTo = sMailTo + ";" + email;
                                }
                            }
                        }
                    }
                    //
                    if (string.IsNullOrEmpty(sMailTo))
                    {
                        sMailTo = MissingEmail;
                    }


                    sMailBody = "GSP: " + supplierInfo.SFAName + "\n \n";

                    sMailBody = sMailBody + "Customer : " + CustomerName + "\n";
                    sMailBody = sMailBody + "Tracking #   : " + sessionTrackingNumber;
                    sMailBody = sMailBody + "Order Status : " + OrderStatus + "\n";
                    sMailBody = sMailBody + "Order PO     : " + OrderPO + "\n";
                    sMailBody = sMailBody + "Dest PO/Ref  : " + DestinationPO + "\n";
                    sMailBody = sMailBody +
                                "Please logon to https://gsonesource.global-serve.com to review the Order.\n \n";
                    sMailBody = sMailBody +
                                "Thank you for using GlobalServe ONESOURCE.\nIf you need to contact GlobalServe, please call or fax the Customer Center at :\n \n";

                    sMailBody = sMailBody + "Americas \n";
                    sMailBody = sMailBody + "phone : +1 (914) 729-6800 \n";
                    sMailBody = sMailBody + "fax : +1 201-408-5275 \n \n";
                    sMailBody = sMailBody + "GlobalServe ONESOURCE : https://gsonesource.global-serve.com/";


                    sMailSubject = "GlobalServe ONESOURCE - Order status update - (" + CustomerName + ") - " +
                                   sessionTrackingNumber
                                   + "-" + OrderTNumber + "-" + DestinationTNumber;

                    //Log before sending email
                    StaticHelper.LogException("Customer:  \nMail To: " + sMailTo + "\nMail From: " + sMailFrom + "\nMail CC: " + sMailCC + "\nMail body: " + sMailBody, "SendOrderUpdateNotification");
                    StaticHelper.SendEmailMessage(sMailTo, sMailFrom, sMailCC, "", sMailBody,
                             sMailSubject);

                    //send mail to supplier 
                    var bodyForSupplier = sMailBody.Replace("https://gsonesource.global-serve.com",
                       " https://onesource.global-serve.com");


                    StaticHelper.LogException("Supplier:  \nMail To: " + supplierToEmail + "\nMail From: " + sMailFrom + "\nMail CC: " + "No CC" + "\nMail body: " + bodyForSupplier, "SendOrderUpdateNotification");
                    StaticHelper.SendEmailMessage(supplierToEmail, sMailFrom, "", "", bodyForSupplier,
                             sMailSubject);

                }

                // }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>SendOrderUpdateNotification<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>SendOrderUpdateNotification");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "SendOrderUpdateNotification");
                }
            }
        }


        /// <summary>
        /// send order status override notification
        /// </summary>
        /// <param name="StrackNo"></param>
        /// <param name="OtrackNo"></param>
        /// <param name="ODestNo"></param>
        /// <param name="FromStatus"></param>
        /// <param name="toStatus"></param>
        /// <param name="Reasons"></param>
        public void SendOrderStatusOverrideNotification(int StrackNo, int OtrackNo, int ODestNo, string FromStatus, string toStatus, string Reasons)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string sMailTo = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }
                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(StrackNo, OtrackNo, ODestNo).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID : 0;
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = 0;
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }
                mailbcc = mailfrom;

                if (CustomerName.Contains("IMS Health"))
                {
                    if (string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy))
                    {
                        mailto = orderAndCustomerDetailForEmail.OrderedBy;
                    }
                    else
                    {
                        mailto = orderAndCustomerDetailForEmail.CreatedBy;
                    }
                    mailcc = "";
                }
                else
                {
                    if (!string.IsNullOrEmpty(OrderedBy))
                    {
                        mailto = OrderedBy;
                    }
                    else
                    {
                        mailto = "";
                    }
                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    if (!string.IsNullOrEmpty(CreatedBy))
                    {
                        mailcc = CreatedBy;
                    }

                    if (!string.IsNullOrEmpty(OrderEmail))
                    {
                        if (!string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + ";" + OrderEmail;
                        }
                        else
                        {
                            mailcc = OrderEmail;
                        }
                    }



                }
                mailbody = "Company: " + CustomerName + Environment.NewLine;
                mailbody = mailbody + "The Order Status has been Overriden From " + FromStatus + " To " + toStatus + Environment.NewLine;
                mailbody = mailbody + "Tracking #    : " + StrackNo + "-" + OtrackNo + Environment.NewLine;
                mailbody = mailbody + "Order Status  : " + toStatus + Environment.NewLine;
                mailbody = mailbody + "Order PO  : " + OrderPO + Environment.NewLine;
                mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to review the Order." + Environment.NewLine;
                mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                mailbody = mailbody + "EMEA" + Environment.NewLine;
                mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                 Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                mailsubject = "GlobalServe ONESOURCE - Order Status Override - (" + CustomerName + ") - " + StrackNo + "-" + OtrackNo;
                OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>SendOrderStatusOverrideNotification<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>SendOrderStatusOverrideNotification");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "SendOrderStatusOverrideNotification");
                }
            }
        }

        /// <summary>
        /// order update
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserGroupName"></param>
        /// <returns></returns>
        public int UpdateOrderUpdate(OrderDetailUpdateModel model, string UserGroupName)
        {
            int count = 0;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    if (UserGroupName == "ISD")
                    {
                        var tblOrder = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber);
                        if (tblOrder != null)
                        {
                            tblOrder.OrderDate = model.OrderDate;
                            tblOrder.OrderPO = model.OrderPO;
                            _dbCon.Entry(tblOrder).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }
                        var tblOrderDestination = _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber && x.DestinationTrackingNumber == model.DestinationTrackingNumber && model.OrderTrackingNumber == model.OrderTrackingNumber);
                        if (tblOrderDestination != null)
                        {
                            tblOrderDestination.VAT = model.VAT;
                            tblOrderDestination.ShippingAmount = model.ShippingAmount;
                            tblOrderDestination.InvoiceNumber = model.InvoiceNumber;
                            tblOrderDestination.InvoiceDate = model.InvoiceDate;
                            tblOrderDestination.DestinationPO = model.DestinationPO;
                            tblOrderDestination.DateAcceptedbySupplier = model.DateAcceptedBySupplier;

                            _dbCon.Entry(tblOrderDestination).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                        }
                    }
                    else if (UserGroupName == "SFA")
                    {
                        var tblOrderDestination = _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber && x.DestinationTrackingNumber == model.DestinationTrackingNumber && model.OrderTrackingNumber == model.OrderTrackingNumber);
                        if (tblOrderDestination != null)
                        {
                            tblOrderDestination.VAT = model.VAT;
                            tblOrderDestination.ShippingAmount = model.ShippingAmount;
                            tblOrderDestination.InvoiceNumber = model.InvoiceNumber;
                            tblOrderDestination.DateAcceptedbySupplier = model.DateAcceptedBySupplier;

                            _dbCon.Entry(tblOrderDestination).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                        }
                    }
                    transaction.Commit();
                }

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>UpdateOrderUpdate<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>UpdateOrderUpdate");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateOrderUpdate");
                }
            }
            return count;
        }

        /// <summary>
        /// this method changes and cancellation request 
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="Comments"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int ChangesAndCancellationRequest(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string Comments, string userName)
        {
            int count = 0;
            try
            {
                string OrderComment = string.Empty, BRtmp = string.Empty;

                var tblorder = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (tblorder != null)
                {
                    if (!string.IsNullOrEmpty(tblorder.Comments))
                    {
                        OrderComment = tblorder.Comments;
                        BRtmp = "<br><br>";
                    }

                    tblorder.Comments = OrderComment + BRtmp + "***** Change/Cancel Request for Order " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber +
                        " *****<br>***** Placed at " + DateTime.Now + " by GlobalServe CSM " + userName + " *****<br>" + Comments.Trim() + "<br>*****";

                    _dbCon.Entry(tblorder).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();

                    ChangesAndCancellationRequestEmail(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, Comments, userName);

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>ChangesAndCancellationRequest<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter =>ChangesAndCancellationRequest");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ChangesAndCancellationRequest");
                }
            }
            return count;
        }

        /// <summary>
        /// this method changes and cancellation request email
        /// </summary>
        /// <param name="StrackNo"></param>
        /// <param name="OtrackNo"></param>
        /// <param name="DtrackNo"></param>
        /// <param name="Comments"></param>
        /// <param name="userName"></param>
        public void ChangesAndCancellationRequestEmail(int StrackNo, int OtrackNo, int DtrackNo, string Comments, string userName)
        {
            try
            {
                StaticHelper.LogException("enter in ChangesAndCancellationRequestEmail  method", "ChangesAndCancellationRequestEmail");
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => enter in ChangesAndCancellationRequestEmail  method<<<<<--");
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string DestinationPO = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }
                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(StrackNo, OtrackNo, DtrackNo).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID : 0;
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                    DestinationPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.DestinationPO) ? orderAndCustomerDetailForEmail.DestinationPO : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = 0;
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                    DestinationPO = "";
                }

                if ((OrderStatusID == 1) || (OrderStatusID == 2) || (OrderStatusID == 3))
                {
                    OrderStatus = "New Order";
                }
                else if ((OrderStatusID == 4))
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if ((OrderStatusID == 8))
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if ((OrderStatusID == 7))
                {
                    OrderStatus = "Shipped Partial";
                }
                else if ((OrderStatusID == 6))
                {
                    OrderStatus = "Shipped Complete";
                }
                else if ((OrderStatusID == 13))
                {
                    OrderStatus = "Cancelled";
                }
                else if ((OrderStatusID == 9))
                {
                    OrderStatus = "Under Review";
                }
                else
                {
                    OrderStatus = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }
                mailbcc = mailfrom;

                if (!string.IsNullOrEmpty(OrderedBy))
                {
                    mailcc = OrderedBy;
                }

                if (!string.IsNullOrEmpty(CreatedBy))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + CreatedBy;
                    }
                    else
                    {
                        mailcc = CreatedBy;
                    }
                }

                if (!string.IsNullOrEmpty(OrderEmail))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + OrderEmail;
                    }
                    else
                    {
                        mailcc = OrderEmail;
                    }
                }

                if (string.IsNullOrEmpty(mailcc))
                {
                    mailcc = MissingEmail;
                }

                var sfaInfo = _dbCon.sp_GetSFAEmailByTrackingNumbers_AdminR(StrackNo, OtrackNo).ToList();

                foreach (var item in sfaInfo)
                {
                    var contactEmails = _dbCon.sp_GetContactEmail_R(StrackNo, item.SFAID).ToList();
                    if (string.IsNullOrEmpty(item.Email))
                    {
                        mailto = "";
                    }
                    else
                    {
                        mailto = item.Email;
                    }
                    foreach (var conEmail in contactEmails)
                    {
                        if (string.IsNullOrEmpty(mailto))
                        {
                            mailto = mailto + conEmail;
                        }
                        else
                        {
                            if (!mailto.Contains(conEmail))
                            {
                                mailto = mailto + ";" + conEmail;
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    mailbody = "GSP: " + item.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Change/Cancellation Requested By: GlobalServe CSM - " + userName + Environment.NewLine;
                    mailbody = mailbody + "Tracking #    : " + StrackNo + "-" + OtrackNo + Environment.NewLine;
                    mailbody = mailbody + "Order Status  : " + OrderStatus + Environment.NewLine;
                    mailbody = mailbody + "Order PO  : " + OrderPO + Environment.NewLine;
                    mailbody = mailbody + "Destination PO/Ref  : " + DestinationPO + Environment.NewLine;
                    mailbody = mailbody + "Order Change/Cancellation Request on a Best Effort basis:" + Environment.NewLine;
                    mailbody = mailbody + "Comments: " + Comments + Environment.NewLine;
                    mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to review/add comments to the Order." + Environment.NewLine;
                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Change/Cancellation Request for Order - (" + CustomerName + ") - " + StrackNo + "-" + OtrackNo + "-" + DtrackNo;

                    EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => before sending email <<<<<--");
                    StaticHelper.LogException("before sending email", "ChangesAndCancellationRequestEmail");

                    EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter ChangesAndCancellationRequestEmail=>" + "from=" + mailfrom + ", to=" + mailto + " ,mailcc=" + mailcc + ", mailsubject=" + mailsubject + " ,mailbody=" + mailbody);
                    StaticHelper.LogException("from=" + mailfrom + ", to=" + mailto + " ,mailcc=" + mailcc + ", mailsubject=" + mailsubject + " ,mailbody=" + mailbody, "ChangesAndCancellationRequestEmail");

                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);
                    StaticHelper.LogException("after sending email", "ChangesAndCancellationRequestEmail");
                    EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => after sending email <<<<<--ChangesAndCancellationRequestEmail");

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => ChangesAndCancellationRequestEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => ChangesAndCancellationRequestEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ChangesAndCancellationRequestEmail");
                }
            }

        }

        /// <summary>
        /// this method resend order
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        public void ResendOrder(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber)
        {
            try
            {
                StaticHelper.LogException("enter in ResendOrder  method", "ResendOrder");
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => enter in ResendOrder  method <<<<<--ResendOrder");
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string DestinationPO = string.Empty;
                string SupplierName = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }

                int CustomerID = 0;
                var tblOrder = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (tblOrder != null)
                {
                    CustomerID = tblOrder.CustomerID;
                    OrderStatusID = tblOrder.OrderStatusID;
                    var tblCustomer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == CustomerID);
                    if (tblCustomer != null)
                    {
                        CustomerName = tblCustomer.CustomerName;
                        var ParentCompany = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == tblCustomer.ParentCompanyID);
                        if (ParentCompany != null)
                        {
                            CustomerEmail = ParentCompany.GSContactEmail ?? string.Empty;
                        }
                    }
                }
                if ((OrderStatusID == 1) || (OrderStatusID == 2) || (OrderStatusID == 3) || (OrderStatusID == 5))
                {
                    OrderStatus = "New Order";
                }
                else if ((OrderStatusID == 4))
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if ((OrderStatusID == 8))
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if ((OrderStatusID == 7))
                {
                    OrderStatus = "Shipped Partial";
                }
                else if ((OrderStatusID == 6))
                {
                    OrderStatus = "Shipped Complete";
                }
                else if ((OrderStatusID == 13))
                {
                    OrderStatus = "Cancelled";
                }
                else
                {
                    OrderStatus = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }

                // 'Released by customer or Released By ISD or Shipped Partial or Accepted By Supplier
                if ((OrderStatusID == 3) || (OrderStatusID == 5) || (OrderStatusID == 7) || (OrderStatusID == 8))
                {

                    //supplier notification
                    List<int> orderItemStatus = _dbCon.tblOrderItemStatus.Where(x => x.OrderItemStatusCode == "X").Select(x => x.OrderItemStatusID).ToList();

                    orderItemStatus.ForEach(x => x = Convert.ToInt32(x));

                    var suppliers = _dbCon.sp_getSFAByTrackingNumbers_AdminR(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber).ToList();

                    if (suppliers != null)
                    {
                        tblOrdersProcessQueue orq;
                        foreach (var supplier in suppliers)
                        {
                            var isAlreadyExist = _dbCon.tblOrdersProcessQueue.FirstOrDefault(x => x.SessionTrackingNumber == supplier.SessionTrackingNumber
                              && x.OrderTrackingNumber == supplier.OrderTrackingNumber && x.DestinationTrackingNumber == supplier.DestinationTrackingNumber && x.SFAID == supplier.SFAID);

                            orq = new Entity.tblOrdersProcessQueue();
                            if (isAlreadyExist == null)
                            {
                                orq.DestinationTrackingNumber = supplier.DestinationTrackingNumber;
                                orq.OrderTrackingNumber = supplier.OrderTrackingNumber;
                                orq.SessionTrackingNumber = supplier.SessionTrackingNumber;
                                orq.SFAID = Convert.ToInt32(supplier.SFAID);
                                orq.LastUpdated = DateTime.Now;
                                _dbCon.tblOrdersProcessQueue.Add(orq);
                                _dbCon.SaveChanges();
                            }
                            else
                            {
                                orq = isAlreadyExist;
                            }

                            var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == orq.SFAID);

                            if (sfa != null)
                            {
                                if (string.IsNullOrEmpty(sfa.Email))
                                {
                                    mailto = string.Empty;
                                }
                                else
                                {
                                    mailto = sfa.Email;
                                }
                                if (string.IsNullOrEmpty(sfa.SFAName))
                                {
                                    SupplierName = string.Empty;
                                }
                                else
                                {
                                    SupplierName = sfa.SFAName;
                                }
                            }
                            else
                            {
                                mailto = "";
                                SupplierName = "";
                            }
                            var contactEmails = _dbCon.sp_GetContactEmail_R(SessionTrackingNumber, supplier.SFAID).ToList();

                            foreach (var conEmail in contactEmails)
                            {
                                if (string.IsNullOrEmpty(mailto))
                                {
                                    mailto = mailto + conEmail;
                                }
                                else
                                {
                                    if (!mailto.Contains(conEmail))
                                    {
                                        mailto = mailto + ";" + conEmail;
                                    }
                                }
                            }
                            if (string.IsNullOrEmpty(mailto))
                            {
                                mailto = MissingEmail;
                            }
                            mailcc = mailfrom;


                            mailbody = "GSP: " + SupplierName + Environment.NewLine;
                            mailbody = mailbody + "Customer: " + CustomerName + Environment.NewLine;
                            mailbody = mailbody + "The following order(s) comes from GlobalServe ONESOURCE system " + Environment.NewLine;

                            var resendItem = _dbCon.sp_getResendOrderItems_AdminR(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, supplier.SFAID).ToList();

                            if (resendItem.Count > 0)
                            {
                                foreach (var it in resendItem)
                                {
                                    mailbody = mailbody + "Tracking#        : " + it.TrackingNo + Environment.NewLine;
                                    mailbody = mailbody + "Order Date       : " + it.OrderDate + Environment.NewLine;
                                    mailbody = mailbody + "Order Status     : " + OrderStatus + Environment.NewLine;
                                    mailbody = mailbody + "Order PO         : " + it.OrderPO + Environment.NewLine;
                                    mailbody = mailbody + "Dest PO/Ref      : " + it.DestinationPO + Environment.NewLine;
                                    mailbody = mailbody + "Delivery Address : " + it.CondensedAddress + Environment.NewLine + Environment.NewLine;
                                }
                            }

                            mailbody = mailbody + "Please logon to https://gsonesource.global-serve.comto to accept and retrieve the Order(s)." + Environment.NewLine;
                            mailbody = mailbody + "Please review the details of the order(s), including the contracted" + Environment.NewLine;
                            mailbody = mailbody + "global prices for this order which are based on a contract between" + Environment.NewLine;
                            mailbody = mailbody + CustomerName + " and " + SupplierName + ". By accepting the order(s) and performing" + Environment.NewLine;
                            mailbody = mailbody + "hereunder, " + SupplierName + " agrees to comply fully with the following conditions:" + Environment.NewLine;
                            mailbody = mailbody + "order(s) must be accepted within two business days with a" + Environment.NewLine;
                            mailbody = mailbody + "estimated ship date; and any change to the status of the order(s)," + Environment.NewLine;
                            mailbody = mailbody + "revised estimated ship date and actual date shipped must be updated" + Environment.NewLine;
                            mailbody = mailbody + "via the ONESOURCE system withinone business day; and any order(s)" + Environment.NewLine;
                            mailbody = mailbody + "not delivered within fifteen business days of the estimated or" + Environment.NewLine;
                            mailbody = mailbody + "revised ship date (accepted) may be cancelled within " + SupplierName + "'s" + Environment.NewLine;
                            mailbody = mailbody + "flexibility to cancel with the manufacturer. " + SupplierName + " must" + Environment.NewLine;
                            mailbody = mailbody + "work with the manufacturer to resolve all  customer DOAs within three" + Environment.NewLine;
                            mailbody = mailbody + "business days or offer a refund if made available by the manufacturer." + Environment.NewLine;
                            mailbody = mailbody + "Serial numbers must be provided for all shipped products. Paper copies" + Environment.NewLine;
                            mailbody = mailbody + "of the order(s) may be obtained from the ONESOURCE system once" + Environment.NewLine;
                            mailbody = mailbody + "the order is accepted." + Environment.NewLine;
                            mailbody = mailbody + "If ONESOURCE User IDs or system training is required please contact" + Environment.NewLine;
                            mailbody = mailbody + "fmg@global-serve.com" + Environment.NewLine;
                            mailbody = mailbody + "If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                            mailbody = mailbody + "EMEA" + Environment.NewLine;
                            mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                             Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                            mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                            mailsubject = "GlobalServe ONESOURCE - New Order - (" + CustomerName + ") - " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber;

                            StaticHelper.LogException("before sending email", "resendorder");
                            EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => before sending email  method <<<<<--ResendOrder");

                            StaticHelper.LogException("from=" + mailfrom + ", to=" + mailto + " ,mailcc=" + mailcc + ", mailsubject=" + mailsubject + " ,mailbody=" + mailbody, "resendorder");
                            EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => " + "from=" + mailfrom + ", to=" + mailto + " ,mailcc=" + mailcc + ", mailsubject=" + mailsubject + " ,mailbody=" + mailbody + "  method <<<<<--ResendOrder");

                            OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, "", mailbody, mailsubject);
                            StaticHelper.LogException("after sending email", "resendorder");
                            EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => after sending email  method <<<<<--ResendOrder");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => ResendOrder<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => ResendOrder");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ResendOrder");
                }
            }
        }

        /// <summary>
        /// this method get order items
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="CustomerName"></param>
        /// <param name="Status"></param>
        /// <param name="DateAcceptedBySupplier"></param>
        /// <param name="datetimeformat"></param>
        /// <returns></returns>
        public OrderItemsModel GetOrderItems(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string CustomerName, string Status, DateTime? DateAcceptedBySupplier, string datetimeformat)
        {
            try
            {
                OrderItemsModel obj = new Model.OrderItemsModel();

                obj.GSPList = _dbCon.tblSFAs.Where(x => x.IsComposite == true && x.IsActive == true && x.ParentCompanyID != null).OrderBy(x => x.SFAName)
                   .Select(x => new GenericList
                   {
                       Value = x.SFAName,
                       Key = x.SFAID
                   }).ToList();

                obj.GSPList.Insert(0, new Model.GenericList { Key = 0, Value = "< GSP NOT Selected >" });
                obj.OrderItems = _dbCon.sp_getOrderItems_AdminR(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber).ToList();
                obj.Distributors = _dbCon.tblDistributors.Where(x => x.IsActive == true).OrderBy(x => x.DistributorName).Select(x => new GenericList
                {
                    Key = x.DistributorID,
                    Value = x.DistributorName
                }).ToList();
                obj.Distributors.Insert(0, new Model.GenericList { Key = 0, Value = "< Distributor NOT Selected >" });
                obj.CustomerName = CustomerName;
                obj.Status = Status;
                obj.DateAcceptedBySupplier = DateAcceptedBySupplier;

                obj.ViewFields = new List<string>();
                var customer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerName == CustomerName);
                if (customer != null)
                {
                    obj.ViewFields = _dbCon.sp_getViewFieldsByCustomer_AdminR(customer.CustomerID, "/search-order").ToList();
                }
                var order = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (order != null)
                {
                    //obj.Reason = order.ReasonsForDelay;
                    if (!string.IsNullOrEmpty(order.ReasonsForDelay))
                    {
                        obj.tblReason = GetTableFromComments(order.ReasonsForDelay, datetimeformat);
                    }
                    var currency = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == order.OrderDestinationCurrencyID);
                    if (currency != null)
                    {
                        obj.CurrencyName = currency.CurrencyName;
                    }
                }
                obj.NetPaymentDays = _dbCon.sp_getNetPaymentDays_AdminR(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber).FirstOrDefault();

                var dispositionQuoteIds = @"select distinct dq.dispositionquoteid
                                from tbldispositionquotes dq
                                inner  join tblorderitems oi on oi.sessiontrackingnumber =" + SessionTrackingNumber + @"
                                and oi.ordertrackingnumber = " + OrderTrackingNumber + @" and oi.destinationtrackingnumber = " + DestinationTrackingNumber + @"
                                inner join tbldispositionresponses dr on dr.dispositionresponseid = oi.assetdispositionpartid and dr.dispositionquoteid = dq.dispositionquoteid
                                order by dq.dispositionquoteid";

                obj.DispositionQuoteIds = _dbCon.Database.SqlQuery<int>(dispositionQuoteIds).ToList();

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => GetOrderItems<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => GetOrderItems");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrderItems");
                }
                return null;
            }
        }

        /// <summary>
        /// this method use for part cancellation request
        /// </summary>
        /// <param name="OrderItemId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <param name="SessionUserName"></param>
        public void PartCancellationRequest(int OrderItemId, int userId, string comments, string SessionUserName)
        {
            try
            {
                var OrderItemDetail = _dbCon.tblOrderItems.FirstOrDefault(x => x.OrderItemId == OrderItemId);
                if (OrderItemDetail != null)
                {
                    int CancelID;
                    var OrderItemStatus = _dbCon.tblOrderItemStatus.FirstOrDefault(x => x.OrderItemStatusCode == "X");
                    if (OrderItemStatus != null)
                    {
                        CancelID = OrderItemStatus.OrderItemStatusID > 0 ? OrderItemStatus.OrderItemStatusID : 1;
                    }
                    else
                    {
                        CancelID = 1;
                    }
                    OrderItemDetail.ItemStatusID = CancelID;
                    OrderItemDetail.ItemStatusLastUpdated = DateTime.Now;
                    OrderItemDetail.ItemStatusUpdatedByUserID = userId;
                    OrderItemDetail.ItemStatusComments = "***** Cancellation/Returns comments *****<br>***** Placed at " + DateTime.Now + " by GlobalServe CSM " + SessionUserName + " *****<br>" + comments + "<br>*****";

                    _dbCon.Entry(OrderItemDetail).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();


                    CancelOrderItem(OrderItemId, OrderItemDetail.SessionTrackingNumber, OrderItemDetail.OrderTrackingNumber);
                    CheckOrderDeliveryStatus(OrderItemDetail.SessionTrackingNumber, OrderItemDetail.OrderTrackingNumber, OrderItemDetail.DestinationTrackingNumber, userId);
                    UpdateOrderStatus(OrderItemDetail.SessionTrackingNumber, OrderItemDetail.OrderTrackingNumber, OrderItemDetail.DestinationTrackingNumber);
                    PartCancellationRequestEmail(OrderItemDetail.SessionTrackingNumber, OrderItemDetail.OrderTrackingNumber, OrderItemDetail.DestinationTrackingNumber, comments, SessionUserName, OrderItemId);
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => PartCancellationRequest<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => PartCancellationRequest");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "PartCancellationRequest");
                }
            }
        }

        /// <summary>
        /// this method use for cancel order items
        /// </summary>
        /// <param name="OrderItemID"></param>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        public void CancelOrderItem(int OrderItemID, int SessionTrackingNumber, int OrderTrackingNumber)
        {
            try
            {
                var OrderItemTotal = _dbCon.sp_getTotalOrderValue_AdminR(SessionTrackingNumber, OrderTrackingNumber).FirstOrDefault();

                if (OrderItemTotal != null)
                {
                    var tblOrders = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                    tblOrders.TotalOrderValue = Convert.ToDecimal(OrderItemTotal.TotalOrderValue);
                    tblOrders.TotalOrderValueUSD = Convert.ToDecimal(OrderItemTotal.TotalOrderValueUSD);

                    _dbCon.Entry(tblOrders).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();

                    //var assetOwned = _dbCon.GSNetP_tbl_AssetsOwned.FirstOrDefault(x => x.LineItemID == OrderItemID);

                    //if (assetOwned != null)
                    //{
                    _dbCon.Database.CommandTimeout = 5000;
                    // _dbCon.sp_DeleteOrderItemAssets_AdminR(OrderItemID);
                    //}
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => CancelOrderItem<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => CancelOrderItem");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "CancelOrderItem");
                }
            }
        }

        /// <summary>
        /// this method use for part cancellation request email
        /// </summary>
        /// <param name="StrackNo"></param>
        /// <param name="OtrackNo"></param>
        /// <param name="DtrackNo"></param>
        /// <param name="Comments"></param>
        /// <param name="userName"></param>
        /// <param name="orderItemId"></param>
        public void PartCancellationRequestEmail(int StrackNo, int OtrackNo, int DtrackNo, string Comments, string userName, int orderItemId)
        {
            try
            {
                StaticHelper.LogException("enter in PartCancellationRequestEmail  method", "PartCancellationRequestEmail");
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>enter in PartCancellationRequestEmail  method <<<<<--PartCancellationRequestEmail");
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string DestinationPO = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }
                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(StrackNo, OtrackNo, DtrackNo).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID : 0;
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                    DestinationPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.DestinationPO) ? orderAndCustomerDetailForEmail.DestinationPO : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = 0;
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                    DestinationPO = "";
                }

                if ((OrderStatusID == 1) || (OrderStatusID == 2) || (OrderStatusID == 3))
                {
                    OrderStatus = "New Order";
                }
                else if ((OrderStatusID == 4))
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if ((OrderStatusID == 8))
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if ((OrderStatusID == 7))
                {
                    OrderStatus = "Shipped Partial";
                }
                else if ((OrderStatusID == 6))
                {
                    OrderStatus = "Shipped Complete";
                }
                else if ((OrderStatusID == 13))
                {
                    OrderStatus = "Cancelled";
                }
                else if ((OrderStatusID == 9))
                {
                    OrderStatus = "Under Review";
                }
                else
                {
                    OrderStatus = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }
                mailbcc = mailfrom;

                if (!string.IsNullOrEmpty(OrderedBy))
                {
                    mailcc = OrderedBy;
                }

                if (!string.IsNullOrEmpty(CreatedBy))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + CreatedBy;
                    }
                    else
                    {
                        mailcc = CreatedBy;
                    }
                }

                if (!string.IsNullOrEmpty(OrderEmail))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + OrderEmail;
                    }
                    else
                    {
                        mailcc = OrderEmail;
                    }
                }

                if (string.IsNullOrEmpty(mailcc))
                {
                    mailcc = MissingEmail;
                }



                var orderItem = _dbCon.tblOrderItems.FirstOrDefault(x => x.OrderItemId == orderItemId);
                List<tblSFA> sfaInfo = new List<tblSFA>();
                if (orderItem != null)
                {
                    sfaInfo = _dbCon.tblSFAs.Where(x => x.SFAID == orderItem.SFAID).ToList();
                }
                foreach (var item in sfaInfo)
                {
                    var contactEmails = _dbCon.sp_GetContactEmail_R(StrackNo, item.SFAID).ToList();
                    if (string.IsNullOrEmpty(item.Email))
                    {
                        mailto = "";
                    }
                    else
                    {
                        mailto = item.Email;
                    }
                    foreach (var conEmail in contactEmails)
                    {
                        if (string.IsNullOrEmpty(mailto))
                        {
                            mailto = mailto + conEmail;
                        }
                        else
                        {
                            if (!mailto.Contains(conEmail))
                            {
                                mailto = mailto + ";" + conEmail;
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    mailbody = "GSP: " + item.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Part cancellation/return Requested By: GlobalServe CSM - " + userName + Environment.NewLine;
                    mailbody = mailbody + "Tracking #    : " + StrackNo + "-" + OtrackNo + Environment.NewLine;
                    mailbody = mailbody + "Order Status  : " + OrderStatus + Environment.NewLine;
                    mailbody = mailbody + "Order PO  : " + OrderPO + Environment.NewLine;
                    mailbody = mailbody + "Destination PO/Ref  : " + DestinationPO + Environment.NewLine;


                    var order = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == StrackNo && x.OrderTrackingNumber == OtrackNo);
                    var orderItems = _dbCon.tblOrderItems.FirstOrDefault(x => x.SessionTrackingNumber == StrackNo && x.OrderTrackingNumber == OtrackNo && x.DestinationTrackingNumber == DtrackNo && x.OrderItemId == orderItemId);
                    if (order != null && orderItems != null)
                    {
                        var currency = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == order.OrderDestinationCurrencyID);
                        if (currency != null)
                        {
                            mailbody = mailbody + "SKU             : " + orderItems.ManufacturerSKU + Environment.NewLine;
                            mailbody = mailbody + "Part Description: " + orderItems.PartDescription + Environment.NewLine;
                            mailbody = mailbody + "Currency        : " + currency.CurrencyName + Environment.NewLine;
                            mailbody = mailbody + "Price           : " + orderItems.LineItemPrice + Environment.NewLine;
                            mailbody = mailbody + "Quantity        : " + orderItems.Quantity + Environment.NewLine;
                            mailbody = mailbody + "Total Price     : " + orderItems.LineItemTotal + Environment.NewLine;
                        }
                    }

                    mailbody = mailbody + "Comments: " + Comments.Trim() + Environment.NewLine;
                    mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to review the Order." + Environment.NewLine;
                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    StaticHelper.LogException("before sending email", "PartCancellationRequestEmail");
                    EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>before sending email  method <<<<<--PartCancellationRequestEmail");

                    mailsubject = "GlobalServe ONESOURCE - Part Cancellation/Return Request for Order - (" + CustomerName + ") - " + StrackNo + "-" + OtrackNo + "-" + DtrackNo;

                    StaticHelper.LogException("from=" + mailfrom + ", to=" + mailto + " ,mailcc=" + mailcc + ", mailsubject=" + mailsubject + " ,mailbody=" + mailbody, "PartCancellationRequestEmail");
                    EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter =>" + "from=" + mailfrom + ", to=" + mailto + " ,mailcc=" + mailcc + ", mailsubject=" + mailsubject + " ,mailbody=" + mailbody + " <<<<<--PartCancellationRequestEmail");

                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);
                    EventLog.WriteEntry("Application", "-->>>>>after sending email => <<<<<--PartCancellationRequestEmail");

                    StaticHelper.LogException("after sending email", "PartCancellationRequestEmail");


                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => PartCancellationRequestEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => PartCancellationRequestEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "PartCancellationRequestEmail");
                }
            }
        }

        /// <summary>
        /// this method use for updating order item
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sessionUserName"></param>
        /// <returns></returns>
        public int UpdateOrderItems(OrderItemUpdateModel model, int userId, string userGroup, string sessionUserName)
        {
            try
            {

                string CustomerName = string.Empty;
                var order = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber && x.OrderTrackingNumber == model.OrderTrackingNumber);
                if (order != null)
                {
                    var customer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == order.CustomerID);
                    if (customer != null)
                    {
                        CustomerName = customer.CustomerName;
                    }
                }
                if (string.IsNullOrEmpty(model.OrderItemIDs)) return 0;

                List<int> lstOrderItemIDs = new List<int>();
                List<int> lstOrderDeliveryDetailsID = new List<int>();

                foreach (var item in model.OrderItemIDs.Split(','))
                {
                    lstOrderItemIDs.Add(Convert.ToInt32(item.Split('^')[0]));
                    lstOrderDeliveryDetailsID.Add(Convert.ToInt32(string.IsNullOrEmpty(item.Split('^')[1]) ? "0" : item.Split('^')[1]));
                }


                var OrderItems = _dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == model.SessionTrackingNumber
                   && x.OrderTrackingNumber == model.OrderTrackingNumber && x.DestinationTrackingNumber == model.DestinationTrackingNumber && lstOrderItemIDs.Contains(x.OrderItemId)).ToList();

                //GSP Update
                if (model.GSP > 0)
                {

                    foreach (var item in OrderItems)
                    {
                        item.SFAID = model.GSP;

                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }
                //UpdateOrderItemsETA
                if (model.ExpectedDeliveryDate != null)
                {
                    if (userGroup == "SFA")
                    {
                        // bool slaAlert = false;
                        int slaDays = 0;
                        var SQLBizdays = "select dbo.GetWorkingDayswithinRange('" + order.OrderDate + "','" + model.ExpectedDeliveryDate + "')";

                        var bizDays = _dbCon.Database.SqlQuery<int>(SQLBizdays).FirstOrDefault();

                        var countryDeliverySLAs = _dbCon.tblCountryDeliverySLAs.FirstOrDefault(x => x.CountryID == order.OrderDestinationCountryID);
                        if (countryDeliverySLAs != null)
                        {
                            slaDays = Convert.ToInt32(countryDeliverySLAs.Days);
                        }

                        if (bizDays > 0 && slaDays > 0)
                        {
                            if (bizDays > slaDays && model.ReasonType == "0")
                            {
                                //slaAlert = true;
                                return -2;
                            }
                            else
                            {
                                var reason = model.ReasonType + " - " + model.ReasonForDelay;
                                ReasonForDelaySupplier(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber, reason, sessionUserName);
                            }
                        }

                    }


                    int OrderActionID = 0;
                    int OrderTrackingTypeID = 0;
                    int OrderStatusID = 0;
                    string ChangeETAStr = string.Empty;
                    foreach (var item in OrderItems)
                    {
                        item.ETA = model.ExpectedDeliveryDate;
                        item.ETAUpdatedByUserID = userId;
                        item.ETALastUpdated = DateTime.Now;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                    var orderDestination = _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber
                     && x.OrderTrackingNumber == model.OrderTrackingNumber && x.DestinationTrackingNumber == model.DestinationTrackingNumber);
                    if (orderDestination != null)
                    {
                        if (orderDestination.ETA != null)
                        {
                            ChangeETAStr = "ETS changed from " + orderDestination.ETA + " to ";
                        }
                        else
                        {
                            ChangeETAStr = "ETS changed from Nothing to ";
                        }
                    }
                    else
                    {
                        ChangeETAStr = "ETS changed from Nothing to ";
                    }
                    //UpdateOrderETA
                    var maxETA = _dbCon.sp_getMaxETA_AdminR(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber).FirstOrDefault();

                    orderDestination.ETA = maxETA;
                    orderDestination.ETALastUpdatedByUserID = userId;
                    orderDestination.ETALastUpdated = DateTime.Now;

                    _dbCon.Entry(orderDestination).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();

                    orderDestination = _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber
                && x.OrderTrackingNumber == model.OrderTrackingNumber && x.DestinationTrackingNumber == model.DestinationTrackingNumber);
                    if (orderDestination != null)
                    {
                        if (orderDestination.ETA != null)
                        {
                            ChangeETAStr += orderDestination.ETA;
                        }
                        else
                        {
                            ChangeETAStr = ChangeETAStr + "Nothing";
                        }
                    }
                    else
                    {
                        ChangeETAStr = ChangeETAStr + "Nothing";
                    }

                    var orderAction = _dbCon.tblOrderActions.FirstOrDefault(x => x.OrderActionName == "ETS Change");
                    if (orderAction != null)
                    {
                        OrderActionID = orderAction.OrderActionID > 0 ? orderAction.OrderActionID : 0;
                    }

                    var orderTrackingType = _dbCon.tblOrderTrackingTypes.FirstOrDefault(x => x.OrderTrackingTypeName == "GS Only Comments");
                    if (orderTrackingType != null)
                    {
                        OrderTrackingTypeID = orderTrackingType.OrderTrackingTypeID > 0 ? orderTrackingType.OrderTrackingTypeID : 0;
                    }
                    if (order.OrderStatusID > 0)
                    {
                        OrderStatusID = order.OrderStatusID;
                    }

                    if (OrderActionID > 0 && OrderTrackingTypeID > 0)
                    {
                        tblOrderTracking obj = new Entity.tblOrderTracking();
                        obj.SessionTrackingNumber = model.SessionTrackingNumber;
                        obj.OrderTrackingNumber = Convert.ToInt16(model.OrderTrackingNumber);
                        obj.DestinationTrackingNumber = Convert.ToInt16(model.DestinationTrackingNumber);
                        obj.OrderStatusID = OrderStatusID;
                        obj.OrderTrackingTypeID = OrderTrackingTypeID;
                        obj.OrderActionID = OrderActionID;
                        obj.Comment = ChangeETAStr;
                        obj.IsActive = true;
                        obj.LastUpdated = DateTime.Now;
                        obj.LastUpdatedByUserID = userId;
                        obj.QuoteActionName = null;

                        _dbCon.tblOrderTracking.Add(obj);
                        _dbCon.SaveChanges();
                    }

                }

                if (!string.IsNullOrEmpty(model.OEMOrderNo))
                {
                    foreach (var item in OrderItems)
                    {
                        item.MfgTrackingID = model.OEMOrderNo;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }
                if (model.DateShipped != null)
                {
                    #region DateShipped
                    int Quantity = 1;
                    for (int i = 0; i < lstOrderItemIDs.Count; i++)
                    {
                        var value = lstOrderItemIDs[i];
                        var OrderItemQuantity = _dbCon.tblOrderItems.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber
                        && x.DestinationTrackingNumber == model.DestinationTrackingNumber && x.OrderTrackingNumber == model.OrderTrackingNumber && x.OrderItemId == value);
                        if (OrderItemQuantity != null)
                        {
                            Quantity = OrderItemQuantity.Quantity;
                        }
                        var iTmp = 0;
                        bool bDoUpdate = false;
                        var orderDeliveryDetail = _dbCon.tblOrderDeliveryDetails.FirstOrDefault(x => x.OrderDeliveryItemID == value);
                        if (orderDeliveryDetail != null)
                        {
                            iTmp = orderDeliveryDetail.OrderDeliveryDetailsID > 0 ? orderDeliveryDetail.OrderDeliveryDetailsID : 0;
                        }
                        if (iTmp > 0)
                        {
                            lstOrderDeliveryDetailsID[i] = iTmp;
                            bDoUpdate = true;
                        }
                        if (bDoUpdate)
                        {
                            _dbCon.usp_UpdateDeliveryDetail(lstOrderDeliveryDetailsID[i], Convert.ToDateTime(model.DateShipped).ToString("MM/dd/yyyy"), null, Quantity, null, null, null, null, null, userId);
                        }
                        else
                        {
                            _dbCon.usp_InsertDeliveryDetail(value, Quantity, null, null, null, null, model.DateShipped, null, null, null, null, userId);
                        }

                    }
                    CheckOrderDeliveryStatus(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber, userId);

                    //UpdateOrderDateShipped
                    int shippedID = 0;
                    int deliveryStatusID = 0;
                    DateTime? dateShipped = null;
                    var deliveryStatus = _dbCon.tblDeliveryStatus.FirstOrDefault(x => x.DeliverystatusName == "Shipped");
                    if (deliveryStatus != null)
                    {
                        shippedID = deliveryStatus.DeliveryStatusID > 0 ? deliveryStatus.DeliveryStatusID : 0;
                    }
                    if (shippedID > 0)
                    {
                        var orderDestination = _dbCon.tblOrderDestinations.FirstOrDefault(x => x.SessionTrackingNumber == model.SessionTrackingNumber
                        && x.DestinationTrackingNumber == model.DestinationTrackingNumber && x.OrderTrackingNumber == model.OrderTrackingNumber);

                        if (orderDestination != null)
                        {
                            deliveryStatusID = orderDestination.DeliveryStatusID != null ? Convert.ToInt32(orderDestination.DeliveryStatusID) : 0;
                        }
                        if (deliveryStatusID > 0 && deliveryStatusID == shippedID)
                        {
                            var maxDateShipped = _dbCon.sp_getMaxDateShipped_AdminR(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber).FirstOrDefault();
                            if (maxDateShipped != null)
                            {
                                dateShipped = maxDateShipped;
                            }

                            orderDestination.DateShipped = dateShipped;
                            orderDestination.DateShippedLastUpdatedByUserID = userId;
                            orderDestination.DateShippedLastUpdated = DateTime.Now;

                            _dbCon.Entry(orderDestination).State = System.Data.Entity.EntityState.Modified;
                            _dbCon.SaveChanges();

                        }
                    }
                    #endregion
                }

                if (model.DistributorId > 0)
                {
                    foreach (var item in OrderItems)
                    {
                        item.DistributorID = model.DistributorId;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                if (!string.IsNullOrEmpty(model.GSPtoDistriNo))
                {
                    foreach (var item in OrderItems)
                    {
                        item.GSPToDistriOrderNum = model.GSPtoDistriNo;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }

                }
                if (!string.IsNullOrEmpty(model.DistritoOEMNo))
                {
                    foreach (var item in OrderItems)
                    {
                        item.DistriToOEMOrderNum = model.DistritoOEMNo;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }
                if (model.ShipDateOEM != null)
                {
                    foreach (var item in OrderItems)
                    {
                        item.OEMShipDate = model.ShipDateOEM;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }
                if (model.ShipDateDistri != null)
                {
                    foreach (var item in OrderItems)
                    {
                        item.DistriShipDate = model.ShipDateDistri;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                if (!string.IsNullOrEmpty(model.ShipperTrackingName))
                {
                    foreach (var item in OrderItems)
                    {
                        item.ShipperTrackingName = model.ShipperTrackingName;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                if (!string.IsNullOrEmpty(model.ShipperTrackingNo))
                {
                    foreach (var item in OrderItems)
                    {
                        item.ShipperTrackingNum = model.ShipperTrackingNo;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                if (model.CustomClearanceDate != null)
                {
                    foreach (var item in OrderItems)
                    {
                        item.CustomClearanceDate = model.CustomClearanceDate;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                if (model.EstimatedInstallationDate != null)
                {
                    foreach (var item in OrderItems)
                    {
                        item.EstimatedInstallationDate = model.EstimatedInstallationDate;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }
                if (model.ActualInstallationDate != null)
                {
                    foreach (var item in OrderItems)
                    {
                        item.ActualInstallationDate = model.ActualInstallationDate;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                CheckOrderDeliveryStatus(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber, userId);

                UpdateOrderStatus(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber);

                if (model.DateDelivered != null)
                {
                    for (int i = 0; i < lstOrderItemIDs.Count; i++)
                    {
                        var iTmp = 0;
                        bool bDoUpdate = false;
                        var value = lstOrderItemIDs[i];
                        var orderDeliveryDetail = _dbCon.tblOrderDeliveryDetails.FirstOrDefault(x => x.OrderDeliveryItemID == value);
                        if (orderDeliveryDetail != null)
                        {
                            iTmp = orderDeliveryDetail.OrderDeliveryDetailsID > 0 ? orderDeliveryDetail.OrderDeliveryDetailsID : 0;
                        }
                        if (iTmp > 0)
                        {
                            lstOrderDeliveryDetailsID[i] = iTmp;
                            bDoUpdate = true;
                        }
                        if (bDoUpdate)
                        {
                            _dbCon.usp_UpdateDateDeliveryDetail(lstOrderDeliveryDetailsID[i], Convert.ToDateTime(model.DateDelivered).ToString("MM/dd/yyyy"), null, null, null, null, null, userId);
                        }
                    }
                    CheckOrderDeliveryStatus(model.SessionTrackingNumber, model.OrderTrackingNumber, model.DestinationTrackingNumber, userId);
                }

                if (!String.IsNullOrWhiteSpace(model.TrackingReferenceUrl)) // added (SUNNY GOHAR)
                {
                    foreach (var item in OrderItems)
                    {
                        item.TrackingReference = model.TrackingReferenceUrl;
                        _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                }

                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => UpdateOrderItems<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => UpdateOrderItems");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateOrderItems");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method use for getting order comments
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="DateFormatID"></param>
        /// 
        /// Changes by Aurangzeb
        /// Changed this function for added functionality of tblOrderCommentsBySupplier
        ///<param name="SupplierID"></param>
        /// <returns></returns>
        public OrderCommentsModel GetOrderComments(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, int DateFormatID, int SupplierID)
        {
            try
            {
                OrderCommentsModel model = new OrderCommentsModel();
                model.Comments = string.Empty;
                var order = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (order != null)
                {
                    model.JiraTicketNumber = order.JiraTicketNumber;
                    model.Comments = order.Comments;

                    model.tblComments = GetTableFromComments(model.Comments, DateFormatID.ToString());
                }
                model.GSComments = _dbCon.sp_getGSOnlyComments_AdminR(SessionTrackingNumber.ToString(), OrderTrackingNumber.ToString(), DestinationTrackingNumber.ToString(), DateFormatID).ToList();
                model.OrderCommentsbySupplier = _dbCon.sp_getCommentsBySupplier_AdminR(SessionTrackingNumber.ToString(), DateFormatID, SupplierID, 0).ToList();
                return model;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => GetOrderComments<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => GetOrderComments");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrderComments");
                }
                return new OrderCommentsModel();
            }
        }

        /// <summary>
        /// this method use for getting comment from string form to datatable
        /// </summary>
        /// <param name="comments"></param>
        /// <param name="dateFormat"></param>
        /// <returns></returns>
        public DataTable GetTableFromComments(string comments, string dateFormat)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("LogDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("Event");
                dt.Columns.Add("Comments");
                DataRow dr = default(DataRow);
                string[] arrline = null;

                ArrayList arrlist = new ArrayList();
                int i = 0;
                int j = 0;
                string tempcomments = comments;

                int StartIndex = 0;
                int StrLength = 0;


                if (comments != null)
                {

                    while (!(i == -1))
                    {
                        i = tempcomments.IndexOf("<br><br>");
                        if (i > 0)
                        {
                            arrlist.Add(tempcomments.Substring(0, i));
                            tempcomments = tempcomments.Remove(0, i + 8);
                        }
                    }
                    arrlist.Add(tempcomments);

                    foreach (string Str in arrlist)
                    {
                        if (!(Str.StartsWith("*****") & Str.EndsWith("*****")))
                        {
                            if (Str.Contains("Placed by ISD"))
                            {
                                i = Str.IndexOf("ISD") + 4;
                                dr = dt.NewRow();
                                dr["Event"] = "Placed by ISD Representative";

                                int indexofR = Str.IndexOf("Representative");
                                if (Str.IndexOf("<p>") > 0)
                                {
                                    StrLength = Str.IndexOf("<p>") - indexofR - 15;
                                    if (StrLength > 0)
                                    {
                                        dr["UpdateBy"] = Str.Substring(indexofR + 15, StrLength);
                                    }
                                    else
                                    {
                                        dr["UpdateBy"] = Str;
                                    }
                                }
                                else
                                {
                                    dr["UpdateBy"] = Str.Substring(indexofR + 15);
                                    //, Str.Length - 1)
                                }

                                string s = Str;
                                s = s.Remove(0, Str.IndexOf("<p>") + 4);
                                s = s.Remove(0, s.IndexOf("Auto-updated") + 13);
                                StartIndex = s.IndexOf("<p>");
                                if (StartIndex >= 0)
                                {
                                    StrLength = s.Length - s.IndexOf("<p>");
                                    s = s.Remove(StartIndex, StrLength);
                                    dr["LogDate"] = getDateString(s, dateFormat);
                                }
                                else
                                {
                                    dr["LogDate"] = "";
                                }
                                dr["Comments"] = "Auto-Updated";
                            }
                            else
                            {
                                dr = dt.NewRow();
                                dr["LogDate"] = "";
                                dr["UpdateBy"] = "";
                                dr["Event"] = "Comments Added during Order";
                                dr["Comments"] = Str;
                            }
                        }
                        else
                        {
                            arrline = Str.Split(new string[] { "<br>" }, StringSplitOptions.None);

                            dr = dt.NewRow();
                            if (arrline.Length == 4)
                            {
                                if (arrline[0].Contains("Customer Comments"))
                                {
                                    dr["Event"] = "Customer Comments";
                                }
                                else if (arrline[0].Contains("Change/Cancel Request"))
                                {
                                    dr["Event"] = "Change/Cancel Request";
                                }
                                else if (arrline[0].Contains("Supplier Comments"))
                                {
                                    dr["Event"] = "Supplier Comments";
                                }
                                else if (arrline[0].Contains("Additional Comments for Order"))
                                {
                                    dr["Event"] = arrline[0].Replace("*", "");
                                }
                                else if (arrline[0].Contains("Supplier Reasons for Order"))
                                {
                                    dr["Event"] = arrline[0].Replace("*", "");
                                }
                                else if (arrline[0].Contains("Additional Reasons for Order"))
                                {
                                    dr["Event"] = arrline[0].Replace("*", "");
                                }

                                int indexAT = arrline[1].IndexOf("at");
                                int indexBY = arrline[1].IndexOf("by");

                                dr["LogDate"] = getDateString(arrline[1].Substring(indexAT + 3, indexBY - indexAT - 3), dateFormat);
                                dr["UpdateBy"] = arrline[1].Substring(indexBY + 3, arrline[1].Length - 8 - indexBY).Trim('*');
                                dr["Comments"] = arrline[2].ToString().Substring(arrline[2].IndexOf(">") + 1);
                            }

                        }
                        dt.Rows.Add(dr);

                    }
                }
                return dt;// Newtonsoft.Json.JsonConvert.SerializeObject(dt);


            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => GetTableFromComments<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => GetTableFromComments");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTableFromComments");
                }
                return null;
            }


        }

        /// <summary>
        /// this method use for datetime formate
        /// </summary>
        /// <param name="datestr"></param>
        /// <param name="dateformat"></param>
        /// <returns></returns>
        public string getDateString(string datestr, string dateformat)
        {
            if (string.IsNullOrEmpty(dateformat))
            {
                dateformat = "dd/MM/yyyy";
            }
            else if (dateformat == "1" || dateformat == "0")
            {
                dateformat = "dd/MM/yyyy";
            }
            else if (dateformat == "2")
            {
                dateformat = "MM/dd/yyyy";
            }

            DateTime dt = default(DateTime);
            if (DateTime.TryParse(datestr, out dt))
            {
                return dt.ToString(dateformat);
            }
            return datestr;

        }

        /// <summary>
        /// this method use for adding comment
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="txtcomment"></param>
        /// <param name="isGSOnly"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// 
        /// New parameters by Aurangzeb
        /// <param name="SupplierId"></param>
        /// <param name="SupplierName"></param>
        /// <param name="isPCComment"></param>
        /// <returns></returns>
        public int AddOrderComment(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string txtcomment, bool isGSOnly, int userId, string userName, int? SupplierId, string SupplierName, bool isPCComment, bool IsUrgent = false, string jiraTicketNumber = "")
        {
            var comments = string.Empty;
            var OrderActionID = 0;
            var OrderTrackingTypeID = 0;
            try
            {
                var orders = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (orders != null)
                {
                    comments = orders.Comments;

                    //Aurangzeb Khan
                    //3-12-2018
                    //Update order ticket here

                    var updatedOrder = orders;
                    updatedOrder.JiraTicketNumber = jiraTicketNumber;
                    _dbCon.Entry(orders).CurrentValues.SetValues(updatedOrder);
                    _dbCon.SaveChanges();
                }

                if (isGSOnly)
                {
                    var orderAction = _dbCon.tblOrderActions.FirstOrDefault(x => x.OrderActionName == "Add Comment");
                    if (orderAction != null)
                    {
                        OrderActionID = orderAction.OrderActionID;
                    }

                    var ordertrackingtype = _dbCon.tblOrderTrackingTypes.FirstOrDefault(x => x.OrderTrackingTypeName == "GS Only Comments");
                    if (ordertrackingtype != null)
                    {
                        OrderTrackingTypeID = ordertrackingtype.OrderTrackingTypeID;
                    }

                    if (OrderTrackingTypeID > 0 && OrderActionID > 0)
                    {
                        //insert into tblordertracking (sessiontrackingnumber, ordertrackingnumber, destinationtrackingnumber, orderstatusid, ordertrackingtypeid, orderactionid, comment, lastupdatedbyuserid)
                        tblOrderTracking orderTracking = new tblOrderTracking();
                        orderTracking.SessionTrackingNumber = SessionTrackingNumber;
                        orderTracking.OrderTrackingNumber = Convert.ToInt16(OrderTrackingNumber);
                        orderTracking.DestinationTrackingNumber = Convert.ToInt16(DestinationTrackingNumber);
                        orderTracking.OrderStatusID = orders.OrderStatusID;
                        orderTracking.OrderTrackingTypeID = OrderTrackingTypeID;
                        orderTracking.OrderActionID = OrderActionID;
                        orderTracking.Comment = txtcomment;
                        orderTracking.LastUpdatedByUserID = userId;
                        orderTracking.LastUpdated = DateTime.Now;

                        _dbCon.tblOrderTracking.Add(orderTracking);
                        _dbCon.SaveChanges();


                    }
                }
                else
                {
                    string BRtmp = string.Empty;
                    string AdditionalComments = string.Empty;
                    if (!string.IsNullOrEmpty(comments))
                    {
                        BRtmp = "<br><br>";
                    }

                    AdditionalComments = comments + BRtmp + "***** Additional Comments for Order " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber +
                        " *****<br>***** Placed at " + DateTime.Now + " by GlobalServe CSM " + userName + " *****<br>" + txtcomment + "<br>*****";
                    if (orders != null)
                    {
                        orders.Comments = AdditionalComments;
                        _dbCon.Entry(orders).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }

                    AddOrderCommentEmail(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, userName, txtcomment, IsUrgent);

                }

                //insert into tblOrderCommentsBySupplier
                tblOrderCommentsbySupplier OrderComments = new tblOrderCommentsbySupplier();
                OrderComments.SessionTrackingNumber = SessionTrackingNumber;
                OrderComments.SupplierID = SupplierId == 0 ? null : SupplierId;
                OrderComments.SupplierName = SupplierName == "" ? null : SupplierName;
                OrderComments.Comments = txtcomment;
                OrderComments.Created = DateTime.Now;
                OrderComments.CreatedBy = userId;
                OrderComments.PCComment = isPCComment;
                OrderComments.GSOnly = isGSOnly;
                _dbCon.tblOrderCommentsbySuppliers.Add(OrderComments);
                _dbCon.SaveChanges();

                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => AddOrderComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => AddOrderComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddOrderComment");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method use for email for adding comment
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="userName"></param>
        /// <param name="txtComments"></param>
        public void AddOrderCommentEmail(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string userName, string txtComments, bool IsUrgent = false)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string DestinationPO = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }
                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID : 0;
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                    DestinationPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.DestinationPO) ? orderAndCustomerDetailForEmail.DestinationPO : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = 0;
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                    DestinationPO = "";
                }

                if ((OrderStatusID == 1) || (OrderStatusID == 2) || (OrderStatusID == 3))
                {
                    OrderStatus = "New Order";
                }
                else if ((OrderStatusID == 4))
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if ((OrderStatusID == 8))
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if ((OrderStatusID == 7))
                {
                    OrderStatus = "Shipped Partial";
                }
                else if ((OrderStatusID == 6))
                {
                    OrderStatus = "Shipped Complete";
                }
                else if ((OrderStatusID == 13))
                {
                    OrderStatus = "Cancelled";
                }
                else if ((OrderStatusID == 9))
                {
                    OrderStatus = "Under Review";
                }
                else
                {
                    OrderStatus = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }
                mailbcc = mailfrom;

                if (!string.IsNullOrEmpty(OrderedBy))
                {
                    mailcc = OrderedBy;
                }

                if (!string.IsNullOrEmpty(CreatedBy))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + CreatedBy;
                    }
                    else
                    {
                        mailcc = CreatedBy;
                    }
                }

                if (!string.IsNullOrEmpty(OrderEmail))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + OrderEmail;
                    }
                    else
                    {
                        mailcc = OrderEmail;
                    }
                }

                if (string.IsNullOrEmpty(mailcc))
                {
                    mailcc = MissingEmail;
                }

                var sfaInfo = _dbCon.sp_GetSFAEmailByTrackingNumbers_AdminR(SessionTrackingNumber, OrderTrackingNumber).ToList();

                foreach (var item in sfaInfo)
                {
                    var contactEmails = _dbCon.sp_GetContactEmail_R(SessionTrackingNumber, item.SFAID).ToList();
                    if (string.IsNullOrEmpty(item.Email))
                    {
                        mailto = "";
                    }
                    else
                    {
                        mailto = item.Email;
                    }
                    foreach (var conEmail in contactEmails)
                    {
                        if (string.IsNullOrEmpty(mailto))
                        {
                            mailto = mailto + conEmail;
                        }
                        else
                        {
                            if (!mailto.Contains(conEmail))
                            {
                                mailto = mailto + ";" + conEmail;
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    mailbody = "GSP: " + item.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Additional order comments by: GlobalServe CSM - " + userName + Environment.NewLine;
                    mailbody = mailbody + "Tracking #    : " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber + Environment.NewLine;
                    mailbody = mailbody + "Order Status  : " + OrderStatus + Environment.NewLine;
                    mailbody = mailbody + "Order PO  : " + OrderPO + Environment.NewLine;
                    mailbody = mailbody + "Destination PO/Ref  : " + DestinationPO + Environment.NewLine;
                    mailbody = mailbody + "Additional Comments: " + txtComments + Environment.NewLine;
                    mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to review/add comments to the Order." + Environment.NewLine;
                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    if (IsUrgent == true)
                    {
                        mailsubject = "Escalation! - GlobalServe ONESOURCE - GlobalServe Comments for Order - (" + CustomerName + ") - " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber;
                    }
                    else
                    {
                        mailsubject = "GlobalServe ONESOURCE - GlobalServe Comments for Order - (" + CustomerName + ") - " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber;
                    }

                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => AddOrderCommentEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => AddOrderCommentEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddOrderCommentEmail");
                }
            }

        }

        /// <summary>
        /// this method use for getting order attachments
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <returns></returns>
        public List<sp_GetAttachments_R_Result> GetOrderAttachments(int sessionTrackingNumber)
        {
            try
            {
                var obj = _dbCon.sp_GetAttachments_R(sessionTrackingNumber).ToList();
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => GetOrderAttachments<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => GetOrderAttachments");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrderAttachments");
                }
                return new List<sp_GetAttachments_R_Result>();
            }
        }

        /// <summary>
        /// this method use for adding digital asset
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="daid"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url, int userId, string sessionTrackingNumber, int? daid)
        {
            try
            {
                return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                      null, null, null, null, null, null, null, null, null, Convert.ToInt32(sessionTrackingNumber)
                      , null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => AddDigitalAsset<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method use for getting requested attachments url
        /// </summary>
        /// <param name="daid"></param>
        /// <returns></returns>
        public string GetRequestedAttachmentUrl(int daid)
        {
            try
            {
                string url = string.Empty;
                var digitalAsset = _dbCon.tblDigitalAssets.FirstOrDefault(x => x.DAID == daid);
                if (digitalAsset != null)
                {
                    url = digitalAsset.DAURL;
                }
                return url;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => GetRequestedAttachmentUrl<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => GetRequestedAttachmentUrl");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRequestedAttachmentUrl");
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// this method use for deleting attachment
        /// </summary>
        /// <param name="daid"></param>
        /// <returns></returns>
        public int DeleteAttachment(int daid)
        {
            int count = 0;
            try
            {
                using (var transcation = _dbCon.Database.BeginTransaction())
                {

                    var OrderAttachment = _dbCon.tblOrderAttachments.FirstOrDefault(x => x.DAID == daid);
                    if (OrderAttachment != null)
                    {
                        _dbCon.tblOrderAttachments.Remove(OrderAttachment);
                        count = _dbCon.SaveChanges();
                    }

                    var digitalAsset = _dbCon.tblDigitalAssets.FirstOrDefault(x => x.DAID == daid);
                    if (digitalAsset != null)
                    {
                        _dbCon.tblDigitalAssets.Remove(digitalAsset);
                        count = _dbCon.SaveChanges();
                    }
                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => DeleteAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => DeleteAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteAttachment");
                }
            }
            return count;
        }

        /// <summary>
        /// this method use for adding reason for delay ISD
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="delayReason"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int ReasonForDelayISD(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string delayReason, string userName)
        {
            int count = 0;
            try
            {
                string reason = string.Empty, BRtmp = string.Empty;

                var tblorder = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (tblorder != null)
                {
                    if (!string.IsNullOrEmpty(tblorder.ReasonsForDelay))
                    {
                        reason = tblorder.ReasonsForDelay;
                        BRtmp = "<br><br>";
                    }

                    tblorder.ReasonsForDelay = reason + BRtmp + "***** Additional Reasons for Order " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber +
                        " *****<br>***** Placed at " + DateTime.Now + " by GlobalServe CSM " + userName + " *****<br>" + delayReason.Trim() + "<br>*****";

                    _dbCon.Entry(tblorder).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();

                    ReasonForDelayISDEmail(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, delayReason, userName);

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => ReasonForDelayISD<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => ReasonForDelayISD");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ReasonForDelayISD");
                }
            }
            return count;
        }

        /// <summary>
        /// this method use for email for adding reason for delay ISD
        /// </summary>
        /// <param name="StrackNo"></param>
        /// <param name="OtrackNo"></param>
        /// <param name="DtrackNo"></param>
        /// <param name="delayReason"></param>
        /// <param name="userName"></param>
        public void ReasonForDelayISDEmail(int StrackNo, int OtrackNo, int DtrackNo, string delayReason, string userName)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string DestinationPO = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }
                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(StrackNo, OtrackNo, DtrackNo).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID : 0;
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                    DestinationPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.DestinationPO) ? orderAndCustomerDetailForEmail.DestinationPO : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = 0;
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                    DestinationPO = "";
                }

                if ((OrderStatusID == 1) || (OrderStatusID == 2) || (OrderStatusID == 3))
                {
                    OrderStatus = "New Order";
                }
                else if ((OrderStatusID == 4))
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if ((OrderStatusID == 8))
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if ((OrderStatusID == 7))
                {
                    OrderStatus = "Shipped Partial";
                }
                else if ((OrderStatusID == 6))
                {
                    OrderStatus = "Shipped Complete";
                }
                else if ((OrderStatusID == 13))
                {
                    OrderStatus = "Cancelled";
                }
                else if ((OrderStatusID == 9))
                {
                    OrderStatus = "Under Review";
                }
                else
                {
                    OrderStatus = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }
                mailbcc = mailfrom;

                if (!string.IsNullOrEmpty(OrderedBy))
                {
                    mailcc = OrderedBy;
                }

                if (!string.IsNullOrEmpty(CreatedBy))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + CreatedBy;
                    }
                    else
                    {
                        mailcc = CreatedBy;
                    }
                }

                if (!string.IsNullOrEmpty(OrderEmail))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + OrderEmail;
                    }
                    else
                    {
                        mailcc = OrderEmail;
                    }
                }

                if (string.IsNullOrEmpty(mailcc))
                {
                    mailcc = MissingEmail;
                }

                var sfaInfo = _dbCon.sp_GetSFAEmailByTrackingNumbers_AdminR(StrackNo, OtrackNo).ToList();

                foreach (var item in sfaInfo)
                {
                    var contactEmails = _dbCon.sp_GetContactEmail_R(StrackNo, item.SFAID).ToList();
                    if (string.IsNullOrEmpty(item.Email))
                    {
                        mailto = "";
                    }
                    else
                    {
                        mailto = item.Email;
                    }
                    foreach (var conEmail in contactEmails)
                    {
                        if (string.IsNullOrEmpty(mailto))
                        {
                            mailto = mailto + conEmail;
                        }
                        else
                        {
                            if (!mailto.Contains(conEmail))
                            {
                                mailto = mailto + ";" + conEmail;
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    mailbody = "GSP: " + item.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Additional reasons for order delay by: GlobalServe CSM - " + userName + Environment.NewLine;
                    mailbody = mailbody + "Tracking #    : " + StrackNo + "-" + OtrackNo + Environment.NewLine;
                    mailbody = mailbody + "Order Status  : " + OrderStatus + Environment.NewLine;
                    mailbody = mailbody + "Order PO  : " + OrderPO + Environment.NewLine;
                    mailbody = mailbody + "Destination PO/Ref  : " + DestinationPO + Environment.NewLine;
                    mailbody = mailbody + "Additional Reasons for Delay: " + delayReason + Environment.NewLine;
                    mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to review/add reasons for delay to the Order." + Environment.NewLine;
                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - GlobalServe Reasons for Order Delay - (" + CustomerName + ") - " + StrackNo + "-" + OtrackNo + "-" + DtrackNo;
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => ReasonForDelayISDEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => ReasonForDelayISDEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ReasonForDelayISDEmail");
                }
            }

        }

        public string GetOrderStatusCode(string sessionTrackingNo)
        {
            return _dbCon.sp_GetOrderStatus_R(sessionTrackingNo).FirstOrDefault();
        }

        public DispositionQuoteDetail GetAssetDisQuoteDetail(int quoteId)
        {
            DispositionQuoteDetail obj = new DispositionQuoteDetail();

            var data = (from dq in _dbCon.tblDispositionQuotes
                        join dpt in _dbCon.tblDispositionPaymentTypes on dq.DispositionPaymentTypeID equals dpt.DispositionPaymentTypeID
                        where dq.DispositionQuoteID == quoteId
                        select new DispositionQuoteDetail
                        {
                            pickupdate1 = dq.PickupDate,
                            dispositionpaymenttypename = dpt.DispositionPaymentTypeName,
                            locationname = dq.LocationName,
                            address1 = dq.Address1,
                            address2 = dq.Address2,
                            city = dq.City,
                            state = dq.State,
                            zip = dq.ZIP,
                            contactname = dq.ContactName,
                            contactphone = dq.ContactPhone,
                            contactemail = dq.ContactEmail,
                            specialinstructions = dq.SpecialInstructions,
                            quoteId = quoteId
                        }).FirstOrDefault();
            if (data != null && data.pickupdate1 != null)
            {
                data.pickupdate = data.pickupdate1.ToString("MM/dd/yyyy");
            }
            return data;
        }
        #region SFA

        /// <summary>
        /// this method use for updating invoice detail
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="SFAInvoiceDate"></param>
        /// <param name="SFAInvoiceNumber"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public int UpdateInvoiceDetail(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, DateTime SFAInvoiceDate, string SFAInvoiceNumber, int UserId)
        {
            int count = 0;
            try
            {
                var order = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber);
                if (order != null)
                {
                    order.SupplierInvoiceDate = SFAInvoiceDate;
                    order.SupplierInvoiceNumber = SFAInvoiceNumber;

                    _dbCon.Entry(order).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => UpdateInvoiceDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => UpdateInvoiceDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateInvoiceDetail");
                }
            }
            return count;
        }

        /// <summary>
        /// this method use for adding reason for delay supplier
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="delayReason"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int ReasonForDelaySupplier(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string delayReason, string userName)
        {
            int count = 0;
            try
            {
                string reason = string.Empty, BRtmp = string.Empty;

                var tblorder = _dbCon.tblOrders.FirstOrDefault(x => x.SessionTrackingNumber == SessionTrackingNumber && x.OrderTrackingNumber == OrderTrackingNumber);
                if (tblorder != null)
                {
                    if (!string.IsNullOrEmpty(tblorder.ReasonsForDelay))
                    {
                        reason = tblorder.ReasonsForDelay;
                        BRtmp = "<br><br>";
                    }

                    tblorder.ReasonsForDelay = reason + BRtmp + "***** Supplier Reasons for Order " + SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber +
                        " *****<br>***** Placed at " + DateTime.Now + " by " + userName + " *****<br>" + delayReason.Trim() + "<br>*****";

                    _dbCon.Entry(tblorder).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();

                    ReasonForDelaySupplierEmail(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, delayReason, userName);

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => ReasonForDelaySupplier<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => ReasonForDelaySupplier");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ReasonForDelaySupplier");
                }
            }
            return count;
        }

        /// <summary>
        /// this method use for email for adding reason for delay supplier
        /// </summary>
        /// <param name="StrackNo"></param>
        /// <param name="OtrackNo"></param>
        /// <param name="DtrackNo"></param>
        /// <param name="delayReason"></param>
        /// <param name="userName"></param>
        public void ReasonForDelaySupplierEmail(int StrackNo, int OtrackNo, int DtrackNo, string delayReason, string userName)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string OrderedBy = string.Empty;
                string CreatedBy = string.Empty;
                int OrderStatusID = 0;
                string OrderPO = string.Empty;
                string OrderStatus = string.Empty;
                string CustomerName = string.Empty;
                string rsMissing = string.Empty;
                string CustomerEmail = string.Empty;
                string OrderEmail = string.Empty;
                string DestinationPO = string.Empty;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.OrderEmail;
                }
                else
                {
                    MissingEmail = string.Empty;
                }
                var orderAndCustomerDetailForEmail = _dbCon.sp_getOrderAndCustomerDetailForEmail_AdminR(StrackNo, OtrackNo, DtrackNo).FirstOrDefault();
                if (orderAndCustomerDetailForEmail != null)
                {
                    OrderedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderedBy) ? orderAndCustomerDetailForEmail.OrderedBy : string.Empty;
                    CreatedBy = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CreatedBy) ? orderAndCustomerDetailForEmail.CreatedBy : string.Empty;
                    OrderStatusID = orderAndCustomerDetailForEmail.OrderStatusID > 0 ? orderAndCustomerDetailForEmail.OrderStatusID : 0;
                    OrderPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.OrderPO) ? orderAndCustomerDetailForEmail.OrderPO : string.Empty;
                    CustomerName = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerName) ? orderAndCustomerDetailForEmail.CustomerName : string.Empty;
                    CustomerEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.CustomerEmail) ? orderAndCustomerDetailForEmail.CustomerEmail : string.Empty;
                    OrderEmail = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.NetPOrderEmail) ? orderAndCustomerDetailForEmail.NetPOrderEmail : string.Empty;
                    DestinationPO = !string.IsNullOrEmpty(orderAndCustomerDetailForEmail.DestinationPO) ? orderAndCustomerDetailForEmail.DestinationPO : string.Empty;
                }
                else
                {
                    OrderedBy = "";
                    CreatedBy = "";
                    OrderStatusID = 0;
                    OrderPO = "";
                    CustomerName = "";
                    CustomerEmail = "";
                    OrderEmail = "";
                    DestinationPO = "";
                }

                if ((OrderStatusID == 1) || (OrderStatusID == 2) || (OrderStatusID == 3))
                {
                    OrderStatus = "New Order";
                }
                else if ((OrderStatusID == 4))
                {
                    OrderStatus = "GlobalServe Review Required";
                }
                else if ((OrderStatusID == 8))
                {
                    OrderStatus = "Accepted By Supplier";
                }
                else if ((OrderStatusID == 7))
                {
                    OrderStatus = "Shipped Partial";
                }
                else if ((OrderStatusID == 6))
                {
                    OrderStatus = "Shipped Complete";
                }
                else if ((OrderStatusID == 13))
                {
                    OrderStatus = "Cancelled";
                }
                else if ((OrderStatusID == 9))
                {
                    OrderStatus = "Under Review";
                }
                else
                {
                    OrderStatus = "";
                }

                if (!string.IsNullOrEmpty(CustomerEmail))
                {
                    mailfrom = CustomerEmail;
                }
                else
                {
                    mailfrom = MissingEmail;
                }
                mailbcc = mailfrom;

                if (!string.IsNullOrEmpty(OrderedBy))
                {
                    mailcc = OrderedBy;
                }

                if (!string.IsNullOrEmpty(CreatedBy))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + CreatedBy;
                    }
                    else
                    {
                        mailcc = CreatedBy;
                    }
                }

                if (!string.IsNullOrEmpty(OrderEmail))
                {
                    if (!string.IsNullOrEmpty(mailcc))
                    {
                        mailcc = mailcc + ";" + OrderEmail;
                    }
                    else
                    {
                        mailcc = OrderEmail;
                    }
                }

                if (string.IsNullOrEmpty(mailcc))
                {
                    mailcc = MissingEmail;
                }

                var sfaInfo = _dbCon.sp_GetSFAEmailByTrackingNumbers_AdminR(StrackNo, OtrackNo).ToList();

                foreach (var item in sfaInfo)
                {
                    var contactEmails = _dbCon.sp_GetContactEmail_R(StrackNo, item.SFAID).ToList();
                    if (string.IsNullOrEmpty(item.Email))
                    {
                        mailto = "";
                    }
                    else
                    {
                        mailto = item.Email;
                    }
                    foreach (var conEmail in contactEmails)
                    {
                        if (string.IsNullOrEmpty(mailto))
                        {
                            mailto = mailto + conEmail;
                        }
                        else
                        {
                            if (!mailto.Contains(conEmail))
                            {
                                mailto = mailto + ";" + conEmail;
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    mailbody = "GSP: " + item.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Supplier Reasons for Order Delay by: " + userName + Environment.NewLine;
                    mailbody = mailbody + "Tracking #    : " + StrackNo + "-" + OtrackNo + Environment.NewLine;
                    mailbody = mailbody + "Order Status  : " + OrderStatus + Environment.NewLine;
                    mailbody = mailbody + "Order PO  : " + OrderPO + Environment.NewLine;
                    mailbody = mailbody + "Destination PO/Ref  : " + DestinationPO + Environment.NewLine;
                    mailbody = mailbody + "Supplier Reasons for Order Delay: " + delayReason + Environment.NewLine;
                    mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to review/add reasons for delay to the Order." + Environment.NewLine;
                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Supplier Reasons for Order Delay - (" + CustomerName + ") - " + StrackNo + "-" + OtrackNo + "-" + DtrackNo;
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Order Search Presenter => ReasonForDelaySupplierEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => ReasonForDelaySupplierEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ReasonForDelaySupplierEmail");
                }
            }

        }
        #endregion

        public int GetOrderStatusIdByOrderId(int orderId)
        {

            var _dbCon = new globalserveEntities();
            var data = _dbCon.tblOrders.Where(x => x.SessionTrackingNumber == orderId).FirstOrDefault();
            if (data != null)
            {
                return data.OrderStatusID;
            }
            return 0;

        }

        /// <summary>
        /// 10th April
        /// Aurangzeb Khan
        /// This method returns the list of suppliers for a particular order by SessionTrackingNumber
        /// </summary>
        /// <param name="sessionTrackingNo"></param>
        /// <returns></returns>
        public List<GenericList> GetSupplierListByOrder(int sessionTrackingNo)
        {
            var _dbCon = new globalserveEntities();
            var UniqueSFAIds = _dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == sessionTrackingNo).Select(x => x.SFAID).Distinct().ToList();
            List<GenericList> finalResult = new List<GenericList>();

            //finalResult.Add(new GenericList
            //{
            //    Key = 0,
            //    Value = "--Select--",
            //    Count = 1
            //});

            foreach (int SFAId in UniqueSFAIds)
            {
                finalResult.Add(new GenericList
                {
                    Key = SFAId,
                    Value = _dbCon.tblSFAs.Where(x => x.SFAID == SFAId).Select(x => x.SFAName).FirstOrDefault().ToString(),
                    Count = 1
                });
            }
            return finalResult;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Jul 15 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Case Study Attachment data"
    /// </summary>
    public class CaseStudyAttachementPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public CaseStudyAttachementPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// Get all case study attachments records
        /// </summary>
        /// <returns></returns>
        public List<tblCaseStudyAttachement> GetAttachements()
        {
            var caseStudyAttachements = _dbCon.tblCaseStudyAttachements.OrderByDescending(x => x.CreatedDate).ToList();

            caseStudyAttachements.ForEach(x => x.FileName = x.FileName.Split('^')[1]);
            return caseStudyAttachements;
        }

        /// <summary>
        /// delete case study attachment by its ids
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCaseStudyAttachement(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblCaseStudyAttachements.FirstOrDefault(x => x.CaseStudyAttachementId == id);
                if (alert != null)
                {
                    _dbCon.tblCaseStudyAttachements.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        /// <summary>
        /// add case study data to database
        /// </summary>
        /// <param name="attachement"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddCaseStudyAttachement(tblCaseStudyAttachement attachement, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                attachement.CreatedDate = DateTime.Now;
                attachement.CreatedBy = userId;

                _dbCon.tblCaseStudyAttachements.Add(attachement);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// get the file path of attachment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetFilePathById(int id)
        {
            var tblCaseStudyAttachement = _dbCon.tblCaseStudyAttachements.FirstOrDefault(x => x.CaseStudyAttachementId == id);
            if (tblCaseStudyAttachement != null)
                return tblCaseStudyAttachement.FilePath;
            return string.Empty;
        }
    }
}

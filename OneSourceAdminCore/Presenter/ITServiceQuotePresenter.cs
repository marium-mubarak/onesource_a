﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class ITServiceQuotePresenter
    {
        private readonly globalserveEntities _dbCon;
        tblITServiceQuote _itQuote;

        public ITServiceQuotePresenter()
        {
            _dbCon = new Entity.globalserveEntities();
            _itQuote = new tblITServiceQuote();
        }

        #region Properties
        public tblITServiceQuote ServiceQuote
        {
            get { return _itQuote; }
            set { _itQuote = value; }
        }
        #endregion

        #region IT Service Quote

        public int Save()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (_itQuote.ISQuoteId > 0)
                {
                    _dbCon.Entry(_itQuote).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _dbCon.tblITServiceQuotes.Add(_itQuote);
                }

                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;
        }

        public int Delete(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblITServiceQuotes.FirstOrDefault(x => x.ISQuoteId == id);
                if (data != null)
                {
                    _dbCon.tblITServiceQuotes.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public List<tblITServiceQuote> GetItServiceQuote()
        {
            return _dbCon.tblITServiceQuotes.ToList();
        }
        public List<ITServiceQuoteModel> Get()
        {
            var iq = (from I in _dbCon.tblITServiceQuotes
                      join C in _dbCon.tblCountries on I.CountryId equals C.CountryID
                      //join CR in _dbCon.tblCurrencies on I.CurrencyId equals CR.CurrencyID
                      join S in _dbCon.tblServiceTypes on I.ServiceTypeId equals S.ServiceTypeId
                      join SL in _dbCon.tblSLATypes on I.SLAId equals SL.SLATypeId
                      //join R in _dbCon.tblQuoteRateTypes on I.RatesId equals R.RateId
                      //join F in _dbCon.tblSFAs on I.SfaId equals F.SFAID
                      select new ITServiceQuoteModel
                      {
                          Attachment1 = I.Attachment1,
                          Attachment2 = I.Attachment2,
                          Attachment3 = I.Attachment3,
                          Attachment4 = I.Attachment4,
                          Attachment5 = I.Attachment5,
                          City = I.City,
                          Comments = I.Comments,
                          Country = C.CountryName,
                          CountryId = I.CountryId,
                          CreatedBy = I.CreatedBy,
                          CreatedDate = I.CreatedDate,
                          //Currency = CR.CurrencyName,
                          CurrencyId = I.CurrencyId,
                          CustomerName = I.CustomerName,
                          EngineerAvailabilityId = I.EngineerAvailabilityId,
                          EquipmentDetails = I.EquipmentDetails,
                          InvoiceAddress = I.InvoiceAddress,
                          InvoiceContact = I.InvoiceContact,
                          ISQuoteId = I.ISQuoteId,
                          NoofResources = I.NoofResources,
                          ProjectExpectedDuration = I.ProjectExpectedDuration,
                          ProjectName = I.ProjectName,
                          ProjectOverView = I.ProjectOverView,
                          ProjectStartDate = I.ProjectStartDate,
                          RatesId = I.RatesId,
                          RatesPoint = I.RatesPoint,
                          //RateType = R.ReteType,
                          ResponseDate = I.ResponseDate,
                          ResponsedBy = I.ResponsedBy,
                          ResponseId = I.ResponseId,
                          ScopeofWork = I.ScopeofWork,
                          ServiceType = S.ServiceTypeName,
                          ServiceTypeId = I.ServiceTypeId,
                          SiteAddress = I.SiteAddress,
                          SiteContact = I.SiteContact,
                          SLAId = I.SLAId,
                          SLAType = SL.SLAName,
                          AccountManagerName = I.AccountManagerName,
                          SfaId = I.SfaId,
                          LineItems = _dbCon.tblQuoteLineItems.Where(x => x.QuoteId == I.ISQuoteId && x.QuoteTypeId == 4).Count()
                          //Supplier = F.SFAName,
                          //SupplierEmail = F.Email
                      }).ToList();

            return iq;
        }

        #endregion

        #region Options

        public List<tblSLAType> GetSLA()
        {
            return _dbCon.tblSLATypes.ToList();
        }

        public List<tblQuoteRateType> GetRates()
        {
            return _dbCon.tblQuoteRateTypes.ToList();
        }

        public List<tblServiceType> GetServiceType()
        {
            return _dbCon.tblServiceTypes.ToList();
        }


        #endregion
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : feb 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for providing support service maintenance"
    /// </summary>
    public class SupportServiceMaintenancePresenter
    {
        private readonly globalserveEntities _dbCon;
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>


        public SupportServiceMaintenancePresenter()
        {
            _dbCon = new globalserveEntities();
        }
        #region service

        /// <summary>
        ///  this method is responsible for providing service
        /// </summary>
        /// <returns></returns>
        public List<SSM_ServiceModel> GetServices()
        {
            //var data = _dbCon.tblServices.GroupBy(x => x.ParentServiceID, (key, g) => new
            //{
            //    ParentServiceID = key,
            //    List = g.ToList()
            //}).ToList();
            var data = _dbCon.tblServices.Where(x => x.ParentServiceID == null && x.IsActive == true).Select(x => new { x.ServiceID, x.ServiceName }).OrderBy(x => x.ServiceName).ToList();
            List<SSM_ServiceModel> ssm = new List<SSM_ServiceModel>();
            SSM_ServiceModel obj;
            foreach (var item in data)
            {
                obj = new SSM_ServiceModel();
                obj.serviceInfo = new Entity.tblService();
                obj.serviceInfo.ServiceName = item.ServiceName;
                obj.serviceInfo.ServiceID = item.ServiceID;
                var subServices = _dbCon.tblServices.Where(x => x.ParentServiceID == item.ServiceID).OrderBy(x => x.ServiceName).ToList();
                obj.lstService = subServices;
                ssm.Add(obj);
            }

            return ssm;

        }

        /// <summary>
        ///  this method is responsible for providing specific service 
        /// </summary>
        /// <param name="SId"></param>
        /// <returns></returns>
        public SSM_ServiceModel GetServiceInfo(int SId)
        {
            SSM_ServiceModel obj = new SSM_ServiceModel();
            obj.lstService = _dbCon.tblServices.Where(x => x.ParentServiceID == null).OrderBy(x => x.ServiceName).ToList();
            obj.lstService.Insert(0, new tblService { ServiceID = 0, ServiceName = "(None)" });
            obj.serviceInfo = new Entity.tblService();
            if (SId > 0)
            {
                obj.serviceInfo = _dbCon.tblServices.FirstOrDefault(x => x.ServiceID == SId);
                if (obj.serviceInfo != null)
                {
                    var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == obj.serviceInfo.LastUpdatedByUserID);
                    if (user != null)
                    {
                        obj.UserName = user.UserName;
                    }
                    if (obj.lstService.Count > 0 && obj.serviceInfo.ParentServiceID == null)
                    {
                        var existingparent = obj.lstService.FirstOrDefault(x => x.ServiceID == obj.serviceInfo.ServiceID);
                        if (existingparent != null)
                        {
                            obj.lstService.Remove(existingparent);
                        }
                    }
                }


            }
            return obj;

        }

        /// <summary>
        ///  this method is responsible for add edit service
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object AddUpdateService(tblService model, int userId)
        {
            int count = 0;
            string msg = string.Empty;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    if (!string.IsNullOrEmpty(model.ServiceName))
                    {
                        var ExistingRecord = _dbCon.tblServices.FirstOrDefault(x => x.ServiceName.ToLower() == model.ServiceName.ToLower());
                        if (ExistingRecord != null)
                        {
                            if (model.ServiceID != ExistingRecord.ServiceID)
                            {
                                return new
                                {
                                    count = 0,
                                    msg = "A Service with this name is already exists"
                                };
                            }
                            ((IObjectContextAdapter)_dbCon).ObjectContext.Detach(ExistingRecord);
                        }

                    }

                    model.LastUpdated = DateTime.Now;
                    model.LastUpdatedByUserID = userId;
                    model.ParentServiceID = model.ParentServiceID == 0 ? null : model.ParentServiceID;
                    if (model.ServiceID > 0)
                    {
                        _dbCon.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Service Updated Successfully" : "Error Occured.";
                    }
                    else
                    {
                        model.IsActive = true;
                        _dbCon.tblServices.Add(model);
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Service Added Successfully" : "Error Occured.";
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateService");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateService");
                }
            }
            return new
            {
                count = count,
                msg = msg
            };
        }
        #endregion

        #region Request Type/Ticket Status

        /// <summary>
        /// this method is responsible for action request ticket status
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="editType"></param>
        /// <returns></returns>
        public ActionRequestTicketStatusModel GetActionRequestTicketStatusModel(string companyId, string editType)
        {
            ActionRequestTicketStatusModel obj = new Model.ActionRequestTicketStatusModel();
            CompanyPresenter cp = new Presenter.CompanyPresenter();
            obj.Companies = cp.GetCompanyByParentCompanyAndRoleName("GlobalServe", "Customer");
            obj.lstActionRequests = new List<sp_getActionRequests_AdminR_Result>();
            obj.lstTicketStatus = new List<sp_getTicketStatus_AdminR_Result>();

            obj.Companies.Insert(0, new Entity.sp_getCompanyByParentCompanyAndRoleName_AdminR_Result { ParentCompanyID = 0, ParentCompanyName = "Select a Company" });

            if (!string.IsNullOrEmpty(companyId) && editType == "1")
            {
                if (companyId == "0")
                    obj.lstActionRequests = _dbCon.sp_getActionRequests_AdminR(string.Empty).ToList();
                else
                    obj.lstActionRequests = _dbCon.sp_getActionRequests_AdminR(companyId).ToList();
            }
            else if (!string.IsNullOrEmpty(companyId) && editType == "2")
            {
                if (companyId == "0")
                    obj.lstTicketStatus = _dbCon.sp_getTicketStatus_AdminR(string.Empty, string.Empty).ToList();
                else
                    obj.lstTicketStatus = _dbCon.sp_getTicketStatus_AdminR(companyId, string.Empty).ToList();
            }
            else if (string.IsNullOrEmpty(companyId) && string.IsNullOrEmpty(editType))
            {
                obj.lstActionRequests = _dbCon.sp_getActionRequests_AdminR(string.Empty).ToList();
            }
            return obj;
        }

        /// <summary>
        /// this method is responsible for providing ticket status model for modal
        /// </summary>
        /// <param name="TktStatusID"></param>
        /// <returns></returns>
        public TicketStatusModalModel GetTicketStatusModal(int? TktStatusID)
        {
            TicketStatusModalModel obj = new Model.TicketStatusModalModel();
            obj.ticketStatus = new Entity.tblTicketStatu();

            if (TktStatusID > 0)
            {
                var ticketStatus = _dbCon.tblTicketStatus.FirstOrDefault(x => x.TktStatusID == TktStatusID);
                obj.ticketStatus = ticketStatus != null ? ticketStatus : new Entity.tblTicketStatu();

                var tblXRefTicketStatus = _dbCon.tblXRefTicketStatus.FirstOrDefault(x => x.TicketStatusID == TktStatusID);
                obj.XRefStatusCode = tblXRefTicketStatus != null ? tblXRefTicketStatus.XRefStatusCode : "";
                obj.XRefStatusName = tblXRefTicketStatus != null ? tblXRefTicketStatus.XRefStatusName : "";

                if (ticketStatus != null)
                {
                    var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == ticketStatus.LastUpdateUserID);
                    obj.UserName = user != null ? user.UserName : "";
                }
            }
            return obj;
        }

        /// <summary>
        ///  this method is responsible for providing action request model for modal
        /// </summary>
        /// <param name="artypeId"></param>
        /// <returns></returns>
        public object GetActionRequestModal(int? artypeId)
        {
            tblActionRequestType obj = new Entity.tblActionRequestType();
            string userName = string.Empty;
            if (artypeId > 0)
            {
                obj = _dbCon.tblActionRequestTypes.FirstOrDefault(x => x.ARTypeID == artypeId);
                if (obj == null) obj = new Entity.tblActionRequestType();
                else
                {
                    var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == obj.LastUpdateUserID);
                    userName = user != null ? user.UserName : string.Empty;
                }
            }
            return new { userName = userName, actionRequestType = obj };
        }

        /// <summary>
        ///  this method is responsible for action detail model
        /// </summary>
        /// <param name="artypeId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionRequestDetailModel GetActionDetailModel(int artypeId, int companyId)
        {
            ActionRequestDetailModel obj = new Model.ActionRequestDetailModel();
            obj.ActionRequest = new Entity.tblActionRequestType();
            obj.TicketStatus = new List<Entity.sp_getTicketStatus_AdminR_Result>();
            obj.CompanyId = companyId;
            if (artypeId > 0)
            {
                obj.ActionRequest = _dbCon.tblActionRequestTypes.FirstOrDefault(x => x.ARTypeID == artypeId);
                if (obj.ActionRequest != null)
                {
                    var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == obj.ActionRequest.LastUpdateUserID);
                    if (user != null)
                    {
                        obj.UserName = user.UserName;
                    }
                }
                obj.TicketStatus = _dbCon.sp_getTicketStatus_AdminR(companyId == 0 ? string.Empty : Convert.ToString(companyId), artypeId.ToString()).ToList();
            }
            return obj;
        }

        /// <summary>
        /// this method is responsible for add edit action request type
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object AddEditActionRequestType(tblActionRequestType model, int userId)
        {
            int count = 0;
            string msg = string.Empty;
            model.CompanyID = model.CompanyID == 0 ? null : model.CompanyID;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    if (!string.IsNullOrEmpty(model.ARTypeName))
                    {
                        var ExistingRecord = _dbCon.tblActionRequestTypes.FirstOrDefault(x => x.ARTypeName.ToLower() == model.ARTypeName.ToLower());
                        if (ExistingRecord != null)
                        {
                            if (model.ARTypeID != ExistingRecord.ARTypeID)
                            {
                                return new
                                {
                                    count = 0,
                                    msg = "A Action Request Type with this name is already exists"
                                };
                            }
                            ((IObjectContextAdapter)_dbCon).ObjectContext.Detach(ExistingRecord);
                        }

                    }

                    model.LastUpdate = DateTime.Now;
                    model.LastUpdateUserID = userId;
                    if (model.ARTypeID > 0)
                    {
                        _dbCon.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Action Request Type Updated Successfully" : "Error Occured.";
                    }
                    else
                    {
                        model.IsActive = true;
                        _dbCon.tblActionRequestTypes.Add(model);
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Action Request Type Added Successfully" : "Error Occured.";
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddEditActionRequestType");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddEditActionRequestType");
                }
            }
            return new
            {
                count = count,
                msg = msg
            };
        }

        /// <summary>
        /// this method is responsible for add edit ticket status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="XRefStatusCode"></param>
        /// <param name="XRefStatusName"></param>
        /// <param name="companyId"></param>
        /// <param name="artypeID"></param>
        /// <param name="isActive"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object AddEditTicketStatus(tblTicketStatu model, string XRefStatusCode, string XRefStatusName, int? companyId, int? artypeID, bool isActive, int userId)
        {
            int count = 0;
            string msg = string.Empty;
            model.CompanyID = companyId == 0 ? null : companyId;
            model.ARTypeID = artypeID;
            model.IsActive = isActive;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    if (!string.IsNullOrEmpty(model.TktStatusName))
                    {
                        var ExistingRecord = _dbCon.tblTicketStatus.FirstOrDefault(x => x.TktStatusName.ToLower() == model.TktStatusName.ToLower());
                        if (ExistingRecord != null)
                        {
                            if (model.TktStatusID != ExistingRecord.TktStatusID)
                            {
                                return new
                                {
                                    count = 0,
                                    msg = "A ticket status with this name is already exists"
                                };
                            }
                            ((IObjectContextAdapter)_dbCon).ObjectContext.Detach(ExistingRecord);
                        }

                    }

                    model.LastUpdate = DateTime.Now;
                    model.LastUpdateUserID = userId;
                    if (model.TktStatusID > 0)
                    {
                        _dbCon.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Ticket status Updated Successfully" : "Error Occured.";

                        if (!string.IsNullOrEmpty(XRefStatusCode) && !string.IsNullOrEmpty(XRefStatusName) && model.CompanyID != null)
                        {
                            tblXRefTicketStatu xrefTicketStatus = _dbCon.tblXRefTicketStatus.FirstOrDefault(x => x.TicketStatusID == model.TktStatusID);
                            if (xrefTicketStatus == null)
                            {
                                xrefTicketStatus = new tblXRefTicketStatu();
                                xrefTicketStatus.TicketStatusID = model.TktStatusID;
                                xrefTicketStatus.XRefStatusName = XRefStatusName;
                                xrefTicketStatus.XRefStatusCode = XRefStatusCode;
                                xrefTicketStatus.CompanyID = Convert.ToInt32(model.CompanyID);
                                xrefTicketStatus.ARTypeID = artypeID;
                                xrefTicketStatus.IsActive = true;
                                xrefTicketStatus.LastUpdated = DateTime.Now;
                                xrefTicketStatus.LastUpdatedByUserID = userId;
                                _dbCon.tblXRefTicketStatus.Add(xrefTicketStatus);
                                _dbCon.SaveChanges();
                            }
                            else
                            {
                                xrefTicketStatus.XRefStatusName = XRefStatusName;
                                xrefTicketStatus.XRefStatusCode = XRefStatusCode;
                                xrefTicketStatus.LastUpdated = DateTime.Now;
                                xrefTicketStatus.LastUpdatedByUserID = userId;
                                _dbCon.Entry(xrefTicketStatus).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                        }

                    }
                    else
                    {
                        model.IsActive = true;
                        _dbCon.tblTicketStatus.Add(model);
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Ticket status Added Successfully" : "Error Occured.";

                        if (!string.IsNullOrEmpty(XRefStatusCode) && !string.IsNullOrEmpty(XRefStatusName) && model.CompanyID != null)
                        {
                            tblXRefTicketStatu xrefTicketStatus = new tblXRefTicketStatu();
                            xrefTicketStatus.TicketStatusID = model.TktStatusID;
                            xrefTicketStatus.XRefStatusName = XRefStatusName;
                            xrefTicketStatus.XRefStatusCode = XRefStatusCode;
                            xrefTicketStatus.CompanyID = Convert.ToInt32(model.CompanyID);
                            xrefTicketStatus.ARTypeID = artypeID;
                            xrefTicketStatus.IsActive = true;
                            xrefTicketStatus.LastUpdated = DateTime.Now;
                            xrefTicketStatus.LastUpdatedByUserID = userId;
                            _dbCon.tblXRefTicketStatus.Add(xrefTicketStatus);
                            _dbCon.SaveChanges();
                        }

                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddEditActionRequestType");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddEditActionRequestType");
                }
            }
            return new
            {
                count = count,
                msg = msg
            };
        }
        #endregion
    }
}

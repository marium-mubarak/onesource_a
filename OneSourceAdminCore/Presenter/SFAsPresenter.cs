﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : jan 27 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for making sfa uplifts"
    /// </summary>
    public class SFAsPresenter
    {
        private globalserveEntities _dbCon;

        public SFAsPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public tblOneStreetUplift CheckOneStreetUpliftExist(int OneStreetUpliftId)
        {
            var existing = _dbCon.tblOneStreetUplifts
                                .FirstOrDefault(x => x.OneStreetUpliftID == OneStreetUpliftId);

            return existing;
        }
        /// <summary>
        /// this method is responsible for providing one street uplift view
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public OneStreetSFAsUpliftsModel GetSFAModel(int? supplierId, int? countryId, int customerId = 0)
        {
            OneStreetSFAsUpliftsModel obj = new Model.OneStreetSFAsUpliftsModel();
            CountryPresenter cp = new Presenter.CountryPresenter();

            if (supplierId == null)
            {
                supplierId = 0;
            }

            obj.Countries = cp.GetCountries();
            obj.SFAsSuppliers =
                _dbCon.tblSFAs.Where(x => x.IsOneStreetSupplier == true && x.IsActive == true)
                    .Select(x => new GenericList
                    {
                        Key = x.SFAID,
                        Value = x.SFAName
                    }).ToList();

            if (supplierId != null && supplierId > 0 && countryId != null && countryId > 0 && customerId > 0) //added customerId (SUNNY GOHAR)
            {
                var tblOneStreetUplift = OneStreetUpliftInfo(Convert.ToInt32(supplierId), Convert.ToInt32(countryId), customerId);
                if (tblOneStreetUplift != null)
                {
                    obj.OneStreetUplift = tblOneStreetUplift.OneStreetUpliftID.ConvertObjectToInt32();
                    obj.GSPUplift = tblOneStreetUplift.GSPUplift;
                    obj.GSUplift = tblOneStreetUplift.GSUplift;
                    obj.Country = Convert.ToInt32(tblOneStreetUplift.Country);
                    obj.SupplierID = Convert.ToInt32(tblOneStreetUplift.SupplierID);
                    obj.CustomerId = Convert.ToInt32(tblOneStreetUplift.CustomerId);
                }
            }

            if (supplierId != null || customerId > 0)
            {
                obj.supplierDetail = _dbCon.sp_getOneStreetSFAsUpliftsInfo_AdminR(supplierId.ToString(), customerId.ToString()).ToList(); //added customerId (SUNNY GOHAR)
                obj.supplierDetail.ForEach(x =>
                {
                    x.GSPUplift = Convert.ToDecimal(x.GSPUplift).ToString("G29");
                    x.GSUplift = Convert.ToDecimal(x.GSUplift).ToString("G29");
                });
            }


            //added (SUNNY GOHAR)
            obj.Customers = GetOneStreetCustomers();

            return obj;
        }

        /// <summary>
        /// this method is responsible for providing add edit view for uplifts
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int AddSFAsUplift(tblOneStreetUplift obj, AdminSetting currentSettings)
        {
            try
            {
                int count = 0;

                if (obj != null)
                {
                    if (obj.GSPUplift != null)
                    {
                        obj.GSPUplift = obj.GSPUplift / 100;
                    }

                    if (obj.GSUplift != null)
                    {
                        obj.GSUplift = obj.GSUplift / 100;
                    }

                    if (obj.OneStreetUpliftID > 0)
                    {
                        var chkExisting =
                      _dbCon.tblOneStreetUplifts.FirstOrDefault(x => x.OneStreetUpliftID == obj.OneStreetUpliftID);
                        if (chkExisting != null)//updated  
                        {
                            chkExisting.CustomerId = obj.CustomerId;
                            chkExisting.SupplierID = obj.SupplierID;
                            chkExisting.Country = obj.Country;
                            chkExisting.UpdatedDate = DateTime.Now;
                            chkExisting.UpdatedBy = currentSettings.UserID;
                            chkExisting.GSPUplift = obj.GSPUplift;
                            chkExisting.GSUplift = obj.GSUplift;
                            _dbCon.Entry(chkExisting).State = System.Data.Entity.EntityState.Modified;

                        }
                    }
                    else
                    {
                        var newUplifts = new tblOneStreetUplift
                        {
                            CustomerId = obj.CustomerId,
                            SupplierID = obj.SupplierID,
                            Country = obj.Country,
                            UpdatedBy = currentSettings.UserID,
                            GSPUplift = obj.GSPUplift,
                            GSUplift = obj.GSUplift,
                            UpdatedDate = DateTime.Now
                        };

                        _dbCon.tblOneStreetUplifts.Add(newUplifts);

                    }
                    count = _dbCon.SaveChanges();


                }

                if (obj != null)
                {
                    var strLog =
                        $"{currentSettings.UserName} has updated {obj.SupplierID} supplier's settings against customer id {obj.CustomerId}";

                    StaticHelper.LogException(strLog, "AddSFAsUplift -- change Log");
                }

                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddSFAsUplift");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSFAsUplift");
                }

                return 0;
            }

        }

        /// <summary>
        /// this action is responsible for providing uplift info
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public tblOneStreetUplift OneStreetUpliftInfo(int supplierId, int countryId, int customerId) //added customerId (SUNNY GOHAR)
        {
            var data =
                _dbCon.tblOneStreetUplifts //added customerId (SUNNY GOHAR)
                    .FirstOrDefault(x => x.SupplierID == supplierId && x.Country == countryId && x.CustomerId == customerId);

            if (data != null)
            {
                if (data.GSUplift != null)
                {
                    data.GSUplift = data.GSUplift * 100;
                }
                if (data.GSPUplift != null)
                {
                    data.GSPUplift = data.GSPUplift * 100;
                }
            }

            return data;
        }

        public tblOneStreetUplift GetSfaSettingsById(int settingId) //added customerId (SUNNY GOHAR)
        {
            var data =
                _dbCon.tblOneStreetUplifts
                    .FirstOrDefault(x => x.OneStreetUpliftID == settingId);

            return data;
        }

        public List<tblSFA> GetSFAs()
        {
            return _dbCon.tblSFAs.ToList();
        }
        public List<GenericList> GetIsActiveSFAs()
        {
            return _dbCon.tblSFAs.Where(x => x.IsActive == true).Select(x => new GenericList { Value = x.SFAName, Key = (int)x.ParentCompanyID }).ToList();
        }
        public List<tblSFA> GetActiveSuppliers()
        {

            var sfaList = _dbCon.tblSFAs.Where(x => x.IsActive).ToList();
            return sfaList;
        }

        public List<tblSfaDetailName> GetSfaDetailNameList()
        {
            var sfaList = _dbCon.tblSfaDetailNames.ToList();
            return sfaList;
        }

        public tblSfaDetailName GetSfaNameDetailsBySfaId(int sfaId)
        {
            var sfaDetail = _dbCon.tblSfaDetailNames.FirstOrDefault(x => x.SfaID == sfaId);
            return sfaDetail;
        }

        public bool UpdateSfaDetails(int sfaId, string sfaName)
        {
            try
            {
                if (sfaId > 0 && !string.IsNullOrWhiteSpace(sfaName))
                {
                    var chkExisting = _dbCon.tblSfaDetailNames.FirstOrDefault(x => x.SfaID == sfaId);
                    if (chkExisting != null)
                    {
                        _dbCon.tblSfaDetailNames.Remove(chkExisting);
                    }

                    var getSfaById = _dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == sfaId);
                    if (getSfaById == null)
                    {
                        return false;
                    }

                    var addNew = new tblSfaDetailName
                    {
                        SfaDetailName = sfaName,
                        SfaID = sfaId,
                        SfaName = getSfaById.SFAName
                    };

                    _dbCon.tblSfaDetailNames.Add(addNew);
                    _dbCon.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSfaDetails");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSfaDetails");
                }
                return false;
            }
        }

        public List<SelectListItem> GetOneStreetCustomers()
        {
            //var cust = new CustomerPresenter();

            //    var customers = cust.GetCustomersWithPriority();

            var query = (from c in _dbCon.tblCustomers
                         join osu in _dbCon.tblOneStreetUsers on c.CustomerID equals osu.CustomerID
                         where c.IsActive && osu.OnstreetAccess
                         select c).ToList()
                .Distinct();

            var oneStreetCustomers = query.Select(x => new SelectListItem
            {
                Value = x.CustomerID.ToString(),
                Text = x.CustomerName.ToString()

            }).ToList();

            return oneStreetCustomers;
        }

        public List<GenericList> GetSuppliersList()//Added by Adnan on 10-7-2019 for Invoiced Order Detail Report
        {
            var sfaList = _dbCon.tblSFAs.Where(x => x.IsActive).Select(x => new GenericList
            {
                Key = x.SFAID,
                Value = x.SFAName
            }).ToList();
            return sfaList;
        }

        public List<GenericList> GetSFAs(int customerId, int countryId)
        {
            if (customerId == 245)//Mars
            {
                var MarsSFAs = (from sfap in _dbCon.tblSFAPreferreds
                                join cd in _dbCon.tblCustomerDestinations on sfap.CustomerDestinationID equals cd.CustomerDestinationID
                                join ca in _dbCon.tblCustomerAddresses on cd.CustomerDestinationID equals ca.CustomerAddressID
                                join sfa in _dbCon.tblSFAs on sfap.SFAID equals sfa.SFAID
                                join sdn in _dbCon.tblSfaDetailNames on sfap.SFAID equals sdn.SfaID into t
                                from nt in t.DefaultIfEmpty()
                                where cd.IsActive == 1 && ca.IsActive == 1 && ca.CustomerID == customerId
                                && sfa.IsActive == true && sfa.IsComposite == true && sfa.ParentCompanyID != null && sfa.SFAID != 1
                                && sfap.IsPreferred == true && sfap.IsActive == 1
                                select new GenericList { Key = sfap.SFAID, Value = sfa.SFAName != null ? sfa.SFAName : nt.SfaDetailName }).Distinct().ToList();
                return MarsSFAs;
            }
            else
            {
                var SFAs = (from sfap in _dbCon.tblSFAPreferreds
                            join cd in _dbCon.tblCustomerDestinations on sfap.CustomerDestinationID equals cd.CustomerDestinationID
                            join ca in _dbCon.tblCustomerAddresses on cd.CustomerDestinationID equals ca.CustomerAddressID
                            join sfa in _dbCon.tblSFAs on sfap.SFAID equals sfa.SFAID
                            where cd.IsActive == 1 && ca.IsActive == 1 && ca.CustomerID == customerId
                            && sfa.IsActive == true && sfa.IsComposite == true && sfa.ParentCompanyID != null && sfa.SFAID != 1
                            && sfap.IsPreferred == true && sfap.IsActive == 1 && ca.CountryID == countryId
                            select new GenericList { Key = sfap.SFAID, Value = sfa.SFAName }).Distinct().ToList();
                return SFAs;
            }


        }
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static OneSourceAdminCore.Model.SMBQuotesModel;

namespace OneSourceAdminCore.Presenter
{
    public class PriceDeskReportsPresenter
    {
        /// <summary>
        /// authenticate user by given username and password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        globalserveEntities dbCon;
        public PriceDeskReportsPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public List<QuotesBySuppiler> GetSupplierQuotes(List<int> ids, int quoteTypeId,string status)
        {
            try
            {
                if (quoteTypeId == 1)
                {
                    var query = (from h in dbCon.tblHardwareQuotes.Where(x => ids.Contains(x.HardwareQuoteId))
                                 join C in dbCon.tblCountries on h.CountryId equals C.CountryID
                                 join SF in dbCon.tblSFAs on h.SfaId equals SF.SFAID into SFA
                                 from SF in SFA.DefaultIfEmpty()
                                 join Q in dbCon.tblQuoteResponseStatus.Where(x => x.QuoteTypeId == quoteTypeId) on h.HardwareQuoteId equals Q.QuoteId into QS
                                 from Q in QS.DefaultIfEmpty()
                                 select new
                                 {
                                     QuoteId = h.HardwareQuoteId,
                                     AccountManagerName = h.AccountManagerName,
                                     AccountManagerType = h.AccountManagerType == null ? "-" : h.AccountManagerType,
                                     CustomerName = h.CustomerName,
                                     CountryId = h.CountryId,
                                     City = h.City,
                                     Status = status=="Cancelled"?status:(Q == null ? "" : Q.Status),
                                     CreatedDate = h.CreatedDate,

                                 }).OrderByDescending(x=>x.QuoteId).ToList();

                    var list = query.Select(s => new QuotesBySuppiler
                    {
                        QuoteId = s.QuoteId,
                        AccountManagerName = s.AccountManagerName == null ? "-" : s.AccountManagerName,
                        QuoteType = "Hardware Quote",
                        AccountManagerType = s.AccountManagerType == null ? "-" : s.AccountManagerType,
                        CustomerName = s.CustomerName == null ? "-" : s.CustomerName,
                        Country = dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault() != null ? dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault().CountryName : "-",
                        City = s.City == null ? "-" : s.City,
                        Status = s == null ? "" : s.Status,
                        Date = s.CreatedDate != null ? s.CreatedDate.Value.ToShortDateString() : "-",

                    }).ToList();


                    return list;
                }
                else if (quoteTypeId == 2)
                {
                    var query = (from s in dbCon.tblSoftwareQuotes.Where(x => ids.Contains(x.SoftwareQuoteId))
                                 join C in dbCon.tblCountries on s.CountryId equals C.CountryID
                                 join SF in dbCon.tblSFAs on s.SfaId equals SF.SFAID into SFA
                                 from SF in SFA.DefaultIfEmpty()
                                 join Q in dbCon.tblQuoteResponseStatus.Where(x => x.QuoteTypeId == quoteTypeId) on s.SoftwareQuoteId equals Q.QuoteId into QS
                                 from Q in QS.DefaultIfEmpty()
                                 select new
                                 {
                                     QuoteId = s.SoftwareQuoteId,
                                     AccountManagerName = s.AccountManagerName,
                                     AccountManagerType = s.AccountManagerType == null ? "-" : s.AccountManagerType,
                                     CustomerName = s.CustomerName,
                                     CountryId = s.CountryId,
                                     City = s.City,
                                     Status = status == "Cancelled" ? status : (Q == null ? "" : Q.Status),
                                     CreatedDate = s.CreatedDate,

                                 }).OrderByDescending(x => x.QuoteId).ToList();

                    var list = query.Select(s => new QuotesBySuppiler
                    {
                        QuoteId = s.QuoteId,
                        QuoteType = "Software Quote",
                        AccountManagerName = s.AccountManagerName == null ? "-" : s.AccountManagerName,
                        AccountManagerType = s.AccountManagerType == null ? "-" : s.AccountManagerType,
                        CustomerName = s.CustomerName == null ? "-" : s.CustomerName,
                        Country = dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault() != null ? dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault().CountryName : "-",
                        City = s.City == null ? "-" : s.City,
                        Status = s == null ? "" : s.Status,
                        Date = s.CreatedDate != null ? s.CreatedDate.Value.ToShortDateString() : "-",

                    }).ToList();

                    return list;
                }
                else if (quoteTypeId == 3)
                {
                    var query = (from a in dbCon.tblAssetDisposalQuotes.Where(x => ids.Contains(x.AssertDisposalId))
                                 join C in dbCon.tblCountries on a.CountryId equals C.CountryID
                                 join SF in dbCon.tblSFAs on a.SfaId equals SF.SFAID into SFA
                                 from SF in SFA.DefaultIfEmpty()
                                 join Q in dbCon.tblQuoteResponseStatus.Where(x => x.QuoteTypeId == quoteTypeId) on a.AssertDisposalId equals Q.QuoteId into QS
                                 from Q in QS.DefaultIfEmpty()
                                 select new
                                 {
                                     QuoteId = a.AssertDisposalId,
                                     AccountManagerName = a.AccountManagerName,
                                     AccountManagerType = a.AccountManagerType == null ? "-" : a.AccountManagerType,
                                     CustomerName = a.CustomerName,
                                     CountryId = a.CountryId,
                                     City = a.City,
                                     Status = status == "Cancelled" ? status : (Q == null ? "" : Q.Status),
                                     CreatedDate = a.CreatedDate,

                                 }).OrderByDescending(x => x.QuoteId).ToList();

                    var list = query.Select(s => new QuotesBySuppiler
                    {
                        QuoteId = s.QuoteId,
                        QuoteType = "Asset Disposal Quote",
                        AccountManagerName = s.AccountManagerName == null ? "-" : s.AccountManagerName,
                        AccountManagerType = s.AccountManagerType == null ? "-" : s.AccountManagerType,
                        CustomerName = s.CustomerName == null ? "-" : s.CustomerName,
                        Country = dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault() != null ? dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault().CountryName : "-",
                        City = s.City == null ? "-" : s.City,
                        Status = s == null ? "" : s.Status,
                        Date = s.CreatedDate != null ? s.CreatedDate.Value.ToShortDateString() : "-",

                    }).ToList();

                    return list;
                }
                else if (quoteTypeId == 4)
                {
                    var query = (from it in dbCon.tblITServiceQuotes.Where(x => ids.Contains(x.ISQuoteId))
                                 join C in dbCon.tblCountries on it.CountryId equals C.CountryID
                                 join SF in dbCon.tblSFAs on it.SfaId equals SF.SFAID into SFA
                                 from SF in SFA.DefaultIfEmpty()
                                 join Q in dbCon.tblQuoteResponseStatus.Where(x => x.QuoteTypeId == quoteTypeId) on it.ISQuoteId equals Q.QuoteId into QS
                                 from Q in QS.DefaultIfEmpty()
                                 select new
                                 {
                                     QuoteId = it.ISQuoteId,
                                     AccountManagerName = it.AccountManagerName,
                                     AccountManagerType = it.AccountManagerType == null ? "-" : it.AccountManagerType,
                                     CustomerName = it.CustomerName,
                                     CountryId = it.CountryId,
                                     City = it.City,
                                     Status = status == "Cancelled" ? status : (Q == null ? "" : Q.Status),
                                     CreatedDate = it.CreatedDate,

                                 }).OrderByDescending(x => x.QuoteId).ToList();

                    var list = query.Select(s => new QuotesBySuppiler
                    {
                        QuoteId = s.QuoteId,
                        QuoteType = "IT Service Quote",
                        AccountManagerName = s.AccountManagerName == null ? "-" : s.AccountManagerName,
                        AccountManagerType = s.AccountManagerType == null ? "-" : s.AccountManagerType,
                        CustomerName = s.CustomerName == null ? "-" : s.CustomerName,
                        Country = dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault() != null ? dbCon.tblCountries.Where(x => x.CountryID == s.CountryId).FirstOrDefault().CountryName : "-",
                        City = s.City == null ? "-" : s.City,
                        Status = s == null ? "" : s.Status,
                        Date = s.CreatedDate != null ? s.CreatedDate.Value.ToShortDateString() : "-",

                    }).ToList();

                    return list;
                }

                return new List<QuotesBySuppiler>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetProcurementReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProcurementReportData");
                }
                return new List<QuotesBySuppiler>();
            }
        }


        public DataTable GetPriceDeskReportData(string reportName, PriceDeskReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_PriceDeskReportMain_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@SupplierID", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@QuoteTypeId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@QuoteStatus", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@AccountManagerId", SqlDbType.Int));
                    command.Parameters[0].Value = reportName;
                    command.Parameters[1].Value = model.StartDate;
                    command.Parameters[2].Value = model.EndDate;
                    command.Parameters[3].Value = model.SupplierId;
                    command.Parameters[4].Value = model.QuoteTypeId;
                    command.Parameters[5].Value = model.Status;
                    command.Parameters[6].Value = model.AccountManagerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                if(reportName=="Quotes By Status Report")
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["Last Updated"] = !String.IsNullOrEmpty(dr["Last Updated"].ToString()) ? Convert.ToDateTime(dr["Last Updated"].ToString()).ToShortDateString() : "-";
                        dr["Payment Terms"] = !String.IsNullOrEmpty(dr["Payment Terms"].ToString()) ? dr["Payment Terms"].ToString() : "-";
                        dr["Account Manager"] = !String.IsNullOrEmpty(dr["Account Manager"].ToString()) ? dr["Account Manager"].ToString() : "-";
                        dr["Pending Start"] = !String.IsNullOrEmpty(dr["Pending Start"].ToString()) ? Convert.ToDateTime(dr["Pending Start"].ToString()).ToShortDateString() : "-";
                        dr["Pending End"] = !String.IsNullOrEmpty(dr["Pending End"].ToString()) ? Convert.ToDateTime(dr["Pending End"].ToString()).ToShortDateString() : "-";
                        dr["Quote Reference"] = !String.IsNullOrEmpty(dr["Quote Reference"].ToString()) ? dr["Quote Reference"].ToString() : "-";
                        dr["Details"] = !String.IsNullOrEmpty(dr["Details"].ToString()) ? dr["Details"].ToString() : "-";
                        dr["Delivery Charges"] = !String.IsNullOrEmpty(dr["Delivery Charges"].ToString()) ? dr["Delivery Charges"] : 0;
                        dr["Currency"] = !String.IsNullOrEmpty(dr["Currency"].ToString()) ? dr["Currency"].ToString() : "-";
                        dr["Special Instructions"] = !String.IsNullOrEmpty(dr["Special Instructions"].ToString()) ? dr["Special Instructions"].ToString() : "-";
                        dr["Updated By User"] = !String.IsNullOrEmpty(dr["Updated By User"].ToString()) ? dr["Updated By User"].ToString() : "-";
                        if (model.Status == "Cancelled")
                        {
                            dr["Status"] = "Cancelled";
                        }
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetProcurementReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProcurementReportData");
                }
            }

            return null;
        }

        public List<QuoteResponseItems> GetQuoteResponseLineItems(string id, int quoteTypeId)
        {
            try
            {
                var quoteId = !String.IsNullOrEmpty(id) ? Convert.ToInt32(id) : 0;

                if (quoteTypeId == 1) // Hardware Quotes
                {
                    var query = (from h in dbCon.tblHardwareQuotes.Where(x => x.HardwareQuoteId == quoteId)
                                 from q in dbCon.tblResponseQuotes.Where(x => x.QuoteId == quoteId && x.QuoteType == quoteTypeId)
                                 join r in dbCon.tblResponseQuoteLineItems on q.ResponseQuoteId equals r.ResponseQuoteId
                                 select new
                                 {
                                     Id = h.HardwareQuoteId,
                                     Description = r.Description,
                                     Manufacturer = dbCon.tblManufacturers.FirstOrDefault(x=>x.ManufacturerID==r.ManufacturerId) != null ?
                                     dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == r.ManufacturerId).ManufacturerName : "-",
                                     Sku = r.SKU,
                                     Category = dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == r.CategoryId) != null ?
                                     dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == r.CategoryId).PartCategoryName : "-",
                                     LeadTime = r.LeadTime,
                                     EOL = r.EOL,
                                     Quantity = r.Quantity,
                                     UnitPrice = r.UnitCostPrice.ToString()
                                     //UnitPrice = dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == q.CurrencyId) != null ?
                                     //dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == a.CurrencyId).Symbol + " " +
                                     //r.UnitCostPrice : "" + " " +
                                     //r.UnitCostPrice

                                 }).OrderByDescending(x=>x.Id).ToList();

                    var list = query.Select(s => new QuoteResponseItems
                    {
                        Description = s.Description!=null?s.Description:"-",
                        Manufacturer = s.Manufacturer,
                        Sku = s.Sku,
                        Category = s.Category,
                        LeadTime = s.LeadTime,
                        EOL = s.EOL != null ? s.EOL.Value.ToShortDateString() : "-",
                        Quantity = s.Quantity.ToString(),
                        UnitPrice = s.UnitPrice
                    }).ToList();

                    return list;
                }
                else if (quoteTypeId == 2) // Software Quotes
                {
                    var query = (from s in dbCon.tblSoftwareQuotes.Where(x => x.SoftwareQuoteId == quoteId)
                                 from q in dbCon.tblResponseQuotes.Where(x => x.QuoteId == quoteId && x.QuoteType == quoteTypeId)
                                 join r in dbCon.tblResponseQuoteLineItems on q.ResponseQuoteId equals r.ResponseQuoteId
                                 select new
                                 {
                                     Id = s.SoftwareQuoteId,
                                     Description = r.Description,
                                     Manufacturer = dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == r.ManufacturerId) != null ?
                                     dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == r.ManufacturerId).ManufacturerName : "-",
                                     Sku = r.SKU,
                                     SerialNumber = r.SerailNumber,
                                     Category = dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == r.CategoryId) != null ?
                                     dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == r.CategoryId).PartCategoryName : "-",
                                     LeadTime = r.LeadTime,
                                     EOL = r.EOL,
                                     Quantity = r.Quantity,
                                     UnitPrice = r.UnitCostPrice.ToString()
                                     //UnitPrice = dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == q.CurrencyId) != null ?
                                     //dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == a.CurrencyId).Symbol + " " +
                                     //r.UnitCostPrice : "" + " " +
                                     //r.UnitCostPrice

                                 }).OrderByDescending(x => x.Id).ToList();

                    var list = query.Select(s => new QuoteResponseItems
                    {
                        Description = s.Description != null ? s.Description : "-",
                        Manufacturer = s.Manufacturer,
                        Sku = s.Sku,
                        SerialNumber = s.SerialNumber,
                        Category = s.Category,
                        LeadTime = s.LeadTime,
                        EOL = s.EOL != null ? s.EOL.Value.ToShortDateString() : "-",
                        Quantity = s.Quantity.ToString(),
                        UnitPrice = s.UnitPrice
                    }).ToList();

                    return list;
                }
                else if (quoteTypeId == 3) // Asset Disposal Quotes
                {
                    var query = (from a in dbCon.tblAssetDisposalQuotes.Where(x => x.AssertDisposalId == quoteId)
                                 from q in dbCon.tblResponseQuotes.Where(x => x.QuoteId == quoteId && x.QuoteType == quoteTypeId)
                                 join r in dbCon.tblResponseQuoteLineItems on q.ResponseQuoteId equals r.ResponseQuoteId
                                 select new
                                 {
                                     Id = a.AssertDisposalId,
                                     Description = r.Description,
                                     Category = dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == r.CategoryId) != null ?
                                     dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == r.CategoryId).PartCategoryName : "-",
                                     LeadTime = r.LeadTime,
                                     EOL = r.EOL,
                                     Quantity = r.Quantity,
                                     UnitPrice = r.UnitCostPrice.ToString()
                                     //UnitPrice = dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == q.CurrencyId) != null ?
                                     //dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == a.CurrencyId).Symbol + " " +
                                     //r.UnitCostPrice : "" + " " +
                                     //r.UnitCostPrice

                                 }).OrderByDescending(x => x.Id).ToList();

                    var list = query.Select(s => new QuoteResponseItems
                    {
                        Description = s.Description != null ? s.Description : "-",
                        Category = s.Category,
                        LeadTime = s.LeadTime,
                        EOL = s.EOL != null ? s.EOL.Value.ToShortDateString() : "-",
                        Quantity = s.Quantity.ToString(),
                        UnitPrice = s.UnitPrice
                    }).ToList();

                    return list;
                }
                else // IT Service Quotes
                {
                    var query = (from i in dbCon.tblITServiceQuotes.Where(x => x.ISQuoteId == quoteId)
                                 from q in dbCon.tblResponseQuotes.Where(x => x.QuoteId == quoteId && x.QuoteType == quoteTypeId)
                                 join r in dbCon.tblResponseQuoteLineItems on q.ResponseQuoteId equals r.ResponseQuoteId
                                 select new
                                 {
                                     Id = i.ISQuoteId,
                                     ServiceType = dbCon.tblServiceTypes.FirstOrDefault(x => x.ServiceTypeId == r.ServiceTypeId)!=null?
                                     dbCon.tblServiceTypes.FirstOrDefault(x => x.ServiceTypeId == r.ServiceTypeId).ServiceTypeName:"-",
                                     SLAType = dbCon.tblSLATypes.FirstOrDefault(x=>x.SLATypeId == r.SLATypeId)!=null?
                                     dbCon.tblSLATypes.FirstOrDefault(x => x.SLATypeId == r.SLATypeId).SLAName:"-",
                                     Resources = r.NoOfResources,
                                     HourlyRate = r.HourlyRate,
                                     TravelCharges = r.TravelCharge
                                 }).OrderByDescending(x => x.Id).ToList();

                    var list = query.Select(s => new QuoteResponseItems
                    {
                        ServiceType = s.ServiceType,
                        SLAType = s.SLAType,
                        Resources = s.Resources.ToString(),
                        HourlyRate = s.HourlyRate.HasValue ? s.HourlyRate.Value.ToString() : "-",
                        TravelCharges = s.TravelCharges.HasValue ? s.TravelCharges.Value.ToString() : "-"
                    }).ToList();

                    return list;
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetQuoteResponseLineItems");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteResponseLineItems");
                }
                return new List<QuoteResponseItems>();
            }
        }

        public DataTable GetSupplierQuoteDetailsToExport(DataTable dt, int quoteTypeId)
        {
            try
            {

                if(dt != null && dt.Rows.Count > 0) //main quotes table
                {

                    var newdt = new DataTable();
                    if (quoteTypeId == 1) //Hardware Quotes
                    {
                        newdt.Columns.Add("Quote #");
                        newdt.Columns.Add("Quote Type");
                        newdt.Columns.Add("Description");
                        newdt.Columns.Add("Manufacturer");
                        newdt.Columns.Add("SKU");
                        newdt.Columns.Add("Category");
                        newdt.Columns.Add("Lead Time");
                        newdt.Columns.Add("EOL");
                        newdt.Columns.Add("Quantity");
                        newdt.Columns.Add("Unit Price");

                    }
                    else if (quoteTypeId == 2) //Software Quotes
                    {
                        newdt.Columns.Add("Quote #");
                        newdt.Columns.Add("Quote Type");
                        newdt.Columns.Add("Description");
                        newdt.Columns.Add("Manufacturer");
                        newdt.Columns.Add("SKU");
                        newdt.Columns.Add("Serial Number");
                        newdt.Columns.Add("Category");
                        newdt.Columns.Add("Lead Time");
                        newdt.Columns.Add("EOL");
                        newdt.Columns.Add("Quantity");
                        newdt.Columns.Add("Unit Price");
                    }
                    else if (quoteTypeId == 3) //Asset Quotes
                    {
                        newdt.Columns.Add("Quote #");
                        newdt.Columns.Add("Quote Type");
                        newdt.Columns.Add("Description");
                        newdt.Columns.Add("Category");
                        newdt.Columns.Add("Lead Time");
                        newdt.Columns.Add("EOL");
                        newdt.Columns.Add("Quantity");
                        newdt.Columns.Add("Unit Price");
                    }
                    else if (quoteTypeId == 4) //IT Quotes
                    {
                        newdt.Columns.Add("Quote #");
                        newdt.Columns.Add("Quote Type");
                        newdt.Columns.Add("Service Type");
                        newdt.Columns.Add("SLA Type");
                        newdt.Columns.Add("Resources");
                        newdt.Columns.Add("Hourly Rate");
                        newdt.Columns.Add("Travel Charges");
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var itemsList = GetQuoteResponseLineItems(dt.Rows[i][0].ToString(), quoteTypeId);
                        foreach( var item in itemsList)
                        {
                            var row = newdt.NewRow();
                            if (quoteTypeId == 1) // Hardware quotes
                            {
                                row["Quote #"] = dt.Rows[i]["QuoteId"].ToString();
                                row["Quote Type"] ="Hardware Quote";
                                row["Description"] = item.Description;
                                row["Manufacturer"] = item.Manufacturer;
                                row["SKU"] = item.Sku;
                                row["Category"] = item.Category;
                                row["Lead Time"] = item.LeadTime;
                                row["EOL"] = item.EOL;
                                row["Quantity"] = item.Quantity;
                                row["Unit Price"] = item.UnitPrice;
                            }
                            else if (quoteTypeId == 2) //Software Quotes
                            {
                                row["Quote #"] = dt.Rows[i]["QuoteId"].ToString();
                                row["Quote Type"] = "Software Quote";
                                row["Description"] = item.Description;
                                row["Manufacturer"] = item.Manufacturer;
                                row["SKU"] = item.Sku;
                                row["Serial Number"] = item.SerialNumber;
                                row["Category"] = item.Category;
                                row["Lead Time"] = item.LeadTime;
                                row["EOL"] = item.EOL;
                                row["Quantity"] = item.Quantity;
                                row["Unit Price"] = item.UnitPrice;
                            }
                            else if (quoteTypeId == 3) //Asset Quotes
                            {
                                row["Quote #"] = dt.Rows[i]["QuoteId"].ToString();
                                row["Quote Type"] = "Asset Disposal Quote";
                                row["Description"] = item.Description;
                                row["Category"] = item.Category;
                                row["Lead Time"] = item.LeadTime;
                                row["EOL"] = item.EOL;
                                row["Quantity"] = item.Quantity;
                                row["Unit Price"] = item.UnitPrice;
                            }
                            else if (quoteTypeId == 4) //IT Quotes
                            {
                                row["Quote #"] = dt.Rows[i]["QuoteId"].ToString();
                                row["Quote Type"] = "IT Service Quote";
                                row["Service Type"] = item.ServiceType;
                                row["SLA Type"] = item.SLAType;
                                row["Resources"] = item.Resources;
                                row["Hourly Rate"] = item.HourlyRate;
                                row["Travel Charges"] = item.TravelCharges;
                            }
                            newdt.Rows.Add(row);
                        }
                    }

                    return newdt;
                }

                return dt;
            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetQuoteResponseLineItems");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetQuoteResponseLineItems");
                }
                return new DataTable();
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (prop.Name == "SessionTrackingNumber")
                {
                    dataTable.Columns.Add(prop.Name, typeof(string));
                }
                else
                {
                    dataTable.Columns.Add(prop.Name, type);
                }

            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        public List<SMBUsers> GetUserWithInfo()
        {
            var _user = (from U in dbCon.tblUsers
                         join C in dbCon.tblContacts on U.UserID equals C.UserID
                         select new SMBUsers { UserName = C.FirstName + " " + C.LastName, UserID = U.UserID, UserGroupID = U.UserGroupID }).ToList();

            return _user.ToList();
        }

    }

}

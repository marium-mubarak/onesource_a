﻿using System.Linq;
using OneSourceAdminCore.Entity;
using System.Collections.Generic;
using OneSourceAdminCore.Model;
using System;
using System.Data;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Web.Mvc;
using OneSourceAdminCore.Helper;

namespace OneSourceAdminCore.Presenter
{
    public class AssetReportsPresenter
    {
        /// <summary>
        /// authenticate user by given username and password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        globalserveEntities dbCon;
        public AssetReportsPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public int GetCompanyIdByCustomerId(int customerId)
        {
            try
            {
                var id = 0;

                var cust = dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerId);
                if (cust != null)
                {
                    id = cust.ParentCompanyID;
                }

                return id;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyIdByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyIdByCustomerId");
                }
                return 0;
            }
        }

        public List<GSNetP_tbl_AddrWithAssets> GetLocationsByCustomerId(int customerId)
        {
            try
            {
                var list = dbCon.GSNetP_tbl_AddrWithAssets.ToList();
                var addresses = dbCon.GSNetP_tbl_AddrWithAssets.Where(x => x.CustomerID == customerId).Distinct().ToList();

                return addresses;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetLocationsByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetLocationsByCustomerId");
                }
                return null;
            }
        }

        public DataTable GetAssetReportData(AssetReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_AssetReportMain_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@LocationIDS", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CountryIDS", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@ManufacturerIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@orderby", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@orderdir", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@issummary", SqlDbType.Int));
                    command.Parameters[0].Value = model.ReportName;
                    var customerId = model.CompanyId;
                    if(model.ReportName =="Asset Depreciation Summary")
                    {
                        customerId = model.CustomerId;
                    }
                    command.Parameters[1].Value = customerId;
                    command.Parameters[2].Value = model.AddressIDs;
                    command.Parameters[3].Value = model.CountryIDs;
                    command.Parameters[4].Value = model.ManufacturerIDs;
                    command.Parameters[7].Value = model.StartDate;
                    command.Parameters[8].Value = model.EndDate;
                    command.Parameters[9].Value = 1;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetReportData");
                }
                return null;
            }
        }

        public DataTable GetAssetsByType(AssetReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ISDAssetByType_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@customerid", SqlDbType.Int));
                    command.Parameters[0].Value = model.CustomerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetsByType");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetsByType");
                }
                return null;
            }
        }

        public List<AssetManufacturerClass> GetManufactures()
        {
            return dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new AssetManufacturerClass { ManufacturerId = x.ManufacturerID, ManufacturerName = x.ManufacturerName }).Distinct().OrderBy(x => x.ManufacturerId).ToList();

        }

        public Object GetReportDataToExport(AssetReportModel model, DataRow[] list, bool? assetType)
        {
            try
            {
                string name = model.ReportName.Trim().ToLower().Replace(" ", "");
                model.routeName = name;
                object objData = "";
                List<object> dataList = new List<object>();
                var distinct = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[0].ToString()))
                                                .Select(a => a.ItemArray[0].ToString()).Distinct().ToArray();
                if (model.routeName == "assetsbylocation")
                {
                    //var yaxis = list.Select(x => x.ItemArray[5].ToString()).ToArray();
                    //var xaxis = list.Select(x => x.ItemArray[0].ToString()).Distinct().ToArray();

                    //var distinctAddresses = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[0].ToString()))
                    //                            .Select(a => a.ItemArray[0].ToString()).Distinct().ToArray();
                    var distinctManufacturers = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[2].ToString()))
                                                    .Select(m => m.ItemArray[2].ToString()).Distinct().ToArray();

                    var quantity = 0;

                    foreach (var manufacturer in distinctManufacturers)
                    {
                        List<int> quantities = new List<int>();

                        for (int i = 0; i < distinct.Count(); i++)
                        {
                            quantity = list.Where(x => x.ItemArray[2].ToString() == manufacturer &&
                                                       x.ItemArray[0].ToString() == distinct[i].ToString()).Select(x => Convert.ToInt32(x.ItemArray[5].ToString())).FirstOrDefault();

                            quantities.Add(quantity);
                        }

                        if (quantities.Count() > 0)
                        {
                            var data = new
                            {
                                name = manufacturer,
                                data = quantities.ToArray()
                            };

                            dataList.Add(data);
                        }
                    }

                    //var obj = new
                    //{
                    //    data = dataList.Distinct().ToArray(),
                    //    categories = distinct

                    //};

                    //objData = obj;
                    //if (dataList.Count <= 0)
                    //{
                    //    objData = "";
                    //}              
                }
                else if (model.routeName == "assetsbycountry")
                {
                    //var yaxis = list.Select(x => x.ItemArray[0].ToString()).ToArray();

                    //var distinctCountries = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[0].ToString()))
                    //                            .Select(a => a.ItemArray[0].ToString()).Distinct().ToArray();
                    var distinctManufacturers = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[1].ToString()))
                                                    .Select(m => m.ItemArray[1].ToString()).Distinct().ToArray();

                    var quantity = 0;

                    foreach (var manufacturer in distinctManufacturers)
                    {
                        List<int> quantities = new List<int>();

                        for (int i = 0; i < distinct.Count(); i++)
                        {
                            quantity = list.Where(x => x.ItemArray[1].ToString() == manufacturer && x.ItemArray[0].ToString() == distinct[i].ToString()).Select(x => Convert.ToInt32(x.ItemArray[4].ToString())).FirstOrDefault();

                            quantities.Add(quantity);
                        }

                        if (quantities.Count() > 0)
                        {
                            var data = new
                            {
                                name = manufacturer,
                                data = quantities.ToArray()
                            };

                            dataList.Add(data);
                        }
                    }

                    //var obj = new
                    //{
                    //    data = dataList.Distinct().ToArray(),
                    //    categories = distinct

                    //};

                    //objData = obj;
                    //if (dataList.Count <= 0)
                    //{
                    //    objData = "";
                    //}
                }
                else if (model.routeName == "assetsbymanufacturer")
                {
                    //var yaxis = list.Select(x => x.ItemArray[0].ToString()).ToArray();

                    //var distinctManufacturers = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[0].ToString())).Select(m => m.ItemArray[0].ToString()).Distinct().ToArray();

                    foreach (var manufacturer in distinct)
                    {
                        var quantity = 0;

                        int[] quantities = new int[distinct.Count()];

                        var listquantities = list.Where(x => x.ItemArray[0].ToString() == manufacturer).Select(x => Convert.ToInt32(x.ItemArray[3].ToString())).ToArray();

                        foreach (var item in listquantities)
                        {
                            quantity += item;
                        }

                        for (int i = 0; i < distinct.Count(); i++)
                        {
                            if (distinct[i].ToString() == manufacturer)
                            {
                                quantities[i] = quantity;
                            }
                            else
                            {
                                quantities[i] = 0;
                            }

                        }
                        var data = new
                        {
                            name = manufacturer,
                            data = quantities.ToArray()
                        };

                        dataList.Add(data);
                    }

                }
                else if (model.routeName == "assetsbystandard")
                {
                    foreach (var standard in distinct)
                    {
                        var quantity = 0;

                        int[] quantities = new int[distinct.Count()];

                        var listquantities = list.Where(x => x.ItemArray[0].ToString() == standard).Select(x => Convert.ToInt32(x.ItemArray[1].ToString())).ToArray();

                        foreach (var item in listquantities)
                        {
                            quantity += item;
                        }

                        for (int i = 0; i < distinct.Count(); i++)
                        {
                            if (distinct[i].ToString() == standard)
                                quantities[i] = quantity;
                            else
                                quantities[i] = 0;

                        }
                        var data = new
                        {
                            name = standard,
                            data = quantities.ToArray()
                        };

                        dataList.Add(data);
                    }
                }
                else if (model.routeName == "assetsbystatus")
                {
                    if (assetType == true)
                    {
                        var assetTypeData = GetAssetsByType(model);

                        var assetTypes = assetTypeData.Select().Distinct().ToArray();

                        var types = assetTypes.Select(x => x.ItemArray[0].ToString()).Distinct().ToArray();

                        foreach (var type in types)
                        {
                            var count = assetTypes.Where(x => x.ItemArray[0].ToString() == type).Count();

                            var data = new
                            {
                                name = type,
                                y = count
                            };
                            dataList.Add(data);
                        }
                        var objNew = new
                        {
                            data = dataList.Distinct().ToArray(),
                            name = "Total Assets"

                        };
                        List<object> objList = new List<object>();
                        objList.Add(objNew);
                        objData = objNew;
                        if (dataList.Count <= 0)
                        {
                            objData = "";
                        }

                        return objList.ToArray();
                    }
                    else
                    {
                        foreach (var status in distinct)
                        {
                            var quantity = 0;

                            int[] quantities = new int[distinct.Count()];

                            var listquantities = list.Where(x => x.ItemArray[0].ToString() == status).Select(x => Convert.ToInt32(x.ItemArray[1].ToString())).ToArray();

                            foreach (var item in listquantities)
                            {
                                quantity += item;
                            }

                            for (int i = 0; i < distinct.Count(); i++)
                            {
                                if (distinct[i].ToString() == status)
                                    quantities[i] = quantity;
                                else
                                    quantities[i] = 0;
                            }
                            var data = new
                            {
                                name = status,
                                data = quantities.ToArray()
                            };

                            dataList.Add(data);
                        }
                    }
                }
                if (model.routeName == "assetdepreciationsummary")
                {
                    var dataValues = list.Select(x => new
                    {
                        Year = Convert.ToDateTime(x.ItemArray[7].ToString()).Year,
                        Depreciation = Convert.ToDouble(x.ItemArray[13])
                    });

                    var distinctYears = dataValues.Select(x => x.Year).Distinct().ToList();

                    foreach (var year in distinctYears)
                    {
                        var value = 0.0;

                        object[] values = new object[distinctYears.Count()];

                        var listvalues = dataValues.Where(x => x.Year == year).Select(x => x.Depreciation).ToArray();

                        foreach (var item in listvalues)
                        {
                            value += Math.Round(item, 2);
                        }

                        for (int i = 0; i < distinctYears.Count(); i++)
                        {
                            if (distinctYears[i] == year)
                            {
                                values[i] = new
                                {
                                    name = year,
                                    y = value,
                                    drilldown = year
                                };
                            }
                            else
                            {
                                values[i] = new
                                {
                                    name = year,
                                    y = 0.00,
                                    drilldown = year
                                };
                            }
                        }
                        var data = new
                        {
                            name = year,
                            data = values.ToArray()
                        };

                        dataList.Add(data);
                    }

                    var objNew = new
                    {
                        data = dataList.Distinct().ToArray(),
                        categories = distinctYears
                    };

                    objData = objNew;
                    if (dataList.Count <= 0)
                    {
                        objData = "";
                    }

                    return objNew;
                }

                var obj = new
                {
                    data = dataList.Distinct().ToArray(),
                    categories = distinct

                };

                objData = obj;
                if (dataList.Count <= 0)
                {
                    objData = "";
                }

                return objData;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportDataToExport");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportDataToExport");
                }
                return null;
            }
        }
    }
}

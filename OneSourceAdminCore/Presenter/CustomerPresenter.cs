﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class CustomerPresenter
    {
        private readonly globalserveEntities _dbCon;
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public CustomerPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public List<SelectListItem> GetMyCustomers()
        {
            return _dbCon.tblCustomers.Where(x => x.IsActive == true).Select(x => new SelectListItem
            {
                Text = x.CustomerName,
                Value = x.CustomerID.ToString()
            }).OrderBy(x => x.Value).ToList();
        }
        /// <summary>
        /// give list of customers
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetCustomers()
        {
            var customers = _dbCon.tblCustomers.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Key = x.CustomerID,
                Value = x.CustomerName
            }).OrderBy(x => x.Value).ToList();
            return customers;
        }

        public List<CustomerCompanyClass> GetCustomersWithPriority()
        {
            var customers = _dbCon.tblCustomers.Where(x => x.IsActive == true && x.CustomerOf == null).Select(x => new CustomerCompanyClass
            {
                CustomerId = x.CustomerID,
                CustomerName = x.CustomerName,
                ParentCompanyId = x.ParentCompanyID

            }).OrderBy(x => x.CustomerName).ToList();
            return customers;
        }

        public List<int> GetCustomerWithParentCompanyId(int parentcompanyId, int customerId)//Added by adnan for YTD report on 1-7-2019
        {
            var customers = new List<int>();
            var parentcompany = _dbCon.tblParentCompanies.Where(x => x.ParentParentCompanyID == parentcompanyId).Select(x => x.ParentCompanyID).ToList();
            if (parentcompany != null && parentcompany.Count > 0)
            {
                 customers = _dbCon.tblCustomers.Where(x => parentcompany.Contains(x.ParentCompanyID) && x.CustomerID != customerId).Select(x => x.CustomerID).ToList();


            }
            customers.Add(customerId);
                
            
            return customers;
        }
        /// <summary>
        /// give list of customers alert
        /// </summary>
        /// <returns></returns>
        public List<CustomCustomerAlert> GetCustomerAlert()
        {
            var customerAlert = _dbCon.tblCustomerAlerts.Select(x => new CustomCustomerAlert
            {
                CustomerAlertId = x.CustomerAlertId,
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName,
                Alert = x.Alert,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsTopStory = x.IsTopStory

            }).OrderBy(x => x.CustomerName).ToList();

            customerAlert.ForEach(x =>
            {
                x.strStartDate = x.StartDate.Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.EndDate != null ? Convert.ToDateTime(x.EndDate).Date.ToString("MM/dd/yyy") : string.Empty);

            });
            return customerAlert;
        }
        /// <summary>
        /// add customers alert to the database
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddCustomerAlert(tblCustomerAlert alert, int userId)
        {
            int count;

            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                alert.CreatedDate = DateTime.Now;
                alert.CreatedBy = userId;
                alert.CustomerName = string.IsNullOrEmpty(alert.CustomerName) == true ? string.Empty : alert.CustomerName;

                _dbCon.tblCustomerAlerts.Add(alert);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// edit customers alert in the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditCustomerAlert(tblCustomerAlert model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCustomerAlert alert =
                    _dbCon.tblCustomerAlerts.FirstOrDefault(x => x.CustomerAlertId == model.CustomerAlertId);
                if (alert != null)
                {
                    alert.Alert = model.Alert;
                    alert.CustomerId = model.CustomerId;
                    alert.CustomerName = model.CustomerName;
                    alert.StartDate = model.StartDate;
                    alert.EndDate = model.EndDate;
                    alert.UpdatedBy = userId;
                    alert.UpdatedDate = DateTime.Now;
                    alert.IsTopStory = model.IsTopStory;
                }
                _dbCon.Entry(alert).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// delete customers alert from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCustomerAlert(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblCustomerAlerts.FirstOrDefault(x => x.CustomerAlertId == id);
                if (alert != null)
                {
                    _dbCon.tblCustomerAlerts.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }


        public List<GenericList> GetCustomerUsers(int customerid)
        {

            var list = (from cu in _dbCon.tblCustomerUsers
                        join u in _dbCon.tblUsers on cu.UserID equals u.UserID
                        where cu.CustomerID == customerid
                        select new GenericList { Value = u.UserName, Key = cu.UserID }).ToList();

            return list;
        }
    }
    public class CustomerCompanyClass
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int ParentCompanyId { get; set; }
    }
}
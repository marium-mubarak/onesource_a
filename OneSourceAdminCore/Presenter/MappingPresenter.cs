﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OneSourceAdminCore
{
    /// <summary>   
    /// Date created : OCT 28 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of manufacturers,categories,products mapping"
    /// </summary>
    public class MappingPresenter
    {
        globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public MappingPresenter()
        {
            _dbCon = new Entity.globalserveEntities();
        }

        /// <summary>
        /// Get unmapped and mapped one street category mapping details
        /// </summary>
        /// <param name="oneSourceCategoryId"></param>
        /// <param name="allMapped"></param>
        /// <returns></returns>
        public OneStreetCategoryMappingDetailsModel GetOneStreetCategoryMappingDetails(int oneSourceCategoryId, bool allMapped)
        {
            var obj = new OneStreetCategoryMappingDetailsModel();
            obj.Categories = _dbCon.tblPartCategories.Where(x => x.IsActive == true).OrderBy(x => x.PartCategoryName).ToList();
            obj.MappingDetails = _dbCon.Sp_OneStreetCategoryMappingDetails_AdminR(oneSourceCategoryId, allMapped).ToList();
            return obj;
        }


        /// <summary>
        /// Retrieves unmapped and mapped categories details by categoryid
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public List<Sp_OneStreetCategoryMappingDetails_AdminR_Result> GetCategoriesMappingDetailByOSCategoryId(int categoryId)
        {
            var data = _dbCon.Sp_OneStreetCategoryMappingDetails_AdminR(categoryId, false).ToList();
            return data;
        }


        /// <summary>
        /// Get unmapped and mapped one street manufacturer mapping details
        /// </summary>
        /// <param name="oneSourceManufacturerId"></param>
        /// <param name="allMapped"></param>
        /// <returns></returns>
        public OneStreetManufacturerMappingDetailsModel GetOneStreetManufacturerMappingDetails(int oneSourceManufacturerId, bool allMapped)
        {
            var obj = new OneStreetManufacturerMappingDetailsModel();
            obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).OrderBy(x => x.ManufacturerName).ToList();
            obj.MappingDetails = _dbCon.Sp_OneStreetManufacturerMappingDetails_AdminR(oneSourceManufacturerId, allMapped).ToList();
            return obj;
        }


        /// <summary>
        /// Retrieves unmapped and mapped manufacturer details by manufacturerId
        /// </summary>
        /// <param name="manufacturerId"></param>
        /// <returns></returns>
        public List<Sp_OneStreetManufacturerMappingDetails_AdminR_Result> GetManufacturersMappingDetailByOSCategoryId(int manufacturerId)
        {
            var data = _dbCon.Sp_OneStreetManufacturerMappingDetails_AdminR(manufacturerId, false).ToList();
            return data;
        }


        /// <summary>
        /// Get upmapped categories from onestreet and all active categories from onesource in order to map
        /// </summary>
        /// <returns></returns>
        public CategoryMappingModel GetMapUnmapCategories()
        {
            var obj = new CategoryMappingModel();
            obj.Categories = _dbCon.tblPartCategories.Where(x => x.IsActive == true).OrderBy(x => x.PartCategoryName).ToList();
            obj.UnmappedCategories = _dbCon.sp_OneStreet_GetUnmappedCategories().OrderBy(x => x.TreeClassClassificationName).ToList();
            obj.MappingDetail = _dbCon.sp_OneStreet_GetCategoryMappingDetail().OrderBy(x => x.PartCategoryName).ToList();
            return obj;
        }

        /// <summary>
        /// Get upmapped manufacturers from onestreet and all active manufacturers from onesource in order to map
        /// </summary>
        /// <returns></returns>
        public ManufacturerMappingModel GetMapUnmapManufacturer()
        {
            var obj = new ManufacturerMappingModel();
            obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).OrderBy(x => x.ManufacturerName).ToList();
            obj.UnmappedManufacturer = _dbCon.sp_OneStreet_GetUnmappedManufacturers().OrderBy(x => x.ManufacturerName).ToList();
            obj.MappingDetail = _dbCon.sp_OneStreet_GetManufacturerMappingDetail().OrderBy(x => x.OnesourceMan).ToList();
            return obj;
        }

        /// <summary>
        /// update the mapping of categories between onestreet and onesource
        /// </summary>
        /// <param name="onesourceCat"></param>
        /// <param name="onestreetCatArr"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateCategoryMapping(string onesourceCat, string[] onestreetCatArr, int userId)
        {
            int count = 0;

            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                int onesourceCatID = Convert.ToInt32(onesourceCat);
                var data = _dbCon.tblOneStreetCategoryMappings.Where(x => x.OSCategoryID == onesourceCatID).ToList();
                _dbCon.tblOneStreetCategoryMappings.RemoveRange(data);
                count = _dbCon.SaveChanges();

                // Only Add categories if Select All wasn't selected
                if (onestreetCatArr != null && onestreetCatArr.All(cat => cat != "0"))
                {
                    foreach (var item in onestreetCatArr)
                    {
                        int oneStreetCategoryID = Convert.ToInt32(item);

                        var obj = new tblOneStreetCategoryMapping();
                        obj.OSCategoryID = onesourceCatID;
                        obj.OneStreetCategoryID = oneStreetCategoryID;
                        obj.LastUpdated = DateTime.Now;
                        obj.LastUpdatedBy = userId;
                        _dbCon.tblOneStreetCategoryMappings.Add(obj);
                        count = _dbCon.SaveChanges();
                    }
                }

                transaction.Commit();
            }

            return count;
        }

        /// <summary>
        /// update the mapping of manufacturers between onestreet and onesource
        /// </summary>
        /// <param name="onesourceMan"></param>
        /// <param name="onestreetManArr"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateManufacturerMapping(string onesourceMan, string[] onestreetManArr, int userId)
        {
            int count = 0;

            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                int onesourceManID = Convert.ToInt32(onesourceMan);

                var data = _dbCon.tblOneStreetManufacturersMappings.Where(x => x.OSManufacturerID == onesourceManID).ToList();
                _dbCon.tblOneStreetManufacturersMappings.RemoveRange(data);
                count = _dbCon.SaveChanges();

                // Only Add manufacturers if Select All wasn't selected
                if (onestreetManArr != null && onestreetManArr.All(cat => cat != "0"))
                {
                    foreach (var item in onestreetManArr)
                    {
                        int oneStreetManufacturerId = Convert.ToInt32(item);

                        var obj = new tblOneStreetManufacturersMapping();
                        obj.OneStreetManufacturerID = oneStreetManufacturerId;
                        obj.OSManufacturerID = onesourceManID;
                        obj.LastUpdated = DateTime.Now;
                        obj.LastUpdatedByUser = userId;
                        _dbCon.tblOneStreetManufacturersMappings.Add(obj);
                        count = _dbCon.SaveChanges();
                    }
                }

                transaction.Commit();
            }

            return count;
        }

        /// <summary>
        /// delete the manufacturer mapping by the onesource and onestreet id
        /// </summary>
        /// <param name="osrManId"></param>
        /// <param name="ostManId"></param>
        /// <returns></returns>
        public int DeleteManufacturerMapping(int osrManId, int ostManId)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblOneStreetManufacturersMappings.FirstOrDefault(x => x.OneStreetManufacturerID == ostManId && x.OSManufacturerID == osrManId);
                if (data != null)
                {
                    _dbCon.tblOneStreetManufacturersMappings.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        /// <summary>
        /// delete the Category mapping by the onesource and onestreet id
        /// </summary>
        /// <param name="osrCatId"></param>
        /// <param name="ostCatId"></param>
        /// <returns></returns>
        public int DeleteCategoryMapping(int osrCatId, int ostCatId)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblOneStreetCategoryMappings.FirstOrDefault(x => x.OneStreetCategoryID == ostCatId && x.OSCategoryID == osrCatId);
                if (data != null)
                {
                    _dbCon.tblOneStreetCategoryMappings.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }
    }
}

﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System.Diagnostics;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for managing companies"
    /// </summary>
    public class CompanyPresenter
    {
        private readonly globalserveEntities _dbCon;

        public CompanyPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// this method is use for getting companies
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="searchText"></param>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public List<sp_getCompanies_AdminR_Result> GetCompanies(int roleId, string searchText,
            int parentCompanyId)
        {
            try
            {
                return _dbCon.sp_getCompanies_AdminR(roleId, searchText, parentCompanyId.ToString()).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanies");
                }
                return new List<sp_getCompanies_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method is use for getting company model
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <param name="parentCompanyName"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public CompanyModel GetCompanyModel(int parentCompanyId, string parentCompanyName, int partyId)
        {
            try
            {
                CompanyModel obj = new Model.CompanyModel();
                PrimeContractorService Pc = new PrimeContractorService();
                obj.industries = _dbCon.sp_getIndustry_AdminR().ToList();
                obj.roles = _dbCon.sp_getRole_AdminR(parentCompanyId, partyId).Where(x => !x.rolename.ToLower().Contains("prime")).ToList();

                obj.parentCompanies = Pc.GetParentCompanyList();
                obj.parentCompanies.Insert(0, new GenericList { Key = 0, Value = "(None)" });

                obj.contacts = _dbCon.sp_getContact_AdminR(parentCompanyName).ToList();
                obj.contacts.Insert(0, new sp_getContact_AdminR_Result { ContactID = 0, ContactName = "(None)" });

                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyModel");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyModel");
                }
                return new Model.CompanyModel();
            }
        }

        /// <summary>
        /// this method is use for setting parent company detail
        /// </summary>
        /// <param name="model"></param>
        /// <param name="partyId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public CompanyModel SetParentCompanyDetail(CompanyModel model, int partyId, int companyId)
        {
            try
            {
                var info = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == model.parentcompanyid);
                if (info != null)
                {
                    model.parentcompanyname = info.ParentCompanyName;
                    model.ParentParentCompanyID = info.ParentParentCompanyID; //for setting parent company dropdown

                    if (info.YearInc != null) model.yearInc = Convert.ToDateTime(info.YearInc).Year;
                    else
                        model.yearInc = null;
                    model.numEmployees = info.NumEmployees;
                    model.companyURL = info.CompanyURL;
                    model.DUNS = info.DUNS;
                    model.taxID = info.Taxid;
                    model.marketCap = info.MarketCap;
                    model.annualitspending = info.AnnualITSpending;
                    model.annualsvcsspending = info.AnnualSvcsSpending;
                    model.isActive = info.IsActive;
                    model.gscontactId = info.GSContactID; //for setting contact dropdown
                    model.docmgtvisible = info.DocMgtVisible == true ? 1 : 0;
                    model.gscontactEmail = info.GSContactEmail;

                }
                var partyRoles = _dbCon.tblPartyRoles.Where(x => x.PartyID == partyId).Select(x => x.RoleID).ToList();
                if (partyRoles.Count > 0)
                {
                    model.roleIds = string.Join(",", partyRoles);
                }
                var sicIds = _dbCon.tblCompanySICs.Where(x => x.companyID == companyId).Select(x => x.sic).ToList();

                if (sicIds.Count > 0)
                {
                    model.sicIds = string.Join(",", sicIds);
                }
                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "SetParentCompanyDetail");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "SetParentCompanyDetail");
                }
                return new Model.CompanyModel();
            }
        }

        /// <summary>
        /// this method is use for creating company
        /// </summary>
        /// <param name="companyModel"></param>
        /// <returns></returns>
        public object CreateCompany(CompanyModel companyModel)
        {
            try
            {
                ObjectParameter companyId = new ObjectParameter("companyId", typeof(Int32));
                ObjectParameter partyid = new ObjectParameter("partyid", typeof(Int32));
                ObjectParameter errMsg = new ObjectParameter("errMsg", typeof(string));

                DateTime? yearinc = null;

                if (companyModel.gscontactId == 0)
                {
                    companyModel.gscontactId = null;
                }
                if (companyModel.ParentParentCompanyID == 0)
                {
                    companyModel.ParentParentCompanyID = null;
                }

                if (companyModel.yearInc > 0)
                {
                    yearinc = new DateTime(Convert.ToInt32(companyModel.yearInc), DateTime.Now.Month, DateTime.Now.Day);
                }

                var existingRecord =
                    _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == companyModel.companyName.Trim())
                        .FirstOrDefault();
                if (existingRecord != null)
                {
                    return new { companyId = -1, partyid = -1, addCount = -1, errMsg = "" };
                }

                StaticHelper.LogException(" -> before calling sp_AddCompany_AdminR method", "CreateCompany");
                var count = _dbCon.sp_AddCompany_AdminR(companyModel.companyName, companyModel.numEmployees,
                    companyModel.ParentParentCompanyID, yearinc, companyModel.companyURL, companyModel.taxID,
                    companyModel.DUNS, companyModel.marketCap, companyModel.annualitspending,
                    companyModel.annualsvcsspending, true, companyModel.gscontactId, companyModel.docmgtvisible,
                    companyModel.gscontactEmail, companyModel.userID, companyId, partyid, errMsg);
                StaticHelper.LogException(" -> after calling sp_AddCompany_AdminR method", "CreateCompany");
                if (count > 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(errMsg.Value)) && !string.IsNullOrEmpty(companyModel.sicIds))
                    {
                        string[] sicArr = companyModel.sicIds.Split(',');
                        foreach (var sic in sicArr)
                        {
                            _dbCon.sp_UpdateCompanySIC_AdminR(Convert.ToInt32(companyId.Value), sic, companyModel.userID);
                        }

                    }
                    if (string.IsNullOrEmpty(Convert.ToString(errMsg.Value)) && !string.IsNullOrEmpty(companyModel.roleIds))
                    {
                        string[] roleIdArr = companyModel.roleIds.Split(',');
                        foreach (var roleId in roleIdArr)
                        {
                            _dbCon.sp_UpdatePartyRole_AdminR(Convert.ToInt32(partyid.Value), Convert.ToInt32(roleId),
                                companyModel.userID);
                        }
                    }
                }
                var data =
                    new { companyId = companyId.Value, partyid = partyid.Value, errMsg = errMsg.Value, addCount = count };
                return data;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Creating Company<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "CreateCompany");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "CreateCompany");
                }
                var data =
                   new { companyId = 0, partyid = 0, errMsg = "Error Occured", addCount = 0 };
                return data;
            }
        }

        /// <summary>
        /// this method is use for updating company
        /// </summary>
        /// <param name="model"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public object UpdateCompany(CompanyModel model, int? partyId)
        {
            try
            {
                DateTime? yearinc = null;
                if (model.yearInc > 0)
                {
                    yearinc = new DateTime(Convert.ToInt32(model.yearInc), DateTime.Now.Month, DateTime.Now.Day);
                }
                if (model.gscontactId == 0)
                {
                    model.gscontactId = null;
                }
                if (model.ParentParentCompanyID == 0)
                {
                    model.ParentParentCompanyID = null;
                }

                var existingRecord =
                    _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == model.companyName.Trim()).FirstOrDefault();
                if (existingRecord != null)
                {
                    if (existingRecord.ParentCompanyID != model.CompanyId)
                    {
                        var data = new { companyId = model.CompanyId, partyid = partyId, updateCount = -1 };
                        return data;
                    }
                }

                //model.ParentParentCompanyID = model.parentcompanyid;
                StaticHelper.LogException(" -> before calling sp_UpdateCompany_AdminR method", "UpdateCompany");
                var count = _dbCon.sp_UpdateCompany_AdminR(model.CompanyId, null, model.companyName, model.annualitspending,
                    model.annualsvcsspending, model.marketCap, model.numEmployees, model.ParentParentCompanyID, yearinc,
                    model.DUNS, model.taxID, model.companyURL, model.userID, model.isActive == true ? 1 : 0,
                    model.gscontactId, model.docmgtvisible, model.gscontactEmail);
                StaticHelper.LogException(" -> before calling sp_UpdateCompany_AdminR method", "UpdateCompany");

                if (count > 0)
                {
                    if (!string.IsNullOrEmpty(model.sicIds))
                    {
                        string[] sicArr = model.sicIds.Split(',');
                        if (sicArr.Length > 0)
                        {
                            var sicToDelete = _dbCon.tblCompanySICs.Where(x => x.companyID == model.CompanyId).ToList();
                            _dbCon.tblCompanySICs.RemoveRange(sicToDelete);
                            _dbCon.SaveChanges();

                            foreach (var sic in sicArr)
                            {
                                _dbCon.sp_UpdateCompanySIC_AdminR(model.CompanyId, sic, model.userID);
                            }
                        }


                    }
                    if (!string.IsNullOrEmpty(model.roleIds))
                    {
                        string[] roleIdArr = model.roleIds.Split(',');
                        if (roleIdArr.Length > 0)
                        {
                            var roleToDelete = _dbCon.tblPartyRoles.Where(x => x.PartyID == partyId).ToList();
                            _dbCon.tblPartyRoles.RemoveRange(roleToDelete);
                            _dbCon.SaveChanges();

                            foreach (var roleId in roleIdArr)
                            {
                                _dbCon.sp_UpdatePartyRole_AdminR(partyId, Convert.ToInt32(roleId), model.userID);
                            }
                        }

                    }
                }
                var obj = new { companyId = model.CompanyId, partyid = partyId, updateCount = count };
                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCompany");
                EventLog.WriteEntry("Application", "-->>>>>Updating Company<<<<<--" + ex.ToString());

                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCompany");
                }
                var obj = new { companyId = model.CompanyId, partyid = partyId, updateCount = 0 };
                return obj;
            }
        }

        /// <summary>
        /// this method is use for getting manufacturer Id
        /// </summary>
        /// <param name="parentcompanyId"></param>
        /// <returns></returns>
        public int GetManufacturerId(int parentcompanyId)
        {
            try
            {
                return _dbCon.tblManufacturers.FirstOrDefault(x => x.ParentCompanyID == parentcompanyId).ManufacturerID;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManufacturerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturerId");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for getting contact
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <param name="userId"></param>
        /// <param name="searchValue"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<sp_getCompanyContacts_AdminR_Result> GetContacts(string parentCompanyId, string userId,
            string searchValue, string companyId)
        {
            try
            {
                return _dbCon.sp_getCompanyContacts_AdminR(parentCompanyId, userId, searchValue, companyId).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetContacts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetContacts");
                }
                return new List<sp_getCompanyContacts_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method is use for getting company by party id
        /// </summary>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public tblParentCompany GetCompanyByPartyId(int partyId)
        {
            try
            {
                return _dbCon.tblParentCompanies.FirstOrDefault(x => x.PartyID == partyId);

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyByPartyId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyByPartyId");
                }

                return null;
            }
        }

        /// <summary>
        /// this method is use for getting role name by party id
        /// </summary>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public string GetRoleNameByPartyId(int partyId)
        {
            try
            {
                string RoleName = string.Empty;
                var partyRole = _dbCon.tblPartyRoles.Where(x => x.PartyID == partyId).Select(x => x.RoleID).ToList();

                foreach (var item in partyRole)
                {
                    var role = _dbCon.tblRoles.FirstOrDefault(x => x.RoleID == item);
                    if (role != null)
                    {
                        RoleName = RoleName + role.RoleName + ",";
                    }
                }


                return RoleName;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRoleNameByPartyId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRoleNameByPartyId");
                }

                return null;
            }
        }

        #region contact

        /// <summary>
        /// this method is use for getting referrals
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetReferrals()
        {
            try
            {
                return _dbCon.tblReferrals.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.ReferralID,
                    Value = x.ReferralName

                }).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReferrals");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReferrals");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        /// this method is use for getting users
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetUsers()
        {
            try
            {
                return _dbCon.tblUsers.Where(x => x.IsActive == 1).Select(x => new GenericList
                {
                    Key = x.UserID,
                    Value = x.UserName
                }).OrderBy(x => x.Value).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUsers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUsers");
                }

                return new List<GenericList>();
            }
        }

        /// <summary>
        /// this method is use for getting users by company id
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<GenericList> GetUsers(int companyId)
        {
            try
            {
                var company = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == companyId);

                if (company != null)
                {
                    var partyRole = _dbCon.tblPartyRoles.FirstOrDefault(x => x.PartyID == company.PartyID);
                    if (partyRole != null)
                    {
                        var role = _dbCon.tblRoles.FirstOrDefault(x => x.RoleID == partyRole.RoleID);
                        if (role != null)
                        {
                            if (role.RoleName.ToLower() == "manufacturer")
                            {
                                var userIds = _dbCon.tblMFGUsers.Where(x => x.IsActive == true).Select(x => x.UserID).ToList();
                                return _dbCon.tblUsers.Where(x => userIds.Contains(x.UserID)).OrderBy(x => x.LastUpdated).Take(2000).Select(x => new GenericList
                                {
                                    Key = x.UserID,
                                    Value = x.UserName
                                }).OrderBy(x => x.Value).ToList();
                            }
                            else
                            {
                                return GetUsers();
                            }
                        }
                        else
                        {
                            return GetUsers();
                        }
                    }
                    else
                    {
                        return GetUsers();
                    }
                }
                else
                {
                    return GetUsers();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUsers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUsers");
                }

                return new List<GenericList>();
            }
        }

        /// <summary>
        /// this method is use for getting user by search
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<GenericList> GetUserBySearch(string value)
        {
            try
            {
                var data = _dbCon.tblUsers.Where(x => x.IsActive == 1).OrderBy(x => x.LastUpdated).Skip(1000).Where(x => x.UserName.Contains(value)).Select(x => new GenericList
                {
                    Key = x.UserID,
                    Value = x.UserName
                }).OrderBy(x => x.Value).ToList();
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUserBySearch");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUserBySearch");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        /// this method is use for getting companies
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public List<GenericList> GetCompanies(int parentCompanyId)
        {
            try
            {
                return
                        _dbCon.tblParentCompanies.Where(
                            x => (x.CreateByCompanyID == null || x.CreateByCompanyID == parentCompanyId))
                            .Select(x => new GenericList
                            {
                                Key = x.ParentCompanyID,
                                Value = x.ParentCompanyName
                            }).OrderBy(x => x.Value).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanies");
                }
                return new List<GenericList>();
            }

        }

        /// <summary>
        /// this method is use for getting contact model
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <param name="contactId"></param>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public CompanyContactModel GetContactModel(int parentCompanyId, int contactId, int companyId, int? userId)
        {
            try
            {
                CountryPresenter cp = new CountryPresenter();
                CompanyContactModel cm = new Model.CompanyContactModel();
                cm.contactAttachments = new List<Entity.sp_getContactAttachment_AdminR_Result>();
                cm.roleIds = string.Empty;

                cm.Companies = GetCompanies(parentCompanyId);
                cm.Companies.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                cm.Referrals = GetReferrals();
                cm.Referrals.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                //cm.Users = GetUsers(companyId);
                cm.Users = GetUsers();
                cm.Users.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                cm.Countries = cp.GetAllCountries();
                if (contactId > 0)
                {
                    cm.ContactInfo = _dbCon.sp_getCompanyContactInfo_AdminR(contactId, companyId).FirstOrDefault();
                    if (cm.ContactInfo != null)
                    {
                        cm.ContactType =
                            _dbCon.sp_GetContactType_AdminR(cm.ContactInfo.partyid, parentCompanyId)
                                .Select(x => new GenericList
                                {
                                    Key = x.roleid,
                                    Value = x.rolename
                                }).ToList();
                        cm.roleIds = String.Join(",",
                            _dbCon.tblPartyRoles.Where(x => x.PartyID == cm.ContactInfo.partyid)
                                .Select(x => x.RoleID)
                                .ToList());
                    }

                    cm.ContactType.Insert(0, new GenericList { Key = 0, Value = "(None)" });

                    cm.contactAttachments = _dbCon.sp_getContactAttachment_AdminR(contactId).ToList();
                }
                else
                {
                    cm.ContactType =
                        _dbCon.tblRoles.Where(x => x.CompanyID == parentCompanyId && x.IsActive == true)
                            .Select(x => new GenericList
                            {
                                Key = x.RoleID,
                                Value = x.RoleName
                            }).ToList();
                    cm.ContactType.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                    cm.ContactInfo = new sp_getCompanyContactInfo_AdminR_Result();
                    if (userId != null)
                    {
                        int intUser = Convert.ToInt32(userId);
                        var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == intUser);
                        if (user != null)
                        {
                            cm.ContactInfo.lastname = user.UserName;

                            var company = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == companyId);

                            if (company != null)
                            {
                                var partyRole = _dbCon.tblPartyRoles.FirstOrDefault(x => x.PartyID == company.PartyID);
                                if (partyRole != null)
                                {
                                    cm.roleIds = partyRole.RoleID.ToString();

                                    var role = _dbCon.tblRoles.FirstOrDefault(x => x.RoleID == partyRole.RoleID);
                                    if (role != null)
                                    {
                                        if (role.RoleName.ToLower() == "manufacturer")
                                        {
                                            var mfgUser = _dbCon.tblMFGUsers.FirstOrDefault(x => x.UserID == intUser);
                                            if (mfgUser != null)
                                            {
                                                cm.ContactInfo.email = mfgUser.Email;
                                            }


                                        }

                                    }

                                }

                            }

                        }
                    }
                }

                return cm;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetContactModel");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetContactModel");
                }

                return new CompanyContactModel();
            }

        }

        /// <summary>
        /// this method is use for updating contact
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        public object UpdateContact(sp_getCompanyContactInfo_AdminR_Result model, int userId, string roleIds)
        {
            try
            {
                int? reportstopartyid = null;
                int? referralID = null;
                int? userID = null;
                int? companyId = null;

                if (model.reportstopartyid != 0)
                {
                    reportstopartyid = model.reportstopartyid;
                }
                if (model.referralID != 0)
                {
                    referralID = model.referralID;
                }
                if (model.userid != 0)
                {
                    userID = model.userid;
                }
                if (model.companyid != 0)
                {
                    companyId = model.companyid;
                }

                var count = _dbCon.sp_UpdateContact_AdminR(model.ContactID, companyId, model.partyid, model.lastname,
                    model.firstname, model.middlename,
                    model.nickname, model.salutation, model.title, model.department, model.homephonenumber,
                    model.workphonenumber, model.mobilephonenumber,
                    model.pagernumber, model.faxnumber, model.email, null, null, null, model.companylocationid, referralID,
                    model.contactNotes, reportstopartyid,
                    model.isPublic, model.address1, model.address2, model.city, model.stateprovinceregion, model.postalcode,
                    model.countryid == 0 ? null : model.countryid, userID, userId);

                if (count > 0 && roleIds.Length > 0)
                {
                    string[] roleIdArr = roleIds.Split(',');
                    if (roleIdArr.Length > 0)
                    {
                        var roleToDelete = _dbCon.tblPartyRoles.Where(x => x.PartyID == model.partyid).ToList();
                        _dbCon.tblPartyRoles.RemoveRange(roleToDelete);
                        _dbCon.SaveChanges();

                        foreach (var roleId in roleIdArr)
                        {
                            _dbCon.sp_UpdatePartyRole_AdminR(model.partyid, Convert.ToInt32(roleId), userId);
                        }
                    }
                }
                return new { UpdateCount = count, msg = "Contact has been updated successfully" };
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateContact");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateContact");

                    return new { UpdateCount = 0, msg = ex.InnerException.Message };
                }
                return new { UpdateCount = 0, msg = "Error Occured" };
            }
        }

        /// <summary>
        /// this method is use for adding contact
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sessionuserId"></param>
        /// <param name="roleIds"></param>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public object AddContact(sp_getCompanyContactInfo_AdminR_Result model, int sessionuserId, string roleIds,
            int parentCompanyId)
        {
            try
            {
                int? reportstopartyid = null;
                int? referralID = null;
                int? userID = null;
                int? companyId = null;

                if (model.reportstopartyid != 0)
                {
                    reportstopartyid = model.reportstopartyid;
                }
                if (model.referralID != 0)
                {
                    referralID = model.referralID;
                }
                if (model.userid != 0)
                {
                    userID = model.userid;
                }
                if (model.companyid != 0)
                {
                    companyId = model.companyid;
                }
                ObjectParameter newcontactid = new ObjectParameter("newcontactid", typeof(Int32));
                ObjectParameter partyid = new ObjectParameter("partyid", typeof(Int32));

                var count = _dbCon.sp_AddContact_AdminR(model.lastname, model.firstname, model.middlename, model.title,
                    model.homephonenumber,
                    model.workphonenumber, model.faxnumber, model.mobilephonenumber, model.pagernumber, companyId,
                    model.department, model.email, referralID,
                    model.contacttypeid, reportstopartyid, null, model.salutation, null, model.companylocationid, userID,
                    model.contactNotes, sessionuserId, parentCompanyId, newcontactid, partyid);

                if (count > 0 && roleIds.Length > 0)
                {
                    string[] roleIdArr = roleIds.Split(',');
                    if (roleIdArr.Length > 0)
                    {
                        foreach (var roleId in roleIdArr)
                        {
                            _dbCon.sp_UpdatePartyRole_AdminR(Convert.ToInt32(partyid.Value), Convert.ToInt32(roleId),
                                sessionuserId);
                        }
                    }
                }
                var data = new { newcontactId = newcontactid.Value, partyId = partyid.Value, addCount = count };
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddContact");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddContact");
                }
                return null;
            }

        }

        /// <summary>
        /// this method is use for adding contact attachment
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="daid"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url,
            int userId, int? daid)
        {
            try
            {
                return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                        null, null, null, null, null, null, null, null, null,
                        null, null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for adding contact attachment info
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="daid"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddContactAttachmentInfo(int contactId, int daid, int userId)
        {
            try
            {
                int count = 0;
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblContactAttachment ca = new Entity.tblContactAttachment();
                    ca.ContactID = contactId;
                    ca.DAID = daid;
                    ca.IsActive = true;
                    ca.LastUpdated = DateTime.Now;
                    ca.LastUpdatedByUserID = userId;
                    _dbCon.tblContactAttachments.Add(ca);
                    count = _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for deleting contact 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public int DeleteContact(int contactId)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                return _dbCon.sp_DeleteContact_AdminR(contactId);
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "DeleteContact");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteContact");
                }
                return 0;
            }
        }

        #endregion

        #region Company Manufacturer

        /// <summary>
        /// this method is use for getting company manufacturer info
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public CompanyManufacturerInfoModel GetCompanyManufacturerInfo(int parentCompanyId)
        {
            try
            {
                CompanyManufacturerInfoModel obj = new Model.CompanyManufacturerInfoModel();

                obj.DateFormates =
                    _dbCon.tblDateFormats.Where(x => x.IsActive == true)
                        .OrderByDescending(x => x.IsDefaultFormat)
                        .ThenBy(x => x.DateFormatName)
                        .Select(x => new GenericList
                        {
                            Value = x.DateFormatName,
                            Key = x.DateFormatID
                        }).ToList();

                obj.MfgExchangeRates =
                    _dbCon.tblManufacturers.Where(x => x.IsActive == true && x.hasExRate == true)
                        .Select(x => new GenericList
                        {
                            Value = x.ManufacturerName,
                            Key = x.ManufacturerID
                        }).OrderBy(x => x.Value).ToList();

                var manufacturerInfo = _dbCon.tblManufacturers.FirstOrDefault(x => x.ParentCompanyID == parentCompanyId);
                var lastUpdatedUser = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == manufacturerInfo.LastUpdatedByUserID);

                obj.GivesAccreditation = Convert.ToBoolean(manufacturerInfo.GivesAccreditation);
                obj.HasExchangeRates = Convert.ToBoolean(manufacturerInfo.hasExRate);
                obj.LastUpdated = manufacturerInfo.LastUpdated;
                obj.ExRateFromMFG = Convert.ToInt32(manufacturerInfo.ExRateFromMFG);
                obj.UpdatedBy = lastUpdatedUser.UserName;
                obj.DateFormatId = Convert.ToInt32(manufacturerInfo.DateFormatID);
                obj.ParentCompanyId = manufacturerInfo.ParentCompanyID;
                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyManufacturerInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyManufacturerInfo");
                }
                return new CompanyManufacturerInfoModel();
            }
        }

        /// <summary>
        /// this method is use for updating company manufacturer info
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="username"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public CompanyManufacturerInfoModel UpdateCompanyManufacturerInfo(CompanyManufacturerInfoModel model, int userId,
            string username, out int count)
        {
            count = 0;
            try
            {
                tblManufacturer mfg = new Entity.tblManufacturer();
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    mfg =
                        _dbCon.tblManufacturers.FirstOrDefault(x => x.ParentCompanyID == model.ParentCompanyId);
                    if (mfg != null)
                    {
                        mfg.LastUpdated = DateTime.Now;
                        mfg.ExRateFromMFG = model.ExRateFromMFG;
                        mfg.GivesAccreditation = model.GivesAccreditation;
                        mfg.hasExRate = model.HasExchangeRates;
                        mfg.LastUpdatedByUserID = userId;
                        mfg.DateFormatID = model.DateFormatId;

                        _dbCon.Entry(mfg).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();

                        model.LastUpdated = DateTime.Now;
                        model.UpdatedBy = username;
                    }


                    transaction.Commit();

                }

                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCompanyManufacturerInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCompanyManufacturerInfo");
                }
                return new CompanyManufacturerInfoModel();
            }
        }

        /// <summary>
        /// this method is use for getting address list
        /// </summary>
        /// <param name="partyid"></param>
        /// <returns></returns>
        public List<sp_GetAddressByPartyId_AdminR_Result> GetAddressList(int partyid)
        {
            try
            {
                return _dbCon.sp_GetAddressByPartyId_AdminR(partyid).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAddressList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAddressList");
                }
                return new List<sp_GetAddressByPartyId_AdminR_Result>();
            }

        }

        /// <summary>
        /// this method is use for getting address info
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public CompanyManufacturerAddressModel GetAddressInfo(int addressId)
        {
            try
            {
                CompanyManufacturerAddressModel obj = new CompanyManufacturerAddressModel();
                CountryPresenter cp = new CountryPresenter();
                if (addressId != 0)
                    obj.AddressInfo = _dbCon.sp_GetAddressInfo_AdminR(addressId).FirstOrDefault();
                else
                    obj.AddressInfo = new sp_GetAddressInfo_AdminR_Result();

                obj.Countries = cp.GetAllCountries();
                obj.AddressTypes = _dbCon.tblAddressTypes.Where(x => x.IsActive).Select(x => new GenericList
                {
                    Value = x.AddressTypeName,
                    Key = x.AddressTypeID
                }).ToList();

                obj.AddressTypes.Insert(0, new GenericList { Value = "(None)", Key = 0 });


                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAddressInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAddressInfo");
                }
                return new CompanyManufacturerAddressModel();
            }
        }

        /// <summary>
        /// this method is use for creating address
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="partyId"></param>
        /// <param name="CorporateHeadquartersId"></param>
        /// <returns></returns>
        public object CreateAddress(sp_GetAddressInfo_AdminR_Result model, int userId, int partyId,
            int CorporateHeadquartersId)
        {
            try
            {
                ObjectParameter addressId = new ObjectParameter("addressid", typeof(Int32));
                ObjectParameter partylocationId = new ObjectParameter("partylocationid", typeof(Int32));

                tblPartyAddress pa =
                    _dbCon.tblPartyAddresses.Where(x => x.PartyID == partyId && x.AddressTypeID == CorporateHeadquartersId)
                        .FirstOrDefault();

                if (pa == null)
                {
                    if (model.AddressTypeId != CorporateHeadquartersId)
                    {
                        var norecord = new { addressId = 0, partylocationId = 0, addCount = -1, previousAddressTypeId = 0 };
                        return norecord;
                    }
                }
                else
                {
                    if (model.AddressTypeId == CorporateHeadquartersId)
                    {
                        var norecord = new { addressId = 0, partylocationId = 0, addCount = -2, previousAddressTypeId = 0 };
                        return norecord;
                    }
                }



                int? AddressTypeId = null;
                if (model.AddressTypeId != 0)
                {
                    AddressTypeId = model.AddressTypeId;
                }
                var count = _dbCon.sp_AddAddress_AdminR(model.Address1, model.Address2, model.Address3, model.Address4,
                    model.Phone, model.Fax, model.City, model.StateProvinceRegion, model.PostalCode, model.CountryId,
                    AddressTypeId, model.AddressName, model.Directions, userId, partyId, model.IsBillTo, model.IsShipTo,
                    addressId, partylocationId);
                var data = new { addressId = addressId.Value, partylocationId = partylocationId.Value, addCount = count };
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "CreateAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "CreateAddress");
                }
                return null;
            }
        }

        /// <summary>
        /// this method is use for updating address
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="partyId"></param>
        /// <param name="CorporateHeadquartersId"></param>
        /// <returns></returns>
        public object UpdateAddress(sp_GetAddressInfo_AdminR_Result model, int userId, int partyId,
            int CorporateHeadquartersId)
        {
            try
            {
                int? AddressTypeId = null;
                if (model.AddressTypeId != 0)
                {
                    AddressTypeId = model.AddressTypeId;
                }

                tblPartyAddress pa1 =
                    _dbCon.tblPartyAddresses.Where(x => x.PartyID == partyId && x.AddressID == model.AddressId)
                        .FirstOrDefault();
                if (pa1 != null)
                {
                    if (pa1.AddressTypeID != AddressTypeId && AddressTypeId == CorporateHeadquartersId)
                    {
                        tblPartyAddress pa =
                            _dbCon.tblPartyAddresses.Where(
                                x => x.PartyID == partyId && x.AddressTypeID == CorporateHeadquartersId).FirstOrDefault();
                        if (pa != null)
                        {
                            var norecord = new { addressId = 0, updateCount = -2, previousAddressTypeId = pa1.AddressTypeID };
                            return norecord;
                        }
                    }
                    else if (pa1.AddressTypeID != AddressTypeId && AddressTypeId != CorporateHeadquartersId)
                    {
                        tblPartyAddress pa =
                            _dbCon.tblPartyAddresses.Where(
                                x => x.PartyID == partyId && x.AddressTypeID == CorporateHeadquartersId).FirstOrDefault();
                        if (pa != null)
                        {
                            if (pa.AddressID == model.AddressId)
                            {
                                var norecord = new { addressId = 0, updateCount = -1, previousAddressTypeId = pa1.AddressTypeID };
                                return norecord;
                            }
                        }
                        else
                        {
                            var norecord = new { addressId = 0, updateCount = -1, previousAddressTypeId = pa1.AddressTypeID };
                            return norecord;
                        }
                    }
                }

                var count = _dbCon.sp_UpdateAddress_AdminR(model.AddressId, model.Address1, model.Address2, model.Address3,
                    model.Address4, model.Phone, model.Fax, model.City, model.StateProvinceRegion, model.PostalCode,
                    AddressTypeId, model.AddressName, model.CountryId, userId, model.Directions, model.IsBillTo,
                    model.IsShipTo, partyId);
                var data = new { addressId = model.AddressId, updateCount = count };
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateAddress");
                }
                return null;
            }
        }

        /// <summary>
        /// this method is use for deleting address
        /// </summary>
        /// <param name="partyId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public int DeleteAddress(int partyId, int addressId)
        {
            try
            {
                int count = _dbCon.sp_RemoveAddress_AdminR(addressId, partyId);
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "DeleteAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteAddress");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for updating company manufacturer TBU and Duties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tbu"></param>
        /// <param name="duties"></param>
        /// <param name="countryId"></param>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        public int UpdateCompanyManufacturerTBUandDuties(int userId, decimal? tbu, decimal? duties, int countryId,
            int mfgId)
        {
            try
            {
                int count;
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    var model =
                        _dbCon.tblGenTradeBlockUplifts.FirstOrDefault(
                            x => x.ManufacturerID == mfgId && x.CountryID == countryId);
                    if (model != null)
                    {
                        _dbCon.tblGenTradeBlockUplifts.Remove(model);
                    }

                    tblGenTradeBlockUplift obj = new Entity.tblGenTradeBlockUplift();
                    obj.ManufacturerID = mfgId;
                    obj.CountryID = countryId;
                    obj.Duties = duties == null ? null : (duties / 100);
                    obj.TradeBlockUplift = tbu == null ? null : (tbu / 100);
                    obj.LastUpdated = DateTime.Now;
                    obj.LastUpdatedByUserID = userId;
                    obj.IsActive = true;
                    _dbCon.tblGenTradeBlockUplifts.Add(obj);
                    count = _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCompanyManufacturerTBUandDuties");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCompanyManufacturerTBUandDuties");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for getting TBU and Duties
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        public tblGenTradeBlockUplift GetTBUandDuties(int countryId, int mfgId)
        {
            try
            {
                var data =
                    _dbCon.tblGenTradeBlockUplifts.FirstOrDefault(x => x.CountryID == countryId && x.ManufacturerID == mfgId);
                if (data != null)
                {
                    data.TradeBlockUplift *= 100;
                    data.Duties *= 100;
                }
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTBUandDuties");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTBUandDuties");
                }
                return null;
            }
        }

        /// <summary>
        /// this method is use for getting Manufacturer's users
        /// </summary>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        public List<sp_getManufacturerUsers_AdminR_Result> GetManufacturerUsers(int mfgId)
        {
            try
            {
                return _dbCon.sp_getManufacturerUsers_AdminR(mfgId).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManufacturerUsers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturerUsers");
                }
                return new List<sp_getManufacturerUsers_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method is use for updating user access
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="securityLevel"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="userGroupName"></param>
        /// <returns></returns>
        public int UpdateUserAccess(int userId, int securityLevel, int sessionUserId, string userGroupName)
        {
            try
            {
                var usergroupId = _dbCon.tblUserGroups.FirstOrDefault(x => x.UserSecurityLevel == securityLevel && x.UserGroupName == userGroupName && x.IsActive == 1).UserGroupID;

                int count;
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblUser user =
                        _dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId);
                    if (user != null)
                    {
                        user.UserGroupID = usergroupId;
                        user.LastUpdated = DateTime.Now;
                        user.LastUpdatedByUserID = sessionUserId;
                    }
                    _dbCon.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateUserAccess");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateUserAccess");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for getting manufacturer user detail model
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public CompanyManufacturerUserModel GetManufacturerUserDetailModel(int userId)
        {
            try
            {
                CompanyManufacturerUserModel obj = new Model.CompanyManufacturerUserModel();

                obj.DateFormates =
                    _dbCon.tblDateFormats.Where(x => x.IsActive == true)
                        .OrderByDescending(x => x.IsDefaultFormat)
                        .ThenBy(x => x.DateFormatName)
                        .Select(x => new GenericList
                        {
                            Value = x.DateFormatName,
                            Key = x.DateFormatID
                        }).ToList();
                if (userId > 0)
                {
                    obj.userInfo = _dbCon.sp_getManufacturerUserInfo_AdminR(userId).FirstOrDefault();
                    if (obj.userInfo != null)
                    {
                        obj.ContactId = 0;
                        var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == obj.userInfo.UserID);
                        if (contact != null)
                        {
                            obj.ContactId = contact.ContactID;
                        }

                    }
                }
                else
                    obj.userInfo = new sp_getManufacturerUserInfo_AdminR_Result();

                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManufacturerUserDetailModel");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturerUserDetailModel");
                }
                return new CompanyManufacturerUserModel();
            }
        }

        /// <summary>
        /// this method is use for updating manufacturer user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int UpdateManufacturerUser(sp_getManufacturerUserInfo_AdminR_Result model, int sessionUserId)
        {
            try
            {
                int count = 0;

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblUser user =
                        _dbCon.tblUsers.FirstOrDefault(x => x.UserID == model.UserID);


                    var existingUser = _dbCon.tblUsers.FirstOrDefault(x => x.UserName.ToLower() == model.UserName.Trim().ToLower());
                    if (existingUser != null)
                    {
                        if (existingUser.UserID != user.UserID)
                        {
                            return -1;
                        }
                    }

                    user.UserName = model.UserName;
                    user.UserPassword = model.UserPassword;
                    user.LastUpdated = DateTime.Now;
                    user.LastUpdatedByUserID = sessionUserId;
                    user.DateFormatID = model.DateFormatID;
                    user.IsActive = model.IsActive == true ? Convert.ToByte(1) : Convert.ToByte(0);

                    _dbCon.Entry(user).State = System.Data.Entity.EntityState.Modified;

                    tblMFGUser mfgUser = _dbCon.tblMFGUsers.FirstOrDefault(x => x.UserID == model.UserID);

                    mfgUser.Email = string.IsNullOrEmpty(model.Email) ? null : model.Email;
                    mfgUser.IsActive = model.IsActive;
                    mfgUser.LastUpdated = DateTime.Now;
                    mfgUser.LastUpdatedByUserID = sessionUserId;

                    _dbCon.Entry(mfgUser).State = System.Data.Entity.EntityState.Modified;

                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateManufacturerUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateManufacturerUser");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for adding manufacturer user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="mfgId"></param>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public int AddManufacturerUser(sp_getManufacturerUserInfo_AdminR_Result model, int sessionUserId, int mfgId,
            int parentCompanyId)
        {
            try
            {
                var existingUser = _dbCon.tblUsers.FirstOrDefault(x => x.UserName.ToLower() == model.UserName.Trim().ToLower());
                if (existingUser != null)
                {
                    return -1;
                }

                int count = 0;
                string FirstName = string.Empty;
                string LastName = string.Empty;
                int? CompanyId =
                    _dbCon.tblManufacturers.Where(x => x.ManufacturerID == mfgId).FirstOrDefault().ParentCompanyID;
                int index = model.UserName.IndexOf(" ");
                if (index != -1)
                {
                    FirstName = model.UserName.Substring(index + 1);
                    LastName = model.UserName.Substring(0, index);
                }
                else
                {
                    FirstName = "";
                    LastName = model.UserName;
                }


                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    var userId =
                        _dbCon.Sp_CreateOneSourceUser_AdminR(model.UserName, model.UserPassword, 15, null,
                            Convert.ToInt32(model.DateFormatID).ToString(), null, null, "MANUFACTURER", LastName, FirstName,
                            null, null, null, null, null, null, null, CompanyId, null, model.Email, null, null, null, null,
                            null, null, null, parentCompanyId, sessionUserId).FirstOrDefault();

                    tblMFGUser user = new tblMFGUser();
                    user.UserID = Convert.ToInt32(userId);
                    user.ManufacturerID = mfgId;
                    user.Email = model.Email;
                    user.Phone = null;
                    user.Fax = null;
                    user.LastUpdated = DateTime.Now;
                    user.LastUpdatedByUserID = sessionUserId;
                    user.IsActive = true;
                    _dbCon.tblMFGUsers.Add(user);

                    count = _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddManufacturerUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddManufacturerUser");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is use for getting manufacturer exchange rate
        /// </summary>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        public CompanyManufacturerExchangeRate GetMfgExchangeRate(int mfgId)
        {
            try
            {
                CompanyManufacturerExchangeRate obj = new Model.CompanyManufacturerExchangeRate();
                obj.MfgExchangeRateCountries =
                    _dbCon.sp_getCountriesByCurrency_AdminR().Where(x => x.CountryID != null).Select(x => new GenericList
                    {
                        Key = Convert.ToInt32(x.CountryID),
                        Value = x.CountryName

                    }).ToList();

                obj.MfgExchangeRateCountries.Insert(0, new GenericList { Key = 0, Value = "(None)" });

                obj.MfgExchangeRateInfo = _dbCon.sp_GetMfgExchangeRateInfo_AdminR(mfgId.ToString(), null).ToList();

                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetMfgExchangeRate");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetMfgExchangeRate");
                }
                return new CompanyManufacturerExchangeRate();
            }
        }

        /// <summary>
        /// this method is use for getting exchange rate info by country
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        public List<sp_GetMfgExchangeRateInfo_AdminR_Result> GetExchangeRateInfoByCountry(int countryId, int mfgId)
        {
            try
            {
                var MfgExchangeRateInfo =
                    _dbCon.sp_GetMfgExchangeRateInfo_AdminR(mfgId.ToString(), countryId > 0 ? countryId.ToString() : null)
                        .ToList();
                return MfgExchangeRateInfo;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetExchangeRateInfoByCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetExchangeRateInfoByCountry");
                }
                return new List<sp_GetMfgExchangeRateInfo_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method is use for adding updating manufacturer's exchange rate
        /// </summary>
        /// <param name="currencyID"></param>
        /// <param name="exchangeRate"></param>
        /// <param name="mfgId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddUpdateMfgExchangeRate(int currencyID, decimal exchangeRate, int mfgId, int userId)
        {
            try
            {
                int count = 0;
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblManufacturerExchangeRate obj =
                        _dbCon.tblManufacturerExchangeRates.Where(
                            x => x.ManufacturerID == mfgId && x.CurrencyID == currencyID).FirstOrDefault();

                    if (obj == null)
                    {
                        obj = new Entity.tblManufacturerExchangeRate();
                        obj.ManufacturerID = mfgId;
                        obj.CurrencyID = currencyID;
                        obj.ExchangeRateToUSDollars = exchangeRate;
                        obj.ExchangeRateDate = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                        obj.LastUpdated = DateTime.Now;
                        obj.LastUpdatedByUserID = userId;
                        obj.IsActive = 1;
                        _dbCon.tblManufacturerExchangeRates.Add(obj);
                    }
                    else
                    {
                        obj.ExchangeRateToUSDollars = exchangeRate;
                        obj.ExchangeRateDate = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                        obj.LastUpdated = DateTime.Now;
                        obj.LastUpdatedByUserID = userId;
                        obj.IsActive = 1;
                        _dbCon.Entry(obj).State = System.Data.Entity.EntityState.Modified;

                    }

                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateMfgExchangeRate");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateMfgExchangeRate");
                }
                return 0;
            }
        }

        #endregion


        #region Customer

        public tblCustomer GetCustomerByCompanyId(int companyId)
        {
            try
            {
                var customer = _dbCon.tblCustomers.FirstOrDefault(x => x.ParentCompanyID == companyId);
                return customer;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerByCompanyId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerByCompanyId");
                }
                return null;
            }
        }

        public List<tblOffering> GetOfferings()
        {
            try
            {
                var offerings = _dbCon.tblOfferings.Where(x => x.IsActive).OrderBy(x => x.OfferingName).ToList();
                return offerings;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOfferings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOfferings");
                }
                return new List<tblOffering>();
            }
        }

        public bool ChecktUserByUsername(string userName, int userId)
        {
            try
            {
                var chk = _dbCon.tblUsers.Any(x => x.UserName.ToLower() == userName.ToLower() && x.UserID != userId);

                return chk;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "ChecktUserByUsername");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "ChecktUserByUsername");
                }
                return false;
            }
        }

        public string GetCustomerOfferingByCustomerId(int customerId)
        {
            try
            {
                var offerings =
                        _dbCon.tblCustomerOfferings.Where(x => x.CustomerID == customerId).Select(x => x.OfferingID).ToList();
                var ids = String.Join(",", offerings);
                return ids;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerOfferingByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerOfferingByCustomerId");
                }
                return null;
            }
        }

        public List<sp_GetBundleCategoriesSorted_AdminR_Result> getBundleCategoriesSortedByCustomerID(int customerID)
        {
            try
            {
                var resultList = _dbCon.sp_GetBundleCategoriesSorted_AdminR(customerID).ToList();
                return resultList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "getBundleCategoriesSortedByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(),
                        "getBundleCategoriesSortedByCustomerID");
                }
                return null;
            }
        }

        public bool updateBundleCategoriesSortedByCustomerID(List<sp_GetBundleCategoriesSorted_AdminR_Result> categoryList)
        {
            try
            {
                foreach (var data in categoryList)
                {
                    tblBundleCategorySort model = new tblBundleCategorySort();

                    var completeRecord = _dbCon.tblBundleCategorySorts.Where(x => x.CustomerID == data.CustomerID && x.PartCategoryID == data.PartCategoryID).FirstOrDefault();

                    //if the record is found then update it
                    if (completeRecord != null && (completeRecord.SortNumber != data.SortNumber))
                    {
                        var final = completeRecord;
                        final.SortNumber = data.SortNumber;

                        _dbCon.Entry(completeRecord).CurrentValues.SetValues(final);
                    }
                    //if not found then add it
                    else if (completeRecord == null && (data.SortNumber != null || data.SortNumber.ToString() != ""))
                    {
                        model.CustomerID = data.CustomerID;
                        model.PartCategoryID = data.PartCategoryID;
                        model.SortNumber = data.SortNumber;

                        _dbCon.tblBundleCategorySorts.Add(model);
                    }
                }

                _dbCon.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "updateBundleCategoriesSortedByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(),
                        "updateBundleCategoriesSortedByCustomerID");
                }
                return false;
            }
        }

        public CompanyCustomerModel GetCompanyCustomerInfoByCompanyId(int companyId, AdminSetting currentSetting)
        {
            try
            {

                var model = new CompanyCustomerModel();
                var pcService = new PrimeContractorService();
                var countryService = new CountryPresenter();
                model.Customer = GetCustomerByCompanyId(companyId);
                if (model.Customer.PrimeContractorUplift != null && model.Customer.PrimeContractorUplift > 0)
                {
                    model.Customer.PrimeContractorUplift = Decimal.Round(Convert.ToDecimal(model.Customer.PrimeContractorUplift * 100), 2);
                }
                model.OfferingList = GetOfferings();
                if (model.Customer != null)
                {
                    model.CustomerOfferingIds = GetCustomerOfferingByCustomerId(model.Customer.CustomerID);
                    model.UpdatedByUser = countryService.GetUserNameByUserId(model.Customer.LastUpdatedByUserID);
                    model.CustomerUsersList = _dbCon.Sp_GetUserByCustomerId_AdminR(model.Customer.CustomerID).ToList();
                    model.CustomerAddressList =
                        _dbCon.Sp_getCustomerAddress_AdminR(model.Customer.CustomerID, "").ToList();

                    //additional Setup Start
                    model.CustomerDestinationCountryList = GetCustomerDestinationCounrtryList(model.Customer.CustomerID);

                    model.SetupManufacturersList =
                        _dbCon.tblManufacturers.Where(x => x.IsActive && x.GivesAccreditation == true).Select(x => new GenericList()
                        {
                            Key = x.ManufacturerID,
                            Value = x.ManufacturerName
                        }).OrderBy(x => x.Value).ToList();

                    //model.CustomerDestinationAddressList = GetCustomerAddressesWithCountryName(model.Customer.CustomerID);

                    if (model.CustomerDestinationCountryList.Count > 0)
                    {
                        model.CustomerDestinationAddressList = GetCustomerAddressesWithCountryName(model.Customer.CustomerID, model.CustomerDestinationCountryList[0].Key);
                    }
                    else
                    {
                        model.CustomerDestinationAddressList = GetCustomerAddressesWithCountryName(model.Customer.CustomerID);
                    }


                    model.CustomerUpliftsCountryList = GetCustomerAddressCountries(model.Customer.CustomerID);

                    model.TbuManufacturerList = GetManufacturersList("");
                    //additional Setup end

                }
                model.PrimeContractorList = pcService.GetPrimeContractorList("");

                model.CustomerCountryList = GetCustomerCountryListByCustomerId(model.Customer.CustomerID);
                model.ContactList = _dbCon.Sp_GetContactList_AdminR(15, model.Customer.ParentCompanyID, currentSetting.UserID, "", "500").ToList();
                model.CountryDisclaimerList = GetCountryDisclaimerListByCustomerId(model.Customer.CustomerID);


                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyCustomerInfoByCompanyId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(),
                        "GetCompanyCustomerInfoByCompanyId");
                }
                return null;
            }
        }

        public bool UpdateCustomer(CompanyCustomerModel customerInfo, AdminSetting currenSettings)
        {
            try
            {
                var updatedCustomer = customerInfo.Customer;
                var tblcustomer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == updatedCustomer.CustomerID);

                tblcustomer.LastUpdated = DateTime.Now;
                tblcustomer.PrimeContractorID = updatedCustomer.PrimeContractorID;
                tblcustomer.OnesourceVersion = updatedCustomer.OnesourceVersion;
                tblcustomer.ContractStartDate = updatedCustomer.ContractStartDate;
                tblcustomer.PartPriceLimitUSD = updatedCustomer.PartPriceLimitUSD;
                tblcustomer.ServiceLevel = updatedCustomer.ServiceLevel;
                tblcustomer.LastUpdatedByUserID = currenSettings.UserID;
                tblcustomer.NetpQuoteEmail = updatedCustomer.NetpQuoteEmail ?? "";
                tblcustomer.Email = updatedCustomer.Email ?? "";
                tblcustomer.NetpEnquiryEmail = updatedCustomer.NetpEnquiryEmail ?? "";
                tblcustomer.NetpOrderEmail = updatedCustomer.NetpOrderEmail ?? "";
                tblcustomer.PrimeContractorUplift = null;
                tblcustomer.Disclaimer = updatedCustomer.Disclaimer == null ? null : updatedCustomer.Disclaimer.Trim();
                if (customerInfo.Customer.PrimeContractorUplift > 0)
                {
                    tblcustomer.PrimeContractorUplift =
                        Convert.ToDecimal(customerInfo.Customer.PrimeContractorUplift) / 100;
                }
                _dbCon.Entry(tblcustomer).State = System.Data.Entity.EntityState.Modified;
                _dbCon.SaveChanges();

                if (!string.IsNullOrWhiteSpace(customerInfo.CustomerOfferingIds))
                {
                    var check = _dbCon.tblCustomerOfferings.Where(x => x.CustomerID == tblcustomer.CustomerID).ToList();
                    _dbCon.tblCustomerOfferings.RemoveRange(check);
                    _dbCon.SaveChanges();

                    List<int> offeringIds = customerInfo.CustomerOfferingIds.Split(',').Select(int.Parse).ToList();
                    foreach (var offeringId in offeringIds)
                    {
                        var tbloffering = new tblCustomerOffering()
                        {
                            CustomerID = updatedCustomer.CustomerID,
                            IsActive = true,
                            LastUpdateUserID = currenSettings.UserID,
                            LastUpdate = DateTime.Now,
                            OfferingID = offeringId
                        };
                        _dbCon.tblCustomerOfferings.Add(tbloffering);
                        _dbCon.SaveChanges();
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomer");
                }
            }
            return false;
        }

        public List<GenericList> GetCustomerCountryListByCustomerId(int customerId)
        {
            try
            {

                var customerCountryList =
                    _dbCon.tblCustomerCountryCurrencies.Where(x => x.CustomerID == customerId && x.IsActive == 1).ToList();

                var selectExistingCountryIds = customerCountryList.Select(x => x.CountryID).ToList();

                var countryList = _dbCon.tblCountries.Where(x => x.IsActive).ToList();

                var finalList = countryList.Select(x => new GenericList()
                {
                    Key = x.CountryID,
                    Value =
                        x.CountryName.Trim() + " <b>(" + selectExistingCountryIds.Count(c => c == x.CountryID) +
                        " Setup) </b>",
                    Count = selectExistingCountryIds.Count(c => c == x.CountryID)

                }).OrderByDescending(x => x.Count).ThenBy(x => x.Value).ToList();


                return finalList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerCountryListByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerCountryListByCustomerId");
                }
                return new List<GenericList>();
            }
        }

        public List<CustomerCurrencyModel> GetCustomerCurrencyByCountry(int customerId, int countryId)
        {
            try
            {

                var customerCountryList =
                    _dbCon.tblCustomerCountryCurrencies.Where(
                        x => x.CustomerID == customerId && x.CountryID == countryId && x.IsActive == 1).ToList();

                var selectExistingCurrencyIds = customerCountryList.Select(x => x.CurrencyID).ToList();

                var currencyList = _dbCon.tblCurrencies.Where(x => x.IsActive).ToList();

                var finalList = currencyList.Select(x => new CustomerCurrencyModel()
                {
                    CountryId = countryId,
                    CurrencyName = x.CurrencyName.Trim() + " - " + x.CurrencySymbol.Trim(),
                    CurrencyId = x.CurrencyID,
                    CustomerId = customerId,
                    IsSelected = selectExistingCurrencyIds.Any(c => c == x.CurrencyID),
                    IsDefaultCurrency =
                        customerCountryList.Any(c => c.CurrencyID == x.CurrencyID && c.IsDefaultCurrency)
                }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.CurrencyName).ToList();


                return finalList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerCurrencyByCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerCurrencyByCountry");


                }
                return new List<CustomerCurrencyModel>();
            }
        }


        public bool UpdateCustomerCountryCurrencySetups(List<CustomerCurrencyModel> setups, AdminSetting currentSetting)
        {
            try
            {
                var countryId = setups.FirstOrDefault().CountryId;
                var customerId = setups.FirstOrDefault().CustomerId;
                var deleteExisting =
                    _dbCon.tblCustomerCountryCurrencies.Where(
                        x => x.CountryID == countryId && x.CustomerID == customerId).ToList();

                _dbCon.tblCustomerCountryCurrencies.RemoveRange(deleteExisting);
                _dbCon.SaveChanges();

                if (setups.Any(x => x.CurrencyId > 0 && x.IsSelected))
                {

                    var newData = setups.Select(x => new tblCustomerCountryCurrency()
                    {
                        CountryID = x.CountryId,
                        CustomerID = x.CustomerId,
                        IsActive = 1,
                        CurrencyID = x.CurrencyId,
                        LastUpdatedByUserID = currentSetting.UserID,
                        LastUpdated = DateTime.Now,
                        IsDefaultCurrency = x.IsDefaultCurrency,
                    }).ToList();

                    _dbCon.tblCustomerCountryCurrencies.AddRange(newData);
                    _dbCon.SaveChanges();




                }

                var countryIds =
                 _dbCon.tblCustomerCountryCurrencies.Where(x => x.CustomerID == customerId).Select(x => x.CountryID).ToList();

                if (countryIds.Count > 0)
                {
                    var userInvoiceLocations = _dbCon.tblUserDestinationInvoiceLocations.Where(x => x.UserCompanyID == customerId && !countryIds.Contains(x.CountryID)).ToList();
                    _dbCon.tblUserDestinationInvoiceLocations.RemoveRange(userInvoiceLocations);
                    _dbCon.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerCountryCurrencySetups");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerCountryCurrencySetups");


                }
                return false;
            }
        }

        public List<GenericList> GetCustomerDefaultCountries(int customerId)
        {
            try
            {
                var countries = _dbCon.Sp_GetCustomerCountries_AdminR(customerId).Select(x => new GenericList()
                {
                    Key = x.CountryID,
                    Value = x.CountryName
                }).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerDefaultCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerDefaultCountries");
                }
                return new List<GenericList>();
            }
        }

        public List<GenericList> GetCustomerInvoiceAddressCountries(int customerId)
        {
            try
            {
                var countries = _dbCon.Sp_GetCustomerInvoiceLocationCountries_AdminR(customerId).Select(x => new GenericList()
                {
                    Key = x.CountryID,
                    Value = x.CountryName,
                    //Count = _dbCon.sp_GetCustomerInvoiceAddressList_AdminR(customerId, x.CountryID).Count(p => !string.IsNullOrWhiteSpace(p.CondensedAddress))
                }).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerInvoiceAddressCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerInvoiceAddressCountries");
                }
                return new List<GenericList>();
            }
        }


        public List<GenericList> GetCustomerUserGroupList(int customerId)
        {
            try
            {
                var countries = _dbCon.Sp_GetCustomerUserGroup_AdminR(customerId).Select(x => new GenericList()
                {
                    Key = x.CustomUserGroupID,
                    Value = x.CustomUserGroupName
                }).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerUserGroupList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerUserGroupList");
                }
                return new List<GenericList>();
            }
        }

        public List<GenericList> GetCustomerDefaultCurrencies(int customerId)
        {
            try
            {
                var currencies = _dbCon.Sp_GetCustomerCurrencies_AdminR(customerId).Select(x => new GenericList()
                {
                    Key = x.CurrencyID,
                    Value = x.CurrencyName
                }).ToList();
                return currencies;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerDefaultCurrencies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerDefaultCurrencies");
                }
                return new List<GenericList>();
            }
        }

        public tblCustomerBuyerMetric GetCustomerBuyerMetricsyUserId(int userId)
        {
            try
            {
                return _dbCon.tblCustomerBuyerMetrics.FirstOrDefault(x => x.isActive == true && x.customerUserID == userId);

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerBuyerMetricsyUserId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerBuyerMetricsyUserId");
                }
                return null;
            }
        }

        public List<GenericList> Sp_GetCustomerAddressesByCountry_AdminR(int countryId, int customerId)
        {
            try
            {
                var countries =
                       _dbCon.Sp_GetCustomerAddressesByCountry_AdminR(customerId, countryId).Select(x => new GenericList()
                       {
                           Key = x.customerdestinationid,
                           Value = x.condensedaddress.Trim() + " - " + x.countryname.Trim()
                       }).OrderBy(x => x.Value).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "Sp_GetCustomerAddressesByCountry_AdminR");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "Sp_GetCustomerAddressesByCountry_AdminR");
                }
                return new List<GenericList>();
            }
        }


        public bool UpdateCustomerUser(CompanyCustomerModel model, AdminSetting currentSetting)
        {
            try
            {
                var tblUser = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == model.CustomerUser.UserID);
                if (tblUser != null)
                {

                    tblUser.UserName = model.CustomerUser.UserName.Trim();
                    tblUser.UserPassword = model.CustomerUser.UserPassword.Trim();
                    tblUser.DateFormatID = model.CustomerUser.DateFormatID;
                    tblUser.CustomUserGroupID = model.CustomerUser.CustomUserGroupID ?? null;
                    tblUser.LastUpdatedByUserID = currentSetting.UserID;
                    tblUser.LastUpdated = DateTime.Now;
                    tblUser.IsActive = model.CustomerUser.IsActive;
                    _dbCon.Entry(tblUser).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();

                    if (model != null && model.ExpectedMonthlySpend != null)
                    {
                        _dbCon.Sp_UpdateBuyerMetrics_AdminR(tblUser.UserID, null, model.ExpectedMonthlySpend,
                            currentSetting.UserID);
                    }

                }
                var tblCustomerUser = _dbCon.tblCustomerUsers.FirstOrDefault(x => x.UserID == model.CustomerUser.UserID);
                if (tblCustomerUser != null)
                {

                    tblCustomerUser.Email = model.CustomerUser.EMail.Trim();
                    tblCustomerUser.CountryID = model.CustomerUser.CountryID;
                    tblCustomerUser.UserDefaultCountryID = model.CustomerUser.UserDefaultCountryID;
                    tblCustomerUser.UserDefaultAddressID = (model.CustomerUser.UserDefaultAddressID > 0
                        ? model.CustomerUser.UserDefaultAddressID
                        : null);
                    tblCustomerUser.UserSpendingLimit = (model.CustomerUser.UserSpendingLimit > 0
                        ? model.CustomerUser.UserSpendingLimit
                        : null);
                    tblCustomerUser.UserSpendingLimitCurrencyID = model.CustomerUser.UserSpendingLimitCurrencyID;
                    tblCustomerUser.UserSpendingLimitStartDate = model.CustomerUser.UserSpendingLimitStartDate;
                    tblCustomerUser.UserSpendingLimitEndDate = model.CustomerUser.UserSpendingLimitEndDate;
                    tblCustomerUser.IsISDOrderApprovalRequired = model.CustomerUser.IsISDOrderApprovalRequired;
                    tblCustomerUser.IsActive = (model.CustomerUser.IsActive == 1 ? true : false);
                    tblCustomerUser.OrderSpendingLimit = model.CustomerUser.OrderSpendingLimit;
                    tblCustomerUser.OrderSpendingLimitCurrencyID = model.CustomerUser.OrderSpendingLimitCurrencyID;
                    tblCustomerUser.OrderSpendingLimitApprovalCode = model.CustomerUser.OrderSpendingLimitApprovalCode;
                    tblCustomerUser.AddAddressCode = model.CustomerUser.AddAddressCode;
                    tblCustomerUser.UserSpendingTotalToDate = (model.CustomerUser.UserSpendingTotalToDate > 0
                        ? model.CustomerUser.UserSpendingTotalToDate
                        : null);

                    tblCustomerUser.ApproverType = model.CustomerUser.ApproverType;

                    tblCustomerUser.LastUpdated = DateTime.Now;
                    tblCustomerUser.LastUpdatedByUserID = currentSetting.UserID;
                    _dbCon.Entry(tblCustomerUser).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                }


                var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == model.CustomerUser.UserID);
                if (contact != null)
                {
                    contact.Email = model.CustomerUser.EMail.Trim();
                    _dbCon.Entry(contact).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerUser");


                }
                return false;
            }

        }

        public bool AddNewCustomerUser(CompanyCustomerModel model, AdminSetting currentSetting)
        {
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    var firstName = "";
                    var lastName = "";
                    var userNameSlpit = model.CustomerUser.UserName.Trim().Split(' ');
                    var chkCompany = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == model.Customer.CustomerID);

                    if (userNameSlpit.Length > 1)
                    {
                        lastName = userNameSlpit[0];
                        firstName = userNameSlpit[1];
                    }
                    else
                    {
                        lastName = userNameSlpit[0];
                    }

                    var newUuserId =
                        _dbCon.Sp_CreateOneSourceUser_AdminR(model.CustomerUser.UserName,
                            model.CustomerUser.UserPassword, 1, null,
                            Convert.ToInt32(model.CustomerUser.DateFormatID).ToString(), null,
                            model.CustomerUser.CustomUserGroupID, "CUSTOMER", lastName, firstName,
                            null, null, null, null, null, null, null, chkCompany.ParentCompanyID, null,
                            model.CustomerUser.EMail, null, null, null, null,
                            null, null, null, chkCompany.ParentCompanyID, currentSetting.UserID).FirstOrDefault();

                    if (newUuserId != null)
                    {
                        var tblCustomerUser = new tblCustomerUser();

                        tblCustomerUser.UserID = Convert.ToInt32(newUuserId);
                        tblCustomerUser.CustomerID = Convert.ToInt32(model.Customer.CustomerID);
                        tblCustomerUser.Email = model.CustomerUser.EMail.Trim();
                        tblCustomerUser.CountryID = model.CustomerUser.CountryID;
                        tblCustomerUser.UserDefaultCountryID = model.CustomerUser.UserDefaultCountryID;
                        tblCustomerUser.UserDefaultAddressID = (model.CustomerUser.UserDefaultAddressID > 0
                            ? model.CustomerUser.UserDefaultAddressID
                            : null);
                        tblCustomerUser.UserSpendingLimit = (model.CustomerUser.UserSpendingLimit > 0
                            ? model.CustomerUser.UserSpendingLimit
                            : null);
                        tblCustomerUser.UserSpendingLimitCurrencyID = model.CustomerUser.UserSpendingLimitCurrencyID;
                        tblCustomerUser.UserSpendingLimitStartDate = model.CustomerUser.UserSpendingLimitStartDate;
                        tblCustomerUser.UserSpendingLimitEndDate = model.CustomerUser.UserSpendingLimitEndDate;
                        tblCustomerUser.IsISDOrderApprovalRequired = model.CustomerUser.IsISDOrderApprovalRequired;
                        tblCustomerUser.IsActive = true;
                        tblCustomerUser.OrderSpendingLimit = model.CustomerUser.OrderSpendingLimit;
                        tblCustomerUser.OrderSpendingLimitCurrencyID = model.CustomerUser.OrderSpendingLimitCurrencyID;
                        tblCustomerUser.OrderSpendingLimitApprovalCode =
                            model.CustomerUser.OrderSpendingLimitApprovalCode;
                        tblCustomerUser.AddAddressCode = model.CustomerUser.AddAddressCode;
                        tblCustomerUser.UserSpendingTotalToDate = (model.CustomerUser.UserSpendingTotalToDate > 0
                            ? model.CustomerUser.UserSpendingTotalToDate
                            : null);
                        tblCustomerUser.LastUpdated = DateTime.Now;
                        tblCustomerUser.LastUpdatedByUserID = currentSetting.UserID;

                        tblCustomerUser.ApproverType = model.CustomerUser.ApproverType;

                        _dbCon.tblCustomerUsers.Add(tblCustomerUser);
                        _dbCon.SaveChanges();

                        string[] prefNames = new[] { "NotifyAccepted", "NotifyETAChange", "NotifyOrderShip" };

                        var selectPref = _dbCon.tblPreferences.Where(x => prefNames.Contains(x.PreferenceName)).ToList();

                        var userPref = selectPref.Select(x => new tblUserPreference()
                        {
                            PreferenceID = x.PreferenceID,
                            IsActive = true,
                            PreferenceValue = "1",
                            UserID = Convert.ToInt32(newUuserId),
                            LastUpdated = DateTime.Now,
                            LastUpdatedByUserID = currentSetting.UserID,
                            PreferenceValueRevamp = "1"

                        }).ToList();

                        _dbCon.tblUserPreferences.AddRange(userPref);
                        _dbCon.SaveChanges();

                        if (model != null && model.ExpectedMonthlySpend != null)
                        {
                            _dbCon.Sp_UpdateBuyerMetrics_AdminR(newUuserId, null, model.ExpectedMonthlySpend,
                                currentSetting.UserID);
                        }

                    }

                    _dbCon.SaveChanges();
                    transaction.Commit();

                }




                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewCustomerUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewCustomerUser");


                }
                return false;
            }

        }

        public List<GenericList> GetCustomerDestinationCountriesByCustomerId(int customerId)
        {
            var countries = _dbCon.Sp_GetShippingCountriesByCustomer_AdminR(customerId).Select(x => new GenericList()
            {
                Key = x.CountryID,
                Value = x.CountryName.Trim()
            }).OrderBy(x => x.Value).ToList();
            return countries;
        }

        public List<GenericList> GetCustomerDestinationAddressByCountryId(int customerId, int countryId)
        {
            try
            {
                var address =
                        _dbCon.Sp_GetCustomerShippingAddressessByCountryId_AdminR(customerId, countryId)
                            .Where(x => !string.IsNullOrWhiteSpace(x.CondensedAddress))
                            .Select(x => new GenericList()
                            {
                                Key = x.CustomerAddressID,
                                Value = x.CondensedAddress.Trim()
                            }).OrderBy(x => x.Value).ToList();
                return address;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerDestinationAddressByCountryId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerDestinationAddressByCountryId");
                }
                return new List<GenericList>();
            }
        }

        public List<GenericList> GetCustomerInvoiceCountries(int customerId)
        {
            try
            {
                var countries =
                        _dbCon.Sp_getCustomerInvoiceCountriesByCustomerId_AdminR(customerId).Select(x => new GenericList()
                        {
                            Key = x.CountryID,
                            Value = x.CountryName.Trim()
                        }).OrderBy(x => x.Value).ToList();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerInvoiceCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerInvoiceCountries");
                }
                return new List<GenericList>();
            }
        }


        public List<CustomerUserAddressPermissionModel> GetCustomerInvoiceAddressByUserIdAndDestinationId(
            int customerId, int userId,
            int destinationId, int countryId)
        {
            try
            {
                var chkExisting =
                        _dbCon.tblUserDestinationInvoiceLocations.Where(
                            x =>
                                x.CountryID == countryId && x.UserCompanyID == customerId && x.UserID == userId &&
                                x.CustomerDestinationID == destinationId).ToList();

                var address =
                    _dbCon.sp_GetCustomerInvoiceAddressByDestinationAddressId_AdminR(destinationId, countryId)
                        .Where(x => !string.IsNullOrWhiteSpace(x.CondensedAddress))
                        .Select(x => new CustomerUserAddressPermissionModel()
                        {

                            InVoiceAddressName = x.CondensedAddress.Trim(),
                            InvoiceAddressId = x.CustomerAddressID,
                            IsGlobal =
                                chkExisting.Any(p => p.CustomerInvoiceLocationID == x.CustomerAddressID && p.IsGlobal == 1),
                            IsSelected = chkExisting.Any(p => p.CustomerInvoiceLocationID == x.CustomerAddressID)

                        }).OrderBy(x => x.InVoiceAddressName).ToList();

                return address;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerInvoiceAddressByUserIdAndDestinationId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerInvoiceAddressByUserIdAndDestinationId");
                }
                return new List<CustomerUserAddressPermissionModel>();
            }
        }

        public bool UpdateUserAddressPermissions(List<CustomerUserAddressPermissionModel> setups,
            AdminSetting currentSetting)
        {
            try
            {
                var countryId = setups.FirstOrDefault().CountryId;
                var customerId = setups.FirstOrDefault().CustomerId;
                var userId = setups.FirstOrDefault().UserId;
                var destinationId = setups.FirstOrDefault().DestinationId;

                var deleteExisting =
                    _dbCon.tblUserDestinationInvoiceLocations.Where(
                        x =>
                            x.CountryID == countryId && x.UserCompanyID == customerId && x.UserID == userId &&
                            x.CustomerDestinationID == destinationId).ToList();

                _dbCon.tblUserDestinationInvoiceLocations.RemoveRange(deleteExisting);
                _dbCon.SaveChanges();

                if (setups.Any(x => x.InvoiceAddressId > 0 && x.IsSelected))
                {

                    var newData = setups.Select(x => new tblUserDestinationInvoiceLocation()
                    {
                        CountryID = x.CountryId,
                        UserCompanyID = x.CustomerId,
                        CustomerInvoiceLocationID = x.InvoiceAddressId,
                        LastUpdatedByUserID = currentSetting.UserID,
                        LastUpdated = DateTime.Now,
                        IsActive = 1,
                        UserID = userId,
                        IsGlobal = Convert.ToByte(x.IsGlobal),
                        CustomerDestinationID = x.DestinationId

                    }).ToList();

                    _dbCon.tblUserDestinationInvoiceLocations.AddRange(newData);
                    _dbCon.SaveChanges();

                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateUserAddressPermissions");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateUserAddressPermissions");


                }
                return false;
            }
        }


        public string GetUserEmail(int userId)
        {
            try
            {
                var getEmail = _dbCon.tblCustomerUsers.FirstOrDefault(x => x.UserID == userId);

                return getEmail.Email;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUserEmail");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUserEmail");
                }
                return "";
            }

        }

        public UserCredentialModel GetUserById(int userId, int companyId)
        {
            try
            {

                var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId);
                var company = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == companyId);
                var data = new UserCredentialModel()
                {
                    CompanyName = (company != null ? company.ParentCompanyName ?? "-" : "-"),
                    UserName = (user != null ? user.UserName ?? "-" : "-"),
                    Password = (user != null ? user.UserPassword ?? "-" : "-"),
                };
                return data;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUserEmail");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUserEmail");
                }
                return new UserCredentialModel();
            }
        }

        #endregion

        public List<sp_getCompanyByParentCompanyAndRoleName_AdminR_Result> GetCompanyByParentCompanyAndRoleName(
            string ParentCompanyName, string RoleName)
        {
            try
            {
                return _dbCon.sp_getCompanyByParentCompanyAndRoleName_AdminR().ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyByParentCompanyAndRoleName");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyByParentCompanyAndRoleName");
                }
                return new List<sp_getCompanyByParentCompanyAndRoleName_AdminR_Result>();
            }
        }

        public sp_GetAddressDetailByCustomer_AdminR_Result GetCustomerAddressByIdResult(int addressId, int customerId)
        {
            try
            {
                var address = _dbCon.sp_GetAddressDetailByCustomer_AdminR(customerId, addressId).FirstOrDefault();
                return address;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerAddressByIdResult");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerAddressByIdResult");
                }
                return new sp_GetAddressDetailByCustomer_AdminR_Result();
            }
        }

        public List<GenericList> GetTimeZoneList()
        {
            try
            {
                var timeZoneList = _dbCon.tblTimeZones.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.TimeZoneID,
                    Value = x.TimeZoneName.Trim()
                }).OrderBy(x => x.Value).ToList();
                return timeZoneList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTimeZoneList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTimeZoneList");
                }
                return new List<GenericList>();
            }
        }

        public List<GenericList> GetCompanyRegionsList(int companyId)
        {
            try
            {
                List<GenericList> regionList;
                var getCompanyRegions = _dbCon.tblRegions.Where(x => x.CompanyID == companyId).ToList();
                if (getCompanyRegions.Count > 0)
                {
                    regionList = getCompanyRegions.Select(x => new GenericList()
                    {
                        Key = x.RegionID,
                        Value = x.RegionName.Trim()
                    }).OrderBy(x => x.Value).ToList();

                }
                else
                {
                    regionList = _dbCon.tblRegions.Where(x => x.CompanyID == 15).Select(x => new GenericList()
                    {
                        Key = x.RegionID,
                        Value = x.RegionName.Trim()
                    }).OrderBy(x => x.Value).ToList();

                }

                return regionList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyRegionsList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyRegionsList");
                }
                return new List<GenericList>();
            }
        }


        public List<GenericList> GetCarriersList()
        {

            try
            {
                return _dbCon.tblFreightCarriers.Where(x => x.IsActive == 1).Select(x => new GenericList()
                {
                    Key = x.FreightCarrierID,
                    Value = x.FreightCarrierName

                }).OrderBy(x => x.Value).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCarriersList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCarriersList");
                }
                return new List<GenericList>();
            }
        }

        public bool UpdateCustomerAddress(CompanyCustomerModel data, AdminSetting currentSetting)
        {
            try
            {
                var customerAddress = data.Address;

                var tblcustomerAddress = new tblCustomerAddress()
                {
                    CustomerAddressID = customerAddress.CustomerAddressID,
                    CustomerAddressName = customerAddress.CustomerAddressName,
                    Address1 = customerAddress.Address1,
                    Address2 = customerAddress.Address2,
                    City = customerAddress.City,
                    PostalCode = customerAddress.PostalCode,
                    StateProvinceRegion = customerAddress.StateProvinceRegion,
                    CountryID = customerAddress.CountryID,
                    TimeZoneID = customerAddress.TimeZoneID,
                    CondensedAddress = customerAddress.CondensedAddress,
                    Notes = customerAddress.Notes,
                    RegionID = customerAddress.RegionID,
                    BuisnessSegment = customerAddress.BuisnessSegment
                };
                AddUpdateTblCustomerAddress(tblcustomerAddress, currentSetting.UserID);
                if (customerAddress.CdIsActive == 1)
                {
                    var customerDestination = new tblCustomerDestination()
                    {
                        CustomerDestinationID = customerAddress.CustomerAddressID,
                        Contact = customerAddress.CdContact,
                        Email = customerAddress.CdEmail,
                        Phone = customerAddress.CdPhone,
                        Fax = customerAddress.CdFax,
                        IsGlobal = Convert.ToByte(customerAddress.CdIsGlobal),
                        PreferredFreightCarrierID = Convert.ToInt32(customerAddress.PreferredFreightCarrierID),
                        DeliveryLocationInformation = customerAddress.DeliveryLocationInformation,
                        TertiaryReceiver = customerAddress.TertiaryReceiver,
                        PrimaryReceiver = customerAddress.PrimaryReceiver,
                        SecondaryReceiver = customerAddress.SecondaryReceiver,

                        PrimeContractorLocationID =
                            Convert.ToInt32(customerAddress.PrimeContractorLocationID) > 0
                                ? Convert.ToInt32(customerAddress.PrimeContractorLocationID)
                                : GetPcLocationId(data.Customer.CustomerID),
                    };


                    AddUpdateCustomerDestinationAddress(customerDestination, currentSetting.UserID);

                }
                else
                {
                    UpdateCustomerDestinationAddressPreference(tblcustomerAddress, currentSetting.UserID);
                }

                if (customerAddress.InIsActive == 1)
                {
                    var customerInvoiceLocation = new tblCustomerInvoiceLocation()
                    {
                        CustomerInvoiceLocationID = customerAddress.CustomerAddressID,
                        Contact = customerAddress.InContact,
                        Email = customerAddress.InEmail,
                        Phone = customerAddress.InPhone,
                        Fax = customerAddress.InFax,
                        IsGlobal = Convert.ToByte(customerAddress.InIsGlobal),
                        AccountNumber = customerAddress.AccountNumber,

                    };


                    AddUpdateCustomerInvoiceLocation(customerInvoiceLocation, currentSetting.UserID);

                }
                else
                {
                    UpdateCustomerInvoiceLocationPreference(tblcustomerAddress, currentSetting.UserID);
                }

                UpdateCustomerXrefLocations(data.Customer.CustomerID, customerAddress.CustomerAddressID,
                    customerAddress.XrefCode, currentSetting.UserID);


                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerAddress");
                }
                return false;
            }

        }

        public int GetPcLocationId(int customerId)
        {
            try
            {
                var getPcId = _dbCon.tblCustomers.FirstOrDefault(c => c.CustomerID == customerId).PrimeContractorID;
                if (getPcId != null && getPcId > 0)
                {
                    return getPcId;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPcLocationId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPcLocationId");
                }
                return 0;
            }
        }

        public tblCustomerAddress AddUpdateTblCustomerAddress(tblCustomerAddress model, int updatedUserId)
        {
            try
            {
                var customerAddress = new tblCustomerAddress();

                if (model.CustomerAddressID > 0)
                {

                    customerAddress =
                        _dbCon.tblCustomerAddresses.FirstOrDefault(x => x.CustomerAddressID == model.CustomerAddressID);
                    if (customerAddress != null)
                    {
                        customerAddress.CustomerAddressName = model.CustomerAddressName ?? "-";
                        customerAddress.Address1 = model.Address1 ?? "-";
                        customerAddress.Address2 = model.Address2 ?? "-";
                        customerAddress.City = model.City ?? "-";
                        customerAddress.StateProvinceRegion = model.StateProvinceRegion ?? "-";
                        customerAddress.CondensedAddress = model.CondensedAddress ?? "-";
                        customerAddress.Notes = model.Notes ?? "-";
                        customerAddress.PostalCode = model.PostalCode ?? "-";
                        customerAddress.LastUpdated = DateTime.Now;
                        customerAddress.LastUpdatedbyUserID = updatedUserId;
                        customerAddress.BuisnessSegment = model.BuisnessSegment;
                        customerAddress.CountryID = model.CountryID;
                        customerAddress.TimeZoneID = model.TimeZoneID;
                        customerAddress.RegionID = model.RegionID;

                        _dbCon.Entry(customerAddress).State = System.Data.Entity.EntityState.Modified;
                    }
                    _dbCon.SaveChanges();

                }
                else
                {
                    customerAddress.CustomerAddressName = model.CustomerAddressName ?? "-";
                    customerAddress.CustomerID = model.CustomerID;
                    customerAddress.Address1 = model.Address1 ?? "-";
                    customerAddress.Address2 = model.Address2 ?? "-";
                    customerAddress.City = model.City ?? "-";
                    customerAddress.IsActive = 1;
                    customerAddress.StateProvinceRegion = model.StateProvinceRegion ?? "-";
                    customerAddress.CondensedAddress = model.CondensedAddress ?? "-";
                    customerAddress.Notes = model.Notes ?? "-";
                    customerAddress.PostalCode = model.PostalCode ?? "-";
                    customerAddress.LastUpdated = DateTime.Now;
                    customerAddress.LastUpdatedbyUserID = updatedUserId;
                    customerAddress.CountryID = model.CountryID;
                    customerAddress.TimeZoneID = model.TimeZoneID;
                    customerAddress.BuisnessSegment = model.BuisnessSegment;
                    customerAddress.RegionID = model.RegionID;
                    _dbCon.tblCustomerAddresses.Add(customerAddress);
                    _dbCon.SaveChanges();
                }
                return customerAddress;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateTblCustomerAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateTblCustomerAddress");
                }
                return new tblCustomerAddress();
            }
        }

        public tblCustomerDestination AddUpdateCustomerDestinationAddress(tblCustomerDestination model,
            int updatedUserId)
        {
            try
            {
                var customerDestination = new tblCustomerDestination();
                customerDestination =
                    _dbCon.tblCustomerDestinations.FirstOrDefault(
                        x => x.CustomerDestinationID == model.CustomerDestinationID);
                if (customerDestination != null)
                {


                    customerDestination.Phone = model.Phone ?? "-";
                    customerDestination.Fax = model.Fax ?? "-";
                    customerDestination.Contact = model.Contact ?? "-";
                    customerDestination.Email = model.Email ?? "-";
                    customerDestination.PreferredFreightCarrierID = model.PreferredFreightCarrierID;
                    customerDestination.DeliveryLocationInformation = model.DeliveryLocationInformation ?? "-";
                    customerDestination.TertiaryReceiver = model.TertiaryReceiver ?? "-";
                    customerDestination.PrimaryReceiver = model.PrimaryReceiver ?? "-";
                    customerDestination.SecondaryReceiver = model.SecondaryReceiver ?? "-";
                    customerDestination.LastUpdated = DateTime.Now;
                    customerDestination.LastUpdatedByUserID = updatedUserId;
                    customerDestination.IsActive = 1;
                    customerDestination.PrimeContractorLocationID = model.PrimeContractorLocationID;
                    customerDestination.IsGlobal = model.IsGlobal;
                    _dbCon.Entry(customerDestination).State = System.Data.Entity.EntityState.Modified;

                    _dbCon.SaveChanges();

                }
                else
                {
                    customerDestination = new tblCustomerDestination();
                    customerDestination.CustomerDestinationID = model.CustomerDestinationID;
                    customerDestination.Phone = model.Phone ?? "-";
                    customerDestination.Fax = model.Fax ?? "-";
                    customerDestination.Contact = model.Contact ?? "-";
                    customerDestination.Email = model.Email ?? "-";
                    customerDestination.PreferredFreightCarrierID = model.PreferredFreightCarrierID;
                    customerDestination.DeliveryLocationInformation = model.DeliveryLocationInformation ?? "-";
                    customerDestination.TertiaryReceiver = model.TertiaryReceiver ?? "-";
                    customerDestination.PrimaryReceiver = model.PrimaryReceiver ?? "-";
                    customerDestination.SecondaryReceiver = model.SecondaryReceiver ?? "-";
                    customerDestination.LastUpdated = DateTime.Now;
                    customerDestination.IsActive = 1;
                    customerDestination.LastUpdatedByUserID = updatedUserId;
                    customerDestination.PrimeContractorLocationID = model.PrimeContractorLocationID;
                    customerDestination.IsGlobal = model.IsGlobal;

                    _dbCon.tblCustomerDestinations.Add(customerDestination);
                    _dbCon.SaveChanges();
                }
                return customerDestination;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateCustomerDestinationAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateCustomerDestinationAddress");
                }
                return new tblCustomerDestination();
            }
        }

        public bool UpdateCustomerDestinationAddressPreference(tblCustomerAddress model, int userId)
        {
            try
            {
                var customerUser =
                        _dbCon.tblCustomerUsers.FirstOrDefault(x => x.UserDefaultAddressID == model.CustomerAddressID);
                if (customerUser != null)
                {
                    customerUser.UserDefaultAddressID = null;
                    _dbCon.Entry(customerUser).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                }

                var sfaCustomerContacts =
                    _dbCon.tblSFACustomerContacts.Where(x => x.CustomerDestinationID == model.CustomerAddressID).ToList();
                if (sfaCustomerContacts.Count > 0)
                {
                    _dbCon.tblSFACustomerContacts.RemoveRange(sfaCustomerContacts);
                    _dbCon.SaveChanges();
                }


                var tblSfaPreferred =
                    _dbCon.tblSFAPreferreds.Where(x => x.CustomerDestinationID == model.CustomerAddressID).ToList();
                if (tblSfaPreferred.Count > 0)
                {
                    _dbCon.tblSFAPreferreds.RemoveRange(tblSfaPreferred);
                    _dbCon.SaveChanges();
                }

                var tblSfaCustomerWa =
                    _dbCon.tblSFACustomerWAs.Where(x => x.CustomerDestinationID == model.CustomerAddressID).ToList();
                if (tblSfaCustomerWa.Count > 0)
                {
                    _dbCon.tblSFACustomerWAs.RemoveRange(tblSfaCustomerWa);
                    _dbCon.SaveChanges();
                }


                var tblXrefCustomerDefaultAddress =
                    _dbCon.tblXrefCustomerDefaultAddresses.Where(x => x.DestinationAddressID == model.CustomerAddressID)
                        .ToList();

                if (tblXrefCustomerDefaultAddress.Count > 0)
                {
                    _dbCon.tblXrefCustomerDefaultAddresses.RemoveRange(tblXrefCustomerDefaultAddress);
                    _dbCon.SaveChanges();
                }


                var tblUserDestinationInvoiceLocations =
                    _dbCon.tblUserDestinationInvoiceLocations.Where(x => x.CustomerDestinationID == model.CustomerAddressID)
                        .ToList();

                if (tblUserDestinationInvoiceLocations.Count > 0)
                {
                    _dbCon.tblUserDestinationInvoiceLocations.RemoveRange(tblUserDestinationInvoiceLocations);
                    _dbCon.SaveChanges();
                }

                var tblDestinationInvoiceLocations =
                    _dbCon.tblDestinationInvoiceLocations.Where(x => x.CustomerDestinationID == model.CustomerAddressID)
                        .ToList();

                if (tblDestinationInvoiceLocations.Count > 0)
                {
                    _dbCon.tblDestinationInvoiceLocations.RemoveRange(tblDestinationInvoiceLocations);
                    _dbCon.SaveChanges();
                }
                var tblCustomerDestinations =
                    _dbCon.tblCustomerDestinations.Where(x => x.CustomerDestinationID == model.CustomerAddressID).ToList();
                if (tblCustomerDestinations.Count > 0)
                {
                    _dbCon.tblCustomerDestinations.RemoveRange(tblCustomerDestinations);
                    _dbCon.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerDestinationAddressPreference");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerDestinationAddressPreference");
                }
                return false;
            }
        }

        public tblCustomerInvoiceLocation AddUpdateCustomerInvoiceLocation(tblCustomerInvoiceLocation model,
            int updatedUserId)
        {
            try
            {
                var customerInvoiceLocation = new tblCustomerInvoiceLocation();

                customerInvoiceLocation =
                    _dbCon.tblCustomerInvoiceLocations.FirstOrDefault(
                        x => x.CustomerInvoiceLocationID == model.CustomerInvoiceLocationID);

                if (customerInvoiceLocation != null)
                {


                    customerInvoiceLocation.Phone = model.Phone ?? "-";
                    customerInvoiceLocation.Fax = model.Fax ?? "-";
                    customerInvoiceLocation.Contact = model.Contact ?? "-";
                    customerInvoiceLocation.Email = model.Email ?? "-";
                    customerInvoiceLocation.AccountNumber = model.AccountNumber ?? "-";
                    customerInvoiceLocation.LastUpdated = DateTime.Now;
                    customerInvoiceLocation.LastUpdatedByUserID = updatedUserId;
                    customerInvoiceLocation.IsActive = 1;

                    customerInvoiceLocation.IsGlobal = model.IsGlobal;
                    _dbCon.Entry(customerInvoiceLocation).State = System.Data.Entity.EntityState.Modified;

                    _dbCon.SaveChanges();

                }
                else
                {
                    customerInvoiceLocation = new tblCustomerInvoiceLocation();
                    customerInvoiceLocation.CustomerInvoiceLocationID = model.CustomerInvoiceLocationID;
                    customerInvoiceLocation.Phone = model.Phone ?? "-";
                    customerInvoiceLocation.Fax = model.Fax ?? "-";
                    customerInvoiceLocation.Contact = model.Contact ?? "-";
                    customerInvoiceLocation.Email = model.Email ?? "-";
                    customerInvoiceLocation.AccountNumber = model.AccountNumber ?? "-";
                    customerInvoiceLocation.LastUpdated = DateTime.Now;
                    customerInvoiceLocation.LastUpdatedByUserID = updatedUserId;
                    customerInvoiceLocation.IsActive = 1;
                    customerInvoiceLocation.IsGlobal = model.IsGlobal;

                    _dbCon.tblCustomerInvoiceLocations.Add(customerInvoiceLocation);
                    _dbCon.SaveChanges();
                }
                return customerInvoiceLocation;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateCustomerInvoiceLocation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateCustomerInvoiceLocation");
                }
                return new tblCustomerInvoiceLocation();
            }
        }

        public bool UpdateCustomerInvoiceLocationPreference(tblCustomerAddress model, int userId)
        {

            try
            {
                var tblXrefCustomerDefaultAddress =
                        _dbCon.tblXrefCustomerDefaultAddresses.Where(x => x.DestinationAddressID == model.CustomerAddressID)
                            .ToList();

                if (tblXrefCustomerDefaultAddress.Count > 0)
                {
                    _dbCon.tblXrefCustomerDefaultAddresses.RemoveRange(tblXrefCustomerDefaultAddress);
                    _dbCon.SaveChanges();
                }

                var tblUserDestinationInvoiceLocations =
                        _dbCon.tblUserDestinationInvoiceLocations.Where(x => x.CustomerInvoiceLocationID == model.CustomerAddressID)
                            .ToList();
                if (tblUserDestinationInvoiceLocations.Count > 0)
                {
                    _dbCon.tblUserDestinationInvoiceLocations.RemoveRange(tblUserDestinationInvoiceLocations);
                    _dbCon.SaveChanges();
                }

                var tblDestinationInvoiceLocations =
                    _dbCon.tblDestinationInvoiceLocations.Where(x => x.CustomerInvoiceLocationID == model.CustomerAddressID)
                        .ToList();
                if (tblDestinationInvoiceLocations.Count > 0)
                {
                    _dbCon.tblDestinationInvoiceLocations.RemoveRange(tblDestinationInvoiceLocations);
                    _dbCon.SaveChanges();
                }
                var tblCustomerInvoiceLocations =
                    _dbCon.tblCustomerInvoiceLocations.Where(x => x.CustomerInvoiceLocationID == model.CustomerAddressID)
                        .ToList();
                if (tblCustomerInvoiceLocations.Count > 0)
                {
                    _dbCon.tblCustomerInvoiceLocations.RemoveRange(tblCustomerInvoiceLocations);
                    _dbCon.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerInvoiceLocationPreference");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerInvoiceLocationPreference");
                }
                return false;
            }
        }

        public bool UpdateCustomerXrefLocations(int customerId, int customerAddressId, string xrefCode, int userId)
        {
            try
            {
                var companyId = _dbCon.tblCustomers.FirstOrDefault(c => c.CustomerID == customerId) != null ? _dbCon.tblCustomers.FirstOrDefault(c => c.CustomerID == customerId).ParentCompanyID : 0;

                if (!string.IsNullOrWhiteSpace(xrefCode))
                {

                    var chkXref =
                        _dbCon.tblXrefLocations.FirstOrDefault(
                            x => x.CompanyID == companyId && x.CustomerAddressID == customerAddressId);
                    if (chkXref != null)
                    {
                        chkXref.XrefCode = xrefCode;
                        chkXref.LastUpdated = DateTime.Now;
                        chkXref.LastUpdatedByUserID = userId;
                        _dbCon.Entry(chkXref).State = System.Data.Entity.EntityState.Modified;
                        _dbCon.SaveChanges();
                    }
                    else
                    {
                        var tblXrefLocations = new tblXrefLocation()
                        {
                            CompanyID = companyId,
                            CustomerAddressID = customerAddressId,
                            IsActive = true,
                            XrefCode = xrefCode,
                            LastUpdated = DateTime.Now,
                            LastUpdatedByUserID = userId,

                        };
                        _dbCon.tblXrefLocations.Add(tblXrefLocations);

                        _dbCon.SaveChanges();
                    }
                }
                else
                {
                    var chkXref =
                        _dbCon.tblXrefLocations.FirstOrDefault(
                            x => x.CompanyID == companyId && x.CustomerAddressID == customerAddressId);
                    if (chkXref != null)
                    {
                        _dbCon.tblXrefLocations.Remove(chkXref);
                        _dbCon.SaveChanges();
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerXrefLocations");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerXrefLocations");
                }
                return false;
            }
        }

        public bool AddCustomerAddress(CompanyCustomerModel data, AdminSetting currentSetting)
        {
            try
            {
                var customerAddress = data.Address;

                var tblcustomerAddress = new tblCustomerAddress()
                {
                    CustomerAddressName = customerAddress.CustomerAddressName,
                    Address1 = customerAddress.Address1,
                    Address2 = customerAddress.Address2,
                    City = customerAddress.City,
                    PostalCode = customerAddress.PostalCode,
                    StateProvinceRegion = customerAddress.StateProvinceRegion,
                    CountryID = customerAddress.CountryID,
                    TimeZoneID = customerAddress.TimeZoneID,
                    CondensedAddress = customerAddress.CondensedAddress,
                    Notes = customerAddress.Notes,
                    CustomerID = data.Customer.CustomerID,
                    RegionID = customerAddress.RegionID,
                    BuisnessSegment = customerAddress.BuisnessSegment
                };


                var newCustomerAddress = AddUpdateTblCustomerAddress(tblcustomerAddress, currentSetting.UserID);
                if (newCustomerAddress != null && newCustomerAddress.CustomerAddressID > 0)
                {
                    if (customerAddress.CdIsActive == 1)
                    {
                        customerAddress.CustomerAddressID = newCustomerAddress.CustomerAddressID;

                        var customerDestination = new tblCustomerDestination()
                        {
                            CustomerDestinationID = newCustomerAddress.CustomerAddressID,
                            Contact = customerAddress.CdContact,
                            Email = customerAddress.CdEmail,
                            Phone = customerAddress.CdPhone,
                            Fax = customerAddress.CdFax,
                            IsActive = 1,
                            IsGlobal = Convert.ToByte(customerAddress.CdIsGlobal),
                            PreferredFreightCarrierID = Convert.ToInt32(customerAddress.PreferredFreightCarrierID),
                            DeliveryLocationInformation = customerAddress.DeliveryLocationInformation,
                            TertiaryReceiver = customerAddress.TertiaryReceiver,
                            PrimaryReceiver = customerAddress.PrimaryReceiver,
                            SecondaryReceiver = customerAddress.SecondaryReceiver,
                            PrimeContractorLocationID =
                                Convert.ToInt32(customerAddress.PrimeContractorLocationID) > 0
                                    ? Convert.ToInt32(customerAddress.PrimeContractorLocationID)
                                    : GetPcLocationId(data.Customer.CustomerID),
                        };
                        AddUpdateCustomerDestinationAddress(customerDestination, currentSetting.UserID);

                    }
                    if (customerAddress.InIsActive == 1)
                    {
                        var customerInvoiceLocation = new tblCustomerInvoiceLocation()
                        {
                            CustomerInvoiceLocationID = newCustomerAddress.CustomerAddressID,
                            Contact = customerAddress.InContact,
                            Email = customerAddress.InEmail,
                            Phone = customerAddress.InPhone,
                            Fax = customerAddress.InFax,
                            IsActive = 1,
                            IsGlobal = Convert.ToByte(customerAddress.InIsGlobal),
                            AccountNumber = customerAddress.AccountNumber,

                        };
                        AddUpdateCustomerInvoiceLocation(customerInvoiceLocation, currentSetting.UserID);

                    }
                    UpdateCustomerXrefLocations(data.Customer.CustomerID, newCustomerAddress.CustomerAddressID,
                        customerAddress.XrefCode, currentSetting.UserID);
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddCustomerAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddCustomerAddress");
                }
                return false;
            }

        }

        public Object GetCustomerShippedAddressDetailByAddressId(int addressId, int customerId)
        {
            try
            {
                var countryService = new CountryPresenter();
                var getDetials =
                    _dbCon.tblCustomerAddresses.FirstOrDefault(
                        x => x.CustomerID == customerId && x.CustomerAddressID == addressId);

                var getCountryIdWithSelectedBillingAddress = 0;

                var chkBillingCountryId = _dbCon.sp_GetFirstCountryWithBillToAddress_AdminR(addressId).FirstOrDefault();
                if (chkBillingCountryId != null)
                {
                    getCountryIdWithSelectedBillingAddress = Convert.ToInt32(chkBillingCountryId);
                }

                if (getDetials != null)
                {
                    var data = new
                    {
                        AddressId = getDetials.CustomerAddressID,
                        AddressName = getDetials.CustomerAddressName ?? "-",
                        Address1 = getDetials.Address1 ?? "-",
                        Address2 = getDetials.Address2 ?? "-",
                        City = getDetials.City ?? "-",
                        StatProvice = getDetials.StateProvinceRegion ?? "-",
                        PostalCode = getDetials.PostalCode ?? "-",
                        ConAddress = getDetials.CondensedAddress ?? "-",
                        Country = countryService.GetCountryById(getDetials.CountryID).CountryName ?? "-",
                        CountryIdToSelect = getCountryIdWithSelectedBillingAddress
                    };
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerShippedAddressDetailByAddressId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerShippedAddressDetailByAddressId");
                }
                return null;
            }
        }


        public List<CustomerUserAddressPermissionModel> GetCustomerInvoiceAddressesDestinationId(int customerId, int destinationId, int countryId)
        {
            try
            {
                var chkExistingInvoiceLocations =
                        _dbCon.tblDestinationInvoiceLocations.Where(x => x.CustomerDestinationID == destinationId).ToList();

                var address =
                    _dbCon.sp_GetCustomerInvoiceAddressList_AdminR(customerId, countryId)
                        .Where(x => !string.IsNullOrWhiteSpace(x.CondensedAddress))
                        .Select(x => new CustomerUserAddressPermissionModel()
                        {

                            InVoiceAddressName = x.CondensedAddress.Trim(),
                            InvoiceAddressId = x.CustomerAddressID,
                            IsSelected = chkExistingInvoiceLocations.Any(p => p.CustomerInvoiceLocationID == x.CustomerAddressID)

                        }).OrderBy(x => x.InVoiceAddressName).ToList();

                return address;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerInvoiceAddressesDestinationId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerInvoiceAddressesDestinationId");
                }
                return new List<CustomerUserAddressPermissionModel>();
            }
        }

        public bool UpdateCustomerInvoiceAddress(List<CustomerUserAddressPermissionModel> setups,
           AdminSetting currentSetting)
        {
            try
            {

                var customerId = setups.FirstOrDefault().CustomerId;
                var destinationId = setups.FirstOrDefault().DestinationId;
                var getInvoiceAddressByCountryId = _dbCon.sp_GetCustomerInvoiceAddressList_AdminR(customerId, setups.FirstOrDefault().CountryId).ToList();

                if (getInvoiceAddressByCountryId != null && getInvoiceAddressByCountryId.Count > 0)
                {
                    var selectInvoiceIds = getInvoiceAddressByCountryId.Select(x => x.CustomerAddressID);

                    var deleteExisting =
                        _dbCon.tblDestinationInvoiceLocations.Where(
                            x => x.CustomerDestinationID == destinationId && selectInvoiceIds.Contains(x.CustomerInvoiceLocationID)).ToList();

                    _dbCon.tblDestinationInvoiceLocations.RemoveRange(deleteExisting);
                    _dbCon.SaveChanges();
                }


                if (setups.Any(x => x.InvoiceAddressId > 0 && x.IsSelected))
                {

                    var newData = setups.Select(x => new tblDestinationInvoiceLocation()
                    {

                        CustomerDestinationID = destinationId,
                        CustomerInvoiceLocationID = x.InvoiceAddressId,
                        IsActive = 1,
                        LastUpdated = DateTime.Now,
                        LastUpdatedByUserID = currentSetting.UserID,
                    }).ToList();

                    _dbCon.tblDestinationInvoiceLocations.AddRange(newData);
                    _dbCon.SaveChanges();

                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerInvoiceAddress");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerInvoiceAddress");


                }
                return false;
            }
        }

        public int GetUserContactIdbyUserId(int userId)
        {

            try
            {
                var chk = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == userId);
                if (chk != null)
                {
                    return chk.ContactID;
                }
                return 0;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUserContactIdbyUserId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUserContactIdbyUserId");
                }
                return 0;
            }
        }

        //Customer additional setup start

        public List<GenericList> GetCustomerDestinationCounrtryList(int customerId)
        {
            try
            {
                var countries = _dbCon.Sp_GetCustomerDestinationCountries_AdminR(customerId).Select(x => new GenericList()
                {
                    Value = x.CountryName,
                    Key = x.CountryId
                }).OrderBy(x => x.Value).ToList();

                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerDestinationCounrtryList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerDestinationCounrtryList");
                }
                return new List<GenericList>();
            }
        }

        public List<GenericList> GetPreferredSUppliers(string basedOn, int basedOnId, int manufacturerId)
        {

            try
            {
                var supplierList = _dbCon.Sp_GetGspPreferencesSuppliers_AdminR(basedOn, basedOnId, manufacturerId)
                          .Select(x => new GenericList()
                          {
                              Key = x.SfaId,
                              Value = x.SfaName

                          }).OrderBy(x => x.Value).ToList();

                return supplierList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPreferredSUppliers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPreferredSUppliers");
                }
                return new List<GenericList>();
            }

        }

        public int GetSelectedSupplierId(int manufacturerId, int countryId, int customerId)
        {
            try
            {
                var supplierId = 0;
                var chk = _dbCon.Sp_GetSelectedSupplierForCountryByManufacturer_AdminR(manufacturerId, customerId,
                    countryId).Select(x => x.SfaId).FirstOrDefault();
                if (chk != null && chk > 0)
                {
                    supplierId = chk;
                }

                return supplierId;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedSupplierId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedSupplierId");
                }
                return 0;
            }

        }

        public int GetSelectedSupplierForAddressId(int manufacturerId, int destinationId, int customerId)
        {
            try
            {
                var supplierId = 0;
                var chk = _dbCon.Sp_GetSelectedSupplierForAddressByManufacturer_AdminR(manufacturerId, customerId, destinationId).FirstOrDefault();
                if (chk != null && chk > 0)
                {
                    supplierId = Convert.ToInt32(chk);
                }

                return supplierId;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedSupplierForAddressId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedSupplierForAddressId");
                }
                return 0;
            }

        }

        public bool UpdateSupplierGenPreference(int customerId, int manufacturerId, int countryId, int sfaId, int userId)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 360;
                var chk = _dbCon.Sp_UpdateSupplierGenPreference_AdminR(customerId, manufacturerId, countryId, userId,
                    sfaId);
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSupplierGenPreference");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSupplierGenPreference");
                }
                return false;
            }
        }


        public List<GenericList> GetCustomerAddressesWithCountryName(int customerId, int? countryId = null)
        {
            try
            {
                var addressList = _dbCon.Sp_GetCustomerAddressesByCustomerId_AdminR(customerId, countryId).Select(x => new GenericList()
                {
                    Key = x.CustomerAddressId,
                    Value = (!string.IsNullOrWhiteSpace(x.CondensedAddress) ? x.CondensedAddress.Trim() + " (" + x.CountryName + ")" : "-" + " (" + x.CountryName + ")")
                }).ToList();
                return addressList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerAddressesWithCountryName");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerAddressesWithCountryName");
                }
                return new List<GenericList>();
            }
        }

        public bool UpdateSupplierPreferenceByDestinations(int customerId, int manufacturerId, int destinationId, int sfaId, int userId)
        {
            try
            {
                var chk = _dbCon.Sp_UpdateSupplierPrefByDestination_AdminR(customerId, manufacturerId, destinationId, userId,
                    sfaId);
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSupplierPreferenceByDestinations");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSupplierPreferenceByDestinations");
                }
                return false;
            }
        }

        public List<GenericList> GetCustomerAddressCountries(int customerId)
        {
            try
            {
                var countryList = _dbCon.Sp_GetCustomerCountryByAddress_AdminR(customerId).Select(x => new GenericList()
                {
                    Key = x.CountryID,
                    Value = x.CountryName
                }).OrderBy(x => x.Value).ToList();
                return countryList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerAddressCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerAddressCountries");
                }
                return new List<GenericList>();
            }
        }

        public List<GenericList> GetCustomerAddressesByCountry(int customerId, int countryId)
        {
            try
            {
                var countryList = _dbCon.Sp_GetCustomerAddressesByCountry_AdminR(customerId, countryId).Select(x => new GenericList()
                {
                    Key = x.customerdestinationid,
                    Value = x.condensedaddress + "(" + x.countryname + ")"
                }).OrderBy(x => x.Value).ToList();
                return countryList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerAddressCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerAddressCountries");
                }
                return new List<GenericList>();
            }
        }


        public List<GenericList> GetCountryPreferedSupplierList(int customerId, int countryId)
        {
            try
            {
                var sfaList = _dbCon.sp_GetCustomerPreferedSuppliersByCountryId_AdminR(customerId, countryId).Select(x => new GenericList()
                {
                    Key = x.SFAID,
                    Value = x.SFAName
                }).OrderBy(x => x.Value).ToList();
                return sfaList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCountryPreferedSupplierList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountryPreferedSupplierList");
                }
                return new List<GenericList>();
            }
        }


        public Sp_GetCustomerCountrySfaUpliftsBySfaId_AdminR_Result GetSupplierStandardUpliftsBySfaId(int customerId, int countryId, int sfaId, int primecontractorId)
        {
            try
            {
                var sfaUplifts = _dbCon.Sp_GetCustomerCountrySfaUpliftsBySfaId_AdminR(customerId, countryId, sfaId,
                    primecontractorId).FirstOrDefault();
                return sfaUplifts;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierStandardUpliftsBySfaId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierStandardUpliftsBySfaId");
                }
                return new Sp_GetCustomerCountrySfaUpliftsBySfaId_AdminR_Result();
            }
        }

        public bool AddUpdateSfaUplifts(int customerId, int countryId, int sfaId, int primecontractorId, decimal sfaStandardUplifts, decimal pcUplifts, bool isPrefered, int userId)
        {
            try
            {
                var deleteCustomerUplifts = _dbCon.Sp_DeleteExistingUpliftsByCustomer_AdminR(customerId, countryId, 1);
                decimal standardUplifts = 0, pcUpliftsCon = 0;

                if (sfaStandardUplifts > 0) standardUplifts = Convert.ToDecimal(Convert.ToDecimal(sfaStandardUplifts) / 100);
                if (pcUplifts > 0) pcUpliftsCon = Convert.ToDecimal(Convert.ToDecimal(pcUplifts) / 100);


                if (isPrefered)
                {
                    var updateAllSfa =
                        _dbCon.tblSFAStandardUplifts.Where(
                            x => x.CustomerID == customerId && x.CountryID == countryId && x.SFAID != sfaId)
                            .ToList();
                    updateAllSfa.ForEach(x =>
                    {
                        x.IsPreferred = false;

                    });
                    if (updateAllSfa.Count > 0)
                    {
                        foreach (var item in updateAllSfa)
                        {
                            _dbCon.Entry(item).State = System.Data.Entity.EntityState.Modified;
                            _dbCon.SaveChanges();
                        }

                    }

                }
                var chkExisting =
                    _dbCon.tblSFAStandardUplifts.FirstOrDefault(
                        x =>
                            x.CustomerID == customerId && x.CountryID == countryId && x.SFAID == sfaId &&
                            x.PrimeContractorID == primecontractorId);

                if (chkExisting != null)
                {
                    chkExisting.PrimeContractorUplift = pcUpliftsCon;
                    chkExisting.SFAStandardUplift = standardUplifts;
                    chkExisting.LastUpdated = DateTime.Now;
                    chkExisting.LastUpdatedByUserID = userId;
                    chkExisting.IsActive = 1;
                    chkExisting.IsPreferred = isPrefered;
                    _dbCon.Entry(chkExisting).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                }
                else
                {
                    var newUlift = new tblSFAStandardUplifts()
                    {
                        CountryID = countryId,
                        CustomerID = customerId,
                        SFAID = sfaId,
                        IsActive = 1,
                        LastUpdated = DateTime.Now,
                        LastUpdatedByUserID = userId,
                        PrimeContractorUplift = pcUpliftsCon,
                        SFAStandardUplift = standardUplifts,
                        IsPreferred = isPrefered,
                        PrimeContractorID = primecontractorId,
                    };
                    _dbCon.tblSFAStandardUplifts.Add(newUlift);
                    _dbCon.SaveChanges();
                }


                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateSfaUplifts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateSfaUplifts");
                }
                return false;
            }
        }

        public List<GenericList> GetManufacturersList(string searchTxt)
        {
            try
            {
                var manuList = _dbCon.tblManufacturers.Where(
                        x =>
                            x.IsActive)
                        .Select(x => new GenericList()
                        {
                            Key = x.ManufacturerID,
                            Value = x.ManufacturerName.Trim()

                        }).ToList();
                return manuList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManufacturersList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturersList");
                }
                return new List<GenericList>();
            }
        }

        public tblTradeBlockUplifts GetCustomerTbuUplifts(int customerId, int countryId, int manufacturerId)
        {
            try
            {
                var tbuUplift =
                        _dbCon.tblTradeBlockUplifts.FirstOrDefault(
                            x => x.CustomerID == customerId && x.CountryID == countryId && x.ManufacturerID == manufacturerId);

                return tbuUplift;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerTbuUplifts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerTbuUplifts");
                }
                return null;
            }
        }


        public bool AddTradeBlockUplifts(int customerId, int countryId, int manufacturerId, decimal blockUplifts, decimal duties, int userId)
        {
            try
            {
                var chkExisting =
                    _dbCon.tblTradeBlockUplifts.FirstOrDefault(
                        x => x.CustomerID == customerId && x.CountryID == countryId && x.ManufacturerID == manufacturerId);
                if (chkExisting != null)
                {
                    _dbCon.tblTradeBlockUplifts.Remove(chkExisting);
                    _dbCon.SaveChanges();
                }

                decimal tradeBlockUplifts = 0, dutiesPer = 0;

                if (blockUplifts > 0) tradeBlockUplifts = Convert.ToDecimal(Convert.ToDecimal(blockUplifts) / 100);
                if (duties > 0) dutiesPer = Convert.ToDecimal(Convert.ToDecimal(duties) / 100);

                var newData = new tblTradeBlockUplifts();

                newData.CountryID = countryId;
                newData.ManufacturerID = manufacturerId;
                newData.IsActive = 1;
                newData.CustomerID = customerId;
                newData.LastUpdated = DateTime.Now;

                if (dutiesPer > 0) newData.Duties = dutiesPer;
                newData.LastUpdatedByUserID = userId;
                if (tradeBlockUplifts > 0) newData.TradeBlockUplift = tradeBlockUplifts;

                _dbCon.tblTradeBlockUplifts.Add(newData);
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddTradeBlockUplifts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddTradeBlockUplifts");
                }
                return false;
            }
        }


        public List<Sp_GetSpecialUpliftsByCountryId_AdminR_Result> Sp_GetSpecialUpliftsByCountryId(int customerId, int countryId)
        {
            try
            {
                var specialUpliftsList = _dbCon.Sp_GetSpecialUpliftsByCountryId_AdminR(customerId, countryId).ToList();
                return specialUpliftsList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "Sp_GetSpecialUpliftsByCountryId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "Sp_GetSpecialUpliftsByCountryId");
                }
                return new List<Sp_GetSpecialUpliftsByCountryId_AdminR_Result>();
            }
        }

        public List<GenericList> GetSpecialUpliftsManufacturersBySupplier(int customerId, int countryId, int sfaId)
        {
            try
            {
                var sfaList = _dbCon.sp_GetCountryManufacturersForSpecialUpliftsBySfaId_AdminR(customerId, countryId, sfaId).Select(x => new GenericList()
                {
                    Key = x.ManufacturerId,
                    Value = x.ManufacturerName
                }).OrderBy(x => x.Value).ToList();
                return sfaList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSpecialUpliftsManufacturersBySupplier");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSpecialUpliftsManufacturersBySupplier");
                }
                return new List<GenericList>();
            }
        }

        public bool AddSpecialUplifts(int customerId, int countryId, int sfaId, int manfId, int supplierUplifts, int gsUplifts, int userId)
        {
            try
            {

                decimal spSupplierUplifts = 0, spGsUplifts = 0;

                if (supplierUplifts > 0) spSupplierUplifts = Convert.ToDecimal(Convert.ToDecimal(supplierUplifts) / 100);
                if (gsUplifts > 0) spGsUplifts = Convert.ToDecimal(Convert.ToDecimal(gsUplifts) / 100);

                var manfSfaUplifts = new tblManufacturerSFAUplift()
                {
                    CustomerID = customerId,
                    ManufacturerID = manfId,
                    SFAID = sfaId,
                    CountryID = countryId,
                    LastUpdated = DateTime.Now,
                    LastUpdatedByUserID = userId,
                    PrimeContractorID = 1,
                    GlobalserveUplift = spGsUplifts,
                    ManufacturerUplift = spSupplierUplifts

                };
                _dbCon.tblManufacturerSFAUplift.Add(manfSfaUplifts);
                _dbCon.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddSpecialUplifts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSpecialUplifts");
                }
                return false;
            }
        }

        public bool UpdateSpecialUplifts(int sfaUpliftId, int supplierUplifts, int gsUplifts, int userId)
        {
            try
            {

                decimal spSupplierUplifts = 0, spGsUplifts = 0;

                if (supplierUplifts > 0) spSupplierUplifts = Convert.ToDecimal(Convert.ToDecimal(supplierUplifts) / 100);
                if (gsUplifts > 0) spGsUplifts = Convert.ToDecimal(Convert.ToDecimal(gsUplifts) / 100);

                var manfSfaUplifts = _dbCon.tblManufacturerSFAUplift.FirstOrDefault(x => x.ManufacturerUpliftID == sfaUpliftId);
                if (manfSfaUplifts != null)
                {
                    manfSfaUplifts.ManufacturerUplift = spSupplierUplifts;
                    manfSfaUplifts.GlobalserveUplift = spGsUplifts;
                    manfSfaUplifts.LastUpdated = DateTime.Now;
                    manfSfaUplifts.LastUpdatedByUserID = userId;
                }
                _dbCon.Entry(manfSfaUplifts).State = System.Data.Entity.EntityState.Modified;
                _dbCon.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSpecialUplifts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSpecialUplifts");
                }
                return false;
            }
        }


        public bool DeleteManftUpliftById(int sfaUpliftId)
        {
            try
            {
                var manfSfaUplifts = _dbCon.tblManufacturerSFAUplift.FirstOrDefault(x => x.ManufacturerUpliftID == sfaUpliftId);
                if (manfSfaUplifts != null)
                {
                    _dbCon.tblManufacturerSFAUplift.Remove(manfSfaUplifts);
                    _dbCon.SaveChanges();
                }

                return true;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "DeleteManftUpliftById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteManftUpliftById");
                }
                return false;
            }
        }
        //Customer additional setup end

        //Company Search Role List
        public List<GenericList> GetRolesList(int parentCompanyId)
        {
            try
            {
                var roles = _dbCon.tblRoles.Where(x => x.IsActive && x.CompanyID == parentCompanyId).Select(x => new GenericList()
                {
                    Key = x.RoleID,
                    Value = x.RoleName

                }).OrderBy(x => x.Value).ToList();
                roles.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                return roles;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRolesList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRolesList");
                }
                return new List<GenericList>();
            }
        }

        public bool UpdateCustomerDefaultHomepage(int customerId, string homePageVal, bool setForAllUsers, int userId)
        {
            try
            {
                var getHomePagePref = _dbCon.tblPreferences.FirstOrDefault(x => x.PreferenceName.ToLower().Contains("home"));
                if (getHomePagePref != null && getHomePagePref.PreferenceID > 0)
                {
                    var homePageId = getHomePagePref.PreferenceID;
                    var getCustomer =
                        _dbCon.tblUserPreferences.FirstOrDefault(
                            x => x.PreferenceID == homePageId && x.CustomerId != null && x.CustomerId == customerId);

                    if (getCustomer != null)
                    {
                        if (setForAllUsers)
                        {
                            RemoveExistingCustomerUserHomePageSetting(customerId, homePageId);
                            var getCustomerFirstUser =
                                _dbCon.tblCustomerUsers.FirstOrDefault(x => x.CustomerID == customerId);
                            if (getCustomerFirstUser != null)
                            {
                                var tblPref = new tblUserPreference()
                                {
                                    PreferenceID = homePageId,
                                    CustomerId = customerId,
                                    IsActive = true,
                                    ForAllUsers = true,
                                    PreferenceValueRevamp = homePageVal,
                                    LastUpdated = DateTime.Now,
                                    UserID = getCustomerFirstUser.UserID,
                                    LastUpdatedByUserID = userId,
                                    PreferenceValue = homePageVal,

                                };

                                _dbCon.tblUserPreferences.Add(tblPref);
                                _dbCon.SaveChanges();

                            }
                        }
                        else
                        {
                            _dbCon.tblUserPreferences.Remove(getCustomer);
                            _dbCon.SaveChanges();
                        }
                    }
                    else
                    {
                        RemoveExistingCustomerUserHomePageSetting(customerId, homePageId);
                        var getCustomerFirstUser =
                            _dbCon.tblCustomerUsers.FirstOrDefault(x => x.CustomerID == customerId);
                        if (getCustomerFirstUser != null)
                        {
                            var tblPref = new tblUserPreference()
                            {
                                PreferenceID = homePageId,
                                CustomerId = customerId,
                                IsActive = true,
                                ForAllUsers = true,
                                PreferenceValueRevamp = homePageVal,
                                LastUpdated = DateTime.Now,
                                UserID = getCustomerFirstUser.UserID,
                                LastUpdatedByUserID = userId,
                                PreferenceValue = homePageVal,

                            };

                            _dbCon.tblUserPreferences.Add(tblPref);
                            _dbCon.SaveChanges();

                        }
                    }
                }
                return true;
            }

            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerDefaultHomepage");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerDefaultHomepage");
                }
                return false;
            }

        }

        public bool RemoveExistingCustomerUserHomePageSetting(int customerId, int homePrefId)
        {
            try
            {
                var getUserIds = _dbCon.tblCustomerUsers.Where(x => x.CustomerID == customerId).Select(x => x.UserID).ToList();

                if (getUserIds != null && getUserIds.Count > 0)
                {

                    var getUsersPref =
                        _dbCon.tblUserPreferences.Where(
                            x => x.PreferenceID == homePrefId && getUserIds.Contains(x.UserID)).ToList();

                    _dbCon.tblUserPreferences.RemoveRange(getUsersPref);
                    _dbCon.SaveChanges();

                }
                return true;
            }

            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "RemoveExistingCustomerUserHomePageSetting");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "RemoveExistingCustomerUserHomePageSetting");
                }
                return false;
            }
        }

        public List<Sp_GetRestrictedCategoriesListForUser_r_Result> GetCategoriesListForUserRestrictionByCustomerId(int customerId, int userId)
        {
            try
            {
                var categories = _dbCon.Sp_GetRestrictedCategoriesListForUser_r(customerId, userId);
                return categories.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCategoriesListForUserRestrictionByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCategoriesListForUserRestrictionByCustomerId");
                }
                return new List<Sp_GetRestrictedCategoriesListForUser_r_Result>();
            }

        }


        public bool AddRestrictedCategoriesForUser(int userId, string categories, int createdBy)
        {
            try
            {
                var getExistingData = _dbCon.tblUserCategoryRestriction_R.Where(x => x.UserId == userId).ToList();
                if (getExistingData.Any())
                {

                    _dbCon.tblUserCategoryRestriction_R.RemoveRange(getExistingData);
                    _dbCon.SaveChanges();

                }

                var newData = new List<tblUserCategoryRestriction_R>();

                if (!string.IsNullOrWhiteSpace(categories))
                    foreach (var category in categories.Split(','))
                    {
                        if (!string.IsNullOrWhiteSpace(category))
                        {

                            var obj = new tblUserCategoryRestriction_R();
                            obj.CategoryId = Convert.ToInt32(category);
                            obj.UserId = userId;
                            obj.CreatedBy = createdBy;
                            obj.CreateDate = DateTime.Now;
                            newData.Add(obj);

                        }
                    }

                if (newData.Any())
                {
                    _dbCon.tblUserCategoryRestriction_R.AddRange(newData);
                    _dbCon.SaveChanges();
                }

                return true;



            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddRestrictedCategoriesForUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddRestrictedCategoriesForUser");
                }
                return false;
            }
        }

        #region CompanySupplier
        /// <summary>
        ///  this method is responsible for providing supplier info
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="custserviceId"></param>
        /// <param name="selectedCustomerId"></param>
        /// <returns></returns>
        public SupplierInfoModel GetSupplierInfo(int companyId, int custserviceId, int selectedCustomerId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {

                    SupplierInfoModel obj = new SupplierInfoModel();
                    obj.SFAId = sfa.SFAID;
                    obj.DateTimeId = sfa.DateFormatID ?? 1;
                    obj.OrderFormatId = sfa.OrderFormatID ?? 0;
                    obj.SelectedCustomerId = selectedCustomerId;
                    obj.Fsadate = sfa.FSADate;

                    obj.OrderFormat = _dbCon.tblOrderFormats.Where(x => x.IsActive == true).OrderBy(x => x.OrderFormatName).Select(x => new GenericList
                    {
                        Key = x.OrderFormatID,
                        Value = x.OrderFormatName
                    }).ToList();
                    obj.OrderFormat.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });

                    var SFAISDContact = _dbCon.tblSFAISDContacts.FirstOrDefault(x => x.SFAID == sfa.SFAID);
                    if (SFAISDContact != null)
                    {
                        obj.CSRInvoiceId = SFAISDContact.InvoiceContactID ?? 0;
                        obj.CSROrderId = SFAISDContact.ContactID ?? 0;
                    }
                    //csr wala kam

                    obj.DefaultCountryId = sfa.CountryID ?? 0;
                    if (sfa.CountryID != null)
                    {
                        var con = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == sfa.CountryID);
                        if (con != null)
                        {
                            obj.CountryName = con.CountryName;
                        }

                    }
                    else
                    {
                        obj.CountryList = _dbCon.tblCountries.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.CountryID, Value = x.CountryName }).OrderBy(x => x.Value).ToList();
                    }

                    obj.DateTimeList = _dbCon.tblDateFormats.Where(x => x.IsActive == true).OrderByDescending(x => x.IsDefaultFormat).OrderBy(x => x.DateFormatName).Select(x => new GenericList
                    {
                        Key = x.DateFormatID,
                        Value = x.DateFormatName
                    }).ToList();
                    obj.IsGlobal = sfa.IsGlobal;
                    obj.Email = sfa.Email;

                    var Offering = _dbCon.tblOfferings;
                    if (Offering != null)
                    {
                        obj.ProcureId = Offering.FirstOrDefault(x => x.OfferingShort == "PROCURE").OfferingID;
                        obj.SupportId = Offering.FirstOrDefault(x => x.OfferingShort == "SUPPORT").OfferingID;
                    }

                    obj.DeliverySLADays = 5;
                    obj.NetPaymentDays = 30;
                    if (custserviceId > 0)
                    {
                        obj.CustServiceId = custserviceId;
                    }
                    else
                    {
                        obj.CustServiceId = obj.ProcureId;
                    }

                    if (selectedCustomerId > 0)
                    {
                        var SFACustomerWA = _dbCon.tblSFACustomerWAs.Where(x => x.SFAID == sfa.SFAID && x.CustomerID == selectedCustomerId && x.IsActive == true && x.OfferingID == obj.CustServiceId).ToList();
                        if (SFACustomerWA.Count > 0)
                        {
                            obj.DeliverySLADays = SFACustomerWA[0].DeliverySLADays;
                            obj.NetPaymentDays = SFACustomerWA[0].NetPaymentDays;
                            obj.WAMonths = SFACustomerWA[0].WAMonths;
                            obj.WANumber = SFACustomerWA[0].WANumber;
                            obj.Wadate = SFACustomerWA[0].WADate;
                            obj.AddressIds = string.Join(",", SFACustomerWA.Select(x => Convert.ToString(x.CustomerDestinationID)).Distinct().ToList());
                        }

                        var addresses = (
                        from co in _dbCon.tblCountries
                        join
                        ca in _dbCon.tblCustomerAddresses on co.CountryID equals ca.CountryID
                        join
                        cd in _dbCon.tblCustomerDestinations on ca.CustomerAddressID equals cd.CustomerDestinationID
                        where cd.IsActive == 1 && ca.CustomerID == selectedCustomerId
                        orderby co.CountryName, ca.CondensedAddress
                        select new { ca.CustomerAddressID, CountryName = co.CountryName, ca.CondensedAddress }).ToList();

                        obj.AddressList = addresses.Select(x => new GenericList
                        {
                            Key = x.CustomerAddressID,
                            Value = Convert.ToString(x.CountryName) + " - " + Convert.ToString(x.CondensedAddress)
                        }).ToList();
                        if (!string.IsNullOrEmpty(obj.AddressIds))
                        {
                            var addIds = obj.AddressIds.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                            var tempAddList1 = obj.AddressList.Where(x => addIds.Contains(x.Key)).ToList();
                            var tempAddList2 = obj.AddressList.Where(x => !addIds.Contains(x.Key)).ToList();
                            obj.AddressList = tempAddList1.Concat(tempAddList2).ToList();
                        }

                    }
                    else
                    {
                        obj.AddressList = new List<Model.GenericList>();
                    }
                    //for csr invoice etc

                    var invoiceData = (from c in _dbCon.tblContacts
                                       join p in _dbCon.tblPartyRoles on c.PartyID equals p.PartyID
                                       join r in _dbCon.tblRoles on p.RoleID equals r.RoleID
                                       where r.RoleName == "CSR"
                                       select new { c.LastName, c.FirstName, c.ContactID }).ToList();

                    obj.CSRDataList = invoiceData.Select(x => new GenericList
                    {
                        Key = x.ContactID,
                        Value = Convert.ToString(x.FirstName) + " " + Convert.ToString(x.LastName)
                    }).OrderBy(x => x.Value).ToList();

                    obj.CSRDataList.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                    //for supplier contact 
                    List<int> sfaParentCompanyIds = new List<int>();
                    foreach (var item in _dbCon.tblSFAs.Where(x => x.SFAID == sfa.SFAID).Select(x => x.ParentCompanyID).ToList())
                    {
                        sfaParentCompanyIds.Add(Convert.ToInt32(item));
                    }
                    var parentCompanyIds = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName.StartsWith("GlobalServe") || sfaParentCompanyIds.Contains(x.ParentCompanyID)).Select(x => x.ParentCompanyID).ToList();

                    if (selectedCustomerId > 0)
                    {
                        var sfaContactIds = _dbCon.tblSFACustomerContacts.Where(x => x.SFAID == sfa.SFAID && x.CustomerID == selectedCustomerId && x.OfferingID == obj.CustServiceId).Select(x => x.SFAContactID).ToList();
                        var contacts = _dbCon.tblContacts.Where(x => x.IsActive == true && x.Email != null).ToList();
                        contacts = contacts.Where(x => parentCompanyIds.Contains(Convert.ToInt32(x.CompanyID)) || sfaContactIds.Contains(x.ContactID)).ToList();
                        var fullContactDetail = (from c in contacts
                                                 join p in _dbCon.tblParentCompanies on c.CompanyID equals p.ParentCompanyID
                                                 select new { c.ContactID, c.LastName, c.FirstName, p.ParentCompanyName }).ToList();
                        obj.ContactList = fullContactDetail.Select(x => new GenericList
                        {
                            Key = x.ContactID,
                            Value = Convert.ToString(x.LastName) + ", " + Convert.ToString(x.FirstName) + " (" + Convert.ToString(x.ParentCompanyName) + ")"
                        }).OrderBy(x => x.Value).ToList();

                        obj.ContactIds = string.Join(",", sfaContactIds.Select(x => Convert.ToString(x)).Distinct().ToList());
                        if (!string.IsNullOrEmpty(obj.ContactIds))
                        {
                            var contIds = obj.ContactIds.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                            var tempConList1 = obj.ContactList.Where(x => contIds.Contains(x.Key)).ToList();
                            var tempConList2 = obj.ContactList.Where(x => !contIds.Contains(x.Key)).ToList();
                            obj.ContactList = tempConList1.Concat(tempConList2).ToList();
                        }
                    }
                    else
                    {
                        var contacts = _dbCon.tblContacts.Where(x => x.IsActive == true && x.Email != null).ToList();
                        contacts = contacts.Where(x => parentCompanyIds.Contains(Convert.ToInt32(x.CompanyID))).ToList();
                        var fullContactDetail = (from c in contacts
                                                 join p in _dbCon.tblParentCompanies on c.CompanyID equals p.ParentCompanyID
                                                 select new { c.ContactID, c.LastName, c.FirstName, p.ParentCompanyName }).ToList();
                        obj.ContactList = fullContactDetail.Select(x => new GenericList
                        {
                            Key = x.ContactID,
                            Value = Convert.ToString(x.LastName) + ", " + Convert.ToString(x.FirstName) + " (" + Convert.ToString(x.ParentCompanyName) + ")"
                        }).OrderBy(x => x.Value).ToList();

                    }

                    //
                    obj.CustomerList = _dbCon.sp_getSupplierCustomerBySFAId_AdminR(sfa.SFAID).ToList();
                    obj.CustomerList.Insert(0, new sp_getSupplierCustomerBySFAId_AdminR_Result { customerid = 0, customername = "(None)" });
                    return obj;
                }
                return null;
            }

            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierInfo");
                }
                return new Model.SupplierInfoModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating supplier information area at supplier info tab
        /// </summary>
        /// <param name="smodel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateSupplierInformation(SupplierInfoModel smodel, int userId)
        {
            try
            {
                if (smodel.SFAId != 0)
                {
                    using (var transcation = _dbCon.Database.BeginTransaction())
                    {
                        var data = _dbCon.tblSFAISDContacts.FirstOrDefault(x => x.SFAID == smodel.SFAId);
                        if (data != null)
                        {
                            _dbCon.tblSFAISDContacts.Remove(data);
                            _dbCon.SaveChanges();
                        }
                        tblSFAISDContact obj = new tblSFAISDContact();
                        obj.SFAID = smodel.SFAId;

                        if (smodel.CSROrderId == 0)
                        {
                            obj.ContactID = null;
                        }
                        else
                        {
                            obj.ContactID = smodel.CSROrderId;
                        }
                        if (smodel.CSRInvoiceId == 0)
                        {
                            obj.InvoiceContactID = null;
                        }
                        else
                        {
                            obj.InvoiceContactID = smodel.CSRInvoiceId;
                        }
                        obj.LastUpdatedByUserID = userId;
                        obj.LastUpdated = DateTime.Now;
                        _dbCon.tblSFAISDContacts.Add(obj);
                        _dbCon.SaveChanges();

                        transcation.Commit();
                    }

                    return _dbCon.sp_UpdateSFAInfo(smodel.SFAId, null //smodel.Wadate 
                        , smodel.Fsadate, userId, smodel.IsGlobal == false ? 0 : 1, smodel.OrderFormatId, smodel.DefaultCountryId, smodel.Email, smodel.DateTimeId);
                }
                return 1;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSupplierInformation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSupplierInformation");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for updating customer information area at supplier info tab
        /// </summary>
        /// <param name="smodel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateCustomerInformation(SupplierInfoModel smodel, int userId)
        {
            try
            {
                if (smodel.SelectedCustomerId > 0)
                {

                    var intAddress = smodel.AddressIds.Split(',').Select(int.Parse).ToList();

                    if (!string.IsNullOrEmpty(smodel.AddressIds) && string.IsNullOrEmpty(smodel.ContactIds))
                    {
                        using (var transcation = _dbCon.Database.BeginTransaction())
                        {
                            var data = _dbCon.tblSFACustomerContacts.
                                Where(x => x.SFAID == smodel.SFAId && x.CustomerID == smodel.SelectedCustomerId
                                && x.OfferingID == smodel.CustServiceId /*&& intAddress.Contains(x.CustomerDestinationID)*/
                                ).ToList();
                            if (data != null)
                            {
                                _dbCon.tblSFACustomerContacts.RemoveRange(data);
                                _dbCon.SaveChanges();
                            }

                            transcation.Commit();
                        }
                    }
                    else if (!string.IsNullOrEmpty(smodel.ContactIds) && !string.IsNullOrEmpty(smodel.AddressIds))
                    {
                        if (smodel.ContactIds.IndexOf(',') == 0)
                        {
                            smodel.ContactIds = smodel.ContactIds.Substring(1, smodel.ContactIds.Length - 1);
                        }
                        var intContacts = smodel.ContactIds.Split(',').Select(int.Parse).ToList();
                        using (var transcation = _dbCon.Database.BeginTransaction())
                        {
                            var data = _dbCon.tblSFACustomerContacts.
                                Where(x => x.SFAID == smodel.SFAId && x.CustomerID == smodel.SelectedCustomerId
                                && x.OfferingID == smodel.CustServiceId
                                //&& intAddress.Contains(x.CustomerDestinationID)
                                //&& !intContacts.Contains(x.SFAContactID)
                                );
                            if (data != null)
                            {
                                _dbCon.tblSFACustomerContacts.RemoveRange(data);
                                _dbCon.SaveChanges();
                            }

                            foreach (var addId in intAddress)
                            {
                                foreach (var conId in intContacts)
                                {
                                    _dbCon.sp_UpdateSFACustomerContacts(conId, smodel.SFAId, smodel.SelectedCustomerId, addId, smodel.CustServiceId, userId);
                                }
                            }
                            transcation.Commit();
                        }
                    }
                    if (!string.IsNullOrEmpty(smodel.AddressIds))
                    {
                        using (var transcation = _dbCon.Database.BeginTransaction())
                        {
                            var SFACustomerWAs = _dbCon.tblSFACustomerWAs.
                            Where(x => x.SFAID == smodel.SFAId && x.CustomerID == smodel.SelectedCustomerId
                            && x.OfferingID == smodel.CustServiceId).ToList();

                            if (SFACustomerWAs != null)
                            {
                                _dbCon.tblSFACustomerWAs.RemoveRange(SFACustomerWAs);
                                _dbCon.SaveChanges();
                            }


                            foreach (var addId in intAddress)
                            {
                                //var tblSFACustomerWA = _dbCon.tblSFACustomerWAs.
                                //FirstOrDefault(x => x.SFAID == smodel.SFAId && x.CustomerID == smodel.SelectedCustomerId
                                //&& x.OfferingID == smodel.CustServiceId && x.CustomerDestinationID==addId );
                                //if (tblSFACustomerWA == null)
                                //{
                                var tblSFACustomerWA = new tblSFACustomerWA();
                                tblSFACustomerWA.SFAID = smodel.SFAId;
                                tblSFACustomerWA.CustomerID = smodel.SelectedCustomerId;
                                tblSFACustomerWA.CustomerDestinationID = addId;
                                tblSFACustomerWA.WADate = smodel.Wadate;
                                tblSFACustomerWA.DeliverySLADays = smodel.DeliverySLADays;
                                tblSFACustomerWA.NetPaymentDays = smodel.NetPaymentDays;
                                tblSFACustomerWA.WAMonths = smodel.WAMonths;
                                tblSFACustomerWA.WANumber = smodel.WANumber;
                                tblSFACustomerWA.LastUpdatedByUserID = userId;
                                tblSFACustomerWA.OfferingID = smodel.CustServiceId;
                                tblSFACustomerWA.LastUpdated = DateTime.Now;
                                tblSFACustomerWA.LastUpdatedByUserID = userId;
                                tblSFACustomerWA.IsActive = true;
                                _dbCon.tblSFACustomerWAs.Add(tblSFACustomerWA);
                                _dbCon.SaveChanges();
                                //}
                                //else
                                //{
                                //    tblSFACustomerWA.WADate = smodel.Wadate;
                                //    tblSFACustomerWA.DeliverySLADays = smodel.DeliverySLADays;
                                //    tblSFACustomerWA.NetPaymentDays = smodel.NetPaymentDays;
                                //    tblSFACustomerWA.WAMonths = smodel.WAMonths;
                                //    tblSFACustomerWA.WANumber = smodel.WANumber;
                                //    tblSFACustomerWA.LastUpdated = DateTime.Now;
                                //    tblSFACustomerWA.LastUpdatedByUserID = userId;
                                //    _dbCon.Entry(tblSFACustomerWA).State = System.Data.Entity.EntityState.Modified;
                                //    _dbCon.SaveChanges();                                   
                                //}
                            }
                            transcation.Commit();
                        }
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerInformation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerInformation");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting supplier contacts 
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetSupplierContacts()
        {
            try
            {
                var data = (from c in _dbCon.tblContacts
                            join p in _dbCon.tblParentCompanies on c.CompanyID equals p.ParentCompanyID
                            where p.IsActive == true && c.IsActive == true
                            orderby c.LastName
                            select new { c.LastName, c.FirstName, c.ContactID, p.ParentCompanyName }).ToList();
                var data1 = data.Select(x => new GenericList
                {
                    Key = x.ContactID,
                    Value = Convert.ToString(x.LastName).Trim() + " , " + Convert.ToString(x.FirstName) + " (" + Convert.ToString(x.ParentCompanyName).Trim() + ")"
                }).OrderBy(x => x.Value).ToList();
                return data1;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierContacts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierContacts");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting supplier accredications
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="custserviceId"></param>
        /// <returns></returns>
        public SupplierAccreditationModel GetSupplierAccreditation(int companyId, int custserviceId)
        {
            try
            {
                SupplierAccreditationModel obj = new Model.SupplierAccreditationModel();
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var mfgs = (from a in _dbCon.tblManufacturers.Where(x => x.GivesAccreditation == true && x.IsActive == true)
                                join b in _dbCon.tblSFAAccreditations.Where(x => x.SFAID == sfa.SFAID && x.OfferingID == custserviceId && x.IsActive == true) on a.ManufacturerID equals b.ManufacturerID into ps
                                from b in ps.DefaultIfEmpty()
                                orderby a.ManufacturerName
                                select new { SelectedMfg = (int?)b.ManufacturerID, a.ManufacturerID, a.ManufacturerName }).ToList();


                    var mfgs1 = (from a in _dbCon.tblManufacturers.Where(x => x.GivesAccreditation == true && x.IsActive == true)
                                 join b in _dbCon.tblSFAAccreditations.Where(x => x.SFAID == sfa.SFAID && x.OfferingID != custserviceId && x.IsActive == true) on a.ManufacturerID equals b.ManufacturerID into ps
                                 from b in ps.DefaultIfEmpty()
                                 orderby a.ManufacturerName
                                 select new { SelectedMfg = (int?)b.ManufacturerID, a.ManufacturerID, a.ManufacturerName }).ToList();

                    var mfgs1List = mfgs1.Where(x => x.SelectedMfg != null).Select(x => x.ManufacturerID).ToList();


                    obj.Mfgs = mfgs.Where(x => !mfgs1List.Contains(x.ManufacturerID)).Select(x => new AccreditationMfg
                    {
                        ManufacturerID = x.ManufacturerID,
                        ManufacturerName = x.ManufacturerName,
                        SelectedMfg = x.SelectedMfg
                    }).ToList();

                    obj.custserviceId = custserviceId;
                    return obj;
                }
                return new SupplierAccreditationModel();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierAccreditation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierAccreditation");
                }
                return new SupplierAccreditationModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating supplier accredation
        /// </summary>
        /// <param name="custserviceId"></param>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <param name="mfgs"></param>
        /// <returns></returns>
        public int UpdateSupplierAccreditation(int custserviceId, int companyId, int userId, string mfgs)
        {
            try
            {
                int count = 0;
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    using (var transcation = _dbCon.Database.BeginTransaction())
                    {
                        var data = _dbCon.tblSFAAccreditations.
                            Where(x => x.SFAID == sfa.SFAID && x.OfferingID == custserviceId
                            );
                        if (data != null)
                        {
                            _dbCon.tblSFAAccreditations.RemoveRange(data);
                            count = _dbCon.SaveChanges();
                        }
                        if (!string.IsNullOrEmpty(mfgs))
                        {
                            tblSFAAccreditation obj;
                            foreach (var mfg in mfgs.Split(','))
                            {
                                obj = new Entity.tblSFAAccreditation();
                                obj.SFAID = sfa.SFAID;
                                obj.ManufacturerID = Convert.ToInt32(mfg);
                                obj.OfferingID = custserviceId;
                                obj.IsActive = true;
                                obj.LastUpdated = DateTime.Now;
                                obj.LastUpdatedByUserID = userId;
                                _dbCon.tblSFAAccreditations.Add(obj);
                                count = _dbCon.SaveChanges();
                            }
                        }
                        transcation.Commit();
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSupplierAccreditation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSupplierAccreditation");
                }
                return 0;
            }
        }

        /// <summary>
        ///  this method is responsible for getting supplier services
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<GenericList> GetSupplierServices(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var services = (from a in _dbCon.tblServices_VSRM
                                    join b in _dbCon.tblGSPServices_VSRM.Where(x => x.GSPID == sfa.SFAID) on a.ServiceID equals b.ServiceID into ps
                                    from b in ps.DefaultIfEmpty()
                                    orderby a.ServiceName
                                    select new { SelectedService = (int?)b.ServiceID, a.ServiceID, a.ServiceName }).ToList();
                    List<GenericList> obj = services.Select(x => new GenericList
                    {
                        Key = x.ServiceID,
                        Value = x.ServiceName,
                        Count = Convert.ToInt32(x.SelectedService)
                    }).ToList();
                    //var count = obj.Where(x => x.Count == 0).ToList().Count;
                    //var count1 = obj.Where(x => x.Count != 0).ToList().Count;
                    return obj;
                }
                return new List<GenericList>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierServices");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierServices");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for updating supplier services
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <param name="services"></param>
        /// <returns></returns>
        public int UpdateSupplierService(int companyId, int userId, string services)
        {
            try
            {
                int count = 0;
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    using (var transcation = _dbCon.Database.BeginTransaction())
                    {
                        var data = _dbCon.tblGSPServices_VSRM.
                            Where(x => x.GSPID == sfa.SFAID);
                        if (data != null)
                        {
                            _dbCon.tblGSPServices_VSRM.RemoveRange(data);
                            count = _dbCon.SaveChanges();
                        }
                        tblGSPServices_VSRM obj;
                        if (!string.IsNullOrEmpty(services))
                        {
                            foreach (var service in services.Split(','))
                            {
                                obj = new Entity.tblGSPServices_VSRM();
                                obj.GSPID = sfa.SFAID;
                                obj.ServiceID = Convert.ToInt32(service);
                                obj.CreatedBy = userId;
                                obj.CreatedOn = DateTime.Now;
                                obj.UpdatedBy = userId;
                                obj.UpdatedOn = DateTime.Now;
                                _dbCon.tblGSPServices_VSRM.Add(obj);
                                count = _dbCon.SaveChanges();
                            }
                        }

                        transcation.Commit();
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSupplierAccreditation");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSupplierAccreditation");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for providing address information
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public CompanySupplierAddressModel GetSupplierAddressInfo(int addressId)
        {
            try
            {
                CompanySupplierAddressModel obj = new CompanySupplierAddressModel();
                CountryPresenter cp = new CountryPresenter();
                if (addressId != 0)
                    obj.AddressInfo = _dbCon.sp_GetAddressInfo_AdminR(addressId).FirstOrDefault();
                else
                    obj.AddressInfo = new sp_GetAddressInfo_AdminR_Result();

                obj.Countries = cp.GetAllCountries();
                obj.AddressTypes = _dbCon.tblAddressTypes.Where(x => x.IsActive).Select(x => new GenericList
                {
                    Value = x.AddressTypeName,
                    Key = x.AddressTypeID
                }).ToList();

                obj.AddressTypes.Insert(0, new GenericList { Value = "(None)", Key = 0 });


                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAddressInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAddressInfo");
                }
                return new CompanySupplierAddressModel();
            }
        }

        /// <summary>
        /// this method is reponsible for getting supplierid( sfaId )
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public int GetSupplierId(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    return sfa.SFAID;
                }
                return 0;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierId");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for providing supplier's user
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<sp_getManufacturerUsers_AdminR_Result> GetSupplierUsers(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var data = (from u in _dbCon.tblUsers
                                join
                                sfau in _dbCon.tblSFAUsers on u.UserID equals sfau.UserID
                                join ug in _dbCon.tblUserGroups on u.UserGroupID equals ug.UserGroupID
                                where sfau.SFAID == sfa.SFAID
                                orderby u.IsActive, u.UserName
                                select new { UserName = u.UserName, UserID = sfau.UserID, UserSecurityLevel = ug.UserSecurityLevel, IsActive = u.IsActive }).ToList();


                    return data.Select(x => new sp_getManufacturerUsers_AdminR_Result { UserName = x.UserName, IsActive = x.IsActive.ToString(), UserID = x.UserID, UserSecurityLevel = x.UserSecurityLevel }).ToList();
                }
                return new List<sp_getManufacturerUsers_AdminR_Result>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierUsers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAddressInfo");
                }
                return new List<sp_getManufacturerUsers_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method is responsible for providing supplier's user detail model
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public CompanySupplierUserModel GetSupplierUserDetailModel(int userId)
        {
            try
            {
                CompanySupplierUserModel obj = new Model.CompanySupplierUserModel();

                obj.DateFormates =
                    _dbCon.tblDateFormats.Where(x => x.IsActive == true)
                        .OrderByDescending(x => x.IsDefaultFormat)
                        .ThenBy(x => x.DateFormatName)
                        .Select(x => new GenericList
                        {
                            Value = x.DateFormatName,
                            Key = x.DateFormatID
                        }).ToList();
                if (userId > 0)
                {
                    var info = (from a in _dbCon.tblUsers
                                join
     b in _dbCon.tblSFAUsers on a.UserID equals b.UserID
                                join c in _dbCon.tblUsers on b.LastUpdatedByUserID equals c.UserID
                                where a.UserID == userId
                                select new CompanySupplierUserInfo
                                {
                                    UserId = a.UserID,
                                    UserName = a.UserName,
                                    UserPassword = a.UserPassword,
                                    SFAID = b.SFAID,
                                    LastUpdated = b.LastUpdated,
                                    LastUpdatedBy = c.UserName,
                                    Email = b.Email,
                                    Phone = b.Phone,
                                    Fax = b.Fax,
                                    IsActive = b.IsActive,
                                    DateFormatID = a.DateFormatID
                                }).FirstOrDefault();

                    obj.UserInfo = info;
                    if (obj.UserInfo != null)
                    {
                        obj.ContactId = 0;
                        var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == obj.UserInfo.UserId);
                        if (contact != null)
                        {
                            obj.ContactId = contact.ContactID;
                        }

                    }
                }
                else
                    obj.UserInfo = new CompanySupplierUserInfo();

                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierUserDetailModel");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierUserDetailModel");
                }
                return new CompanySupplierUserModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating supplier's user detail
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int UpdateSupplierUser(CompanySupplierUserInfo model, int sessionUserId)
        {
            try
            {
                int count = 0;

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblUser user =
                        _dbCon.tblUsers.FirstOrDefault(x => x.UserID == model.UserId);


                    var existingUser = _dbCon.tblUsers.FirstOrDefault(x => x.UserName.ToLower() == model.UserName.Trim().ToLower());
                    if (existingUser != null)
                    {
                        if (existingUser.UserID != user.UserID)
                        {
                            return -1;
                        }
                    }

                    user.UserName = model.UserName;
                    user.UserPassword = model.UserPassword;
                    user.LastUpdated = DateTime.Now;
                    user.LastUpdatedByUserID = sessionUserId;
                    user.DateFormatID = model.DateFormatID;
                    user.IsActive = model.IsActive == 1 ? Convert.ToByte(1) : Convert.ToByte(0);

                    _dbCon.Entry(user).State = System.Data.Entity.EntityState.Modified;

                    tblSFAUser sfaUser = _dbCon.tblSFAUsers.FirstOrDefault(x => x.UserID == model.UserId);

                    sfaUser.Email = string.IsNullOrEmpty(model.Email) ? null : model.Email;
                    sfaUser.IsActive = model.IsActive == 1 ? Convert.ToByte(1) : Convert.ToByte(0);
                    sfaUser.LastUpdated = DateTime.Now;
                    sfaUser.LastUpdatedByUserID = sessionUserId;

                    _dbCon.Entry(sfaUser).State = System.Data.Entity.EntityState.Modified;

                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateSupplierUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateSupplierUser");
                }
                return 0;
            }
        }
        /// <summary>
        /// this method is use for adding supplier user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="mfgId"></param>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public int AddSupplierUser(CompanySupplierUserInfo model, int sessionUserId, int supplierId,
            int parentCompanyId)
        {
            try
            {
                var existingUser = _dbCon.tblUsers.FirstOrDefault(x => x.UserName.ToLower() == model.UserName.Trim().ToLower());
                if (existingUser != null)
                {
                    return -1;
                }

                int count = 0;
                string FirstName = string.Empty;
                string LastName = string.Empty;
                int? CompanyId =
                    _dbCon.tblSFAs.Where(x => x.SFAID == supplierId).FirstOrDefault().ParentCompanyID;
                int index = model.UserName.IndexOf(" ");
                if (index != -1)
                {
                    FirstName = model.UserName.Substring(index + 1);
                    LastName = model.UserName.Substring(0, index);
                }
                else
                {
                    FirstName = "";
                    LastName = model.UserName;
                }


                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    //user groupid =6 is full access
                    var userId =
                        _dbCon.Sp_CreateOneSourceUser_AdminR(model.UserName, model.UserPassword, 6, null,
                            Convert.ToInt32(model.DateFormatID).ToString(), null, null, "SUPPLIER", LastName, FirstName,
                            null, null, null, null, null, null, null, CompanyId, null, model.Email, null, null, null, null,
                            null, null, null, parentCompanyId, sessionUserId).FirstOrDefault();

                    tblSFAUser user = new tblSFAUser();
                    user.UserID = Convert.ToInt32(userId);
                    user.SFAID = supplierId;
                    user.Email = model.Email;
                    user.Phone = null;
                    user.Fax = null;
                    user.LastUpdated = DateTime.Now;
                    user.LastUpdatedByUserID = sessionUserId;
                    user.IsActive = 1;
                    _dbCon.tblSFAUsers.Add(user);

                    count = _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddSupplierUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSupplierUser");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for checking current supplier is live or not
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string IsLiveSupplier(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsActive == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var tblSFApreferredCount = _dbCon.tblSFAPreferreds.Where(x => x.SFAID == sfa.SFAID && x.IsActive == 1).Count();
                    if (sfa.IsActive == true && tblSFApreferredCount > 0)
                    {
                        return "Yes";
                    }
                }
                return "No";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "IsLiveSupplier");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "IsLiveSupplier");
                }
                return "No";
            }
        }

        /// <summary>
        /// this method is responsible for providing supplier additional setup data
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<CompanySupplierAddSetupModel> GetSupplierGenManufacturer(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var offering = _dbCon.tblOfferings.FirstOrDefault(x => x.OfferingShort == "PROCURE");
                    if (offering != null)
                    {
                        var mfgs = (from sfaa in _dbCon.tblSFAAccreditations
                                    join m in _dbCon.tblManufacturers.Where(x => x.GivesAccreditation == true && x.IsActive == true)
                                     on sfaa.ManufacturerID equals m.ManufacturerID into ps
                                    from m in ps.DefaultIfEmpty()
                                    join gdu in _dbCon.tblGenDistributionUplifts on new { sfaa.SFAID, sfaa.ManufacturerID } equals new { gdu.SFAID, gdu.ManufacturerID } into pt
                                    from gdu in pt.DefaultIfEmpty()
                                    where sfaa.SFAID == sfa.SFAID && sfaa.IsActive == true && sfaa.OfferingID == offering.OfferingID
                                    orderby m.ManufacturerName
                                    select new { m.ManufacturerID, m.ManufacturerName, DistributionUplift = (decimal?)gdu.DistributionUplift }).ToList();
                        return mfgs.Select(x => new CompanySupplierAddSetupModel
                        {
                            Id = x.ManufacturerID,
                            Name = x.ManufacturerName,
                            DistributionUplift = x.DistributionUplift != null ? x.DistributionUplift * 100 : x.DistributionUplift
                        }).ToList();
                    }
                }
                return new List<CompanySupplierAddSetupModel>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierManufacturer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierManufacturer");
                }
                return new List<CompanySupplierAddSetupModel>();
            }
        }

        /// <summary>
        ///  this method is responsible for providing supplier additional setup tab 'Distribution uplifts' data
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<CompanySupplierAddSetupModel> GetSupplierManufacturer(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var offering = _dbCon.tblOfferings.FirstOrDefault(x => x.OfferingShort == "PROCURE");
                    if (offering != null)
                    {
                        var mfgs = (from sfaa in _dbCon.tblSFAAccreditations
                                    join m in _dbCon.tblManufacturers.Where(x => x.GivesAccreditation == true && x.IsActive == true)
                                     on sfaa.ManufacturerID equals m.ManufacturerID
                                    where sfaa.SFAID == sfa.SFAID && sfaa.IsActive == true && sfaa.OfferingID == offering.OfferingID
                                    orderby m.ManufacturerName
                                    select new { m.ManufacturerID, m.ManufacturerName }).ToList();
                        return mfgs.Select(x => new CompanySupplierAddSetupModel
                        {
                            Id = x.ManufacturerID,
                            Name = x.ManufacturerName
                        }).ToList();
                    }
                }
                return new List<CompanySupplierAddSetupModel>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierManufacturer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierManufacturer");
                }
                return new List<CompanySupplierAddSetupModel>();
            }
        }

        /// <summary>
        /// this method is responsible for getting supplier manufacturer's tier status data
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<CompanySupplierAddSetupModel> GetSupplierManufacturerTier(int companyId)
        {
            try
            {
                var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                if (sfa != null)
                {
                    var offering = _dbCon.tblOfferings.FirstOrDefault(x => x.OfferingShort == "PROCURE");
                    if (offering != null)
                    {
                        var mfgs = (from sfaa in _dbCon.tblSFAAccreditations
                                    join m in _dbCon.tblManufacturers.Where(x => x.GivesAccreditation == true && x.IsActive == true)
                                     on sfaa.ManufacturerID equals m.ManufacturerID into ps
                                    from m in ps.DefaultIfEmpty()
                                    join sms in _dbCon.tblSFAManufacturerStatus on new { sfaa.SFAID, sfaa.ManufacturerID } equals new { sms.SFAID, sms.ManufacturerID } into pt
                                    from sms in pt.DefaultIfEmpty()
                                    where sfaa.SFAID == sfa.SFAID && sfaa.IsActive == true && sfaa.OfferingID == offering.OfferingID
                                    orderby m.ManufacturerName
                                    select new { m.ManufacturerID, m.ManufacturerName, TierId = (int?)sms.TierID, sms.Comments }).ToList();
                        return mfgs.Select(x => new CompanySupplierAddSetupModel
                        {
                            Id = x.ManufacturerID,
                            Name = x.ManufacturerName,
                            Comments = x.Comments,
                            TierId = x.TierId
                        }).ToList();
                    }
                }
                return new List<CompanySupplierAddSetupModel>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierManufacturer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierManufacturer");
                }
                return new List<CompanySupplierAddSetupModel>();
            }
        }

        /// <summary>
        /// this action is responsible for providing countries and thier distribution uplifts by manufacturerid
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<CompanySupplierAddSetupModel> GetSupplierMfgCountry(int mfgId, int sfaId)
        {
            try
            {
                if (mfgId > 0)
                {
                    var countries = (from a in _dbCon.tblAddresses
                                     join
      sfa in _dbCon.tblSFAs on sfaId equals sfa.SFAID
                                     join
pc in _dbCon.tblParentCompanies on sfa.ParentCompanyID equals pc.ParentCompanyID
                                     join
pa in _dbCon.tblPartyAddresses.Where(x => x.IsActive == true) on pc.PartyID equals pa.PartyID
                                     join
c in _dbCon.tblCountries on a.CountryID equals c.CountryID
                                     join
du in _dbCon.tblDistributionUplifts.Where(x => x.ManufacturerID == mfgId) on new { sfa.SFAID, a.CountryID } equals new { du.SFAID, du.CountryID } into ps
                                     from du in ps.DefaultIfEmpty()
                                     where a.IsActive == true && a.AddressID == pa.AddressID
                                     orderby c.CountryName
                                     select new { c.CountryID, c.CountryName, DistributionUplift = (decimal?)du.DistributionUplift }).Distinct().ToList();



                    var result = countries.Select(x => new CompanySupplierAddSetupModel
                    {
                        Id = x.CountryID,
                        Name = x.CountryName,
                        DistributionUplift = x.DistributionUplift != null ? x.DistributionUplift * 100 : x.DistributionUplift
                    }).OrderBy(x => x.Name).ToList();

                    result.Insert(0, new CompanySupplierAddSetupModel { Name = "(None)", Id = 0 });

                    return result;

                }
                return new List<CompanySupplierAddSetupModel>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierManufacturer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierManufacturer");
                }
                return new List<CompanySupplierAddSetupModel>();
            }
        }

        /// <summary>
        ///  this method is responsible for updating distribution uplifts
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="sfaId"></param>
        /// <param name="userId"></param>
        /// <param name="distributionUplift"></param>
        /// <returns></returns>
        public int UpdateDistributionUplift(int mfgId, int sfaId, int userId, decimal? distributionUplift)
        {
            int count = 0;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblGenDistributionUplift obj =
                        _dbCon.tblGenDistributionUplifts.FirstOrDefault(x => x.SFAID == sfaId && x.ManufacturerID == mfgId);
                    if (obj != null)
                    {
                        if (distributionUplift == null)
                        {
                            _dbCon.tblGenDistributionUplifts.Remove(obj);
                            count = _dbCon.SaveChanges();
                        }
                        else
                        {
                            obj.DistributionUplift = Convert.ToDecimal(distributionUplift) / 100;
                            obj.LastUpdatedByUserID = userId;
                            obj.LastUpdated = DateTime.Now;
                            _dbCon.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }
                    }
                    else if (obj == null && distributionUplift != null)
                    {
                        obj = new tblGenDistributionUplift();
                        obj.SFAID = sfaId;
                        obj.ManufacturerID = mfgId;
                        obj.LastUpdatedByUserID = userId;
                        obj.LastUpdated = DateTime.Now;
                        obj.IsActive = true;
                        obj.DistributionUplift = Convert.ToDecimal(distributionUplift) / 100;

                        _dbCon.tblGenDistributionUplifts.Add(obj);
                        count = _dbCon.SaveChanges();
                    }
                    else
                    {
                        return 1;
                    }

                    transaction.Commit();

                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateDistributionUplift");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateDistributionUplift");
                }
                return count;
            }

            return count;

        }

        /// <summary>
        /// this method is responsible for updating country distribution uplifts
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="countryId"></param>
        /// <param name="sfaId"></param>
        /// <param name="userId"></param>
        /// <param name="distributionUplift"></param>
        /// <returns></returns>
        public int UpdateCountryDistributionUplift(int mfgId, int countryId, int sfaId, int userId, decimal? distributionUplift)
        {
            int count = 0;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblDistributionUplift obj =
                        _dbCon.tblDistributionUplifts.FirstOrDefault(x => x.SFAID == sfaId && x.ManufacturerID == mfgId && x.CountryID == countryId);
                    if (obj != null)
                    {
                        if (distributionUplift == null)
                        {
                            _dbCon.tblDistributionUplifts.Remove(obj);
                            count = _dbCon.SaveChanges();
                        }
                        else
                        {
                            obj.DistributionUplift = Convert.ToDecimal(distributionUplift) / 100;
                            obj.LastUpdatedByUserID = userId;
                            obj.LastUpdated = DateTime.Now;
                            _dbCon.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }
                    }
                    else if (obj == null && distributionUplift != null)
                    {
                        obj = new tblDistributionUplift();
                        obj.SFAID = sfaId;
                        obj.ManufacturerID = mfgId;
                        obj.CountryID = countryId;
                        obj.LastUpdatedByUserID = userId;
                        obj.LastUpdated = DateTime.Now;
                        obj.IsActive = true;
                        obj.DistributionUplift = Convert.ToDecimal(distributionUplift) / 100;

                        _dbCon.tblDistributionUplifts.Add(obj);
                        count = _dbCon.SaveChanges();
                    }
                    else
                    {
                        return 1;
                    }

                    transaction.Commit();

                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCountryDistributionUplift");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCountryDistributionUplift");
                }
                return count;
            }

            return count;

        }

        /// <summary>
        /// this method is responsible for providing tiers
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetTiers()
        {
            try
            {
                var data = _dbCon.tblTiers.Where(x => x.IsActive == true).OrderBy(x => x.TierName).Select(x => new GenericList
                {
                    Value = x.TierName,
                    Key = x.TierID
                }).ToList();

                data.Insert(0, new Model.GenericList
                {
                    Key = 0,
                    Value = "(None)"
                });
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTiers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTiers");
                }
                return new List<GenericList>();
            }

        }

        /// <summary>
        /// this method is responsible for updating manufacturer tier
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="tierId"></param>
        /// <param name="sfaId"></param>
        /// <param name="userId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public int UpdateManufacturerTier(int mfgId, int? tierId, int sfaId, int userId, string comment)
        {
            int count = 0;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblSFAManufacturerStatu obj =
                        _dbCon.tblSFAManufacturerStatus.FirstOrDefault(x => x.SFAID == sfaId && x.ManufacturerID == mfgId);
                    if (obj != null)
                    {
                        _dbCon.tblSFAManufacturerStatus.Remove(obj);
                        count = _dbCon.SaveChanges();
                    }
                    if (tierId != null && tierId > 0)
                    {
                        obj = new tblSFAManufacturerStatu();
                        obj.SFAID = sfaId;
                        obj.ManufacturerID = mfgId;
                        obj.TierID = Convert.ToInt32(tierId);
                        obj.LastUpdatedByUserID = userId;
                        obj.LastUpdated = DateTime.Now;
                        obj.IsActive = true;
                        obj.Comments = comment.Trim();

                        _dbCon.tblSFAManufacturerStatus.Add(obj);
                        count = _dbCon.SaveChanges();
                    }

                    transaction.Commit();

                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateManufacturerTier");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateManufacturerTier");
                }
                return -1;
            }

            return count;

        }

        public UserCredentialModel SendSupplierUserPassword(string username, int companyId, int userId)
        {
            try
            {
                int count = 0;
                var company = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == companyId);
                if (company != null)
                {
                    var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.IsComposite == true && x.ParentCompanyID == companyId);
                    if (sfa != null)
                    {
                        var users = (from a in _dbCon.tblUsers
                                     join b in _dbCon.tblSFAUsers on a.UserID equals b.UserID
                                     where b.SFAID == sfa.SFAID
                                     select new { a.UserName, a.UserID, a.UserPassword }).ToList();
                        if (users.Count > 0)
                        {
                            var currentUser = users.FirstOrDefault(x => x.UserID == userId);
                            if (currentUser != null)
                            {
                                return new Model.UserCredentialModel() { CompanyName = company.ParentCompanyName, Password = currentUser.UserPassword, UserName = currentUser.UserName };
                            }

                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "SendSupplierUserPassword");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "SendSupplierUserPassword");
                }
                return null;
            }
        }
        #endregion

        public List<Sp_GetCustomerCountryDisclaimers_R_Result> GetCountryDisclaimerListByCustomerId(int customerId)
        {
            try
            {
                var getCustomerDisclaimer = _dbCon.Sp_GetCustomerCountryDisclaimers_R(customerId, 0).OrderBy(x => x.CountryName).ToList();
                return getCustomerDisclaimer;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCountryDisclaimerListByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountryDisclaimerListByCustomerId");
                }
                return new List<Sp_GetCustomerCountryDisclaimers_R_Result>();
            }
        }

        public bool AddCustomerCountryDisclaimer(int customerId, string disclaimer, string countryIds, int updatedBy)
        {
            try
            {

                var newData = new List<tblCustomerCountryDisclaimer_R>();

                if (!string.IsNullOrWhiteSpace(countryIds))
                    foreach (var countryId in countryIds.Split(','))
                    {
                        if (!string.IsNullOrWhiteSpace(countryId))
                        {
                            var countryIdConverted = Convert.ToInt32(countryId);
                            var getExistingData = _dbCon.tblCustomerCountryDisclaimer_R.Where(x => x.CountryId == countryIdConverted).ToList();
                            if (getExistingData.Any())
                            {
                                _dbCon.tblCustomerCountryDisclaimer_R.RemoveRange(getExistingData);
                                _dbCon.SaveChanges();
                            }

                            if (!string.IsNullOrWhiteSpace(disclaimer))
                            {
                                var obj = new tblCustomerCountryDisclaimer_R();
                                obj.CountryId = Convert.ToInt32(countryId);
                                obj.CustomerId = customerId;
                                obj.Disclaimer = disclaimer;
                                obj.UpdatedBy = updatedBy;
                                obj.UpdatedDae = DateTime.Now;

                                newData.Add(obj);
                            }

                        }
                    }

                if (newData.Any())
                {
                    _dbCon.tblCustomerCountryDisclaimer_R.AddRange(newData);
                    _dbCon.SaveChanges();
                }

                return true;



            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddCustomerCountryDisclaimer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddCustomerCountryDisclaimer");
                }
                return false;
            }
        }
        public bool RemoveCustomerCountryDisclaimer(int customerId, int countryId)
        {
            try
            {
                var chkItem = _dbCon.tblCustomerCountryDisclaimer_R.Where(x => x.CountryId == countryId && x.CustomerId == customerId).ToList();
                if (chkItem.Any())
                {
                    _dbCon.tblCustomerCountryDisclaimer_R.RemoveRange(chkItem);
                    _dbCon.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "RemoveCustomerCountryDisclaimer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "RemoveCustomerCountryDisclaimer");
                }
                return false;
            }
        }

        public tblCustomer GetCustomerById(int customerId)
        {
            return _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerId);
        }

        public Tbl_ResetPassword_R AddPasswordResetLink(int customerId, int userId, AdminSetting adminSettings, string url)
        {
            try
            {
                var model = new Tbl_ResetPassword_R();

                model.UserId = userId;
                model.CustomerId = customerId;
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = adminSettings.UserID;
                model.IsExpired = false;
                model.ResetToken = Guid.NewGuid().ToString();
                model.Url = url;
                _dbCon.Tbl_ResetPassword_R.Add(model);
                _dbCon.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddPasswordResetLink");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddPasswordResetLink");
                }
                return new Tbl_ResetPassword_R();
            }
        }


    }
}
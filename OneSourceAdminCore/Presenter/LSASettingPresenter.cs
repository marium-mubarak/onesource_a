﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of SLA data"
    /// </summary>
    public class LSASettingPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public LSASettingPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// get SLA Setting detail
        /// </summary>
        /// <returns></returns>
        public SLASettingModel GetLSASettingModel()
        {
            CustomerPresenter cp = new CustomerPresenter();

            SLASettingModel model = new SLASettingModel
            {
                Customers = cp.GetCustomersWithPriority()
            };
            return model;
        }
        /// <summary>
        /// Add SLA data to the data
        /// </summary>
        /// <param name="name"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddSLA(string name, int userId)
        {
            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblGenericRequestSLAs.Where(x => x.SLAName == name.Trim()).ToList();
                if (data.Count > 0)
                {
                    return 0;
                }
                var tblGenericRequestSLA = new tblGenericRequestSLA();
                tblGenericRequestSLA.LastUpdated = DateTime.Now;
                tblGenericRequestSLA.LastUpdatedByID = userId;
                tblGenericRequestSLA.SLAName = name;
                _dbCon.tblGenericRequestSLAs.Add(tblGenericRequestSLA);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// Get SLA by Customer 
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public SLASettingModel GetSLAinfo(int customerId)
        {
            SLASettingModel obj = new Model.SLASettingModel
            {
                LstUnselectedSLA = _dbCon.sp_GetUnselectedSLA_R(customerId).OrderBy(x => x.SLAName).ToList(),
                LstSelectedSLA = GetSelectedSLA(customerId).OrderBy(x => x.Value).ToList(),
                Viewable = isFieldViewable(customerId)
            };


            return obj;
        }
        /// <summary>
        /// return boolean value wheather SLA dropdown in viewable or not
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public bool isFieldViewable(int customerId)
        {
            bool isviewable = false;
            var customerViewField = _dbCon.tblCustomerViewFields.FirstOrDefault(x => x.CustomerID == customerId);
            if (customerViewField != null)
            {
                isviewable = true;
            }
            return isviewable;
        }
        /// <summary>
        /// Get SLA selected by customer Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>

        public List<GenericList> GetSelectedSLA(int customerId)
        {
            List<int> ids = _dbCon.tblRequestSLAs.Where(x => x.CustomerID == customerId).Select(x => x.SLAId).ToList();
            var result = _dbCon.tblGenericRequestSLAs.Where(x => ids.Contains(x.GenericRequestSLAId)).Select(x => new GenericList
            {
                Key = x.GenericRequestSLAId,
                Value = x.SLAName
            }).ToList();
            return result;
        }
        /// <summary>
        /// Add SLA to the Customer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddSLACustomerWise(SLAModel model, int userId)
        {
            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (model.unselectedSLA != null && model.unselectedSLA.Count > 0)
                {
                    for (int i = 0; i < model.unselectedSLA.Count; i++)
                    {
                        var RequestSLA = new Entity.tblRequestSLA();
                        RequestSLA.CustomerID = model.CustomerId;
                        RequestSLA.LastUpdated = DateTime.Now;
                        RequestSLA.LastUpdatedByID = userId;
                        RequestSLA.SLAId = Convert.ToInt32(model.unselectedSLA[i]);

                        _dbCon.tblRequestSLAs.Add(RequestSLA);
                    }
                    count = _dbCon.SaveChanges();
                }
                transaction.Commit();

            }
            return count;
        }

        /// <summary>
        /// set SLA field to the customer's view fields
        /// </summary>
        /// <param name="isViewable"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public int SetCustomerView(bool isViewable, int customerId)
        {
            int count = 0;

            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var customerViewField = _dbCon.tblCustomerViewFields.FirstOrDefault(x => x.CustomerID == customerId);

                if (customerViewField == null && isViewable)
                {
                    var viewFields = _dbCon.tblViewFields.Where(x => x.VFieldName == "SLA").Select(x => new { x.VFieldID, x.ViewID }).FirstOrDefault();
                    if (viewFields != null)
                    {
                        var CustomerViewField = new Entity.tblCustomerViewField();
                        CustomerViewField.CustomerID = customerId;
                        CustomerViewField.VFieldID = viewFields.VFieldID;
                        CustomerViewField.ViewID = viewFields.ViewID;
                        _dbCon.tblCustomerViewFields.Add(CustomerViewField);
                        count = _dbCon.SaveChanges();
                    }
                }


                if (!isViewable && customerViewField != null)
                {
                    _dbCon.tblCustomerViewFields.Remove(customerViewField);
                    count = _dbCon.SaveChanges();
                }


                transcation.Commit();
            }


            return count;
        }
        public int DeleteCustomerSLA(string customerId, string SLA)
        {
            int count = 0;
            var lst = SLA.Split(',');
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                int slaId;
                int intCustomerId = Convert.ToInt32(customerId);

                foreach (var item in lst)
                {
                    slaId = Convert.ToInt32(item);
                    var record = _dbCon.tblRequestSLAs.FirstOrDefault(x => x.SLAId == slaId && x.CustomerID == intCustomerId);
                    if (record != null)
                    {
                        _dbCon.tblRequestSLAs.Remove(record);
                        count = _dbCon.SaveChanges();
                    }
                }

                transcation.Commit();
            }
            return count;
        }

        #region OrderQuoteSLA
        public List<sp_getSLAs_AdminR_Result> GetOrderQuoteSLAs()
        {
            return _dbCon.sp_getSLAs_AdminR().ToList();
        }
        public int UpdateOrderQuoteSLA(List<sp_getSLAs_AdminR_Result> OrderQuoteSLAModel, int userId)
        {
            int count = 0;
            if (OrderQuoteSLAModel.Count > 0)
            {
                var countryDeliverySLAs = _dbCon.tblCountryDeliverySLAs.ToList();
                _dbCon.tblCountryDeliverySLAs.RemoveRange(countryDeliverySLAs);

                tblCountryDeliverySLA obj;
                using (var transcation = _dbCon.Database.BeginTransaction())
                {
                    foreach (var item in OrderQuoteSLAModel)
                    {
                        if (item.AcceptanceDays != null && item.QuoteResponseDays != null && item.Days != null)
                        {
                            obj = new Entity.tblCountryDeliverySLA();
                            obj.CountryID = item.CountryID;
                            obj.Days = Convert.ToDecimal(item.Days);
                            obj.AcceptanceDays = Convert.ToDecimal(item.AcceptanceDays);
                            obj.QuoteResponseDays = Convert.ToDecimal(item.QuoteResponseDays);
                            obj.IsActive = true;
                            obj.LastUpdated = DateTime.Now;
                            obj.LastUpdatedByUserID = userId;

                            _dbCon.tblCountryDeliverySLAs.Add(obj);
                            count = _dbCon.SaveChanges();
                        }
                    }
                    transcation.Commit();
                }
            }
            return count;
        }
        #endregion

        #region OEM Specific SLA

        public OEMSpecificSLAsModel GetOEMSpecificSLAModel()
        {

            OEMSpecificSLAsModel obj = new OEMSpecificSLAsModel();
            CountryPresenter cp = new CountryPresenter();
            obj.Countries = cp.GetCountries();
            obj.Countries.Insert(0, new GenericList { Key = 0, Value = "(None)" });
            obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Value = x.ManufacturerName,
                Key = x.ManufacturerID
            }).ToList();

            obj.Manufacturers.Insert(0, new GenericList { Key = 0, Value = "(None)" });

            return obj;
        }
        public object GetSLAByOEM(int countryId, int manufacturerId)
        {
            tblCountryDeliverySLAsByOEM data = _dbCon.tblCountryDeliverySLAsByOEMs.FirstOrDefault(x => x.CountryID == countryId && x.ManufacturerID == manufacturerId);
            if (data != null)
            {
                var info = new { OrderDeliverySLA = data.OrderDeliverySLA, QuoteResponseSLA = data.QuoteResponseSLA, data.OrderAcceptanceSLA, data.CatalogUploadSLA };
                return info;
            }
            return null;
        }
        public int UpdateOEMSpecificSLAs(OEMSpecificSLAsModel model, int userId)
        {
            int count = 0;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblCountryDeliverySLAsByOEMs.Where(x => x.CountryID == model.countryId && x.ManufacturerID == model.manufacturerId);
                _dbCon.tblCountryDeliverySLAsByOEMs.RemoveRange(data);

                tblCountryDeliverySLAsByOEM obj = new tblCountryDeliverySLAsByOEM();
                obj.CountryID = model.countryId;
                obj.ManufacturerID = model.manufacturerId;
                obj.LastUpdatedByUserID = userId;
                obj.LastUpdated = DateTime.Now;
                obj.CatalogUploadSLA = model.CatalogUploadSLA;
                obj.OrderAcceptanceSLA = model.OrderAcceptanceSLA;
                obj.OrderDeliverySLA = model.OrderDeliverySLA;
                obj.QuoteResponseSLA = model.QuoteResponseSLA;

                _dbCon.tblCountryDeliverySLAsByOEMs.Add(obj);

                count = _dbCon.SaveChanges();
                transcation.Commit();
            }
            return count;
        }

        #endregion

        #region local taxes

        public int UpdateCountryTax(int countryId, decimal? localtax, int userId)
        {
            int count = 0;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblLocalTaxes.Where(x => x.CountryID == countryId);
                _dbCon.tblLocalTaxes.RemoveRange(data);
                if (localtax != null)
                {
                    tblLocalTax obj = new tblLocalTax();
                    obj.CountryID = countryId;

                    obj.LocalTax = Convert.ToDecimal(localtax) / 100;

                    obj.IsActive = true;
                    obj.LastUpdatedByUserID = userId;
                    obj.LastUpdated = DateTime.Now;

                    _dbCon.tblLocalTaxes.Add(obj);
                }
                count = _dbCon.SaveChanges();
                transcation.Commit();
            }
            return count;
        }

        public decimal? GetCountryTax(int countryId)
        {
            tblLocalTax localtax = _dbCon.tblLocalTaxes.FirstOrDefault(x => x.CountryID == countryId);
            if (localtax != null)
            {

                return localtax.LocalTax * 100;
            }
            return null;
        }
        #endregion

        #region sla history

        /// <summary>
        /// this method is responsible for providing SLA history
        /// </summary>
        /// <param name="slaId"></param>
        /// <returns></returns>
        public SLAHistoryModel GetSLAHistory(int slaId)
        {
            try
            {
                SLAHistoryModel obj = new Model.SLAHistoryModel();
                obj.SLAHistory = _dbCon.sp_getSLAHistory_AdminR(slaId).FirstOrDefault() ?? new Entity.sp_getSLAHistory_AdminR_Result();
                obj.BreachCauses = _dbCon.tblBreachCauses.Where(x => x.isActive == true).Select(x => new GenericList { Key = x.breachCauseID, Value = x.breachCauseName }).ToList();
                obj.BreachCauses.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SLA Setting Presenter => GetSLAHistory<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SLA Setting Presenter => GetSLAHistory");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSLAHistory");
                }
                return new Model.SLAHistoryModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating Timer to start state
        /// </summary>
        /// <param name="slaId"></param>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        /// <param name="serverTimeZone"></param>
        /// <returns></returns>
        public bool StartTimer(int slaId, int userId, int contactId, int serverTimeZone)
        {
            var issubmit = false;
            try
            {
                var data = _dbCon.sp_getSLAHistory_AdminR(slaId).FirstOrDefault();
                if (data != null)
                {
                    var ar = new GSSupport.ActionRequest();
                    ar.Qualify = false;
                    ar.GenerateTicket = false;
                    ar.CategoryName = "Ticket Change";
                    ar.GSTicketID = data.ticketid;
                    ar.AddFieldByDisplay("Open SLA", Convert.ToString(data.tacid));
                    ar.SubmitUserID = userId;
                    ar.AffectedContactID = contactId;
                    ar.RequestDateLocal = DateTime.Now;
                    ar.RequestDate = DateTime.Now;
                    ar.RequestDateTimeZoneVBConst = serverTimeZone;
                    ar.Summary = "SLA Timer Override: Timer Start";

                    var action = _dbCon.tblActions.FirstOrDefault(x => x.ActionName == "Resume SLA Timer");
                    if (action != null)
                    {
                        ar.RequestedActionID = action.ActionID;
                    }
                    else
                    {
                        ar.RequestedActionID = action.ActionID;
                    }
                    issubmit = ar.Submit();
                }
                return issubmit;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SLA Setting Presenter => StartTimer<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SLA Setting Presenter => StartTimer");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "StartTimer");
                }
                return issubmit;
            }
        }

        /// <summary>
        /// this method is responsible for updating timer to stop state
        /// </summary>
        /// <param name="slaId"></param>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        /// <param name="serverTimeZone"></param>
        /// <returns></returns>
        public bool StopTimer(int slaId, int userId, int contactId, int serverTimeZone)
        {
            var issubmit = false;
            try
            {
                var data = _dbCon.sp_getSLAHistory_AdminR(slaId).FirstOrDefault();
                if (data != null)
                {
                    var ar = new GSSupport.ActionRequest();
                    ar.Qualify = false;
                    ar.GenerateTicket = false;
                    ar.CategoryName = "Ticket Change";
                    ar.GSTicketID = data.ticketid;
                    ar.AddFieldByDisplay("Open SLA", Convert.ToString(data.tacid));
                    ar.SubmitUserID = userId;
                    ar.AffectedContactID = contactId;
                    ar.RequestDateLocal = DateTime.Now;
                    ar.RequestDate = DateTime.Now;
                    ar.RequestDateTimeZoneVBConst = serverTimeZone;
                    ar.Summary = "SLA Timer Override: Timer Stop";

                    var action = _dbCon.tblActions.FirstOrDefault(x => x.ActionName == "Suspend SLA Timer");
                    if (action != null)
                    {
                        ar.RequestedActionID = action.ActionID;
                    }
                    else
                    {
                        ar.RequestedActionID = action.ActionID;
                    }
                    issubmit = ar.Submit();
                }
                return issubmit;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SLA Setting Presenter => StopTimer<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SLA Setting Presenter => StopTimer");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "StopTimer");
                }
                return issubmit;
            }
        }

        /// <summary>
        /// this method is responsible for adding comment to the SLA
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="slaId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public int AddSLAComment(string comment, int slaId, string username)
        {
            try
            {
                int count = 0;
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    var slaTracking = _dbCon.tblSLATrackings.FirstOrDefault(x => x.slaInstanceID == slaId);
                    if (slaTracking != null)
                    {
                        slaTracking.comments = slaTracking.comments + "<br>(" + DateTime.Now.ToString("MMM dd yyyy HH:MM tt") + " - " + username + ") " + comment.Trim();
                        _dbCon.Entry(slaTracking).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        transaction.Commit();

                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SLA Setting Presenter => AddSLAComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SLA Setting Presenter => AddSLAComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSLAComment");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for modifying/updating SLA
        /// </summary>
        /// <param name="model"></param>
        /// <param name="resolution"></param>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int ModifySLA(SLAHistoryUpdateModel model, string resolution, string username, int userId)
        {
            try
            {

                int count = 0;
                var data = _dbCon.sp_getSLAHistory_AdminR(model.slainstanceid).FirstOrDefault();
                if (data != null)
                {
                    if (data.isbreach)
                    {
                        if (model.breachCauseID != null && model.breachCauseID != 0)
                        {
                            var slaTrackingDetail = _dbCon.tblSLATrackings.FirstOrDefault(x => x.slaInstanceID == model.slainstanceid);
                            using (var transaction = _dbCon.Database.BeginTransaction())
                            {
                                if (slaTrackingDetail != null)
                                {
                                    slaTrackingDetail.breachCauseID = model.breachCauseID;
                                    slaTrackingDetail.breachResolutionText = model.breachResolutionText;
                                    slaTrackingDetail.lastUpdate = DateTime.Now;
                                    slaTrackingDetail.lastUpdateUserID = userId;
                                    slaTrackingDetail.minutesToBreach = model.minutestobreach;
                                    _dbCon.Entry(slaTrackingDetail).State = System.Data.Entity.EntityState.Modified;
                                    count = _dbCon.SaveChanges();
                                    transaction.Commit();
                                }
                            }
                        }
                    }
                    if (model.oldisbreach == false && model.oldisexception == false && model.oldisclosedwithinsla == false)
                    {
                        var ar = new GSSupport.ActionRequest();
                        ar.Qualify = false;
                        ar.GenerateTicket = false;
                        ar.CategoryName = "Ticket Change";
                        ar.GSTicketID = data.ticketid;
                        ar.AddFieldByDisplay("Open SLA", Convert.ToInt32(data.tacid).ToString());

                        if (resolution == "closedwithinsla")
                        {
                            ar.AddFieldByDisplay("Is Closed Within SLA", "true");
                        }
                        else
                        {
                            ar.AddFieldByDisplay("Is Closed Within SLA", "false");
                        }
                        if (resolution == "breach")
                        {
                            ar.AddFieldByDisplay("Is Breach", "true");
                        }
                        else
                        {
                            ar.AddFieldByDisplay("Is Breach", "false");
                        }
                        if (resolution == "exception")
                        {
                            ar.AddFieldByDisplay("Is Exception", "true");
                        }
                        else
                        {
                            ar.AddFieldByDisplay("Is Exception", "false");
                        }
                        if (model.minutestobreach > 0)
                        {
                            ar.AddFieldByDisplay("Minutes Until Violation", model.minutestobreach.ToString());
                        }


                        ar.Summary = "SLA Timer Override";
                        var action = _dbCon.tblActions.FirstOrDefault(x => x.ActionName == "SLA Override");
                        if (action != null)
                        {
                            ar.RequestedActionID = action.ActionID;
                        }
                        else
                        {
                            ar.RequestedActionID = action.ActionID;
                        }
                        var issubmit = ar.Submit();
                        return issubmit ? 1 : 0;

                    }
                    else
                    {
                        //if request("resolution") = "breach" and CBool(request("oldisbreach")) = False
                        tblSLATracking slaTracking = _dbCon.tblSLATrackings.FirstOrDefault(x => x.slaInstanceID == model.slainstanceid);
                        if (slaTracking != null)
                        {
                            string oldres = string.Empty;

                            if (model.oldisbreach)
                            {
                                oldres = "Breach";
                            }
                            else if (model.oldisexception)
                            {
                                oldres = "Exception";
                            }
                            else if (model.oldisclosedwithinsla)
                            {
                                oldres = "Closed Within SLA";
                            }

                            if (resolution == "breach" && model.oldisbreach == false)
                            {
                                slaTracking.isBreach = true;
                                slaTracking.closedWithinSLA = false;
                                slaTracking.isException = false;
                                slaTracking.comments = slaTracking.comments + "<br>(" + DateTime.Now.ToString("MMM dd yyyy HH:MM tt") + " - " + username + ") " +
                                    "SLA Override: resolution manually changed from " + oldres + " to Breach by " + username;
                            }
                            else if (resolution == "exception" && model.oldisexception == false)
                            {
                                slaTracking.isBreach = false;
                                slaTracking.closedWithinSLA = false;
                                slaTracking.isException = true;
                                slaTracking.comments = slaTracking.comments + "<br>(" + DateTime.Now.ToString("MMM dd yyyy HH:MM tt") + " - " + username + ") " +
                                    "SLA Override: resolution manually changed from " + oldres + " to Exception by " + username;
                            }
                            else if (resolution == "closedwithinsla" && model.oldisclosedwithinsla == false)
                            {
                                slaTracking.isBreach = false;
                                slaTracking.closedWithinSLA = true;
                                slaTracking.isException = false;
                                slaTracking.comments = slaTracking.comments + "<br>(" + DateTime.Now.ToString("MMM dd yyyy HH:MM tt") + " - " + username + ") " +
                                    "SLA Override: resolution manually changed from " + oldres + " to Closed Within SLA by " + username;
                            }
                            slaTracking.minutesToBreach = model.minutestobreach;
                            _dbCon.Entry(slaTracking).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                            //transaction.Commit();
                        }
                        return count;
                    }

                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SLA Setting Presenter => AddSLAComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SLA Setting Presenter => AddSLAComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSLAComment");
                }
                return 0;
            }


        }

        #endregion
    }
}
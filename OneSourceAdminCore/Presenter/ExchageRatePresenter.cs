﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class ExchageRatePresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public ExchageRatePresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// give list of countries
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetCountries()
        {
            var countries = _dbCon.tblCountries.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Key = x.CountryID,
                Value = x.CountryName

            }).OrderBy(x => x.Value).ToList();
            return countries;
        }
        /// <summary>
        /// give list of country alert
        /// </summary>
        /// <returns></returns>
        public List<CustomExchangeRateAlert> GetExchageRateAlert()
        {
            var exchangeRateAlert = _dbCon.tblExchangeRateAlerts.Select(x => new CustomExchangeRateAlert
            {
                ExchangeRateAlertId = x.ExchangeRateAlertId,
                CountryId = x.CountryId,
                CountryName = x.CountryName,
                Alert = x.Alert,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                FileName = x.FileName,
                FilePath = x.FilePath,
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName
            }).OrderBy(x=>x.CountryName).ToList();

            exchangeRateAlert.ForEach(x =>
            {
                x.strStartDate = x.StartDate.Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.EndDate != null ? Convert.ToDateTime(x.EndDate).Date.ToString("MM/dd/yyy") : string.Empty);
            x.FileName = (x.FileName != null ? x.FileName.Substring(x.FileName.IndexOf('^')+1,x.FileName.Length- x.FileName.IndexOf('^')-1) :string.Empty);
            });
            return exchangeRateAlert;
        }
        /// <summary>
        /// add exchangerate alert to the database
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddExchageRateAlert(tblExchangeRateAlert alert, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                alert.CreatedDate = DateTime.Now;
                alert.CreatedBy = userId;
                alert.CountryName = string.IsNullOrEmpty(alert.CountryName) == true ? string.Empty : alert.CountryName;
                alert.CustomerId = alert.CustomerId ?? 0;
                _dbCon.tblExchangeRateAlerts.Add(alert);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// edit exchangerate alert in the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditExchageRateAlert(tblExchangeRateAlert model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblExchangeRateAlert alert =
                    _dbCon.tblExchangeRateAlerts.FirstOrDefault(x => x.ExchangeRateAlertId == model.ExchangeRateAlertId);
                if (alert != null)
                {
                    alert.Alert = model.Alert;
                    alert.CountryId = model.CountryId;
                    alert.CountryName = model.CountryName;
                    alert.StartDate = model.StartDate;
                    alert.EndDate = model.EndDate;
                    alert.UpdatedBy = userId;
                    alert.UpdatedDate = DateTime.Now;
                    alert.CustomerId = model.CustomerId ?? 0;
                    alert.CustomerName = model.CustomerName;
                    if (!string.IsNullOrEmpty(model.FileName))
                    {
                        alert.FileName = model.FileName == (alert.FileName!=null?alert.FileName.Split('^')[1]:string.Empty)?alert.FileName:model.FileName;
                        alert.FilePath = model.FilePath??alert.FilePath;
                    }
                    else
                    {
                        alert.FileName = null;
                        alert.FilePath = null; 
                    }
                   

                }
                _dbCon.Entry(alert).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// delete exchangerate alert from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteExchageRateAlert(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblExchangeRateAlerts.FirstOrDefault(x => x.ExchangeRateAlertId == id);
                if (alert != null)
                {
                    _dbCon.tblExchangeRateAlerts.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public string GetFilePathById(int id)
        {
            var tblExchangeRateAlerts = _dbCon.tblExchangeRateAlerts.FirstOrDefault(x => x.ExchangeRateAlertId == id);
            if (tblExchangeRateAlerts != null)
                return tblExchangeRateAlerts.FilePath;
            return string.Empty;
        }
    }
}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class AssetDispositionPresenter
    {
        private readonly globalserveEntities _dbCon;
        private readonly ADUploadPresenter _adupload;

        public AssetDispositionPresenter()
        {
            _dbCon = new globalserveEntities();
            _adupload = new ADUploadPresenter();
        }

        public List<SelectListItem> GetCustomers()
        {
            try
            {
                var Customers = (from cus in _dbCon.tblCustomers
                                 join pc in _dbCon.tblParentCompanies on cus.ParentCompanyID equals pc.ParentCompanyID
                                 join pr in _dbCon.tblPartyRoles on pc.PartyID equals pr.PartyID
                                 join r in _dbCon.tblRoles on pr.RoleID equals r.RoleID
                                 where pc.IsActive == true && cus.IsActive == true && r.RoleName.Contains("Customer")
                                 orderby cus.CustomerName
                                 select new { cus.CustomerID, cus.CustomerName }).ToList();

                var customersList = Customers.Select(x => new SelectListItem
                {
                    Value = x.CustomerID.ToString(),
                    Text = x.CustomerName.ToString()
                }).ToList();

                return customersList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomers");
                }
                return new List<SelectListItem>();
            }

        }

        public List<SelectListItem> GetCountries(int customerId)
        {
            try
            {
                var countries = (from coun in _dbCon.tblCountries
                                 join ccc in _dbCon.tblCustomerCountryCurrencies on coun.CountryID equals ccc.CountryID
                                 where ccc.CustomerID == customerId && ccc.IsActive == 1 && coun.IsActive == true
                                 orderby coun.CountryName
                                 select new { coun.CountryID, coun.CountryName }).Distinct().ToList();

                var countriesList = countries.Select(x => new SelectListItem
                {
                    Value = x.CountryID > 0 ? x.CountryID.ToString() : "",
                    Text = x.CountryName
                }).ToList();

                return countriesList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountries");
                }
                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> GetAssetTypes()
        {
            try
            {
                var assetTypes = (from ADInfo in _dbCon.tblADDetails_RepEng
                                  where ADInfo.Status != null
                                  select new { ADInfo.Status }).ToList().Distinct().OrderBy(n => n.Status);

                var assetTypeList = assetTypes.Select(x => new SelectListItem
                {
                    Value = x.Status.ToString(),
                    Text = x.Status.ToString()
                }).ToList();

                return assetTypeList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetTypes");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetTypes");
                }
                return new List<SelectListItem>();

            }

        }
        public List<SelectListItem> GetProductCategories()
        {
            try
            {
                string[] names = { "Accessories", "DeskPhones-Headsets-Handsets", "Desktops", "Laptops", "Mobile Phones", 
                                   "Monitors", "Networking", "Printers & Options", "Projectors", "Servers", "Storage Devices", 
                                   "Tablets", "UPS & Power Devices", "Medical Devices   - 0-10 lbs", "Medical Devices  - 11-25 lbs",
                                   "Medical Devices  - more than  25 lbs", "Medical" };


                var categories = (from pc in _dbCon.tblPartCategories
                                  where pc.IsActive == true && names.Contains(pc.PartCategoryName)
                                  orderby pc.PartCategoryName
                                  select new { pc.PartCategoryID, pc.PartCategoryName }).ToList().Distinct();

                var categoryList = categories.Select(x => new SelectListItem
                {
                    Value = x.PartCategoryID.ToString(),
                    Text = x.PartCategoryName.ToString()
                }).ToList();

                return categoryList;
            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetProductCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProductCategories");
                }
                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> GetRegions(int customerId)
        {
            try
            {
                //Make query to fetch regions against CustomerID
                var regions = (from Reg in _dbCon.tblADRegions_RegEng
                               where Reg.CustomerID == customerId
                               orderby Reg.Region_Name
                               select new { Reg.RegionID, Reg.Region_Name }).Distinct().ToList();

                var regionList = regions.Select(x => new SelectListItem
                {
                    Value = x.RegionID > 0 ? x.RegionID.ToString() : "",
                    Text = x.Region_Name
                }).ToList();

                return regionList;
            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetRegions");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRegions");
                }
                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> GetSegments(int customerId)
        {

            var segments = (from seg in _dbCon.tblCustomerAddresses
                            where seg.CustomerID == customerId && seg.BuisnessSegment != null && seg.IsActive == 1 && seg.BuisnessSegment != ""
                            select new { seg.BuisnessSegment }).Distinct().ToList();

            var segmentList = segments.Select(x => new SelectListItem
            {
                Value = x.BuisnessSegment.ToString(),
                Text = x.BuisnessSegment.ToString()
            }).ToList();

            return segmentList;

        }

        public DataTable GetAssetDispositionReportData(string reportName, string customerId, string countryIds, string regionIds, string startDate, string endDate, string assetTypes, string partCategoryIDs, string type, string subType)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                if(type == "CountryWise")
                {
                    using (SqlCommand command = new SqlCommand("RE_sp_GetAssetSummaryRepByCountry", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                        command.Parameters[0].Value = customerId;
                        command.Parameters[1].Value = countryIds;
                        command.Parameters[2].Value = assetTypes;
                        command.Parameters[3].Value = partCategoryIDs;
                        command.Parameters[4].Value = startDate;
                        command.Parameters[5].Value = endDate;
                        command.CommandTimeout = 1000;
                        using (SqlDataAdapter da = new SqlDataAdapter(command))
                        {
                            da.Fill(dt);
                        }
                    }
                }
                else
                {
                    using (SqlCommand command = new SqlCommand("RE_sp_GetAssetSummaryRepByRegion", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@RegionIDs", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                        command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                        command.Parameters[0].Value = customerId;
                        command.Parameters[1].Value = countryIds;
                        command.Parameters[2].Value = assetTypes;
                        command.Parameters[3].Value = partCategoryIDs;
                        command.Parameters[4].Value = startDate;
                        command.Parameters[5].Value = endDate;
                        command.CommandTimeout = 1000;
                        using (SqlDataAdapter da = new SqlDataAdapter(command))
                        {
                            da.Fill(dt);
                        }
                    }
                }

                //using (SqlCommand command = new SqlCommand("sp_AssetDispositionMain_AdminR", con))
                //{
                //    command.CommandType = CommandType.StoredProcedure;

                //    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                //    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@Type", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@RegionIDs", SqlDbType.Text));
                //    command.Parameters.Add(new SqlParameter("@SubType", SqlDbType.Text));
                //    command.Parameters[0].Value = reportName;
                //    command.Parameters[1].Value = customerId;
                //    command.Parameters[2].Value = countryIds;
                //    command.Parameters[3].Value = assetTypes;
                //    command.Parameters[4].Value = partCategoryIDs;
                //    command.Parameters[5].Value = startDate;
                //    command.Parameters[6].Value = endDate;
                //    command.Parameters[7].Value = type;
                //    command.Parameters[8].Value = regionIds;
                //    command.Parameters[9].Value = subType;
                //    command.CommandTimeout = 1000;
                //    using (SqlDataAdapter da = new SqlDataAdapter(command))
                //    {
                //        da.Fill(dt);
                //    }
                //}

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetDispositionReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetDispositionReportData");
                }
                return null;
            }
        }

        public DataTable GetTotalAssetsByCountry(string reportName, string customerId, string countryIds, string startDate, string endDate, string assetTypes, string partCategoryIDs)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetTotalAssetsByCountry", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = countryIds;
                    command.Parameters[2].Value = assetTypes;
                    command.Parameters[3].Value = partCategoryIDs;
                    command.Parameters[4].Value = startDate;
                    command.Parameters[5].Value = endDate;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetDispositionReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetDispositionReportData");
                }
                return null;
            }
        }

        public DataTable GetTotalAssetsByRegion(string reportName, string customerId, string regionIds, string startDate, string endDate, string assetTypes, string partCategoryIDs)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetTotalAssetsByRegion", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@RegionIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = regionIds;
                    command.Parameters[2].Value = assetTypes;
                    command.Parameters[3].Value = partCategoryIDs;
                    command.Parameters[4].Value = startDate;
                    command.Parameters[5].Value = endDate;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetDispositionReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetDispositionReportData");
                }
                return null;
            }
        }

        //public void AddRegion(string countryIds,string regionName )
        //{
        //    if()

        //}

        public List<Region> GetRegionsByCustomerIdAndRegionName(int customerId, string regionName)
        {
            var RegNames = (from Reg in _dbCon.tblADRegions_RegEng
                            where Reg.Region_Name == regionName && Reg.CustomerID == customerId
                            select new { Reg.Region_Name }).ToList();
            var results = RegNames.Select(x => new Region
            {
                regionName = x.Region_Name.ToString()

            }).ToList();

            return results;

        }

        public string GetRegionCountriesByCustomerId(int customerId, int[] countryIds)
        {
            var msg = "";
            foreach (var countryId in countryIds)
            {
                var Countries = (from RegCoun in _dbCon.tblADCountries_RepEng
                                 join Reg in _dbCon.tblADRegions_RegEng on RegCoun.RegionID equals Reg.RegionID
                                 where RegCoun.CountryID == countryId && Reg.CustomerID == customerId
                                 select new { RegCoun.Country_Name, Reg.Region_Name }).ToList();
                if (Countries.Count > 0)
                {
                    msg = "Country: " + Countries.FirstOrDefault().Country_Name + " already exists in " + Countries.FirstOrDefault().Region_Name + ".";

                    //Exit event
                    break;
                }
            }

            return msg;

        }

        public int AddRegion(int customerId, int userId, string regionName, RegionCountries[] selectedCountries)
        {
            tblADRegions_RegEng ADReg = new tblADRegions_RegEng();

            //Assign values to entity columns
            ADReg.Region_Name = regionName;
            ADReg.CustomerID = customerId;
            ADReg.UpdatedByUserID = userId;
            ADReg.LastUpdated = DateTime.Today;


            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                _dbCon.tblADRegions_RegEng.Add(ADReg);
                count = _dbCon.SaveChanges();

                transaction.Commit();
            }

            var RegionID = (from Reg in _dbCon.tblADRegions_RegEng
                            select new { Reg.RegionID }).ToList().Last();

            count = AddRegionCountries(int.Parse(RegionID.RegionID.ToString()), selectedCountries);

            return count;

        }
        public int DeleteRegion(int customerId, int regionId)
        {
            try
            {
                //Make query to fetch countries against selected region
                var Countries = (from coun in _dbCon.tblADCountries_RepEng
                                 where coun.RegionID == regionId
                                 select coun).ToList();

                int count = 0;
                //Iterate through the countries to delete them
                foreach (var coun in Countries)
                {
                    using (var transaction = _dbCon.Database.BeginTransaction())
                    {
                        _dbCon.tblADCountries_RepEng.Remove(coun);
                        count = _dbCon.SaveChanges();

                        transaction.Commit();
                    }
                }

                //Make query to fetch region info against selected region
                var Regions = (from reg in _dbCon.tblADRegions_RegEng
                               where reg.RegionID == regionId
                               select reg).ToList();



                //Check if record is found
                if (Regions.Count > 0)
                {
                    using (var transaction = _dbCon.Database.BeginTransaction())
                    {
                        _dbCon.tblADRegions_RegEng.Remove(Regions.FirstOrDefault());
                        count = _dbCon.SaveChanges();

                        transaction.Commit();
                    }
                }

                return count;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public int[] GetCountryIdsByRegionId(int regionId)
        {
            try
            {
                //Make query to fetch countries against selected region
                var Countries = (from coun in _dbCon.tblADCountries_RepEng
                                 where coun.RegionID == regionId
                                 select coun.CountryID).ToList();

                int[] countryIds = Countries.ToArray();

                return countryIds;
            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetCountryIdsByRegionId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountryIdsByRegionId");
                }
                return null;
            }
        }

        public int DeleteRegionCountries(int regionId)
        {
            //Make query to fetch countries against selected region
            var Countries = (from coun in _dbCon.tblADCountries_RepEng
                             where coun.RegionID == regionId
                             select coun).ToList();

            int count = 0;
            //Iterate through the countries to delete them
            foreach (var coun in Countries)
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    _dbCon.tblADCountries_RepEng.Remove(coun);
                    count = _dbCon.SaveChanges();

                    transaction.Commit();
                }
            }
            return count;
        }

        public int AddRegionCountries(int regionId, RegionCountries[] selectedCountries)
        {
            var count = 0;
            foreach (var country in selectedCountries)
            {
                tblADCountries_RepEng ADCoun = new tblADCountries_RepEng();

                //Assign values to entity columns
                ADCoun.CountryID = int.Parse(country.Key);
                ADCoun.Country_Name = country.Value;
                ADCoun.RegionID = regionId;

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    _dbCon.tblADCountries_RepEng.Add(ADCoun);
                    count = _dbCon.SaveChanges();

                    transaction.Commit();
                }
            }

            return count;
        }

        #region Dashboard

        public DataTable GetAssetTrendinglist(int customerId)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetAssetTrendingReview_Dashboard", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters[0].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetTrendinglist");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetTrendinglist");
                }
                return null;
            }
        }

        public DataTable GetFinancialTrendinglist(int customerId)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetFinancialTrendingReview_Dashboard", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters[0].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetFinancialTrendinglist");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetFinancialTrendinglist");
                }
                return null;
            }
        }
        public DataTable GetAssetCollectionSummary(int customerId)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetAssetCollectionSummary_Dashboard", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters[0].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetCollectionSummary");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetFinancialTrendinglist");
                }
                return null;
            }
        }
        #endregion


        public DataTable GetAssetCollectionDispositionReportData(string customerId, string countryIds, string startDate, string endDate, string assetTypes, string partCategoryIDs)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetAssetCollectionReport", con))
                {
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));


                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = countryIds;
                    command.Parameters[2].Value = assetTypes;
                    command.Parameters[3].Value = partCategoryIDs;
                    command.Parameters[4].Value = startDate;
                    command.Parameters[5].Value = endDate;


                    command.CommandTimeout = 1000;
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetCollectionDispositionReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetCollectionDispositionReportData");
                }
                return null;
            }
        }
        public DataTable GetAssetfinancialDispositionReportData(string customerId, string countryIds, string startDate, string endDate, string assetTypes, string partCategoryIDs)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("RE_sp_GetAssetFinancialReport_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@AssetTypes", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@PartCategoryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));


                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = countryIds;
                    command.Parameters[2].Value = assetTypes;
                    command.Parameters[3].Value = partCategoryIDs;
                    command.Parameters[4].Value = startDate;
                    command.Parameters[5].Value = endDate;


                    command.CommandTimeout = 1000;
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetfinancialDispositionReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetfinancialDispositionReportData");
                }
                return null;
            }
        }


        public List<SelectListItem> fillSuppliers(int CustomerID)
        {
            try
            {
                //Make query to fetch suppliers who are assigned ticket(s) against given CustomerID
                var Suppliers = _adupload.GetSuppliersByCustomerID(CustomerID).Select(x => new SelectListItem
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();
                //(from sfa in _dbCon.tblSFAs
                //             join tck in _dbCon.tblTickets on sfa.SFAID equals tck.SFAID
                //             join ar in _dbCon.tblActionRequests on tck.ARID equals ar.ARID
                //             join cus in _dbCon.tblCustomers on ar.CompanyID equals cus.ParentCompanyID
                //             where cus.CustomerID == CustomerID && sfa.IsActive == true
                //             select new SelectListItem {Value = sfa.SFAID.ToString(),Text= sfa.SFAName }).Distinct().OrderBy(n => n.Text).ToList();

                return Suppliers;
            }
            //Catch block for exceptions
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "fillSuppliers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "fillSuppliers");
                }
                return new List<SelectListItem>();

            }
        }


        public List<int> fillTicketNumbers(int CustomerID, int SupplierID, bool chkbxHasAttachment, string FrmDate, string ToDate)
        {



            List<int> cancel = new List<int>();
            DateTime dtfrom = Convert.ToDateTime(FrmDate);
            DateTime dtTo = Convert.ToDateTime(ToDate);
            try
            {

                if (chkbxHasAttachment)
                {


                    var exceptionlist = (from ad in _dbCon.tblADFileUploads_RepEng
                                         select ad.TicketID).ToList();
                    //Make query to fetch ticket numbers against given CustomerID, SupplierID and CountryID
                    var TicketNOs = (from tck in _dbCon.tblTickets
                                     join ar in _dbCon.tblActionRequests on tck.ARID equals ar.ARID
                                     join cus in _dbCon.tblCustomers on ar.CompanyID equals cus.ParentCompanyID
                                     join sfa in _dbCon.tblSFAs on tck.SFAID equals sfa.SFAID
                                     join ta in _dbCon.tblTicketAttachments on tck.TicketID equals ta.ticketid
                                     join ts in _dbCon.tblTicketStatus on tck.TktStatusID equals ts.TktStatusID
                                     where cus.CustomerID == CustomerID && sfa.SFAID == SupplierID && tck.CreateDate > dtfrom
                                     && tck.CreateDate < dtTo && !ts.TktStatusName.Contains("Cancelled") && tck.IsActive == true
                                     orderby tck.TicketID
                                     select tck.TicketID).Distinct().ToList();

                    cancel = TicketNOs.Except(exceptionlist).ToList();


                }
                else
                {


                    var exceptionlist = (from ad in _dbCon.tblADFileUploads_RepEng
                                         select ad.TicketID).ToList();
                    //Make query to fetch ticket numbers against given CustomerID, SupplierID and CountryID
                    var TicketNOs = (from tck in _dbCon.tblTickets
                                     join ar in _dbCon.tblActionRequests on tck.ARID equals ar.ARID
                                     join cus in _dbCon.tblCustomers on ar.CompanyID equals cus.ParentCompanyID
                                     join sfa in _dbCon.tblSFAs on tck.SFAID equals sfa.SFAID
                                     join ts in _dbCon.tblTicketStatus on tck.TktStatusID equals ts.TktStatusID
                                     where cus.CustomerID == CustomerID && sfa.SFAID == SupplierID && tck.CreateDate > dtfrom
                                     && tck.CreateDate < dtTo && !ts.TktStatusName.Contains("Cancelled") && tck.IsActive == true
                                     orderby tck.TicketID
                                     select tck.TicketID).ToList();

                    cancel = TicketNOs.Except(exceptionlist).ToList();

                }




            }
            //Catch block for exceptions
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "fillTicketNumbers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "fillTicketNumbers");
                }

            }
            return cancel;
        }

        public string getPendingTickets(int CustomerId, int SupplierId, DateTime StartDate, DateTime EndDate, bool HasAttachment)
        {
            try
            {



                StringBuilder builder = new StringBuilder();
                List<sp_RE_PendingTicketsExport_Result> objlst = new List<sp_RE_PendingTicketsExport_Result>();

                objlst = _dbCon.sp_RE_PendingTicketsExport(CustomerId, SupplierId, StartDate, EndDate, HasAttachment).ToList<sp_RE_PendingTicketsExport_Result>();



                string cus = GetCustomers().Where(x => x.Value == CustomerId.ToString()).Select(x => x.Text).FirstOrDefault();
                string supplier = fillSuppliers(Convert.ToInt32(CustomerId)).Where(x => x.Value == SupplierId.ToString()).Select(x => x.Text).FirstOrDefault();

                builder.Append("," + "," + "," + "Customer: " + cus + "\n");
                builder.Append("," + "," + "," + "Supplier: " + supplier + "\n");
                builder.Append("," + "," + "," + "Date Range: " + StartDate + " - " + EndDate + "\n");
                if (HasAttachment)
                {
                    builder.Append("," + "," + "," + "Has Attachment \n\n");
                }
                builder.Append(" Pending Tickets, Created  \n");
                for (int i = 0; i < objlst.Count; i++)
                {
                    builder.Append(objlst[i].TicketID + " , " + objlst[i].CreateDate + "\n");
                }

                return builder.ToString();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "getPendingTickets");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "getPendingTickets");
                }
                return "";
            }
        }
    }
}

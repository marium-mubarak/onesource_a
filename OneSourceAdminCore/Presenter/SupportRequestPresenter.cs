﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : July 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This presenter Class is responsible for providing request/ticket support detail"
    /// </summary>
    public class SupportRequestPresenter
    {
        private readonly globalserveEntities _dbCon;
        public SupportRequestPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// this method is responsible for providing NewRequest View data For New request
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <param name="contactId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public SupportRequestSearchModel GetSupportRequestSeach(int parentCompanyId, int contactId, string userGroupName, int sfaId)
        {
            try
            {
                SupportRequestSearchModel obj = new Model.SupportRequestSearchModel();
                obj.companyId = parentCompanyId;
                obj.affectedUserId = contactId;
                //Request type

                var requestTypes = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && x.CompanyID == parentCompanyId);
                var requestType = requestTypes.OrderByDescending(x => x.ARTypeName).Select(x => new GenericList { Key = x.ARTypeID, Value = x.ARTypeName }).ToList();

                if (requestTypes.FirstOrDefault(x => x.IsDefault == true) != null)
                {
                    obj.requestTypeId = requestTypes.FirstOrDefault(x => x.IsDefault == true).ARTypeID;
                    obj.RequestTypeName = requestTypes.FirstOrDefault(x => x.IsDefault == true).ARTypeName;
                }
                if (requestType.Count == 0)
                {
                    requestTypes = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && (x.CompanyID == parentCompanyId || x.CompanyID == null));
                    requestType = requestTypes.OrderByDescending(x => x.ARTypeName).Select(x => new GenericList { Key = x.ARTypeID, Value = x.ARTypeName }).ToList();
                    if (requestTypes.FirstOrDefault(x => x.IsDefault == true) != null)
                    {
                        obj.requestTypeId = requestTypes.FirstOrDefault(x => x.IsDefault == true).ARTypeID;
                        obj.RequestTypeName = requestTypes.FirstOrDefault(x => x.IsDefault == true).ARTypeName;
                    }
                }
                obj.RequestType = requestType;
                //Priorities
                var globalServeCompany = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == "GlobalServe").Select(x => x.ParentCompanyID).ToList();
                bool includeGS = true;
                var priorities = _dbCon.tblPriorities.Where(x => x.CompanyID == parentCompanyId).ToList();
                includeGS = priorities.Count > 0 ? false : true;
                if (includeGS)
                {


                    obj.Priorities = (from p in _dbCon.tblPriorities
                                      join
                                      pc in _dbCon.tblParentCompanies on p.CompanyID equals pc.ParentCompanyID into ps
                                      from pc in ps.DefaultIfEmpty()
                                      where p.IsActive == true && (globalServeCompany.Contains(p.CompanyID) || p.CompanyID == parentCompanyId)
                                      orderby p.PriorityIntValue
                                      select new GenericList { Key = p.PriorityID, Value = p.PriorityName }).ToList();

                }
                else
                {
                    obj.Priorities = (from p in _dbCon.tblPriorities
                                      join
                                      pc in _dbCon.tblParentCompanies on p.CompanyID equals pc.ParentCompanyID into ps
                                      from pc in ps.DefaultIfEmpty()
                                      where p.IsActive == true && p.CompanyID == parentCompanyId
                                      orderby p.PriorityIntValue
                                      select new GenericList { Key = p.PriorityID, Value = p.PriorityName }).ToList();



                }


                var temPriorities = _dbCon.tblPriorities.Where(x => x.IsActive == true && x.CompanyID == parentCompanyId).ToList();

                if (temPriorities.Count == 0)
                {
                    int gsCompanyId = globalServeCompany[0];
                    var priority = temPriorities.LastOrDefault(x => x.IsActive == true && x.CompanyID == gsCompanyId && x.PriorityDefaultInt == x.PriorityIntValue);
                    if (priority != null)
                    {
                        obj.priorityId = priorities != null ? priority.PriorityID : 0;
                        obj.PriorityName = obj.Priorities.FirstOrDefault(x => x.Key == obj.priorityId) == null ? "" : obj.Priorities.FirstOrDefault(x => x.Key == obj.priorityId).Value;
                    }
                }
                else
                {
                    var priority = temPriorities.LastOrDefault(x => x.IsActive == true && x.CompanyID == parentCompanyId && x.PriorityDefaultInt == x.PriorityIntValue);
                    if (priority != null)
                    {
                        obj.priorityId = priorities != null ? priority.PriorityID : 0;
                        obj.PriorityName = obj.Priorities.FirstOrDefault(x => x.Key == obj.priorityId) == null ? "" : obj.Priorities.FirstOrDefault(x => x.Key == obj.priorityId).Value;
                    }
                }


                //
                obj.TimeZone = _dbCon.sp_getTimeZone_AdminR().ToList();

                //for Companies
                if (userGroupName == "ISD")
                {
                    var customerCompanyIds = _dbCon.tblCustomers.Where(x => x.IsActive == true).Select(x => x.ParentCompanyID).ToList();

                    obj.Companies = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == "GlobalServe" || customerCompanyIds.Contains(x.ParentCompanyID))
                        .Select(x => new GenericList { Key = x.ParentCompanyID, Value = x.ParentCompanyName }).OrderBy(x => x.Value).ToList();
                    obj.CompanyName = obj.Companies.FirstOrDefault(x => x.Key == parentCompanyId).Value;
                }
                else
                {

                    var query = @"select parentcompanyid as [Key], parentcompanyname as Value from tblparentcompanies where parentcompanyid = " + parentCompanyId + @"
                or parentcompanyid in (select parentcompanyid from tblcustomers where isactive = 1 and customerid in (
                  select customerid from tblsfacustomerwa where isactive = 1 and offeringid in (select offeringid from tblofferings where offeringshort = 'SUPPORT')
                and sfaid = " + sfaId + ")) order by parentcompanyname";

                    obj.Companies = _dbCon.Database.SqlQuery<GenericList>(query).ToList();
                    obj.CompanyName = obj.Companies.FirstOrDefault(x => x.Key == parentCompanyId).Value;
                }
                //
                //for locations                       

                if (_dbCon.tblCustomers.Where(x => x.ParentCompanyID == parentCompanyId).Count() > 0)
                {
                    var locations = (from ca in _dbCon.tblCustomerAddresses
                                     join c in _dbCon.tblCountries on ca.CountryID equals c.CountryID
                                     join d in _dbCon.tblCustomers on ca.CustomerID equals d.CustomerID
                                     where ca.IsActive == 1 && d.ParentCompanyID == parentCompanyId
                                     orderby c.CountryName, ca.CustomerAddressName
                                     select new { ca.CustomerAddressID, c.CountryName, ca.CustomerAddressName, ca.Address1 }).ToList();
                    obj.Locations = locations.Select(x => new GenericList
                    {
                        Key = x.CustomerAddressID,
                        Value = Convert.ToString(x.CountryName) + " - " + Convert.ToString(x.CustomerAddressName) + " " + Convert.ToString(x.Address1)
                    }).ToList();
                }
                else
                {
                    var locations = (from a in _dbCon.tblAddresses
                                     join
                                        pa in _dbCon.tblPartyAddresses on a.AddressID equals pa.AddressID
                                     join c in _dbCon.tblCountries on a.CountryID equals c.CountryID
                                     join d in _dbCon.tblParentCompanies on pa.PartyID equals d.PartyID
                                     where a.IsActive == true && pa.IsActive == true && d.ParentCompanyID == parentCompanyId
                                     orderby c.CountryName, pa.AddressName
                                     select new { a.AddressID, c.CountryName, pa.AddressName }).ToList();



                    obj.Locations = locations.Select(x => new GenericList
                    {
                        Key = x.AddressID,
                        Value = Convert.ToString(x.CountryName) + " - " + Convert.ToString(x.AddressName)
                    }).ToList();
                }

                // 

                //affected user
                var affectedUser = (from c in _dbCon.tblContacts
                                    join
                                       u in _dbCon.tblUsers on c.UserID equals u.UserID into ps
                                    from u in ps.DefaultIfEmpty()
                                    where c.IsActive == true && u.IsActive == 1 && c.CompanyID == parentCompanyId
                                    orderby c.FirstName, c.LastName
                                    select new { c.ContactID, c.FirstName, c.LastName, u.UserName }).ToList();
                obj.AffectedUser = affectedUser.Select(x => new GenericList
                {
                    Key = x.ContactID,
                    Value = Convert.ToString(x.FirstName) + " " + Convert.ToString(x.LastName) + " (" + Convert.ToString(x.UserName) + ")"
                }).ToList();
                //



                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter => GetSupportRequestSeach<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter => GetSupportRequestSeach");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupportRequestSeach");
                }
                return new Model.SupportRequestSearchModel();
            }
        }

        /// <summary>
        /// this method is responsible for adding digital asset
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="daid"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url, int userId, int? daid)
        {
            try
            {
                return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                                        null, null, null, null, null, null, null, null, null,
                                        null, null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter => AddDigitalAsset<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter => AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        /// <summary>
        /// This method is responsible for providing Ticket Info
        /// </summary>
        /// <param name="ticketid"></param>
        /// <param name="daid"></param>
        /// <param name="lastUpdate"></param>
        /// <param name="userId"></param>
        /// <returns>int</returns>
        public int AddTicketInfo(string ticketid, string daid, DateTime lastUpdate, string userId)
        {
            try
            {
                return _dbCon.sp_AddTicketInfo_R(ticketid, daid, lastUpdate, userId);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter => AddTicketInfo<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter => AddTicketInfo");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddTicketInfo");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for adding new request
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        /// <param name="serverTimezoneId"></param>
        /// <param name="userName"></param>
        /// <param name="userGroupName"></param>
        /// <returns></returns>
        public int AddRequest(SupportRequestSearchModel model, int userId, int contactId, int serverTimezoneId, string userName, string userGroupName)
        {
            try
            {
                var tblactionrequesttype = _dbCon.tblActionRequestTypes.FirstOrDefault(x => x.IsAssetMandatory == true && x.CompanyID == model.companyId);
                if (tblactionrequesttype != null)
                {
                    if (model.requestTypeId == tblactionrequesttype.ARTypeID)
                    {
                        return -10;
                    }
                }
                

                int ticketcount = 0;
                DateTime? thisYearDSTStart = null, thisYearDSTEnd = null, nextYearDSTStart = null, nextYearDSTEnd = null;
                int servertzbias, serverdst;
                if (model.RequestDateTime == null)
                {
                    model.RequestDateTime = DateTime.Now;
                }
                else
                {
                    model.RequestDateTime = Convert.ToDateTime(model.RequestDateTime).Date + new TimeSpan(model.Hour, model.Minute, 0);
                }

                var ServerTimeZoneBias = _dbCon.tblWorldTimeServers.FirstOrDefault(x => x.LocationID == "US-NY");
                if (ServerTimeZoneBias != null)
                {
                    thisYearDSTStart = ServerTimeZoneBias.ThisYearDSTStart;
                    thisYearDSTEnd = ServerTimeZoneBias.ThisYearDSTEnd;
                    nextYearDSTEnd = ServerTimeZoneBias.NextYearDSTEnd;
                    nextYearDSTStart = ServerTimeZoneBias.NextYearDSTStart;
                }
                if ((model.RequestDateTime >= Convert.ToDateTime(thisYearDSTStart) && model.RequestDateTime <= Convert.ToDateTime(thisYearDSTEnd)) ||
                    (model.RequestDateTime >= Convert.ToDateTime(nextYearDSTStart) && model.RequestDateTime <= Convert.ToDateTime(nextYearDSTEnd)))
                {
                    servertzbias = Convert.ToInt32(ServerTimeZoneBias.DstBias);
                    serverdst = 1;
                }
                else
                {
                    servertzbias = Convert.ToInt32(ServerTimeZoneBias.StdBias);
                    serverdst = 0;
                }

                //for timezone id
                if (!string.IsNullOrEmpty(model.TimeZoneId))
                {
                    model.RequestDateTime = AddTimeZoneDifference(model.RequestDateTime, servertzbias.ToString(), model.TimeZoneId);
                }
                //

                var ar = new GSSupport.ActionRequest();
                ar.SubmitUserID = userId;
                ar.AffectedContactID = contactId;
                ar.RequestDate = DateTime.Now;
                ar.RequestDateLocal = DateTime.Now;
                ar.RequestDateTimeZoneVBConst = serverTimezoneId;
                ar.Summary = model.Summary;
                ar.Description = model.Description;

                if (!string.IsNullOrEmpty(model.assetId))
                {
                    var assetId = Convert.ToInt32(model.assetId);
                    var assets = (from ao in _dbCon.GSNetP_tbl_AssetsOwned
                                  join
                                    a in _dbCon.GSNetP_tbl_Assets on ao.AssetsOwnedID equals a.AssetsOwnedID
                                  join pc in _dbCon.tblPartCategories on ao.CustomerPartCategoryID equals pc.PartCategoryID into ps
                                  from pc in ps.DefaultIfEmpty()
                                  where a.AssetID == assetId && a.IsActive == true
                                  select new { ao.AssetTypeID, ao.Model, ao.Manufacturer, ao.ManufacturerID, ao.CustomerPartCategoryID, pc.PartCategoryName }).ToList();

                    foreach (var item in assets)
                    {
                        if (!string.IsNullOrEmpty(item.AssetTypeID))
                        {
                            ar.AddFieldWithDisplay("assetmfgsku", item.AssetTypeID, item.AssetTypeID);
                            if (item.CustomerPartCategoryID != null)
                            {
                                ar.AddFieldWithDisplay("assetcustomercategory", Convert.ToString(item.CustomerPartCategoryID), item.PartCategoryName);
                            }
                            ar.AddFieldWithDisplay("assetmodel", item.Model, item.Model);
                            if (item.ManufacturerID != null)
                            {
                                ar.AddFieldByDisplay("Manufacturer of Affected Asset", Convert.ToString(item.ManufacturerID), item.Manufacturer);
                            }
                        }

                    }
                    ar.SerialNum = model.serialNum.Trim();
                    ar.AddFieldByDisplay("Serial Number", model.serialNum);
                    ar.AssetTag = model.assetTag;
                }
                int coverageid = 0;
                if (model.locationId > 0)
                {
                    var address = _dbCon.tblCustomerAddresses.FirstOrDefault(x => x.CustomerAddressID == model.locationId);
                    if (address != null)
                    {
                        coverageid = Convert.ToInt32(address.CoverageID);
                    }

                }

                if (!string.IsNullOrEmpty(model.assetId))
                {
                    ar.GSAssetID = Convert.ToInt32(model.assetId);
                    var asset = _dbCon.GSNetP_tbl_Assets.FirstOrDefault(x => x.AssetID == ar.GSAssetID);
                    if (asset != null)
                    {
                        coverageid = Convert.ToInt32(asset.CoverageID);
                    }
                }

                ar.AddFieldByDisplay("Coverage Type", coverageid.ToString());

                ar.SubmitDate = DateTime.Now;
                ar.RequestDate = Convert.ToDateTime(model.RequestDateTime);
                ar.RequestDateTimeZoneID = 0;
                ar.RequestDateLocalGMTOffset = Convert.ToInt32(model.gmtoffset);
                ar.RequestDateLocalGMTOffsetString = model.gmtoffsetstring;

                if (!string.IsNullOrEmpty(model.SupplierTicket))
                {
                    ar.SupplierRequestID = model.SupplierTicket;
                    ar.AddFieldByDisplay("GSP Ticket ID", model.SupplierTicket);
                }
                if (!string.IsNullOrEmpty(model.CustomerTicket))
                {
                    ar.CustomerRequestID = model.CustomerTicket;
                    ar.AddFieldByDisplay("Customer Ticket ID", model.CustomerTicket);
                }
                if (!string.IsNullOrEmpty(model.ContactName))
                {
                    ar.TicketContactName = model.ContactName;
                    ar.AddFieldByDisplay("Ticket Contact Name", model.ContactName);
                }
                if (!string.IsNullOrEmpty(model.ContactPhone))
                {
                    ar.TicketContactPhone = model.ContactPhone;
                    ar.AddFieldByDisplay("Ticket Contact Phone", model.ContactPhone);
                }

                ar.SetCompanyID(model.companyId, model.CompanyName);
                ar.SetPriorityID(model.priorityId, model.PriorityName);
                ar.AddFieldByDisplay("Ticket Priority", model.priorityId.ToString(), model.PriorityName);
                ar.SetTypeID(model.requestTypeId, model.RequestTypeName);
                ar.AddFieldByDisplay("Request Type", model.requestTypeId.ToString(), model.RequestTypeName);

                var customer = _dbCon.tblCustomers.Where(x => x.ParentCompanyID == model.companyId).ToList();

                if (model.locationId > 0)
                {

                    if (customer.Count > 0)
                    {
                        var aa = (from a in _dbCon.tblCustomerAddresses
                                  join
                                    c in _dbCon.tblCountries on a.CountryID equals c.CountryID
                                  where a.CustomerAddressID == model.locationId
                                  select new { a.Address1, a.Address2, a.City, a.StateProvinceRegion, a.PostalCode, c.CountryName }).FirstOrDefault();
                        if (aa != null)
                        {
                            var displayName = model.LocationName + (aa.Address1 == null ? "" : "<br>" + aa.Address1) + (aa.Address2 == null ? "" : "<br>" + aa.Address2) +
                                (aa.City == null ? "" : "<br>" + aa.City) + (aa.StateProvinceRegion == null ? "" : "<br>" + aa.StateProvinceRegion) +
                                (aa.PostalCode == null ? "" : "<br>" + aa.PostalCode) + (aa.CountryName == null ? "" : "<br>" + aa.CountryName);
                            ar.SetAddressID(model.locationId, displayName);
                        }

                    }
                    else
                    {
                        var aa = (from a in _dbCon.tblAddresses
                                  join c in _dbCon.tblCountries on a.CountryID equals c.CountryID
                                  where a.AddressID == model.locationId
                                  select new { a.Address1, a.Address2, a.City, a.StateProvinceRegion, a.PostalCode, c.CountryName }).FirstOrDefault();
                        if (aa != null)
                        {
                            var displayName = model.LocationName + (aa.Address1 == null ? "" : "<br>" + aa.Address1) + (aa.Address2 == null ? "" : "<br>" + aa.Address2) +
                                 (aa.City == null ? "" : "<br>" + aa.City) + (aa.StateProvinceRegion == null ? "" : "<br>" + aa.StateProvinceRegion) +
                                 (aa.PostalCode == null ? "" : "<br>" + aa.PostalCode) + (aa.CountryName == null ? "" : "<br>" + aa.CountryName);
                            ar.SetAddressID(model.locationId, displayName);
                        }
                    }
                }

                if (model.affectedUserId > 0)
                {
                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.ContactID == model.affectedUserId);
                    if (contact != null)
                    {
                        ar.AddFieldByDisplay("Affected Contact Phone", contact.WorkPhoneNumber ?? "");
                    }
                }
                if (userId > 0)
                {
                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == userId);
                    if (contact != null)
                    {
                        ar.AddFieldByDisplay("Submitting User Phone", contact.WorkPhoneNumber ?? "");
                    }
                }


                if (model.locationId > 0)
                {
                    int countryId;
                    if (customer.Count > 0)
                    {
                        var address = _dbCon.tblCustomerAddresses.FirstOrDefault(x => x.CustomerAddressID == model.locationId);
                        countryId = address == null ? 0 : address.CountryID;
                    }
                    else
                    {
                        var address = _dbCon.tblAddresses.FirstOrDefault(x => x.AddressID == model.locationId);
                        countryId = address == null ? 0 : address.CountryID;
                    }
                    var country = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == countryId);
                    ar.AddFieldByDisplay("Country of Affected User", countryId.ToString(), country == null ? "Nothing" : country.CountryName);
                    if (!string.IsNullOrEmpty(model.assetId) && model.assetId != "0")
                    {
                        ar.AddFieldByDisplay("Country of Affected User", countryId.ToString(), country == null ? "Nothing" : country.CountryName);
                    }
                }

                ar.AffectedContactID = model.affectedUserId;

                ar.AddFieldByDisplay("Ticket Summary", model.Summary);
                ar.AddFieldByDisplay("Ticket Description", model.Description);
                ar.AddFieldByDisplay("Ticket Submitted By", userId.ToString(), userName);
                ar.AddFieldByDisplay("Ticket Affected User", model.affectedUserId.ToString(), model.AffectedUserName);

                //' CustomerID field gets set to companyid for now...
                ar.CustomerID = model.companyId;
                ar.Qualify = true;
                ar.GenerateTicket = true;
                ar.GSComments = model.GSOnlyComment;
                //' Category is new request
                ar.CategoryName = "New Request";

                ar.TicketCreateLocal = Convert.ToDateTime(model.RequestDateTime);

                ar.AddFieldWithDisplay("customeraddressid", model.locationId.ToString(), model.LocationName);
                ar.AddFieldWithDisplay("tktsummary", model.Summary, model.Summary);
                ar.AddFieldWithDisplay("tktdesc", model.Description, model.Description);
                ar.AddFieldWithDisplay("tktaffecteduser", model.affectedUserId.ToString(), model.AffectedUserName);
                ar.AddFieldWithDisplay("tktsubmitby", userId.ToString(), userName);

                if (!ar.Submit())
                {
                    return -1;
                }
                else
                {
                    ticketcount = ar.GSTicketID;

                    //------------------For specific workflow (Ticket - ONESOURCE-310)------------------
                    //Date:        25 Jan 2012

                    //Written By:  Aurangzaib Najam, Tahir Masood 

                    //Description: Following is the implementation to execute particular type of workflow which is mentioned in Ticket (ONESOURCE-310)
                    //             This type of workflow contains Request type (i:e; New Request for this workflow), customer, country and address.
                    //             It also contains an action to be launched if criteria is met. 
                    //             Criteria for this type of worfklow is that if there is a setup for supplier against customer and its address,
                    //             then assign a supplier to ticket when it is created by that customer and having that address.

                    var formulaDetail = _dbCon.tblFormulaDetails.Where(x => x.fieldDisplayValue == model.CompanyName.Trim()).Select(x => x.formulaID).ToList();

                    foreach (var rsfid in formulaDetail)
                    {
                        var fidCount = _dbCon.tblFormulaDetails.Where(x => x.formulaID == rsfid).ToList().Count;
                        if (fidCount == 6)
                        {
                            var fieldIds = _dbCon.tblFormulaDetails.Where(x => x.formulaID == rsfid).Select(x => x.fieldID).ToList();
                            if (fieldIds[0] == 121 && fieldIds[2] == 121 && fieldIds[4] == 121 && fieldIds[1] == 100 && fieldIds[3] == 9 && fieldIds[5] == 12)
                            {
                                var workflowaddIDs = _dbCon.tblFormulaDetails.Where(x => x.fieldID == 12 && x.formulaID == rsfid).Select(x => x.fieldTextValue);
                                foreach (var workflowaddID in workflowaddIDs)
                                {
                                    if (Convert.ToInt32(workflowaddID.Trim()) == model.locationId)
                                    {
                                        var actionInfo = (from a in _dbCon.tblActions
                                                          join
                                                          w in _dbCon.tblWorkflowDetails on a.ActionID equals w.actionid
                                                          join q in _dbCon.tblQualifications on w.qualificationID equals q.qualificationID
                                                          join fd in _dbCon.tblFormulaDetails on q.formulaid equals fd.formulaID
                                                          where fd.formulaID == rsfid && fd.fieldID == 12
                                                          select new { a.ActionID, a.ActionTypeID, a.FieldTypeID }).FirstOrDefault();
                                        if (Convert.ToInt32(actionInfo.ActionTypeID) == 3)
                                        {
                                            var supplierID = (from f in _dbCon.tblFields
                                                              join ftr in _dbCon.tblFieldTypeRelations on f.fieldID equals ftr.fieldID
                                                              join afdv in _dbCon.tblActionFieldDefaultValues on f.fieldID equals afdv.FieldID
                                                              where ftr.fieldTypeID == actionInfo.FieldTypeID && afdv.ActionID == actionInfo.ActionID
                                                              orderby ftr.fieldDisplayOrder
                                                              select afdv.FieldTxtValue).FirstOrDefault();
                                            if (supplierID != null)
                                            {

                                                int customerId = 0;
                                                if (userGroupName == "ISD")
                                                {
                                                    customerId = customer.Count > 0 ? customer[0].CustomerID : 0;
                                                }
                                                else
                                                {
                                                    model.CompanyName = model.CompanyName.Trim();
                                                    var temcustomer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerName == model.CompanyName);
                                                    if (temcustomer != null)
                                                    {
                                                        customerId = temcustomer.CustomerID;
                                                    }
                                                }
                                                var sfaCustomer = _dbCon.tblSFACustomerContacts.Where(x => x.SFAID == Convert.ToInt32(supplierID) && x.CustomerID == customerId).ToList().Count;
                                                if (sfaCustomer > 0)
                                                {

                                                    var tblticket = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketcount);
                                                    if (tblticket != null)
                                                    {
                                                        using (var transaction = _dbCon.Database.BeginTransaction())
                                                        {
                                                            tblticket.SFAID = Convert.ToInt32(supplierID);
                                                            tblticket.LastUpdate = DateTime.Now;
                                                            tblticket.LastUpdateUserID = userId;
                                                            _dbCon.Entry(tblticket).State = System.Data.Entity.EntityState.Modified;
                                                            _dbCon.SaveChanges();
                                                            transaction.Commit();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                return ticketcount;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter => AddRequest<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter => AddRequest");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddRequest");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for providing timezone difference
        /// </summary>
        /// <param name="RequestDate"></param>
        /// <param name="LocalTimezoneBias"></param>
        /// <param name="ServerTimezoneBias"></param>
        /// <returns></returns>
        public DateTime? AddTimeZoneDifference(DateTime? RequestDate, string LocalTimezoneBias, string ServerTimezoneBias)
        {
            try
            {

                string ltzHrs = null;
                string ltzMins = null;
                DateTime? ConvertedDate = null;
                string stzHrs = null;
                string stzMins = null;

                if (LocalTimezoneBias.Contains("+"))
                {
                    var LocalTimezoneArr = LocalTimezoneBias.Split('+');
                    if (LocalTimezoneArr[1].Length == 3)
                    {
                        ltzHrs = LocalTimezoneArr[1].Substring(0, 1);
                        ltzMins = LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 1);
                    }
                    else if (LocalTimezoneArr[1].Length == 4)
                    {
                        ltzHrs = LocalTimezoneArr[1].Substring(0, 2);
                        ltzMins = LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 2);
                    }
                }

                else if (LocalTimezoneBias.Contains("-"))
                {
                    var LocalTimezoneArr = LocalTimezoneBias.Split('-');
                    if (LocalTimezoneArr[1].Length == 3)
                    {
                        ltzHrs = "-" + LocalTimezoneArr[1].Substring(0, 1);
                        ltzMins = "-" + LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 1);
                    }
                    else if (LocalTimezoneArr[1].Length == 4)
                    {
                        ltzHrs = "-" + LocalTimezoneArr[1].Substring(0, 2);
                        ltzMins = "-" + LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 2);
                    }
                }
                else
                {
                    if (LocalTimezoneBias.Length == 3)
                    {
                        ltzHrs = LocalTimezoneBias.Substring(0, 1);
                        ltzMins = LocalTimezoneBias.Substring(LocalTimezoneBias.Length - 2, LocalTimezoneBias.Length - 1);
                    }
                    else if (LocalTimezoneBias.Length == 4)
                    {
                        ltzHrs = LocalTimezoneBias.Substring(0, 2);
                        ltzMins = LocalTimezoneBias.Substring(LocalTimezoneBias.Length - 2, LocalTimezoneBias.Length - 2);
                    }
                }



                if (ServerTimezoneBias.Contains("+"))
                {
                    var ServerTimezoneArr = ServerTimezoneBias.Split('+');
                    if (ServerTimezoneArr[1].Length == 3)
                    {
                        stzHrs = ServerTimezoneArr[1].Substring(0, 1);
                        stzMins = ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 1);
                    }
                    else if (ServerTimezoneArr[1].Length == 4)
                    {
                        stzHrs = ServerTimezoneArr[1].Substring(0, 2);
                        stzMins = ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 2);
                    }
                }
                else if (ServerTimezoneBias.Contains("-"))
                {
                    var ServerTimezoneArr = ServerTimezoneBias.Split('-');
                    if (ServerTimezoneArr[1].Length == 3)
                    {
                        stzHrs = "-" + ServerTimezoneArr[1].Substring(0, 1);
                        stzMins = "-" + ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 1);
                    }
                    else if (ServerTimezoneArr[1].Length == 4)
                    {
                        stzHrs = "-" + ServerTimezoneArr[1].Substring(0, 2);
                        stzMins = "-" + ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 2);
                    }
                }
                else
                {
                    if (ServerTimezoneBias.Length == 3)
                    {
                        stzHrs = ServerTimezoneBias.Substring(0, 1);
                        stzMins = ServerTimezoneBias.Substring(ServerTimezoneBias.Length - 2, ServerTimezoneBias.Length - 1);
                    }
                    else if (ServerTimezoneBias.Length == 4)
                    {
                        stzHrs = ServerTimezoneBias.Substring(0, 2);
                        stzMins = ServerTimezoneBias.Substring(ServerTimezoneBias.Length - 2, ServerTimezoneBias.Length - 2);
                    }
                }

                if (RequestDate == null)
                {
                    return null;
                }

                int Hrs = Convert.ToInt32(ltzHrs) - Convert.ToInt32(stzHrs);
                int Mins = Convert.ToInt32(ltzMins) - Convert.ToInt32(stzMins);

                ConvertedDate = Convert.ToDateTime(RequestDate).AddHours(Convert.ToDouble(Hrs)).AddMinutes(Convert.ToDouble(Mins));

                return ConvertedDate;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter => AddTimeZoneDifference<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter => AddTimeZoneDifference");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddTimeZoneDifference");
                }
                return null;
            }
        }

        /// <summary>
        /// Get List of Asset Country Address against customer id 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="group"></param>
        /// <returns>List of countries addresses</returns>
        public List<AssetTagAddress> GetAssetCountryAddress(int companyId, string userGroup, int sfaId)
        {
            try
            {
                string query = string.Empty;
                if (userGroup == "ISD")
                {
                    query =
                       "select distinct awa.customeraddressid as CustomerAddressId, " +
                       " substring(c.countryname + ' - ' + awa.condensedaddress + ' ' + awa.address1, 0, 60) as CustomerAddress " +
                       " from gsnetp_tbl_addrwithassets awa " +
                    " inner join gsnetp_tbl_assets a on a.addrwithassetsid=awa.addrwithassetsid and a.isactive=1 " +
                    " inner join tblcountries c on c.countryid=awa.countryid  inner join tblcustomers cust on cust.parentcompanyid=" + companyId +
                    " and cust.customerid=awa.customerid order by CustomerAddress ";
                }
                else
                {
                    query =
                     "select distinct awa.customeraddressid," +
                     " substring(c.countryname + ' - ' + awa.condensedaddress + ' ' + awa.address1, 0, 60) as customeraddressname " +
                     "  from gsnetp_tbl_addrwithassets awa " +
                  " inner join gsnetp_tbl_assets a on a.addrwithassetsid=awa.addrwithassetsid and a.isactive=1 " +
                  " inner join tblcountries c on c.countryid=awa.countryid " +
                  " inner join tblofferings ofe on ofe.offeringshort='SUPPORT' inner join tblsfacustomerwa wa on wa.sfaid=" + sfaId
                  + " and wa.offeringid=ofe.offeringid and wa.customerid=awa.customerid and wa.isactive=1 "
                  + "  inner join tblcustomers cust on cust.parentcompanyid=" + companyId +
                  " and cust.customerid=awa.customerid order by customeraddressname";
                }

                var results = _dbCon.Database.SqlQuery<AssetTagAddress>(query).ToList();


                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetAssetCountryAddress<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetAssetCountryAddress");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetCountryAddress");
                }
                return new List<AssetTagAddress>();
            }
        }

        /// <summary>
        /// this method is responsible for getting manufacturer list
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<GenericList> GetManufacturersList(int companyId, string userGroup, int sfaId)
        {
            try
            {
                string query = string.Empty;
                if (userGroup == "ISD")
                {
                    query = "select distinct ao.manufacturerid as [Key], mfg.manufacturername as Value from gsnetp_tbl_assetsowned ao " +
                          " inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1 " +
                          " inner join tblmanufacturers mfg on mfg.manufacturerid=ao.manufacturerid " +
                          " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + " and cust.customerid=ao.customerid order by mfg.manufacturername ";

                }
                else
                {
                    query = "select distinct ao.manufacturerid as [Key], mfg.manufacturername as Value  from gsnetp_tbl_assetsowned ao " +
                             "  inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1 " +
                             " inner join tblmanufacturers mfg on mfg.manufacturerid=ao.manufacturerid " +
                             " inner join tblofferings ofe on ofe.offeringshort='SUPPORT' inner join tblsfacustomerwa wa on wa.sfaid=" + sfaId
                             + " and wa.offeringid=ofe.offeringid and wa.customerid=ao.customerid and wa.isactive=1 " +
                             " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + "and cust.customerid=ao.customerid order by mfg.manufacturername";

                }
                var results = _dbCon.Database.SqlQuery<GenericList>(query).ToList();


                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetManufacturersList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetManufacturersList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturersList");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting category list
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<GenericList> GetCategoryList(int companyId, string userGroup, int sfaId)
        {
            try
            {
                string query = string.Empty;
                if (userGroup == "ISD")
                {
                    query = "select distinct ao.customerpartcategoryid as [Key], pc.partcategoryname as Value" +
                " from gsnetp_tbl_assetsowned ao" +
                " inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1" +
                " inner join tblpartcategories pc on pc.partcategoryid=ao.customerpartcategoryid" +
                " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + " and cust.customerid=ao.customerid order by pc.partcategoryname";

                }
                else
                {
                    query = "select distinct ao.customerpartcategoryid as [Key], pc.partcategoryname as Value" +
                  " from gsnetp_tbl_assetsowned ao" +
                  " inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1" +
                  " inner join tblpartcategories pc on pc.partcategoryid=ao.customerpartcategoryid" +
                  " inner join tblofferings ofe on ofe.offeringshort='SUPPORT'" +
                  " inner join tblsfacustomerwa wa on wa.sfaid=" + sfaId +
                  "and wa.offeringid=ofe.offeringid and wa.customerid=ao.customerid and wa.isactive=1 " +
                  " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + "and cust.customerid=ao.customerid order by pc.partcategoryname";

                }
                var results = _dbCon.Database.SqlQuery<GenericList>(query).ToList();


                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetCategoryList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetCategoryList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCategoryList");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting status list
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<GenericList> GetStatusList(int companyId)
        {
            try
            {
                var chkCount = "select assetstatusid from GSNetP_tbl_AssetStatus where isactive=1 and companyid= " + companyId;
                var chkResult = _dbCon.Database.SqlQuery<int>(chkCount).ToList();
                var query = "";
                if (chkResult.Count > 0)
                {
                    query = "select assetstatusid as [Key], status as Value from GSNetP_tbl_AssetStatus" +
                                       " left join tblParentCompanies on GSNetP_tbl_assetstatus.CompanyID=tblParentCompanies.parentcompanyid" +
                                       " where GSNetP_tbl_assetstatus.isActive=1 and companyid =" + companyId +
                                       " order by parentcompanyname, statusIntValue";
                }
                else
                {
                    query = "select assetstatusid as [Key], status as Value from GSNetP_tbl_AssetStatus " +
                                " left join tblParentCompanies on GSNetP_tbl_assetstatus.CompanyID=tblParentCompanies.parentcompanyid" +
                                " where GSNetP_tbl_assetstatus.isActive=1 and companyid =" + companyId + " or companyid is null" +
                                " order by parentcompanyname, statusIntValue";
                }

                var results = _dbCon.Database.SqlQuery<GenericList>(query).ToList();

                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetStatusList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetStatusList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetStatusList");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting asset tag search popup data
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public AssetPopupModel GetAssetTagSearchPopupData(int companyId, string userGroup, int sfaId)
        {
            try
            {
                AssetPopupModel obj = new Model.AssetPopupModel();

                obj.AssetTagAddress = GetAssetCountryAddress(companyId, userGroup, sfaId);
                obj.AssetManufacturList = GetManufacturersList(companyId, userGroup, sfaId);
                obj.AssetCategoryList = GetCategoryList(companyId, userGroup, sfaId);
                obj.AssetStatuses = GetStatusList(companyId);
                obj.ParentCompanyId = companyId;
                return obj;

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetAssetTagSearchPopupData<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetAssetTagSearchPopupData");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetTagSearchPopupData");
                }
                return new AssetPopupModel();
            }
        }

        /// <summary>
        /// This method is responsible for asset grid data
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="serialNumber"></param>
        /// <param name="assetLocationId"></param>
        /// <param name="assetStatusId"></param>
        /// <param name="assetCategoryId"></param>
        /// <param name="assetManufactureId"></param>
        /// <param name="assetPartNumber"></param>
        /// <param name="assetTag"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<Sp_GetAssetPopupGrid_AdminR_Result> GetAssetDataGridDataList(string companyId, string userGroupName, string serialNumber, string assetLocationId,
                       string assetStatusId, string assetCategoryId, string assetManufactureId, string assetPartNumber, string assetTag, string sfaId)
        {
            try
            {
                return _dbCon.Sp_GetAssetPopupGrid_AdminR(userGroupName, companyId, serialNumber, assetTag, assetLocationId == "0" ? "" : assetLocationId, assetManufactureId, assetPartNumber, assetCategoryId, assetStatusId == "0" ? "" : assetStatusId, sfaId).ToList();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetAssetDataGridDataList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetAssetDataGridDataList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetDataGridDataList");
                }
                return new List<Sp_GetAssetPopupGrid_AdminR_Result>();
            }

        }

        /// <summary>
        /// this method is responsible for providing support request list
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="parentcompanyId"></param>
        /// <param name="filterId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="SFAId"></param>
        /// <param name="ContactId"></param>
        /// <param name="requestFilterName"></param>
        /// <returns></returns>
        public SupportRequestListModel GetSupportRequestList(int userId, int parentcompanyId, string filterId, string userGroupName, string SFAId, string ContactId)
        {
            string requestFilterName = "RequestFilter";
            SupportRequestListModel obj = new SupportRequestListModel();
            try
            {
                obj.FilterId = filterId;

                if (string.IsNullOrEmpty(filterId))
                {
                    var preference = _dbCon.tblPreferences.FirstOrDefault(x => x.PreferenceName == requestFilterName);
                    if (preference != null)
                    {
                        var userCount = _dbCon.tblUserPreferences.Where(x => x.UserID == userId && x.PreferenceID == preference.PreferenceID).ToList().Count;
                        if (userCount > 0)
                        {
                            var userPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == preference.PreferenceID && x.UserID == userId);
                            if (userPreference != null)
                            {
                                obj.FilterId = userPreference.PreferenceValue;
                            }
                        }
                    }
                }

                var contacts = _dbCon.tblContacts.Where(x => x.CompanyID == parentcompanyId).Select(x => x.UserID).ToList();

                obj.Filters = _dbCon.tblRequestFilters.Where(x => x.OwnerID == userId || (x.IsPublic == true && contacts.Contains(x.OwnerID))).
                    OrderBy(x => x.FilterName).Select(x => new GenericList { Key = x.FilterID, Value = x.FilterName }).ToList();

                obj.Request = _dbCon.sp_getSupportRequest_AdminR(userGroupName, obj.FilterId, SFAId, ContactId, userId.ToString()).ToList();


                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetSupportRequestList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetSupportRequestList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupportRequestList");
                }
                return obj;
            }
        }

        /// <summary>
        /// this method is responsible for making default filter
        /// </summary>
        /// <param name="preferenceName"></param>
        /// <param name="filterValue"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int MakeDefaultFilter( string filterValue, int userId)
        {
            try
            {
                int count = 0;
                string preferenceName = "RequestFilter";
                var preference = _dbCon.tblPreferences.FirstOrDefault(x => x.PreferenceName == preferenceName);
                if (preference != null)
                {
                    var userCount = _dbCon.tblUserPreferences.Where(x => x.UserID == userId && x.PreferenceID == preference.PreferenceID).ToList().Count;
                    if (userCount > 0)
                    {
                        var userPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == preference.PreferenceID && x.UserID == userId);
                        if (userPreference != null)
                        {
                            using (var transaction = _dbCon.Database.BeginTransaction())
                            {
                                userPreference.LastUpdated = DateTime.Now;
                                userPreference.LastUpdatedByUserID = userId;
                                userPreference.PreferenceValue = filterValue;
                                _dbCon.Entry(userPreference).State = System.Data.Entity.EntityState.Modified;
                                count = _dbCon.SaveChanges();
                                transaction.Commit();
                            }
                        }
                    }
                    else
                    {
                        using (var transaction = _dbCon.Database.BeginTransaction())
                        {
                            tblUserPreference obj = new Entity.tblUserPreference();
                            obj.PreferenceID = preference.PreferenceID;
                            obj.PreferenceValue = filterValue;
                            obj.UserID = userId;
                            obj.LastUpdatedByUserID = userId;
                            obj.LastUpdated = DateTime.Now;
                            obj.IsActive = true;

                            _dbCon.tblUserPreferences.Add(obj);
                            count = _dbCon.SaveChanges();
                            transaction.Commit();
                        }

                    }
                }


                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>MakeDefaultFilter<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>MakeDefaultFilter");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "MakeDefaultFilter");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for providing new request filter view
        /// </summary>
        /// <param name="userGroupName"></param>
        /// <param name="sfaId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public NewRequestFilterModel GetNewRequestFilterModel(string userGroupName, int sfaId, NewRequestFilterModel obj)
        {
            try
            {
                // NewRequestFilterModel obj = new Model.NewRequestFilterModel();
                obj.CompanyIds = obj.CompanyIds ?? "";

                if (userGroupName == "SFA")
                {
                    obj.Companies = (from p in _dbCon.tblParentCompanies
                                     join
                                      c in _dbCon.tblCustomers on p.ParentCompanyID equals c.ParentCompanyID
                                     join
                                      s in _dbCon.tblSFACustomerContacts on c.CustomerID equals s.CustomerID
                                     where s.SFAID == sfaId && p.IsActive == true
                                     orderby p.ParentCompanyName
                                     select new GenericList { Key = p.ParentCompanyID, Value = p.ParentCompanyName }
                                     ).Distinct().ToList();

                    //action request Type     
                    List<int?> customerIds = new List<int?>();
                    if (!string.IsNullOrEmpty(obj.CompanyIds))
                    {
                        foreach (var item in obj.CompanyIds.Split(',').Select(int.Parse).ToList())
                        {
                            customerIds.Add((int?)item);
                        }
                    }
                    else
                    {
                        customerIds.Add(0);
                    }
                    int ArTypeCount = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && customerIds.Contains(x.CompanyID)).ToList().Count;
                    if (ArTypeCount == 0)
                    {
                        obj.ActionTypes = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && (customerIds.Contains(x.CompanyID) || x.CompanyID == null)).OrderByDescending(x => x.ARTypeName)
                              .Select(x => new GenericList { Value = x.ARTypeName, Key = x.ARTypeID }).ToList();
                    }
                    else
                    {
                        obj.ActionTypes = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && customerIds.Contains(x.CompanyID)).OrderByDescending(x => x.ARTypeName)
                          .Select(x => new GenericList { Value = x.ARTypeName, Key = x.ARTypeID }).ToList();
                    }
                    //
                    //status request Type     
                    List<int?> typeIds = new List<int?>();
                    if (!string.IsNullOrEmpty(obj.TypeIds))
                    {
                        foreach (var item in obj.TypeIds.Split(',').Select(int.Parse).ToList())
                        {
                            typeIds.Add((int?)item);
                        }
                    }
                    else
                    {
                        typeIds.Add(0);
                    }
                    bool includeGS = false, areCustomTypeStatuses = false, areCustomStatuses = false;

                    var gsId = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyName == "GlobalServe").ParentCompanyID;
                    if (customerIds.Contains(gsId))
                    {
                        includeGS = true;
                    }
                    foreach (var item in customerIds)
                    {
                        var ticketStatuses = _dbCon.tblTicketStatus.Where(x => x.IsActive && x.CompanyID == item).ToList();
                        if (ticketStatuses.Count == 0)
                        {
                            includeGS = true;
                        }
                        else
                        {
                            areCustomStatuses = true;
                            ticketStatuses = _dbCon.tblTicketStatus.Where(x => x.IsActive && x.CompanyID == item && typeIds.Contains(x.ARTypeID)).ToList();
                            if (ticketStatuses.Count > 0)
                            {
                                areCustomTypeStatuses = true;
                            }
                        }
                    }

                    if (includeGS)
                    {

                        var Statuses = (from ts in _dbCon.tblTicketStatus
                                        join
                                        pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                        from pc in ps.DefaultIfEmpty()
                                        join art in _dbCon.tblActionRequestTypes on ts.ARTypeID equals art.ARTypeID into ps1
                                        from art in ps1.DefaultIfEmpty()
                                        where ts.IsActive == true
                                        orderby ts.StatusIntValue
                                        select new { ts.TktStatusID, ts.TktStatusName, pc.ParentCompanyName, art.ARTypeName }).ToList();
                        obj.Statuses = Statuses.Select(x => new GenericList { Key = x.TktStatusID, Value = x.TktStatusName + (string.IsNullOrEmpty(x.ARTypeName) ? "" : " - " + x.ARTypeName) + " (" + (x.ParentCompanyName ?? "None") + ")" }).ToList();
                    }
                    else if (areCustomTypeStatuses)
                    {

                        var Statuses = (from ts in _dbCon.tblTicketStatus
                                        join
                                        pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                        from pc in ps.DefaultIfEmpty()
                                        join art in _dbCon.tblActionRequestTypes on ts.ARTypeID equals art.ARTypeID into ps1
                                        from art in ps1.DefaultIfEmpty()

                                        where ts.IsActive == true && customerIds.Contains(ts.CompanyID) && typeIds.Contains(ts.ARTypeID)
                                        orderby ts.StatusIntValue
                                        select new { ts.TktStatusID, ts.TktStatusName, pc.ParentCompanyName, art.ARTypeName }).ToList();
                        obj.Statuses = Statuses.Select(x => new GenericList { Key = x.TktStatusID, Value = x.TktStatusName + (string.IsNullOrEmpty(x.ARTypeName) ? "" : " - " + x.ARTypeName) + " (" + (x.ParentCompanyName ?? "None") + ")" }).ToList();
                    }
                    else if (areCustomStatuses)
                    {

                        var Statuses = (from ts in _dbCon.tblTicketStatus
                                        join
                                        pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                        from pc in ps.DefaultIfEmpty()
                                        join art in _dbCon.tblActionRequestTypes on ts.ARTypeID equals art.ARTypeID into ps1
                                        from art in ps1.DefaultIfEmpty()
                                        where ts.IsActive == true && customerIds.Contains(ts.CompanyID)
                                        orderby ts.StatusIntValue
                                        select new { ts.TktStatusID, ts.TktStatusName, pc.ParentCompanyName, art.ARTypeName }).ToList();
                        obj.Statuses = Statuses.Select(x => new GenericList { Key = x.TktStatusID, Value = x.TktStatusName + (string.IsNullOrEmpty(x.ARTypeName) ? "" : " - " + x.ARTypeName) + " (" + (x.ParentCompanyName ?? "None") + ")" }).ToList();
                    }
                    else
                    {
                        var Statuses = (from ts in _dbCon.tblTicketStatus
                                        join
                                        pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                        from pc in ps.DefaultIfEmpty()
                                        join art in _dbCon.tblActionRequestTypes on ts.ARTypeID equals art.ARTypeID into ps1
                                        from art in ps1.DefaultIfEmpty()
                                        where ts.IsActive == true && customerIds.Contains(ts.CompanyID)
                                        orderby ts.StatusIntValue
                                        select new { ts.TktStatusID, ts.TktStatusName, pc.ParentCompanyName, art.ARTypeName }).ToList();
                        obj.Statuses = Statuses.Select(x => new GenericList { Key = x.TktStatusID, Value = x.TktStatusName + (string.IsNullOrEmpty(x.ARTypeName) ? "" : " - " + x.ARTypeName) + " (" + (x.ParentCompanyName ?? "None") + ")" }).ToList();
                    }

                    //Priorities
                    foreach (var item in customerIds)
                    {
                        var priorities = _dbCon.tblPriorities.Where(x => x.CompanyID == item).ToList();
                        includeGS = priorities.Count > 0 ? false : true;
                    }

                    if (includeGS)
                    {
                        var globalServeCompany = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == "GlobalServe").Select(x => x.ParentCompanyID).ToList();

                        obj.Priorities = (from p in _dbCon.tblPriorities
                                          join
                                          pc in _dbCon.tblParentCompanies on p.CompanyID equals pc.ParentCompanyID into ps
                                          from pc in ps.DefaultIfEmpty()
                                          where p.IsActive == true && (globalServeCompany.Contains(p.CompanyID) || customerIds.Contains(p.CompanyID))
                                          orderby p.PriorityIntValue
                                          select new GenericList { Key = p.PriorityID, Value = p.PriorityName }).ToList();

                    }
                    else
                    {
                        obj.Priorities = (from p in _dbCon.tblPriorities
                                          join
                                          pc in _dbCon.tblParentCompanies on p.CompanyID equals pc.ParentCompanyID into ps
                                          from pc in ps.DefaultIfEmpty()
                                          where p.IsActive == true && customerIds.Contains(p.CompanyID)
                                          orderby p.PriorityIntValue
                                          select new GenericList { Key = p.PriorityID, Value = p.PriorityName }).ToList();

                    }

                }
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>GetNewRequestFilterModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>GetNewRequestFilterModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetNewRequestFilterModel");
                }
                return new Model.NewRequestFilterModel ();
            }
        }

        /// <summary>
        /// this method is responsible for adding new request filter
        /// </summary>
        /// <param name="newmodel"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddRequestFilter(NewRequestFilterModel newmodel, int userId)
        {
            try
            {
                int count = 0;
                string filtername = "UNTITLED",
                    filterString = string.Empty;
                bool isPublic = false;

                if (!string.IsNullOrEmpty(newmodel.Name))
                {
                    filtername = newmodel.Name;
                }

                if (newmodel.FilterType == "public")
                {
                    isPublic = true;
                }

                if (!string.IsNullOrEmpty(newmodel.CompanyIds))
                {
                    filterString = filterString + " and ar.companyid in (" + newmodel.CompanyIds + ")";
                }

                if (newmodel.DateId > 0)
                {
                    filterString = filterString + "  and datediff(hour, t.createdate, getdate()) < " + newmodel.DateId;
                }

                if (!string.IsNullOrEmpty(newmodel.TypeIds))
                {
                    filterString = filterString + " and ar.artypeid in (" + newmodel.TypeIds + ")";
                }

                if (!string.IsNullOrEmpty(newmodel.StatusIds))
                {
                    filterString = filterString + " and t.tktstatusid in (" + newmodel.StatusIds + ")";
                }

                if (!string.IsNullOrEmpty(newmodel.PriorityIds))
                {
                    filterString = filterString + " and t.priorityid in (" + newmodel.PriorityIds + ")";
                }

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    tblRequestFilter rf = new tblRequestFilter();
                    rf.FilterName = filtername;
                    rf.IsPublic = isPublic;
                    rf.FilterString = filterString;
                    rf.OwnerID = userId;
                    rf.IsActive = true;
                    rf.LastUpdate = DateTime.Now;
                    rf.LastUpdateUserID = userId;
                    rf = _dbCon.tblRequestFilters.Add(rf);
                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>support request Presenter =>AddRequestFilter<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "support request Presenter =>AddRequestFilter");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddRequestFilter");
                }
                return 0;
            }
        }
    }
}

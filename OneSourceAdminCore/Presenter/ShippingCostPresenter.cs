﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class ShippingCostPresenter
    {
        private readonly globalserveEntities _dbCon;
        private readonly CustomerPresenter _customerpresenter;
        private readonly ADUploadPresenter _adupload;
        public ShippingCostPresenter()
        {
            _dbCon = new globalserveEntities();
            _customerpresenter = new CustomerPresenter();
            _adupload = new ADUploadPresenter();
        }
        /// <summary>
        /// get the customers
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetCustomers()
        {
            try
            {
                var Customers = _customerpresenter.GetCustomers();

                var customersList = Customers.Select(x => new SelectListItem
                {
                    Value = x.Key.ToString(),
                    Text = x.Value.ToString()
                }).ToList();
                return customersList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomers");
                }

                return new List<SelectListItem>();
            }




        }

        /// <summary>
        /// get the customer's suppliers
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        public List<SelectListItem> GetSuppliersByCustomer(int customerId)
        {
            try
            {
                var Suppliers = _dbCon.Sp_GetSuppliersByCustomer_AdminR(customerId).Select(x => new SelectListItem
                {
                    Value = x.SupplierId.ToString(),
                    Text = x.SupplierName
                }).ToList();


                return Suppliers;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSuppliersByCustomer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSuppliersByCustomer");
                }
                return new List<SelectListItem>();
            }


        }

        public List<SelectListItem> GetCatalogCategoryList(int CustomerID)
        {
            try
            {


                var getCategories = (from PC in _dbCon.tblPartCategories
                                     join CCP in _dbCon.tblCustomerCountryParts on PC.PartCategoryID equals CCP.PartCategoryID
                                     where CCP.CustomerID == CustomerID && CCP.IsActive == true && PC.IsActive == true
                                     select PC).Distinct().AsEnumerable();

                var data = getCategories.Select(x => new SelectListItem { Value = x.PartCategoryID.ToString(), Text = x.PartCategoryName }).Distinct().OrderBy(x => x.Text).ToList();

                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCatalogCategoryList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCatalogCategoryList");
                }

                return new List<SelectListItem>();
            }
        }

        /// <summary>
        /// get the active shipping cost list
        /// </summary>
        /// <returns></returns>
        public List<ShippingCostModel> GetShippingCostList()
        {
            try
            {
                var ShippingCostList = _dbCon.tblShippingCost_R.Where(x => x.IsActive == true).Select(x => new ShippingCostModel
                {
                    ShippingCostID = x.ShippingCostId,
                    CustomerID = x.CustomerId,
                    CategoryID = x.CategoryId,
                    MinQty = x.MinQty,
                    MaxQty = x.MaxQty,
                    ShippingCostInUSD = x.ShippingCostInUSD,
                    IsActive = x.IsActive,
                    LastUpdated = x.LastUpdated,
                    LastUpdateByUserId = x.LastUpdateByUserId
                }).ToList();
                return ShippingCostList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetShippingCostList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetShippingCostList");
                }

                return new List<ShippingCostModel>();
            }

        }

        /// <summary>
        ///  get the active shipping cost by ID
        /// </summary>
        /// <param name="ShippingCostID"></param>
        /// <returns></returns>
        public ShippingCostModel GetShippingCostByID(int ShippingCostID)
        {
            try
            {
                var model = _dbCon.tblShippingCost_R.Where(x => x.IsActive == true && x.ShippingCostId == ShippingCostID).Select(x => new ShippingCostModel
                {
                    ShippingCostID = x.ShippingCostId,
                    CustomerID = x.CustomerId,
                    CategoryID = x.CategoryId,
                    SupplierID = x.SupplierId,
                    MinQty = x.MinQty,
                    MaxQty = x.MaxQty,
                    ShippingCostInUSD = x.ShippingCostInUSD,
                    IsActive = x.IsActive,
                    LastUpdated = x.LastUpdated,
                    LastUpdateByUserId = x.LastUpdateByUserId
                }).FirstOrDefault();
                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetShippingCostByID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetShippingCostByID");
                }
                return null;
            }

        }

        public ResultModel AddandUpdateShippingCost(ShippingCostModel model, AdminSetting current)
        {
            try
            {
                if (model.ShippingCostID > 0)
                {
                    var check = _dbCon.tblShippingCost_R.Any(x => x.ShippingCostId != model.ShippingCostID && x.CustomerId == model.CustomerID && x.SupplierId == model.SupplierID && x.CategoryId == model.CategoryID && x.IsActive == true &&
                                                                  ((x.MinQty <= model.MinQty && x.MaxQty >= model.MinQty) || (x.MinQty <= model.MaxQty && x.MaxQty >= model.MaxQty) || (x.MinQty >= model.MinQty && x.MaxQty <= model.MaxQty)));
                    if (!check)
                    {
                        var entity = _dbCon.tblShippingCost_R.FirstOrDefault(x => x.ShippingCostId == model.ShippingCostID);
                        if (entity != null)
                        {
                            entity.CustomerId = model.CustomerID;
                            entity.SupplierId = model.SupplierID;
                            entity.CategoryId = model.CategoryID;
                            entity.MinQty = model.MinQty;
                            entity.MaxQty = model.MaxQty;
                            entity.IsActive = true;
                            entity.ShippingCostInUSD = model.ShippingCostInUSD;
                            entity.LastUpdated = DateTime.Now;
                            entity.LastUpdateByUserId = current.UserID;
                            _dbCon.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                            _dbCon.SaveChanges();
                            return new ResultModel { ID = model.ShippingCostID, Message = "Updated Successfully", Result = true };
                        }
                        else
                        {
                            return new ResultModel { ID = 0, Message = "No Data Found", Result = false };
                        }
                    }
                    else
                    {
                        return new ResultModel { ID = 0, Message = "Shipping cost already exist within this quantity range", Result = false };
                    }
                }
                else
                {
                    var check = _dbCon.tblShippingCost_R.Any(x => x.CustomerId == model.CustomerID && x.SupplierId == model.SupplierID && x.CategoryId == model.CategoryID && x.IsActive == true &&
                                                                  ((x.MinQty <= model.MinQty && x.MaxQty >= model.MinQty) || (x.MinQty <= model.MaxQty && x.MaxQty >= model.MaxQty) || (x.MinQty >= model.MinQty && x.MaxQty <= model.MaxQty)));

                    if (!check)
                    {
                        var entity = new tblShippingCost_R();
                        entity.CustomerId = model.CustomerID;
                        entity.SupplierId = model.SupplierID;
                        entity.CategoryId = model.CategoryID;
                        entity.MinQty = model.MinQty;
                        entity.MaxQty = model.MaxQty;
                        entity.IsActive = true;
                        entity.ShippingCostInUSD = model.ShippingCostInUSD;
                        entity.LastUpdated = DateTime.Now;
                        entity.LastUpdateByUserId = current.UserID;
                        _dbCon.tblShippingCost_R.Add(entity);
                        _dbCon.SaveChanges();
                        return new ResultModel { ID = entity.ShippingCostId, Message = "Added Successfully", Result = true };
                    }
                    else
                    {
                        return new ResultModel { ID = 0, Message = "Shipping cost already exist within this quantity range", Result = false };
                    }
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddandUpdateShippingCost");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddandUpdateShippingCost");
                }

                return new ResultModel { ID = 0, Message = ex.Message, Result = false };

            }
        }

        public ResultModel DeleteShippingCost(int ShippingCostID, AdminSetting current)
        {
            try
            {
                if (ShippingCostID > 0)
                {
                    var entity = _dbCon.tblShippingCost_R.FirstOrDefault(x => x.ShippingCostId == ShippingCostID);
                    if (entity != null)
                    {
                        _dbCon.tblShippingCost_R.Remove(entity);
                        _dbCon.SaveChanges();
                        return new ResultModel { ID = ShippingCostID, Message = "Deleted Successfully", Result = true };
                    }
                    else
                    {
                        return new ResultModel { ID = ShippingCostID, Message = "No Data Found", Result = true };
                    }

                }
                return new ResultModel { ID = ShippingCostID, Message = "Something Went Wrong", Result = false };
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "DeleteShippingCost");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteShippingCost");
                }

                return new ResultModel { ID = 0, Message = ex.Message, Result = false };
            }
        }

        public ShippingCostModel GetShippingCost(int customerId, int supplierId, int categoryId)
        {
            try
            {
                var model = _dbCon.tblShippingCost_R.Where(x => x.IsActive == true && x.SupplierId == supplierId && x.CustomerId == customerId && x.CategoryId == categoryId).Select(x => new ShippingCostModel
                {
                    ShippingCostID = x.ShippingCostId,
                    CustomerID = x.CustomerId,
                    CategoryID = x.CategoryId,
                    MinQty = x.MinQty,
                    MaxQty = x.MaxQty,
                    ShippingCostInUSD = x.ShippingCostInUSD,
                    IsActive = x.IsActive,
                    LastUpdated = x.LastUpdated,
                    LastUpdateByUserId = x.LastUpdateByUserId
                }).FirstOrDefault();
                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetShippingCost");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetShippingCost");
                }

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="supplierId"></param>
        /// <param name="categoryId"></param>
        /// <param name="shippingcostId"></param>
        /// <returns></returns>
        public ShippingCostModel GetShippingCostModel(int customerId = 0, int supplierId = 0, int categoryId = 0, int shippingcostId = 0)
        {
            try
            {


                ShippingCostModel model = new ShippingCostModel();
                model.CustomerID = customerId;
                model.SupplierID =  supplierId;
                model.CategoryID = categoryId;
                model.ShippingCostID = shippingcostId;
                model.ListCustomer = GetCustomers();
                if (customerId > 0)
                {
                    model.ListSupplier = GetSuppliersByCustomer(customerId);
                    model.ListPartCategory = GetCatalogCategoryList(customerId);
                }
                if (shippingcostId > 0)
                {
                    var shippingcostModel = GetShippingCostByID(shippingcostId);
                    if (shippingcostModel != null)
                    {
                        model.ShippingCostID = shippingcostModel.ShippingCostID;
                        model.CustomerID = shippingcostModel.CustomerID;
                        model.CategoryID = shippingcostModel.CategoryID;
                        model.SupplierID = shippingcostModel.SupplierID;
                        model.MinQty = shippingcostModel.MinQty;
                        model.MaxQty = shippingcostModel.MaxQty;
                        model.ShippingCostInUSD = shippingcostModel.ShippingCostInUSD;
                        model.IsActive = shippingcostModel.IsActive;
                        model.LastUpdated = shippingcostModel.LastUpdated;
                        model.LastUpdateByUserId = shippingcostModel.LastUpdateByUserId;

                        model.ListSupplier = GetSuppliersByCustomer(shippingcostModel.CustomerID);
                        model.ListPartCategory = GetCatalogCategoryList(shippingcostModel.CustomerID);
                        model.ShippingCostList = _dbCon.sp_getShippingCostInfo_R(shippingcostModel.CustomerID.ToString(), shippingcostModel.SupplierID.ToString(), shippingcostModel.CategoryID.ToString()).ToList();
                    }
                }
                if (customerId > 0 &&  supplierId >0  && categoryId > 0)
                {
                    //var shippingcostModel = GetShippingCost(customerId, supplierId, categoryId);
                    //if (shippingcostModel != null)
                    //{
                    //    model.ShippingCostID = shippingcostModel.ShippingCostID;
                    //    model.CustomerID = shippingcostModel.CustomerID;
                    //    model.CategoryID = shippingcostModel.CategoryID;
                    //    model.MinQty = shippingcostModel.MinQty;
                    //    model.MaxQty = shippingcostModel.MaxQty;
                    //    model.ShippingCostInUSD = shippingcostModel.ShippingCostInUSD;
                    //    model.IsActive = shippingcostModel.IsActive;
                    //    model.LastUpdated = shippingcostModel.LastUpdated;
                    //    model.LastUpdateByUserId = shippingcostModel.LastUpdateByUserId;
                    //}

                }
                if (customerId > 0 || supplierId >0 || categoryId > 0)
                {

                    model.ShippingCostList = _dbCon.sp_getShippingCostInfo_R(customerId.ToString(), supplierId.ToString(), categoryId.ToString()).ToList();
                }
                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetShippingCostModel");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetShippingCostModel");
                }
                return null;
            }

        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class ReportMenuPresenter
    {
        private globalserveEntities dbCon;
        public ReportMenuPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public List<tblReportsMenu> GetReportMenuList()
        {
            return dbCon.tblReportsMenus.ToList();
        }
        public List<tblAdminMenu> GetAdminMenuList()
        {
            return dbCon.tblAdminMenus.ToList();
        }
        public tblReportsMenu GetMenuById(int id)
        {
            return dbCon.tblReportsMenus.FirstOrDefault(x => x.ReportMenuId == id);
        }
        public tblAdminMenu GetAdminMenuById(int id)
        {
            return dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == id);
        }

        public bool AddNewMenu(tblAdminMenu menu, AdminSetting currentSetting)
        {
            try
            {
                menu.LastUpdated = DateTime.Now;
                menu.UpdatedBy = currentSetting.UserID;
                menu.IsActive = true;
                dbCon.tblAdminMenus.Add(menu);
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewMenu");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewMenu");
                }
            }
            return false;
        }

        public bool EditMenu(tblAdminMenu menu, AdminSetting currentSetting)
        {
            try
            {
                menu.LastUpdated = DateTime.Now;
                menu.UpdatedBy = currentSetting.UserID;
                dbCon.Entry(menu).State = System.Data.Entity.EntityState.Modified;
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "EditMenu");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "EditMenu");
                }
            }
            return false;
        }


        public List<GenericList> GetUserGroupList()
        {
            var data = dbCon.tblUserGroups.Select(x => new GenericList()
            {

                Key = x.UserGroupID,
                Value = x.UserGroupName.Trim() + " (" + x.Description.Trim() + ")"
            }).ToList();

            return data;
        }

        public List<GenericList> GetUserGroupListByGroupName(string name)
        {
            var data = dbCon.tblUserGroups.Where(x => x.UserGroupName.ToLower() == name).Select(x => new GenericList()
            {

                Key = x.UserGroupID,
                Value = x.UserGroupName.Trim() + " (" + x.Description.Trim() + ")"
            }).ToList();

            return data;
        }

        public List<ReportRoleMenuMappingModel> GetRoleMenuMappingByRoleId(int roleId)
        {
            try
            {
                var getMenulist = dbCon.tblReportsMenus.Where(x => x.IsActive).ToList();

                var getExistingRoleMenuMapping = dbCon.tblReportMenuRoleMappings.Where(x => x.UserGroupId == roleId).ToList();


                var existingMenuIds = getExistingRoleMenuMapping.Select(x => x.ReportMenuId).ToList();


                var menuList = new List<ReportRoleMenuMappingModel>();

                foreach (var item in getMenulist.Where(x => !existingMenuIds.Contains(x.ReportMenuId)))
                {
                    menuList.Add(new ReportRoleMenuMappingModel()
                    {
                        MenuId = item.ReportMenuId,
                        MenuName = item.ReportMenuName.Trim(),
                        UserGroupId = roleId,
                        UserGroupName = GetUserGroupNameWithDescById(roleId)
                    });
                }

                foreach (var item in getExistingRoleMenuMapping)
                {
                    menuList.Add(new ReportRoleMenuMappingModel()
                    {
                        MenuId = item.ReportMenuId,
                        MenuName = GetMenuById(item.ReportMenuId).ReportMenuName,
                        UserGroupId = roleId,
                        UserGroupName = GetUserGroupNameWithDescById(roleId)
                    });
                }
                return menuList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRoleMenuMappingByRoleId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRoleMenuMappingByRoleId");
                }
                return null;
            }
        }


        public string GetUserGroupNameWithDescById(int userGroupId)
        {
            var firstOrDefault = dbCon.tblUserGroups.FirstOrDefault(x => x.UserGroupID == userGroupId);
            var name = "";
            var desc = "";
            if (firstOrDefault != null)
            {
                name = firstOrDefault.UserGroupName ?? "";
                desc = firstOrDefault.Description ?? "";
            }
            return name + " (" + desc + ")";
        }

        public bool AddUpdateRoleMenuSettings(int menuId, int roleId, string accessType, bool accessValue, int currentUserId)
        {
            try
            {

                var chkExisting =
                    dbCon.tblMenuAndRoleMappings.FirstOrDefault(x => x.MenuId == menuId && x.UserGroupId == roleId);
                if (chkExisting != null)
                {
                    chkExisting.LastUpdated = DateTime.Now;
                    chkExisting.LastUpdatedBy = currentUserId;
                    if (accessType == "viewAccessChk")
                    {
                        chkExisting.ViewOnlyAccess = accessValue;
                        if (!accessValue)
                        {
                            chkExisting.AddEditAccess = false;
                        }
                    }
                    if (accessType == "AddEditAccess")
                    {
                        chkExisting.AddEditAccess = accessValue;
                        if (accessValue)
                        {
                            chkExisting.ViewOnlyAccess = true;
                        }
                    }
                    dbCon.Entry(chkExisting).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();
                }
                else
                {

                    var addNewRoleMenu = new tblMenuAndRoleMapping
                    {
                        LastUpdated = DateTime.Now,
                        LastUpdatedBy = currentUserId,
                        MenuId = menuId,
                        UserGroupId = roleId
                    };
                    if (accessType == "viewAccessChk")
                    {
                        addNewRoleMenu.ViewOnlyAccess = accessValue;
                    }
                    if (accessType == "AddEditAccess")
                    {
                        addNewRoleMenu.AddEditAccess = accessValue;
                        if (accessValue)
                        {
                            addNewRoleMenu.ViewOnlyAccess = true;
                        }
                    }

                    dbCon.tblMenuAndRoleMappings.Add(addNewRoleMenu);
                    dbCon.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateRoleMenuSettings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateRoleMenuSettings");
                }
                return false;
            }
        }

        public List<GenericStringList> GetGroupTypes()
        {
            var typeList = dbCon.tblUserGroups.Where(x => x.UserGroupName.ToLower() != "customer" && x.UserGroupName.ToLower() != "finance").Select(x => new GenericStringList()
            {
                Key = x.UserGroupName.ToLower(),
                Value = x.UserGroupName.Trim()

            }).Distinct().ToList();
            return typeList;
        }

        public List<GenericList> GetEntityTypeListByTypeName(string typeName)
        {
            var list = new List<GenericList>();
            if (typeName.ToLower() == "sfa")
            {

                list = dbCon.tblSFAs.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.SFAID,
                    Value = x.SFAName.Trim()
                }).ToList();
                return list;
            }

            if (typeName.ToLower() == "manufacturer")
            {

                list = dbCon.tblManufacturers.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.ManufacturerID,
                    Value = x.ManufacturerName.Trim()
                }).ToList();
                return list;
            }

            if (typeName.ToLower() == "isd")
            {

                list = dbCon.tblPrimeContractors.Where(x => x.IsActive == 1).Select(x => new GenericList()
                {
                    Key = x.PrimeContractorID,
                    Value = x.PrimeContractorName.Trim()
                }).ToList();
                return list;
            }

            if (typeName.ToLower() == "subisd")
            {

                list =
                    dbCon.tblPrimeContractors.Where(x => x.IsActive == 1 && x.IsSubAdmin).Select(x => new GenericList()
                    {
                        Key = x.PrimeContractorID,
                        Value = x.PrimeContractorName.Trim()
                    }).ToList();
                return list;
            }


            return list;

        }

        public List<GenericList> GetUsersByRoleId(int grpId)
        {
            var data = dbCon.tblUsers.Where(x => x.IsActive == 1 && x.UserGroupID == grpId).Select(x => new GenericList()
            {
                Key = x.UserID,
                Value = x.UserName.Trim()

            }).ToList();

            return data;
        }


        public ReportMenuModel GetReportsMenuByUserGroupAndParentMenuId(int userGroupId,int parentMenuId)
        {
            var model = new ReportMenuModel();

            var menuIdsOfGroup =
                    dbCon.tblReportMenuRoleMappings.Where(
                        x => x.UserGroupId == userGroupId).ToList();

            var menuIds = menuIdsOfGroup.Select(x => x.ReportMenuId);

            var menuList = dbCon.tblReportsMenus.Where(x => x.ParentMenuId == parentMenuId && menuIds.Contains(x.ReportMenuId)).ToList();

            model.ReportMenuList = menuList;

            return model;
        }

        public string GetParentMenuName(int parentMenuId)
        {
            var name = "";

            var menu =
                    dbCon.tblAdminMenus.FirstOrDefault(
                        x => x.MenuId == parentMenuId);
            if (menu != null)
            {
                name = menu.MenuName;
            }
            return name;
        }

    }
}

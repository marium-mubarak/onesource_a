﻿using OneSourceAdmin.Models;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : July 12 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This presenter Class is responsible for providing asset tracking detail"
    /// </summary>
    public class AssetSearchPresenter
    {
        //AssetSearchModel
        private readonly globalserveEntities _dbCon;
        public AssetSearchPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// this method use for marking asset attachment Isactive false
        /// </summary>
        /// <returns></returns>
        public int MarkIsActiveAssetAttachment(int daid, int userid)//Adnan 19-june-2019
        {
            int count = 0;
            try
            {
                using (var transcation = _dbCon.Database.BeginTransaction())
                {

                    var tblAssetAttachments = _dbCon.tblAssetAttachments.FirstOrDefault(x => x.DAID == daid);
                    if (tblAssetAttachments != null)
                    {
                        tblAssetAttachments.IsActive = false;
                        tblAssetAttachments.LastUpdate = DateTime.Now;
                        tblAssetAttachments.LastUpdateUserID = userid;
                        count = _dbCon.SaveChanges();
                    }


                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter => MarkIsActiveTicketAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter => MarkIsActiveTicketAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteAttachment");
                }
            }
            return count;
        }
        /// <summary>
        /// This method is responsible for providing asset search view
        /// </summary>
        /// <param name="userGroupName"></param>
        /// <param name="customerId"></param>
        /// <param name="SFAID"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public AssetSearchModel GetAssetSearchModel(string userGroupName, int customerId, string SFAID, string userId)
        {
            try
            {
                string strCustomerId = customerId == 0 ? "" : customerId.ToString();
                AssetSearchModel obj = new AssetSearchModel();
                obj.Categories = _dbCon.sp_getCategoryAssetSearch_AdminR(userGroupName, strCustomerId, SFAID).ToList();
                obj.Customers = _dbCon.sp_getCustomerAssetSearch_AdminR(userGroupName, strCustomerId, SFAID).ToList();
                obj.Customers.Insert(0, new sp_getCustomerAssetSearch_AdminR_Result() { parentcompanyid = 0, parentcompanyname = "(None)" });
                obj.Locations = _dbCon.sp_getLocationAssetSearch_AdminR(userGroupName, strCustomerId, SFAID).ToList();
                obj.Locations.Insert(0, new sp_getLocationAssetSearch_AdminR_Result() { customeraddressid = 0, customeraddressname = "(None)" });
                obj.Manufacturers = _dbCon.sp_getManufacturerAssetSearch_AdminR(userGroupName, strCustomerId, SFAID).ToList();
                obj.Statuses = GetSatuses(customerId);
                obj.Statuses.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>asset Search Presenter => GetAssetSearchModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "asset Search Presenter => GetAssetSearchModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetSearchModel");
                }
                return new AssetSearchModel();
            }
        }

        /// <summary>
        /// This method is responsible for providing asset statuses
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<GenericList> GetSatuses(int customerId)//customerId is basically companyid
        {
            bool includeGS = true;
            var assetStatus = _dbCon.GSNetP_tbl_AssetStatus.Where(x => x.CompanyID == customerId && x.IsActive == true).ToList();
            includeGS = assetStatus.Count > 0 ? false : true;
            if (includeGS)
            {
                var data = (from a in _dbCon.GSNetP_tbl_AssetStatus
                            join b in _dbCon.tblParentCompanies on a.CompanyID equals b.ParentCompanyID into ps
                            from b in ps.DefaultIfEmpty()
                            where a.IsActive == true && (a.CompanyID == customerId || a.CompanyID == null)
                            orderby b.ParentCompanyName, a.StatusIntValue
                            select new GenericList { Key = a.AssetStatusID, Value = a.Status }).ToList();
                //data.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                return data;
            }
            else
            {
                var data = (from a in _dbCon.GSNetP_tbl_AssetStatus
                            join b in _dbCon.tblParentCompanies on a.CompanyID equals b.ParentCompanyID into ps
                            from b in ps.DefaultIfEmpty()
                            where a.IsActive == true && a.CompanyID == customerId
                            orderby b.ParentCompanyName, a.StatusIntValue
                            select new GenericList { Key = a.AssetStatusID, Value = a.Status }).ToList();
                //data.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                return data;
            }

        }

        /// <summary>
        ///  This method is responsible for asset Search request
        /// </summary>
        /// <param name="model"></param>
        /// <param name="customerId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="sfaid"></param>
        /// <param name="userId"></param>
        /// <param name="requiredRecord"></param>
        /// <returns></returns>
        public AssetListModel GetSearchedAssets(AssetSearchModel model, string userGroupName, string sfaid, string userId, int requiredRecord)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                AssetListModel oslmodel = new Model.AssetListModel();
                ObjectParameter totalCount = new ObjectParameter("TotalCount", typeof(Int32)); //here customerid is basically parentcompanyid
                List<sp_getAssetSearch_AdminR_Result> obj = _dbCon.sp_getAssetSearch_AdminR(sfaid, model.CustomerId == 0 ? "" : model.CustomerId.ToString(), userId, model.SerialNumber, model.AssetTag
                    , model.LocationId == 0 ? "" : model.LocationId.ToString(), model.StrManufacturers, model.PartNumber, model.StrCategories, model.StatusId == 0 ? "" : model.StatusId.ToString(), userGroupName, requiredRecord.ToString(), totalCount).ToList();
                oslmodel.LstAsset = obj;
                if (obj.Count == 0)
                {
                    oslmodel.TotalRecord = 0;
                }
                else { oslmodel.TotalRecord = Convert.ToInt32(totalCount.Value); }
                return oslmodel;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter =>GetSearchedAssets<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter =>GetSearchedAssets");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Asset Search Presenter =>GetSearchedAssets");
                }
                return new AssetListModel();
            }

        }

        /// <summary>
        /// This method is responsible for getting asset detail
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userGroupName"></param>
        /// <returns></returns>
        public AssetDetailModel GetAssetDetail(int assetId, string userGroupName, int sfaId)
        {
            try
            {
                AssetDetailModel obj = new Model.AssetDetailModel();
                obj.AssetId = assetId;

                obj.AssetDetail = _dbCon.sp_getAssetDetail_AdminR(assetId, sfaId, userGroupName).FirstOrDefault();

                if (obj.AssetDetail != null)
                {

                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.ManufacturerID, Value = x.ManufacturerName }).Distinct().OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                    //partcategories
                    var parentCompanyIds = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == "GlobalServe").Select(x => (int?)x.ParentCompanyID).ToList();
                    parentCompanyIds.Add(obj.AssetDetail.parentcompanyid);

                    obj.Categories = _dbCon.tblPartCategories.Where(x => x.IsActive == true && parentCompanyIds.Contains(x.CompanyID)).Distinct().OrderBy(x => x.PartCategoryName).Select(x => new
                    GenericList
                    {
                        Key = x.PartCategoryID,
                        Value = x.PartCategoryName
                    }).ToList();

                    obj.Categories.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                    //

                    obj.Statuses = GetSatuses(Convert.ToInt32(obj.AssetDetail.parentcompanyid));

                    //location
                    var Customers = _dbCon.tblCustomers.Where(x => x.ParentCompanyID == obj.AssetDetail.parentcompanyid).Select(x => x.CustomerID).Distinct().ToList();
                    if (Customers.Count > 0)
                    {
                        var locations = (from ca in _dbCon.tblCustomerAddresses
                                         join
                                         co in _dbCon.tblCountries on ca.CountryID equals co.CountryID
                                         where Customers.Contains(ca.CustomerID) && ca.IsActive == 1 && co.IsActive == true
                                         orderby ca.CustomerAddressName
                                         select new { ca.CustomerAddressID, co.CountryName, ca.CondensedAddress, ca.Address1 }).ToList();
                        obj.Locations = locations.Select(x => new GenericList { Key = x.CustomerAddressID, Value = x.CountryName + " - " + x.CondensedAddress + " " + x.Address1 }).OrderBy(X => X.Value).ToList();

                    }
                    //
                    obj.SupportedByList = _dbCon.tblSFAs.Where(x => x.IsComposite == true && x.ParentCompanyID != null && x.IsActive == true).Select(x => new GenericList { Key = x.SFAID, Value = x.SFAName }).Distinct().OrderBy(x => x.Value).ToList();
                    obj.SupportedByList.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                    if (userGroupName == "ISD")
                    {
                        var ownerlist = _dbCon.tblContacts.Where(x => x.CompanyID == obj.AssetDetail.parentcompanyid && x.IsActive == true && (x.FirstName != null || x.LastName != null)).ToList();
                        if (ownerlist.Count > 0)
                        {
                            obj.CurrentOwnerList = ownerlist.Select(x => new GenericList { Key = x.ContactID, Value = ((x.FirstName ?? "") + " " + (x.LastName ?? "")) }).OrderBy(x => x.Value).ToList();
                            obj.CurrentOwnerList.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                        }
                    }
                    obj.WarrantyList = _dbCon.tblWarrantyTypes.Where(x => x.IsActive == true).
                        Select(x => new GenericList { Key = x.WarrantyTypeID, Value = x.WarrantyTypeName }).Distinct().OrderBy(x => x.Value).ToList();
                    obj.WarrantyList.Insert(0, new GenericList { Key = 0, Value = "(None)" });

                    //for asset image
                    var imageQuery = "select isnull(imgid, '') as Image from cds..cds_prod (NOLOCK) where prodid='" + obj.AssetDetail.mfgpartid + "'";
                    var image = _dbCon.Database.SqlQuery<string>(imageQuery).FirstOrDefault();
                    if (!string.IsNullOrEmpty(image))
                    {
                        obj.ImageUrl = image + ".jpg";
                    }
                    else
                    {
                        if (obj.AssetDetail.customerpartcategoryid != null)
                        {
                            imageQuery = "select isnull(defaultimage, '') as imagename from tblpartcategories where partcategoryid=" + obj.AssetDetail.customerpartcategoryid;
                            image = _dbCon.Database.SqlQuery<string>(imageQuery).FirstOrDefault();
                            obj.ImageUrl = image;
                            if (string.IsNullOrEmpty(image))
                            {
                                imageQuery = "select isnull(defaultimage, '') as imagename from tblpartcategories where partcategoryname='Desktops' and isactive=1";
                                image = _dbCon.Database.SqlQuery<string>(imageQuery).FirstOrDefault();
                                obj.ImageUrl = image;
                            }
                        }
                    }
                    //
                    //Parts / Peripherals               

                    obj.PartsPeripherals = (from a in _dbCon.GSNetP_tbl_Assets
                                            join
                                            ao in _dbCon.GSNetP_tbl_AssetsOwned on a.AssetsOwnedID equals ao.AssetsOwnedID into ps
                                            from ao in ps.DefaultIfEmpty()
                                            join
                                            pc in _dbCon.tblPartCategories on ao.CustomerPartCategoryID equals pc.PartCategoryID into ps1
                                            from pc in ps1.DefaultIfEmpty()
                                            where (a.ParentAssetID == obj.AssetId || a.ParentAssetID == obj.AssetDetail.parentassetid || a.AssetID == obj.AssetDetail.parentassetid)
                                            && a.AssetID != obj.AssetId && ao.AssetType != "S"
                                            orderby a.AssetID
                                            select new AssetPartsPeripherals
                                            {
                                                Category = pc.PartCategoryName,
                                                Manufacturer = ao.Manufacturer,
                                                Model = ao.Model,
                                                Description = ao.Description,
                                                AssetId = a.AssetID,
                                                Serial = a.SerialNum,
                                                AssetTagNum = a.AssetTagNum
                                            }).ToList();
                    //

                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter =>GetAssetDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter =>GetAssetDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Asset Search Presenter =>GetAssetDetail");
                }
                return new AssetDetailModel();
            }
        }

        /// <summary>
        /// This method is responsible for updating asset
        /// </summary>
        /// <param name="detailModel"></param>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public GenericList UpdateAsset(sp_getAssetDetail_AdminR_Result detailModel, int assetId, int userId)
        {
            try
            {
                int count = 0;
                if (assetId > 0)
                {
                    if (!string.IsNullOrEmpty(detailModel.serialnum))
                    {
                        var Customer = (from a in _dbCon.GSNetP_tbl_Assets
                                        join
                                          ao in _dbCon.GSNetP_tbl_AssetsOwned on a.AssetsOwnedID equals ao.AssetsOwnedID
                                        where a.AssetID == assetId
                                        select new { ao.CustomerID }).FirstOrDefault();
                        if (Customer != null)
                        {
                            var acount = (from a in _dbCon.GSNetP_tbl_Assets
                                          join
                                            ao in _dbCon.GSNetP_tbl_AssetsOwned on a.AssetsOwnedID equals ao.AssetsOwnedID
                                          where a.AssetID != assetId && ao.CustomerID == Customer.CustomerID && a.SerialNum.ToUpper() == detailModel.serialnum
                                          select new { a.AssetID }).Count();
                            if (acount > 0)
                            {
                                return new GenericList { Key = -1, Value = "An asset with serial number: " + detailModel.serialnum + " already exists." };
                            }

                        }
                    }
                    if (detailModel.ticketid != null)
                    {
                        var ticketCount = _dbCon.tblTickets.Where(x => x.TicketID == detailModel.ticketid).Count();
                        if (ticketCount == 0)
                        {
                            return new GenericList { Key = -2, Value = "Invalid Ticket#" };
                        }
                    }

                    count = _dbCon.sp_UpdateAsset_AdminR(assetId, (detailModel.assetstatusid == 0 ? null : detailModel.assetstatusid), detailModel.customeraddressid, detailModel.description, (detailModel.manufacturerid == 0 ? null : detailModel.manufacturerid),
                        (detailModel.customerpartcategoryid == 0 ? null : detailModel.customerpartcategoryid), detailModel.model, (detailModel.currentownerid == 0 ? null : detailModel.currentownerid), detailModel.assettagnum, detailModel.serialnum, null,
                        (detailModel.supportedbysfaid == 0 ? null : (int?)detailModel.supportedbysfaid), null, null, null, null, null, null, null, detailModel.costcenter, null, string.Empty, detailModel.warrantystartdate,
                        detailModel.warrantyenddate, null, null, null, null, null, detailModel.ticketid, null, userId, detailModel.SKU, detailModel.modelnumber, detailModel.systemname
                        , null, detailModel.leaseStartDate, detailModel.leaseEndDate, detailModel.region, detailModel.OldAssetName, detailModel.OldAssetTag, detailModel.OldLeasedorPurchased
                        , detailModel.NewLeasedorPurchased, detailModel.Imaging, detailModel.WhiteGloveDelivery, detailModel.AssetRentalAmount, detailModel.OldAssetModel,
                        detailModel.OldAssetModelNumber, detailModel.UserLoginName, detailModel.UserEmailAddress, detailModel.UserFirstName, detailModel.UserLastName,
                        detailModel.UserPhoneNumber, detailModel.ScheduledReturnDate, detailModel.InstallDate, null, null, null, null, null, null);

                    _dbCon.usp_AddWarranty_R(assetId, detailModel.warrantytypeid, detailModel.warrantystartdate, detailModel.warrantyenddate, userId);

                    return new GenericList { Key = count, Value = "" };
                }
                return new GenericList();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter =>UpdateAsset<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter =>UpdateAsset");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Asset Search Presenter =>UpdateAsset");
                }
                return new GenericList();
            }

        }

        /// <summary>
        /// This method is responsible for getting asset history
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public List<AssetHistory> GetAssetHistory(int assetId)
        {
            try
            {
                var data = (from at in _dbCon.tblAssetTrackings
                            join
                            a in _dbCon.tblActions on at.ActionID equals a.ActionID into ps
                            from a in ps.DefaultIfEmpty()
                            join ast in _dbCon.GSNetP_tbl_AssetStatus on at.AssetStatusID equals ast.AssetStatusID into ps1
                            from ast in ps1.DefaultIfEmpty()
                            join u in _dbCon.tblUsers on at.LastUpdateUserID equals u.UserID into ps2
                            from u in ps2.DefaultIfEmpty()
                            where at.AssetID == assetId
                            select new AssetHistory
                            {
                                Comments = at.Comment,
                                Logdate = at.LogDate,
                                UserName = u.UserName,
                                ActionName = a.ActionName,
                                Status = ast.Status,
                                IsPublic = at.IsPublic,
                                AssetLogID = at.AssetLogID
                            }).OrderByDescending(x => x.Logdate).ToList();
                return data;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter => GetAssetHistory<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter => GetAssetHistory");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetHistory");
                }
                return new List<Model.AssetHistory>();
            }
        }

        /// <summary>
        /// this method is responsible for updating asset log
        /// </summary>
        /// <param name="assetLogID"></param>
        /// <param name="userId"></param>
        /// <param name="isPublic"></param>
        /// <returns></returns>
        public int UpdateAssetLog(int assetLogID, int userId, bool isPublic)
        {
            try
            {
                return _dbCon.sp_ChangeAssetLogVisible(assetLogID, isPublic, userId);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter => UpdateAssetLog<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter => UpdateAssetLog");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateAssetLog");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for updating asset comments
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public int UpdateAssetComment(int assetId, int userId, string comments)
        {
            try
            {
                int count = 0;
                var asset = _dbCon.GSNetP_tbl_Assets.FirstOrDefault(x => x.AssetID == assetId);
                if (asset != null)
                {
                    int? actionId = null;
                    var actions = _dbCon.tblActions.Where(x => x.IsActive == true).ToList();
                    foreach (var item in actions)
                    {
                        if (item.ActionName.ToUpper() == "ADD A COMMENT TO A TICKET")
                        {
                            actionId = item.ActionID;
                        }
                    }


                    tblAssetTracking obj = new tblAssetTracking();
                    obj.AssetID = assetId;
                    obj.AssetStatusID = Convert.ToInt32(asset.AssetStatusID);
                    obj.ActionID = actionId;
                    obj.Comment = comments;
                    obj.IsActive = true;
                    obj.IsPublic = true;
                    obj.LastUpdateUserID = userId;
                    obj.LogDate = DateTime.Now;
                    obj.LastUpdateUserID = userId;

                    _dbCon.tblAssetTrackings.Add(obj);
                    count = _dbCon.SaveChanges();
                }


                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>asset Search Presenter => UpdateAssetComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "asset Search Presenter => UpdateAssetComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateAssetComment");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting asset attachment
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public List<sp_GetAttachments_R_Result> GetAssetAttachment(int assetId)// Adnan 19-06-2019
        {
            try
            {
                var assetAttachments = _dbCon.tblAssetAttachments.Where(x => x.AssetID == assetId && x.IsActive == true).Select(x => x.DAID).ToList();
                if (assetAttachments != null)
                {
                    var data = (from a in _dbCon.tblDigitalAssets
                                join
                                  b in _dbCon.tblMimeTypes on a.DAMimeTypeID equals b.MimeTypeID into ps
                                from b in ps.DefaultIfEmpty()
                                where assetAttachments.Contains(a.DAID)
                                select new { daid = a.DAID, daname = a.DAName, docid = a.DocID, daurl = a.DAURL, mimetypeicon = b.MimeTypeIcon }).ToList();
                    List<sp_GetAttachments_R_Result> lst = new List<sp_GetAttachments_R_Result>();
                    sp_GetAttachments_R_Result att;
                    foreach (var item in data)
                    {
                        att = new sp_GetAttachments_R_Result();
                        att.daid = item.daid;
                        att.daname = item.daname;
                        att.docid = item.docid;
                        att.daurl = item.daurl;
                        att.mimetypeicon = item.mimetypeicon ?? "blank.gif";
                        lst.Add(att);
                    }
                    return lst;
                }
                return new List<sp_GetAttachments_R_Result>();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter => GetAssetAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter => GetAssetAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetAttachment");
                }
                return new List<sp_GetAttachments_R_Result>();
            }
        }

        /// <summary>
        ///  this method is responsible for adding digital asset for asset detail
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url, int userId, int assetId)
        {
            try
            {
                return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                      assetId, null, null, null, null, null, null, null, null, null
                      , null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>asset Search Presenter => AddDigitalAsset<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "asset Search Presenter => AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        public string getMfgNameById(int mfgId)
        {
            var mfg = _dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == mfgId);
            if (mfg != null)
            {
                return mfg.ManufacturerName;
            }
            return string.Empty;
        }
        public string getCategoryNameById(int catId)
        {
            var category = _dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == catId);
            if (category != null)
            {
                return category.PartCategoryName;
            }
            return string.Empty;
        }
        public string getLocationNameById(int locId)
        {
            var category = _dbCon.tblCustomerAddresses.FirstOrDefault(x => x.CustomerAddressID == locId);
            if (category != null)
            {
                return category.CustomerAddressName;
            }
            return string.Empty;
        }
        public bool CheckTicket(int ticketId)
        {
            if (ticketId > 0)
            {
                var ticketCount = _dbCon.tblTickets.Where(x => x.TicketID == ticketId).Count();
                if (ticketCount == 0)
                {
                    return false;
                }
                return true;
            }
            return false;
        }


        //sunny gohar on 17th Jan 2018 ============= ASSET BULK UPLOAD =======================================

        public int GetManufacturerIdByName(string Name)
        {
            var mf = _dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerName == Name && x.IsActive == true);
            if (mf != null)
            {
                return mf.ManufacturerID;
            }
            return 0;
        }
        public int GetCategoryIdByName(string Name)
        {
            var c = _dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryName == Name && x.IsActive == true);
            if (c != null)
            {
                return c.PartCategoryID;
            }
            return 0;
        }

        public int GetCustomerIdBySerialNumber(string SerialNumber)
        {
            var cust = (from a in _dbCon.GSNetP_tbl_Assets
                        join ao in _dbCon.GSNetP_tbl_AssetsOwned on a.AssetsOwnedID equals ao.AssetsOwnedID
                        where a.SerialNum == SerialNumber
                        select new
                        {
                            CustomerId = ao.CustomerID,
                        }).FirstOrDefault();
            if (cust != null)
            {

                if (cust.CustomerId > 0)
                {
                    return cust.CustomerId;
                }

            }
            return 0;
        }

        public int GetAssetIdBySerialNumberAndCustomerId(string SerialNumber, int CustomerId)
        {
            var asset = (from a in _dbCon.GSNetP_tbl_Assets
                         join ao in _dbCon.GSNetP_tbl_AssetsOwned on new { x1 = a.AssetsOwnedID, x2 = CustomerId } equals new { x1 = ao.AssetsOwnedID, x2 = ao.CustomerID }
                         where a.SerialNum.ToUpper() == SerialNumber.ToUpper()
                         select new
                         {
                             AssetId = a.AssetID,
                         }).FirstOrDefault();
            if (asset != null)
            {
                if (asset.AssetId > 0)
                {
                    return asset.AssetId;
                }

            }
            return 0;
        }

        public int GetStatusIdByStatusNameAndAssetId(string StatusName, int AssetId)
        {
            var status = (from a in _dbCon.GSNetP_tbl_AssetStatus
                          where a.Status == StatusName && a.CompanyID == (from c in _dbCon.tblCustomers
                                                                          join ao in _dbCon.GSNetP_tbl_AssetsOwned on c.CustomerID equals ao.CustomerID
                                                                          join asset in _dbCon.GSNetP_tbl_Assets on new { x1 = ao.AssetsOwnedID, x2 = AssetId } equals new { x1 = asset.AssetsOwnedID, x2 = asset.AssetID }
                                                                          select c.ParentCompanyID).FirstOrDefault()
                          select new
                          {
                              AssetStatusId = a.AssetStatusID,
                          }).FirstOrDefault();
            if (status != null)
            {
                if (status.AssetStatusId > 0)
                {
                    return status.AssetStatusId;
                }
            }

            return 0;
        }

        public int GetLocationIdByLocation(string Location)
        {
            var location = (from ca in _dbCon.tblCustomerAddresses
                            join co in _dbCon.tblCountries on ca.CountryID equals co.CountryID
                            where new { address = co.CountryName + " - " + ca.CondensedAddress + " " + ca.Address1 }.address.Substring(0, 60) == Location && ca.IsActive == 1 && co.IsActive == true
                            select new
                            {
                                LocationId = ca.CustomerAddressID
                            }
                            ).Distinct().FirstOrDefault();

            if (location != null)
            {
                if (location.LocationId > 0)
                {
                    return location.LocationId;
                }
            }

            return 0;
        }

        public int GetSupplierIdByName(string Name)
        {
            var s = _dbCon.tblSFAs.FirstOrDefault(x => x.ParentCompanyID != null && x.IsComposite == true && x.SFAName == Name && x.IsActive == true);
            if (s != null)
            {
                return s.SFAID;
            }
            return 0;
        }

        public int GetContactId(string Name, int CustomerId)
        {
            var contact = (from con in _dbCon.tblContacts
                           join cust in _dbCon.tblCustomers on con.CompanyID equals cust.ParentCompanyID
                           where cust.CustomerID == CustomerId && con.IsActive == true && ((con.FirstName ?? "")+ " " + (con.LastName ?? "")).Contains(Name)
                           select new
                           {
                               ContactId = con.ContactID
                           }
                            ).Distinct().FirstOrDefault();
            if (contact != null)
            {
                if (contact.ContactId > 0)
                {
                    return contact.ContactId;
                }
            }

            return 0;
        }

        public int GetWarrantyIdByName(string Name)
        {
            var w = _dbCon.tblWarrantyTypes.FirstOrDefault(x => x.WarrantyTypeName == Name && x.IsActive == true);
            if (w != null)
            {
                return w.WarrantyTypeID;
            }
            return 0;
        }

        public int GetTicketCount(string TicketNo)
        {
            var ticketnum = 0;
            int.TryParse(TicketNo, out ticketnum);

            var count = _dbCon.tblTickets.Count(x => x.TicketID == ticketnum);
            if (count > 0)
            {
                return count;
            }
            return 0;
        }

        public int GetCustomerIdByAssetId(int AssetId)
        {
            var cust = (from a in _dbCon.GSNetP_tbl_Assets
                        join ao in _dbCon.GSNetP_tbl_AssetsOwned on a.AssetsOwnedID equals ao.AssetsOwnedID
                        where a.AssetID == AssetId
                        select new
                        {
                            CustomerId = ao.CustomerID,
                        }).FirstOrDefault();
            if (cust != null)
            {
                if (cust.CustomerId > 0)
                {
                    return cust.CustomerId;
                }

            }

            return 0;
        }
        public int GetAssetCountByCustomerIdAssetIdAndSerialNumber(int CustomerId, int AssetId, string SerialNumber)
        {
            var assetCount = (from a in _dbCon.GSNetP_tbl_Assets
                              join ao in _dbCon.GSNetP_tbl_AssetsOwned on new { x1 = a.AssetsOwnedID, x2 = CustomerId } equals new { x1 = ao.AssetsOwnedID, x2 = ao.CustomerID }
                              where a.SerialNum.ToUpper() == SerialNumber.ToUpper() && a.AssetID != AssetId
                              select a.AssetID).Count();
            if (assetCount > 0)
            {
                return assetCount;
            }
            return 0;
        }

        public int UpdateAssetDetail(AssetBulkUploadModel detailModel, int assetId, int userId)
        {
            try
            {
                if (assetId > 0)
                {
                    _dbCon.sp_UpdateAsset_AdminR(assetId, (detailModel.assetstatusid == 0 ? null : detailModel.assetstatusid), detailModel.customeraddressid, detailModel.description, (detailModel.manufacturerid == 0 ? null : detailModel.manufacturerid),
                        (detailModel.customerpartcategoryid == 0 ? null : detailModel.customerpartcategoryid), detailModel.model, (detailModel.currentownerid == 0 ? null : detailModel.currentownerid), detailModel.assettagnum, detailModel.serialnum, detailModel.Comments,
                        (detailModel.supportedbysfaid == 0 ? null : (int?)detailModel.supportedbysfaid), null, null, null, null, null, null, null, detailModel.costcenter, null, detailModel.warrantytypename, detailModel.warrantystartdate,
                        detailModel.warrantyenddate, null, null, null, null, null, detailModel.ticketid, null, userId, detailModel.SKU, detailModel.modelnumber, detailModel.systemname
                        , null, detailModel.leaseStartDate, detailModel.leaseEndDate, detailModel.region, detailModel.OldAssetName, detailModel.OldAssetTag, detailModel.OldLeasedorPurchased
                        , detailModel.NewLeasedorPurchased, detailModel.Imaging, detailModel.WhiteGloveDelivery, detailModel.AssetRentalAmount, detailModel.OldAssetModel,
                        detailModel.OldAssetModelNumber, detailModel.UserLoginName, detailModel.UserEmailAddress, detailModel.UserFirstName, detailModel.UserLastName,
                        detailModel.UserPhoneNumber, detailModel.ScheduledReturnDate, detailModel.InstallDate,null, null, null, null, null, null);

                    _dbCon.usp_AddWarranty_R(assetId, detailModel.warrantytypeid, detailModel.warrantystartdate, detailModel.warrantyenddate, userId);

                    return 1;
                }
                return 0;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Asset Search Presenter =>UpdateAssetDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Asset Search Presenter =>UpdateAssetDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Asset Search Presenter =>UpdateAssetDetail");
                }
                return 0;
            }

        }

        //sunny gohar on 18th Jan 2018 ======================

        public string GetParentCompanyNameByParentCompanyId(int ParentCompanyId)
        {
            var c = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == ParentCompanyId);
            if (c != null)
            {
                return c.ParentCompanyName;
            }
            return null;
        }

        //====================================================================

    }
}

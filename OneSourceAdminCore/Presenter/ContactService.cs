﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;

namespace OneSourceAdminCore.Presenter
{
    public class ContactService
    {

        private globalserveEntities dbCon;

        public ContactService()
        {
            dbCon = new globalserveEntities();
        }


        public List<Sp_GetContactList_AdminR_Result> GetContactList(int contactOwnedByCompany, int contactCratedByUser, int companyId, string searchStr)
        {
            var conList = dbCon.Sp_GetContactList_AdminR(contactOwnedByCompany, companyId, contactCratedByUser, searchStr, "500").ToList();
            return conList;
        }




    }
}

﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System.Diagnostics;
namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : july 21 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for managing sfa setting"
    /// </summary>
    public class SFAPreferencesPresenter
    {
        private readonly globalserveEntities _dbCon;

        public SFAPreferencesPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// this method is responsible for getting user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public tblUser GetUser(int id)
        {
            return _dbCon.tblUsers.FirstOrDefault(x => x.UserID == id);
        }

        /// <summary>
        /// this method is responsible for updating user password
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <param name="confirmPassword"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateUserPassword(string oldPassword, string newPassword, string confirmPassword, int userId)
        {
            try
            {
                int count = 0;
                var user = GetUser(userId);
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    user.UserPassword = newPassword;
                    user.LastUpdated = DateTime.Now;
                    user.LastUpdatedByUserID = userId;
                    _dbCon.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();
                    transaction.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => UpdateUserPassword<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => UpdateUserPassword");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateUserPassword");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting contact information
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public sp_getCompanyContactInfo_AdminR_Result GetContactInfo(int contactId, int companyId)
        {
            try
            {
                var data = _dbCon.sp_getCompanyContactInfo_AdminR(contactId, companyId).FirstOrDefault();
                return data == null ? new sp_getCompanyContactInfo_AdminR_Result() : data;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => GetContactInfo<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => GetContactInfo");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetContactInfo");
                }
                return new sp_getCompanyContactInfo_AdminR_Result();
            }
        }

        /// <summary>
        /// this method is responsible for updating contact information
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        /// <param name="usergroupName"></param>
        /// <returns></returns>
        public int UpdateContactInfo(sp_getCompanyContactInfo_AdminR_Result model, int userId, int contactId, string usergroupName)
        {
            try
            {
                int count = 0;
                var contact = _dbCon.tblContacts.FirstOrDefault(x => x.ContactID == contactId && x.UserID == userId);
                if (contact != null)
                {
                    using (var transaction = _dbCon.Database.BeginTransaction())
                    {
                        contact.LastName = model.lastname.Trim();
                        contact.FirstName = model.firstname;
                        contact.MiddleName = model.middlename;
                        contact.Salutation = model.salutation;
                        contact.Title = model.title;
                        contact.Department = model.department;
                        contact.FaxNumber = model.faxnumber;
                        contact.MobilePhoneNumber = model.mobilephonenumber;
                        contact.PagerNumber = model.pagernumber;
                        contact.LastUpdate = DateTime.Now;
                        contact.LastUpdateUserID = userId;
                        contact.Email = model.email;
                        contact.WorkPhoneNumber = model.workphonenumber;
                        _dbCon.Entry(contact).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        transaction.Commit();
                    }
                    if (usergroupName == "SFA")
                    {
                        var sfaUser = _dbCon.tblSFAUsers.FirstOrDefault(x => x.UserID == userId);
                        if (sfaUser != null)
                        {
                            using (var transaction = _dbCon.Database.BeginTransaction())
                            {
                                sfaUser.Email = model.email;
                                sfaUser.LastUpdated = DateTime.Now;
                                sfaUser.LastUpdatedByUserID = userId;
                                _dbCon.Entry(sfaUser).State = System.Data.Entity.EntityState.Modified;
                                count = _dbCon.SaveChanges();
                                transaction.Commit();
                            }
                        }
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => UpdateContactInfo<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => UpdateContactInfo");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateContactInfo");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting customer service id by service name
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public int GetCustServiceId(string serviceName)
        {
            try
            {
                var procure = _dbCon.tblOfferings.FirstOrDefault(x => x.OfferingShort == serviceName);
                return procure == null ? 0 : procure.OfferingID;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => GetCustServiceId<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => GetCustServiceId");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustServiceId");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting datetime formats
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetDateFormatList()
        {
            var list =
                _dbCon.tblDateFormats.Where(x => x.IsActive)
                    .OrderByDescending(x => x.IsDefaultFormat)
                    .Select(x => new GenericList()
                    {
                        Key = x.DateFormatID,
                        Value = x.DateFormatName.Trim()

                    }).OrderBy(x => x.Value).ToList();
            list.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });
            return list;
        }

        /// <summary>
        /// this method is responsible for getting preferences
        /// </summary>
        /// <param name="userGroupName"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public PreferenceModel GetPreferenceModel(string userGroupName, int userId)
        {
            try
            {
                PreferenceModel obj = new Model.PreferenceModel();
                obj.DateTimeList = GetDateFormatList();
                obj.CountryList = _dbCon.tblCountries.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.CountryID,
                    Value = x.CountryName
                }).OrderBy(x => x.Value).ToList();
                obj.CountryList.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });

                if (userGroupName == "SFA")
                {
                    //    List<string> lst = new List<string>() { "HomePage", "SearchPage", "DateFormat", "Country", "Language", "Rows", "Ticker", "ReportViewer" };

                    var preferences = _dbCon.tblPreferences.Where(x => x.IsActive == true).ToList();


                    var Id = preferences.FirstOrDefault(x => x.PreferenceName == "HomePage").PreferenceID;
                    var UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        obj.DHomePage = UserPreference.PreferenceValueRevamp ?? "0";
                    }
                    else
                    {
                        obj.DHomePage = "0";
                    }
                    // obj.DHomePage = obj.DHomePage.ToUpper();
                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "SearchPage").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        obj.DSearchPage = UserPreference.PreferenceValueRevamp ?? "0";
                    }
                    else
                    {
                        obj.DSearchPage = "0";
                    }
                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "DateFormat").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        obj.DDateFormatId = UserPreference.PreferenceValueRevamp == null ? 0 : Convert.ToInt32(UserPreference.PreferenceValueRevamp);
                    }
                    else
                    {
                        obj.DDateFormatId = 0;
                    }
                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "Country").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        obj.DCountryId = UserPreference.PreferenceValueRevamp == null ? 0 : Convert.ToInt32(UserPreference.PreferenceValueRevamp);
                    }
                    else
                    {
                        obj.DCountryId = 0;
                    }
                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "ReportViewer").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        obj.DReportViewer = UserPreference.PreferenceValueRevamp ?? "0";
                    }
                    else
                    {
                        obj.DReportViewer = "0";
                    }


                }


                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => GetPreferenceModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => GetPreferenceModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPreferenceModel");
                }
                return new Model.PreferenceModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating preferences
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int ChangePreference(PreferenceModel model, int userId)
        {
            try
            {
                int count = 0;

                var preferences = _dbCon.tblPreferences.Where(x => x.IsActive == true).ToList();
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    var Id = preferences.FirstOrDefault(x => x.PreferenceName == "HomePage").PreferenceID;
                    var UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        UserPreference.PreferenceValueRevamp = model.DHomePage;
                        UserPreference.LastUpdatedByUserID = userId;
                        UserPreference.LastUpdated = DateTime.Now;
                        _dbCon.Entry(UserPreference).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }

                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "SearchPage").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        UserPreference.PreferenceValueRevamp = model.DSearchPage;
                        UserPreference.LastUpdatedByUserID = userId;
                        UserPreference.LastUpdated = DateTime.Now;
                        _dbCon.Entry(UserPreference).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }
                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "DateFormat").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        UserPreference.PreferenceValueRevamp = model.DDateFormatId.ToString();
                        UserPreference.LastUpdatedByUserID = userId;
                        UserPreference.LastUpdated = DateTime.Now;
                        _dbCon.Entry(UserPreference).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }

                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "Country").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        UserPreference.PreferenceValueRevamp = model.DCountryId.ToString();
                        UserPreference.LastUpdatedByUserID = userId;
                        UserPreference.LastUpdated = DateTime.Now;
                        _dbCon.Entry(UserPreference).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }
                    Id = preferences.FirstOrDefault(x => x.PreferenceName == "ReportViewer").PreferenceID;
                    UserPreference = _dbCon.tblUserPreferences.FirstOrDefault(x => x.PreferenceID == Id && x.UserID == userId);
                    if (UserPreference != null)
                    {
                        UserPreference.PreferenceValueRevamp = model.DReportViewer.ToString();
                        UserPreference.LastUpdatedByUserID = userId;
                        UserPreference.LastUpdated = DateTime.Now;
                        _dbCon.Entry(UserPreference).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }
                    transaction.Commit();
                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => ChangePreference<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => ChangePreference");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "ChangePreference");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting services
        /// </summary>
        /// <param name="SFAID"></param>
        /// <returns></returns>
        public List<GenericList> GetServices(int SFAID)
        {
            var services = (from a in _dbCon.tblServices_VSRM
                            join b in _dbCon.tblGSPServices_VSRM.Where(x => x.GSPID == SFAID) on a.ServiceID equals b.ServiceID into ps
                            from b in ps.DefaultIfEmpty()
                            orderby a.ServiceName
                            select new { SelectedService = (int?)b.ServiceID, a.ServiceID, a.ServiceName }).ToList();
            List<GenericList> obj = services.Select(x => new GenericList
            {
                Key = x.ServiceID,
                Value = x.ServiceName,
                Count = Convert.ToInt32(x.SelectedService)
            }).ToList();
            return obj;
        }

        /// <summary>
        /// this method is responsible for getting uplifts and duties
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public List<SupplierUpliftsDutiesModel> GetUpliftsDuties(int parentCompanyId)
        {
            try
            {
                var query = @"select distinct m.manufacturername as 'Manufacturer', c.countryname as 'Country',
	                        CASE isnull(tbu.tradeblockuplift ,0) 
	                            WHEN 0 then 
	                            cast(cast(isnull(gtbu.tradeblockuplift , 0) * 100  as decimal(4,2)) as varchar) + '%'
	                        ELSE cast(cast(isnull(tbu.tradeblockuplift , 0) * 100 as decimal(4,2)) as varchar) + '%' 
	                        END [TradeBlockUplift], 
	                        
                            CASE isnull(tbu.duties ,0) * 100 
	                            WHEN 0 then 
	                            cast(cast(isnull(gtbu.duties , 0) * 100 as decimal(6,2)) as varchar) + '%'
	                        ELSE cast(cast(isnull(tbu.duties , 0) * 100 as decimal(6,2)) as varchar) + '%' 
	                        END [Duties], 
        
	                        CASE isnull(du.distributionuplift ,0) 
	                        WHEN 0 then 
	                        cast(cast(isnull(gdu.distributionuplift , 0) * 100 as decimal(4,2)) as varchar) + '%' 
	                        ELSE cast(cast(isnull(du.distributionuplift , 0) * 100 as decimal(4,2)) as varchar) + '%'
	                        END [DistributionUplift]
                            from 
                            tblparentcompanies pc 
                            join tblpartyaddress pa on pa.partyid = pc.partyid 
                            join tblsfas s on s.parentcompanyid = pc.parentcompanyid 
                            and s.isComposite=1 
                            join tbladdress a on a.addressid = pa.addressid 
                            join tblcountries c on c.countryid = a.countryid 
                            join tblsfaaccreditation sfaa on sfaa.sfaid = s.sfaid 
                            join tblmanufacturers m on m.manufacturerid = sfaa.manufacturerid 
                            left join tbltradeblockuplifts tbu on c.countryid = tbu.countryid and m.manufacturerid = tbu.manufacturerid 
                            left join tblGenTradeBlockUplifts gtbu on gtbu.manufacturerid = m.manufacturerid 
                            and gtbu.countryid = c.countryid 
                            left join tblDistributionUplifts du on du.manufacturerid = tbu.manufacturerid 
                            and s.sfaid = du.sfaid and du.countryid = c.countryid 
                            left join tblGenDistributionUplifts gdu on gdu.manufacturerid = tbu.manufacturerid and 
                            gdu.sfaid = s.sfaid where  
                            (isnull(tbu.duties, 0) > 0 or isnull(tbu.tradeblockuplift, 0) > 0 or isnull(du.distributionuplift, 0) > 0  or
                            isnull(gtbu.duties, 0) > 0 or isnull(gtbu.tradeblockuplift, 0) > 0 or isnull(gdu.distributionuplift, 0) > 0 ) and 
                            pc.parentcompanyid=" + parentCompanyId;
                var data = _dbCon.Database.SqlQuery<SupplierUpliftsDutiesModel>(query).ToList();
                return data;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => GetUpliftsDuties<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => GetUpliftsDuties");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUpliftsDuties");
                }
                return new List<SupplierUpliftsDutiesModel>();
            }
        }

        /// <summary>
        /// this method is responsible for getting addresses
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public List<sp_GetAddressByPartyId_AdminR_Result> GetAddresses(int parentCompanyId)
        {
            try
            {
                var partyId = GetPartyId(parentCompanyId);
                return _dbCon.sp_GetAddressByPartyId_AdminR(partyId).ToList();
            } 
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => GetAddresses<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => GetAddresses");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAddresses");
                }
                return new List<sp_GetAddressByPartyId_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method is responsible for getting party id by parent companyid
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public int GetPartyId(int parentCompanyId)
        {
            try
            {
                var data = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == parentCompanyId);
                return data != null ? Convert.ToInt32(data.PartyID) : 0;
            } 
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>sfa preference Presenter => GetPartyId<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "sfa preference Presenter => GetPartyId");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPartyId");
                }
                return 0;
            }
        }
        
    }
}

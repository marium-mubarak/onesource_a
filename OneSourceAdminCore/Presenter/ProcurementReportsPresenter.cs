﻿using System.Linq;
using OneSourceAdminCore.Entity;
using System.Collections.Generic;
using OneSourceAdminCore.Model;
using System;
using System.Data;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Helper;

namespace OneSourceAdminCore.Presenter
{
    public class ProcurementReportsPresenter
    {
        /// <summary>
        /// authenticate user by given username and password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        globalserveEntities dbCon;
        public ProcurementReportsPresenter()
        {
            dbCon = new globalserveEntities();
        }


        /// <summary>
        /// get contact information by userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public tblContact GetContactInfo(int userId)
        {
            try
            {
                tblContact contact = dbCon.tblContacts.Where(x => x.UserID == userId).FirstOrDefault();
                if (contact != null && !string.IsNullOrEmpty(contact.ContactID.ToString()))
                {
                    return contact;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetContactInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetContactInfo");
                }
            }

            return null;
        }

        public List<CustomerOrderReportModel> GetCustomerOrderDetails(int customerId, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                if (customerId <= 0)
                {
                    return null;
                }
                DateTime? date = null;

                var query = dbCon.tblOrders.Where(x => x.CustomerID == customerId).Select(x => new
                {
                    SessionTrackingNumber = x.SessionTrackingNumber,
                    DateShipped = x.tblOrderDestinations.FirstOrDefault(d => d.OrderTrackingNumber == x.OrderTrackingNumber && d.SessionTrackingNumber == x.SessionTrackingNumber) != null ? x.tblOrderDestinations.FirstOrDefault(d => d.OrderTrackingNumber == x.OrderTrackingNumber && d.SessionTrackingNumber == x.SessionTrackingNumber).DateShipped.Value : date,
                    OrderDate = x.OrderDate,
                    OrderedByUser = dbCon.tblUsers.Where(u => u.UserID == x.OrderedByUserID).FirstOrDefault() != null ? dbCon.tblUsers.Where(u => u.UserID == x.OrderedByUserID).FirstOrDefault().UserName : "",
                    OrderPO = x.OrderPO,
                    OrderTrackingNumber = x.OrderTrackingNumber,
                    DestinationTrackingNumber = x.tblOrderDestinations.FirstOrDefault(d => d.OrderTrackingNumber == x.OrderTrackingNumber && d.SessionTrackingNumber == x.SessionTrackingNumber) != null ? x.tblOrderDestinations.FirstOrDefault(d => d.OrderTrackingNumber == x.OrderTrackingNumber && d.SessionTrackingNumber == x.SessionTrackingNumber).DestinationTrackingNumber : 0,
                }).Where(x => x.DateShipped != null).OrderByDescending(x => x.DateShipped).ToList();

                if (startDate != null)
                {
                    query = query.Where(x => x.DateShipped != null ? x.DateShipped.Value.Date >= startDate.Value.Date : 1 == 1).OrderBy(x => x.DateShipped).ToList();
                }
                if (endDate != null)
                {
                    query = query.Where(x => x.DateShipped != null ? x.DateShipped.Value.Date <= endDate.Value.Date : 1 == 1).OrderBy(x => x.DateShipped).ToList();
                }

                var list = query.Select(x => new CustomerOrderReportModel
                {
                    SessionTrackingNumber = x.SessionTrackingNumber,
                    DateShipped = x.DateShipped != null ? x.DateShipped.Value.ToShortDateString() : null,
                    OrderDate = x.OrderDate != null ? x.OrderDate.Date.ToShortDateString() : null,
                    OrderedByUser = x.OrderedByUser,
                    OrderPO = x.OrderPO,
                    OrderTrackingNumber = x.OrderTrackingNumber,
                    DestinationTrackingNumber = x.DestinationTrackingNumber
                }).ToList();

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerOrderDetails");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerOrderDetails");
                }
            }

            return null;
        }

        public List<CustomerOrderItems> GetCustomerOrderItems(int sessionTrackingNumber, int orderTrackingNumber, int destinationTrackingNumber)
        {
            try
            {
                var query = dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == sessionTrackingNumber && x.OrderTrackingNumber == orderTrackingNumber && x.DestinationTrackingNumber == destinationTrackingNumber).Select(x => new CustomerOrderItems()
                {
                    OrderItemId = x.OrderItemId,
                    ItemDescription = x.PartDescription,
                    Sku = x.ManufacturerSKU,
                    UnitPrice = x.LineItemPriceUSD,
                    TotalPrice = x.LineItemTotalUSD,
                    Quantity = x.Quantity

                }).OrderByDescending(x => x.OrderItemId).ToList();

                return query;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerOrderItems");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerOrderItems");
                }
            }

            return null;
        }

        #region Quote Summary Report
        public List<QuoteSummaryReportOutput> getQuoteSummaryReportData(int customerID, int countryID, string dateFrom, string dateTo)
        {
            try
            {
                List<QuoteSummaryReportOutput> ListOutputData = new List<QuoteSummaryReportOutput>();

                //Quote Status ID of completed => where QuoteStatusCode = C
                int QuoteStatusID = dbCon.tblQuoteStatus.Where(i => i.QuoteStatusCode == "C").FirstOrDefault().QuoteStatusID;

                //Get all the quotes which are completed
                var CompletedQuotes = dbCon.tblQuoteSummaries.Where(i => countryID != 0 ? (i.CountryID == countryID && i.CustomerID == customerID && i.QuoteStatusID == QuoteStatusID) : (i.CustomerID == customerID && i.QuoteStatusID == QuoteStatusID)).ToList();

                if (dateFrom != null && dateFrom != "")
                {
                    if (dateTo != null && dateTo != "")
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd"));
                        DateTime ToDate = DateTime.Parse(DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                        ToDate = ToDate.Add(new TimeSpan(23, 59, 59));
                        var dateFilter = CompletedQuotes.Where(i => i.CreateDate >= FromDate && i.CreateDate <= ToDate);
                        CompletedQuotes = dateFilter.ToList();
                    }
                    else
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd"));
                        var dateFilter = CompletedQuotes.Where(i => i.CreateDate >= FromDate);
                        CompletedQuotes = dateFilter.ToList();
                    }
                }

                else if (dateTo != null && dateTo != "")
                {
                    if (dateFrom != null && dateFrom != "")
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd"));
                        DateTime ToDate = DateTime.Parse(DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                        ToDate = ToDate.Add(new TimeSpan(23, 59, 59));
                        var dateFilter = CompletedQuotes.Where(i => i.CreateDate >= FromDate && i.CreateDate <= ToDate);
                        CompletedQuotes = dateFilter.ToList();
                    }
                    else
                    {
                        DateTime ToDate = DateTime.Parse(DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                        ToDate = ToDate.Add(new TimeSpan(23, 59, 59));
                        var dateFilter = CompletedQuotes.Where(i => i.CreateDate <= ToDate);
                        CompletedQuotes = dateFilter.ToList();
                    }
                }

                //Iteration in records

                if (CompletedQuotes.Count() > 0)
                {
                    foreach (var item in CompletedQuotes)
                    {
                        QuoteSummaryReportOutput model = new QuoteSummaryReportOutput();
                        model.QuoteId = item.QuoteID;
                        model.CustomerReference = item.CustomerReference != null ? item.CustomerReference : "-";
                        model.CreatedDate = item.CreateDate.ToString("dd-MM-yyyy");
                        model.CurrencyId = item.CurrencyID;
                        model.CurrencySymbol = dbCon.tblCurrencies.Where(x => x.CurrencyID == model.CurrencyId).FirstOrDefault().CurrencySymbol;
                        var UserName = dbCon.tblUsers.Where(i => i.UserID == item.CreatedBy).FirstOrDefault().UserName;

                        model.CreatedBy = UserName;
                        ListOutputData.Add(model);
                    }
                }

                return ListOutputData;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "getQuoteSummaryReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "getQuoteSummaryReportData");
                }
            }

            return null;
        }

        public List<QuoteSummaryResponseOutput> getQuoteResponseByQuoteID(int quoteId)
        {
            try
            {
                List<QuoteSummaryResponseOutput> ListResponse = new List<QuoteSummaryResponseOutput>();

                var QuoteResponse = dbCon.tblQuoteResponses.Where(i => i.QuoteID == quoteId).ToList();

                if (QuoteResponse != null)
                {
                    foreach (var response in QuoteResponse)
                    {
                        QuoteSummaryResponseOutput model = new QuoteSummaryResponseOutput();
                        model.ManufacturerID = response.ManufacturerID;
                        model.Manufacturer = response.tblManufacturer.ManufacturerName;
                        model.SKU = response.SKU;
                        model.PartCategoryID = response.PartCategoryID;
                        model.PartCategory = response.tblPartCategory.PartCategoryName;
                        model.Description = response.Description;
                        model.Qunatity = Convert.ToInt32(response.Quantity);
                        model.Price = response.Price.ToString();
                        model.EOL = response.EOL.ToShortDateString();
                        ListResponse.Add(model);
                    }
                }

                return ListResponse;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "getQuoteResponseByQuoteID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "getQuoteResponseByQuoteID");
                }
            }

            return null;
        }
        #endregion

        #region ISD Amount Spent By user
        public List<sp_ISDAmountSpendByUserRep_AdminR_Result> GetOrdersByUserId(int userId, string startDate, string endDate)
        {
            try
            {
                return dbCon.sp_ISDAmountSpendByUserRep_AdminR(userId.ToString(), startDate, endDate).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOrdersByUserId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrdersByUserId");
                }
            }

            return null;
        }

        public List<Sp_GetUserByCustomerId_AdminR_Result> GetUserByCustomerId(int customerId)
        {
            try
            {
                return dbCon.Sp_GetUserByCustomerId_AdminR(customerId).ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUserByCustomerId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUserByCustomerId");
                }
            }

            return null;
        }
        #endregion

        #region Procurement Reports
        public DataTable GetProcurementReportData(string reportId, string reportName, ProcurementReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ProcurementReportMain_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@ReportId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CurrencyID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@OrderStatus", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@QuoteType", SqlDbType.Text));
                    command.Parameters[0].Value = reportName;
                    command.Parameters[1].Value = reportId;
                    command.Parameters[2].Value = model.StartDate != null ? model.StartDate : DateTime.Now.ToShortDateString();
                    command.Parameters[3].Value = model.EndDate != null ? model.EndDate : DateTime.Now.AddMonths(-6).ToShortDateString();
                    command.Parameters[4].Value = reportName.Contains("Order Report") || reportName.Contains("Quote Report") || reportName.Contains("Jira Orders") ? model.CustomerIds : model.CustomerId.ToString();
                    command.Parameters[5].Value = (model.CurrencyId != null) ? model.CurrencyId : "0";
                    command.Parameters[6].Value = (model.CountryIDs != null) ? model.CountryIDs.TrimEnd(',') : "";
                    command.Parameters[7].Value = (model.OrderStatus != null) ? model.OrderStatus.TrimEnd(',') : "";
                    command.Parameters[8].Value = (model.QuoteType != null) ? model.QuoteType : "";

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetProcurementReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProcurementReportData");
                }
            }

            return null;
        }

        public List<sp_GetOrderStatuses_AdminR_Result> GetOrderStatus()
        {
            try
            {
                return dbCon.sp_GetOrderStatuses_AdminR().ToList();

                //obj.OrderStatuses = new List<Entity.sp_GetOrderStatuses_AdminR_Result>();
                //obj.OrderStatuses.Insert(0, tempStatus.FirstOrDefault(x => x.OrderStatusId == "1,2,3"));
                //obj.OrderStatuses.Insert(1, tempStatus.FirstOrDefault(x => x.OrderStatusId == "4"));
                //obj.OrderStatuses.Insert(2, tempStatus.FirstOrDefault(x => x.OrderStatusId == "9"));
                //obj.OrderStatuses.Insert(3, tempStatus.FirstOrDefault(x => x.OrderStatusId == "8"));
                //obj.OrderStatuses.Insert(4, tempStatus.FirstOrDefault(x => x.OrderStatusId == "7"));
                //obj.OrderStatuses.Insert(5, tempStatus.FirstOrDefault(x => x.OrderStatusId == "6"));
                //obj.OrderStatuses.Insert(6, tempStatus.FirstOrDefault(x => x.OrderStatusId == "13"));
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOrderStatus");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOrderStatus");
                }
            }

            return null;
        }
        #endregion

        #region Open Order Report

        public DataTable GetOpenOrderComment(long sessionTrackingNumber, bool isAdmin = true)
        {
            try
            {

                var comments = dbCon.tblOrderCommentsbySuppliers.Where(x => x.SessionTrackingNumber == sessionTrackingNumber && x.PCComment == isAdmin).Select(x => x.Comments).ToList();
                DataTable dt = new DataTable();
                if (comments.Count > 0)
                {
                    dt.Columns.Add(" ");
                    dt.Columns.Add("Comments");
                    var count = 0;
                    foreach (var comment in comments)
                    {
                        count++;
                        DataRow dr = dt.NewRow();
                        dr[" "] = count;
                        dr["Comments"] = comment;
                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetOpenOrderComment");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOpenOrderComment");
                }
                return null;
            }
        }

        #endregion

        #region Shipped Orders Report

        public DataTable GetShippedOrdersReport(string startDate, string endDate, int currencyId, int expirationTime = 1800)
        {
            try
            {
                var dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                var con = new SqlConnection(regularConnectionString);
                con.Open();

                using (var command = new SqlCommand("SP_ShippedOrdersReport_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.VarChar));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.VarChar));
                    command.Parameters.Add(new SqlParameter("@CurrencyId", SqlDbType.Int));
                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = currencyId;

                    using (var da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = expirationTime;
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "ProcurementReportsPresenter.GetShippedOrdersReport");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "ProcurementReportsPresenter.GetShippedOrdersReport");
                }

                return null;
            }
        }

        #endregion
    }
}

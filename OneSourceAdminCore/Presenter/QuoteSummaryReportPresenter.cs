﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class QuoteSummaryReportPresenter
    {
        private readonly globalserveEntities _dbCon;

        public QuoteSummaryReportPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public List<QuoteSummaryReportOutput> getQuoteSummaryReportData(int customerID, int countryID, string dateFrom, string dateTo)
        {
            List<QuoteSummaryReportOutput> ListOutputData = new List<QuoteSummaryReportOutput>();

            //Quote Status ID of completed => where QuoteStatusCode = C
            int QuoteStatusID = _dbCon.tblQuoteStatus.Where(i => i.QuoteStatusCode == "C").FirstOrDefault().QuoteStatusID;

            //Get all the quotes which are completed
            var CompletedQuotes = _dbCon.tblQuoteSummaries.Where(i => countryID != 0 ? (i.CountryID == countryID && i.CustomerID == customerID && i.QuoteStatusID == QuoteStatusID) : (i.CustomerID == customerID && i.QuoteStatusID == QuoteStatusID)).ToList();
           
            if (dateFrom != null && dateFrom != "")
            {
                if (dateTo != null && dateTo != "")
                {
                    DateTime FromDate = DateTime.Parse(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd"));
                    DateTime ToDate = DateTime.Parse(DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                    var dateFilter = CompletedQuotes.Where(i => i.CreateDate >= FromDate && i.CreateDate <= ToDate);
                    CompletedQuotes = dateFilter.ToList();
                }
                else
                {
                    DateTime FromDate = DateTime.Parse(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd"));
                    var dateFilter = CompletedQuotes.Where(i => i.CreateDate >= FromDate);
                    CompletedQuotes = dateFilter.ToList();
                }
            }

            else if (dateTo != null && dateTo != "")
            {
                if (dateFrom != null && dateFrom != "")
                {
                    DateTime FromDate = DateTime.Parse(DateTime.Parse(dateFrom).ToString("yyyy-MM-dd"));
                    DateTime ToDate = DateTime.Parse(DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                    var dateFilter = CompletedQuotes.Where(i => i.CreateDate >= FromDate && i.CreateDate <= ToDate);
                    CompletedQuotes = dateFilter.ToList();
                }
                else
                {
                    DateTime ToDate = DateTime.Parse(DateTime.Parse(dateTo).ToString("yyyy-MM-dd"));
                    var dateFilter = CompletedQuotes.Where(i => i.CreateDate <= ToDate);
                    CompletedQuotes = dateFilter.ToList();
                }
            }

            //Iteration in records

            if (CompletedQuotes.Count() > 0)
            {
                foreach (var item in CompletedQuotes)
                {
                    QuoteSummaryReportOutput model = new QuoteSummaryReportOutput();
                    model.QuoteId = item.QuoteID;
                    model.CustomerReference = item.CustomerReference;
                    model.CreatedDate = item.CreateDate.ToShortDateString();

                    var UserName = _dbCon.tblUsers.Where(i => i.UserID == item.CreatedBy).FirstOrDefault().UserName;

                    model.CreatedBy = UserName;
                    ListOutputData.Add(model);
                }
            }

            return ListOutputData;
        }

        public List<QuoteSummaryResponseOutput> getQuoteResponseByQuoteID(int quoteId)
        {
            List<QuoteSummaryResponseOutput> ListResponse = new List<QuoteSummaryResponseOutput>();

            var QuoteResponse = _dbCon.tblQuoteResponses.Where(i => i.QuoteID == quoteId).ToList();

            if (QuoteResponse != null)
            {
                foreach (var response in QuoteResponse)
                {
                    QuoteSummaryResponseOutput model = new QuoteSummaryResponseOutput();
                    model.ManufacturerID = response.ManufacturerID;
                    model.Manufacturer = response.tblManufacturer.ManufacturerName;
                    model.SKU = response.SKU;
                    model.PartCategoryID = response.PartCategoryID;
                    model.PartCategory = response.tblPartCategory.PartCategoryName;
                    model.Description = response.Description;
                    model.Qunatity = Convert.ToInt32(response.Quantity);
                    model.Price = response.Price.ToString();
                    model.EOL = response.EOL.ToShortDateString();
                    ListResponse.Add(model);
                }
            }

            return ListResponse;
        }
    }
}

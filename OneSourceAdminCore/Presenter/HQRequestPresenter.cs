﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class HQRequestPresenter
    {
        private readonly globalserveEntities _dbCon;
        tblHardwareQuote _hquotes;
        tblQuoteLineItem _lineitems;

        public HQRequestPresenter()
        {
            _dbCon = new Entity.globalserveEntities();
            _hquotes = new tblHardwareQuote();
            _lineitems = new tblQuoteLineItem();
        }

        #region properties

        public tblHardwareQuote HardwareQuote
        {
            get { return _hquotes; }
            set { _hquotes = value; }
        }

        public tblQuoteLineItem QuoteLineItems
        {
            get { return _lineitems; }
            set { _lineitems = value; }
        }

        #endregion

        #region Hardware Quotes

        public int Save()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (_hquotes.HardwareQuoteId > 0)
                {
                    _dbCon.Entry(_hquotes).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _dbCon.tblHardwareQuotes.Add(_hquotes);
                }

                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;
        }

        public int Delete(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblHardwareQuotes.FirstOrDefault(x => x.HardwareQuoteId == id);
                if (data != null)
                {
                    _dbCon.tblHardwareQuotes.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public List<tblHardwareQuote> GetHarwareQuotes()
        {
            return _dbCon.tblHardwareQuotes.ToList();
        }

        public List<HardwareQuoteModel> Get()
        {
            var hq = (from H in _dbCon.tblHardwareQuotes
                      join C in _dbCon.tblCountries on H.CountryId equals C.CountryID
                      //join CR in _dbCon.tblCurrencies on H.CurrencyId equals CR.CurrencyID
                      join O in _dbCon.tblHQOptions on H.HQOptionId equals O.HQOptionId
                      //join S in _dbCon.tblSFAs on H.SfaId equals S.SFAID
                      select new HardwareQuoteModel
                      {
                          Attachment1 = H.Attachment1,
                          Attachment2 = H.Attachment2,
                          Attachment3 = H.Attachment3,
                          Attachment4 = H.Attachment4,
                          Attachment5 = H.Attachment5,
                          City = H.City,
                          Comments = H.Comments,
                          Country = C.CountryName,
                          CountryId = H.CountryId,
                          CreatedBy = H.CreatedBy,
                          CreatedDate = H.CreatedDate,
                          //Currency = CR.CurrencyName,
                          CurrencyId = H.CurrencyId,
                          CustomerDeliveryAddress = H.CustomerDeliveryAddress,
                          CustomerInvoiceAddress = H.CustomerInvoiceAddress,
                          CustomerName = H.CustomerName,
                          Description = H.Description,
                          ExpectedPricePoint = H.ExpectedPricePoint,
                          HardwareQuoteId = H.HardwareQuoteId,
                          HQOption = O.HQOptionName,
                          HQOptionComment = H.HQOptionComment,
                          HQOptionId = H.HQOptionId,
                          HQResponseId = H.HQResponseId,
                          Manufacturer = H.Manufacturer,
                          ProjectTimeLine = H.ProjectTimeLine,
                          Quantity = H.Quantity,
                          SKU = H.SKU,
                          UpdatedBy = H.UpdatedBy,
                          UpdatedDate = H.UpdatedDate,
                          SfaId = H.SfaId,
                          AccountManagerName = H.AccountManagerName,
                          EndUserAccountDetails = H.EndUserAccountDetails,
                          GlobalDealId = H.GlobalDealId,
                          OEMAccountManager = H.OEMAccountManager,
                          LineItems = _dbCon.tblQuoteLineItems.Where(x => x.QuoteId == H.HardwareQuoteId && x.QuoteTypeId == 1).Count()
                          //Supplier = S.SFAName,
                          //SupplierEmail = S.Email
                      }).ToList();

            return hq;
        }


        #endregion

        #region Options
        public List<tblCountry> Countries()
        {
            return _dbCon.tblCountries.ToList();
        }

        public List<tblCurrency> Currencies()
        {
            return _dbCon.tblCurrencies.ToList();
        }

        public List<tblHQOption> HQOptions()
        {
            return _dbCon.tblHQOptions.ToList();
        }

        #endregion

        #region Line Items

        public int SaveLineItems()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {

                _dbCon.tblQuoteLineItems.Add(_lineitems);
                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;

        }

        public int UpdateLineItem()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                _dbCon.Entry(_lineitems).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;
        }

        public void DeleteLineItems(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblQuoteLineItems.FirstOrDefault(x => x.QuoteLineItemId == id);
                if (data != null)
                {
                    _dbCon.tblQuoteLineItems.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
        }

        public List<tblQuoteLineItem> GetQuoteLineItemByQuoteId(int quoteid, int qoutetype)
        {
            return _dbCon.tblQuoteLineItems.Where(x => x.QuoteId == quoteid && x.QuoteTypeId == qoutetype).ToList();
        }

        public tblQuoteLineItem GetQouteLineItemById(int quotelineId)
        {
            return _dbCon.tblQuoteLineItems.FirstOrDefault(x => x.QuoteLineItemId == quotelineId);
        }

        #endregion
    }
}

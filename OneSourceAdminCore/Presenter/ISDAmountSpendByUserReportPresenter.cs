﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class ISDAmountSpendByUserReportPresenter
    {
        private readonly globalserveEntities _dbCon;

        public ISDAmountSpendByUserReportPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        public List<sp_ISDAmountSpendByUserRep_AdminR_Result> GetOrdersByUserId(int userId, string startDate, string endDate)
        {
            return _dbCon.sp_ISDAmountSpendByUserRep_AdminR(userId.ToString(), startDate, endDate).ToList();
        }

        public List<Sp_GetUserByCustomerId_AdminR_Result> GetUserByCustomerId(int customerId)
        {
            return _dbCon.Sp_GetUserByCustomerId_AdminR(customerId).ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class CustomerMenuService
    {

        private globalserveEntities dbCon;

        public CustomerMenuService()
        {
            dbCon = new globalserveEntities();
        }


        public List<CustomerMenuModel> GetActiveCustomersWithMenu()
        {
            var customerList = dbCon.tblCustomers.Where(x => x.IsActive).ToList();

            var menuCustomerList = customerList.Select(x => new CustomerMenuModel()
            {
                CustomerId = x.CustomerID,
                CustomerName = x.CustomerName.Trim(),
                IsRestrictedDashBoard = CheckCustomerMenurestriction(x.CustomerID, 1),
                IsRestrictedReports = CheckCustomerMenurestriction(x.CustomerID, 2),
                IsRestrictedSmbQuotes= CheckCustomerMenurestriction(x.CustomerID, 3),
                IsRestrictedSoftware = CheckCustomerMenurestriction(x.CustomerID, 4)
            }).OrderBy(x => x.CustomerName).ToList();
            return menuCustomerList;
        }

        public bool CheckCustomerMenurestriction(int customerId, int menuId)
        {
            var chk =
                dbCon.tblCustomerMenu_R.Any(x => x.CustomerId == customerId && x.MenuId == menuId && x.IsRestricted);
            return chk;
        }

        public bool UpdateCustomerMenuRestriction(int customerId, int menuId, string menuName, bool isRestricted,
            int currentUser)
        {
            try
            {
                if (!isRestricted)
                {
                    var customerMenu = new tblCustomerMenu_R()
                    {
                        MenuId = menuId,
                        MenuName = menuName.Trim(),
                        CustomerId = customerId,
                        CreatedBy = currentUser,
                        CreatedDate = DateTime.Now,
                        IsRestricted = true
                    };
                    dbCon.tblCustomerMenu_R.Add(customerMenu);
                    dbCon.SaveChanges();
                }
                else
                {
                    var chkMenu =
                        dbCon.tblCustomerMenu_R.Where(x => x.MenuId == menuId && x.CustomerId == customerId).ToList();
                    if (chkMenu.Count > 0)
                    {
                        dbCon.tblCustomerMenu_R.RemoveRange(chkMenu);
                        dbCon.SaveChanges();
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCustomerMenuRestriction");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCustomerMenuRestriction");
                }
                return false;
            }
        }

    }

    public class CustomerMenuModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public bool IsRestrictedDashBoard { get; set; }
        public bool IsRestrictedReports { get; set; }
        public bool IsRestrictedSmbQuotes { get; set; }
        public bool IsRestrictedSoftware { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Helper;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Feb 10 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Region"
    /// </summary>
    public class RegionPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public RegionPresenter()
        {
            _dbCon = new globalserveEntities();
        }
        /// <summary>
        /// give list of Regions to RegionController
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetRegions()
        {
            var countries = _dbCon.tblRegion_R.Where(x => x.IsActive).Select(x => new GenericList
            {
                Key = x.RegionId,
                Value = x.RegionName

            }).ToList();
            return countries;
        }

        /// <summary>
        /// give list if country region mapping
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public List<tblCountryRegionMapping> GetRegionWiseCountry(int regionId)
        {
            var countries = _dbCon.tblCountryRegionMappings.Where(x => x.IsActive && x.RegionId == regionId).ToList();
            return countries;
        }

        public List<tblCountryRegionMapping> GetRegionAndCustomerWiseCountry(int regionId,int customerId)
        {
            var countries = _dbCon.tblCountryRegionMappings.Where(x => x.IsActive && x.RegionId == regionId && x.CustomerID==customerId).ToList();
            return countries;
        }

        /// <summary>
        /// add country region mapping
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        public ParentCountryRegionMapping AddRegionCountryMapping(tblCountryRegionMapping mapping,int userId)
        {
            ParentCountryRegionMapping parent = new ParentCountryRegionMapping();
            tblCountryRegionMapping countryRegionmap =
                _dbCon.tblCountryRegionMappings.FirstOrDefault(x => x.IsActive && x.CountryId == mapping.CountryId && x.CustomerID == mapping.CustomerID && x.RegionId==mapping.RegionId); //sunny gohar on 9th Jan 2018
            if (countryRegionmap != null)
            {
                parent.RegionWiseCountry = countryRegionmap;
                parent.Count = 0;
                return parent;
            }
           
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                mapping.IsActive = true;
                mapping.LastUpdated = DateTime.Now;
                mapping.UpdatedByUserID = userId;

                _dbCon.tblCountryRegionMappings.Add(mapping);

                var region = _dbCon.tblRegion_R.FirstOrDefault(x => x.RegionId == mapping.RegionId);
                if (region != null)
                {
                    region.LastUpdated = DateTime.Now;
                    region.UpdatedByUserID = userId;
                    _dbCon.Entry(region).State = System.Data.Entity.EntityState.Modified;
                }
                parent.Count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return parent;
        }


        /// <summary>
        /// delete country region mapping
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteRegionCountryMapping(int id,int userId)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var result = _dbCon.tblCountryRegionMappings.FirstOrDefault(x => x.CountryRegionMappingId == id);
                if (result != null)
                {
                    _dbCon.tblCountryRegionMappings.Remove(result);

                    var region = _dbCon.tblRegion_R.FirstOrDefault(x => x.RegionId == result.RegionId);
                    if (region != null)
                    {
                        region.LastUpdated = DateTime.Now;
                        region.UpdatedByUserID = userId;
                        _dbCon.Entry(region).State = System.Data.Entity.EntityState.Modified;
                    }

                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public string AddRegion(RegionModel region,int userId)
        {
            try
            {
                tblRegion_R exists =
                        _dbCon.tblRegion_R.FirstOrDefault(x => x.IsActive && x.RegionName == region.RegionName); //sunny gohar on 9th Jan 2018
                if (exists != null)
                {
                    exists.RegionName = region.RegionName;
                    return exists.RegionName+" already exists";
                }

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    var newRegion = new tblRegion_R();
                    newRegion.IsActive = true;
                    newRegion.RegionName = region.RegionName;
                    newRegion.LastUpdated = DateTime.Now;
                    newRegion.UpdatedByUserID = userId;
                    _dbCon.tblRegion_R.Add(newRegion);
                    _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return "Region '"+ region.RegionName + "' has been added";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddRegion");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddRegion");
                }
                return "Something went wrong while adding region, please try again later";
            }
        }
    }
}

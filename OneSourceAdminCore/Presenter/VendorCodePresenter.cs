﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Helper;
using System.Data.Entity.Infrastructure;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : Feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for providing vendor code detail"
    /// </summary>
    public class VendorCodePresenter
    {
        private readonly globalserveEntities _dbCon;
        public VendorCodePresenter()
        {
            _dbCon = new globalserveEntities();
        }
        /// <summary>
        /// this method is reponsible for getting vendor codes detail from database
        /// </summary>
        /// <returns></returns>
        public List<sp_getVendorCodes_AdmniR_Result> GetVendorCodes()
        {
            return _dbCon.sp_getVendorCodes_AdmniR().ToList();
        }

        /// <summary>
        /// this method is responsible for getting vendor code
        /// </summary>
        /// <param name="Vcode"></param>
        /// <returns></returns>
        public VendorCodeModel GetVendorCodeInfo(int Vcode)
        {
            VendorCodeModel obj = new VendorCodeModel();
            obj.vendorCodeInfo = new tblXrefSFA();
            obj.VCode = Vcode;

            CountryPresenter cp = new CountryPresenter();
            obj.Countries = cp.GetCountries();
            obj.Suppliers = _dbCon.tblSFAs.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Value = x.SFAName,
                Key = x.SFAID
            }).OrderBy(x => x.Value).ToList();

            obj.XrefTypes = _dbCon.tblXrefTypes.Where(x => x.IsActive == 1).Select(x => new GenericList { Value = x.XrefTypeName, Key = x.XrefTypeID }).ToList();

            if (Vcode > 0)
            {
                obj.vendorCodeInfo = _dbCon.tblXrefSFAs.FirstOrDefault(x => x.XrefSFAsID == Vcode);
            }
            return obj;

        }

        /// <summary>
        /// this method is responsible for add and update vendor
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object AddUpdateVendorCode(tblXrefSFA model, int userId)
        {

            int count = 0;
            string msg = string.Empty;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    var ExistingRecord = _dbCon.tblXrefSFAs.FirstOrDefault(x => x.XrefTableID == model.XrefTableID && x.SFAID == model.SFAID && x.XrefCode == model.XrefCode && x.CountryID == model.CountryID);
                    if (ExistingRecord != null)
                    {
                        if (model.XrefSFAsID != ExistingRecord.XrefSFAsID)
                        {
                            return new
                            {
                                count = 0,
                                msg = "Similar record already exists"
                            };
                        }
                        ((IObjectContextAdapter)_dbCon).ObjectContext.Detach(ExistingRecord);
                    }
                     

                    model.LastUpdated = DateTime.Now;
                    model.LastUpdatedByUserID = userId;

                    if (model.XrefSFAsID > 0)
                    {
                        _dbCon.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Vendor Code Updated Successfully" : "Error Occured.";
                    }
                    else
                    {
                        model.IsActive = true;
                        _dbCon.tblXrefSFAs.Add(model);
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Vendor Code Added Successfully" : "Error Occured.";
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateVendorCode");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateVendorCode");
                }
            }
            return new
            {
                count = count,
                msg = msg
            };

        }

    }
}

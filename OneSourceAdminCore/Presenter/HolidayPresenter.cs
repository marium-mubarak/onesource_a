﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : dec 22 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for national holiday"
    /// </summary>
    public class HolidayPresenter
    {
        private readonly globalserveEntities _dbCon;
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public HolidayPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// this action is responsible for getting holiday
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public List<sp_getHolidays_AdminR_Result> GetHolidays(int countryId)
        {
            return _dbCon.sp_getHolidays_AdminR(countryId).ToList();
        }

        /// <summary>
        /// this action is responsible for adding holiday
        /// </summary>
        /// <param name="holiday"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddHoliday(HolidayModel holiday, int userId)
        {
            var a = _dbCon.sp_UpdateHoliday_R(holiday.CountryID, holiday.holidayName, null, new DateTime(1900, holiday.month, holiday.day), true, userId);
            return a;
        }
        /// <summary>
        /// this action is responsible for deleting holiday
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="holidayId"></param>
        /// <returns></returns>
        public int DeleteHoliday(int countryId, int holidayId)
        {
            var a = _dbCon.sp_UpdateHoliday_R(countryId, null, holidayId, null, false, null);
            return a;
        }
    }
}

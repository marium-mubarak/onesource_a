﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class SOCShiftPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public SOCShiftPresenter()
        {
            _dbCon = new globalserveEntities();
        }
        /// <summary>
        /// give list of SOC Shifts
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetSOCShifts()
        {
            var countries = _dbCon.tblSOCShifts.Select(x => new GenericList
            {
                Key = x.SOCShiftID,
                Value = x.SOCShiftName
            }).ToList();
            return countries;
        }

        /// <summary>
        /// give list if country SOC Shifts mapping
        /// </summary>
        /// <param name="socShiftId"></param>
        /// <returns></returns>
        public List<tblSOCShiftMapping> GetSOCShiftWiseCountry(int socShiftId)
        {
            var countries = _dbCon.tblSOCShiftMappings.Where(x => x.IsActive && x.SOCShiftID == socShiftId).ToList();
            return countries;
        }
        /// <summary>
        /// add country shift mapping
        /// 
        /// Aurangzeb Khan
        /// 12-12-2018
        /// Changed mappping functionality
        /// 
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        public string AddSOCShiftCountryMapping(SOCShiftModel mapping, int userId)
        {
            StringBuilder message = new StringBuilder();
            List<string> existingIds = new List<string>();
            var insertedCount = 0;
            try
            {
                ParentCountrySOCShiftMapping parent = new ParentCountrySOCShiftMapping();
                var count = 0;

                //remove exisiting mappings
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    var mappings = _dbCon.tblSOCShiftMappings.Where(x => x.IsActive && x.SOCShiftID == mapping.SOCShiftId && x.TimeZone == mapping.TimeZone).ToList();
                    _dbCon.tblSOCShiftMappings.RemoveRange(mappings);
                    _dbCon.SaveChanges();
                    transaction.Commit();
                }

                if (!String.IsNullOrEmpty(mapping.CountryIds))
                {
                    foreach (var countryId in mapping.CountryIds.Split(','))
                    {
                        var Id = Convert.ToInt32(countryId);
                        tblSOCShiftMapping countryRegionmap =
                        _dbCon.tblSOCShiftMappings.FirstOrDefault(x => x.IsActive && x.CountryId == Id);
                        if (countryRegionmap != null)
                        {
                            count++;
                            parent.SOCShiftWiseCountry = countryRegionmap;
                            parent.Count = 0;
                            var countryName = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == Id) != null ? _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == Id).CountryName : "";
                            message.Append(countryName + ",");
                            existingIds.Add(countryId);
                        }
                    }

                    if (message.Length > 0)
                    {
                        message.Remove(message.Length - 1, 1);
                    }

                    using (var transaction = _dbCon.Database.BeginTransaction())
                    {
                        var countryIds = mapping.CountryIds.Split(',');
                        var countryNames = mapping.CountryNames.Split(',');
                        for (int i = 0; i < countryIds.Count(); i++)
                        {
                            if (!existingIds.Contains(countryIds[i]))
                            {
                                var newMapping = new tblSOCShiftMapping();
                                newMapping.CountryId = Convert.ToInt32(countryIds[i]);
                                newMapping.CountryName = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == newMapping.CountryId) != null ? _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == newMapping.CountryId).CountryName : "";
                                newMapping.IsActive = true;
                                newMapping.SOCShiftID = mapping.SOCShiftId;
                                newMapping.SOCShiftName = mapping.SOCShiftName;
                                newMapping.TimeZone = mapping.TimeZone;
                                newMapping.LastUpdated = DateTime.Now;
                                newMapping.UpdatedByUserID = userId;

                                _dbCon.tblSOCShiftMappings.Add(newMapping);

                                var shift = _dbCon.tblSOCShifts.FirstOrDefault(x => x.SOCShiftID == mapping.SOCShiftId);
                                if (shift != null)
                                {
                                    shift.CreateDate = DateTime.Now;
                                    shift.CreatedBy = userId;
                                    _dbCon.Entry(shift).State = System.Data.Entity.EntityState.Modified;
                                }

                                insertedCount = _dbCon.SaveChanges();
                            }
                        }

                        transaction.Commit();
                    }
                }
                else
                {
                    return "No country mappings available!";
                }

                if (!String.IsNullOrEmpty(message.ToString()))
                {
                    if (existingIds.Count > 1)
                    {
                        return message + " are already mapped with another SOC shift";
                    }
                    else
                    {
                        return message + " is already mapped with another SOC shift";
                    }
                }
                else
                {
                    return "Country has been mapped";
                }

            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "AddSOCShiftCountryMapping");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSOCShiftCountryMapping");
                }
                return null;
            }
        }


        /// <summary>
        /// delete country shift mapping
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteSOCShiftCountryMapping(int id, int userId)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var result = _dbCon.tblSOCShiftMappings.FirstOrDefault(x => x.CountrySOCShiftMappingId == id);
                if (result != null)
                {
                    _dbCon.tblSOCShiftMappings.Remove(result);

                    var region = _dbCon.tblSOCShifts.FirstOrDefault(x => x.SOCShiftID == result.SOCShiftID);
                    if (region != null)
                    {
                        region.CreateDate = DateTime.Now;
                        region.CreatedBy = userId;
                        _dbCon.Entry(region).State = System.Data.Entity.EntityState.Modified;
                    }

                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public string AddSOCShift(SOCShiftModel SOCShift, int userId)
        {
            try
            {
                tblSOCShift exists =
                        _dbCon.tblSOCShifts.FirstOrDefault(x => x.SOCShiftName == SOCShift.SOCShiftName);
                if (exists != null)
                {
                    exists.SOCShiftName = SOCShift.SOCShiftName;
                    return exists.SOCShiftName + " already exists";
                }

                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    var newShift = new tblSOCShift();
                    newShift.SOCShiftName = SOCShift.SOCShiftName;
                    newShift.CreateDate = DateTime.Now;
                    newShift.CreatedBy = userId;
                    _dbCon.tblSOCShifts.Add(newShift);
                    _dbCon.SaveChanges();
                    transaction.Commit();

                }
                return "SOC Shift '" + SOCShift.SOCShiftName + "' has been added";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddSOCShift");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddSOCShift");
                }
                return "Something went wrong while adding SOC Shift, please try again later";
            }
        }

        //public List<SelectListItem> GetSOCShiftCountries(int shiftId)
        //{
        //    try
        //    {
        //        var shiftCountryIds = _dbCon.tblSOCShiftMappings.Where(x => x.IsActive == true && x.SOCShiftID == shiftId).Select(x => x.CountryId).ToList();

        //        var noMappings = false;

        //        if (shiftCountryIds.Count <= 0)
        //        {
        //            noMappings = true;
        //        }

        //        var countries = _dbCon.tblCountries.Where(x => x.IsActive).Select(x => new GenericList
        //        {
        //            Key = x.CountryID,
        //            Value = x.CountryName

        //        }).OrderBy(x => x.Value).ToList();

        //        countries.Add(new GenericList() { Value = "--Select--", Key = -1 });

        //        var shiftCountryList = countries.Select(x => new SelectListItem
        //        {
        //            Value = x.Key.ToString(),
        //            Text = x.Value,
        //            Selected = (shiftCountryIds.Contains(x.Key)) || ( noMappings == true && x.Key ==-1) ? true : false
        //        }).OrderByDescending(x => x.Selected == true).ToList();


        //        return shiftCountryList;
        //    }
        //    catch (Exception ex)
        //    {
        //        StaticHelper.LogException(ex.ToString(), "GetSOCShiftCountries");
        //        if (ex.InnerException != null)
        //        {
        //            StaticHelper.LogException(ex.InnerException.ToString(), "GetSOCShiftCountries");
        //        }
        //        return null;
        //    }
        //}

        public List<SelectListItem> GetCountriesByTimeZone(string timeZone, int shiftId)
        {
            try
            {
                var shiftCountryIds = _dbCon.tblSOCShiftMappings.Where(x => x.IsActive == true && x.TimeZone == timeZone && x.SOCShiftID == shiftId).Select(x => x.CountryId).ToList();

                var noMappings = false;

                if (shiftCountryIds.Count <= 0)
                {
                    noMappings = true;
                }

                var countries = _dbCon.tblCountries.Where(x => x.IsActive).Select(x => new GenericList
                {
                    Key = x.CountryID,
                    Value = x.CountryName

                }).OrderBy(x => x.Value).ToList();

                //countries.Add(new GenericList() { Value = "--Select--", Key = -1 });

                var shiftCountryList = countries.Select(x => new SelectListItem
                {
                    Value = x.Key.ToString(),
                    Text = x.Value,
                    Selected = (shiftCountryIds.Contains(x.Key)) || (noMappings == true && x.Key == -1) ? true : false
                }).OrderByDescending(x => x.Selected == true).ToList();


                return shiftCountryList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSOCShiftCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSOCShiftCountries");
                }
                return null;
            }
        }

        public List<SelectListItem> GetSOCShiftTimeZones(int shiftId = 0)
        {
            try
            {
                var timeZonesMappings = _dbCon.tblSOCShiftMappings.Where(x => x.IsActive == true && x.SOCShiftID == shiftId).Select(x => x.TimeZone).ToList();

                var noMappings = false;

                if (timeZonesMappings.Count <= 0)
                {
                    noMappings = true;
                }

                var timeZones = _dbCon.tblTimeZones.Where(x => x.IsActive).Select(x => new SelectListItem
                {
                    Value = x.TimeZoneName,
                    Text = !String.IsNullOrEmpty(x.TimeZoneName) ? x.TimeZoneName : ""

                }).Distinct().OrderBy(x => x.Value).ToList();

                timeZones.Insert(0, new SelectListItem() { Text = "--Select--", Value = "-1" });

                var timeZonesList = timeZones.Select(x => new SelectListItem
                {
                    Value = x.Value,
                    Text = x.Text,
                    //Selected = timeZonesMappings.Contains(x.Value) || (noMappings == true && x.Value == "-1") ? true : false
                    Selected = false
                }).OrderByDescending(x => x.Selected == true).ToList();


                return timeZonesList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSOCShiftTimeZones");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSOCShiftTimeZones");
                }
                return null;
            }
        }

        public List<SocShiftsListModel> GetAllSocShiftMappings()
        {
            try
            {
                var mappings = _dbCon.tblSOCShiftMappings.Where(x => x.IsActive == true).Select(x => new SocShiftsListModel
                {
                    SocShift = x.SOCShiftName,
                    Country = x.CountryName,
                    TimeZone = x.TimeZone

                }).ToList();

                return mappings;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAllSocShiftMappings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAllSocShiftMappings");
                }
                return new List<SocShiftsListModel>();
            }
        }
    }
}

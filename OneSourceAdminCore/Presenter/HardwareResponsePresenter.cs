﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class HardwareResponsePresenter
    {
        private readonly globalserveEntities _dbCon;
        tblHardwareQuoteResponse _hresponsequote;
        tblResponseQuoteLineItem _lineitems;

        public HardwareResponsePresenter()
        {
            _dbCon = new globalserveEntities();
            _hresponsequote = new tblHardwareQuoteResponse();
            _lineitems = new tblResponseQuoteLineItem();
        }

        #region Properties
        public tblHardwareQuoteResponse HardwareResponseQuote
        {
            get { return _hresponsequote; }
            set { value = _hresponsequote; }
        }

        public tblResponseQuoteLineItem LineItems
        {
            get { return _lineitems; }
            set { value = _lineitems; }
        }

        #endregion

        #region Hardware Response Quotes

        public int SaveHardwareQuote()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (_hresponsequote.HardwareResponseID > 0)
                {
                    _dbCon.Entry(_hresponsequote).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _dbCon.tblHardwareQuoteResponses.Add(_hresponsequote);
                }

                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;
        }

        public int DeleteHadwareResponseQoute(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblHardwareQuoteResponses.FirstOrDefault(x => x.HardwareResponseID == id);
                if (data != null)
                {
                    _dbCon.tblHardwareQuoteResponses.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }

                transcation.Commit();
            }

            return count;
        }

        public List<tblHardwareQuoteResponse> GetHardwareResponseQuote()
        {
            return _dbCon.tblHardwareQuoteResponses.ToList();
        }

        #endregion

        #region Line Items

        public int SaveLineItem()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (_lineitems.ResponseLineItemId > 0)
                {
                    _dbCon.Entry(_lineitems).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _dbCon.tblResponseQuoteLineItems.Add(_lineitems);
                }

                count = _dbCon.SaveChanges();
                transaction.Commit();

                _lineitems = new tblResponseQuoteLineItem();
            }

            return count;
        }

        public int DeleteResponseLineItem(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblResponseQuoteLineItems.FirstOrDefault(x => x.ResponseLineItemId == id);
                if (data != null)
                {
                    _dbCon.tblResponseQuoteLineItems.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }

                transcation.Commit();
            }

            return count;
        }

        public List<tblResponseQuoteLineItem> GetRsponseLineItem()
        {
            return _dbCon.tblResponseQuoteLineItems.ToList();
        }

        #endregion

        #region Category

        public List<tblPartCategory> GetCategory()
        {
            return _dbCon.tblPartCategories.ToList();
        }

        #endregion
    }
}

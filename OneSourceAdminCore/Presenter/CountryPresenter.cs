﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Web.UI.WebControls;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Feb 04 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Country Alert"
    /// </summary>
    public class CountryPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public CountryPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// give list of countries
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetCountries()
        {
            var countries = _dbCon.tblCountries.Where(x => x.IsActive).Select(x => new GenericList
            {
                Key = x.CountryID,
                Value = x.CountryName

            }).OrderBy(x => x.Value).ToList();
            return countries;
        }
        /// <summary>
        /// give list of country alert
        /// </summary>
        /// <returns></returns>
        public List<CustomCountryAlert> GetCountryAlert()
        {
            var countriesAlert = _dbCon.tblCountryAlerts.Select(x => new CustomCountryAlert
            {
                CountryAlertId = x.CountryAlertId,
                CountryId = x.CountryId,
                CountryName = x.CountryName,
                Alert = x.Alert,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsTopStory = x.IsTopStory,
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName
            }).OrderBy(x => x.CountryName).ToList();

            countriesAlert.ForEach(x =>
            {
                x.strStartDate = x.StartDate.Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.EndDate != null ? Convert.ToDateTime(x.EndDate).Date.ToString("MM/dd/yyy") : string.Empty);

            });
            return countriesAlert;
        }
        /// <summary>
        /// add country alert to the database
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddCountryAlert(tblCountryAlert alert, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                alert.CreatedDate = DateTime.Now;
                alert.CreatedBy = userId;
                alert.CustomerId = alert.CustomerId ?? 0;
                alert.CountryName = string.IsNullOrEmpty(alert.CountryName) == true ? string.Empty : alert.CountryName;

                _dbCon.tblCountryAlerts.Add(alert);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// edit country alert in the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditCountryAlert(tblCountryAlert model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCountryAlert alert =
                    _dbCon.tblCountryAlerts.FirstOrDefault(x => x.CountryAlertId == model.CountryAlertId);
                if (alert != null)
                {
                    alert.Alert = model.Alert;
                    alert.CountryId = model.CountryId;
                    alert.CountryName = model.CountryName;
                    alert.StartDate = model.StartDate;
                    alert.EndDate = model.EndDate;
                    alert.UpdatedBy = userId;
                    alert.UpdatedDate = DateTime.Now;
                    alert.IsTopStory = model.IsTopStory;
                    alert.CustomerId = model.CustomerId ?? 0;
                    alert.CustomerName = model.CustomerName;
                }
                _dbCon.Entry(alert).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// delete country alert from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCountryAlert(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblCountryAlerts.FirstOrDefault(x => x.CountryAlertId == id);
                if (alert != null)
                {
                    _dbCon.tblCountryAlerts.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public List<GenericList> GetAllCountries()
        {
            var countries = _dbCon.tblCountries.Select(x => new GenericList
            {
                Key = x.CountryID,
                Value = x.CountryName

            }).OrderBy(x => x.Value).ToList();
            countries.Insert(0, new Model.GenericList {Value="(None)",Key=0 });
            return countries;
        }
        public List<GenericList> GetCountryByCustomer(int customerId)
        {
            var countries = (from cd in _dbCon.tblCustomerDestinations
                     join ca in _dbCon.tblCustomerAddresses on cd.CustomerDestinationID equals ca.CustomerAddressID
                     join c in _dbCon.tblCountries on ca.CountryID equals c.CountryID
                     where ca.CustomerID == customerId && c.IsActive == true && cd.IsActive == 1 && ca.IsActive == 1
                     select new GenericList { Key = ca.CountryID, Value = c.CountryName }).Distinct().OrderBy(x=> x.Value).ToList() ;
            return countries;


        }

        #region  Country CRUD operations
        //12-28-2016 by Danyal Manzoor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public List<CountryListing> GetCountriesListing(string countryName)
        {
            var countries = new List<CountryListing>();
            foreach (CountryListing listing in (_dbCon.tblCountries.Join
                (_dbCon.tblCountryCodes, a => a.CountryCodeID, b => b.CountryCodeID, (a, b) => new { a, b })
                .Where(@t => @t.a.CountryName.Contains(countryName))
                .Select(@t => new CountryListing
                {
                    CountryId = @t.a.CountryID,
                    CountryName = @t.a.CountryName.Trim(),
                    IsoCountryName = @t.b.ISOCountryName.Trim(),
                    IsActive = @t.a.IsActive
                })))
                countries.Add(listing);

            return countries;
        }

        public List<GenericList> GetDateFormatesList()
        {

            var model =
                _dbCon.tblDateFormats.Where(x => x.IsActive)
                    .OrderByDescending(x => x.IsDefaultFormat)
                    .Select(x => new GenericList()
                    {
                        Key = x.DateFormatID,
                        Value = x.DateFormatName.Trim()
                    }).OrderBy(x => x.Value).ToList();
            return model;
        }

        public List<GenericList> GetTimeZonesList()
        {

            var model =
                _dbCon.tblTimeZones.Where(x => x.IsActive)
                    .Select(x => new GenericList()
                    {
                        Key = x.TimeZoneID,
                        Value = x.TimeZoneName.Trim()
                    }).OrderBy(x => x.Value).ToList();
            return model;
        }

        public bool UpdateCountryStatus(int countryId, bool isActive, int userId)
        {
            try
            {
                var tblCountry = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == countryId);
                tblCountry.IsActive = isActive;
                tblCountry.LastUpdated = DateTime.Now;
                tblCountry.LastUpdatedByUserID = userId;
                _dbCon.Entry(tblCountry).State = System.Data.Entity.EntityState.Modified;
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCountryStatus");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCountryStatus");
                }
            }
            return false;
        }

        public bool AddNewCountry(tblCountry country, int userId)
        {
            try
            {
                country.LastUpdated = DateTime.Now;
                country.LastUpdatedByUserID = userId;
                country.IsActive = true;
                _dbCon.tblCountries.Add(country);
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewCountry");
                }
                return false;
            }
        }

        public bool UpdateCountry(tblCountry country, int userId)
        {
            try
            {
                var getCountry = new tblCountry();
                getCountry = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == country.CountryID);
                if (getCountry != null)
                {
                    getCountry.CountryName = country.CountryName ?? getCountry.CountryName;
                    getCountry.DefaultTimeZoneID = country.DefaultTimeZoneID;
                    getCountry.DefaultDateFormatID = country.DefaultDateFormatID;
                    getCountry.IsActive = country.IsActive;
                    getCountry.LastUpdated = DateTime.Now;
                    getCountry.LastUpdatedByUserID = userId;
                    _dbCon.Entry(getCountry).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                    return true;
                }
                return false;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCountry");
                }
                return false;
            }
        }

        public List<GenericList> GetIsoCountryListToAdd()
        {
            try
            {
                var getExistingCountryCodes = _dbCon.tblCountries.Select(x => x.CountryCodeID).ToList();
                var model =
                    _dbCon.tblCountryCodes.Where(x => !getExistingCountryCodes.Contains(x.CountryCodeID))
                        .Select(x => new GenericList()
                        {
                            Key = x.CountryCodeID,
                            Value = x.ISOCountryName.Trim()
                        }).OrderBy(x => x.Value).ToList();

                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCountryListToAdd");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountryListToAdd");
                }

            }
            return new List<GenericList>();
        }

        public tblCountry GetCountryById(int countryId)
        {
            var country = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == countryId);

            return country;
        }


        public string GetUserNameByUserId(int userId)
        {
            var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId);
            if (user != null) return user.UserName.Trim();
            return "";
        }

        public string GetIsoCountryNameByCountryCode(int countryCodeId)
        {
            var IsoCountryName = _dbCon.tblCountryCodes.FirstOrDefault(x => x.CountryCodeID == countryCodeId);
            return IsoCountryName.ISOCountryName.Trim();
        }

        public List<CountryWorkShiftsModel> GetWorkShiftsByCountry(int countryId)
        {
            var workShifts =
                _dbCon.tblCountryDefaultWorkshifts.Where(x => x.CountryID == countryId).ToList();

            var data = workShifts.Select(x => new CountryWorkShiftsModel()
                    {
                        CountryId = x.CountryID,
                        StartTime = x.StartTime.ToString("HH:mm"),
                        EndTime = x.EndTime.ToString("HH:mm"),
                        ShiftId = x.ShiftID,
                        WeekDayId = x.DayOfWeek
                    }).OrderBy(x => x.WeekDayId).ToList();

            return data;
        }

        public bool UpdateCountryWorkshift(CountryWorkShiftsModel workShift, int userId)
        {
            try
            {
                var startDateHour = int.Parse(workShift.StartTime.Split(':')[0]);
                var startDateMinute = int.Parse(workShift.StartTime.Split(':')[1]);
                var endDateHour = int.Parse(workShift.EndTime.Split(':')[0]);
                var endDateMinutes = int.Parse(workShift.EndTime.Split(':')[1]);

                var startDateTime = DateTime.Now.Date;
                var endDateTime = DateTime.Now.Date;

                var startTime = TimeSpan.Parse(startDateHour + ":" + startDateMinute + ":00");
                startDateTime = startDateTime + startTime;

                var endTime = TimeSpan.Parse(endDateHour + ":" + endDateMinutes + ":00");
                endDateTime = endDateTime + endTime;

                var data = _dbCon.tblCountryDefaultWorkshifts.FirstOrDefault(x => x.ShiftID == workShift.ShiftId);

                if (data != null)
                {
                    data.DayOfWeek = workShift.WeekDayId;
                    data.CountryID = workShift.CountryId;
                    data.IsActive = true;
                    data.LastUpdate = DateTime.Now;
                    data.LastUpdateUserID = userId;
                    data.StartTime = startDateTime;
                    data.EndTime = endDateTime;
                    _dbCon.Entry(data).State = System.Data.Entity.EntityState.Modified;
                }
                _dbCon.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateCountryWorkshift");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateCountryWorkshift");
                }
                return false;
            }
        }

        public bool AddCountryWorkShifts(CountryWorkShiftsModel workShift, int userId)
        {
            try
            {

                var startDateHour = int.Parse(workShift.StartTime.Split(':')[0]);
                var startDateMinute = int.Parse(workShift.StartTime.Split(':')[1]);
                var endDateHour = int.Parse(workShift.EndTime.Split(':')[0]);
                var endDateMinutes = int.Parse(workShift.EndTime.Split(':')[1]);

                var startDateTime = DateTime.Now.Date;
                var endDateTime = DateTime.Now.Date;

                var startTime = TimeSpan.Parse(startDateHour + ":" + startDateMinute + ":00");
                startDateTime = startDateTime + startTime;

                var endTime = TimeSpan.Parse(endDateHour + ":" + endDateMinutes + ":00");
                endDateTime = endDateTime + endTime;

                var data = new tblCountryDefaultWorkshift();

                if (data != null)
                {
                    data.DayOfWeek = workShift.WeekDayId;
                    data.CountryID = workShift.CountryId;
                    data.IsActive = true;
                    data.LastUpdate = DateTime.Now;
                    data.LastUpdateUserID = userId;
                    data.StartTime = startDateTime;
                    data.EndTime = endDateTime;
                    data.CountryID = workShift.CountryId;

                }
                _dbCon.tblCountryDefaultWorkshifts.Add(data);
                _dbCon.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddCountryWorkShifts");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddCountryWorkShifts");
                }
                return false;
            }
        }

        public bool DeletWorkShiftByShiftId(int shiftId)
        {
            try
            {
                var workShift = _dbCon.tblCountryDefaultWorkshifts.FirstOrDefault(x => x.ShiftID == shiftId);
                _dbCon.tblCountryDefaultWorkshifts.Remove(workShift);
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "DeletWorkShiftByShiftId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeletWorkShiftByShiftId");
                }
                return false;
            }
        }


        /// <summary>
        /// Get ISO Countries
        /// </summary>
        /// <param name="isoCountryName"></param>
        /// <returns></returns>
        public List<IsoCountryListing> GetIsoCountriesListing(string isoCountryName)
        {
            var isoCountries = new List<IsoCountryListing>();
            foreach (IsoCountryListing listing in (_dbCon.tblCountryCodes.Join
                (_dbCon.tblCurrencyCodes, a => a.CurrencyCodeID, b => b.CurrencyCodeID, (a, b) => new { a, b })
                .Where(@t => @t.a.ISOCountryName.Contains(isoCountryName))
                .Select(@t => new IsoCountryListing
                {
                    IsoCountryId = @t.a.CountryCodeID,
                    IsoCountryName = @t.a.ISOCountryName.Trim(),
                    Code = @t.a.ISOCountryCodeA3.Trim(),
                    DefaultCurrency = @t.b.ISOCurrencyName.Trim(),
                    IsActive = @t.a.IsActive
                })))
                isoCountries.Add(listing);

            return isoCountries.OrderBy(x => x.IsoCountryName).ToList();
        }

        public List<GenericList> GetCurrencyCodesList()
        {
            var currencyList = _dbCon.tblCurrencyCodes.Where(x => x.IsActive).Select(x => new GenericList()
            {
                Key = x.CurrencyCodeID,
                Value = x.ISOCurrencyName.Trim()
            }).OrderBy(x => x.Value).ToList();

            return currencyList;
        }

        public bool UpdateIsoCountry(tblCountryCode isoCountry, int userId)
        {
            try
            {
                var getIsoCountry = _dbCon.tblCountryCodes.FirstOrDefault(x => x.CountryCodeID == isoCountry.CountryCodeID);
                if (getIsoCountry != null)
                {
                    getIsoCountry.ISOCountryName = getIsoCountry.ISOCountryName ?? isoCountry.ISOCountryName;
                    getIsoCountry.ISOCountryCodeA2 = isoCountry.ISOCountryCodeA2;
                    getIsoCountry.ISOCountryCodeA3 = isoCountry.ISOCountryCodeA3;
                    getIsoCountry.ISOCountryCodeNumber = isoCountry.ISOCountryCodeNumber;
                    getIsoCountry.CurrencyCodeID = isoCountry.CurrencyCodeID;
                    getIsoCountry.IsActive = getIsoCountry.IsActive;
                    getIsoCountry.LastUpdated = DateTime.Now;
                    getIsoCountry.LastUpdatedByUserID = userId;
                    _dbCon.Entry(getIsoCountry).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                    return true;
                }
                return false;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateIsoCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateIsoCountry");
                }
                return false;
            }
        }

        public bool AddNewIsoCountry(tblCountryCode isoCountry, int userId)
        {
            try
            {
                isoCountry.LastUpdated = DateTime.Now;
                isoCountry.LastUpdatedByUserID = userId;
                isoCountry.IsActive = true;
                _dbCon.tblCountryCodes.Add(isoCountry);
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewIsoCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewIsoCountry");
                }
                return false;
            }
        }

        public bool UpdateIsoCountry(int isoCountry, bool isActive, int userId)
        {
            try
            {
                var tblIsoCountry = _dbCon.tblCountryCodes.FirstOrDefault(x => x.CountryCodeID == isoCountry);
                tblIsoCountry.IsActive = isActive;
                tblIsoCountry.LastUpdated = DateTime.Now;
                tblIsoCountry.LastUpdatedByUserID = userId;
                _dbCon.Entry(tblIsoCountry).State = System.Data.Entity.EntityState.Modified;
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateIsoCountry");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateIsoCountry");
                }
            }
            return false;
        }


        public tblCountryCode GetIsoCountryById(int isoCountryId)
        {
            var isoCountry = _dbCon.tblCountryCodes.FirstOrDefault(x => x.CountryCodeID == isoCountryId);

            return isoCountry;
        }

        #endregion
    }
}

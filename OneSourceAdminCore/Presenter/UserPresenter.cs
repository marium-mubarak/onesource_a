﻿using System.Linq;
using OneSourceAdminCore.Entity;
using System.Collections.Generic;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Feb 04 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Login Authentic User"
    /// </summary>
    public class UserPresenter
    {
        /// <summary>
        /// authenticate user by given username and password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        globalserveEntities dbCon;
        public UserPresenter()
        {
            dbCon = new globalserveEntities();
        }
        //public tblUser LoginAdmin(string userName, string password)
        //{
        //    int[] userGroupId = { 4, 5, 13 };
        //    var users =
        //        dbCon.tblUsers.FirstOrDefault(x => x.UserName == userName && x.UserPassword == password && userGroupId.Contains(x.UserGroupID));
        //    return users;
        //}
        public sp_getUserInfo_AdminR_Result LoginAdmin(string userName, string password)
        {
            var userInfo = dbCon.sp_getUserInfo_AdminR(userName, password).FirstOrDefault();
            if (userInfo != null)
            {
                if (userInfo.UserGroupName.ToUpper() == "ISD")
                {
                    var isdUser = dbCon.tblISDUsers.FirstOrDefault(x => x.UserID == userInfo.UserID);
                    if (isdUser != null)
                    {
                        var primeContractor = dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorID == isdUser.PrimeContractorID);
                        if (primeContractor != null)
                        {
                            if (primeContractor.IsActive == 1)
                            {
                                return userInfo;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (userInfo.UserGroupName.ToLower() == "manufacturer")
                {
                    var mfgUser = dbCon.tblMFGUsers.FirstOrDefault(x => x.UserID == userInfo.UserID);
                    if (mfgUser != null)
                    {
                        var manufacturer = dbCon.tblManufacturers.FirstOrDefault(x => x.ManufacturerID == mfgUser.ManufacturerID);
                        if (manufacturer != null)
                        {
                            if (manufacturer.IsActive)
                            {
                                return userInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (userInfo.UserGroupName.ToLower() == "customer")
                {
                    var customerUser = dbCon.tblCustomerUsers.FirstOrDefault(x => x.UserID == userInfo.UserID);
                    if (customerUser != null)
                    {
                        var customer = dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerUser.CustomerID);
                        if (customer != null)
                        {
                            if (customer.IsActive)
                            {
                                return userInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (userInfo.UserGroupName.ToLower() == "sfa")
                {
                    var sfaUser = dbCon.tblSFAUsers.FirstOrDefault(x => x.UserID == userInfo.UserID);
                    if (sfaUser != null)
                    {
                        var sfa = dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == sfaUser.SFAID);
                        if (sfa != null)
                        {
                            if (sfa.IsActive)
                            {
                                return userInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            return userInfo;
        }

        /// <summary>
        /// get contact information by userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public tblContact GetContactInfo(int userId)
        {

            tblContact contact = dbCon.tblContacts.Where(x => x.UserID == userId).FirstOrDefault();
            if (contact != null && !string.IsNullOrEmpty(contact.ContactID.ToString()))
            {
                return contact;
            }
            else
            {
                return null;
            }
        }


        public sp_getPrimeContractorInfo_AdminR_Result GetPrimeContractorInfo(int userId)
        {
            sp_getPrimeContractorInfo_AdminR_Result data = dbCon.sp_getPrimeContractorInfo_AdminR(userId).FirstOrDefault();
            return data;
        }

        public tblPrimeContractorserviceLocation GetPrimeContractorserviceLocationInfo(int primeContractorLocationID)
        {
            return dbCon.tblPrimeContractorserviceLocations.Where(x => x.PrimeContractorLocationID == primeContractorLocationID).FirstOrDefault();
        }

        /// <summary>
        /// get customer's user detail
        /// </summary>
        /// <returns></returns>
        public OneStreetCustomerUserModel GetCustomerUserSetting()
        {

            OneStreetCustomerUserModel model = new OneStreetCustomerUserModel();
            CustomerPresenter cp = new CustomerPresenter();
            model.OneStreetUserInfo = dbCon.sp_GetOneStreetUserInfo_R().OrderBy(x => x.CustomerName).ToList();
            model.Customers = cp.GetCustomers();
            return model;
        }

        /// <summary>
        /// get customer's user
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public List<sp_GetCustomerUser_R_Result> GetUserByCustomer(int customerID)
        {
            return dbCon.sp_GetCustomerUser_R(customerID).OrderBy(x => x.UserName).ToList();
        }

        /// <summary>
        /// update the customer's user setting
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int UpdateCustomerUserSetting(tblOneStreetUser user)
        {
            int count = 0;
            using (var transaction = dbCon.Database.BeginTransaction())
            {
                if (user.UserID != 0)
                {
                    tblOneStreetUser data =
                    dbCon.tblOneStreetUsers.FirstOrDefault(x => x.CustomerID == user.CustomerID && x.UserID == user.UserID);
                    if (data != null)
                    {
                        data.OnstreetAccess = user.OnstreetAccess;
                        dbCon.Entry(data).State = System.Data.Entity.EntityState.Modified;
                        count = dbCon.SaveChanges();
                    }
                    else
                    {
                        tblOneStreetUser data1 =
                       dbCon.tblOneStreetUsers.FirstOrDefault(x => x.CustomerID == user.CustomerID && x.UserID == 0);
                        if (data1 != null)
                        {
                            dbCon.tblOneStreetUsers.Remove(data1);
                        }

                        dbCon.tblOneStreetUsers.Add(user);
                        count = dbCon.SaveChanges();
                    }
                }
                else
                {
                    List<tblOneStreetUser> data =
                        dbCon.tblOneStreetUsers.Where(x => x.CustomerID == user.CustomerID).ToList();

                    dbCon.tblOneStreetUsers.RemoveRange(data);
                    dbCon.tblOneStreetUsers.Add(user);
                    count = dbCon.SaveChanges();
                }
                transaction.Commit();

            }
            return count;
        }
        public object GetSelectedCustomerUser(int customerID, int userID)
        {

            if (userID == 0)
            {
                var query = (from u in dbCon.tblUsers
                             join ud in dbCon.tblOneStreetUsers on u.UserID equals ud.UserID
                             where ud.CustomerID == customerID & ud.OnstreetAccess == true
                             select new { u.UserID, ud.OnstreetAccess, u.UserName }).OrderBy(x => x.UserName).ToList();
                return query;
            }
            else
            {
                var a = dbCon.tblOneStreetUsers.FirstOrDefault(x => x.CustomerID == customerID && x.UserID == 0);
                if (a != null)
                    return "All";
                else
                    return "AllowToAdd";
            }

        }

        public List<tblUser> GetUsers()
        {
            return dbCon.tblUsers.ToList();

        }

        public sp_getSFAInfo_AdminR_Result GetSFAInfo(int userId)
        {
            return dbCon.sp_getSFAInfo_AdminR(userId).FirstOrDefault();
        }
        public tblSFA GetSFA(int sfaID)
        {
            return dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == sfaID);
        }
        public string GetUserHomePage(int userId)
        {
            //NOTE: modified to only return "AdminDashboard-Index" as default home page as per discussion with Danyal

            //var Preference = dbCon.tblPreferences.FirstOrDefault(x => x.PreferenceName == "HomePage");
            //if (Preference != null)
            //{
            //    var userPreference = dbCon.tblUserPreferences.FirstOrDefault(x => x.UserID == userId && x.PreferenceID == Preference.PreferenceID);
            //    if (userPreference != null)
            //    {
            //        if(string.IsNullOrEmpty(userPreference.PreferenceValueRevamp))
            //        {
            //            return "AdminDashboard-Index";
            //        }
            //        var a = userPreference.PreferenceValueRevamp.ToLower();

            //        if (a== "index" || a== "search" || a== "report" || a== "service")
            //        {
            //            return "AdminDashboard-Index";
            //        }
            //        else if(a == "quote")
            //        {
            //            return "QuoteSearch-QuoteSearch";
            //        }
            //        else if(a== "tracking")
            //        {
            //            return "OrderSearch-OrderSearch";
            //        }
            //        else if (a == "request")
            //        {
            //            return "TicketSearch-TicketSearch";
            //        }
            //    }
            //}

            return "AdminDashboard-Index";
        }
    }
}

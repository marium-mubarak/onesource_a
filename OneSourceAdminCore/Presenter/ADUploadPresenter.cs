﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class ADUploadPresenter
    {

        globalserveEntities dbCon;
        public ADUploadPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public List<ADFileModel> GetADFileInfo()
        {
            try
            {
                var ADFileInfo = from up in dbCon.tblADFileUploads_RepEng
                                 join sfa in dbCon.tblSFAs on up.SFAID equals sfa.SFAID
                                 join cus in dbCon.tblCustomers on up.CustomerID equals cus.CustomerID
                                 join con in dbCon.tblContacts on up.UploadedByUserID equals con.UserID
                                 //orderby sfa.SFAName 
                                 orderby up.Uploaded_Date descending
                                 select new ADFileModel { FileID = up.FileID, CustomerName = cus.CustomerName, SFAName = sfa.SFAName, Representative = con.FirstName + " " + con.LastName, Uploaded_Date = up.Uploaded_Date, ADPickup_Date = up.ADPickup_Date, Status = up.Status, TicketID = up.TicketID };

                return ADFileInfo.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetADFileInfo");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetADFileInfo");
                }
                return null;
            }
        }

        public List<ADFileDetailInfo> GetFileDataByFileID(int FileID)
        {
            try
            {
                var FileData = from ADData in dbCon.tblADDetails_RepEng
                               where ADData.FileID == FileID
                               select new ADFileDetailInfo  { UnitID = ADData.UnitID, Manufacturer = ADData.Manufacturer, Model = ADData.Model, Category = ADData.Category, CPU = ADData.CPU, Screen_Size = ADData.Screen_Size, Status = ADData.Status, RAM = ADData.RAM, HDD = ADData.HDD, Processing_Fees = ADData.Processing_Fees, AR_Value = ADData.AR_Value };

                return FileData.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetFileDataByFileID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetFileDataByFileID");
                }
                return null;
            }
        }

        public List<GenericList> GetSuppliersByCustomerID(int CustomerID)
        {
            try
            {
                var Suppliers = (from sfa in dbCon.tblSFAs
                                 join tck in dbCon.tblTickets on sfa.SFAID equals tck.SFAID
                                 join ar in dbCon.tblActionRequests on tck.ARID equals ar.ARID
                                 join cus in dbCon.tblCustomers on ar.CompanyID equals cus.ParentCompanyID
                                 where cus.CustomerID == CustomerID && sfa.IsActive == true
                                 select new GenericList { Key = sfa.SFAID, Value = sfa.SFAName }).Distinct().OrderBy(n => n.Value).ToList();

                return Suppliers;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSuppliersByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSuppliersByCustomerID");
                }
                return null;
            }
        }

        public List<GenericList> GetTicketNumbers(int CustomerID, int SupplierID)
        {
            try
            {
                //Make query to fetch ticket numbers against given CustomerID, SupplierID and CountryID
                var TicketNOs = (from tck in dbCon.tblTickets
                                join ar in dbCon.tblADFileUploads_RepEng on tck.TicketID equals ar.TicketID
                                where ar.CustomerID == CustomerID && ar.SFAID == SupplierID
                                select tck).ToList();

                var result = TicketNOs.Select(x => new GenericList { Key = x.TicketID, Value = x.TicketID.ToString() });

                return result.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTicketNumbers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketNumbers");
                }
                return null;
            }
        }

        public List<ADFileDetailInfo> GetADFileData(int SFAID, int CustomerID, int TicketID)
        {
            try
            {
                //Make query to fetch FileID against selected customer, supplier and TicketID
                var FileInfo = (from AD in dbCon.tblADFileUploads_RepEng
                                where AD.SFAID == SFAID && AD.CustomerID == CustomerID && AD.TicketID == TicketID
                                select AD).FirstOrDefault();

                if (FileInfo != null)
                {
                    var FileData = (from ADData in dbCon.tblADDetails_RepEng
                                   where ADData.FileID == FileInfo.FileID
                                   select new ADFileDetailInfo  { UnitID = ADData.UnitID, Manufacturer = ADData.Manufacturer, Model = ADData.Model, Category = ADData.Category, CPU = ADData.CPU, Screen_Size = ADData.Screen_Size, Status = ADData.Status, RAM = ADData.RAM, HDD = ADData.HDD, Processing_Fees = ADData.Processing_Fees, AR_Value = ADData.AR_Value }).AsEnumerable();

                    return FileData.ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetADFileData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetADFileData");
                }
                return null;
            }
        }

        public List<GenericList> GetTicketNumbersForUpload(int CustomerID, int SupplierID)
        {
            try
            {
                var TicketNOs = (from tck in dbCon.tblTickets
                                 join ar in dbCon.tblActionRequests on tck.ARID equals ar.ARID
                                 join cus in dbCon.tblCustomers on ar.CompanyID equals cus.ParentCompanyID
                                 join sfa in dbCon.tblSFAs on tck.SFAID equals sfa.SFAID
                                 join ca in dbCon.tblCustomerAddresses on ar.AddressID equals ca.CustomerAddressID
                                 where cus.CustomerID == CustomerID && sfa.SFAID == SupplierID && tck.IsActive == true
                                 orderby tck.TicketID
                                 select tck).ToList();

                var result = TicketNOs.Select(x=>new GenericList { Key = x.TicketID, Value = x.TicketID.ToString() });

                return result.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTicketNumbersForUpload");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketNumbersForUpload");
                }
                return null;
            }
        }

        public List<GenericList> GetOperatingCompanies(int CustomerID)
        {
            try
            {
                var OpCOs = from cus in dbCon.tblCustomers
                            join pc in dbCon.tblParentCompanies on cus.ParentCompanyID equals pc.ParentCompanyID
                            join cus2 in dbCon.tblCustomers on pc.ParentParentCompanyID equals cus2.ParentCompanyID
                            where cus2.CustomerID == CustomerID
                            orderby cus.CustomerName
                            select new GenericList { Key = cus.CustomerID, Value = cus.CustomerName };

                return OpCOs.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOperatingCompanies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOperatingCompanies");
                }
                return null;
            }
        }

        public List<GenericStringList> GetBusinessSegment(int CustomerID)
        {
            try
            {
                var BS = (from ca in dbCon.tblCustomerAddresses
                          where ca.CustomerID == CustomerID && ca.IsActive.Equals(1) && ca.BuisnessSegment != string.Empty && ca.BuisnessSegment != null
                          select new GenericStringList { Key = ca.BuisnessSegment, Value = ca.BuisnessSegment }).Distinct().ToList();

                return BS;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetBusinessSegment");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetBusinessSegment");
                }
                return null;
            }
        }

        public bool AddCategory(int OSCategoryID, string SupplierCategory)
        {
            try
            {
                string[] categories = SupplierCategory.Split(',');
                foreach (var category in categories)
                {
                    if (category.Trim() != "")
                    {
                        tblADCategoryMap_RepEng ADCat = new tblADCategoryMap_RepEng();
                        //Assign values to entity column               
                        ADCat.OSCategoryID = OSCategoryID;
                        ADCat.SupplierCategory = category;

                        //Call AddObject method of entity class and pass its object to it
                        dbCon.tblADCategoryMap_RepEng.Add(ADCat);

                        //Call SaveChanges method to save changes into database
                        dbCon.SaveChanges();
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddCategory");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddCategory");
                }
                return false;
            }
        }

        public bool SaveFileDataToDb(ADUploadModel model, int UserId)
        {
            try
            {
                int FileID = 0;
                tblADFileUploads_RepEng ADFileInfo = new tblADFileUploads_RepEng();

                //Check if file againt selected customer, supplier and ticket is already uploaded
                //Make query to fetch the FileID from tblADFileUploads_RepEng
                var customerId = Convert.ToInt32(model.CustomerId);
                var ExstngFile = from FileUp in dbCon.tblADFileUploads_RepEng
                                 where FileUp.CustomerID == customerId && FileUp.SFAID == model.SupplierId
                                 && FileUp.TicketID == model.TicketId
                                 select new { FileUp.FileID };

                if (ExstngFile.Count() > 0)
                {
                    //Get FileID in variable
                    FileID = ExstngFile.FirstOrDefault().FileID;

                    //Fetch records against FileID
                    var FileRecs = (from AD in dbCon.tblADDetails_RepEng
                                    where AD.FileID == FileID
                                    select AD).ToList();

                    //Loop to iterate through the records
                    foreach (var recs in FileRecs)
                    {
                        //Delete record
                        dbCon.tblADDetails_RepEng.Remove(recs);
                        //Save changes in database
                        dbCon.SaveChanges();
                    }

                    //Make query to fetch file info against FileID
                    var FileInfo = (from FileUp in dbCon.tblADFileUploads_RepEng
                                    where FileUp.FileID == FileID
                                    select FileUp).ToList().FirstOrDefault();

                    FileInfo.Uploaded_Date = DateTime.Today;
                    FileInfo.ADPickup_Date = DateTime.Parse(model.ADPickUpDate);
                    FileInfo.UploadedByUserID = UserId;
                    dbCon.SaveChanges();
                }

                else
                {
                    //Else enter new file record in database

                    //Assign values to entity columns
                    ADFileInfo.File_Version = int.Parse(model.VersionNumber.ToString());
                    ADFileInfo.File_Name = model.FileName.ToString();
                    ADFileInfo.Status = "Completed Successfully";
                    ADFileInfo.SFAID = model.SupplierId;
                    ADFileInfo.CustomerID = int.Parse(model.CustomerId);
                    ADFileInfo.TicketID = model.TicketId;
                    ADFileInfo.UploadedByUserID = UserId;
                    ADFileInfo.Uploaded_Date = DateTime.Today;
                    ADFileInfo.ADPickup_Date = DateTime.Parse(model.ADPickUpDate);
                    if (model.BusinessSegment != null)
                    {
                        ADFileInfo.BusinessSegment = model.BusinessSegment.ToString().Trim();
                    }

                    //Call AddObject method of entity class and pass its object to it
                    dbCon.tblADFileUploads_RepEng.Add(ADFileInfo);

                    //Call SaveChanges method to save changes into database
                    var updatedRec = dbCon.SaveChanges();

                    //Check if changes have been saved successfully
                    if (updatedRec > 0)
                    {
                        //Make query to fetch the last FileID from tblADFileUploads_RepEng
                        var File = (from FileUp in dbCon.tblADFileUploads_RepEng
                                    select new { FileUp.FileID }).ToList().Last();

                        FileID = File.FileID;
                    }
                }

                //Insert records into tblADDetails_RepEng table

                //Iterate through excel dataset to insert record into database
                foreach (DataRow row in model.UploadDataTable.Rows)
                {
                    //Create and instantiate of Entity(table) class
                    tblADDetails_RepEng ADDetailInfo = new tblADDetails_RepEng();

                    //Assign values to entity columns
                    ADDetailInfo.FileID = FileID;
                    ADDetailInfo.CustomerID = int.Parse(model.CustomerId);
                    ADDetailInfo.SFAID = model.SupplierId;
                    ADDetailInfo.UnitID = int.Parse(row["Unit_ID"].ToString().Trim());
                    ADDetailInfo.Manufacturer = row["Manufacturer"].ToString().Trim();
                    ADDetailInfo.Model = row["Model"].ToString().Trim();
                    ADDetailInfo.Category = row["Category"].ToString().Trim();
                    ADDetailInfo.CPU = row["CPU"].ToString().Trim();
                    ADDetailInfo.Screen_Size = row["Screen_Size"].ToString().Trim();
                    ADDetailInfo.Status = row["Status"].ToString().Trim();
                    ADDetailInfo.RAM = row["RAM"].ToString().Trim();
                    ADDetailInfo.HDD = row["HDD"].ToString().Trim();
                    ADDetailInfo.Processing_Fees = decimal.Parse(row["Processing_Fees"].ToString().Trim());

                    //Check if AR value column has no value
                    if (row["AR_Value"].ToString().Trim().Length < 1)
                    {
                        //Then put zero in table column
                        ADDetailInfo.AR_Value = 0;
                    }
                    else
                    {
                        //Else put its value in table column
                        ADDetailInfo.AR_Value = decimal.Parse(row["AR_Value"].ToString().Trim());
                    }
                    ADDetailInfo.Status = row["Status"].ToString().Trim();
                    //Call AddObject method of entity class and pass its object to it
                    dbCon.tblADDetails_RepEng.Add(ADDetailInfo);

                    //Call SaveChanges method to save changes into database
                    dbCon.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "SaveFileDataToDb");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "SaveFileDataToDb");
                }
                return false;
            }
        }

        public List<GenericList> GetOSCategories()
        {
            try
            {
                var PartCategories = (from pc in dbCon.tblPartCategories
                                      where pc.IsActive == true
                                      orderby pc.PartCategoryName
                                      select new GenericList { Key = pc.PartCategoryID, Value = pc.PartCategoryName }).Distinct().ToList();
                return PartCategories;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOSCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOSCategories");
                }
                return null;
            }
        }

        public List<string> MapCategoriesAndGetUnmappedCategories(DataTable dt)
        {
            try
            {
                string suppCat = string.Empty;
                List<string> suppCategories = new List<string>();
                List<string> DistnctSuppCategories = new List<string>();
                DataView view = new DataView(dt);
                DataTable tbl = view.ToTable(true, "Category");

                foreach (DataRow row in tbl.Rows)
                {
                    //Get category
                    suppCat = row["Category"].ToString().Trim();

                    //Make query to fetch category mapping info against current catgeory
                    var SupplierCategories = (from ADCatMap in dbCon.tblADCategoryMap_RepEng
                                              where ADCatMap.SupplierCategory == suppCat
                                              select ADCatMap).ToList();

                    //Check if category mapping data is not found
                    if (SupplierCategories.Count() == 0)
                    {
                        //Query to check if supplier category already exists in onesource
                        var OneSourceCat = (from PartCat in dbCon.tblPartCategories
                                            where PartCat.PartCategoryName == suppCat && PartCat.IsActive == true
                                            select PartCat).ToList();

                        //Check if record is found
                        if (OneSourceCat.Count > 0)
                        {
                            //Get OneSource CategoryID in variable
                            int OneSourceCatID = OneSourceCat.FirstOrDefault().PartCategoryID;

                            //Insert record in tblADCategoryMap_RepEng table
                            //Create and instantiate of Entity(table) class
                            tblADCategoryMap_RepEng ADCatMap = new tblADCategoryMap_RepEng();

                            //Assign values to entity columns
                            ADCatMap.OSCategoryID = OneSourceCatID;
                            ADCatMap.SupplierCategory = suppCat;

                            //Call AddObject method of entity class and pass its object to it
                            dbCon.tblADCategoryMap_RepEng.Add(ADCatMap);

                            //Call SaveChanges method to save the changes into database
                            dbCon.SaveChanges();
                        }
                        else
                        {
                            //Else add it in supplier category list
                            suppCategories.Add(row["Category"].ToString());
                        }
                    }
                }

                //Get the distinct supplier categories fron list
                suppCategories = suppCategories.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();
                return suppCategories;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "MapCategoriesAndGetUnmappedCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "MapCategoriesAndGetUnmappedCategories");
                }
                return null;
            }
        }


        public int getFileVersion(int SupplierID, int CustomerID)
        {
            try
            {
                int fileVer = 0;
                var FileVer = (from FileUp in dbCon.tblADFileUploads_RepEng
                               where FileUp.SFAID == SupplierID && FileUp.CustomerID == CustomerID
                               orderby FileUp.FileID descending
                               select new { FileUp.File_Version }).FirstOrDefault();

                //Check if no record exists
                if (FileVer == null)
                {
                    //Assign 1 to file version since its first file
                    fileVer = 1;
                }
                else
                {
                    //Else add 1 in current version no
                    fileVer = FileVer.File_Version + 1;
                }

                //Return file version
                return fileVer;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "getFileVersion");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "getFileVersion");
                }
                return 0;
            }
        }

    }
}

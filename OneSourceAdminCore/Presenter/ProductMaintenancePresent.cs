﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This presenter Class is responsible for product maintenance"
    /// </summary>
    public class ProductMaintenancePresent
    {
        private readonly globalserveEntities _dbCon;
        public ProductMaintenancePresent()
        {
            _dbCon = new globalserveEntities();
        }

        #region partcategories

        /// <summary>
        /// this method is responsible for providing part categories view
        /// </summary>
        /// <returns></returns>
        public List<sp_getPartCategories_AdminR_Result> GetPartCategories()
        {
            return _dbCon.sp_getPartCategories_AdminR().ToList();
        }

        /// <summary>
        /// this method is responsible for providing part category by ID
        /// </summary>
        /// <param name="PCId"></param>
        /// <returns></returns>
        public PartCategoryModel GetPartCategoryInfo(int PCId)
        {
            PartCategoryModel obj = new PartCategoryModel();
            obj.PCId = PCId;

            obj.Companies = _dbCon.tblParentCompanies.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Value = x.ParentCompanyName,
                Key = x.ParentCompanyID
            }).ToList();
            obj.Companies.Insert(0, new GenericList { Value = "(None)", Key = 0 });

            if (PCId > 0)
            {
                var PartCategories = _dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == PCId);
                if (PartCategories != null)
                {
                    obj.IsActive = PartCategories.IsActive;
                    obj.LastUpdated = PartCategories.LastUpdated;
                    obj.PartCategoryName = PartCategories.PartCategoryName;
                    obj.CompanyId = PartCategories.CompanyID;
                    var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == PartCategories.LastUpdatedByUserID);
                    if (user != null)
                    {
                        obj.UserName = user.UserName;
                    }
                }

            }
            return obj;

        }

        /// <summary>
        /// this method is responsible for add edit part category
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object AddUpdatePartCategories(tblPartCategory model, int userId)
        {
            int count = 0;
            string msg = string.Empty;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    if (!string.IsNullOrEmpty(model.PartCategoryName))
                    {
                        var ExistingRecord = _dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryName.ToLower() == model.PartCategoryName.ToLower());
                        if (ExistingRecord != null)
                        {
                            if (model.PartCategoryID != ExistingRecord.PartCategoryID)
                            {
                                return new
                                {
                                    count = 0,
                                    msg = "A Part Category with this name is already exists"
                                };
                            }
                            ((IObjectContextAdapter)_dbCon).ObjectContext.Detach(ExistingRecord);
                        }

                    }


                    model.LastUpdated = DateTime.Now;
                    model.LastUpdatedByUserID = userId;
                    model.CompanyID = model.CompanyID == 0 ? null : model.CompanyID;

                    if (model.PartCategoryID > 0)
                    {
                        var data = model;

                        _dbCon.Entry(data).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Part Category Updated Successfully" : "Error Occured.";
                    }
                    else
                    {
                        model.IsActive = true;
                        _dbCon.tblPartCategories.Add(model);
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Part Category Added Successfully" : "Error Occured.";
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdatePartCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdatePartCategories");
                }
            }
            return new
            {
                count = count,
                msg = msg
            };
        }
        #endregion

        #region Freight Carriers

        /// <summary>
        ///  this method is responsible for providing Freight Carriers
        /// </summary>
        /// <returns></returns>
        public List<tblFreightCarrier> FreightCarriers()
        {
            return _dbCon.tblFreightCarriers.Distinct().ToList();
        }

        /// <summary>
        /// this method is responsible for providing specific FreightCarrier
        /// </summary>
        /// <param name="FCId"></param>
        /// <returns></returns>
        public FreightCarieerModel GetFreightCarriersInfo(int FCId)
        {
            FreightCarieerModel obj = new Model.FreightCarieerModel();
            obj.FreightCarrier = new tblFreightCarrier();
            if (FCId > 0)
            {
                var FreightCarrier = _dbCon.tblFreightCarriers.FirstOrDefault(x => x.FreightCarrierID == FCId);
                if (FreightCarrier != null)
                {
                    obj.FreightCarrier = FreightCarrier;
                    var user = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == FreightCarrier.LastUpdatedByUserID);
                    if (user != null)
                    {
                        obj.UserName = user.UserName;
                    }
                }

            }
            return obj;

        }

        /// <summary>
        /// this method is responsible for Add Edit Freight Carrier
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object AddUpdateFreightCarriers(tblFreightCarrier model, int userId)
        {
            int count = 0;
            string msg = string.Empty;
            try
            {
                using (var transaction = _dbCon.Database.BeginTransaction())
                {

                    if (!string.IsNullOrEmpty(model.FreightCarrierName))
                    {
                        var ExistingRecord = _dbCon.tblFreightCarriers.FirstOrDefault(x => x.FreightCarrierName.ToLower() == model.FreightCarrierName.ToLower());
                        if (ExistingRecord != null)
                        {
                            if (model.FreightCarrierID != ExistingRecord.FreightCarrierID)
                            {
                                return new
                                {
                                    count = 0,
                                    msg = "A Freight Carrier with this name is already exists"
                                };
                            }
                            ((IObjectContextAdapter)_dbCon).ObjectContext.Detach(ExistingRecord);
                        }

                    }

                    model.LastUpdated = DateTime.Now;
                    model.LastUpdatedByUserID = userId;

                    if (model.FreightCarrierID > 0)
                    {
                        _dbCon.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Freight Carrier Updated Successfully" : "Error Occured.";
                    }
                    else
                    {
                        model.IsActive = 1;
                        _dbCon.tblFreightCarriers.Add(model);
                        count = _dbCon.SaveChanges();
                        msg = count > 0 ? "Freight Carrier Added Successfully" : "Error Occured.";
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateFreightCarriers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateFreightCarriers");
                }
            }
            return new
            {
                count = count,
                msg = msg
            };
        }

        #endregion

        #region ManufacturerProducts Start

        /// <summary>
        /// this action is responsible for providing active category list
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetActiveCategoryList()
        {
            try
            {
                var cats = _dbCon.tblPartCategories.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.PartCategoryID,
                    Value = x.PartCategoryName.Trim()
                }).OrderBy(x => x.Value).ToList();

                return cats;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActiveCategoryList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActiveCategoryList");
                }

                return new List<GenericList>();
            }
        }

        /// <summary>
        /// this action is responsible for getting manufacturer part list
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Sp_GetManuFacturerParts_AdminR_Result> GetManufPartList(ManufacturerPartsModel model)
        {
            try
            {
                var data = _dbCon.Sp_GetManuFacturerParts_AdminR(model.ManfId, model.CategoryId, model.SearchType,
                    model.SearchTxt, 500).ToList();
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManufPartList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufPartList");
                }

                return new List<Sp_GetManuFacturerParts_AdminR_Result>();
            }


        }

        /// <summary>
        /// Get manf part to edit
        /// </summary>
        /// <param name="manfId"></param>
        /// <returns></returns>
        public tblManufacturerPart GetManfPartById(int manfId)
        {
            try
            {
                var data = _dbCon.tblManufacturerParts.FirstOrDefault(x => x.ManufacturerPartID == manfId);
                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManfPartById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManfPartById");
                }

                return new tblManufacturerPart();
            }
        }


        public List<GenericList> GetSupplierList()
        {
            try
            {
                var sfas = _dbCon.tblSFAs.Where(x => x.IsActive && x.ParentCompanyID != null && x.IsComposite.Value).Select(x => new GenericList()
                {
                    Key = x.SFAID,
                    Value = x.SFAName.Trim()
                }).OrderBy(x => x.Value).ToList();

                return sfas;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSupplierList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierList");
                }

                return new List<GenericList>();
            }
        }

        public List<GenericStringList> GetUnsPsCodesList()
        {
            try
            {
                var data = _dbCon.tblUNSPSCcodes.Where(x => x.UNSPSC.StartsWith("43")).Select(x => new GenericStringList()
                {
                    Key = x.UNSPSC,
                    Value = x.Title.Trim()
                }).OrderBy(x => x.Value).ToList();

                return data;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetUnsPsCodesList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetUnsPsCodesList");
                }

                return new List<GenericStringList>();
            }
        }



        public List<GenericList> GetPriceCodeList()
        {
            try
            {
                var pc = _dbCon.tblPriceCodes.Where(x => x.IsActive == 1).Select(x => new GenericList()
                {
                    Key = x.PriceCodeID,
                    Value = x.Description.Trim()
                }).OrderBy(x => x.Value).ToList();

                return pc;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPriceCodeList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPriceCodeList");
                }

                return new List<GenericList>();
            }
        }
        public List<GenericList> GetAvailabilityList()
        {

            var data = new List<GenericList>();
            data.AddRange(new List<GenericList>()
            {
             new GenericList(){   Key =  1,Value = "Available"},
             new GenericList(){   Key =  2,Value = "Constrained"},
             new GenericList(){   Key =  3,Value = "Severely Constrained"},
            });
            return data;
        }


        public bool UpdateManfPart(tblManufacturerPart data, int userId)
        {
            try
            {
                var tblManf =
                    _dbCon.tblManufacturerParts.FirstOrDefault(x => x.ManufacturerPartID == data.ManufacturerPartID);
                if (tblManf != null)
                {

                    tblManf.ManufacturerSKU = data.ManufacturerSKU;
                    tblManf.ManufacturerID = data.ManufacturerID;
                    tblManf.PartCategoryID = data.PartCategoryID;
                    tblManf.Model = data.Model;
                    tblManf.ImageName = data.ImageName;
                    tblManf.Description = data.Description;
                    tblManf.LongDescription = data.LongDescription;
                    tblManf.IsActive = data.IsActive;
                    tblManf.LastUpdatedByUserID = userId;
                    tblManf.LastUpdated = DateTime.Now;
                    _dbCon.Entry(tblManf).State = System.Data.Entity.EntityState.Modified;
                    _dbCon.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateManfPart");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateManfPart");
                }

                return false;
            }
        }

        public int AddManfPart(tblManufacturerPart data, int userId)
        {
            try
            {
                var tblManf = new tblManufacturerPart();

                tblManf.ManufacturerSKU = data.ManufacturerSKU;
                tblManf.ManufacturerID = data.ManufacturerID;
                tblManf.PartCategoryID = data.PartCategoryID;
                tblManf.Model = data.Model;
                tblManf.PartAvailabilityID = data.PartAvailabilityID;
                tblManf.UNSPSC = data.UNSPSC;
                tblManf.PriceCodeID = data.PriceCodeID;
                tblManf.SFAID = data.SFAID;
                tblManf.Description = data.Description;
                tblManf.IsActive = true;
                tblManf.LastUpdatedByUserID = userId;
                tblManf.LastUpdated = DateTime.Now;

                _dbCon.tblManufacturerParts.Add(tblManf);
                _dbCon.SaveChanges();
                return tblManf.ManufacturerPartID;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddManfPart");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddManfPart");
                }

                return 0;
            }
        }

        public bool CheckManfPartBySku(string sku, int manfId, int manfPartId)
        {
            var chk = _dbCon.tblManufacturerParts.Any(x => x.ManufacturerSKU.ToLower() == sku.ToLower() && x.ManufacturerID == manfId && x.ManufacturerPartID != manfPartId);
            return chk;
        }

        #endregion

    }
}

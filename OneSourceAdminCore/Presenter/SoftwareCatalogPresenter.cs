﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class SoftwareCatalogPresenter
    {
        globalserveEntities dbCon;

        public SoftwareCatalogPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public bool BulkCopyDT(DataTable dt, string fileType)
        {
            try
            {
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlBulkCopy bulkInsert = new SqlBulkCopy(regularConnectionString);
                bulkInsert.DestinationTableName = "tblBulkSoftwareTemp";

                if (fileType == "MPSA")
                {
                    bulkInsert.ColumnMappings.Add("ITEM NUMBER", "ItemNumber");
                    bulkInsert.ColumnMappings.Add("ORDERING PART NAME", "OrderingPartName");
                    bulkInsert.ColumnMappings.Add("PART SHORT NAME", "PartShortName");
                    bulkInsert.ColumnMappings.Add("ITEM GROUP", "ItemGroup");
                    bulkInsert.ColumnMappings.Add("ITEM EDITION", "ItemEdition");
                    bulkInsert.ColumnMappings.Add("VERSION", "Version");
                    bulkInsert.ColumnMappings.Add("RESOURCE USAGE", "ResourceUsage");
                    bulkInsert.ColumnMappings.Add("ITEM TYPE", "ItemType");
                    bulkInsert.ColumnMappings.Add("LANGUAGE", "Language");
                    bulkInsert.ColumnMappings.Add("PURCHASING ACCOUNT TYPE", "PurchasingAccountType");
                    bulkInsert.ColumnMappings.Add("DISTRIBUTION CHANNEL TYPE", "DistributionChannelType");
                    bulkInsert.ColumnMappings.Add("PURCHASE TYPE", "PurchaseType");
                    bulkInsert.ColumnMappings.Add("PURCHASE OPTION", "PurchaseOption");
                    bulkInsert.ColumnMappings.Add("PRICE LEVEL", "PriceLevel");
                    bulkInsert.ColumnMappings.Add("PURCHASE UNIT", "PurchaseUnit");
                    bulkInsert.ColumnMappings.Add("PURCHASE DURATION", "PurchaseDuration");
                    bulkInsert.ColumnMappings.Add("COUNTRY", "Country");
                    bulkInsert.ColumnMappings.Add("PRICING LOCALE", "PricingLocale");
                    bulkInsert.ColumnMappings.Add("PRICING CURRENCY", "PricingCurrency");
                    bulkInsert.ColumnMappings.Add("VALID FROM DATE", "ValidFromDate");
                    bulkInsert.ColumnMappings.Add("VALID TO DATE", "ValidToDate");
                    bulkInsert.ColumnMappings.Add("CURRENT NET PRICE AMOUNT", "CurrentNetPriceAmount");
                    bulkInsert.ColumnMappings.Add("ADD/CHG/END FLAG", "ChangeFlag");
                    bulkInsert.ColumnMappings.Add("PRICE REFERENCE NUMBER", "PriceReferenceNumber");
                    bulkInsert.ColumnMappings.Add("POOL", "Pool");
                    bulkInsert.ColumnMappings.Add("ITEM POINT COUNT", "ItemPointCount");
                    bulkInsert.ColumnMappings.Add("REFERENCE LICENSE AMOUNT", "ReferenceLicenseAmount");
                    bulkInsert.ColumnMappings.Add("ESTIMATED RETAIL PRICE AMOUNT", "EstimatedRetailPriceAmount");
                    bulkInsert.ColumnMappings.Add("PRICE LIST DATE", "PriceListDate");
                    bulkInsert.ColumnMappings.Add("PROMOTION INDICATOR", "PromotionIndicator");
                    bulkInsert.ColumnMappings.Add("PREREQUISITE INDICATOR", "PrerequisiteIndicator");
                    bulkInsert.ColumnMappings.Add("CHANGE REASON", "ChangeReason");
                    bulkInsert.ColumnMappings.Add("PRICELIST ID", "PriceListID");
                    bulkInsert.ColumnMappings.Add("Program", "Program");
                }

                if (fileType == "SelectPlus")
                {
                    //Most of the columns are same as for MPSA
                    //Total 21 columns in which only 5 are unique

                    bulkInsert.ColumnMappings.Add("Price List", "PriceList");
                    bulkInsert.ColumnMappings.Add("ACD Indicator", "ChangeFlag");
                    bulkInsert.ColumnMappings.Add("Part Number", "ItemNumber");
                    bulkInsert.ColumnMappings.Add("Item Name", "PartShortName");
                    bulkInsert.ColumnMappings.Add("Item Legal Name", "OrderingPartName");
                    bulkInsert.ColumnMappings.Add("License Agreement Type", "PurchasingAccountType");
                    bulkInsert.ColumnMappings.Add("License Type Name", "LicenseTypeName");
                    bulkInsert.ColumnMappings.Add("Program", "Program");
                    bulkInsert.ColumnMappings.Add("Offering", "PricingLocale");
                    bulkInsert.ColumnMappings.Add("Level", "PriceLevel");
                    bulkInsert.ColumnMappings.Add("Purchase Period", "PurchaseDuration");
                    bulkInsert.ColumnMappings.Add("Pool", "Pool");
                    bulkInsert.ColumnMappings.Add("Product Family", "ItemGroup");
                    bulkInsert.ColumnMappings.Add("Version", "Version");
                    bulkInsert.ColumnMappings.Add("Product Type", "ItemType");
                    bulkInsert.ColumnMappings.Add("Language", "Language");
                    bulkInsert.ColumnMappings.Add("Unit Count", "ItemPointCount");
                    bulkInsert.ColumnMappings.Add("Current Net Price", "CurrentNetPriceAmount");
                    bulkInsert.ColumnMappings.Add("Estimated Retail Price", "EstimatedRetailPriceAmount");
                    bulkInsert.ColumnMappings.Add("Replaced By Part Number", "ReplacedByPartNumber");
                    bulkInsert.ColumnMappings.Add("Replaces Part Number", "ReplacesPartNumber");
                }

                bulkInsert.WriteToServer(dt);
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "BulkCopyDT");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "BulkCopyDT");
                }

                return false;
            }
        }

        public List<GenericList> GetActiveCustomersList()
        {
            try
            {
                var customerList = dbCon.tblCustomers.Where(x => x.IsActive).Select(x => new GenericList
                {
                    Key = x.CustomerID,
                    Value = x.CustomerName
                }).ToList();
                return customerList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActiveCustomersList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActiveCustomersList");
                }

                return new List<GenericList>();
            }
        }

        public List<GenericList> GetProgramsList()
        {
            try
            {
                var list = dbCon.tblPrograms_Softwares.Select(x => new GenericList
                {
                    Key = x.ProgramID,
                    Value = x.ProgramName
                }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetProgramsList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProgramsList");
                }

                return new List<GenericList>();
            }
        }

        public List<GenericList> GetAgreementTypeList()
        {
            try
            {
                var list = dbCon.TblSoftwareAgreementTypes.Select(x => new GenericList
                {
                    Key = x.AgreementTypeId,
                    Value = x.AgreementType
                }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAgreementTypeList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAgreementTypeList");
                }

                return new List<GenericList>();
            }
        }

        public List<GenericList> GetPoolList()
        {
            try
            {
                var list = dbCon.tblPool_Software.Select(x => new GenericList
                {
                    Key = x.PoolID,
                    Value = x.PoolName
                }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPoolList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPoolList");
                }

                return new List<GenericList>();
            }
        }

        public List<GenericList> GetContractListTypeList()
        {
            try
            {
                var list = dbCon.TblContractType_Software.Select(x => new GenericList
                {
                    Key = x.ContractTypeId,
                    Value = x.ContractTypeName
                }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetContractListTypeList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetContractListTypeList");
                }

                return new List<GenericList>();
            }
        }



        public List<TblConnectionCustomerSetting> GetCustomerSettingListById(int customerId)
        {
            try
            {
                var list = dbCon.TblConnectionCustomerSettings.Where(x => x.GsCustomerId == customerId).ToList();

                return list;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerSettingListById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerSettingListById");
                }

                return new List<TblConnectionCustomerSetting>();
            }
        }

        public SoftwareSettingsModel GetCustomerSettingsById(int settingId)
        {
            try
            {
                if (settingId > 0)
                {
                    var chk = dbCon.TblConnectionCustomerSettings.FirstOrDefault(x => x.Id == settingId);
                    var model = new SoftwareSettingsModel();
                    model.CustomerId = chk.GsCustomerId;
                    model.IsActive = chk.IsActive;
                    model.AgreementNumber = chk.AgreementNumber;
                    model.EnrollmentNumber = chk.EnrollmentNo;
                    model.PcnNumber = chk.PcnNumber;
                    model.PusrchasingAccountNumber = chk.Purchasing_Account_No;
                    model.EffectiveDate = chk.EffectiveDate;
                    model.ExpireDate = chk.ExpireDate;
                    model.ConnectionSourceId = chk.ConnectionsSourceId;
                    model.LegalEntityName = chk.LegalEntityName;
                    model.Source = chk.Source;
                    model.ProgramId = Convert.ToInt32(chk.ProgramId);
                    model.AgreementTypeId = Convert.ToInt32(chk.AgreementTypeId);
                    model.ContractTypeId = Convert.ToInt32(chk.ContractTypeId);
                    model.SettingId = chk.Id;
                    model.AnniversryMonth= chk.AnniversryMonth;
                    return model;
                }

                return new SoftwareSettingsModel();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerSettingsById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerSettingsById");
                }

                return new SoftwareSettingsModel();
            }
        }

        public List<TblPool_PriceLevelMapping> GetPoolSettingByCustomerSettingId(int settingId)
        {
            try
            {
                if (settingId > 0)
                {
                    var chk = dbCon.TblPool_PriceLevelMapping.Where(x => x.CustomerSettingId == settingId).ToList();
                    return chk;
                }

                return new List<TblPool_PriceLevelMapping>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPoolSettingByCustomerSettingId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPoolSettingByCustomerSettingId");
                }


                return new List<TblPool_PriceLevelMapping>();
            }
        }


        public bool AddUpdateConnectionSettings(SoftwareSettingsModel model, AdminSetting currentSettings)
        {
            try
            {
                var customerSettings = new TblConnectionCustomerSetting();

                if (model == null) return false;

                customerSettings.ProgramId = model.ProgramId;
                customerSettings.AgreementTypeId = model.AgreementTypeId;
                customerSettings.ContractTypeId = model.ContractTypeId;
                customerSettings.AgreementNumber = model.AgreementNumber;
                customerSettings.LegalEntityName = model.LegalEntityName;
                customerSettings.ConnectionsSourceId = model.ConnectionSourceId;
                customerSettings.EffectiveDate = model.EffectiveDate;
                customerSettings.ExpireDate = model.ExpireDate;
                customerSettings.Source = model.Source;
                customerSettings.GsCustomerId = model.CustomerId;
                customerSettings.AnniversryMonth = model.AnniversryMonth;
                if (dbCon.tblPrograms_Softwares.FirstOrDefault(x => x.ProgramID == model.ProgramId)
                        ?.ProgramName == "MPSA")
                {
                    customerSettings.Purchasing_Account_No = model.PusrchasingAccountNumber;
                    customerSettings.EnrollmentNo = null;
                    customerSettings.PcnNumber = null;
                }
                else
                {
                    customerSettings.EnrollmentNo = model.EnrollmentNumber;
                    customerSettings.PcnNumber = model.PcnNumber;
                    customerSettings.Purchasing_Account_No = null;
                }




                customerSettings.CreatedBy = currentSettings.UserID;
                customerSettings.CreatedDate = DateTime.Now;

                if (model.SettingId > 0)
                {
                    customerSettings.IsActive = model.IsActive;
                    customerSettings.Id = model.SettingId;
                    if (customerSettings.IsActive)
                    {
                        customerSettings.ActiveStatus = "A";
                    }
                    else
                    {
                        customerSettings.ActiveStatus = "I";
                    }
                   var chkCustomerSettings = dbCon.TblConnectionCustomerSettings.Any(x => x.Id == model.SettingId);
                    if (chkCustomerSettings)
                    {
                        dbCon.Entry(customerSettings).State = System.Data.Entity.EntityState.Modified;
                        dbCon.SaveChanges();

                        AddUpdateCustomerPricePoolSettings(model.PoolSettingList, customerSettings.Id);
                        return true;
                    }
                }
                else
                {
                    customerSettings.IsActive = true;
                    if (customerSettings.IsActive)
                    {
                        customerSettings.ActiveStatus = "A";
                    }
                    else
                    {
                        customerSettings.ActiveStatus = "I";
                    }
                    dbCon.TblConnectionCustomerSettings.Add(customerSettings);
                    dbCon.SaveChanges();

                    AddUpdateCustomerPricePoolSettings(model.PoolSettingList, customerSettings.Id);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateConnectionSettings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateConnectionSettings");
                }

                return false;
            }
        }

        public bool AddUpdateCustomerPricePoolSettings(List<TblPool_PriceLevelMapping> poolSettings, int settingId)
        {
            try
            {
                var getSettings = dbCon.TblPool_PriceLevelMapping.Where(x => x.CustomerSettingId == settingId).ToList();
                if (getSettings.Count > 0)
                {
                    dbCon.TblPool_PriceLevelMapping.RemoveRange(getSettings);
                }

                if (poolSettings.Count > 0)
                {
                    poolSettings.ForEach(x => x.CustomerSettingId = settingId);
                    dbCon.TblPool_PriceLevelMapping.AddRange(poolSettings.Where(x => !string.IsNullOrWhiteSpace(x.PoolLevel) && x.PoolId > 0));
                    dbCon.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateCustomerPricePoolSettings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateCustomerPricePoolSettings");
                }

                return false;
            }
        }

    }
}

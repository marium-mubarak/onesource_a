﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class SoftwareQuotePresenter
    {
        private readonly globalserveEntities _dbCon;
        tblSoftwareQuote _softquote;

        public SoftwareQuotePresenter()
        {
            _dbCon = new Entity.globalserveEntities();
            _softquote = new tblSoftwareQuote();
        }

        #region Properties
        public tblSoftwareQuote SoftwareQuote
        {
            get { return _softquote; }
            set { _softquote = value; }
        }
        #endregion

        #region Software Quotes

        public int Save()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (_softquote.SoftwareQuoteId > 0)
                {
                    _dbCon.Entry(_softquote).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _dbCon.tblSoftwareQuotes.Add(_softquote);
                }

                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;
        }

        public int Delete(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblSoftwareQuotes.FirstOrDefault(x => x.SoftwareQuoteId == id);
                if (data != null)
                {
                    _dbCon.tblSoftwareQuotes.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public List<tblSoftwareQuote> GetSoftwareQuotes()
        {
            return _dbCon.tblSoftwareQuotes.ToList();
        }

        public List<SoftwareQuoteModel> Get()
        {
            var sq = (from S in _dbCon.tblSoftwareQuotes
                      join C in _dbCon.tblCountries on S.CountryId equals C.CountryID
                      //join CR in _dbCon.tblCurrencies on S.CurrencyId equals CR.CurrencyID
                      join O in _dbCon.tblHQOptions on S.OptionId equals O.HQOptionId
                      //join F in _dbCon.tblSFAs on S.SfaId equals F.SFAID
                      select new SoftwareQuoteModel
                      {
                          Attachment1 = S.Attachment1,
                          Attachment2 = S.Attachment2,
                          Attachment3 = S.Attachment3,
                          Attachment4 = S.Attachment4,
                          Attachment5 = S.Attachment5,
                          City = S.City,
                          Comments = S.Comments,
                          Country = C.CountryName,
                          CountryId = S.CountryId,
                          CreatedBy = S.CreatedBy,
                          CreatedDate = S.CreatedDate,
                          //Currency = CR.CurrencyName,
                          CurrencyId = S.CurrencyId,
                          DeliveryAddress = S.DeliveryAddress,
                          InvoiceAddress = S.InvoiceAddress,
                          CustomerName = S.CustomerName,
                          Description = S.Description,
                          ExpectedPricePoint = S.ExpectedPricePoint,
                          OptionId = S.OptionId,
                          HQOption = O.HQOptionName,
                          OptionComment = S.OptionComment,
                          LisensePeriod = S.LisensePeriod,
                          LisenseTypeId = S.LisenseTypeId,
                          AccountManagerName = S.AccountManagerName,
                          SfaId = S.SfaId,
                          ResponseId = S.ResponseId,
                          Manufacturer = S.Manufacturer,
                          ProjectTimeline = S.ProjectTimeline,
                          Quantity = S.Quantity,
                          ResponseDate = S.ResponseDate,
                          ResponsedBy = S.ResponsedBy,
                          SerialNumber = S.SerialNumber,
                          SKU = S.SKU,
                          SoftwareQuoteId = S.SoftwareQuoteId,
                          EndUserAccountDetails = S.EndUserAccountDetails,
                          GlobalDealId = S.GlobalDealId,
                          OEMAccountManager = S.OEMAccountManager,
                          LineItems = _dbCon.tblQuoteLineItems.Where(x => x.QuoteId == S.SoftwareQuoteId && x.QuoteTypeId == 2).Count()
                          //Supplier = F.SFAName,
                          //SupplierEmail = F.Email
                      }).ToList();

            return sq;
        }

        #endregion

    }
}

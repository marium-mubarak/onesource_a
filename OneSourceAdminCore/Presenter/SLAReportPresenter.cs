﻿using System.Linq;
using OneSourceAdminCore.Entity;
using System.Collections.Generic;
using OneSourceAdminCore.Model;
using System;
using System.Data;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Web.Mvc;
using OneSourceAdminCore.Helper;

namespace OneSourceAdminCore.Presenter
{
    public class SLAReportPresenter
    {
        globalserveEntities dbCon;
        public SLAReportPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public DataTable GetReportData(string reportId, string reportName, string reportType, SLAReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_SLAReportMain_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 1000;

                    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@ReportId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@ReportType", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@OEMList", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@ProductCategoryIDs", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@BusinessSegments", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@RegionIDs", SqlDbType.Text));

                    command.Parameters[0].Value = reportName;
                    command.Parameters[1].Value = reportId;
                    command.Parameters[2].Value = reportType;
                    command.Parameters[3].Value = (model.CustomerIds != null && model.CustomerIds.ToString() != "") ? model.CustomerIds : "0";
                    command.Parameters[4].Value = model.StartDate;
                    command.Parameters[5].Value = model.EndDate;
                    command.Parameters[6].Value = (model.ManufacturerIds != null && model.ManufacturerIds.ToString() != "") ? model.ManufacturerIds : "0";
                    command.Parameters[7].Value = (model.CountryIds != null && model.CountryIds.ToString() != "") ? model.CountryIds : "0";
                    command.Parameters[8].Value = (model.CategoryIds != null && model.CategoryIds.ToString() != "") ? model.CategoryIds : "0";
                    command.Parameters[9].Value = (model.BusinessSegments != null && model.BusinessSegments.ToString() != "") ? model.BusinessSegments : null;
                    command.Parameters[10].Value = (model.RegionIds != null && model.RegionIds.ToString() != "") ? model.RegionIds : null;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportData");
                }
                return null;
            }
        }

        public List<GenericList> GetCustomerList()
        {
            try
            {
                var customersList = (from cus in dbCon.tblCustomers
                                     join parentco in dbCon.tblParentCompanies on cus.ParentCompanyID equals parentco.ParentCompanyID
                                     where (parentco.ParentParentCompanyID == null) && (cus.IsActive == true)
                                     orderby cus.CustomerName
                                     select new GenericList { Key = cus.CustomerID, Value = cus.CustomerName }).ToList();
                return customersList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCustomerList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCustomerList");
                }
                return null;
            }
        }

        public List<GenericList> GetManufacturers()
        {
            try
            {
                var manufacturersList = (from pc in dbCon.tblParentCompanies
                        join pr in dbCon.tblPartyRoles on pc.PartyID equals pr.PartyID
                        join r in dbCon.tblRoles on pr.RoleID equals r.RoleID
                        join manu in dbCon.tblManufacturers on pc.ParentCompanyID equals manu.ParentCompanyID
                        where (pc.IsActive == true) && (manu.IsActive == true) && r.RoleName.Contains("Manufacturer")
                        orderby manu.ManufacturerName
                        select new GenericList { Key = manu.ManufacturerID, Value = manu.ManufacturerName }).ToList();
                return manufacturersList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetManufacturers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturers");
                }
                return null;
            }
        }

        public List<GenericList> GetSubcompaniesByCustomerID(int customerID)
        {
            try
            {
                var parentCompanyID = (from cus in dbCon.tblCustomers
                                       where cus.CustomerID == customerID
                                       select new { cus.ParentCompanyID }).FirstOrDefault();

                var subcustomersList = (from cus in dbCon.tblCustomers
                             join parentco in dbCon.tblParentCompanies on cus.ParentCompanyID equals parentco.ParentCompanyID
                             where (parentco.ParentParentCompanyID != null) &&
                             (cus.IsActive == true) && (parentco.ParentCompanyID == cus.ParentCompanyID)
                             && (parentco.ParentParentCompanyID == parentCompanyID.ParentCompanyID)//&& parentco.IsActive.Equals(1)
                             orderby cus.CustomerName
                             select new GenericList{ Key= cus.CustomerID, Value = cus.CustomerName }).ToList();
                return subcustomersList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSubcompaniesByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSubcompaniesByCustomerID");
                }
                return null;
            }
        }

        public List<GenericList> GetProductCategoryByCustomerID(int customerID)
        {
            try
            {
                var categories = (from pc in dbCon.tblPartCategories
                                  join ccp in dbCon.tblCustomerCountryParts on pc.PartCategoryID equals ccp.PartCategoryID
                                  where (pc.IsActive == true) && (ccp.CustomerID == customerID) && (ccp.IsActive == true)
                                  select new GenericList { Key = pc.PartCategoryID, Value = pc.PartCategoryName }).Distinct().OrderBy(x => x.Value).ToList();
                return categories;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetProductCategoryByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetProductCategoryByCustomerID");
                }
                return null;
            }
        }

        public List<string> GetBusinessSegmentsByCustomerID(int customerID)
        {
            try
            {
                var customerName = dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerID).CustomerName;
                List<string> business = new List<string>();

                if (customerName == "Mars")
                {
                    business = (from cadd in dbCon.tblCustomerAddresses
                                    where cadd.CustomerID == customerID && cadd.IsActive == 1 && cadd.BuisnessSegment != null
                                    select cadd.BuisnessSegment
                         ).Distinct().ToList<String>();
                }
                else
                {
                    business = null;
                }
                
                return business;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetBusinessSegmentsByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetBusinessSegmentsByCustomerID");
                }
                return null;
            }
        }

        public List<GenericList> GetRegionsByCustomerID(int customerID)
        {
            try
            {
                var customerName = dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerID).CustomerName;
                List<GenericList> regions = new List<GenericList>();

                if (customerName == "Mars")
                {
                    int companyId = 0;
                    var cust = dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == customerID);
                    if (cust != null)
                    {
                        companyId = cust.ParentCompanyID;
                    }

                    regions = (from reg in dbCon.tblRegions
                               where reg.CompanyID == companyId && reg.IsActive == true
                               select new GenericList { Key = reg.RegionID, Value = reg.RegionName }).Distinct().ToList();
                }
                else
                {
                    regions = null;
                }

                return regions;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRegionsByCustomerID");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRegionsByCustomerID");
                }
                return null;
            }
        }
    }
}

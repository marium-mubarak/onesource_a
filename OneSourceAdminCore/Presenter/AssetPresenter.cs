﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using MoreLinq.Extensions;

namespace OneSourceAdminCore.Presenter
{
    public class AssetPresenter
    {
        private globalserveEntities dbCon;
        public AssetPresenter()
        {
            dbCon = new globalserveEntities();
        }

        public ModelToUpdate GetAssetDataToUpdate(IList<AssetSerialNumberViewModel> model, string fileName)    
        {
            try
            {
                int updatedCount = 0;
                List<AssetUpdatedSkuModel> au = new List<AssetUpdatedSkuModel>();
                ModelToUpdate mu = new ModelToUpdate();

                foreach (var items in model)
                {
                    var modelSessionTracking = items.SessionTrackingNumber;
                    var modelSku = items.ManufacturerSKU;

                    var orderItemID = dbCon.tblOrderItems.Where(x => x.SessionTrackingNumber == modelSessionTracking && x.ManufacturerSKU == modelSku).Select(x => x.OrderItemId).FirstOrDefault();

                    var assetOwnedId = dbCon.GSNetP_tbl_AssetsOwned.Where(x => x.LineItemID == orderItemID).Select(x => x.AssetsOwnedID).FirstOrDefault();

                    var assetID = dbCon.GSNetP_tbl_Assets.Where(x => x.AssetsOwnedID == assetOwnedId && (x.SerialNum == "" || x.SerialNum == null)).Select(x => x.AssetID).FirstOrDefault();

                    if(assetID > 0)
                    {
                        var returnRecord = dbCon.sp_GetSessionTrackingAndSku_AdminR(assetID, items.SerialNum, items.AssetTagNum, items.MACAddress);
                        updatedCount++;
                    }
                    else
                    {
                        au.Add(new AssetUpdatedSkuModel
                        {
                            SessionTrackingNumber = items.SessionTrackingNumber,
                            Sku = items.ManufacturerSKU
                        });
                    }

                }

                var modelCount = model.Count();
                var neglectedCount = modelCount - updatedCount;
                string notUpdateMessage,updateMessage;

                notUpdateMessage = $"Following records were not updated : {neglectedCount}";
                updateMessage = $"Updated : {updatedCount}";

                mu.ListOfModel = au;
                mu.NotUpdateMessage = notUpdateMessage;
                mu.UpdateMessage = updateMessage;
                return mu;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAssetDataToUpdate");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetDataToUpdate");
                }
                return null;
            }
        }
    }
}

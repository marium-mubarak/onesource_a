﻿using System;
using System.Collections.Generic;
using System.Linq;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdminCore.Presenter
{
    public class ReportSchedulerPresenter
    {
        globalserveEntities _dbCon = new globalserveEntities();
        PriceDeskReportsPresenter pricedeskPresenter = new PriceDeskReportsPresenter();
        SupportReportsPresenter supportReportsPresenter = new SupportReportsPresenter();
        public bool NewSchedule(tblReportSchedulerSetting model)
        {
            try
            {
                _dbCon.tblReportSchedulerSettings.Add(model);
                _dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "NewSchedule");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "NewSchedule");
                }
                return false;
            }
        }

        public List<tblReportSchedulerSetting> GetAllScheduleReports()
        {
            try
            {
                var a = _dbCon.tblReportSchedulerSettings.ToList();
                return a;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAllScheduleReports");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAllScheduleReports");
                }
                return null;
            }
        }

        public List<GenericList> GetAllReportsSubMenu()
        {
            try
            {
                var subReports = (from r in _dbCon.tblAdminMenus
                                  join r2 in _dbCon.tblAdminMenus on r.ParentMenuId equals r2.MenuId
                                  where r.MenuType == "Dashboard Sub-Menu" && r2.MenuName == "Reports"
                                  select new GenericList
                                  {
                                      Key = r.MenuId,
                                      Value = r.MenuName
                                  }).ToList();

                return subReports;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAllReportsSubMenu");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAllReportsSubMenu");
                }
                return null;
            }
        }

        public List<GenericList> GetReportItemsByReportId(int reportId)
        {
            try
            {
                var items = (from r in _dbCon.tblAdminMenus
                             where r.ParentMenuId == reportId
                             select new GenericList
                             {
                                 Key = r.MenuId,
                                 Value = r.MenuName
                             }).ToList();

                return items;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportItemsByReportId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportItemsByReportId");
                }
                return null;
            }
        }

        public List<ReportSchedulerSettings> GetAllReportSchedulerSettings()
        {
            try
            {
                var items = _dbCon.tblReportSchedulerSettings.ToList();

                var resultItems = items.Select(s => new ReportSchedulerSettings()
                {
                    Id = s.Id,
                    Report = _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == s.ReportId) != null ?
                    _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == s.ReportId).MenuName : "-",
                    ReportType = _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == s.ReportTypeId) != null ?
                    _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == s.ReportTypeId).MenuName : "-",
                    WeekDay = (s.WeekDayNumber != null && s.WeekDayNumber > 0) ? Enum.GetValues(typeof(ReportDays)).Cast<ReportDays>().FirstOrDefault(x => (int)x == Convert.ToInt32(s.WeekDayNumber)).ToString() : "Everyday",
                    ReportTime = s.ReportTime,
                    RecieverEmail = s.RecieverEmail,
                    StartDate = s.StartDate != null ? Convert.ToDateTime(s.StartDate).ToShortDateString() : "-",
                    EndDate = s.EndDate != null ? Convert.ToDateTime(s.EndDate).ToShortDateString() : "-",
                    CreatedBy = _dbCon.tblUsers.FirstOrDefault(x => x.UserID == s.CreatedBy) != null ?
                    _dbCon.tblUsers.FirstOrDefault(x => x.UserID == s.CreatedBy).UserName : "-",
                    CreateDate = s.CreateDate != null ? Convert.ToDateTime(s.CreateDate).ToShortDateString() : "-",

                }).OrderByDescending(x => x.Id).ToList();

                return resultItems;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetAllReportSchedulerSettings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportItemsByReportId");
                }
                return new List<ReportSchedulerSettings>();
            }
        }


        public ReportSchedulerSettings GetReportSchedulerSettingById(int id)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == id);

                var allLocations = false;
                var allCustomers = false;
                var allOrderStatuses = false;
                var allCountries = false;
                var report = _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == item.ReportId);
                if (report != null)
                {
                    if(report.MenuName== "Part Detail Report" || report.MenuName == "Part Detail With Asset")
                    {
                        if (item.CustomerIds == null || item.CustomerIds == "0")
                        {
                            item.CustomerIds = "-1"; // All selected
                            allCustomers = true;
                        }
                    }
                    if (report.MenuName == "Order By Status , Country")
                    {
                        if (item.OrderStatus == null || item.OrderStatusNames == null)
                        {
                            item.OrderStatus = "-1"; // All selected
                            allOrderStatuses = true;
                        }
                    }
                    if (report.MenuName == "Assets By Location")
                    {
                        if (item.LocationIds == null || item.LocationNames == null)
                        {
                            item.LocationIds = "-1"; // All selected
                            allLocations = true;
                        }
                    }
                    if (report.MenuName == "Assets By Country")
                    {
                        if (item.CountryIds == null || item.CountryNames == null)
                        {
                            item.CountryIds = "-1"; // All selected
                            allCountries = true;
                        }
                    }
                    if (report.MenuName == "Quotes By Supplier Report" || report.MenuName == "Quotes By Status Report")
                    {
                        if (item.Status == null || item.Status == null)
                        {
                            item.Status = "All Statuses"; // All selected
                        }
                    }
                }

                var parentReport = _dbCon.tblAdminMenus.FirstOrDefault(x=>x.MenuId == item.ReportTypeId);

                var requestTypesList = supportReportsPresenter.GetRequestTypesByCustomerId(item.CustomerIds);
                var requestTypes = requestTypesList.Select(x => new
                {
                    Value = x.artypeId.ToString(),
                    Text = x.requestType
                }).ToList();

                var requestType = "";
                if (item.RequestTypes!=null && item.RequestTypes != "")
                {
                    foreach(var val in requestTypes)
                    {
                        if (item.RequestTypes.Contains(val.Value))
                        {
                            requestType += val.Text + ",";
                        }
                    }
                    requestType = requestType.TrimEnd(',');
                }
               

                var obj = new ReportSchedulerSettings()
                {
                    Id = item.Id,
                    Report = _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == item.ReportId) != null ?
                    _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == item.ReportId).MenuName : "-",
                    ReportType = _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == item.ReportTypeId) != null ?
                    _dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == item.ReportTypeId).MenuName : "-",
                    Customers = allCustomers == true ? "All Selected" : item.CustomerNames != null ? item.CustomerNames : null,
                    CustomerIds = item.CustomerIds != null ? item.CustomerIds : null,
                    Countries = allCountries==true?"All Selected":item.CountryNames != null ? item.CountryNames.TrimEnd(',') : null,
                    CountryIds = item.CountryIds != null ? item.CountryIds.TrimEnd(',') : null,
                    Users = item.UserNames != null ? item.UserNames.TrimEnd(',') : null,
                    Currencies = item.CurrencyNames != null ? item.CurrencyNames.TrimEnd(',') : null,
                    OrderStatuses = allOrderStatuses == true ? "All Selected" : item.OrderStatusNames != null ? item.OrderStatusNames.TrimEnd(',') : null,
                    Suppliers = item.SupplierNames != null ? item.SupplierNames.TrimEnd(',') : null,
                    Manufacturers = item.ManufacturerNames != null ? item.ManufacturerNames.TrimEnd(',') : null,
                    Locations = allLocations == true ? "All Selected" : item.LocationNames != null ? item.LocationNames.TrimEnd(',') : null,
                    RequestTypes = item.RequestTypes != "" ? requestType : null,
                    Companies = item.CompanyNames != null ? item.CompanyNames.TrimEnd(',') : null,
                    BusinessSegments = item.BusinessSegmentNames != null ? item.BusinessSegmentNames.TrimEnd(',') : null,
                    Regions = item.RegionNames != null ? item.RegionNames.TrimEnd(',') : null,
                    Categories = item.CategoryNames != null ? item.CategoryNames.TrimEnd(',') : null,
                    AssetTypes = item.AssetNames != null ? item.AssetNames.TrimEnd(',') : null,
                    AccountManager = pricedeskPresenter.GetUserWithInfo().FirstOrDefault(x => x.UserID == Convert.ToInt32(item.AccountManagerId)) != null ?
                    pricedeskPresenter.GetUserWithInfo().FirstOrDefault(x => x.UserID == Convert.ToInt32(item.AccountManagerId)).UserName : null,
                    QuoteType = item.QuoteTypeId != null ? (item.QuoteTypeId == "1" ? "Hardware" : item.QuoteTypeId == "2" ? "Software" : item.QuoteTypeId == "3" ? "Asset Disposal" : item.QuoteTypeId == "4" ? "IT Service" : "-") 
                    : parentReport.MenuName=="PriceDesk"?"All Types":null,
                    Status = item.Status ?? null,
                    Assets = item.AssetNames ?? null,
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    FilterType = null
                };



                if (report.MenuName == "Asset Summary Report")
                {
                    if (item.RegionCountry=="CountryWise")
                    {
                        obj.FilterType = "Country Wise"; 
                    }
                    else
                    {
                        obj.FilterType = "Region Wise";
                    }
                }

                return obj;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportSchedulerSettingById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportSchedulerSettingById");
                }
                return null;
            }
        }
        public int RemoveSettingDetailById(int id)
        {
            try
            {
                int count;
                using (var transcation = _dbCon.Database.BeginTransaction())
                {
                    var result = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == id);
                    if (result != null)
                    {
                        _dbCon.tblReportSchedulerSettings.Remove(result);
                        count = _dbCon.SaveChanges();
                    }
                    else
                    {
                        count = 0;
                    }
                    transcation.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReportSchedulerSettingById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReportSchedulerSettingById");
                }
                return 0;
            }
        }
        public string[] GetSelectedCustomers(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var customers = item.CustomerIds.Split(',').Select(x => x.Trim()).ToArray();
                return customers;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedCustomers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedCustomers");
                }
                return null;
            }
        }

        public string[] GetSelectedCountries(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var countries = item.CountryIds.Split(',').Select(x => x.Trim()).ToArray();
                return countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedCountries");
                }
                return null;
            }
        }
        public string[] GetSelectedLocations(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var locations = item.LocationIds.Split(',').Select(x => x.Trim()).ToArray();
                return locations;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedLocations");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedLocations");
                }
                return null;
            }
        }
        public string[] GetSelectedCurrencies(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var currencies = item.CurrencyIds.Split(',').Select(x => x.Trim()).ToArray();
                return currencies;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedCurrencies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedCurrencies");
                }
                return null;
            }
        }

        public string[] GetSelectedSuppliers(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var suppliers = item.SupplierIds.Split(',').Select(x => x.Trim()).ToArray();
                return suppliers;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedSuppliers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedSuppliers");
                }
                return null;
            }
        }

        public string[] GetSelectedManufacturers(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var manufacturers = item.ManufacturerIds.Split(',').Select(x => x.Trim()).ToArray();
                return manufacturers;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedManufacturers");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedManufacturers");
                }
                return null;
            }
        }

        public string[] GetSelectedCompanies(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var companies = item.CompanyIds.Split(',').Select(x => x.Trim()).ToArray();
                return companies;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedCompanies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedCompanies");
                }
                return null;
            }
        }
        public string[] GetSelectedBusinessSegments(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var BusinessSegments = item.BusinessSegments.Split(',').Select(x => x.Trim()).ToArray();
                return BusinessSegments;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedBusinessSegments");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedBusinessSegments");
                }
                return null;
            }
        }

        public string[] GetSelectedProductCategories(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var productCategories = item.ProductCategoryIds.Split(',').Select(x => x.Trim()).ToArray();
                return productCategories;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedProductCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedProductCategories");
                }
                return null;
            }
        }

        public string[] GetSelectedPartCategories(int settingsId)
        {
            try
            {
                var item = _dbCon.tblReportSchedulerSettings.FirstOrDefault(x => x.Id == settingsId);

                var partCategories = item.PartCategoryIds.Split(',').Select(x => x.Trim()).ToArray();
                return partCategories;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSelectedPartCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSelectedPartCategories");
                }
                return null;
            }
        }
    }
}

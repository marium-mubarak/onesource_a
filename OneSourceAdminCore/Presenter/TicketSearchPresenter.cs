﻿using GSSupport;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : June 19 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This presenter Class is responsible for providing ticket tracking detail"
    /// </summary>
    public class TicketSearchPresenter
    {
        private readonly globalserveEntities _dbCon;
        public TicketSearchPresenter()
        {
            _dbCon = new globalserveEntities();
        }
        /// <summary>
        /// This method is responsible for providing ticket search view
        /// </summary>
        /// <param name="userGroupName"></param>
        /// <param name="companyId"></param>
        /// <param name="SFAID"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public TicketSearchModel GetTicketSearchModel(string userGroupName, int companyId, string SFAID, string userId)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                TicketSearchModel obj = new Model.TicketSearchModel();
                string strCompanyId = companyId == 0 ? "" : companyId.ToString();

                obj.Locations = _dbCon.sp_getLocationTicketSearch_AdminR(userGroupName, SFAID, userId, strCompanyId).ToList();

                if (userGroupName == "ISD")
                {
                    obj.SubmittedBy = _dbCon.sp_getSubmittedByTicketSearch_AdminR(userGroupName, strCompanyId).ToList();
                    obj.SubmittedBy.Insert(0, new Entity.sp_getSubmittedByTicketSearch_AdminR_Result { submittedbyuserid = 0, username = "(None)" });
                    obj.AssignedSuppliers = _dbCon.sp_getAssignedSupplierSearch_AdminR(userGroupName, SFAID, userId, strCompanyId).ToList();
                    obj.AssignedSuppliers.Insert(0, new Entity.sp_getAssignedSupplierSearch_AdminR_Result { sfaid = 0, sfaname = "(None)" });
                    obj.AffectedUsers = _dbCon.sp_getAffectedUserTicketSearch_AdminR(userGroupName, strCompanyId).ToList();
                    obj.AffectedUsers.Insert(0, new Entity.sp_getAffectedUserTicketSearch_AdminR_Result { affectedusercontactid = 0, contactname = "(None)" });
                    obj.AssignedCSM = _dbCon.sp_getAssignedCSMSearch_AdminR(userGroupName, strCompanyId).ToList();
                    obj.AssignedCSM.Insert(0, new Entity.sp_getAssignedCSMSearch_AdminR_Result { contactname = "(None)", csrid = 0 });
                }
                obj.Companies = _dbCon.sp_getCompanyTicketSearch_AdminR(userGroupName, SFAID, userId).ToList();
                obj.Companies.Insert(0, new Entity.sp_getCompanyTicketSearch_AdminR_Result { companyid = 0, parentcompanyname = "(None)" });
                obj.Countries = _dbCon.sp_getCountryTicketSearch_AdminR(userGroupName, SFAID, userId, strCompanyId).ToList();
                obj.Priorities = _dbCon.sp_getPriorityTicketSearch_AdminR(userGroupName, SFAID, userId, strCompanyId).ToList();
                obj.Priorities.Insert(0, new Entity.sp_getPriorityTicketSearch_AdminR_Result { priority = "(None)", priorityid = 0 });
                obj.RequestTypes = _dbCon.sp_getRequestTypeTicketSearch_AdminR(userGroupName, SFAID, userId, strCompanyId).ToList();
                obj.RequestTypes.Insert(0, new Entity.sp_getRequestTypeTicketSearch_AdminR_Result { artypeid = 0, requesttype = "(None)" });
                obj.Statuses = _dbCon.sp_getStatusTicketSearch_AdminR(userGroupName, SFAID, userId, strCompanyId).ToList();
                return obj;

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>ticket Search Presenter => GetTicketSearchModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "ticket Search Presenter => GetTicketSearchModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketSearchModel");
                }
                return new TicketSearchModel();
            }
        }
        /// <summary>
        ///  This method is responsible for Ticket Search request
        /// </summary>
        /// <param name="model"></param>
        /// <param name="dateFormatId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="sfaid"></param>
        /// <param name="userId"></param>
        /// <param name="requiredRecord"></param>
        /// <returns></returns>
        public TicketListModel GetSearchedTickets(TicketSearchModel model, string dateFormatId, string userGroupName, string sfaid, string userId, int requiredRecord)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                TicketListModel oslmodel = new Model.TicketListModel();
                ObjectParameter totalCount = new ObjectParameter("TotalCount", typeof(Int32));

                List<sp_getTicketSearch_AdminR_Result> obj = _dbCon.sp_getTicketSearch_AdminR(Convert.ToInt32(dateFormatId), sfaid, userId,
                    userGroupName, model.TicketNumber, model.CTicketNumber, model.STicketNumber, model.CompanyId == 0 ? "" : model.CompanyId.ToString(), model.SubmittedById == 0 ? "" : model.SubmittedById.ToString(),
                    model.AffectedUserContactId == 0 ? "" : model.AffectedUserContactId.ToString(), model.PriorityId == 0 ? "" : model.PriorityId.ToString(), model.ArtypeId == 0 ? "" : model.ArtypeId.ToString()
                    , model.StrLocations, model.StrCountries, model.StrStatuses, model.SFAId == 0 ? "" : model.SFAId.ToString(), model.CSRId == 0 ? "" : model.CSRId.ToString(), model.SerialNumber, model.AssetTag,
                    model.RequestedAfterDate, model.RequestedBeforeDate, model.ClosedAfterDate, model.ClosedBeforeDate, requiredRecord.ToString(), totalCount).ToList();




                oslmodel.LstTicket = obj;
                oslmodel.TicketType = model.TicketType;
                if (obj.Count == 0)
                {
                    oslmodel.TotalRecord = 0;
                }

                else { oslmodel.TotalRecord = Convert.ToInt32(totalCount.Value); }
                return oslmodel;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetSearchedTickets<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetSearchedTickets");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Ticket Search Presenter =>GetSearchedTickets");
                }
                return new TicketListModel();
            }
        }
        /// <summary>
        /// This method is responsible for ticket detail data 
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="SFAID"></param>
        /// <param name="ServerTimeZone"></param>
        /// <returns></returns>
        public TicketDetailModel GetTicketDetail(int ticketId, string userGroupName, int SFAID, int ServerTimeZone, int userId)
        {
            try
            {
                TicketDetailModel obj = new Model.TicketDetailModel();
                obj.TicketId = ticketId;
                obj.TicketDetail = _dbCon.sp_getTicketDetail_AdminR(ticketId).FirstOrDefault() ?? new sp_getTicketDetail_AdminR_Result();

                var ticket = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketId);
                if (ticket != null)
                {
                    obj.ServerDST = ticket.ServerDST;
                }

                if (obj.TicketDetail != null && obj.TicketDetail.parentcompanyid > 0 && obj.TicketDetail.tkttypeid > 0)
                {

                    if (userGroupName == "SFA")
                    {
                        if (obj.TicketDetail.sfaid == SFAID || obj.TicketDetail.submittedbyuserid == userId) { }
                        else { return null; }
                    }

                    //Timezone

                    obj.TimeZone = _dbCon.sp_getTimeZone_AdminR().ToList();
                    var field = _dbCon.tblFields.FirstOrDefault(x => x.vbConst == ServerTimeZone);
                    if (field != null)
                    {
                        var tblTimeZone = _dbCon.tblTimeZones.FirstOrDefault(x => x.FieldID == field.fieldID);
                        if (tblTimeZone != null)
                        {
                            obj.TimeZoneId = tblTimeZone.TimeZoneID;
                        }
                    }
                    obj.WorldTimeServer = _dbCon.tblWorldTimeServers.FirstOrDefault(x => x.LocationID == "US-NY") ?? new Entity.tblWorldTimeServer();
                    //

                    //Assign CSM
                    var invoiceData = (from c in _dbCon.tblContacts
                                       join p in _dbCon.tblPartyRoles on c.PartyID equals p.PartyID
                                       join r in _dbCon.tblRoles on p.RoleID equals r.RoleID
                                       where r.RoleName == "CSR"
                                       select new { c.LastName, c.FirstName, c.ContactID }).ToList();
                    obj.CSRList = invoiceData.Select(x => new GenericList
                    {
                        Key = x.ContactID,
                        Value = Convert.ToString(x.FirstName) + " " + Convert.ToString(x.LastName)
                    }).OrderBy(x => x.Value).ToList();
                    obj.CSRList.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });
                    //

                    //Request Type
                    obj.RequestType = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && x.CompanyID == obj.TicketDetail.parentcompanyid)
                        .Select(x => new GenericList { Key = x.ARTypeID, Value = x.ARTypeName }).OrderByDescending(x => x.Value).ToList();

                    if (obj.RequestType.Count == 0)
                    {
                        obj.RequestType = _dbCon.tblActionRequestTypes.Where(x => x.IsActive == true && x.IsSelectable == true && (x.CompanyID == obj.TicketDetail.parentcompanyid || x.CompanyID == null))
                        .Select(x => new GenericList { Key = x.ARTypeID, Value = x.ARTypeName }).OrderByDescending(x => x.Value).ToList();
                    }
                    obj.RequestType.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });
                    //

                    //Priorities
                    bool includeGS = true;
                    var priorities = _dbCon.tblPriorities.Where(x => x.CompanyID == obj.TicketDetail.parentcompanyid).ToList();
                    includeGS = priorities.Count > 0 ? false : true;
                    if (includeGS)
                    {
                        var globalServeCompany = _dbCon.tblParentCompanies.Where(x => x.ParentCompanyName == "GlobalServe").Select(x => x.ParentCompanyID).ToList();

                        obj.Priorities = (from p in _dbCon.tblPriorities
                                          join
                                          pc in _dbCon.tblParentCompanies on p.CompanyID equals pc.ParentCompanyID into ps
                                          from pc in ps.DefaultIfEmpty()
                                          where p.IsActive == true && (globalServeCompany.Contains(p.CompanyID) || p.CompanyID == obj.TicketDetail.parentcompanyid)
                                          orderby p.PriorityIntValue
                                          select new GenericList { Key = p.PriorityID, Value = p.PriorityName }).ToList();

                    }
                    else
                    {
                        obj.Priorities = (from p in _dbCon.tblPriorities
                                          join
                                          pc in _dbCon.tblParentCompanies on p.CompanyID equals pc.ParentCompanyID into ps
                                          from pc in ps.DefaultIfEmpty()
                                          where p.IsActive == true && p.CompanyID == obj.TicketDetail.parentcompanyid
                                          orderby p.PriorityIntValue
                                          select new GenericList { Key = p.PriorityID, Value = p.PriorityName }).ToList();

                    }
                    obj.Priorities.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });

                    //

                    //Ticket Status
                    bool areCustomStatuses = false, areCustomTypeStatuses = false, usenull = false;


                    var tktStatuses = _dbCon.tblTicketStatus.Where(x => x.IsActive == true && x.CompanyID == obj.TicketDetail.parentcompanyid && (x.ARTypeID == obj.TicketDetail.tkttypeid || x.ARTypeID == null)).ToList();
                    if (tktStatuses.Count == 0)
                    {
                        usenull = true;
                    }
                    else
                    {
                        areCustomStatuses = true;
                        tktStatuses = _dbCon.tblTicketStatus.Where(x => x.IsActive == true && x.CompanyID == obj.TicketDetail.parentcompanyid && x.ARTypeID == obj.TicketDetail.tkttypeid).ToList();
                        if (tktStatuses.Count > 0)
                        {
                            areCustomTypeStatuses = true;
                        }
                    }

                    if (usenull)
                    {

                        obj.TicketStatuses = (from ts in _dbCon.tblTicketStatus
                                              join
                                              pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                              from pc in ps.DefaultIfEmpty()
                                              where ts.IsActive == true && ts.CompanyID == null
                                              orderby ts.StatusIntValue
                                              select new GenericList { Key = ts.TktStatusID, Value = ts.TktStatusName }).ToList();
                    }
                    else if (areCustomTypeStatuses)
                    {
                        obj.TicketStatuses = (from ts in _dbCon.tblTicketStatus
                                              join
                                              pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                              from pc in ps.DefaultIfEmpty()
                                              where ts.IsActive == true && ts.CompanyID == obj.TicketDetail.parentcompanyid && ts.ARTypeID == obj.TicketDetail.tkttypeid
                                              orderby ts.StatusIntValue
                                              select new GenericList { Key = ts.TktStatusID, Value = ts.TktStatusName }).ToList();
                    }
                    else if (areCustomStatuses)
                    {
                        obj.TicketStatuses = (from ts in _dbCon.tblTicketStatus
                                              join
                                              pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                              from pc in ps.DefaultIfEmpty()
                                              where ts.IsActive == true && ts.CompanyID == obj.TicketDetail.parentcompanyid && ts.ARTypeID == null
                                              orderby ts.StatusIntValue
                                              select new GenericList { Key = ts.TktStatusID, Value = ts.TktStatusName }).ToList();
                    }
                    else
                    {
                        obj.TicketStatuses = (from ts in _dbCon.tblTicketStatus
                                              join
                                              pc in _dbCon.tblParentCompanies on ts.CompanyID equals pc.ParentCompanyID into ps
                                              from pc in ps.DefaultIfEmpty()
                                              where ts.IsActive == true && ts.CompanyID == obj.TicketDetail.parentcompanyid
                                              orderby ts.StatusIntValue
                                              select new GenericList { Key = ts.TktStatusID, Value = ts.TktStatusName }).ToList();
                    }

                    obj.CloseTicketStatusIds = _dbCon.tblTicketStatus.Where(x => x.IsActive == true && x.CompanyID == obj.TicketDetail.parentcompanyid && x.TktStatusName == "closed").Select(x => x.TktStatusID).ToList();


                    //

                    //assign supplier

                    obj.AssignSuppliers = (from pc in _dbCon.tblParentCompanies
                                           join
                                             sfa in _dbCon.tblSFAs on pc.ParentCompanyID equals sfa.ParentCompanyID
                                           join
                                             pa in _dbCon.tblPartyAddresses on pc.PartyID equals pa.PartyID
                                           join
                                             a in _dbCon.tblAddresses on pa.AddressID equals a.AddressID
                                           where sfa.IsComposite == true && a.CountryID == obj.TicketDetail.countryid
                                           orderby pc.ParentCompanyName
                                           select new GenericList { Key = sfa.SFAID, Value = pc.ParentCompanyName }).Distinct().ToList();

                    obj.AssignSuppliers.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });
                    //

                    //asset manufacturer


                    var tempMfg = (from t in _dbCon.tblTickets
                                   join
                                        ar in _dbCon.tblActionRequests on t.ARID equals ar.ARID
                                   join
                                        a in _dbCon.GSNetP_tbl_Assets on ar.AssetID equals a.AssetID into ps
                                   from a in ps.DefaultIfEmpty()
                                   join ao in _dbCon.GSNetP_tbl_AssetsOwned on a.AssetsOwnedID equals ao.AssetsOwnedID into ps2
                                   from ao in ps2.DefaultIfEmpty()
                                   where t.TicketID == ticketId
                                   select new { mfgId = (int?)ao.ManufacturerID, mfgModel = ao.Model }).FirstOrDefault();
                    if (tempMfg != null)
                    {
                        obj.tmpmodel = tempMfg.mfgModel;
                        obj.tmpmfgid = Convert.ToInt32(tempMfg.mfgId);
                    }

                    obj.AssetManufacturer = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.ManufacturerID, Value = x.ManufacturerName }).Distinct().OrderBy(x => x.Value).ToList();
                    obj.AssetManufacturer.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });

                    if (userGroupName == "ISD")
                    {


                        if (obj.TicketDetail.sfaid > 0)
                        {
                            obj.SupplierContacts = GetSupplierContacts(obj.TicketDetail.sfaid);
                        }

                    }
                    //

                    //Currencies
                    obj.Currencies = _dbCon.tblCurrencies.Where(x => x.IsActive == true).OrderBy(x => x.CurrencySymbol).Select(x => new GenericList { Value = x.CurrencySymbol + "( " + x.CurrencyName + " )", Key = x.CurrencyID }).Distinct().ToList();

                    //                

                    obj.IsCustomer = _dbCon.tblCustomers.Where(x => x.ParentCompanyID == obj.TicketDetail.parentcompanyid).Count();

                    obj.ServiceLevel = (from st in _dbCon.tblSLATrackings
                                        join
                                        tac in _dbCon.tblTermsAndConditions on st.tacid equals tac.TACID
                                        join sla in _dbCon.tblSLAs on tac.SLAID equals sla.SLAID
                                        where st.ticketid == ticketId
                                        select new ServiceLevel { slaInstanceID = st.slaInstanceID, SLAName = sla.SLAName, tacname = tac.TACName }).ToList();



                }
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetTicketDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetTicketDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketDetail");
                }
                return new TicketDetailModel();
            }
        }

        /// <summary>
        /// this method is responsible for providing supplier contacts data
        /// </summary> 
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<SupplierContact> GetSupplierContacts(int sfaId)
        {
            try
            {
                //Supplier contacts
                var query = "select replace(isnull(c.email, '-'), ',', ';') as Email, replace(isnull(c.firstname + ' ' + c.lastname, '-'), ',', '') as ContactName, " +
                            "replace(isnull(c.workphonenumber, '-'), ',', '') as SFAPhone, replace(isnull(c.title, '-'), ',', '') as Title from tblcontact c " +
                            "inner join tblrole r on r.rolename = 'Support Contact' and r.isactive = 1 " +
                            "inner join tblpartyrole pr on pr.partyid = c.partyid and pr.roleid = r.roleid and pr.isactive = 1 " +
                            "inner join tblsfas sfa on sfa.parentcompanyid = c.companyid " +
                            "where c.isactive = 1 and sfa.sfaid = " + sfaId;

                return _dbCon.Database.SqlQuery<SupplierContact>(query).ToList();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>ticket Search Presenter => GetSupplierContacts<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "ticket Search Presenter => GetSupplierContacts");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSupplierContacts");
                }
                return new List<SupplierContact>();
            }
        }

        /// <summary>
        /// this method is responsible for providing timezone difference
        /// </summary>
        /// <param name="RequestDate"></param>
        /// <param name="LocalTimezoneBias"></param>
        /// <param name="ServerTimezoneBias"></param>
        /// <returns></returns>
        public DateTime? AddTimeZoneDifference(DateTime? RequestDate, string LocalTimezoneBias, string ServerTimezoneBias)
        {
            try
            {

                string ltzHrs = null;
                string ltzMins = null;
                DateTime? ConvertedDate = null;
                string stzHrs = null;
                string stzMins = null;

                if (LocalTimezoneBias.Contains("+"))
                {
                    var LocalTimezoneArr = LocalTimezoneBias.Split('+');
                    if (LocalTimezoneArr[1].Length == 3)
                    {
                        ltzHrs = LocalTimezoneArr[1].Substring(0, 1);
                        ltzMins = LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 1);
                    }
                    else if (LocalTimezoneArr[1].Length == 4)
                    {
                        ltzHrs = LocalTimezoneArr[1].Substring(0, 2);
                        ltzMins = LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 2);
                    }
                }

                else if (LocalTimezoneBias.Contains("-"))
                {
                    var LocalTimezoneArr = LocalTimezoneBias.Split('-');
                    if (LocalTimezoneArr[1].Length == 3)
                    {
                        ltzHrs = "-" + LocalTimezoneArr[1].Substring(0, 1);
                        ltzMins = "-" + LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 1);
                    }
                    else if (LocalTimezoneArr[1].Length == 4)
                    {
                        ltzHrs = "-" + LocalTimezoneArr[1].Substring(0, 2);
                        ltzMins = "-" + LocalTimezoneArr[1].Substring(LocalTimezoneArr[1].Length - 2, LocalTimezoneArr[1].Length - 2);
                    }
                }
                else
                {
                    if (LocalTimezoneBias.Length == 3)
                    {
                        ltzHrs = LocalTimezoneBias.Substring(0, 1);
                        ltzMins = LocalTimezoneBias.Substring(LocalTimezoneBias.Length - 2, LocalTimezoneBias.Length - 1);
                    }
                    else if (LocalTimezoneBias.Length == 4)
                    {
                        ltzHrs = LocalTimezoneBias.Substring(0, 2);
                        ltzMins = LocalTimezoneBias.Substring(LocalTimezoneBias.Length - 2, LocalTimezoneBias.Length - 2);
                    }
                }



                if (ServerTimezoneBias.Contains("+"))
                {
                    var ServerTimezoneArr = ServerTimezoneBias.Split('+');
                    if (ServerTimezoneArr[1].Length == 3)
                    {
                        stzHrs = ServerTimezoneArr[1].Substring(0, 1);
                        stzMins = ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 1);
                    }
                    else if (ServerTimezoneArr[1].Length == 4)
                    {
                        stzHrs = ServerTimezoneArr[1].Substring(0, 2);
                        stzMins = ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 2);
                    }
                }
                else if (ServerTimezoneBias.Contains("-"))
                {
                    var ServerTimezoneArr = ServerTimezoneBias.Split('-');
                    if (ServerTimezoneArr[1].Length == 3)
                    {
                        stzHrs = "-" + ServerTimezoneArr[1].Substring(0, 1);
                        stzMins = "-" + ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 1);
                    }
                    else if (ServerTimezoneArr[1].Length == 4)
                    {
                        stzHrs = "-" + ServerTimezoneArr[1].Substring(0, 2);
                        stzMins = "-" + ServerTimezoneArr[1].Substring(ServerTimezoneArr[1].Length - 2, ServerTimezoneArr[1].Length - 2);
                    }
                }
                else
                {
                    if (ServerTimezoneBias.Length == 3)
                    {
                        stzHrs = ServerTimezoneBias.Substring(0, 1);
                        stzMins = ServerTimezoneBias.Substring(ServerTimezoneBias.Length - 2, ServerTimezoneBias.Length - 1);
                    }
                    else if (ServerTimezoneBias.Length == 4)
                    {
                        stzHrs = ServerTimezoneBias.Substring(0, 2);
                        stzMins = ServerTimezoneBias.Substring(ServerTimezoneBias.Length - 2, ServerTimezoneBias.Length - 2);
                    }
                }

                if (RequestDate == null)
                {
                    return null;
                }

                int Hrs = Convert.ToInt32(ltzHrs) - Convert.ToInt32(stzHrs);
                int Mins = Convert.ToInt32(ltzMins) - Convert.ToInt32(stzMins);

                ConvertedDate = Convert.ToDateTime(RequestDate).AddHours(Convert.ToDouble(Hrs)).AddMinutes(Convert.ToDouble(Mins));

                return ConvertedDate;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>ticket Search Presenter => AddTimeZoneDifference<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "ticket Search Presenter => AddTimeZoneDifference");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddTimeZoneDifference");
                }
                return null;
            }
        }

        /// <summary>
        /// this method is responsible for updating ticket
        /// </summary>
        /// <param name="detailModel"></param>
        /// <param name="addUModel"></param>
        /// <param name="serverTimeZone"></param>
        /// <param name="userId"></param>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public int UpdateTicket(sp_getTicketDetail_AdminR_Result detailModel, TicketAdditionalUpdateModel addUModel, int serverTimeZone, int userId, int contactId, string groupName)
        {
            try
            {
                int count = 0;
                DateTime? fRequestDate = null, fRequestDateLocal = null, fCloseDate = null, fAppointmentDate = null;
                DateTime logdate = DateTime.Now;

                if (detailModel.closedate != null)
                {
                    fCloseDate = Convert.ToDateTime(detailModel.closedate).Date + new TimeSpan(addUModel.closehours, addUModel.closeminutes, 0);
                }
                if (detailModel.appointmentdate != null)
                {
                    fAppointmentDate = Convert.ToDateTime(detailModel.appointmentdate).Date + new TimeSpan(addUModel.appointmenthours, addUModel.appointmentminutes, 0);
                }
                if (detailModel.requestdate != null)
                {
                    //Server Timezone Bias
                    var ServerTimeZoneBias = _dbCon.tblWorldTimeServers.FirstOrDefault(x => x.LocationID == "US-NY");
                    fRequestDate = Convert.ToDateTime(detailModel.requestdate).Date + new TimeSpan(addUModel.requesthours, addUModel.requestminutes, 0);
                    DateTime? rdservertime = null;
                    int servertzbias = 0;
                    if (ServerTimeZoneBias != null)
                    {
                        if ((fRequestDate >= ServerTimeZoneBias.ThisYearDSTStart && fRequestDate <= ServerTimeZoneBias.ThisYearDSTEnd) || (fRequestDate >= ServerTimeZoneBias.NextYearDSTStart && fRequestDate <= ServerTimeZoneBias.NextYearDSTEnd))
                        {
                            servertzbias = Convert.ToInt32(ServerTimeZoneBias.DstBias);
                        }
                        else
                        {
                            servertzbias = Convert.ToInt32(ServerTimeZoneBias.StdBias);
                        }
                        //rdservertime = AddTimeZoneDifference(fRequestDate, servertzbias.ToString(), Convert.ToInt32(addUModel.tzbias).ToString());
                        //if (rdservertime != null)
                        //{
                        //    fRequestDate = rdservertime;
                        //}
                        //else
                        //{
                        //    fRequestDate = null;
                        //}
                        rdservertime = fRequestDate;
                    }
                    //Local Timezone Bias
                    var customerAddr = _dbCon.tblCustomerAddresses.FirstOrDefault(x => x.CustomerAddressID == addUModel.addressid);
                    DateTime? rdlocaltime = null;
                    if (customerAddr != null)
                    {
                        var country = _dbCon.tblCountries.FirstOrDefault(x => x.CountryID == customerAddr.CountryID);
                        if (country != null)
                        {
                            var worldTimeServer = _dbCon.tblWorldTimeServers.FirstOrDefault(x => country.CountryName.Contains(x.Country));
                            if (worldTimeServer != null)
                            {
                                worldTimeServer = _dbCon.tblWorldTimeServers.FirstOrDefault(x => x.LocationID == worldTimeServer.LocationID);
                                if (worldTimeServer != null)
                                {
                                    int localtzbias = 0;

                                    if ((rdservertime >= ServerTimeZoneBias.ThisYearDSTStart && rdservertime <= ServerTimeZoneBias.ThisYearDSTEnd) || (rdservertime >= ServerTimeZoneBias.NextYearDSTStart && rdservertime <= ServerTimeZoneBias.NextYearDSTEnd))
                                    {
                                        localtzbias = Convert.ToInt32(worldTimeServer.DstBias);
                                    }
                                    else
                                    {
                                        localtzbias = Convert.ToInt32(worldTimeServer.StdBias);
                                    }
                                    rdlocaltime = AddTimeZoneDifference(rdservertime, localtzbias.ToString(), servertzbias.ToString());
                                    if (rdlocaltime != null)
                                    {
                                        fRequestDateLocal = rdlocaltime;
                                    }
                                    else
                                    {
                                        fRequestDateLocal = null;
                                    }
                                }
                            }
                        }
                    }

                }

                if (detailModel.requesttimezoneid != null)
                {
                    var fieldIds = _dbCon.tblTimeZones.Where(x => x.TimeZoneID == detailModel.requesttimezoneid).Select(x => x.FieldID).ToList();

                    var tzCount = _dbCon.tblFields.FirstOrDefault(x => fieldIds.Contains(x.fieldID));
                    if (tzCount != null)
                    {
                        var Timeconvert = new GSTime.TimeConverter();
                        logdate = Timeconvert.ConvertTime(DateTime.Now, serverTimeZone, Convert.ToInt32(tzCount.vbConst));
                    }
                    else
                    {
                        var Timeconvert = new GSTime.TimeConverter();
                        logdate = Timeconvert.ConvertTime(DateTime.Now, serverTimeZone, 1);
                    }
                }

                int? priorityid = null, tmpmfgid = null, sfaid = null, csrid = null, tkttypeid = null;

                if (detailModel.priorityid != 0)
                {
                    priorityid = detailModel.priorityid;
                }
                if (detailModel.sfaid != 0)
                {
                    sfaid = detailModel.sfaid;
                }
                if (detailModel.csrid != 0)
                {
                    csrid = detailModel.csrid;
                }
                if (detailModel.tkttypeid != 0)
                {
                    tkttypeid = detailModel.tkttypeid;
                }
                if (addUModel.tmpmfgid != 0)
                {
                    tmpmfgid = addUModel.tmpmfgid;
                }

                if (groupName == "SFA")
                {
                    count = _dbCon.sp_UpdateTicketStatus_R(detailModel.ticketid, priorityid, detailModel.tktstatusid, csrid, sfaid, detailModel.escalationLevel,
                 tkttypeid, addUModel.comments ?? string.Empty, addUModel.isactive, fCloseDate, fRequestDate, fRequestDateLocal, logdate, userId, false, null, null
                 , detailModel.customerticketid, detailModel.sfaticketid, detailModel.sfapo, detailModel.customerpo, detailModel.replaceserial, detailModel.hoursonsite, detailModel.minsonsite
                 , detailModel.hourstravel, detailModel.minstravel, fAppointmentDate, detailModel.ticketcontactname, detailModel.ticketcontactphonevoice, tmpmfgid, addUModel.tmpmodel, detailModel.workedonserial, detailModel.assetid

                 );
                }
                else
                {
                    count = _dbCon.sp_UpdateTicketStatus_R(detailModel.ticketid, priorityid, detailModel.tktstatusid, csrid, sfaid, detailModel.escalationLevel,
                 tkttypeid, addUModel.comments ?? string.Empty, addUModel.isactive, fCloseDate, fRequestDate, fRequestDateLocal, logdate, userId, detailModel.isbillable, detailModel.billamount, detailModel.billamountcurrencyid
                 , detailModel.customerticketid, detailModel.sfaticketid, detailModel.sfapo, detailModel.customerpo, detailModel.replaceserial, detailModel.hoursonsite, detailModel.minsonsite
                 , detailModel.hourstravel, detailModel.minstravel, fAppointmentDate, detailModel.ticketcontactname, detailModel.ticketcontactphonevoice, tmpmfgid, addUModel.tmpmodel, detailModel.workedonserial, detailModel.assetid

                 );
                }



                //ObjectParameter priorityid = new ObjectParameter("priorityid", typeof(Int32));
                //priorityid.Value = detailModel.priorityid;
                //ObjectParameter tktstatusid = new ObjectParameter("tktstatusid", typeof(Int32));
                //tktstatusid.Value = detailModel.tktstatusid;
                //ObjectParameter csrid = new ObjectParameter("csrid", typeof(Int32));
                //csrid.Value = detailModel.csrid;
                //ObjectParameter sfaid = new ObjectParameter("sfaid", typeof(Int32));
                //sfaid.Value = detailModel.sfaid;
                //ObjectParameter escalationLevel = new ObjectParameter("escalationLevel", typeof(Int32));
                //escalationLevel.Value = detailModel.escalationLevel;
                //ObjectParameter tkttypeid = new ObjectParameter("tkttypeid", typeof(Int32));
                //tkttypeid.Value = detailModel.tkttypeid;

                //count = _dbCon.sp_UpdateTicketStatus(addUModel.comments ?? string.Empty, detailModel.IsActive, fCloseDate, fRequestDate, fRequestDateLocal, logdate, userId,
                //   detailModel.isbillable, detailModel.billamount, detailModel.billamountcurrencyid, detailModel.customerticketid, detailModel.sfaticketid, detailModel.sfapo, detailModel.customerpo, detailModel.replaceserial,
                //   detailModel.hoursonsite, detailModel.minsonsite, detailModel.hoursonsite, detailModel.minstravel
                //   , fAppointmentDate, detailModel.ticketcontactname, detailModel.ticketcontactphonevoice, addUModel.tmpmfgid, addUModel.tmpmodel, detailModel.workedonserial, detailModel.assetid,
                //   detailModel.ticketid, priorityid, tktstatusid, csrid, sfaid, escalationLevel,
                // tkttypeid


                // );

                var ar = new GSSupport.ActionRequest();
                ar.SubmitUserID = userId;
                ar.AffectedContactID = contactId;
                ar.RequestDateLocal = DateTime.Now;
                ar.RequestDate = DateTime.Now;
                ar.RequestDateTimeZoneVBConst = serverTimeZone;
                ar.GSTicketID = Convert.ToInt32(detailModel.ticketid);
                ar.Qualify = false;
                ar.CategoryName = "Ticket Change";

                var aut = new GSSupport.ActionUpdateTicket();
                if (!string.IsNullOrEmpty(addUModel.comments))
                {
                    aut.IsAddComment = true;
                    ar.AddFieldByDisplay("Ticket Comment Added", "True", "True");
                }

                var am = new GSSupport.ActionMonitor();
                aut.IsComplete = true;
                aut.IsError = false;
                aut.GSTicketID = Convert.ToInt32(detailModel.ticketid);
                aut.UserID = userId;

                if (detailModel.tktstatusid != null && Convert.ToInt32(detailModel.tktstatusid) > 0)
                {
                    aut.StatusID = Convert.ToInt32(detailModel.tktstatusid);
                }
                if (detailModel.priorityid > 0)
                {
                    aut.PriorityID = detailModel.priorityid;
                }
                aut.TktEscalationLevel = Convert.ToInt32(detailModel.escalationLevel);

                if (detailModel.csrid != null && Convert.ToInt32(detailModel.csrid) > 0)
                {
                    aut.CSRID = Convert.ToInt32(detailModel.csrid);
                }
                if (detailModel.sfaid > 0)
                {
                    aut.SFAID = detailModel.sfaid;
                }
                if (detailModel.tkttypeid != null && Convert.ToInt32(detailModel.tkttypeid) > 0)
                {
                    aut.TypeID = Convert.ToInt32(detailModel.tkttypeid);
                }
                if (fAppointmentDate != null)
                {
                    aut.ApptDate = Convert.ToDateTime(fAppointmentDate);
                }

                am.Action = aut;
                ar.CompletedAction = am;
                ar.IsCompletedAction = true;
                ar.Submit();
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>UpdateTicket<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>UpdateTicket");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateTicket");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for providing ticket status schedule
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public TicketStatusScheduleModel GetTicketStatusSchedule(int ticketId)
        {
            try
            {
                TicketStatusScheduleModel obj = new Model.TicketStatusScheduleModel();
                obj.TicketId = ticketId;
                var tblTicket = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketId);
                if (tblTicket != null)
                {
                    List<tblTicketStatu> listTicketStatus = new List<tblTicketStatu>();
                    var tblActionRequest = _dbCon.tblActionRequests.FirstOrDefault(x => x.ARID == tblTicket.ARID);
                    if (tblActionRequest != null)
                    {
                        listTicketStatus = _dbCon.tblTicketStatus.Where(x => x.IsActive == true && x.CompanyID == tblActionRequest.CompanyID).OrderBy(x => x.StatusIntValue).ToList();
                        if (listTicketStatus.Count > 0)
                        {
                            var data = listTicketStatus.Where(x => x.ARTypeID == tblActionRequest.ARTypeID).ToList();
                            if (data.Count > 0)
                            {
                                listTicketStatus = data;
                            }
                        }
                        else
                        {
                            listTicketStatus = _dbCon.tblTicketStatus.Where(x => x.IsActive == true && x.CompanyID == null).OrderBy(x => x.StatusIntValue).ToList();
                        }
                        obj.TktStatusScheduleList = _dbCon.tblTktStatusSchedules.Where(x => x.isActive == true && x.ticketid == ticketId).ToList();
                        obj.TicketStatusList = listTicketStatus;
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetTicketStatusSchedule<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetTicketStatusSchedule");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketStatusSchedule");
                }
                return new TicketStatusScheduleModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating ticket status schedule
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ServerTimeZone"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int UpdateTicketStatusSchedule(List<tblTktStatusSchedule> model, int ServerTimeZone, int userId, string userName)
        {
            try
            {
                int count = 0;
                // DateTime? dbtarget=null, dbactual = null , target=null, actual=null;
                bool doadd = false;
                Dictionary<int, DateTime?> dicActuals = new Dictionary<int, DateTime?>();


                var aut = new GSSupport.ActionUpdateTicket();
                var ar = new GSSupport.ActionRequest();
                ar.GSTicketID = model[0].ticketid;
                ar.Qualify = false;
                ar.CategoryName = "Ticket Change";
                ar.RequestDateTimeZoneVBConst = ServerTimeZone;

                var am = new GSSupport.ActionMonitor();
                aut.IsComplete = true;
                aut.IsError = false;
                aut.GSTicketID = Convert.ToInt32(model[0].ticketid);
                aut.UserID = userId;

                foreach (var item in model)
                {
                    var tbltktstatusschedule = _dbCon.tblTktStatusSchedules.FirstOrDefault(x => x.tktstatusid == item.tktstatusid && x.ticketid == item.ticketid);
                    if (tbltktstatusschedule != null)
                    {
                        if (item.targetdate == null)
                        {
                            if (tbltktstatusschedule.targetdate != null)
                            {
                                aut.AddStatusActualChange(item.tktstatusid, Convert.ToDateTime(tbltktstatusschedule.targetdate), new DateTime(1970, 1, 1));
                            }
                        }
                        else
                        {
                            doadd = true;
                            if (tbltktstatusschedule.targetdate != null)
                            {
                                var dbDate = Convert.ToDateTime(tbltktstatusschedule.targetdate);
                                var tDate = Convert.ToDateTime(item.targetdate);
                                if (dbDate.Date.Month != tDate.Date.Month || dbDate.Date.Day != tDate.Date.Day || dbDate.Date.Year != tDate.Date.Year)
                                {
                                    aut.AddStatusTargetChange(item.tktstatusid, dbDate, tDate);
                                }
                            }
                            else
                            {
                                aut.AddStatusTargetChange(item.tktstatusid, new DateTime(1970, 1, 1), Convert.ToDateTime(item.targetdate));
                            }
                        }

                        if (item.actualdate != null)
                        {
                            dicActuals.Add(item.tktstatusid, item.actualdate);
                        }

                        _dbCon.tblTktStatusSchedules.Remove(tbltktstatusschedule);
                        count = _dbCon.SaveChanges();

                        if (doadd)
                        {
                            tbltktstatusschedule = new tblTktStatusSchedule();
                            tbltktstatusschedule.isActive = true;
                            tbltktstatusschedule.ticketid = item.ticketid;
                            tbltktstatusschedule.tktstatusid = item.tktstatusid;
                            tbltktstatusschedule.actualdate = item.actualdate;
                            tbltktstatusschedule.targetdate = item.targetdate;
                            tbltktstatusschedule.lastUpdate = DateTime.Now;
                            tbltktstatusschedule.lastUpdateuserID = userId;

                            _dbCon.tblTktStatusSchedules.Add(tbltktstatusschedule);
                            count = _dbCon.SaveChanges();

                            aut.AddStatusTarget(item.tktstatusid, new DateTime(1900, 1, 1), new DateTime(1900, 1, 1));


                        }

                    }
                    else
                    {
                        tbltktstatusschedule = new tblTktStatusSchedule();
                        tbltktstatusschedule.isActive = true;
                        tbltktstatusschedule.ticketid = item.ticketid;
                        tbltktstatusschedule.tktstatusid = item.tktstatusid;
                        tbltktstatusschedule.actualdate = item.actualdate;
                        tbltktstatusschedule.targetdate = item.targetdate;
                        tbltktstatusschedule.lastUpdate = DateTime.Now;
                        tbltktstatusschedule.lastUpdateuserID = userId;

                        _dbCon.tblTktStatusSchedules.Add(tbltktstatusschedule);
                        count = _dbCon.SaveChanges();
                    }
                }

                // ActionUpdateTicket is the completed Action for the Action Monitor
                am.Action = aut;
                // ActionMonitor is the contains the completed action for the Action Request
                ar.CompletedAction = am;
                ar.IsCompletedAction = true;
                ar.Submit();

                // OK, real drag... if the TAC is already closed I need to just update the DB 
                // if any TACs are affected because it won't be listening anymore...
                var tempTicketId = model[0].ticketid;
                var tblSLATrackings = _dbCon.tblSLATrackings.Where(x => x.ticketid == tempTicketId && (x.isBreach == true || x.closedWithinSLA == true)).ToList();
                int endStatus;
                foreach (var item in tblSLATrackings)
                {
                    var manager = new GSSupport.GSSupportManager();
                    var slam = manager.GetSLAMonitor(item.slaInstanceID);
                    var tac = manager.GetTAC(item.tacid);
                    // OK, not see if the end status date changed...
                    for (int i = 0; i < tac.GetEndStatusCount() - 1; i++)
                    {
                        endStatus = tac.GetEndStatusIDByIndex(i);
                        foreach (var dicItem in dicActuals)
                        {
                            // OK we have an actual date for the end status...
                            if (dicItem.Key == endStatus)
                            {
                                if (item.isBreach)
                                {
                                    // If it's a breach I just need one end status within TAC and I'm outta here...
                                    if (slam.CalculateMinutesToBreach(Convert.ToDateTime(dicItem.Value)) > 0)
                                    {
                                        slam.ClosedWithinSLA = true;
                                        slam.IsBreach = false;
                                        slam.UpdateSLAInstanceDB(("Ticket status date manually overridden by " + userName + "; " + slam.GetTACString() +
                                            " <a href=/support/sla/history.asp?slaiid=" + item.slaInstanceID + ">Closed Within SLA</a> at " + Convert.ToDateTime(dicItem.Value).ToString()), true, Convert.ToDateTime(dicItem.Value));
                                    }
                                }
                                else if (item.closedWithinSLA)
                                {

                                }
                            }
                        }
                    }
                }

                _dbCon.sp_UpdateTicketStatusBackdate(model[0].ticketid);

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>UpdateTicketStatusSchedule<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>UpdateTicketStatusSchedule");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateTicketStatusSchedule");
                }
                return 0;
            }
        }

        /// <summary>
        /// Get List of Asset Country Address against customer id 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="group"></param>
        /// <returns>List of countries addresses</returns>
        public List<AssetTagAddress> GetAssetCountryAddress(int companyId, string userGroup, int sfaId)
        {
            try
            {
                string query = string.Empty;
                if (userGroup == "ISD")
                {
                    query =
                       "select distinct awa.customeraddressid as CustomerAddressId, " +
                       " substring(c.countryname + ' - ' + awa.condensedaddress + ' ' + awa.address1, 0, 60) as CustomerAddress " +
                       " from gsnetp_tbl_addrwithassets awa " +
                    " inner join gsnetp_tbl_assets a on a.addrwithassetsid=awa.addrwithassetsid and a.isactive=1 " +
                    " inner join tblcountries c on c.countryid=awa.countryid  inner join tblcustomers cust on cust.parentcompanyid=" + companyId +
                    " and cust.customerid=awa.customerid order by CustomerAddress ";
                }
                else
                {
                    query =
                     "select distinct awa.customeraddressid," +
                     " substring(c.countryname + ' - ' + awa.condensedaddress + ' ' + awa.address1, 0, 60) as customeraddressname " +
                     "  from gsnetp_tbl_addrwithassets awa " +
                  " inner join gsnetp_tbl_assets a on a.addrwithassetsid=awa.addrwithassetsid and a.isactive=1 " +
                  " inner join tblcountries c on c.countryid=awa.countryid " +
                  " inner join tblofferings ofe on ofe.offeringshort='SUPPORT' inner join tblsfacustomerwa wa on wa.sfaid=" + sfaId
                  + " and wa.offeringid=ofe.offeringid and wa.customerid=awa.customerid and wa.isactive=1 "
                  + "  inner join tblcustomers cust on cust.parentcompanyid=" + companyId +
                  " and cust.customerid=awa.customerid order by customeraddressname";
                }

                var results = _dbCon.Database.SqlQuery<AssetTagAddress>(query).ToList();


                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetAssetCountryAddress<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetAssetCountryAddress");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetCountryAddress");
                }
                return new List<AssetTagAddress>();
            }
        }

        /// <summary>
        /// this method is responsible for getting manufacturer list
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<GenericList> GetManufacturersList(int companyId, string userGroup, int sfaId)
        {
            try
            {
                string query = string.Empty;
                if (userGroup == "ISD")
                {
                    query = "select distinct ao.manufacturerid as [Key], mfg.manufacturername as Value from gsnetp_tbl_assetsowned ao " +
                          " inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1 " +
                          " inner join tblmanufacturers mfg on mfg.manufacturerid=ao.manufacturerid " +
                          " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + " and cust.customerid=ao.customerid order by mfg.manufacturername ";

                }
                else
                {
                    query = "select distinct ao.manufacturerid as [Key], mfg.manufacturername as Value  from gsnetp_tbl_assetsowned ao " +
                             "  inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1 " +
                             " inner join tblmanufacturers mfg on mfg.manufacturerid=ao.manufacturerid " +
                             " inner join tblofferings ofe on ofe.offeringshort='SUPPORT' inner join tblsfacustomerwa wa on wa.sfaid=" + sfaId
                             + " and wa.offeringid=ofe.offeringid and wa.customerid=ao.customerid and wa.isactive=1 " +
                             " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + "and cust.customerid=ao.customerid order by mfg.manufacturername";

                }
                var results = _dbCon.Database.SqlQuery<GenericList>(query).ToList();


                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetManufacturersList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetManufacturersList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturersList");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting category list
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<GenericList> GetCategoryList(int companyId, string userGroup, int sfaId)
        {
            try
            {
                string query = string.Empty;
                if (userGroup == "ISD")
                {
                    query = "select distinct ao.customerpartcategoryid as [Key], pc.partcategoryname as Value" +
                " from gsnetp_tbl_assetsowned ao" +
                " inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1" +
                " inner join tblpartcategories pc on pc.partcategoryid=ao.customerpartcategoryid" +
                " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + " and cust.customerid=ao.customerid order by pc.partcategoryname";

                }
                else
                {
                    query = "select distinct ao.customerpartcategoryid as [Key], pc.partcategoryname as Value" +
                  " from gsnetp_tbl_assetsowned ao" +
                  " inner join gsnetp_tbl_assets a on a.assetsownedid=ao.assetsownedid and a.isactive=1" +
                  " inner join tblpartcategories pc on pc.partcategoryid=ao.customerpartcategoryid" +
                  " inner join tblofferings ofe on ofe.offeringshort='SUPPORT'" +
                  " inner join tblsfacustomerwa wa on wa.sfaid=" + sfaId +
                  "and wa.offeringid=ofe.offeringid and wa.customerid=ao.customerid and wa.isactive=1 " +
                  " inner join tblcustomers cust on cust.parentcompanyid=" + companyId + "and cust.customerid=ao.customerid order by pc.partcategoryname";

                }
                var results = _dbCon.Database.SqlQuery<GenericList>(query).ToList();


                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetCategoryList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetCategoryList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCategoryList");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting status list
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<GenericList> GetStatusList(int companyId)
        {
            try
            {
                var chkCount = "select assetstatusid from GSNetP_tbl_AssetStatus where isactive=1 and companyid= " + companyId;
                var chkResult = _dbCon.Database.SqlQuery<int>(chkCount).ToList();
                var query = "";
                if (chkResult.Count > 0)
                {
                    query = "select assetstatusid as [Key], status as Value from GSNetP_tbl_AssetStatus" +
                                       " left join tblParentCompanies on GSNetP_tbl_assetstatus.CompanyID=tblParentCompanies.parentcompanyid" +
                                       " where GSNetP_tbl_assetstatus.isActive=1 and companyid =" + companyId +
                                       " order by parentcompanyname, statusIntValue";
                }
                else
                {
                    query = "select assetstatusid as [Key], status as Value from GSNetP_tbl_AssetStatus " +
                                " left join tblParentCompanies on GSNetP_tbl_assetstatus.CompanyID=tblParentCompanies.parentcompanyid" +
                                " where GSNetP_tbl_assetstatus.isActive=1 and companyid =" + companyId + " or companyid is null" +
                                " order by parentcompanyname, statusIntValue";
                }

                var results = _dbCon.Database.SqlQuery<GenericList>(query).ToList();

                return results;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetStatusList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetStatusList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetStatusList");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        ///  this method is responsible for getting asset tag search popup data
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroup"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public AssetPopupModel GetAssetTagSearchPopupData(int companyId, string userGroup, int sfaId)
        {
            try
            {
                AssetPopupModel obj = new Model.AssetPopupModel();

                obj.AssetTagAddress = GetAssetCountryAddress(companyId, userGroup, sfaId);
                obj.AssetManufacturList = GetManufacturersList(companyId, userGroup, sfaId);
                obj.AssetCategoryList = GetCategoryList(companyId, userGroup, sfaId);
                obj.AssetStatuses = GetStatusList(companyId);
                obj.ParentCompanyId = companyId;
                return obj;

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetAssetTagSearchPopupData<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetAssetTagSearchPopupData");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetTagSearchPopupData");
                }
                return new AssetPopupModel();
            }
        }

        /// <summary>
        ///  this method is responsible for getting getting asset list after filter record
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="serialNumber"></param>
        /// <param name="assetLocationId"></param>
        /// <param name="assetStatusId"></param>
        /// <param name="assetCategoryId"></param>
        /// <param name="assetManufactureId"></param>
        /// <param name="assetPartNumber"></param>
        /// <param name="assetTag"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<Sp_GetAssetPopupGrid_AdminR_Result> GetAssetDataGridDataList(string companyId, string userGroupName, string serialNumber, string assetLocationId,
                        string assetStatusId, string assetCategoryId, string assetManufactureId, string assetPartNumber, string assetTag, string sfaId)
        {
            try
            {
                return _dbCon.Sp_GetAssetPopupGrid_AdminR(userGroupName, companyId, serialNumber, assetTag, assetLocationId == "0" ? "" : assetLocationId, assetManufactureId, assetPartNumber, assetCategoryId, assetStatusId == "0" ? "" : assetStatusId, sfaId).ToList();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetAssetDataGridDataList<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetAssetDataGridDataList");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAssetDataGridDataList");
                }
                return new List<Sp_GetAssetPopupGrid_AdminR_Result>();
            }

        }

        /// <summary>
        ///  this method is responsible for getting disposition assets
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public TicketDispositionAssetsModel GetDispositionAssets(int ticketId)
        {
            try
            {
                TicketDispositionAssetsModel obj = new Model.TicketDispositionAssetsModel();
                obj.CurrencyList = _dbCon.tblCurrencies.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.CurrencyID,
                    Value = x.CurrencyName
                }).OrderBy(x => x.Value).ToList();

                var dispositionSummary = _dbCon.tblDispositionSummaries.FirstOrDefault(x => x.TicketID == ticketId);
                if (dispositionSummary != null)
                {
                    obj.CurrencyId = dispositionSummary.CurrencyID;
                    obj.PreTaxInvoiceAmount = dispositionSummary.PreTaxInvoiceAmount;
                    obj.DispositionAssets = _dbCon.tblDispositionAssets.Where(x => x.DispositionSummaryID == dispositionSummary.DispositionSummaryID).ToList();

                    var attachment = "select daid, docid, daname, daurl, isnull(mimetypeicon, 'blank.gif') as mimetypeicon " +
                    "from tblDigitalAssets a right outer join tblMimeTypes b on b.mimetypeid = a.damimetypeid " +
                    "where daid in (select daid from tblDispositionAssetAttachments where  DispositionSummaryID = " + dispositionSummary.DispositionSummaryID + ")";
                    obj.DispositionAssetAttachments = _dbCon.Database.SqlQuery<TicketDispositionAssetsAttachment>(attachment).ToList();
                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>GetDispositionAssets<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>GetDispositionAssets");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetDispositionAssets");
                }
                return new TicketDispositionAssetsModel();
            }

        }

        /// <summary>
        ///  this method is responsible for adding disposition asset to summary
        /// </summary>
        /// <param name="ticketID"></param>
        /// <param name="currencyID"></param>
        /// <param name="preTaxInvoiceAmount"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public int AddDispositionAssetSummary(int ticketID, int currencyID, decimal? preTaxInvoiceAmount, int userID)
        {
            try
            {
                int count = 0;
                if (ticketID > 0)
                {
                    var dispositionSummary = _dbCon.tblDispositionSummaries.FirstOrDefault(x => x.TicketID == ticketID);
                    if (dispositionSummary != null)
                    {
                        using (var transaction = _dbCon.Database.BeginTransaction())
                        {
                            dispositionSummary.PreTaxInvoiceAmount = preTaxInvoiceAmount;
                            dispositionSummary.CurrencyID = currencyID;
                            dispositionSummary.LastUpdated = DateTime.Now;
                            dispositionSummary.LastUpdatedByUserID = userID;
                            _dbCon.Entry(dispositionSummary).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                            transaction.Commit();

                        }
                    }
                    else
                    {
                        using (var transaction = _dbCon.Database.BeginTransaction())
                        {
                            dispositionSummary = new Entity.tblDispositionSummary();
                            dispositionSummary.TicketID = ticketID;
                            dispositionSummary.PreTaxInvoiceAmount = preTaxInvoiceAmount;
                            dispositionSummary.CurrencyID = currencyID;
                            dispositionSummary.LastUpdatedByUserID = userID;
                            dispositionSummary.IsActive = true;
                            _dbCon.tblDispositionSummaries.Add(dispositionSummary);
                            _dbCon.SaveChanges();
                        }
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>AddDispositionAssetSummary<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter =>AddDispositionAssetSummary");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDispositionAssetSummary");
                }
                return 0;
            }
        }

        /// <summary>
        ///  this method is responsible for adding digital assets
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url, int userId)
        {
            try
            {
                return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                      null, null, null, null, null, null, null, 0, null, null
                      , null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>ticket Search Presenter => AddDigitalAsset<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "ticket Search Presenter => AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        /// <summary>
        ///  this method is responsible for adding digital asset for ticket
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url, int userId, int ticketId)
        {
            try
            {
                return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                      null, ticketId, null, null, null, null, null, null, null, null
                      , null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>ticket Search Presenter => AddDigitalAsset<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "ticket Search Presenter => AddDigitalAsset");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDigitalAsset");
                }
                return 0;
            }
        }

        /// <summary>
        ///  this method is responsible for uploading disposition assets
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currencyId"></param>
        /// <param name="ticketId"></param>
        /// <param name="daid"></param>
        /// <param name="sessionUser"></param>
        /// <param name="dateFormatID"></param>
        /// <returns></returns>
        public int UploadDispositionAssets(List<UploadTicketDispositionAssets> model, int currencyId, int ticketId, int? daid, int sessionUser, int dateFormatID)
        {
            int modelCount = 1;
            try
            {
                int count = 0;
                //Select CurrencyID from tblCurrencies where CurrencySymbol='USD'
                var usdCurrencyId = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencySymbol == "USD").CurrencyID;
                var DispositionSummaryID = _dbCon.tblDispositionSummaries.FirstOrDefault(x => x.TicketID == ticketId).DispositionSummaryID;
                tblDispositionAsset obj;
                using (var transaction = _dbCon.Database.BeginTransaction())
                {
                    foreach (var item in model)
                    {
                        obj = new Entity.tblDispositionAsset();
                        obj.DispositionSummaryID = DispositionSummaryID;
                        obj.Model = item.Model;
                        obj.SerialNumber = item.SerialNumber;
                        obj.AssetTag = string.IsNullOrEmpty(item.AssetTag) ? null : item.AssetTag;
                        obj.OEM = string.IsNullOrEmpty(item.OEM) ? null : item.OEM;
                        obj.Category = string.IsNullOrEmpty(item.Category) ? null : item.Category;
                        obj.ScreenSize = string.IsNullOrEmpty(item.ScreenSize) ? null : item.ScreenSize;
                        obj.Processor = string.IsNullOrEmpty(item.Processor) ? null : item.Processor;
                        obj.RAM = string.IsNullOrEmpty(item.RAM) ? null : item.RAM;
                        obj.HardDrive = string.IsNullOrEmpty(item.HardDrive) ? null : item.HardDrive;
                        obj.ServiceType = string.IsNullOrEmpty(item.ServiceType) ? null : item.ServiceType;
                        obj.GraphicsCardPartNumber = string.IsNullOrEmpty(item.GraphicsCardPartNumber) ? null : item.GraphicsCardPartNumber;
                        obj.GraphicsCardSerialNumber = string.IsNullOrEmpty(item.GraphicsCardSerialNumber) ? null : item.GraphicsCardSerialNumber;
                        obj.RouteSwitchProcessorPartNumber = string.IsNullOrEmpty(item.RouteSwitchProcessorPartNumber) ? null : item.RouteSwitchProcessorPartNumber;
                        obj.RouteSwitchProcessorSerialNumber = string.IsNullOrEmpty(item.RouteSwitchProcessorSerialNumber) ? null : item.RouteSwitchProcessorSerialNumber;
                        obj.OpticalDrivePartNumber = string.IsNullOrEmpty(item.OpticalDrivePartNumber) ? null : item.OpticalDrivePartNumber;
                        obj.OpticalDriveSerialNumber = string.IsNullOrEmpty(item.OpticalDriveSerialNumber) ? null : item.OpticalDriveSerialNumber;
                        obj.Comments = string.IsNullOrEmpty(item.Comments) ? null : item.Comments;

                        if (string.IsNullOrEmpty(item.RemarketingAmount))
                        {
                            obj.RemarketingAmount = null;
                            obj.RemarketingAmountUSD = null;
                        }
                        else
                        {
                            obj.RemarketingAmount = Convert.ToDecimal(item.RemarketingAmount);
                            obj.RemarketingAmountUSD = _dbCon.sp_convertCurrencyNew(currencyId, usdCurrencyId, Convert.ToDecimal(item.RemarketingAmount)).FirstOrDefault();
                        }

                        obj.LastUpdatedByUserID = sessionUser;
                        obj.IsActive = true;
                        obj.LastUpdated = DateTime.Now;

                        _dbCon.tblDispositionAssets.Add(obj);
                        count = _dbCon.SaveChanges();

                    }
                    if (daid != null)
                    {
                        int cDaId = Convert.ToInt32(daid);
                        var att = _dbCon.tblDispositionAssetAttachments.FirstOrDefault(x => x.DAID == cDaId);
                        if (att != null)
                        {
                            att.DispositionSummaryID = DispositionSummaryID;
                            att.LastUpdated = DateTime.Now;
                            att.LastUpdatedByUserID = sessionUser;
                            _dbCon.Entry(att).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                        }
                    }

                    transaction.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter =>UploadDispositionAssets<<<<<-- Error at Row =>" + modelCount + " Exception=>" + ex.ToString());
                StaticHelper.LogException("Ticket Search Presenter =>Error at Row => " + modelCount + " Exception=> " + ex.ToString(), "UploadDispositionAssets");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Ticket Search Presenter =>UploadDispositionAssets");
                }
                return -1;
            }
        }

        /// <summary>
        ///  this method is responsible for getting requested url of attachment
        /// </summary>
        /// <param name="daid"></param>
        /// <returns></returns>
        public string GetRequestedAttachmentUrl(int daid)
        {
            try
            {
                string url = string.Empty;
                var digitalAsset = _dbCon.tblDigitalAssets.FirstOrDefault(x => x.DAID == daid);
                if (digitalAsset != null)
                {
                    url = digitalAsset.DAURL;
                }
                return url;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => GetRequestedAttachmentUrl<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => GetRequestedAttachmentUrl");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRequestedAttachmentUrl");
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// this method use for deleting attachment
        /// </summary>
        /// <param name="daid"></param>
        /// <returns></returns>
        public int DeleteAttachment(int daid)
        {
            int count = 0;
            try
            {
                using (var transcation = _dbCon.Database.BeginTransaction())
                {

                    var DispositionAssetAttachments = _dbCon.tblDispositionAssetAttachments.FirstOrDefault(x => x.DAID == daid);
                    if (DispositionAssetAttachments != null)
                    {
                        _dbCon.tblDispositionAssetAttachments.Remove(DispositionAssetAttachments);
                        count = _dbCon.SaveChanges();
                    }

                    var digitalAsset = _dbCon.tblDigitalAssets.FirstOrDefault(x => x.DAID == daid);
                    if (digitalAsset != null)
                    {
                        _dbCon.tblDigitalAssets.Remove(digitalAsset);
                        count = _dbCon.SaveChanges();
                    }
                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => DeleteAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Order Search Presenter => DeleteAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteAttachment");
                }
            }
            return count;
        }

        /// <summary>
        /// this method use for marking ticket attachment Isactive false
        /// </summary>
        /// <returns></returns>
        public int MarkIsActiveTicketAttachment(int daid, int userid)//Adnan 19-june-2019
        {
            int count = 0;
            try
            {
                using (var transcation = _dbCon.Database.BeginTransaction())
                {

                    var tblTicketAttachments = _dbCon.tblTicketAttachments.FirstOrDefault(x => x.daid == daid);
                    if (tblTicketAttachments != null)
                    {
                        tblTicketAttachments.isActive = false;
                        tblTicketAttachments.lastUpdate = DateTime.Now;
                        tblTicketAttachments.lastUpdateUserID = userid;
                        count = _dbCon.SaveChanges();
                    }

                    
                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => MarkIsActiveTicketAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => MarkIsActiveTicketAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteAttachment");
                }
            }
            return count;
        }
        /// <summary>
        ///  this method is responsible for deleting disposition assets
        /// </summary>
        /// <param name="dispositionAssetIds"></param>
        /// <returns></returns>
        public int DeleteDispositionAssets(string dispositionAssetIds)
        {
            try
            {
                int count = 0;

                var arrDispositionAssetIds = dispositionAssetIds.Split(',').Select(int.Parse).ToArray();
                var dispositionAssets = _dbCon.tblDispositionAssets.Where(x => arrDispositionAssetIds.Contains(x.DispositionAssetID)).ToList();
                if (dispositionAssets.Count > 0)
                {
                    _dbCon.tblDispositionAssets.RemoveRange(dispositionAssets);
                    count = _dbCon.SaveChanges();
                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => DeleteDispositionAssets<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => DeleteDispositionAssets");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteDispositionAssets");
                }
                return 0;
            }
        }

        /// <summary>
        ///  this method is responsible for getting ticket history
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public TicketHistoryModel GetTicketHistory(int ticketId)
        {
            try
            {
                TicketHistoryModel obj = new Model.TicketHistoryModel();
                obj.LstTicketHistory = (from tt in _dbCon.tblTicketTrackings
                                        join
                                          a in _dbCon.tblActions on tt.ActionID equals a.ActionID into ps
                                        from a in ps.DefaultIfEmpty()
                                        join ts in _dbCon.tblTicketStatus on tt.StatusID equals ts.TktStatusID into ps1
                                        from ts in ps1.DefaultIfEmpty()
                                        join u in _dbCon.tblUsers on tt.LastUpdateUserID equals u.UserID into ps2
                                        from u in ps2.DefaultIfEmpty()
                                        where tt.TicketID == ticketId
                                        select new TicketHistory
                                        {
                                            Comments = tt.Comment,
                                            Logdate = tt.Logdate,
                                            UserName = u.UserName,
                                            ActionName = a.ActionName,
                                            TktStatusName = ts.TktStatusName,
                                            IsPublic = tt.IsPublic,
                                            TicketLogID = tt.TicketLogID,
                                        }).OrderByDescending(x => x.TicketLogID).ToList();
                obj.resolution = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketId).ResolutionText;
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => GetTicketHistory<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => GetTicketHistory");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketHistory");
                }
                return new Model.TicketHistoryModel();
            }
        }

        /// <summary>
        /// this method is responsible for updating ticket log
        /// </summary>
        /// <param name="ticketLogID"></param>
        /// <param name="userId"></param>
        /// <param name="isPublic"></param>
        /// <returns></returns>
        public int UpdateTicketLog(int ticketLogID, int userId, bool isPublic)
        {
            try
            {
                return _dbCon.sp_changeTicketLogVisible(ticketLogID, isPublic, userId);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => UpdateTicketLog<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => UpdateTicketLog");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateTicketLog");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for updating ticket resolution
        /// </summary>
        /// <param name="ticketID"></param>
        /// <param name="serverTimeZone"></param>
        /// <param name="userId"></param>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public int UpdateTicketResolution(int ticketID, int serverTimeZone, int userId, string resolution)
        {
            int count = 0;
            try
            {
                var ticket = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketID);
                if (ticket != null)
                {
                    DateTime logdate = DateTime.Now;
                    var actionRequest = _dbCon.tblActionRequests.FirstOrDefault(x => x.ARID == ticket.ARID);
                    if (actionRequest.RequestTimezoneID != null)
                    {
                        var fieldIds = _dbCon.tblTimeZones.Where(x => x.TimeZoneID == actionRequest.RequestTimezoneID).Select(x => x.FieldID).ToList();

                        var tzCount = _dbCon.tblFields.FirstOrDefault(x => fieldIds.Contains(x.fieldID));
                        if (tzCount != null)
                        {
                            var Timeconvert = new GSTime.TimeConverter();
                            logdate = Timeconvert.ConvertTime(DateTime.Now, serverTimeZone, Convert.ToInt32(tzCount.vbConst));
                        }
                        else
                        {
                            var Timeconvert = new GSTime.TimeConverter();
                            logdate = Timeconvert.ConvertTime(DateTime.Now, serverTimeZone, 1);
                        }

                        //update ticket
                        ticket.ResolutionText = resolution.Trim();
                        ticket.ResolutionDate = logdate;

                        _dbCon.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                        //

                        //add tracking 
                        int? actionId = null;
                        var actions = _dbCon.tblActions.Where(x => x.IsActive == true).ToList();
                        foreach (var item in actions)
                        {
                            if (item.ActionName.ToUpper() == "ADD RESOLUTION TO TICKET")
                            {
                                actionId = item.ActionID;
                            }
                        }
                        tblTicketTracking obj = new tblTicketTracking();
                        obj.TicketID = ticketID;
                        obj.StatusID = Convert.ToInt32(ticket.TktStatusID);
                        obj.ActionID = actionId;
                        obj.Comment = "Resolution: " + resolution;
                        obj.IsActive = true;
                        obj.Logdate = logdate;
                        obj.LastUpdateUserID = userId;
                        obj.IsPublic = true;

                        _dbCon.tblTicketTrackings.Add(obj);
                        count = _dbCon.SaveChanges();
                        //
                    }
                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => UpdateTicketResolution<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => UpdateTicketResolution");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateTicketResolution");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for updating ticket comments
        /// </summary>
        /// <param name="ticketID"></param>
        /// <param name="contactId"></param>
        /// <param name="serverTimeZone"></param>
        /// <param name="userId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public int UpdateTicketComment(int ticketID, int contactId, int serverTimeZone, int userId, string comments)
        {
            try
            {
                int count = 0;
                var ticket = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketID);
                if (ticket != null)
                {
                    int? actionId = null;
                    var actions = _dbCon.tblActions.Where(x => x.IsActive == true).ToList();
                    foreach (var item in actions)
                    {
                        if (item.ActionName.ToUpper() == "ADD A COMMENT TO A TICKET")
                        {
                            actionId = item.ActionID;
                        }
                    }

                    DateTime cLogDate = DateTime.Now;

                    var actionRequest = _dbCon.tblActionRequests.FirstOrDefault(x => x.ARID == ticket.ARID);
                    if (actionRequest != null)
                    {
                        var fieldIds = _dbCon.tblTimeZones.Where(x => x.TimeZoneID == actionRequest.RequestTimezoneID).Select(x => x.FieldID).ToList();

                        var tzCount = _dbCon.tblFields.FirstOrDefault(x => fieldIds.Contains(x.fieldID));
                        if (tzCount != null)
                        {
                            var Timeconvert = new GSTime.TimeConverter();
                            cLogDate = Timeconvert.ConvertTime(DateTime.Now, serverTimeZone, Convert.ToInt32(tzCount.vbConst));
                        }
                        else
                        {
                            var Timeconvert = new GSTime.TimeConverter();
                            cLogDate = Timeconvert.ConvertTime(DateTime.Now, serverTimeZone, 1);
                        }
                    }


                    tblTicketTracking obj = new tblTicketTracking();
                    obj.TicketID = ticketID;
                    obj.StatusID = Convert.ToInt32(ticket.TktStatusID);
                    obj.ActionID = actionId;
                    obj.Comment = comments;
                    obj.IsActive = true;
                    obj.Logdate = cLogDate;
                    obj.IsPublic = true;
                    obj.LastUpdateUserID = userId;

                    _dbCon.tblTicketTrackings.Add(obj);
                    count = _dbCon.SaveChanges();




                    var ar = new ActionRequest();
                    ar.SubmitUserID = userId;
                    ar.AffectedContactID = contactId;
                    ar.RequestDateLocal = DateTime.Now;
                    ar.RequestDate = DateTime.Now;
                    ar.RequestDateTimeZoneVBConst = serverTimeZone;
                    ar.GSTicketID = ticketID;
                    ar.Qualify = false;
                    ar.CategoryName = "Ticket Change";

                    var aut = new GSSupport.ActionUpdateTicket();
                    if (!string.IsNullOrEmpty(comments))
                    {
                        aut.IsAddComment = true;
                        ar.AddFieldByDisplay("Ticket Comment Added", "True", "True");
                    }

                    var am = new GSSupport.ActionMonitor();
                    aut.IsComplete = true;
                    aut.IsError = false;
                    aut.GSTicketID = ticketID;
                    aut.UserID = userId;

                    if (ticket.TktStatusID != null && Convert.ToInt32(ticket.TktStatusID) > 0)
                    {
                        aut.StatusID = Convert.ToInt32(ticket.TktStatusID);
                    }


                    am.Action = aut;
                    ar.CompletedAction = am;
                    ar.IsCompletedAction = true;
                    ar.Submit();
                }


                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => UpdateTicketComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => UpdateTicketComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateTicketComment");
                }
                return 0;
            }
        }

        /// <summary>
        /// this method is responsible for getting ticket attachment
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public List<sp_GetAttachments_R_Result> GetTicketAttachment(int ticketId)//Adnan 19-june-2019
        {
            try
            {
                var ticketAttachments = _dbCon.tblTicketAttachments.Where(x => x.ticketid == ticketId && x.isActive == true).Select(x => x.daid).ToList();
                if (ticketAttachments != null)
                {
                    var data = (from a in _dbCon.tblDigitalAssets
                                join
                                  b in _dbCon.tblMimeTypes on a.DAMimeTypeID equals b.MimeTypeID into ps
                                from b in ps.DefaultIfEmpty()
                                where ticketAttachments.Contains(a.DAID)
                                select new { daid = a.DAID, daname = a.DAName, docid = a.DocID, daurl = a.DAURL, mimetypeicon = b.MimeTypeIcon }).ToList();
                    List<sp_GetAttachments_R_Result> lst = new List<sp_GetAttachments_R_Result>();
                    sp_GetAttachments_R_Result att;
                    foreach (var item in data)
                    {
                        att = new sp_GetAttachments_R_Result();
                        att.daid = item.daid;
                        att.daname = item.daname;
                        att.docid = item.docid;
                        att.daurl = item.daurl;
                        att.mimetypeicon = item.mimetypeicon ?? "blank.gif";
                        lst.Add(att);
                    }
                    return lst;
                }
                return new List<sp_GetAttachments_R_Result>();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => GetTicketAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => GetTicketAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTicketAttachment");
                }
                return new List<sp_GetAttachments_R_Result>();
            }
        }

        /// <summary>
        /// this method is responsible for sending ticket email
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="serverTimeZone"></param>
        /// <param name="userId"></param>
        /// <param name="username"></param>
        /// <param name="emailaddres"></param>
        /// <param name="emailMessage"></param>
        /// <returns></returns>
        public int SendTicketEmail(int ticketId, int serverTimeZone, int userId, string username, string emailaddres, string emailMessage)
        {
            try
            {
                string sfaemailstring = string.Empty;
                var ar = new GSSupport.ActionRequest();
                ar.GSTicketID = Convert.ToInt32(ticketId);
                ar.Qualify = false;
                ar.RequestDateTimeZoneVBConst = serverTimeZone;
                ar.GenerateTicket = false;
                ar.CategoryName = "System";
                ar.Summary = "Email Ticket Request";
                ar.AddFieldByDisplay("To", emailaddres);

                var ticketDetail = _dbCon.sp_getTicketDetail_AdminR(ticketId).FirstOrDefault();

                foreach (var item in GetSupplierContacts(Convert.ToInt32(ticketDetail.sfaid)))
                {
                    if (item.Email != "-")
                    {
                        sfaemailstring = sfaemailstring + item.Email + ";";
                    }
                }

                if (!string.IsNullOrEmpty(sfaemailstring))
                {
                    ar.AddFieldByDisplay("Email of Assigned FSC", sfaemailstring.Substring(0, sfaemailstring.Length - 1));
                }
                else
                {
                    ar.AddFieldByDisplay("Email of Assigned FSC", sfaemailstring);
                }

                ar.AddFieldByDisplay("Body", emailMessage);
                ar.AddFieldByDisplay("Submitting User", userId.ToString(), username);
                ar.AddFieldByDisplay("Ticket Submitted By", ticketDetail.submittedbyuserid.ToString(), ticketDetail.username);
                ar.AddFieldByDisplay("Ticket Summary", ticketDetail.arname);
                ar.AddFieldByDisplay("Ticket Description", ticketDetail.ardesc);
                ar.AddFieldByDisplay("Email of Submitting User", ticketDetail.submittinguseremail);
                ar.AddFieldByDisplay("Email of Assigned CSR", ticketDetail.csremail);

                if (ticketDetail.parentcompanyid > 0 && !string.IsNullOrEmpty(ticketDetail.parentcompanyname))
                {
                    ar.AddFieldByDisplay("Ticket Company of Affected User", ticketDetail.parentcompanyid.ToString(), ticketDetail.parentcompanyname);
                }
                if (ticketDetail.contactid != null)
                {
                    ar.AddFieldByDisplay("Ticket Affected User", ticketDetail.contactid.ToString(), ticketDetail.firstname + " " + ticketDetail.lastname);
                }
                ar.AddFieldByDisplay("Ticket Priority", ticketDetail.priorityid.ToString(), ticketDetail.priorityname);
                ar.AddFieldByDisplay("Ticket Status", ticketDetail.tktstatusid.ToString(), ticketDetail.tktstatusname);
                ar.AddFieldByDisplay("Ticket Submitted Date", (ticketDetail.requestdatelocal == null ? string.Empty : Convert.ToDateTime(ticketDetail.requestdatelocal).ToString()), ticketDetail.requestdatelocal + " " + ticketDetail.requestgmtoffsetstring);

                var action = _dbCon.tblActions.FirstOrDefault(x => x.ActionName == "EMAIL TICKET" || x.ActionName == "email ticket" || x.ActionName == "Email Ticket");
                if (action != null)
                {
                    ar.RequestedActionID = action.ActionID;
                }
                ar.Submit();

                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => SendTicketEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => SendTicketEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "SendTicketEmail");
                }
                return 0;
            }
        }

        public string GetArtypeName(int ticketId)
        {
            string artypeName = string.Empty;
            var ticket = _dbCon.tblTickets.FirstOrDefault(x => x.TicketID == ticketId);
            if (ticket != null)
            {
                var actionRequestType = _dbCon.tblActionRequestTypes.FirstOrDefault(x => x.ARTypeID == ticket.TktTypeID);
                if (actionRequestType != null)
                {
                    artypeName = actionRequestType.ARTypeName ?? "";
                }
            }
            return artypeName;
        }

        public ADAttachmentDetailModel GetADAttachmentDetail(int ticketId)
        {

            var data = (from t in _dbCon.tblTickets
                        join ar in _dbCon.tblActionRequests on t.ARID
                        equals ar.ARID
                        join c in _dbCon.tblContacts on ar.AffectedUserContactID equals c.ContactID
                        join ca in _dbCon.tblCustomerAddresses on ar.AddressID equals ca.CustomerAddressID
                        where t.TicketID == ticketId
                        select new ADAttachmentDetailModel { ConEmail = c.Email, CompanyId = ar.CompanyID, CountryId = ca.CountryID }).FirstOrDefault();

            return data;
        }

        public void SendAssetDispAttachEmail(string to, string from, string cc, string bcc, string userGroupName, int ticket, string userName, string SFAName,string filepath,string fileName)
        {
            try
            {
                var emailBody = "This email is to notify you that your Asset Disposition  ticket <b>" + ticket + " </b> has new document attachments uploaded.<br><br>";
                if (userGroupName == "SFA")
                {
                    emailBody = emailBody + "On Behalf of:  <b>" + userName + " at " + SFAName + "</b>";
                }
                OneSourceAdminCore.Helper.StaticHelper.SendEmailMessageWithAttachment(to, from, cc, bcc, emailBody, 
                    "GlobalServe ONESOURCE - Asset Disposition Ticket Attachment (Mars) -" + ticket, filepath,fileName);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Ticket Search Presenter => SendAssetDispAttachEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Ticket Search Presenter => SendAssetDispAttachEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "SendAssetDispAttachEmail");
                }

            }
        }

        public string GetStatus(int statusId,int companyId)
        {
            var status = _dbCon.tblTicketStatus.FirstOrDefault(x => x.TktStatusID == statusId && x.CompanyID == companyId);
            if (status != null)
            {
                return status.TktStatusName;
            }
            return string.Empty;
        }
        public string GetPriority(int priorityId,int companyId)
        {
            var priority = _dbCon.tblPriorities.FirstOrDefault(x => x.PriorityID == priorityId && x.CompanyID == companyId);
            if (priority != null)
            {
                return priority.PriorityName;
            }
            return string.Empty;
        }

        public string GetTimeZoneByTimeZoneId(int id)
        {
            return _dbCon.tblTimeZones.FirstOrDefault(x => x.TimeZoneID == id).TimeZoneName;
        }

        /// <summary>
        /// Send Email Notification on Add Ticket Comment
        /// Created by : Sunny Gohar
        /// Dated : 13th November 2018
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="userId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public bool AddTicketCommentNotification(int ticketId,int userId,string comment)
        {
            try
            {
                UserPresenter up = new UserPresenter();
                var contact = up.GetContactInfo(userId);

                string sMailTo = "";
                var missingEmail = _dbCon.tblPrimeContractors.Select(x => x.Email).FirstOrDefault(); //missing email
                var ticketEmail = GetTicketEmail(); // ticket email

                var ticket = _dbCon.sp_getTicketDetail_AdminR(ticketId).FirstOrDefault();
                var emailcc = contact!=null?contact.Email:"";
                var mailto = ticket!=null?ticket.submittinguseremail:"";
                var userName = ticket != null ? ticket.username ?? "-": "-";

                if (string.IsNullOrWhiteSpace(mailto))
                {
                    if (string.IsNullOrWhiteSpace(ticketEmail))
                    {
                        mailto = missingEmail;
                    }
                    else
                    {
                        mailto = ticketEmail;
                    }
                }
                else
                {
                    mailto = mailto + ";" + ticketEmail;
                }

                if (string.IsNullOrWhiteSpace(emailcc.Trim()))
                {
                    emailcc = missingEmail;
                }

                var id = ticket != null ? ticket.sfaid : 0;
                int SFAId = Convert.ToInt32(id);
                var SFAEmail = _dbCon.tblSFAs.Where(x => x.SFAID == SFAId).Select(x => x.Email).FirstOrDefault();

                if (string.IsNullOrEmpty(SFAEmail))
                {
                    sMailTo = "";
                }
                else
                {
                    sMailTo = SFAEmail;
                }

                List<string> contactEmails = _dbCon.sp_GetContactEmailBySFAId_R(SFAId).Distinct().ToList();

                if (contactEmails.Count > 0)
                {
                    foreach (var email in contactEmails)
                    {
                        if (string.IsNullOrEmpty(sMailTo))
                        {
                            sMailTo = email;
                        }
                        else
                        {
                            if (!sMailTo.Contains(email))
                            {
                                sMailTo = sMailTo + ";" + email;
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(sMailTo))
                {
                    sMailTo = missingEmail;
                }

                var emailSubject = "Comment Added - Ticket # " + ticket.ticketid;
                var emailBody = "";
                emailBody = emailBody + "Ticket Status  : " + ticket.tktstatusname + "\n";
                emailBody = emailBody + "Commented By   : " + userName + "\n";
                emailBody = emailBody + "Comment        : " + comment + "\n\n\n";
                emailBody = emailBody +
                            "Please logon to https://gsonesource.global-serve.com to review and/or authorize/approve the basket order.\n\n" +
                            "Thank you for using GlobalServe ONESOURCE.\n\nIf you need to contact GlobalServe,\n\n" +
                            "please call or fax the Customer Center at: \n\nAmericas \n phone: +1 (914) 729 6800 \n " +
                            "fax : +1-201-408-5275 \n\n" +
                            "GlobalServe ONESOURCE: https://gsonesource.global-serve.com";

                OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, "Automailer@global-serve.com", emailcc, "", emailBody,
                    emailSubject);

                var bodyForSupplier = emailBody.Replace("https://gsonesource.global-serve.com",
                         " https://onesource.global-serve.com");

                StaticHelper.LogException("Sending email to : Supplier", "");
                StaticHelper.LogException("Sending email to: " + sMailTo, "");

                OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(sMailTo, "Automailer@global-serve.com", "", "", bodyForSupplier,
                             emailSubject);
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddTicketCommentNotification");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddTicketCommentNotification");
                }
                return false;
            }
        }

        /// <summary>
        /// Get Ticket Email
        /// Created by : Sunny Gohar
        /// Dated : 13th November 2018
        /// </summary>
        /// <returns>PrimeContractor</returns>
        public string GetTicketEmail()
        {
            return  _dbCon.tblPrimeContractors.Where(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1)
                    .Select(x => x.TicketEmail).FirstOrDefault() ?? "";
        }

    }
}

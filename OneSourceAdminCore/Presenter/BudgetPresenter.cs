﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Country budget data"
    /// </summary>
    public class BudgetPresenter
    {
        private readonly globalserveEntities _dbCon;
        
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public BudgetPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// get the customer's budget record
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<sp_GetCustomerBudgetRecord_R_Result> GetCustomerBudgetRecord(int value)
        {
            return _dbCon.sp_GetCustomerBudgetRecord_R(value).ToList();
        }
        
        /// <summary>
        /// update the budget record
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateCustomerBudget(List<tblCustomerCountryBudget> model, int userId)
        {
            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                int customerId = model[0].CustomerID;
                var data = _dbCon.tblCustomerCountryBudgets.Where(x => x.CustomerID == customerId).ToList();
                _dbCon.tblCustomerCountryBudgets.RemoveRange(data);

                foreach (var item in model)
                {
                    //if (item.CountryID == 0)
                    //{
                    //    var data = _dbCon.tblCustomerCountryBudgets.FirstOrDefault(x => x.CustomerID == item.CustomerID && x.CountryID == 0);
                    //    if (data != null)
                    //    {
                    //        _dbCon.tblCustomerCountryBudgets.Remove(data);
                    //    }
                    //}
                    //else
                    //{
                    //    var data = _dbCon.tblCustomerCountryBudgets.FirstOrDefault(x => x.CustomerID == item.CustomerID && x.CountryID == item.CountryID);
                    //    if (data != null)
                    //    {
                    //        _dbCon.tblCustomerCountryBudgets.Remove(data);
                    //    }

                    //}


                    item.LastUpdated = DateTime.Now;
                    item.LastUpdatedbyUser = userId;

                    _dbCon.tblCustomerCountryBudgets.Add(item);
                    count = _dbCon.SaveChanges();
                }

                transaction.Commit();

            }
            return count;

        }
        /// <summary>
        /// update the customer's site budget
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateCustomerSiteBudget(List<tblCustomerSiteBudget> model, int userId)
        {
            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                foreach (var item in model)
                {
                    var data = _dbCon.tblCustomerSiteBudgets.Where(x => x.CustomerID == item.CustomerID && x.CountryID == item.CountryID).ToList();
                    if (data != null)
                    {
                        _dbCon.tblCustomerSiteBudgets.RemoveRange(data);
                    }
                }


                foreach (var item in model)
                {
                    item.LastUpdated = DateTime.Now;
                    item.LastUpdatedByUser = userId;

                    _dbCon.tblCustomerSiteBudgets.Add(item);
                    count = _dbCon.SaveChanges();
                }

                transaction.Commit();

            }
            return count;
        }

        /// <summary>
        /// delete the customer site budget
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int DeleteCustomerSiteBudget(tblCustomerSiteBudget model, int userId)
        {
            int count = 0;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblCustomerSiteBudgets.Where(x => x.CustomerID == model.CustomerID).ToList();
                if (data != null)
                {
                    _dbCon.tblCustomerSiteBudgets.RemoveRange(data);
                    count = _dbCon.SaveChanges();
                }
                transaction.Commit();
            }
            return count;
        }

        /// <summary>
        /// Get the currency amount by the customer's country
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public BudgetCurrencyModel GetCustomerCurrencyAmountByCountry(int customerId)
        {
            var data = _dbCon.tblCustomerCountryBudgets.Where(x => x.CustomerID == customerId && x.CountryID == 0).Select(x => new BudgetCurrencyModel
            {
                CurrencyID = x.CurrencyID,
                BudgetAmount = x.BudgetAmount,
                Duration = x.Duration
            }).FirstOrDefault();

            if (data == null)
                data = new BudgetCurrencyModel() { CurrencyID = 0, BudgetAmount = 0 };
            return data;

        }

        /// <summary>
        /// Get the currency amount by the customer
        /// </summary>
        /// <param name="currencyId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public decimal? GetTotalAmountByCurrencyCustomer(int currencyId, int customerId)
        {
            decimal? amountValue = null;
            var data = _dbCon.tblCustomerCountryBudgets.FirstOrDefault(x => x.CurrencyID == currencyId && x.CountryID == 0 && x.CustomerID == customerId);
            if (data != null)
            {
                amountValue = data.BudgetAmount;
            }
            return amountValue;
        }

        /// <summary>
        /// Get all the active country of customer
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public List<sp_GetCountriesByCustomer_R_Result> GetCountryByCustomer(int CustomerId)
        {
            return _dbCon.sp_GetCountriesByCustomer_R(CustomerId).OrderBy(x => x.CountryName).ToList();
        }

        /// <summary>
        /// Get Total amount of country set by the customer
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public decimal? GetTotalAmountByCountryCustomer(int countryId, int customerId)
        {
            decimal? amountValue = null;
            var data = _dbCon.tblCustomerCountryBudgets.FirstOrDefault(x => x.CountryID == countryId && x.CustomerID == customerId);
            if (data != null)
            {
                amountValue = data.BudgetAmount;
            }
            return amountValue;
        }
        /// <summary>
        /// Get currency value by the country and customer 
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public decimal? GetCurrencyByCountryCustomer(int countryId, int customerId)
        {
            int? curValue = null;
            var data = _dbCon.tblCustomerCountryBudgets.FirstOrDefault(x => x.CountryID == countryId && x.CustomerID == customerId);
            if (data != null)
            {
                curValue = data.CurrencyID;
            }
            return curValue;
        }

        /// <summary>
        /// Get all address of country by customer id
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<sp_GetAddressByCustomerCountry_R_Result> GetAddressByCustomerCountry(int countryId, int customerId)
        {
            //  _dbCon.Configuration.ProxyCreationEnabled = false;
            return _dbCon.sp_GetAddressByCustomerCountry_R(countryId, customerId).ToList();
        }

    }
}

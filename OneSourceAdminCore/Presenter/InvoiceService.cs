﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Helper;
using System.Web.Mvc;
using System.Data;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Data.Entity.Core.Objects;

namespace OneSourceAdminCore.Presenter
{
    public class InvoiceService
    {
        private globalserveEntities dbCon;


        public InvoiceService()
        {
            dbCon = new globalserveEntities();
        }

        public List<GenericList> GetInvoiceStatusList()
        {
            try
            {
                var list = dbCon.tblInvoiceStatus.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.InvoiceStatusID,
                    Value = x.InvoiceStatusName

                }).OrderBy(x => x.Value).ToList();
                list.Add(new GenericList() { Key = 0, Value = "None" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceStatusList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceStatusList");
                }
                return new List<GenericList>();
            }

        }


        public List<GenericList> GetInvoiceTypeList()
        {
            try
            {
                var list = dbCon.tblInvoiceTypes.Where(x => x.IsActive == true).Select(x => new GenericList()
                {
                    Key = x.InvoiceTypeID,
                    Value = x.InvoiceTypeName

                }).OrderBy(x => x.Value).ToList();
                list.Add(new GenericList() { Key = 0, Value = "None" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceTypeList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceTypeList");
                }
                return new List<GenericList>();
            }
        }

        public List<SelectListItem> GetInvoiceTypeActiveOrInActiveList()
        {
            try
            {
                var list = dbCon.tblInvoiceTypes.Select(x => new SelectListItem()
                {
                    Value = x.InvoiceTypeID.ToString(),
                    Text = x.IsActive ? "(A)" + x.InvoiceTypeName : "(IA)" + x.InvoiceTypeName

                }).OrderBy(x => x.Text).ToList();
                list.Add(new SelectListItem() { Value = "0", Text = "None" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceTypeList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceTypeList");
                }
                return new List<SelectListItem>();
            }
        }

        public List<GenericList> GetSentToList()
        {
            try
            {
                var list = dbCon.Sp_GetInvoiceSentToCompanyList_AdminR().Select(x => new GenericList()
                {
                    Key = x.ParentCompanyID,
                    Value = x.ParentCompanyName

                }).OrderBy(x => x.Value).ToList();
                list.Add(new GenericList() { Key = 0, Value = "None" });
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSentToList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSentToList");
                }
                return new List<GenericList>();
            }
        }


        public List<GenericList> GetOnBehalfOfList()
        {
            try
            {
                var list = dbCon.Sp_GetOnBehalfOfCompanyList_AdminR().Select(x => new GenericList()
                {
                    Key = x.ParentCompanyID,
                    Value = x.ParentCompanyName

                }).OrderBy(x => x.Value).ToList();

                list.Add(new GenericList() { Key = 0, Value = "None" });

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetOnBehalfOfList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetOnBehalfOfList");
                }
                return new List<GenericList>();
            }
        }

        public List<Sp_GetInvoiceList_AdminR_Result> GetIGetInvoicesListnvoicesList(InvoiceModel data, AdminSetting setting)
        {
            try
            {

                var list = dbCon.Sp_GetInvoiceList_AdminR(data.InvoiceNumber, data.PoNumber, data.OrderTrackingNumber,
                    data.OrderPoNumber, data.SentToCompanyId, data.OnBehlafCompanyId, data.StatusId,
                    data.TypeId, setting.DateFormatID, setting.ParentCompanyID.ToString(),
                    setting.UserGroupName.ToUpper(), data.CreatedAfter, data.CreatedBefore, data.SentAfter,
                    data.SentBefore, data.RecievedAfter, data.RecievedBefore).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoicesList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoicesList");
                }
                return new List<Sp_GetInvoiceList_AdminR_Result>();
            }
        }


        public InvoicedetailsModel GetInvoiceDetails(int invoiceId)
        {
            try
            {

                var model = new InvoicedetailsModel();

                model.InvoiceInfo = dbCon.Sp_GetInvoiceDetails_AdminR(invoiceId).FirstOrDefault();
                model.InvoiceId = invoiceId;

                return model;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoicedetails");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoicedetails");
                }
                return new InvoicedetailsModel();
            }
        }

        /// <summary>
        /// Get Currencies with symbol
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetCurrencies(int currencyId = 0)
        {
            try
            {
                var currencies = dbCon.tblCurrencies.Where(x => x.IsActive).OrderBy(x => x.CurrencySymbol).ToList();

                var list = currencies.Select(x => new SelectListItem()
                {

                    Value = x.CurrencyID.ToString(),
                    Text = x.CurrencySymbol + "(" + x.CurrencyName + ")",
                    Selected = currencyId > 0 ? (x.CurrencyID == currencyId ? true : false) : false

                }).Distinct().ToList();

                return list;
            }
            catch (Exception ex)

            {
                StaticHelper.LogException(ex.ToString(), "GetCurrencies");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCurrencies");
                }
                return new List<SelectListItem>();
            }
        }
        /// <summary>
        /// Get Status List
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="securityLevel"></param>
        /// <param name="invoiceStatusId"></param>
        /// <param name="userGroup"></param>
        /// <returns></returns>
        public InvoiceDetailStatuses GetStatusList(int contactId, int securityLevel, int invoiceStatusId, string userGroup)
        {
            try
            {
                var list = new List<SelectListItem>();

                if (userGroup == "ISD")
                {

                    var query =
                        "select count(pr.roleid) from tblpartyrole pr inner join tblcontact c on c.contactid=" +
                        contactId + " and c.partyid=pr.partyid" +
                        " and c.isactive=1 inner join tblrole r on r.rolename='Finance' and r.roleid=pr.roleid and r.isactive=1 where pr.isactive=1";

                    var result = dbCon.Database.SqlQuery<int>(query).FirstOrDefault();


                    if (securityLevel >= 3 || result > 0)
                    {
                        list = dbCon.tblInvoiceStatus.Where(x => x.IsActive).Select(x => new SelectListItem()
                        {
                            Text = x.InvoiceStatusName,
                            Value = x.InvoiceStatusID.ToString(),
                            Selected = invoiceStatusId > 0 ? (x.InvoiceStatusID.ToString() == invoiceStatusId.ToString() ? true : false) : false
                        }).ToList();


                        var obj = new InvoiceDetailStatuses()
                        {
                            EnableDropdown = true,
                            List = list
                        };

                        return obj;
                    }
                    else
                    {
                        var chkNewStatusId =
                            dbCon.tblInvoiceStatus.FirstOrDefault(x => x.InvoiceStatusName == "New");


                        if (chkNewStatusId != null && chkNewStatusId.InvoiceStatusID == invoiceStatusId)
                        {

                            list =
                                dbCon.tblInvoiceStatus.Where(
                                    x => (x.InvoiceStatusName == "New" || x.InvoiceStatusName == "Cancelled"))
                                    .Select(x => new SelectListItem()
                                    {
                                        Text = x.InvoiceStatusName,
                                        Value = x.InvoiceStatusID.ToString(),
                                        Selected = invoiceStatusId > 0 ? (x.InvoiceStatusID.ToString() == invoiceStatusId.ToString() ? true : false) : false
                                    }).ToList();

                            var obj = new InvoiceDetailStatuses()
                            {
                                EnableDropdown = true,
                                List = list
                            };

                            return obj;
                        }
                    }


                }

                list = dbCon.tblInvoiceStatus.Where(x => x.IsActive).Select(x => new SelectListItem()
                {
                    Text = x.InvoiceStatusName,
                    Value = x.InvoiceStatusID.ToString(),
                    Selected = invoiceStatusId > 0 ? (x.InvoiceStatusID.ToString() == invoiceStatusId.ToString() ? true : false) : false
                }).ToList();

                var data = new InvoiceDetailStatuses()
                {
                    EnableDropdown = false,
                    List = list
                };

                return data;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetStatusList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetStatusList");
                }
                return null;
            }
        }
        /// <summary>
        /// Get Invoice Owner List
        /// </summary>
        /// <param name="userGroup"></param>
        /// <param name="billFromCompanyId"></param>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public List<SelectListItem> GetInvoiceOwnerList(string userGroup, int billFromCompanyId = 0, int invoiceId = 0) //Modified (Sunny Gohar : 15th Aug 2018)
        {
            try
            {
                StaticHelper.LogException("userGroup : "+userGroup+ ",billFromCompanyId : "+billFromCompanyId.ToString()+ ",invoiceId : "+invoiceId.ToString(), "Params For GetInvoiceOwnerList");
                var list = new List<SelectListItem>();

                if (userGroup == "ISD")
                {

                    var query =
                        "select contactid as 'Key', isnull(c.firstname, '') + ' ' + isnull(c.lastname, '') as 'Value' from tblcontact c" +
                        " inner join tblrole r on r.rolename = 'Invoice' and r.isactive = 1 inner join tblpartyrole pr on" +
                        " pr.partyid = c.partyid and pr.roleid = r.roleid and pr.isactive = 1  where c.isactive = 1 order by c.firstname, c.lastname";

                    if (billFromCompanyId > 0)
                    {
                        query =
                       "select contactid as 'Key', isnull(c.firstname, '') + ' ' + isnull(c.lastname, '') as 'Value' from tblcontact c" +
                       " inner join tblrole r on r.rolename = 'Invoice' and r.isactive = 1 inner join tblpartyrole pr on" +
                       " pr.partyid = c.partyid and pr.roleid = r.roleid and pr.isactive = 1  where" +
                       " c.companyid =" + billFromCompanyId +" and c.isactive = 1 order by c.firstname, c.lastname";
                    }

                    StaticHelper.LogException(query.ToString(), "Query For GetInvoiceOwnerList");

                    var result = dbCon.Database.SqlQuery<GenericList>(query).ToList();
                    var query2 = "select min(contactid) as contactid from tblinvoiceowners where invoiceid=" + invoiceId;
                    var selected = dbCon.Database.SqlQuery<int>(query2).FirstOrDefault();

                    StaticHelper.LogException(query2.ToString(), "Query2 For GetInvoiceOwnerList");

                    list = result.Select(x => new SelectListItem()
                    {
                        Text = x.Value,
                        Value = x.Key.ToString(),
                        Selected = selected > 0 ? (selected == x.Key ? true : false) : false
                    }).Distinct().ToList();

                    return list;
                }
                return new List<SelectListItem>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceOwnerList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceOwnerList");
                }
                return new List<SelectListItem>();
            }
        }

        /// <summary>
        /// Get Party Location List
        /// </summary>
        /// <param name="userGroup"></param>
        /// <param name="partyId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public List<SelectListItem> GetPartyLocationList(string userGroup, int addressId = 0, int parentCompanyId = 0)
        {
            try
            {
                var list = new List<SelectListItem>();

                var query =
                    "select pa.addressid as 'Key', IsNull(pa.addressName, '') +' ' + IsNull(a.city, '') + ' ' " +
                    "+ IsNull(a.StateProvinceRegion, '') as 'Value' from tblAddress a, " +
                    "tblPartyAddress pa where a.addressid = pa.addressid and a.isActive = 1";

                var partyId = 0;
                if (parentCompanyId > 0)
                {
                    partyId = dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == parentCompanyId) != null ? Convert.ToInt32(
                        dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == parentCompanyId).PartyID) : 0;
                }

                if (partyId > 0)
                {
                    query =
                   "select pa.addressid as 'Key', IsNull(pa.addressName, '') +' ' + IsNull(a.city, '') + ' ' " +
                   "+ IsNull(a.StateProvinceRegion, '') as 'Value' from tblAddress a, " +
                   "tblPartyAddress pa where a.addressid = pa.addressid and pa.partyid = '" + partyId + "' and a.isActive = 1";
                }

                var result = dbCon.Database.SqlQuery<GenericList>(query).ToList();

                list = result.Select(x => new SelectListItem()
                {
                    Text = x.Value,
                    Value = x.Key.ToString(),
                    Selected = addressId > 0 ? (x.Key == addressId ? true : false) : false

                }).Distinct().ToList();

                return list;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetePartyLocationList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetePartyLocationList");
                }
                return new List<SelectListItem>();
            }
        }
        /// <summary>
        /// Get Payment Terms
        /// </summary>
        /// <param name="invoiceTermId"></param>
        /// <returns></returns>
        public List<SelectListItem> GetPaymentTermsList(int invoiceTermId = 0)
        {
            try
            {
                var list = dbCon.tblInvoiceTerms.Where(x => x.IsActive).Select(x => new SelectListItem()
                {
                    Text = x.InvoiceTermName,
                    Value = x.InvoiceTermID.ToString(),
                    Selected = invoiceTermId > 0 ? (x.InvoiceTermID == invoiceTermId ? true : false) : false

                }).OrderBy(x => x.Value).ToList();

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPaymentTermsList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPaymentTermsList");
                }
                return new List<SelectListItem>();
            }
        }

        public tblInvoiceType GetInvoiceType(int invoiceTypeId)
        {
            try
            {
                var type = dbCon.tblInvoiceTypes.FirstOrDefault(x => x.InvoiceTypeID == invoiceTypeId);

                return type;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceType");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceType");
                }
                return null;
            }
        }
        /// <summary>
        /// Get Payment Methods
        /// </summary>
        /// <param name="paymentId"></param>
        /// <returns></returns>
        public List<SelectListItem> GetPaymentMethodsList(int paymentId = 0)
        {
            try
            {
                var list = dbCon.tblPaymentMethods.Where(x => x.IsActive).Select(x => new SelectListItem()
                {
                    Value = x.PaymentMethodID.ToString(),
                    Text = x.PaymentMethodName,
                    Selected = paymentId > 0 ? (x.PaymentMethodID == paymentId ? true : false) : false

                }).OrderBy(x => x.Value).ToList();

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetPaymentMethodsList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPaymentMethodsList");
                }
                return new List<SelectListItem>();
            }
        }

        /// <summary>
        /// Get OBO Company list
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetCompanyList(int companyId = 0)
        {
            try
            {
                var list = dbCon.tblParentCompanies.Where(x => x.IsActive).Select(x => new SelectListItem()
                {
                    Text = x.ParentCompanyName,
                    Value = x.ParentCompanyID.ToString(),
                    Selected = companyId > 0 ? (x.ParentCompanyID == companyId ? true : false) : false

                }).OrderBy(x => x.Text).ToList();

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCompanyList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCompanyList");
                }
                return new List<SelectListItem>();
            }
        }
        /// <summary>
        /// Get Gsp Monthly Id
        /// </summary>
        /// <returns></returns>
        public int GetGspMonthlyId()
        {
            try
            {
                var gspMonthlyId = dbCon.tblInvoiceTypes.FirstOrDefault(x => x.InvoiceTypeName == "Procurement - GSP Monthly") != null ?
                           dbCon.tblInvoiceTypes.FirstOrDefault(x => x.InvoiceTypeName == "Procurement - GSP Monthly").InvoiceTypeID : 0;

                return gspMonthlyId;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetGspMonthlyId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetGspMonthlyId");
                }
                return 0;
            }
        }
        /// <summary>
        /// Get Invoice Total Payment Value
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public double GetTotalInvoiceTotalPayment(int invoiceId)
        {
            try
            {
                var query = "select IsNull(sum(amount), 0) from tblInvoicePayments where invoiceid=" + invoiceId;

                var totalPayment = dbCon.Database.SqlQuery<double>(query).FirstOrDefault();

                return totalPayment;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetTotalInvoiceTotalPayment");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetTotalInvoiceTotalPayment");
                }
                return 0;
            }
        }
        /// <summary>
        /// Get Invoice details
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="invoiceTypeId"></param>
        /// <param name="vatRate"></param>
        /// <param name="vatTotal"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public DataTable GetInvoiceDetailData(int invoiceId, int invoiceTypeId, string vatRate = null, string vatTotal = null, string total = null)
        {
            try
            {
                DataTable dt = new DataTable();
                var list = dbCon.tblInvoiceLineItemDetails.Where(x => x.InvoiceId == invoiceId && x.InvoiceTypeId == invoiceTypeId).ToList();
                dt = ToDataTable(list);
                //var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                //var builder = new EntityConnectionStringBuilder(efConnectionString);
                //var regularConnectionString = builder.ProviderConnectionString;

                //SqlConnection con = new SqlConnection(regularConnectionString);
                //con.Open();

                //using (SqlCommand command = new SqlCommand("sp_GetInvoiceLineItemDetails_AdminR", con))
                //{
                //    command.CommandType = CommandType.StoredProcedure;

                //    command.Parameters.Add(new SqlParameter("@invoiceId", SqlDbType.Int));
                //    command.Parameters.Add(new SqlParameter("@invoiceTypeId", SqlDbType.Int));
                //    command.Parameters[0].Value = invoiceId;
                //    command.Parameters[1].Value = invoiceTypeId;

                //    using (SqlDataAdapter da = new SqlDataAdapter(command))
                //    {
                //        da.Fill(dt);
                //    }
                //}

                var subTotal = decimal.Zero;
                var count = 0;
                var finalDt = dt.Clone();
                for (int i = 0; i < finalDt.Columns.Count; i++)
                {
                    if (finalDt.Columns[i].DataType != typeof(string))
                        finalDt.Columns[i].DataType = typeof(string);
                }

                foreach (DataRow dr in dt.Rows)
                {
                    finalDt.ImportRow(dr);
                }

                if (finalDt != null)
                {
                    if (finalDt.Rows.Count > 0)
                    {

                        foreach (DataRow row in finalDt.Rows)
                        {
                            count++;
                            if (finalDt.Columns.Contains("DetailId"))
                            {
                                finalDt.Columns.Remove("DetailId");
                            }
                            if (finalDt.Columns.Contains("Description"))
                            {
                                if (row["Description"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("Description");
                                }
                            }
                            if (finalDt.Columns.Contains("GSFee"))
                            {
                                if (row["GSFee"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("GSFee");
                                }
                            }
                            if (finalDt.Columns.Contains("OrderNumber"))
                            {
                                if (row["OrderNumber"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("OrderNumber");
                                }
                            }
                            if (finalDt.Columns.Contains("OrderTotal"))
                            {
                                if (row["OrderTotal"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("OrderTotal");
                                }
                            }
                            if (finalDt.Columns.Contains("PO"))
                            {
                                finalDt.Columns.Remove("PO");
                            }
                            if (finalDt.Columns.Contains("Quantity"))
                            {
                                if (row["Quantity"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("Quantity");
                                }
                            }
                            if (finalDt.Columns.Contains("Rate"))
                            {
                                if (row["Rate"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("Rate");
                                }
                            }
                            if (finalDt.Columns.Contains("Amount"))
                            {
                                if (row["Amount"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("Amount");
                                }
                            }
                            if (finalDt.Columns.Contains("Ticket"))
                            {
                                if (row["Ticket"].ToString() == "")
                                {

                                    finalDt.Columns.Remove("Ticket");
                                }
                            }
                            if (finalDt.Columns.Contains("VAT"))
                            {
                                finalDt.Columns.Remove("VAT");
                            }
                            if (finalDt.Columns.Contains("TotalValue"))
                            {
                                var totalval = decimal.Zero;
                                decimal.TryParse(row["TotalValue"].ToString(), out totalval);
                                subTotal += totalval;
                            }
                            if (finalDt.Columns.Contains("LineItemSeqNum"))
                            {
                                finalDt.Columns.Remove("LineItemSeqNum");
                            }
                            if (finalDt.Columns.Contains("ColDisplaySeqNum"))
                            {
                                finalDt.Columns.Remove("ColDisplaySeqNum");
                            }
                            if (finalDt.Columns.Contains("InvoiceId"))
                            {
                                finalDt.Columns.Remove("InvoiceId");
                            }
                            if (finalDt.Columns.Contains("InvoiceTypeId"))
                            {
                                finalDt.Columns.Remove("InvoiceTypeId");
                            }
                            if (finalDt.Rows.Count == count)
                            {
                                if (finalDt.Columns.Contains("TotalValue"))
                                {
                                    finalDt.Columns.Remove("TotalValue");
                                }
                            }

                        }

                        finalDt.Rows.Add();
                        finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 2] = "<b>SUB TOTAL:</b>";
                        finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 1] = subTotal;

                        var gspMonthlyId = GetGspMonthlyId();

                        if (gspMonthlyId != invoiceTypeId)
                        {
                            dynamic vRate = "";
                            dynamic vTotal = "";
                            if (!String.IsNullOrEmpty(vatRate))
                            {
                                vRate = Convert.ToDecimal(vatRate) * 100;
                                vRate = vRate.ToString("0.00");
                            }
                            finalDt.Rows.Add();
                            finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 2] = "<b>VAT RATE(%):</b>";
                            finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 1] = vRate;

                            vTotal = Convert.ToDecimal(vatTotal);
                            vTotal = vTotal.ToString("0.00");

                            finalDt.Rows.Add();
                            finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 2] = "<b>VAT TOTAL:</b>";
                            finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 1] = vTotal;
                        }

                        var payment = dbCon.tblInvoicePayments.Where(x => x.InvoiceID == invoiceId).ToList().Sum(x => x.Amount);
                        finalDt.Rows.Add();
                        finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 2] = "<b>PAYMENTS/CREDITS:</b>";
                        finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 1] = payment.ToString("0.00");

                        var invoiceTotal = decimal.Zero;
                        if (!String.IsNullOrEmpty(total))
                        {
                            invoiceTotal = Convert.ToDecimal(total) - payment;
                        }
                        finalDt.Rows.Add();
                        finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 2] = "<b>TOTAL:</b>";
                        finalDt.Rows[finalDt.Rows.Count - 1][finalDt.Columns.Count - 1] = invoiceTotal.ToString("0.00");
                    }
                }
                return finalDt;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceDetailData");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceDetailData");
                }
            }

            return null;
        }

        /// <summary>
        /// Update Invoice details
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="invoiceCurrencyID"></param>
        /// <param name="billToAddressID"></param>
        /// <param name="billFromAddressID"></param>
        /// <param name="payToAddressID"></param>
        /// <param name="senddate"></param>
        /// <param name="invoiceStatusID"></param>
        /// <param name="invoicePeriodStart"></param>
        /// <param name="invoicePeriodEnd"></param>
        /// <param name="vatReg"></param>
        /// <param name="taxdate"></param>
        /// <param name="userId"></param>
        /// <param name="prefPaymentID"></param>
        /// <param name="recdDate"></param>
        /// <param name="paymentInstr"></param>
        /// <param name="invoiceTermID"></param>
        /// <param name="comments"></param>
        /// <param name="dueDate"></param>
        /// <param name="vatrate"></param>
        /// <param name="po"></param>
        /// <param name="invoicetypeid"></param>
        /// <returns></returns>
        public int UpdateInvoice(int? invoiceId, int? invoiceCurrencyID, int? billToAddressID, int? billFromAddressID, int? payToAddressID,
            DateTime? senddate, int? invoiceStatusID, DateTime? invoicePeriodStart, DateTime? invoicePeriodEnd, string vatReg, DateTime? taxdate,
            int? userId, int? prefPaymentID, DateTime? recdDate, string paymentInstr, int? invoiceTermID, string comments, DateTime? dueDate, double? vatrate,
            string po, int? invoicetypeid)
        {
            try
            {
                return dbCon.sp_UpdateInvoice(invoiceId, invoiceCurrencyID, billToAddressID, billFromAddressID, payToAddressID, senddate, invoiceStatusID,
                     invoicePeriodStart, invoicePeriodEnd, vatReg, taxdate, userId, prefPaymentID, recdDate, paymentInstr, invoiceTermID, comments, dueDate, vatrate,
                     po, invoicetypeid);
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateInvoice");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateInvoice");
                }
                return 0;
            }
        }

        public bool UpdateInvoiceType(tblInvoiceType invoiceType, int invoiceTypeId,int userId)
        {
            try
            {
                var oldRecord = dbCon.tblInvoiceTypes.Where(x => x.InvoiceTypeID == invoiceTypeId).FirstOrDefault();
                var currentRecord = oldRecord;
                currentRecord.InvoiceTypeName = invoiceType.InvoiceTypeName;
                currentRecord.InvoiceTypeDesc = invoiceType.InvoiceTypeDesc;
                currentRecord.ParentTypeID = invoiceType.ParentTypeID;
                currentRecord.CompanyID = invoiceType.CompanyID;
                currentRecord.BillFromAddressID = invoiceType.BillFromAddressID;
                currentRecord.BillToAddressID = invoiceType.BillToAddressID;
                currentRecord.PayToAddressID = invoiceType.PayToAddressID;
                currentRecord.BillToCompanyID = invoiceType.BillToCompanyID;
                currentRecord.StartDate = invoiceType.StartDate;
                currentRecord.EndDate = invoiceType.EndDate;
                currentRecord.InvoiceCurrencyID = invoiceType.InvoiceCurrencyID;
                currentRecord.InvoiceAmount = invoiceType.InvoiceAmount;
                currentRecord.InvoiceAmountUSD = invoiceType.InvoiceAmountUSD;
                currentRecord.VATRate = invoiceType.VATRate;
                currentRecord.NumberStr = invoiceType.NumberStr;
                currentRecord.PO = invoiceType.PO;
                currentRecord.Recurs = invoiceType.Recurs;
                if (invoiceType.OwnerContactID > 0)
                {
                    currentRecord.OwnerContactID = invoiceType.OwnerContactID;
                }
                currentRecord.InvoiceTermID = invoiceType.InvoiceTermID;
                currentRecord.DayOfMonth = invoiceType.DayOfMonth;
                currentRecord.PaymentInstr = invoiceType.PaymentInstr;
                currentRecord.PrefPaymentType = invoiceType.PrefPaymentType;
                currentRecord.VATReg = invoiceType.VATReg;
                currentRecord.Comments = invoiceType.Comments;
                currentRecord.IsActive = invoiceType.IsActive;
                currentRecord.LastUpdate = DateTime.Now;
                currentRecord.LastUpdateUserID = userId;

                dbCon.Entry(oldRecord).CurrentValues.SetValues(currentRecord);
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateInvoiceType");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateInvoiceType");
                }
            }
            return false;
        }


        public int AddInvoice(AdminSetting setting,string companyinvoiceid, Nullable<int> companyid, Nullable<int> billtocompanyid, Nullable<int> invoicetypeid,
            Nullable<System.DateTime> senddate, Nullable<int> invoicecurrencyid, Nullable<System.DateTime> invoiceperiodstart, Nullable<System.DateTime> invoiceperiodend,
            string vatreg, Nullable<System.DateTime> taxdate, Nullable<int> billtoaddressid, Nullable<int> billfromaddressid, Nullable<int> preferredpaymentmethodid,
            Nullable<int> paymenttoaddressid, string paymentinstr, Nullable<int> invoicetermid, Nullable<decimal> invoicetotal, Nullable<decimal> invoicetotalusd,
            string comments, Nullable<System.DateTime> duedate, Nullable<int> invoicerecipient, Nullable<int> invoiceowner, Nullable<int> userid, Nullable<System.DateTime> createdate,
            Nullable<double> vatrate, string po, Nullable<int> obocompanyid)
        {
            try
            {
                ObjectParameter invoiceid = new ObjectParameter("invoiceid", typeof(int));

                dbCon.sp_AddInvoice_AdminR(companyinvoiceid, setting.ParentCompanyID, billtocompanyid, invoicetypeid, senddate, invoicecurrencyid, invoiceperiodstart, invoiceperiodend,
                    vatreg, taxdate, billtoaddressid, billfromaddressid, preferredpaymentmethodid, paymenttoaddressid, paymentinstr, invoicetermid, invoicetotal, invoicetotalusd,
                   comments, duedate, invoicerecipient, invoiceowner, userid, createdate, vatrate, po, obocompanyid, invoiceid);

                var id = 0;
                Int32.TryParse(invoiceid.Value.ToString(), out id);
                return id;


            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddInvoice");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddInvoice");
                }
                return 0;
            }
        }

        public decimal AddInvoiceLineItem(InvoiceLineItemDetailsModel item, int invoiceId, int invoiceTypeId, int userId, int lineItemSeqNum)
        {
            int count = 0;
            decimal invoiceTotal = decimal.Zero;
            try
            {
                using (var transcation = dbCon.Database.BeginTransaction())
                {

                    var invoiceLineItem = new tblInvoiceLineItemDetail();
                    invoiceLineItem.Description = item.Description;
                    invoiceLineItem.OrderNumber = item.OrderNumber;
                    decimal OrderTotal = decimal.Zero;
                    decimal.TryParse(item.OrderTotal, out OrderTotal);
                    decimal GSFee = decimal.Zero;
                    decimal.TryParse(item.GSFee, out GSFee);
                    decimal VAT = decimal.Zero;
                    decimal.TryParse(item.VAT, out VAT);
                    invoiceLineItem.OrderTotal = OrderTotal;
                    invoiceLineItem.GSFee = GSFee;
                    invoiceLineItem.PO = item.PO;
                    invoiceLineItem.Quantity = item.Quantity;
                    invoiceLineItem.Rate = item.Rate;
                    invoiceLineItem.VAT = VAT;
                    invoiceLineItem.Amount = item.Amount;
                    invoiceLineItem.Ticket = item.Ticket;
                    invoiceLineItem.TotalValue = item.TotalValue;
                    invoiceLineItem.InvoiceId = invoiceId;
                    invoiceLineItem.InvoiceTypeId = invoiceTypeId;
                    invoiceLineItem.LineItemSeqNum = lineItemSeqNum.ToString();

                    dbCon.tblInvoiceLineItemDetails.Add(invoiceLineItem);
                    count = dbCon.SaveChanges();

                    if (count > 0)
                    {
                        var colsCount = dbCon.sp_AddInvoiceCols(invoiceId, invoiceLineItem.Description, invoiceLineItem.PO, item.VAT, invoiceLineItem.Rate,
                            invoiceLineItem.Ticket, invoiceLineItem.Quantity, invoiceLineItem.OrderNumber, item.OrderTotal, item.GSFee, userId);

                        if (colsCount > 0)
                        {
                            var result = dbCon.sp_UpdateInvoiceTotal(invoiceId);

                            if (result > 0)
                            {
                                invoiceTotal = dbCon.tblInvoices.FirstOrDefault(x => x.InvoiceID == invoiceId).InvoiceTotal;
                            }
                        }
                    }

                    transcation.Commit();

                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Invoice Service => AddInvoiceLineItem<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Invoice Service => AddInvoiceLineItem");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddInvoiceLineItem");
                }
            }
            return invoiceTotal;
        }

        /// <summary>
        /// Delete invoice owner by invoice id
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public int DeleteInvoiceOwnerByInvoiceId(int invoiceId)
        {
            int count = 0;
            try
            {
                using (var transcation = dbCon.Database.BeginTransaction())
                {

                    var invoiceOwner = dbCon.tblInvoiceOwners.FirstOrDefault(x => x.InvoiceID == invoiceId);
                    if (invoiceOwner != null)
                    {
                        dbCon.tblInvoiceOwners.Remove(invoiceOwner);
                        count = dbCon.SaveChanges();
                    }

                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Invoice Service => DeleteInvoiceOwnerByInvoiceId<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Invoice Service => DeleteInvoiceOwnerByInvoiceId");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteInvoiceOwnerByInvoiceId");
                }
            }
            return count;
        }

        public decimal DeleteLineItemDetail(int invoiceId, int seqNum)
        {
            int count = 0;
            decimal invoiceTotal = decimal.Zero;
            try
            {
                using (var transcation = dbCon.Database.BeginTransaction())
                {

                    var lineItems = dbCon.tblInvoiceDetails.Where(x => x.InvoiceID == invoiceId && x.LineItemSeqNum == seqNum).ToList();
                    if (lineItems.Count > 0)
                    {
                        dbCon.tblInvoiceDetails.RemoveRange(lineItems);
                        count = dbCon.SaveChanges();
                        if (count > 0)
                        {
                            var result = dbCon.sp_UpdateInvoiceTotal(invoiceId);
                            if (result > 0)
                            {
                                invoiceTotal = dbCon.tblInvoices.FirstOrDefault(x => x.InvoiceID == invoiceId).InvoiceTotal;
                            }
                        }

                    }

                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Invoice Service => DeleteLineItemDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Invoice Service => DeleteLineItemDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteLineItemDetail");
                }
            }
            return invoiceTotal;
        }
        /// <summary>
        /// Add invoice owner
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="invoiceOwnerId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddInvoiceOwner(int invoiceId, int invoiceOwnerId, int userId)
        {
            int count = 0;
            try
            {
                using (var transcation = dbCon.Database.BeginTransaction())
                {

                    var invoiceOwner = new tblInvoiceOwner();
                    invoiceOwner.InvoiceID = invoiceId;
                    invoiceOwner.ContactID = invoiceOwnerId;
                    invoiceOwner.LastUpdateUserID = userId;
                    invoiceOwner.IsActive = true;
                    invoiceOwner.LastUpdate = DateTime.Now;
                    dbCon.tblInvoiceOwners.Add(invoiceOwner);
                    count = dbCon.SaveChanges();


                    transcation.Commit();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Invoice Service => AddInvoiceOwner<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Invoice Service => AddInvoiceOwner");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddInvoiceOwner");
                }
            }
            return count;
        }
        /// <summary>
        /// Method to convert any list to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);

                dataTable.Columns.Add(prop.Name, type);


            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        public tblInvoiceType GetInvoiceTypeDetailByInvoiceTypeId(int invoiceTypeId)
        {
            try
            {
                return dbCon.tblInvoiceTypes.Where(x => x.InvoiceTypeID == invoiceTypeId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceTypeDetailByInvoiceTypeId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceTypeDetailByInvoiceTypeId");
                }
                return null;
            }
        }


        public List<SelectListItem> GetInvoiceRecipientList(int invoiceId = 0, int invoiceRecipient = 0, int invoiceTypeId = 0) //Modified (Sunny Gohar : 15th Aug 2018)
        {
            try
            {
                dynamic invoiceRecipientObj = null;
                var invoiceRecipientId = 0;
                dynamic billToCompanyId = 0;

                if (invoiceId > 0)
                {
                    invoiceRecipientObj = dbCon.tblInvoiceRecipients.FirstOrDefault(x => x.InvoiceID == invoiceId);
                    invoiceRecipientId = invoiceRecipientObj != null ? invoiceRecipientObj.ContactID : 0;

                    billToCompanyId = dbCon.tblInvoices.FirstOrDefault(x => x.InvoiceID == invoiceId) != null ? dbCon.tblInvoices.FirstOrDefault(x => x.InvoiceID == invoiceId).BillToCompanyID :
                        dbCon.tblInvoiceTypes.FirstOrDefault(x => x.InvoiceTypeID == invoiceTypeId) != null ? dbCon.tblInvoiceTypes.FirstOrDefault(x => x.InvoiceTypeID == invoiceTypeId).BillToCompanyID : 0;
                }
                else if (invoiceRecipient > 0)
                {
                    invoiceRecipientId = invoiceRecipient;
                }
                else
                {
                    invoiceRecipientId = dbCon.tblInvoiceRecipients.FirstOrDefault(x => x.InvoiceTypeID == invoiceTypeId) != null ?
                        dbCon.tblInvoiceRecipients.FirstOrDefault(x => x.InvoiceTypeID == invoiceTypeId).ContactID : 0;
                }

                string query =
                    "select contactid as 'Key', isnull(firstname, '') + ' ' + isnull(lastname, '') as 'Value'" +
                    " from tblcontact where isActive=1 and companyid=" + billToCompanyId +
                    " order by firstname, lastname";

                var result = dbCon.Database.SqlQuery<GenericList>(query).ToList();

                var list = result.Select(x => new SelectListItem()
                {
                    Text = x.Value,
                    Value = x.Key.ToString(),
                    Selected = invoiceRecipientId > 0 ? (x.Key == invoiceRecipientId ? true : false) : false
                }).Distinct().ToList();
                list.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                return list;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceRecipientList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceRecipientList");
                }
                return new List<SelectListItem>();
            }
        }


        public List<string> GetInvoiceDetailColumnNames(int invoiceTypeId)
        {
            try
            {

                var query = from ic in dbCon.tblInvoiceCols
                                 join idc in dbCon.tblInvoiceDetailCols on ic.ColID equals idc.ColID
                                 where idc.IsActive == true && idc.ColName != "Amount" && ic.InvoiceTemplateID == invoiceTypeId && ic.IsActive == true
                                 orderby ic.ColDisplaySeqNum
                                 select idc.ColName;

                //var query = "select idc.colname from tblinvoicecols ic" +

                //            " inner join tblinvoicedetailcols idc on idc.colid=ic.colid and idc.isactive=1" +

                //            " and idc.colname <> 'Amount'" + " where ic.invoicetemplateid="+invoiceTypeId+" and ic.isactive=1 order by ic.coldisplayseqnum";

                //var result = dbCon.Database.SqlQuery<string>(query).ToList();

                var result = query.ToList();

                return result;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceDetailColumnNames");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceDetailColumnNames");
                }
                return new List<string>();
            }
        }

        public List<SelectListItem> GetReOccurs()
        {
            try
            {
                var query = "select unitofmeasureid as 'Key', unitofmeasurename as 'Value' from tblunitsofmeasure where isactive = 1" +

                            "order by unitofmeasurename";

                var result = dbCon.Database.SqlQuery<GenericList>(query).ToList();

                var list = result.Select(x => new SelectListItem()
                {
                    Value = x.Key.ToString(),
                    Text = x.Value

                }).OrderBy(x => x.Text).ToList();

                list.Add(new SelectListItem() { Value = "0", Text = "None" });

                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetReOccurs");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetReOccurs");
                }
                return new List<SelectListItem>();
            }
        }

        public string GetInvoiceAmountUSD(int invoicecurrency, decimal invoiceAmount)
        {
            try
            {
                StaticHelper.LogException("invoicecurrency : "+invoicecurrency+ ", invoiceAmount : "+invoiceAmount, "Params For GetInvoiceAmountUSD");

                var query = "select exchangeratetousdollars from tblprimecontractorexchangerates" +

                             "where currencyid= " + invoicecurrency + " and isactive=1";

                StaticHelper.LogException(query.ToString(), "Query For GetInvoiceAmountUSD");

                var result = dbCon.Database.SqlQuery<string>(query).ToList();

                var exchangeRate = result != null ? Convert.ToDecimal(result) : 0;
                decimal invoiceAmountUSD = decimal.Zero;
                if (exchangeRate > 0)
                {
                    invoiceAmountUSD = invoiceAmount / exchangeRate;
                }

                return invoiceAmountUSD.ToString();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetInvoiceAmountUSD");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetInvoiceAmountUSD");
                }
                return null;
            }
        }

        public int AddInvoiceLineItemDetails(string invoiceTypeId, Nullable<decimal> orderTotal, Nullable<decimal> gSFee, Nullable<decimal> vAT, string description = null, string orderNumber = null,
            string pO = null, string quantity = null, string rate = null, string amount = null, string ticket = null)
        {
            try
            {

                int count = 0;

                var obj = new tblInvoiceLineItemDetail();
                obj.InvoiceId = 0;
                obj.InvoiceTypeId = Convert.ToInt32(invoiceTypeId);
                obj.OrderTotal = orderTotal;
                obj.GSFee = gSFee;
                obj.VAT = vAT;
                obj.Description = description;
                obj.OrderNumber = orderNumber;
                obj.PO = pO;
                obj.Quantity = quantity;
                obj.Rate = rate;
                obj.Amount = amount;
                obj.Ticket = ticket;

                dbCon.tblInvoiceLineItemDetails.Add(obj);
                count = dbCon.SaveChanges();

                return count;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddInvoiceLineItemDetails");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddInvoiceLineItemDetails");
                }
                return 0;
            }
        }
    }
}

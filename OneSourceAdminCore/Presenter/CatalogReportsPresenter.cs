﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;

namespace OneSourceAdminCore.Presenter
{
    public class CatalogReportsPresenter
    {
        globalserveEntities dbCon;

        public CatalogReportsPresenter()
        {
            dbCon = new globalserveEntities();
        }


        public DataTable GetCatalogReportData(string reportId, string reportName, CatalogReportModel model)
        {
            try
            {
                var dtCatalogReportMain = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;
                var con = new SqlConnection(regularConnectionString);
                con.Open();

                using (var command = new SqlCommand("sp_CatalogReportMain_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ReportName", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@ReportId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CustomerIds", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CatalogStandardID", SqlDbType.Text));
                    command.Parameters[0].Value = reportName.ConvertObjectToString();
                    command.Parameters[1].Value = reportId.ConvertObjectToString();
                    command.Parameters[2].Value = model.CustomerIds.ConvertObjectToString();
                    command.Parameters[3].Value = model.CatalogStandardId.ConvertObjectToString();

                    using (var da = new SqlDataAdapter(command))
                    {
                        da.Fill(dtCatalogReportMain);
                    }
                }

                var catalogReportMainList = CatalogReportMainWithCountryNames(dtCatalogReportMain);
                return catalogReportMainList.ToDataTable();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "GetCatalogReportData");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "GetCatalogReportData");
                }
            }

            return null;
        }

        private List<CatalogReportMainModel> CatalogReportMainWithCountryNames(DataTable dtCatalogReportMain)
        {
            List<CatalogReportMainModel> catalogReportMainList = null;

            try
            {
                var countriesList = dbCon.tblCountries.ToList();
                catalogReportMainList = dtCatalogReportMain.ToList<CatalogReportMainModel>();

                foreach (var catalogReport in catalogReportMainList)
                {
                    var countryIdIntList = catalogReport.CountryIDList.Split(',');

                    foreach (var countryId in countryIdIntList)
                    {
                        foreach (var c in countriesList)
                        {
                            if (c.CountryID == countryId.ConvertObjectToInt32())
                            {
                                catalogReport.Countries += string.Format("{0},", c.CountryName);
                            }
                        }
                    }

                    catalogReport.Countries = catalogReport.Countries.TrimSubString(0, catalogReport.Countries.Length - 1);
                }

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ConvertObjectToString(), "CatalogReportMainWithCountryNames");

                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ConvertObjectToString(), "CatalogReportMainWithCountryNames");
                }
            }

            return catalogReportMainList;
        }
        public List<GenericList> GetBundlesByCustomer(string CustomerIds)
        {
            try
            {
                var bundles = dbCon.sp_BundlesAssociatedParts_AdminR(CustomerIds, "").Select(x => new { x.CatalogStandardID, x.CatalogDescription }).Distinct().ToList().Select(
                    x => new GenericList { Key = (int)x.CatalogStandardID, Value = x.CatalogDescription });
                return bundles.ToList();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetBundlesByCustomer");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetBundlesByCustomer");
                }
                return null;
            }
        }

        public List<GenericList> GetManufactures()//Adnan 19-june-2019
        {
            return dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.ManufacturerID, Value = x.ManufacturerName }).Distinct().OrderBy(x => x.Key).ToList();

        }

        public List<GenericList> GetCategories()//Adnan 27-june-2019
        {
            try
            {
                var PartCategories = (from pc in dbCon.tblPartCategories
                                      where pc.IsActive == true
                                      orderby pc.PartCategoryName
                                      select new GenericList { Key = pc.PartCategoryID, Value = pc.PartCategoryName }).Distinct().ToList();
                return PartCategories;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCategories");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCategories");
                }
                return null;
            }
        }

        public List<GenericList> GetCountries()//Adnan 28-june-2019
        {
            try
            {
                var Countries = (from pc in dbCon.tblCountries
                                      where pc.IsActive == true
                                      orderby pc.CountryName
                                      select new GenericList { Key = pc.CountryID, Value = pc.CountryName }).Distinct().ToList();
                return Countries;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetCountries");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCountries");
                }
                return null;
            }
        }
    }
}

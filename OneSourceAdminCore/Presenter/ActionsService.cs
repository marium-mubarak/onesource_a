﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class ActionsService
    {
        private globalserveEntities dbCon;
        public ActionsService()
        {
            dbCon = new globalserveEntities();
        }


        public List<Sp_GetActionsList_AdminR_Result> GetActionsList()
        {
            try
            {
                var list = dbCon.Sp_GetActionsList_AdminR().ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActionsList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActionsList");
                }
                return new List<Sp_GetActionsList_AdminR_Result>();
            }
        }


        public Sp_GetActionDetailsById_AdminR_Result GetActionDetailsById(int actionId)
        {
            try
            {
                var detail = dbCon.Sp_GetActionDetailsById_AdminR(actionId).FirstOrDefault();
                return detail;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActionDetailsById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActionDetailsById");
                }
                return new Sp_GetActionDetailsById_AdminR_Result();
            }
        }

        public List<GenericList> GetActionsListToModify()
        {
            try
            {
                var list = dbCon.tblActions.Where(x => x.IsActive).Select(x => new GenericList
                {
                    Key = x.ActionID,
                    Value = x.ActionName,
                }).OrderBy(x => x.Value).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActionsListToModify");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActionsListToModify");
                }
                return new List<GenericList>();
            }
        }


        public List<Sp_GetFieldsByAactionId_AdminR_Result> GetFieldsByActionId(int actionId)
        {
            try
            {
                var getFields = dbCon.Sp_GetFieldsByAactionId_AdminR(actionId).Where(x => !string.IsNullOrWhiteSpace(x.FieldLabel) && !string.IsNullOrWhiteSpace(x.FieldMethod)).OrderBy(x => x.DisplayOrder).ToList();
                return getFields;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetFieldsByActionId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAcGetFieldsByActionIdtionsList");
                }
                return new List<Sp_GetFieldsByAactionId_AdminR_Result>();
            }
        }

        public List<GenericList> GetActionListsByName(string listName)
        {
            try
            {
                var query = "";
                if (listName == "CreateCSRList")
                {
                    query =
                       "select contactid as 'Key',IsNull(firstname, '') + ' ' + IsNull(lastname, '') as 'Value'  from tblcontact where tblContact.partyid in (select partyid from tblPartyRole where roleid in (select roleid from tblRole where rolename = 'CSR')) order by Value";
                }
                else if (listName == "CreateCountryList")
                {
                    query =
                        "select countryid as 'Key',countryname as 'Value' from tblCountries order by CountryName";
                }
                else if (listName == "CreateManufacturerList")
                {
                    query =
                        "SELECT DISTINCT  m.ManufacturerID as 'Key',m.ManufacturerName as 'Value' FROM tblManufacturers m WHERE m.IsActive=1 ORDER BY m.ManufacturerName";
                }
                else if (listName == "CreateFSCList")
                {
                    query =
                        "select sfaid as 'Key',parentcompanyname as 'Value'  from tblParentCompanies, tblSFAs where tblSFAs.isComposite = 1 and tblSFAs.parentcompanyid = tblParentCompanies.parentcompanyid order by parentcompanyname";
                }
                else if (listName == "CreateEscalationList")
                {
                    var list = new List<GenericList>();
                    for (int i = 1; i <= 5; i++)
                    {
                        list.Add(new GenericList()
                        {
                            Key = i,
                            Value = i.ToString()

                        });
                    }
                    return list;

                }
                var result = dbCon.Database.SqlQuery<GenericList>(query).ToList();

                if (result.Count > 0) return result.ToList();

                return new List<GenericList>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetActionListsByName");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetActionListsByName");
                }
                return new List<GenericList>();
            }
        }


        public List<GenericStringList> GetFieldTypeRelationList()
        {
            try
            {
                var query =
                    "select '##'+UPPER(fieldName)+'##' 'Key', fieldDisplay as 'Value' from tblFields where fieldID in (select fieldid from tblFieldTypeRelation where fieldtypeid=(select fieldtypeid from tblFieldTypes where fieldtypename='NotificationInsert')) order by fieldDisplay";

                var result = dbCon.Database.SqlQuery<GenericStringList>(query).ToList();

                if (result.Count > 0) return result.ToList();

                return new List<GenericStringList>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetFieldTypeRelationList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetFieldTypeRelationList");
                }
                return new List<GenericStringList>();
            }
        }

        public bool AddUpdateActionFields(FormCollection fm, AdminSetting settings)
        {
            try
            {
                if (fm != null && fm["ActionId"] != null)
                {
                    int actionId = 0;
                    actionId = int.Parse(fm["ActionId"]);
                    if (fm["SaveNewChk"] != null && Convert.ToBoolean(fm["SaveNewChk"]) && actionId > 0)
                    {
                        ObjectParameter newActionId = new ObjectParameter("newactionid", typeof(int));

                        var actionName = (fm["ActionName"] == null ? "" : fm["ActionName"].ToString());

                        var actionIdNew = dbCon.sp_copyAction(actionId, actionName, settings.UserID, newActionId);
                        return true;
                    }
                    else
                    {
                        var actionFields = GetFieldsByActionId(actionId);
                        if (actionFields.Count > 0)
                        {
                            foreach (var item in actionFields)
                            {

                                var actionFeildsDefaultValue =
                                    dbCon.tblActionFieldDefaultValues.FirstOrDefault(
                                        x => x.ActionID == item.ActionID && x.FieldID == item.FieldId);

                                if (actionFeildsDefaultValue != null && fm[item.FieldName] != null)
                                {
                                    actionFeildsDefaultValue.FieldTxtValue = fm[item.FieldName];
                                    dbCon.Entry(actionFeildsDefaultValue).State =
                                        System.Data.Entity.EntityState.Modified;
                                    dbCon.SaveChanges();
                                }
                            }
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "UpdateActionFields");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "UpdateActionFields");
                }
                return false;
            }
        }



        public bool RunAction(int actionId, AdminSetting settings)
        {
            try
            {
                var ar = new GSSupport.ActionRequest();

                if (actionId > 0)
                {

                    ar.SubmitUserID = settings.UserID;
                    ar.AffectedContactID = settings.ContactId;
                    ar.RequestDateLocal = DateTime.Now;
                    ar.RequestDate = DateTime.Now;
                    ar.RequestDateTimeZoneVBConst = Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]);
                    ar.TypeID = 9;
                    ar.PriorityID = 2;
                    ar.CompanyID = settings.ParentCompanyID;
                    ar.GenerateTicket = false;
                    ar.Qualify = false;
                    ar.CategoryName = "System";
                    ar.RequestedActionID = actionId;
                    if (ar.Submit())
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "RunAction");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "RunAction");
                }
                return false;
            }
        }




    }
}

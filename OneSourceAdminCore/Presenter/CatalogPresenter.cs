﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System.Diagnostics;
using OneSourceAdminCore.Helper;
using System.Data.Entity.Core.Objects;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Feb 09 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Catalog Alert"
    /// </summary>
    public class CatalogPresenter
    {
        private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public CatalogPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// get sku and its detail from database by search given sku
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public List<sp_getSearchCatalogSKU_R_Result> GetSearchCatalogSku(string sku, int? customerId)
        {
            var result = _dbCon.sp_getSearchCatalogSKU_R(sku, customerId).ToList();
            return result;
        }
        /// <summary>
        /// add catalog alert to the database
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddCatalogAlert(tblCatalogAlert alert, string description, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                alert.Alert = description + "<br/><br/>" + alert.Alert;
                alert.CreatedDate = DateTime.Now;
                alert.CreatedBy = userId;

                _dbCon.tblCatalogAlerts.Add(alert);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }

        /// <summary>
        /// edit catalog alert in the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditCatalogAlert(tblCatalogAlert model, string description, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCatalogAlert alert =
                    _dbCon.tblCatalogAlerts.FirstOrDefault(x => x.CatalogAlertId == model.CatalogAlertId);
                if (alert != null)
                {
                    alert.Alert = description + "<br/><br/>" + model.Alert; ;
                    alert.StartDate = model.StartDate;
                    alert.EndDate = model.EndDate;
                    alert.UpdatedBy = userId;
                    alert.UpdatedDate = DateTime.Now;
                    alert.SKU = model.SKU;
                    alert.ManufacturerPartId = model.ManufacturerPartId;
                    alert.CustomerId = model.CustomerId;
                    alert.IsTopStory = model.IsTopStory;
                }
                _dbCon.Entry(alert).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }

        /// <summary>
        /// Get list of catalog alert from the database
        /// </summary>
        /// <returns></returns>
        public List<CustomCatalogAlert> GetCatalogAlert()
        {
            var catalogAlert = _dbCon.tblCatalogAlerts.Select(x => new CustomCatalogAlert
            {
                SKU = x.SKU,
                ManufacturerPartId = x.ManufacturerPartId,
                Alert = x.Alert,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                CatalogAlertId = x.CatalogAlertId,
                CustomerId = x.CustomerId,
                IsTopStory = x.IsTopStory
            }).ToList();

            var customers = _dbCon.tblCustomers.Where(x => x.IsActive == true);

            catalogAlert.ForEach(x =>
            {
                x.strStartDate = x.StartDate.Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.EndDate != null ? Convert.ToDateTime(x.EndDate).Date.ToString("MM/dd/yyy") : string.Empty);
                x.customerName = customers.FirstOrDefault(y => y.CustomerID == x.CustomerId) == null ? "All" : customers.FirstOrDefault(y => y.CustomerID == x.CustomerId).CustomerName;
            });
            return catalogAlert;
        }

        /// <summary>
        /// delete catalog alert from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCatalogAlert(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblCatalogAlerts.FirstOrDefault(x => x.CatalogAlertId == id);
                if (alert != null)
                {
                    _dbCon.tblCatalogAlerts.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }
        /// <summary>
        /// get list of the customer
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetCustomerList()
        {
            var customerList = _dbCon.tblCustomers.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Key = x.CustomerID,
                Value = x.CustomerName
            }).OrderBy(x => x.Value).ToList();
            return customerList;
        }

        #region sfaCatalog

        /// <summary>
        /// this method is responsible for providing data for filter view
        /// </summary>
        /// <param name="sfaCountryId"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public SFACatalogModel GetSFACatalogModel(int sfaCountryId, int sfaId)
        {
            try
            {
                SFACatalogModel obj = new SFACatalogModel();
                obj.countryId = sfaCountryId;
                obj.CustomerList = (from cof in _dbCon.tblCustomerOfferings
                                    join ofr in _dbCon.tblOfferings on cof.OfferingID equals ofr.OfferingID
                                    join cust in _dbCon.tblCustomers on cof.CustomerID equals cust.CustomerID
                                    where cust.IsActive == true && ofr.OfferingShort == "PROCURE" && ofr.IsActive == true
                                    orderby cust.CustomerName
                                    select new GenericList { Value = cust.CustomerName, Key = cust.CustomerID }).ToList();

                obj.CurrencyList = _dbCon.tblCurrencies.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.CurrencyID, Value = x.CurrencyName }).OrderBy(x => x.Value).ToList();
                obj.CountryList = _dbCon.tblCountries.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.CountryID, Value = x.CountryName }).OrderBy(x => x.Value).ToList();
                obj.CategoryList = _dbCon.tblPartCategories.Where(x => x.IsActive == true).Select(x => new GenericList { Key = x.PartCategoryID, Value = x.PartCategoryName }).OrderBy(x => x.Value).ToList();
                obj.CategoryList.Insert(0, new Model.GenericList { Key = 0, Value = "All Categories" });
                obj.ManufacturerList = _dbCon.tblManufacturers.Where(x => x.IsActive == true).
                    Select(x => new GenericList { Key = x.ManufacturerID, Value = x.ManufacturerName }).OrderBy(x => x.Value).Distinct().ToList();
                obj.ManufacturerList.Insert(0, new Model.GenericList { Key = 0, Value = "All Manufacturers" });



                var data = (from ccc in _dbCon.tblCustomerCountryCurrencies
                            join sfa in _dbCon.tblSFAs on ccc.CountryID equals sfa.CountryID
                            join cur in _dbCon.tblCurrencies on ccc.CurrencyID equals cur.CurrencyID
                            where ccc.IsDefaultCurrency == true && ccc.IsActive == 1 && ccc.CustomerID == 21 && sfa.SFAID == sfaId && cur.IsActive == true
                            select new { ccc.CurrencyID }).Distinct().FirstOrDefault();

                if (data != null)
                {
                    obj.currencyId = Convert.ToInt32(data.CurrencyID);
                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => GetSFACatalogModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => GetSFACatalogModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSFACatalogModel");
                }
                return new SFACatalogModel();
            }
        }

        /// <summary>
        /// this method is responsible for providing list of search catalog
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<usp_GetPartInfoAdjustedPrice_R_Result> SearchCatalog(SFACatalogModel model, int sfaId)
        {
            try
            {
                var data = _dbCon.usp_GetPartInfoAdjustedPrice_R(model.customerId, model.countryId, model.currencyId, 0, model.catalogTypeId == "Global" ? 1 : 2, model.catalogTypeId == "Global" ? 0 : sfaId, model.manufacturerId,
                    model.partCategoryId, model.ProductSKUName, model.productSearchBy == "SKU" ? "PRODUCTNUM" : "PRODUCTNAME", 1, 500, null, "ASC", "SUPPLIER", 0, sfaId, 0).ToList();

                return data;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => SearchCatalog<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => SearchCatalog");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "SearchCatalog");
                }
                return new List<usp_GetPartInfoAdjustedPrice_R_Result>();
            }
        }

        /// <summary>
        /// this method is responsible for providing currencyname
        /// </summary>
        /// <param name="currencyId"></param>
        /// <returns></returns>
        public string GetCurrencyName(int currencyId)
        {
            try
            {
                var currency = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == currencyId);
                if (currency != null)
                {
                    return currency.CurrencyName;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => GetCurrencyName<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => GetCurrencyName");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCurrencyName");
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// this method is responsible for providing part info detail
        /// </summary>
        /// <param name="mfgPartId"></param>
        /// <returns></returns>
        public PartDetailModel GetPartInfo(string mfgPartId)
        {
            try
            {
                PartDetailModel obj = new PartDetailModel();

                var query = @"SELECT stdnxx.Description 'StdnDesc', prod.MfPN 'SKU', prod.ImgID 'ImageID',
                        (SELECT text FROM CDS..cds_vocxx(NOLOCK) WHERE id = atrPLine.ValID) 'ProdLine',
                        (SELECT text FROM CDS..cds_vocxx(NOLOCK) WHERE id = atrModel.ValID) 'Model',
                        (SELECT text FROM CDS..cds_vocxx(NOLOCK) WHERE id = prod.MfID) 'Manufacturer',
                        replace(mktxx.Description, char(11), '<BR>') 'MarketingDesc'
                        FROM CDS..cds_prod prod (NOLOCK)
                        LEFT JOIN CDS..cds_mktxx mktxx ON prod.mktID = mktxx.MktID
                        LEFT JOIN CDS..cds_atr atrPLine ON atrPLine.prodID = prod.ProdID AND atrPLine.AtrID = 'A00600'
                        LEFT JOIN CDS..cds_atr atrModel ON atrModel.ProdID = prod.ProdID AND atrModel.AtrID = 'A00601'
                        JOIN CDS..cds_stdnxx stdnxx ON stdnxx.ProdID = prod.ProdID
                        WHERE prod.ProdID = " + mfgPartId;

                obj.partInfoModel = _dbCon.Database.SqlQuery<PartInfoModel>(query).FirstOrDefault() ?? new Model.PartInfoModel();

                var query2 = @"SELECT mvoct.text 'Title', ISNULL(mvocb.text,'-') 'Body' 
                        FROM CDS..cds_mspecxx mspec (NOLOCK)
                        JOIN CDS..cds_mvocxx mvoct (NOLOCK) ON mvoct.ID = mspec.HdrID
                        JOIN CDS..cds_mvocxx mvocb (NOLOCK) ON mvocb.ID = mspec.BodyID
                        WHERE mspec.ProdID = " + mfgPartId + @"
                        AND mspec.HdrID NOT IN ('T0000115','T0000023','T0000044','T0000043','T0000205','T0000054','T0000164')
                        ORDER BY DisplayOrder ASC";

                obj.partMainSpecification = _dbCon.Database.SqlQuery<PartMainSpecification>(query2).ToList() ?? new List<Model.PartMainSpecification>();


                var query3 = @"SELECT evocs.Text 'Section', evoct.Text 'Header', ISNULL(evocb.text,'-') 'Body'
                            FROM CDS..cds_especxx espec (NOLOCK)
                            JOIN CDS..cds_evocxx evocs (NOLOCK) ON evocs.ID = espec.SectID
                            JOIN CDS..cds_evocxx evoct (NOLOCK) ON evoct.ID = espec.HdrID
                            JOIN CDS..cds_evocxx evocb (NOLOCK) ON evocb.ID = espec.BodyID
                            WHERE espec.ProdID = " + mfgPartId + @"
                            AND espec.HdrID NOT IN ('T0000192','T0000030','T0000078','T0000081','T0000087','T0000257')
                            ORDER BY DisplayOrder ASC";

                obj.partExtendedSpecification = _dbCon.Database.SqlQuery<PartExtendedSpecification>(query3).ToList() ?? new List<Model.PartExtendedSpecification>();

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => GetCurrencyName<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => GetCurrencyName");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetCurrencyName");
                }
                return new PartDetailModel();
            }
        }


        public SFACatalogModel GetSFADomesticCatalogModel(int sfaId)
        {

            SFACatalogModel obj = new Model.SFACatalogModel();
            try
            {
                var priceCodes = _dbCon.tblPriceCodes.Where(x => x.PriceCode == "DP").Select(x => x.PriceCodeID).ToList();
                obj.CustomerList = (from ccp in _dbCon.tblCustomerCountryParts
                                    join
                                    cus in _dbCon.tblCustomers on ccp.CustomerID equals cus.CustomerID
                                    where priceCodes.Contains(ccp.PriceCodeID) && ccp.SFAID == sfaId && cus.IsActive == true
                                    orderby cus.CustomerName
                                    select new GenericList { Key = cus.CustomerID, Value = cus.CustomerName }
                               ).Distinct().OrderBy(x => x.Value).ToList();

                obj.CategoryList = (from ccp in _dbCon.tblCustomerCountryParts
                                    join
                                    pc in _dbCon.tblPartCategories on ccp.PartCategoryID equals pc.PartCategoryID
                                    where priceCodes.Contains(ccp.PriceCodeID) && ccp.SFAID == sfaId
                                    orderby pc.PartCategoryName
                                    select new GenericList { Key = pc.PartCategoryID, Value = pc.PartCategoryName }
                               ).Distinct().ToList();

                obj.CategoryList.Insert(0, new Model.GenericList { Key = 0, Value = "All Categories" });

                obj.ManufacturerList = (from ccp in _dbCon.tblCustomerCountryParts
                                        join
                                        mp in _dbCon.tblManufacturerParts on ccp.ManufacturerPartID equals mp.ManufacturerPartID
                                        join
                                        m in _dbCon.tblManufacturers on mp.ManufacturerID equals m.ManufacturerID
                                        where priceCodes.Contains(ccp.PriceCodeID) && ccp.SFAID == sfaId
                                        orderby m.ManufacturerName
                                        select new GenericList { Key = m.ManufacturerID, Value = m.ManufacturerName }
                             ).Distinct().ToList();
                obj.ManufacturerList.Insert(0, new Model.GenericList { Key = 0, Value = "All Manufacturers" });

                obj.CountryList = (from ccp in _dbCon.tblCustomerCountryParts
                                   join
                                   pc in _dbCon.tblCountries on ccp.DestinationCountryID equals pc.CountryID
                                   where priceCodes.Contains(ccp.PriceCodeID) && ccp.SFAID == sfaId
                                   orderby pc.CountryName
                                   select new GenericList { Key = pc.CountryID, Value = pc.CountryName }
                               ).Distinct().ToList();
                obj.CountryList.Insert(0, new Model.GenericList { Key = 0, Value = "All Countries" });
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => GetSFADomesticCatalogModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => GetSFADomesticCatalogModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSFADomesticCatalogModel");
                }                
            }

            return obj;
        }


        /// <summary>
        /// this method is responsible for providing list of search catalog
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public List<sp_getDomesticCatalog_AdminR_Result> SearchDomesticCatalog(SFACatalogModel model, int sfaId)
        {
            try
            {
                var data = _dbCon.sp_getDomesticCatalog_AdminR(model.customerId.ToString(), sfaId.ToString(), model.ProductSKUName, model.productSearchBy,
                    model.partCategoryId == 0 ? string.Empty : model.partCategoryId.ToString(), model.manufacturerId == 0 ? string.Empty : model.manufacturerId.ToString(),
                    model.countryId == 0 ? string.Empty : model.countryId.ToString()).ToList();

                return data;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => SearchCatalog<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => SearchCatalog");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "SearchCatalog");
                }
                return new List<sp_getDomesticCatalog_AdminR_Result>();
            }
        }

        public DomesticCatalogPartModel EditDomesticPartModel(int sfaId, int customerId, int customerPartId)
        {
            DomesticCatalogPartModel obj = new Model.DomesticCatalogPartModel();
            try
            {

                obj.Partdetail = (from ccp in _dbCon.tblCustomerCountryParts
                                  join
                                  cus in _dbCon.tblCustomers on ccp.CustomerID equals cus.CustomerID
                                  join co in _dbCon.tblCountries on ccp.DestinationCountryID equals co.CountryID
                                  join mp in _dbCon.tblManufacturerParts on ccp.ManufacturerPartID equals mp.ManufacturerPartID
                                  where ccp.CustomerPartID == customerPartId
                                  select new DomesticPartModel
                                  {
                                      CustomerName = cus.CustomerName,
                                      CountryName = co.CountryName,
                                      PartDescription = ccp.PartDescription,
                                      PartPrice = ccp.PartPrice,
                                      PartPriceCurrencyID = ccp.PartPriceCurrencyID,
                                      IsActive = ccp.IsActive,
                                      ManufacturerSKU = mp.ManufacturerSKU,
                                      EOL = ccp.EOL
                                  }).FirstOrDefault() ?? new DomesticPartModel();

                obj.Partdetail.CustomerID = customerId;
                obj.Partdetail.CustomerPartID = customerPartId;

                obj.Partcountry = _dbCon.sp_getPartCountries_AdminR(sfaId, customerPartId, customerId).ToList();

                obj.Currencies = (from a in _dbCon.tblCurrencies
                                  join b in _dbCon.tblCustomerCountryCurrencies on a.CurrencyID equals b.CurrencyID
                                  where b.CustomerID == customerId && a.IsActive == true && b.IsActive == 1
                                  orderby a.CurrencyName
                                  select new GenericList { Key = a.CurrencyID, Value = a.CurrencyName }).ToList();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => EditDomesticPartModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => EditDomesticPartModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "EditDomesticPartModel");
                }
            }
            return obj;
        }

        public int CountrifyPart(string countries, int customerPartId, int customerId, int sfaId, int userId)
        {
            int count = 0;
            try
            {
                if (!string.IsNullOrEmpty(countries))
                {
                    var customerCountryPart = _dbCon.tblCustomerCountryParts.Where(x => x.CustomerPartID == customerPartId && x.CustomerID == customerId && x.SFAID == sfaId).FirstOrDefault();
                    if (customerCountryPart != null)
                    {
                        List<int> countriesId = countries.Split(',').Select(x => int.Parse(x)).ToList();
                        foreach (var countryId in countriesId)
                        {
                            var customerCountryPart2 = _dbCon.tblCustomerCountryParts.FirstOrDefault(x => x.CatalogPartID == customerCountryPart.CatalogPartID && x.CustomerID == customerId && x.SFAID == sfaId && x.DestinationCountryID == countryId);
                            if (customerCountryPart2 == null)
                            {


                                using (var transaction = _dbCon.Database.BeginTransaction())
                                {
                                    tblCustomerCountryPart obj = new tblCustomerCountryPart();
                                    obj.ManufacturerPartID = customerCountryPart.ManufacturerPartID;
                                    obj.CustomerID = customerId;
                                    obj.CatalogPartID = customerCountryPart.CatalogPartID;
                                    obj.PartPrice = customerCountryPart.PartPrice;
                                    obj.PartPriceCurrencyID = customerCountryPart.PartPriceCurrencyID;
                                    obj.Discount = customerCountryPart.Discount;
                                    obj.NonDirectPurchaseUplift = customerCountryPart.NonDirectPurchaseUplift;
                                    obj.PartTradeBlockUplift = customerCountryPart.PartTradeBlockUplift;
                                    obj.SFASpecialUplift = customerCountryPart.SFASpecialUplift;
                                    obj.PartDescription = customerCountryPart.PartDescription;
                                    obj.PartCategoryID = customerCountryPart.PartCategoryID;
                                    obj.DestinationCountryID = countryId;
                                    obj.LastUpdatedByUserID = userId;
                                    obj.UNSPSC = customerCountryPart.UNSPSC;
                                    obj.PriceCodeID = customerCountryPart.PriceCodeID;
                                    obj.SFAID = customerCountryPart.SFAID;
                                    obj.LastUpdated = DateTime.Now;
                                    if (customerCountryPart.EOL == null)
                                    {
                                        obj.EOL = DateTime.Now.AddDays(30);
                                    }
                                    else
                                    {
                                        obj.EOL = customerCountryPart.EOL;
                                    }

                                    _dbCon.tblCustomerCountryParts.Add(obj);
                                    count = _dbCon.SaveChanges();
                                    transaction.Commit();

                                }
                                return count;



                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => CountrifyPart<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => CountrifyPart");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "CountrifyPart");
                }
            }
            return count;
        }

        public int AddUpdateDomesticPart(DomesticPartModel model, int sfaId, int userId)
        {
            int count = 0;
            try
            {
                if (model.CustomerPartID != 0)
                {
                    var customerCountryParts = _dbCon.tblCustomerCountryParts.FirstOrDefault(x => x.CustomerPartID == model.CustomerPartID && x.CustomerID == model.CustomerID && x.SFAID == sfaId);
                    if (customerCountryParts != null)
                    {
                        using (var transaction = _dbCon.Database.BeginTransaction())
                        {
                            customerCountryParts.PartDescription = model.PartDescription;
                            customerCountryParts.PartPrice = model.PartPrice;
                            customerCountryParts.PartPriceCurrencyID = model.PartPriceCurrencyID;
                            if (model.EOL != null)
                            {
                                customerCountryParts.EOL = Convert.ToDateTime(model.EOL);
                            }
                            else
                            {
                                customerCountryParts.EOL = DateTime.Now.AddDays(30);
                            }
                            customerCountryParts.IsActive = model.IsActive;
                            customerCountryParts.LastUpdated = DateTime.Now;
                            customerCountryParts.LastUpdatedByUserID = userId;

                            _dbCon.Entry(customerCountryParts).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                            transaction.Commit();
                            return count > 0 ? model.CustomerPartID : 0;

                        }
                    }
                }
             
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => AddUpdateDomesticPart<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => AddUpdateDomesticPart");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateDomesticPart");
                }
            }
            return count;
        }

        public DomesticAddPartModel GetDomesticPartAddModel(int customerId, int mfgPartId, int sfaId)
        {
            DomesticAddPartModel obj = new Model.DomesticAddPartModel();
            try
            {
                if (mfgPartId > 0)
                {
                    var mfgPart = _dbCon.tblManufacturerParts.FirstOrDefault(x => x.ManufacturerPartID == mfgPartId);
                    if (mfgPart != null)
                    {
                        obj.Description = mfgPart.Description;
                        obj.SKU = mfgPart.ManufacturerSKU;
                        obj.MfgPartId = mfgPart.ManufacturerPartID;
                    }
                }


                obj.CustomerList = (from cust in _dbCon.tblCustomers
                                    join cof
                                    in _dbCon.tblCustomerOfferings on cust.CustomerID equals cof.CustomerID
                                    join ofe in _dbCon.tblOfferings on cof.OfferingID equals ofe.OfferingID
                                    where cust.IsActive == true && cof.IsActive == true && ofe.OfferingShort == "PROCURE" && ofe.IsActive == true
                                    orderby cust.CustomerName
                                    select new GenericList { Key = cust.CustomerID, Value = cust.CustomerName }).OrderBy(x => x.Value).Distinct().ToList();
                obj.CustomerList.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                if (customerId > 0)
                {
                    //categories
                    var parentCompanies = (from pc in _dbCon.tblParentCompanies
                                           join
                                           c in _dbCon.tblCustomers on pc.ParentCompanyID equals c.ParentCompanyID into ps
                                           from c in ps.DefaultIfEmpty()
                                           where pc.ParentCompanyName == "GlobalServe" || c.CustomerID == customerId
                                           select new { pc.ParentCompanyID }).ToList().Select(i => (int?)i.ParentCompanyID).ToList();

                    obj.CategoryList = _dbCon.tblPartCategories.Where(x => x.IsActive == true && parentCompanies.Contains(x.CompanyID))
                         .Select(x => new GenericList { Key = x.PartCategoryID, Value = x.PartCategoryName }).OrderBy(x => x.Value).ToList();

                    obj.CurrencyList = (from a in _dbCon.tblCurrencies
                                        join
                                        b in _dbCon.tblCustomerCountryCurrencies on a.CurrencyID equals b.CurrencyID
                                        where b.CustomerID == customerId && a.IsActive == true && b.IsActive == 1
                                        select new GenericList { Key = a.CurrencyID, Value = a.CurrencyName }).OrderBy(x => x.Value).ToList();

                    obj.CountryList = _dbCon.sp_getPartCountries_AdminR(sfaId, 0, customerId).ToList();
                }
                else
                {
                    obj.CategoryList = new List<Model.GenericList>();
                    obj.CategoryList.Insert(0, new Model.GenericList { Key = 0, Value = "(None)" });
                    obj.CurrencyList = new List<Model.GenericList>();
                    obj.CurrencyList.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                    obj.CountryList = new List<sp_getPartCountries_AdminR_Result>();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => GetDomesticPartAddModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => GetDomesticPartAddModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetDomesticPartAddModel");
                }
            }
            return obj;
        }

        public List<ManufacturerPartModel> GetManufacturerPart(string sku)
        {
            try
            {
                if (sku != "0")
                {
                    sku = sku == "-1" ? string.Empty : sku;
                    var query = @"SELECT top 500  mp.ManufacturerSKU ,mp.ManufacturerPartID ,
                        mp.Description As 'Description', pc.PartCategoryName As 'Part Category',
                        m.ManufacturerName As Manufacturer
                        FROM tblManufacturerParts mp, tblManufacturers m, tblPartCategories pc
                        WHERE Upper(mp.ManufacturerSKU) LIKE '" + sku.ToUpper() + @"%'
                        AND m.ManufacturerID=mp.ManufacturerID
                        AND pc.PartCategoryID=mp.PartCategoryID
                        AND mp.IsActive=1 AND m.IsActive=1 AND pc.IsActive=1 order by ManufacturerSKU";
                    var data = _dbCon.Database.SqlQuery<ManufacturerPartModel>(query).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => GetManufacturerPart<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => GetManufacturerPart");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturerPart");
                }
            }
            return null;
        }

        public int AddDomesticCatPart(DomesticAddPartModel model, int sfaId,int userId,string contactEmail)
        {
            try
            {
                if (model != null)
                {
                    int catalogPartId = 0;
                    var catPrts = _dbCon.tblCatalogParts.FirstOrDefault(x => x.CustomerID == model.CustomerId && x.ManufacturerPartID == model.MfgPartId && x.SFAID == sfaId);
                    if (catPrts == null)
                    {
                        var defaultPriceCode = 2;
                        var PriceCode = _dbCon.tblPriceCodes.FirstOrDefault(x => x.PriceCode == "DP");
                        if (PriceCode != null)
                        {
                            defaultPriceCode = PriceCode.PriceCodeID;
                        }
                        var customer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == model.CustomerId);
                        if (customer.PartPriceLimitUSD == null)
                        {
                            catalogPartId = (int)_dbCon.sp_AddCatalogPart_AdminR("", model.Description, model.CostPrice, model.CurrencyId,
                                model.CustomerId, "43000000", sfaId, model.CategoryId, defaultPriceCode, model.MfgPartId, userId, 1).FirstOrDefault();

                        }
                        else
                        {
                            string MissingEmail = string.Empty,
                            CustomerEmail = string.Empty,
                            approvalEmailFrom = string.Empty,
                            approvalEmailTo = string.Empty,
                            approvalEmailCC = string.Empty;
                            var primeContractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                            if (primeContractor != null)
                            {
                                MissingEmail = primeContractor.PartQuoteEmail ?? string.Empty;
                            }
                            var gscontactemail = (from cust in _dbCon.tblCustomers
                                                  join pc
                                                   in _dbCon.tblParentCompanies on cust.ParentCompanyID equals pc.ParentCompanyID
                                                  where cust.CustomerID == model.CustomerId
                                                  select new { pc.GSContactEmail }
                                      ).FirstOrDefault();
                            if (gscontactemail != null)
                            {
                                CustomerEmail = gscontactemail.GSContactEmail;
                            }
                            else
                            {
                                CustomerEmail = string.Empty;
                            }
                            if (!string.IsNullOrEmpty(CustomerEmail))
                            {
                                approvalEmailFrom = CustomerEmail;
                            }
                            else
                            {
                                approvalEmailFrom = MissingEmail;
                            }
                            approvalEmailTo = approvalEmailFrom;

                            if (!string.IsNullOrEmpty(contactEmail))
                            {
                                approvalEmailCC = contactEmail;
                            }
                            else
                            {
                                approvalEmailCC = MissingEmail;
                            }

                            if (!string.IsNullOrEmpty(approvalEmailFrom) && !string.IsNullOrEmpty(approvalEmailTo))
                            {
                                decimal ExchangeRate = 0.0M;
                                var ExchangeRateToUSDollars = _dbCon.tblPrimeContractorExchangeRates.FirstOrDefault(x => x.IsActive == 1 && x.PrimeContractorID == 1 && x.CurrencyID == model.CurrencyId);
                                if (ExchangeRateToUSDollars != null)
                                {
                                    ExchangeRate = ExchangeRateToUSDollars.ExchangeRateToUsDollars == 0 ? 1 : ExchangeRateToUSDollars.ExchangeRateToUsDollars;
                                    if ((model.CostPrice / ExchangeRate) > customer.PartPriceLimitUSD)
                                    {
                                        var query = @"SELECT DISTINCT cus.CustomerName, mp.ManufacturerSKU, m.ManufacturerName,
                                                 cur.CurrencyName, sfa.SFAName, u.UserName
                                                 FROM tblManufacturerParts mp, tblManufacturers m
                                                 LEFT OUTER JOIN tblCustomers cus ON cus.CustomerID=
                                                 LEFT OUTER JOIN tblCurrencies cur ON cur.CurrencyID=
                                                 LEFT OUTER JOIN tblSFAs sfa ON sfa.SFAID=
                                                 LEFT OUTER JOIN tblUsers u ON u.UserID=
                                                 WHERE mp.ManufacturerPartID=
                                                 AND m.ManufacturerID=mp.ManufacturerID";
                                        var customerData = _dbCon.Database.SqlQuery<CatalogCustomerApprovalModel>(query).FirstOrDefault();
                                        if (customerData != null)
                                        {
                                            string sMailBody = string.Empty;
                                            sMailBody = "The part below needs approval due to the price limit :" + Environment.NewLine + Environment.NewLine;
                                            sMailBody = sMailBody + "Price Limit   : " + customer.PartPriceLimitUSD + " US Dollars" + Environment.NewLine;
                                            sMailBody = sMailBody + "SKU           : " + customerData.ManufacturerSKU + Environment.NewLine;
                                            sMailBody = sMailBody + "Manufacturer  : " + customerData.ManufacturerName + Environment.NewLine;
                                            sMailBody = sMailBody + "Description   : " + model.Description + Environment.NewLine;
                                            sMailBody = sMailBody + "Price         : " + model.CostPrice + Environment.NewLine;
                                            sMailBody = sMailBody + "Currency      : " + customerData.CurrencyName + Environment.NewLine;
                                            sMailBody = sMailBody + "Customer      : " + customerData.CustomerName + Environment.NewLine;
                                            sMailBody = sMailBody + "Supplier      : " + customerData.SFAName + Environment.NewLine;
                                            sMailBody = sMailBody + "User          : " + customerData.UserName + Environment.NewLine + Environment.NewLine;


                                            sMailBody = sMailBody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com";
                                            OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(approvalEmailTo, approvalEmailFrom, approvalEmailCC, "", sMailBody, "GlobalServe ONESOURCE - Part approval required - " + customerData.CustomerName);
                                        }
                                    }

                                }
                            }

                            catalogPartId = (int)_dbCon.sp_AddCatalogPart_AdminR("", model.Description, model.CostPrice, model.CurrencyId,
                               model.CustomerId, "43000000", sfaId, model.CategoryId, defaultPriceCode, model.MfgPartId, userId, 1).FirstOrDefault();
                        }

                        foreach (var item in model.Countries.Split(','))
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                using (var transaction = _dbCon.Database.BeginTransaction())
                                {
                                    tblCustomerCountryPart obj = new tblCustomerCountryPart();
                                    obj.ManufacturerPartID = model.MfgPartId;
                                    obj.CustomerID = model.CustomerId;
                                    obj.CatalogPartID = catalogPartId;
                                    obj.PartPrice = model.CostPrice;
                                    obj.PartPriceCurrencyID = model.CurrencyId;
                                    obj.Discount = 0;
                                    obj.NonDirectPurchaseUplift = 0;
                                    obj.PartTradeBlockUplift = null;
                                    obj.SFASpecialUplift = null;
                                    obj.PartDescription = model.Description;
                                    obj.PartCategoryID = model.CategoryId;
                                    obj.DestinationCountryID = Convert.ToInt32(item);
                                    obj.LastUpdatedByUserID = userId;
                                    obj.UNSPSC = "43000000";
                                    obj.PriceCodeID = defaultPriceCode;
                                    obj.SFAID = sfaId;
                                    obj.IsActive = true;
                                    obj.EOL = model.Eol;
                                    obj.LastUpdated = DateTime.Now;

                                    _dbCon.tblCustomerCountryParts.Add(obj);
                                    _dbCon.SaveChanges();
                                    transaction.Commit();

                                }
                            }
                        }
                        return catalogPartId;

                    }
                    return -1;
                }

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>catalog Presenter => AddDomesticCatPart<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "catalog Presenter => AddDomesticCatPart");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDomesticCatPart");
                }
            }

            return 0;
        }

        public List<sp_getCatalogBundles_AdminR_Result> SearchCatalogBundleRequest(SFACatalogModel model)
        {
            return _dbCon.sp_getCatalogBundles_AdminR(model.customerId==0?"": model.customerId.ToString(), model.ProductSKUName, model.productSearchBy, 
               model.partCategoryId==0?"":model.partCategoryId.ToString(),model.countryId==0?"":model.countryId.ToString()).ToList();
        }

        public CatalogBundleDetailModel GetCatalogBundleInfo(int catId)
        {
           CatalogBundleDetailModel obj = new Model.CatalogBundleDetailModel();

            obj.ItemList = (from cs in _dbCon.tblCatalogStandards
                        join pis in _dbCon.tblPartsInStandards on cs.CatalogStandardID equals pis.CatalogStandardID into ps
                        from pis in ps.DefaultIfEmpty()
                        join cp in _dbCon.tblCatalogParts on pis.CatalogPartID equals cp.CatalogPartID into ps1
                        from cp in ps1.DefaultIfEmpty()
                        join mp in _dbCon.tblManufacturerParts on cp.ManufacturerPartID equals mp.ManufacturerPartID into ps2
                        from mp in ps2.DefaultIfEmpty()
                        join m in _dbCon.tblManufacturers on mp.ManufacturerID equals m.ManufacturerID
                        where cs.CatalogStandardID == catId && cp.IsActive == true
                        select new CatalogBundleItemList { Description = cp.Description, Manufacturer = m.ManufacturerName, SKU = mp.ManufacturerSKU, Quantity = pis.Quantity, IsOption = pis.IsOption }).Distinct().ToList();
            var catalogInfo = _dbCon.tblCatalogStandards.FirstOrDefault(x => x.CatalogStandardID == catId);
            if (catalogInfo != null)
            {
                obj.ProductSpecification = new Model.ProductSpecification();
                obj.ProductSpecification.Comments = catalogInfo.Comments;
                obj.ProductSpecification.Description = catalogInfo.Description;
                obj.ProductSpecification.ImageFile = catalogInfo.ImageFile;
            }
            if (string.IsNullOrEmpty(obj.ProductSpecification.ImageFile) || obj.ProductSpecification.ImageFile == "GS_PNA.jpg")
            {
                var imageInfo = (from pc in _dbCon.tblPartCategories
                                 join cs in _dbCon.tblCatalogStandards on pc.PartCategoryID equals cs.PartCategoryID
                                 where cs.CatalogStandardID == catId
                                 select new { pc.DefaultImage }).FirstOrDefault();
                obj.DefaultImage = imageInfo != null ? imageInfo.DefaultImage : "";
            }
            else
            {
                obj.DefaultImage = obj.ProductSpecification.ImageFile;
            }
            return obj;
        }
        #endregion


    }
}



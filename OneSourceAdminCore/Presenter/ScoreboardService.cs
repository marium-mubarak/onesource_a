﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class ScoreboardService
    {
        private globalserveEntities dbCon;

        public ScoreboardService()
        {
            dbCon = new globalserveEntities();
        }


        public List<GenericStringList> GetScoreboardFilter(int userId, int companyId)
        {
            try
            {
                var query =
                    "select Convert(varchar(50),filterid) 'Key', filtername as 'Value' from tblRequestFilters where ownerid=" + userId +
                    " or (ispublic=1 and ownerid in (select userid from tblContact where companyid=" + companyId +
                    ")) order by filtername";

                var result = dbCon.Database.SqlQuery<GenericStringList>(query).ToList();

                result.Add(new GenericStringList() { Key = "all", Value = "All" });
                result.Add(new GenericStringList() { Key = "byuser", Value = "Logged By Me - Open" });
                result.Add(new GenericStringList() { Key = "30mib", Value = "Minutes to Breach < 30" });
                result.Add(new GenericStringList() { Key = "60mib", Value = "Minutes to Breach < 60" });
                result.Add(new GenericStringList() { Key = "120mib", Value = "Minutes to Breach < 120" });
                result.Add(new GenericStringList() { Key = "breach24", Value = "Breaches Last 24 Hours" });
                result.Add(new GenericStringList() { Key = "breach48", Value = "Breaches Last 48 Hours" });
                result.Add(new GenericStringList() { Key = "breach7", Value = "Breaches Last 7 Days" });
                result.Add(new GenericStringList() { Key = "breach30", Value = "Breaches Last 30 Days" });
                result.Add(new GenericStringList() { Key = "1hour", Value = "Last Hour" });
                result.Add(new GenericStringList() { Key = "6hour", Value = "Last 6 Hours" });
                result.Add(new GenericStringList() { Key = "12hour", Value = "Last 12 Hours" });
                result.Add(new GenericStringList() { Key = "1day", Value = "Last Day" });
                result.Add(new GenericStringList() { Key = "openticket", Value = "Not Closed or Cancelled" });

                return result;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetScoreboardFilter");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetScoreboardFilter");
                }
                return new List<GenericStringList>();
            }
        }

        public List<ScoreBoardListingModel> GetScoreboardList(int? page, int? limit, string sortBy, string direction, bool hideInternalRequest, bool hideSystemRequest, string filterStr, int userId, out int total)
        {
            try
            {
                StaticHelper.LogException("in method now", "GetScoreboardList");

                string filterRequestStr = "";
                int filterId = 0;

                if (int.TryParse(filterStr, out filterId))
                {
                    var chkFilterRequest = dbCon.tblRequestFilters.FirstOrDefault(x => x.FilterID == filterId);
                    if (chkFilterRequest != null)
                    {
                        filterRequestStr = chkFilterRequest.FilterString;
                        StaticHelper.LogException("in filterRequestStr:" + filterRequestStr, "GetScoreboardList");
                    }

                }


                var query = "select top 500 t.ticketid as 'TicketId', art.arTypeName as 'TypeName',CONVERT(VARCHAR(100), ar.DateReceived,20) as 'DateReceived', p.parentcompanyname as 'CompanyName', " +
                            "p.parentcompanyid as 'CompanyId', ts.tktStatusName as 'Status',tp.PriorityName as 'Priority', st.slainstanceid as 'SlaInstanceId', s.slaname + '.' + tac.tacname as 'SlaName' , " +
                            "st.minutesToBreach   as 'MinutesToBreach',c2.firstname + ' ' + c2.lastname as 'CSR',sfa.sfaname as 'GSP',cou.countryname as 'Country'," +
                            "ao.manufacturer as 'Mfg',ao.model as 'Model',cov.coveragename as 'Coverage',CONVERT(VARCHAR(100), t.lastupdate,20) as 'LastUpdated',u2.Username as 'LastUpdateBy', coun.countryname as 'Country2', " +
                            "isnull(cust.customerid, 0) as 'CustomerId' from tblTicket t join tblActionRequest ar on t.arid = ar.arid " +
                            "left join tblActionRequestType art on art.arTypeID = t.tktTypeID left join tblContact c on  " +
                            "c.contactid = ar.affectedUserContactid left join tblContact c2 on t.csrid = c2.contactid " +
                            "left join tblUsers u on c.userid = u.userid left join tblParentCompanies p on " +
                            "ar.companyid = p.parentcompanyid left join tblCustomers cust on cust.parentcompanyid = p.parentcompanyid " +
                            "left join tblTicketStatus ts on t.tktstatusid = ts.tktstatusid " +
                            "left join tblPriority tp on  t.priorityid = tp.priorityid " +
                            "left join tblSLATracking st on t.ticketid = st.ticketid left join tblTermsAndConditions tac on tac.tacid = st.tacid " +
                            "left join tblSLA s on tac.slaid = s.slaid left join gsnetp_tbl_assets asss on asss.assetid = ar.assetid " +
                            "left join gsnetp_tbl_assetsowned ao on asss.assetsownedid = ao.assetsownedid left join tblUsers u2 on u2.userid = t.lastUpdateUserID " +
                            "left join tblSFAs sfa on sfa.sfaid = t.sfaid left join tblCustomerAddresses ca on ca.customeraddressid = ar.addressid " +
                            "left join tblCountries cou on cou.countryid = ca.countryid left join tblAddress ad on ad.addressid = ar.addressid " +
                            "left join tblCountries coun on coun.countryid = ad.countryid left join tblCoverage cov on cov.coverageID = ca.coverageid " +
                            "where ar.isActive=1 and t.isActive=1 ";

                if (hideInternalRequest)
                {
                    query +=
                            " and (u.userid is null or u.userid not in (select userid from tblUsers where upper(username) ='MASTER ID' or upper(username) = 'SYSTEM') and ar.arid  in (select arid from tblTIcket)) ";
                }

                if (hideSystemRequest)
                {
                    query += "and p.parentcompanyid not in (select parentcompanyid from tblParentCompanies where upper(parentcompanyname) like '%GLOBALSERVE%')";
                }


                if (filterStr == "byuser")
                {
                    query = query + " and ar.submittedbyuserid=" + userId +
                            " and t.tktstatusid not in (select tktstatusid from tblticketstatus where isclosed=1 or iscancelled=1)";
                }
                else if (filterStr == "30mib")
                    query = query +
                            " and st.minutestobreach < 30 and st.minutestobreach > 0 and t.tktstatusid not in (select tktstatusid from tblTicketStatus where isClosed=1 or isCancelled=1)";
                else if (filterStr == "60mib")
                    query = query +
                            " and st.minutestobreach < 60 and st.minutestobreach > 0 and t.tktstatusid not in (select tktstatusid from tblTicketStatus where isClosed=1 or isCancelled=1)";
                else if (filterStr == "120mib")
                    query = query +
                            " and st.minutestobreach < 120 and st.minutestobreach > 0 and t.tktstatusid not in (select tktstatusid from tblTicketStatus where isClosed=1 or isCancelled=1)";
                else if (filterStr == "breach24")
                    query = query + " and t.createdate > '" + DateTime.Now.AddHours(-24) + "' and st.isBreach=1";
                else if (filterStr == "breach48")
                    query = query + " and t.createdate > '" + DateTime.Now.AddHours(-48) + "' and st.isBreach=1";
                else if (filterStr == "breach7")
                    query = query + " and t.createdate > '" + DateTime.Now.AddDays(-7) + "' and st.isBreach=1";
                else if (filterStr == "breach30")
                    query = query + " and t.createdate > '" + DateTime.Now.AddDays(-30) + "' and st.isBreach=1";
                else if (filterStr == "1hour")
                    query = query + " and t.createdate > '" + DateTime.Now.AddHours(-1) + "'";
                else if (filterStr == "6hour")
                    query = query + " and t.createdate > '" + DateTime.Now.AddHours(-6) + "'";
                else if (filterStr == "12hour")
                    query = query + " and t.createdate > '" + DateTime.Now.AddHours(-12) + "'";
                else if (filterStr == "1day")
                    query = query + " and t.createdate > '" + DateTime.Now.AddDays(-1) + "'";
                else if (filterStr == "openticket")
                    query = query +
                            " and t.tktstatusid not in (select tktstatusid from tblTicketStatus where isClosed=1 or isCancelled=1)";


                if (!string.IsNullOrWhiteSpace(filterRequestStr))
                {
                    query = query + " " + filterRequestStr;
                }

                query += " order by 1 desc";
                StaticHelper.LogException("after query complete:\n" + query, "GetScoreboardList");

                var result = dbCon.Database.SqlQuery<ScoreBoardListingModel>(query).AsQueryable();
                StaticHelper.LogException("after result---\n", "GetScoreboardList");
                total = result.Count();

                StaticHelper.LogException("after total:" + total, "GetScoreboardList");
                if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
                {
                    if (direction.Trim().ToLower() == "asc")
                    {
                        result = SortHelper.OrderBy(result, sortBy);
                    }
                    else
                    {
                        result = SortHelper.OrderByDescending(result, sortBy);
                    }
                }

                if (page.HasValue && limit.HasValue)
                {
                    int start = (page.Value - 1) * limit.Value;
                    result = result.Skip(start).Take(limit.Value);
                }

                var finalList = result.ToList();

                StaticHelper.LogException("after final list--", "GetScoreboardList");
                return finalList;

            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetScoreboardList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetScoreboardList");
                }
                total = 0;
                return new List<ScoreBoardListingModel>();
            }
        }



        public string GetDefaultFilter(int userId)
        {
            try
            {
                var getPrefrenceId = dbCon.tblPreferences.FirstOrDefault(x => x.PreferenceName.Contains("requestfilter"));

                if (getPrefrenceId != null)
                {
                    var userPrefrenceId =
                        dbCon.tblUserPreferences.FirstOrDefault(
                            c => c.UserID == userId && c.PreferenceID == getPrefrenceId.PreferenceID);

                    if (userPrefrenceId != null)
                    {
                        if (!string.IsNullOrWhiteSpace(userPrefrenceId.PreferenceValue))
                        {
                            return userPrefrenceId.PreferenceValue;
                        }
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetDefaultFilter");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetDefaultFilter");
                }

                return "";
            }

        }

    }
}

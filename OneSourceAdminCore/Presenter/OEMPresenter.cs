﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : July 12 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of OEM Alert"
    /// </summary>
   public class OEMPresenter
    {
          private readonly globalserveEntities _dbCon;

        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
          public OEMPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// give list of manufacturers
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetManufacturers()
        {
            var manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Key = x.ManufacturerID,
                Value = x.ManufacturerName

            }).OrderBy(x=>x.Value).ToList();
            return manufacturers;
        }        

        /// <summary>
        /// give list of OEM alert
        /// </summary>
        /// <returns></returns>
        public List<CustomOEMAlert> GetOEMAlert()
        {
            CustomerPresenter cp = new Presenter.CustomerPresenter();
            var lstCustomers = cp.GetCustomers();
            var oemAlert = _dbCon.tblOEMAlerts.Select(x => new CustomOEMAlert
            {
                OEMAlertId = x.OEMAlertId,
                ManufacturerId = x.ManufacturerId,
                ManufacturerName = x.ManufacturerName,
                Alert = x.Alert,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsTopStory = x.IsTopStory,                
                CustomerId=x.CustomerId
            }).ToList();

            oemAlert.ForEach(x =>
            {
                x.strStartDate = x.StartDate.Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.EndDate != null ? Convert.ToDateTime(x.EndDate).Date.ToString("MM/dd/yyy") : string.Empty);
                x.CustomerName = (x.CustomerId == 0 || x.CustomerId==null) ? "All" : lstCustomers.FirstOrDefault(y=>y.Key==x.CustomerId).Value;
            });
            return oemAlert.OrderBy(x => x.ManufacturerName).ThenBy(x=>x.CustomerName).ToList();
        }
        /// <summary>
        /// add OEM alert to the database
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddOEMAlert(tblOEMAlert alert, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                alert.CreatedDate = DateTime.Now;
                alert.CreatedBy = userId;
                alert.ManufacturerName = string.IsNullOrEmpty(alert.ManufacturerName) == true ? string.Empty : alert.ManufacturerName;
                alert.CustomerId = alert.CustomerId == null ? 0 : alert.CustomerId;

                _dbCon.tblOEMAlerts.Add(alert);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// edit oem alert in the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditOEMAlert(tblOEMAlert model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblOEMAlert alert =
                    _dbCon.tblOEMAlerts.FirstOrDefault(x => x.OEMAlertId == model.OEMAlertId);
                if (alert != null)
                {
                    alert.Alert = model.Alert;
                    alert.ManufacturerId = model.ManufacturerId;
                    alert.ManufacturerName = model.ManufacturerName;
                    alert.StartDate = model.StartDate;
                    alert.EndDate = model.EndDate;
                    alert.UpdatedBy = userId;
                    alert.UpdatedDate = DateTime.Now;
                    alert.IsTopStory = model.IsTopStory;
                    alert.CustomerId = model.CustomerId == null ? 0 : model.CustomerId; ;
                }
                _dbCon.Entry(alert).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// delete oem alert from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteOEMAlert(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblOEMAlerts.FirstOrDefault(x => x.OEMAlertId == id);
                if (alert != null)
                {
                    _dbCon.tblOEMAlerts.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>   
    /// Date created : Feb 08 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for Adding, Editing, Listing of Currency Alert"
    /// </summary>
    public class CurrencyPresenter
    {
        private readonly globalserveEntities _dbCon;
        /// <summary>
        /// This Constructor is responsible for initializing dbContext
        /// </summary>
        public CurrencyPresenter()
        {
            _dbCon = new globalserveEntities();
        }
        /// <summary>
        /// give list of currencies
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetCurrencies()
        {
            var countries = _dbCon.tblCurrencies.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Key = x.CurrencyID,
                Value = x.CurrencyName

            }).OrderBy(x => x.Value).ToList();
            return countries;
        }
        /// <summary>
        /// give currency alert list
        /// </summary>
        /// <returns></returns>
        public List<CustomCurrencyAlert> GetCurrencyAlert()
        {
            var currenciesAlert = _dbCon.tblCurrencyAlerts.Select(x => new CustomCurrencyAlert
            {
                CurrencyAlertId = x.CurrencyAlertId,
                CurrencyId = x.CurrencyId,
                CurrencyName = x.CurrencyName,
                Alert = x.Alert,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                IsTopStory = x.IsTopStory,
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName


            }).OrderBy(x => x.CurrencyName).ToList();

            currenciesAlert.ForEach(x =>
            {
                x.strStartDate = x.StartDate.Date.ToString("MM/dd/yyy");
                x.strEndDate = (x.EndDate != null ? Convert.ToDateTime(x.EndDate).Date.ToString("MM/dd/yyy") : string.Empty);

            });
            return currenciesAlert;
        }
        /// <summary>
        /// add currency alert to the database
        /// </summary>
        /// <param name="alert"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int AddCurrencyAlert(tblCurrencyAlert alert, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                alert.CreatedDate = DateTime.Now;
                alert.CreatedBy = userId;
                alert.CurrencyName = string.IsNullOrEmpty(alert.CurrencyName) == true ? string.Empty : alert.CurrencyName;
                alert.CustomerId = alert.CustomerId ?? 0;
                _dbCon.tblCurrencyAlerts.Add(alert);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// edit currency alert to the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int EditCurrencyAlert(tblCurrencyAlert model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCurrencyAlert alert =
                    _dbCon.tblCurrencyAlerts.FirstOrDefault(x => x.CurrencyAlertId == model.CurrencyAlertId);
                if (alert != null)
                {
                    alert.Alert = model.Alert;
                    alert.CurrencyId = model.CurrencyId;
                    alert.CurrencyName = model.CurrencyName;
                    alert.StartDate = model.StartDate;
                    alert.EndDate = model.EndDate;
                    alert.UpdatedBy = userId;
                    alert.UpdatedDate = DateTime.Now;
                    alert.IsTopStory = model.IsTopStory;
                    alert.CustomerId = model.CustomerId ?? 0;
                    alert.CustomerName = model.CustomerName;
                }
                _dbCon.Entry(alert).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        /// <summary>
        /// delete currency alert from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteCurrencyAlert(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var alert = _dbCon.tblCurrencyAlerts.FirstOrDefault(x => x.CurrencyAlertId == id);
                if (alert != null)
                {
                    _dbCon.tblCurrencyAlerts.Remove(alert);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        #region  CURRENCY setup CRUD Operations

        public CurrencySetupModel GetCurrencySetupModel(int currencyId)
        {
            CurrencySetupModel obj = new Model.CurrencySetupModel();
            CountryPresenter countryPresent = new Presenter.CountryPresenter();

            obj.CountriesList = countryPresent.GetCountries();
            obj.CurrenciesList = _dbCon.sp_getAdminCurrencyList_R().ToList();
            obj.ISOCurrenciesList = GetISOCurrencies(currencyId);
            return obj;
        }
        public List<sp_getISOCurrencies_R_Result> GetISOCurrencies(int currencyId)
        {
            return _dbCon.sp_getISOCurrencies_R(currencyId).ToList();
        }
        public List<sp_getCurrencyInfo_R_Result> GetCurrencyInfo(int currencyId)
        {
            return _dbCon.sp_getCurrencyInfo_R(currencyId).ToList();
        }
        public int EditCurrency(tblCurrency model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCurrency curr =
                    _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == model.CurrencyID);
                if (curr != null)
                {
                    curr.CurrencyName = model.CurrencyName;
                    curr.LastUpdatedByUserID = userId;
                    curr.LastUpdated = DateTime.Now;
                    curr.IsActive = model.IsActive;
                    curr.CountryID = model.CountryID;
                    curr.CurrencySymbol = model.CurrencySymbol;
                    curr.CurrencyCodeID = model.CurrencyCodeID;
                    curr.Symbol = model.Symbol;
                }
                _dbCon.Entry(curr).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        public int AddCurrency(tblCurrency model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                //CurrencyName, LastUpdatedByUserID, CurrencyCodeID, CountryID, CurrencySymbol,Symbol
                model.LastUpdated = DateTime.Now;
                model.LastUpdatedByUserID = userId;
                _dbCon.tblCurrencies.Add(model);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        public tblCurrencyCode GetNewISOInfo(int currencyCodeID)
        {
            var curr =
                   _dbCon.tblCurrencyCodes.Where(x => x.CurrencyCodeID == currencyCodeID).ToList();
            tblCurrencyCode obj = new tblCurrencyCode();
            obj.CountryCodeID = curr[0].CountryCodeID;
            obj.CurrencyCodeID = curr[0].CurrencyCodeID;
            obj.ISOCurrencyName = curr[0].ISOCurrencyName;
            obj.ISOCurrencyCode = curr[0].ISOCurrencyCode;
            obj.ISOCurrencyCodeNumber = curr[0].ISOCurrencyCodeNumber;
            obj.IsActive = curr[0].IsActive;
            obj.LastUpdated = curr[0].LastUpdated;
            obj.LastUpdatedByUserID = curr[0].LastUpdatedByUserID;

            return obj;
        }
        public int SetActiveStatus(int currencyId,bool isactive,int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCurrency curr =
                    _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == currencyId);
                if (curr != null)
                {
                  
                    curr.LastUpdatedByUserID = userId;
                    curr.LastUpdated = DateTime.Now;
                    curr.IsActive = isactive;
                
                }
                _dbCon.Entry(curr).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        #endregion

        #region ISO CURRENCY setup CRUD Operations
        public ISOCurrencySetupModel GetISOCurrencySetupModel(int currencyId)
        {
            ISOCurrencySetupModel obj = new Model.ISOCurrencySetupModel();

            obj.ISOCurrencyList = _dbCon.sp_getISOCurrencyList_R().OrderBy(x=>x.isocurrencyname).ToList();
            obj.DefaultISOCurrencyList = _dbCon.tblCountryCodes.Where(x => x.IsActive == true).Select(x => new GenericList
            {
                Value=x.ISOCountryName,
                Key=x.CountryCodeID
            }).ToList();
            return obj;
        }
        public int SetISOActiveStatus(int isocurrencyId, bool isactive, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCurrencyCode curr =
                    _dbCon.tblCurrencyCodes.FirstOrDefault(x => x.CurrencyCodeID == isocurrencyId);
                if (curr != null)
                {

                    curr.LastUpdatedByUserID = userId;
                    curr.LastUpdated = DateTime.Now;
                    curr.IsActive = isactive;

                }
                _dbCon.Entry(curr).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        public int EditISOCurrency(tblCurrencyCode model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                tblCurrencyCode curr =
                    _dbCon.tblCurrencyCodes.FirstOrDefault(x => x.CurrencyCodeID == model.CurrencyCodeID);
                if (curr != null)
                {
                    //if already currency exist with the same name,code,codenumber
                    tblCurrencyCode isalreadycurr =
                  _dbCon.tblCurrencyCodes.FirstOrDefault(x => x.CurrencyCodeID != model.CurrencyCodeID &&
                                (x.ISOCurrencyName == model.ISOCurrencyName.Trim() || x.ISOCurrencyCode == model.ISOCurrencyCode || x.ISOCurrencyCodeNumber == model.ISOCurrencyCodeNumber)
                  );
                    if (isalreadycurr != null)
                        return 0;

                    curr.ISOCurrencyName = model.ISOCurrencyName;
                    curr.LastUpdatedByUserID = userId;
                    curr.LastUpdated = DateTime.Now;
                    curr.IsActive = model.IsActive;
                    curr.CountryCodeID = model.CountryCodeID;
                    curr.ISOCurrencyCode = model.ISOCurrencyCode;
                    curr.CurrencyCodeID = model.CurrencyCodeID;
                    curr.ISOCurrencyCodeNumber = model.ISOCurrencyCodeNumber;
                }
                _dbCon.Entry(curr).State = System.Data.Entity.EntityState.Modified;
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        public int AddISOCurrency(tblCurrencyCode model, int userId)
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                
                tblCurrencyCode curr = 
                  _dbCon.tblCurrencyCodes.FirstOrDefault(x => x.ISOCurrencyName == model.ISOCurrencyName.Trim() || x.ISOCurrencyCode==model.ISOCurrencyCode || x.ISOCurrencyCodeNumber==model.ISOCurrencyCodeNumber);
                if (curr != null)
                    return 0;

                model.LastUpdated = DateTime.Now;
                model.LastUpdatedByUserID = userId;
                _dbCon.tblCurrencyCodes.Add(model);
                count = _dbCon.SaveChanges();
                transaction.Commit();

            }
            return count;
        }
        #endregion


        public List<GenericList> GetCustomerCountryCurrency(int customerId,int countryId)
        {

            var currencies = (from ccc in _dbCon.tblCustomerCountryCurrencies
                           join c in _dbCon.tblCurrencies on ccc.CurrencyID equals c.CurrencyID
                             where ccc.CountryID == countryId && ccc.IsDefaultCurrency == true
                             && ccc.IsActive == 1 && ccc.CustomerID == customerId
                             select new GenericList { Key = ccc.CurrencyID, Value = c.CurrencyName}).Distinct().ToList();
            return currencies;

        }
    }
}

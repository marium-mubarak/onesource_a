﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdminCore.Presenter
{
    public class SoftwareQuotesSearchPresenter
    {
        globalserveEntities dbCon;
        CustomerPresenter _customerService;
        AdminSetting currentSettings;
        public SoftwareQuotesSearchPresenter()
        {
            dbCon = new globalserveEntities();
            _customerService = new CustomerPresenter();
            currentSettings = new AdminSetting();

        }

        public int DeleteLineItem(int id)
        {
            try
            {
                int count;
                using (var transcation = dbCon.Database.BeginTransaction())
                {
                    var item = dbCon.tbl_QuoteResponseItems_Software.FirstOrDefault(x => x.ResponseItemId == id);
                    if (item != null)
                    {
                        dbCon.tbl_QuoteResponseItems_Software.Remove(item);
                        count = dbCon.SaveChanges();
                    }
                    else
                    {
                        count = 0;
                    }
                    transcation.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => DeleteLineItem<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => DeleteLineItem");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteLineItem");
                }
                return 0;
            }
        }
        public int DeleteComment(int id)
        {
            try
            {
                int count;
                using (var transcation = dbCon.Database.BeginTransaction())
                {
                    var item = dbCon.tblQuoteComments_Software.FirstOrDefault(x => x.CommenId == id);
                    if (item != null)
                    {
                        dbCon.tblQuoteComments_Software.Remove(item);
                        count = dbCon.SaveChanges();
                    }
                    else
                    {
                        count = 0;
                    }
                    transcation.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => DeleteLineItem<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => DeleteLineItem");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteLineItem");
                }
                return 0;
            }
        }

        public int DeleteAttachment(int id)
        {
            try
            {
                int count;
                using (var transcation = dbCon.Database.BeginTransaction())
                {
                    var item = dbCon.tblQuoteAttachments_Software.FirstOrDefault(x => x.AttachmentId == id);
                    if (item != null)
                    {
                        dbCon.tblQuoteAttachments_Software.Remove(item);
                        count = dbCon.SaveChanges();
                    }
                    else
                    {
                        count = 0;
                    }
                    transcation.Commit();
                }
                return count;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => DeleteLineItem<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => DeleteLineItem");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteLineItem");
                }
                return 0;
            }
        }
        public List<GenericList> GetSoftwareQuoteCustomersByUser(int supplierId = 0)
        {
            try
            {

                var customers = (from c in dbCon.tblCustomers
                                 join sq in dbCon.tblQuote_Softwares
                                 on c.CustomerID equals sq.CustomerId
                                 where c.IsActive == true
                                 select new { c.CustomerID, c.CustomerName }).Distinct().ToList();

                if (supplierId > 0)
                {
                    customers = (from c in dbCon.tblCustomers
                                 join sq in dbCon.tblQuote_Softwares
                                 on c.CustomerID equals sq.CustomerId
                                 where c.IsActive == true && sq.SupplierId == supplierId
                                 select new { c.CustomerID, c.CustomerName }).Distinct().ToList();
                }

                var quoteCustomers = customers.Select(x => new GenericList
                {
                    Value = x.CustomerName,
                    Key = x.CustomerID
                }).OrderBy(x => x.Value).ToList();

                return quoteCustomers;

            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetSoftwareQuoteCustomersByUser<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetSoftwareQuoteCustomersByUser");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSoftwareQuoteCustomersByUser");
                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        /// Gets Software Program List (SUNNY GOHAR : 7th Jan 2019)
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetSoftwareProgramList()
        {
            try
            {
                var list = dbCon.tblPrograms_Softwares.Select(x => new GenericList
                {
                    Key = x.ProgramID,
                    Value = x.ProgramName
                }).ToList();

                return list;
            }

            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSoftwareProgramList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "Inner EX- GetSoftwareProgramList");
                }
                return new List<GenericList>();
            }
        }

        public List<SoftwareQuotesDetailModel> GetSoftwareQuotes(int customerId, int supplierId = 0, DateTime? startDate = null, DateTime? endDate = null,int programId=0)//added program id ( SUNNY GOHAR: 7th Jan 2019 )
        {
            try
            {
                var query = dbCon.tblQuote_Softwares.Where(x => x.CustomerId == customerId && x.ProgramId == programId).ToList();

                if (startDate != null)
                {
                    var start = Convert.ToDateTime(startDate);
                    query = query.Where(x => x.CreatedDate.Date >= start.Date).ToList();
                }
                if (endDate != null)
                {
                    var end = Convert.ToDateTime(endDate);
                    query = query.Where(x => x.CreatedDate.Date <= end.Date).ToList();
                }
                if (supplierId > 0)
                {
                    query = query.Where(x => x.SupplierId == supplierId).ToList();
                }

                var softwareQuotes = query.Select(x => new SoftwareQuotesDetailModel
                {
                    QuoteId = x.QuoteId,
                    CustomerReference = x.CustomerReference,
                    Country = dbCon.tblCountries.FirstOrDefault(c => c.CountryID == x.CountryId && c.IsActive == true) != null
                    ? dbCon.tblCountries.FirstOrDefault(c => c.CountryID == x.CountryId && c.IsActive == true).CountryName : "-",
                    Supplier = dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == x.SupplierId && s.IsActive == true) != null
                    ? dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == x.SupplierId && s.IsActive == true).SFAName : "-",
                    Status = dbCon.tblQuoteStatuses_Software.FirstOrDefault(s => s.QuoteStatusId == x.QuoteStatusId && s.IsActive == true) != null
                    ? dbCon.tblQuoteStatuses_Software.FirstOrDefault(s => s.QuoteStatusId == x.QuoteStatusId && s.IsActive == true).StatusName : "-",
                    CreatedDate = x.CreatedDate.ToShortDateString(),

                }).OrderByDescending(x => x.QuoteId).Distinct().ToList();

                return softwareQuotes;

            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetSoftwareQuotes<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetSoftwareQuotes");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSoftwareQuotes");
                }
                return new List<SoftwareQuotesDetailModel>();
            }
        }

        public SoftwareQuotesDetailModel GetSoftwareQuoteDetails(int quoteId, int userId, bool isSupplier = false)
        {
            try
            {
                var quoteDetail = dbCon.tblQuote_Softwares.FirstOrDefault(x => x.QuoteId == quoteId);
                dynamic obj = null;

                if (quoteDetail != null)
                {
                    var model = new SoftwareQuotesDetailModel();
                    model.QuoteId = quoteId;
                    model.Country = dbCon.tblCountries.FirstOrDefault(c => c.CountryID == quoteDetail.CountryId && c.IsActive == true) != null
                    ? dbCon.tblCountries.FirstOrDefault(c => c.CountryID == quoteDetail.CountryId && c.IsActive == true).CountryName : "-";
                    model.CustomerReference = quoteDetail.CustomerReference != null ? quoteDetail.CustomerReference : "-";
                    model.CreatedDate = quoteDetail.CreatedDate != null ? quoteDetail.CreatedDate.ToShortDateString() : "-";
                    model.Status = dbCon.tblQuoteStatuses_Software.FirstOrDefault(s => s.QuoteStatusId == quoteDetail.QuoteStatusId && s.IsActive == true) != null
                    ? dbCon.tblQuoteStatuses_Software.FirstOrDefault(s => s.QuoteStatusId == quoteDetail.QuoteStatusId && s.IsActive == true).StatusName : "-";
                    model.Supplier = dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == quoteDetail.SupplierId && s.IsActive == true) != null
                    ? dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == quoteDetail.SupplierId && s.IsActive == true).SFAName : "-";
                    model.Currency = dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == quoteDetail.CurrencyId) != null
                        ? dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == quoteDetail.CurrencyId).CurrencyName : "-";
                    model.CreatedBy = dbCon.tblUsers.FirstOrDefault(x => x.UserID == quoteDetail.CreatedBy) != null
                        ? dbCon.tblUsers.FirstOrDefault(x => x.UserID == quoteDetail.CreatedBy).UserName : "-";

                    //added more fields for program type (SUNNY GOHAR : 8th Jan 2018)
                    var program = dbCon.tblPrograms_Softwares.FirstOrDefault(x => x.ProgramID == quoteDetail.ProgramId);
                    model.ProgramName = program!=null?program.ProgramName:"-";
                    model.OrganizationName = quoteDetail.OrganizationName;
                    model.ContactName = quoteDetail.ContactName;
                    model.ContactEmail = quoteDetail.ContactEmail;
                    model.ContactPhone = quoteDetail.ContactPhone;
                    model.AgreementType= dbCon.TblSoftwareAgreementTypes.FirstOrDefault(x => x.AgreementTypeId == quoteDetail.AgreementTypeId) != null ?
                        dbCon.TblSoftwareAgreementTypes.FirstOrDefault(x => x.AgreementTypeId == quoteDetail.AgreementTypeId).AgreementType : "-";
                    model.AgreementNumber = quoteDetail.AgreementNumber;
                    model.ExpectedPurchaseDate = quoteDetail.ExpectedPurchaseDate;
                    model.InvoiceCountry = dbCon.tblCountries.FirstOrDefault(x => x.CountryID == quoteDetail.InvoiceCountryId) != null ? dbCon.tblCountries.FirstOrDefault(x => x.CountryID == quoteDetail.InvoiceCountryId).CountryName : "-";
                    model.AgreementLevel = dbCon.tblAgreementLevel_Software.FirstOrDefault(x => x.AgreementLevelId == quoteDetail.AgreementLevelId) != null ? dbCon.tblAgreementLevel_Software.FirstOrDefault(x => x.AgreementLevelId == quoteDetail.AgreementLevelId).AgreementLevel : "-";
                    var requestItems = dbCon.tblQuoteItems_Software.Where(x => x.SoftwareQuoteId == quoteDetail.QuoteId && x.IsActive == true).ToList();
                    model.RequestItems = requestItems.Select(x => new SoftwareQuoteItems
                    {

                        QuoteItemId = x.QuoteItemId,
                        Description = x.Description,
                        IsActive = x.IsActive,
                        Quantity = x.Quantity,
                        SoftwareQuoteId = x.SoftwareQuoteId,
                        LastUpdated = x.LastUpdated,
                        Manufacturer = x.Manufacturer,
                        Sku = x.Sku,
                        SoftwareItemId = x.SoftwareItemId,
                        UpdatedBy = x.UpdatedBy,
                        PriceLevel = dbCon.tblSoftwares.FirstOrDefault(s => s.ItemNumber == x.SoftwareItemId) != null ? dbCon.tblSoftwares.FirstOrDefault(s => s.ItemNumber == x.SoftwareItemId).PriceLevel : "-"

                    }).ToList();

                    var responseItems = new List<tbl_QuoteResponseItems_Software>();

                    //if (!isSupplier)
                    //{
                        responseItems = dbCon.tbl_QuoteResponseItems_Software.Where(x => x.SoftwareQuoteId == quoteDetail.QuoteId && x.IsActive == true).ToList();
                    //}
                    //else
                    //{
                    //    responseItems = dbCon.tbl_QuoteResponseItems_Software.Where(x => x.SoftwareQuoteId == quoteDetail.QuoteId && x.IsAdminResponse == false && x.IsActive == true && x.CreatedBy == userId).ToList();
                        
                    //}
                    model.ResponseItems = responseItems.Select(s => new SoftwareQuotesResponseLineItem
                    {
                        ResponseItemId = s.ResponseItemId,
                        SoftwareQuoteId = s.SoftwareQuoteId,
                        Description = s.Description,
                        Eol = s.Eol != null ? s.Eol.Date.ToShortDateString() : "-",
                        LeadTime = s.LeadTime,
                        Manufacturer = s.ManufacturerId.ToString(),
                        ManufacturerName = dbCon.tblManufacturers.FirstOrDefault(m => m.ManufacturerID == s.ManufacturerId) != null ? dbCon.tblManufacturers.FirstOrDefault(m => m.ManufacturerID == s.ManufacturerId).ManufacturerName : "-",
                        Price = s.Price,
                        Quantity = s.Quantity,
                        Sku = s.Sku,
                        CreatedBy = dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId) != null ? dbCon.tblUsers.FirstOrDefault(x => x.UserID == userId).UserName : "-",
                        Supplier = dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == s.CreatedBy) != null ? dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == s.CreatedBy).SFAName : "-",
                        isSupplier = s.IsAdminResponse ==false ? true :false,
                    }).OrderByDescending(o => o.ResponseItemId).Distinct().ToList();

                    model.QuoteAttachments = GetAllAttachments(quoteDetail.QuoteId, userId, isSupplier, true);
                    model.Attachments = GetAllAttachments(quoteDetail.QuoteId, userId, isSupplier);

                    var quoteComments = dbCon.tblQuoteComments_Software.Where(x => x.SoftwareQuoteId == quoteDetail.QuoteId && (x.IsAdminComment == null || x.IsAdminComment == false) && (x.IsSupplierComment == null || x.IsSupplierComment == false)).ToList();

                    model.QuoteComments = GetAllComments(quoteDetail.QuoteId, userId, isSupplier, true);
                    model.Comments = GetAllComments(quoteDetail.QuoteId, userId, isSupplier);

                    obj = model;
                }

                return obj;

            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetSoftwareQuoteCustomersByUser<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetSoftwareQuoteCustomersByUser");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSoftwareQuoteCustomersByUser");
                }
                return null;
            }
        }

        public List<SelectListItem> GetManufacturers()
        {
            try
            {
                var list  = dbCon.tblManufacturers.Where(x => x.IsActive == true).ToList();
                
                var result = list.Select(x => new SelectListItem
                { Value = x.ManufacturerID.ToString(), Text = x.ManufacturerName }).Distinct().OrderBy(x => x.Value).ToList();
                return result;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetManufacturers<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetManufacturers");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturers");
                }
                return new List<SelectListItem>();
            }
        }

        public string GetComment(int id)
        {
            try
            {
                return dbCon.tblQuoteComments_Software.FirstOrDefault(x => x.CommenId == id) != null ? dbCon.tblQuoteComments_Software.FirstOrDefault(x => x.CommenId == id).Comment : "";
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetComment");
                }
                return "";
            }
        }

        public int SaveResponseLineItem(SoftwareQuotesResponseLineItem model, bool isAdmin = true)
        {


            try
            {
                tbl_QuoteResponseItems_Software lineItem = new tbl_QuoteResponseItems_Software();

                if (model.ResponseItemId > 0)
                {
                    lineItem = dbCon.tbl_QuoteResponseItems_Software.FirstOrDefault(x => x.ResponseItemId == model.ResponseItemId);
                }

                //Assign values to entity columns
                lineItem.SoftwareQuoteId = model.SoftwareQuoteId;
                lineItem.Description = model.Description;
                lineItem.LeadTime = model.LeadTime;
                lineItem.ManufacturerId = Convert.ToInt32(model.Manufacturer);
                lineItem.Quantity = model.Quantity;
                lineItem.Price = model.Price;
                lineItem.Sku = model.Sku;
                lineItem.Eol = Convert.ToDateTime(model.Eol);
                lineItem.CreatedDate = DateTime.Now;
                lineItem.CreatedBy = model.UserId;
                lineItem.IsActive = true;
                lineItem.IsAdminResponse = isAdmin;

                int count = 0;
                using (var transaction = dbCon.Database.BeginTransaction())
                {
                    if (model.ResponseItemId > 0)
                    {
                        dbCon.Entry(lineItem).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dbCon.tbl_QuoteResponseItems_Software.Add(lineItem);
                    }

                    count = dbCon.SaveChanges();

                    transaction.Commit();
                }

                return count;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetManufacturers<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetManufacturers");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetManufacturers");
                }
                return 0;
            }
        }

        public int AddComment(SoftwareQuoteComments model, int userId, bool isAdmin = true)
        {
            try
            {
                var comment = new tblQuoteComments_Software();

                //Assign values to entity columns
                comment.SoftwareQuoteId = model.SoftwareQuoteId;
                comment.Comment = model.Comment;
                comment.IsActive = true;
                if (isAdmin)
                {
                    comment.IsAdminComment = true;
                }
                else
                {
                    comment.IsSupplierComment = true;
                }
                comment.UpdateBy = userId;
                comment.UpdatedDate = DateTime.Now;

                int count = 0;
                using (var transaction = dbCon.Database.BeginTransaction())
                {
                    dbCon.tblQuoteComments_Software.Add(comment);

                    count = dbCon.SaveChanges();

                    transaction.Commit();
                }

                return count;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => AddComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => AddComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddComment");
                }
                return 0;
            }
        }
        public int AddAttachment(int quoteId, HttpPostedFileBase attachment, int userId,string fileId, bool isSupplier = false)
        {
            try
            {
                var newAttachment = new tblQuoteAttachments_Software();
                HttpPostedFileBase file = attachment;
                newAttachment.AttachmentUniqueId = fileId;
                newAttachment.AttachmentName = file.FileName;
                newAttachment.SoftwareQuoteId = quoteId;
                newAttachment.CreatedBy = userId;
                newAttachment.CreatedDate = DateTime.Now;
                newAttachment.IsActive = true;
                if (isSupplier)
                {
                    newAttachment.IsSupplierAttachment = true;
                }
                else
                {
                    newAttachment.IsAdminAttachment = true;
                }
                var ext = Path.GetExtension(file.FileName);
                newAttachment.MimeTypeExtensionId = GetMimeTypeIdByExtension(ext.ToLower());

                int count = 0;
                using (var transaction = dbCon.Database.BeginTransaction())
                {
                    dbCon.tblQuoteAttachments_Software.Add(newAttachment);

                    count = dbCon.SaveChanges();

                    transaction.Commit();
                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => AddAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => AddAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddAttachment");
                }
                return 0;
            }

        }

        public List<SoftwareQuoteAttachments> GetAllAttachments(int quoteId, int userId, bool isSupplier = false, bool isQuoteAttachment = false)
        {
            try
            {
                var quoteAttachments = dbCon.tblQuoteAttachments_Software.Where(x => x.SoftwareQuoteId == quoteId).ToList();

                if (isQuoteAttachment)
                {
                    quoteAttachments = quoteAttachments.Where(x => (x.IsAdminAttachment == null || x.IsAdminAttachment == false) && (x.IsSupplierAttachment == null || x.IsSupplierAttachment == false)).ToList();
                }
                else
                {
                    //if (isSupplier)
                    //{
                    //    quoteAttachments = quoteAttachments.Where(x => x.IsSupplierAttachment == true && x.CreatedBy == userId).ToList();
                    //}
                    //else
                    //{
                    quoteAttachments = quoteAttachments.Where(x => x.IsAdminAttachment == true || x.IsSupplierAttachment == true).ToList();
                    //}
                }

                var result = quoteAttachments.Select(x => new SoftwareQuoteAttachments
                {
                    AttachmentId = x.AttachmentId,
                    AttachmentName = x.AttachmentName,
                    AttachmentUniqueId = x.AttachmentUniqueId,
                    CreatedBy = dbCon.tblUsers.FirstOrDefault(u => u.UserID == x.CreatedBy) != null ? dbCon.tblUsers.FirstOrDefault(u => u.UserID == x.CreatedBy).UserName : "-",
                    CreatedDate = x.CreatedDate != null ? x.CreatedDate.Date.ToShortDateString() : "-",
                    IsActive = x.IsActive,
                    IsAdminAttachment = x.IsAdminAttachment,
                    IsSupplierAttachment = x.IsSupplierAttachment,
                    MimeTypeExtensionId = x.MimeTypeExtensionId,
                    SoftwareQuoteId = Convert.ToInt32(x.SoftwareQuoteId),
                    Supplier = dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == x.CreatedBy) != null ? dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == x.CreatedBy).SFAName : "-"

                }).OrderByDescending(o => o.AttachmentId).Distinct().ToList();

                return result;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetAllAttachments<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetAllAttachments");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAllAttachments");
                }
                return new List<SoftwareQuoteAttachments>();
            }

        }

        public List<SoftwareQuoteComments> GetAllComments(int quoteId, int userId, bool isSupplier = false, bool isQuoteComment = false)
        {
            try
            {
                var quoteComments = dbCon.tblQuoteComments_Software.Where(x => x.SoftwareQuoteId == quoteId).ToList();

                if (isQuoteComment)
                {
                    quoteComments = quoteComments.Where(x => (x.IsAdminComment == null || x.IsAdminComment == false) && (x.IsSupplierComment == null || x.IsSupplierComment == false)).ToList();
                }
                else
                {
                    //if (isSupplier)
                    //{
                    //    quoteComments = quoteComments.Where(x => x.IsSupplierComment == true && x.UpdateBy == userId).ToList();
                    //}
                    //else
                    //{
                        quoteComments = quoteComments.Where(x => x.IsAdminComment == true || x.IsSupplierComment == true).ToList();
                    //}
                }

                var result = quoteComments.Select(x => new SoftwareQuoteComments
                {
                    CommenId = x.CommenId,
                    Comment = x.Comment,
                    UpdateBy = dbCon.tblUsers.FirstOrDefault(u => u.UserID == x.UpdateBy) != null ? dbCon.tblUsers.FirstOrDefault(u => u.UserID == x.UpdateBy).UserName : "-",
                    UpdatedDate = x.UpdatedDate != null ? x.UpdatedDate.Date.ToShortDateString() : "-",
                    IsActive = x.IsActive,
                    IsSupplierComment = x.IsSupplierComment,
                    IsAdminComment = x.IsAdminComment,
                    SoftwareQuoteId = Convert.ToInt32(x.SoftwareQuoteId),
                    Supplier = dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == x.UpdateBy) != null ? dbCon.tblSFAs.FirstOrDefault(s => s.SFAID == x.UpdateBy).SFAName : "-"

                }).OrderByDescending(o => o.CommenId).Distinct().ToList();

                return result;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetAllAttachments<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetAllAttachments");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetAllAttachments");
                }
                return new List<SoftwareQuoteComments>();
            }

        }

        public int GetMimeTypeIdByExtension(string ext)
        {
            try
            {
                return dbCon.tblMimeTypeExtensions.FirstOrDefault(x => x.extension == ext) != null ?
                     dbCon.tblMimeTypeExtensions.FirstOrDefault(x => x.extension == ext).mimeTypeID : 0;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => GetMimeTypeIdByExtension<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => GetMimeTypeIdByExtension");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetMimeTypeIdByExtension");
                }
                return 0;
            }
        }

        public int UpdateStatus(int id, string status)
        {
            try
            {
                int count = 0;

                var item = dbCon.tblQuoteStatuses_Software.FirstOrDefault(x => x.StatusName == status);
                if (item != null)
                {
                    var quote = dbCon.tblQuote_Softwares.FirstOrDefault(x => x.QuoteId == id);
                    if (quote != null)
                    {
                        quote.QuoteStatusId = item.QuoteStatusId;
                    }

                    using (var transcation = dbCon.Database.BeginTransaction())
                    {
                        dbCon.Entry(quote).State = System.Data.Entity.EntityState.Modified;
                        count = dbCon.SaveChanges();
                        transcation.Commit();
                    }
                }

                return count;
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("Application", "-->>>>>SoftwareQuotesSearchPresenter => DeleteLineItem<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "SoftwareQuotesSearchPresenter => DeleteLineItem");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "DeleteLineItem");
                }
                return 0;
            }
        }
    }
}

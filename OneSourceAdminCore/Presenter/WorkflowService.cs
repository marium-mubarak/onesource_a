﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSSupport;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class WorkflowService
    {


        private globalserveEntities dbCon;

        public WorkflowService()
        {
            dbCon = new globalserveEntities();
        }

        public List<Sp_GetWorkFlowList_AdminR_Result> GetWorkFlowList(string txt)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt))
                {
                    txt = "";
                }
                var list = dbCon.Sp_GetWorkFlowList_AdminR(txt).ToList();
                return list;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetWorkFlowList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetWorkFlowList");
                }
                return new List<Sp_GetWorkFlowList_AdminR_Result>();
            }
        }


        public tblWorkflow GetWorkflowDetailsById(int id)
        {
            try
            {
                if (id > 0)
                {
                    var wf = dbCon.tblWorkflows.FirstOrDefault(x => x.WorkflowID == id);
                    if (wf != null)
                    {
                        return wf;
                    }
                }
                return new tblWorkflow();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetWorkflowDetailsById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetWorkflowDetailsById");
                }
                return new tblWorkflow();
            }
        }

        public string GetWorkSequenceById(int id)
        {
            try
            {
                if (id > 0)
                {
                    var workFlow = new GSSupport.Workflow();
                    workFlow.LoadWorkflow(id);

                    //var count = workFlow.GetStepsCount();
                    //for (int i = 0; i < count - 1; i++)
                    //{
                    //    var data = workFlow.GetStep(i);
                    //}
                    //var getCount = workFlow.GetStep(0);

                    var workFlowDetails = dbCon.tblWorkflowDetails.Where(x => x.workflowID == id).ToList();

                    var steps = "";
                    if (workFlowDetails.Count > 0)
                    {
                        foreach (var item in workFlowDetails)
                        {

                            var workFlowStep = new WorkflowStep();

                            if (item.qualificationID != null && item.qualificationID > 0)
                            {
                                workFlowStep.Qualification = new Qualification(Convert.ToInt32(item.qualificationID));
                            }

                            if (item.actionid != null && item.actionid > 0)
                            {
                                workFlowStep.Action = new ActionFactory().CreateAction(item.actionid);

                                workFlowStep.Qualification = new Qualification(Convert.ToInt32(item.qualificationID));

                                if (item.actionConjID != null && item.actionConjID > 0)
                                {
                                    workFlowStep.ActionConjunction = new Conjunction(Convert.ToInt32(item.actionConjID));
                                }
                            }
                            steps = steps + "" + workFlowStep.GetQualificationText() + "<br/>" + workFlowStep.GetActionText();
                        }

                    }

                    return (string.IsNullOrWhiteSpace(steps) ? "-" : steps.Trim());
                }
                return "-";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetWorkSequenceById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetWorkSequenceById");
                }

                return "-";
            }
        }

        public bool CopyWorkFlowById(int id)
        {
            try
            {
                var workFlow = new GSSupport.Workflow();
                workFlow.LoadWorkflow(id);

                var workFlowCopy = new GSSupport.Workflow();
                workFlowCopy = workFlow.Copy();
                if (workFlowCopy.Save())
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "CopyWorkFlowById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "CopyWorkFlowById");
                }

                return false;
            }

        }

        public List<GenericStringList> GetArCategoryList()
        {
            try
            {
                var query =
                    "select  arcatid as 'Key',arcatname as 'Value'  from tblARCategories where isactive = 1 and arcatid in (select arcategoryid from tblARCategoryFields where isSelectable=1) order by arcatname";
                var result = dbCon.Database.SqlQuery<GenericStringList>(query).ToList();

                if (result.Count > 0) return result.ToList();

                return new List<GenericStringList>();
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetFieldTypeRelationList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetFieldTypeRelationList");
                }
                return new List<GenericStringList>();
            }
        }

        public Workflow GetWorkFlowById(int id)
        {
            try
            {
                var workFlow = new GSSupport.Workflow();
                workFlow.LoadWorkflow(id);

                return workFlow;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetFieldTypeRelationList");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetFieldTypeRelationList");
                }
                return new Workflow();
            }
        }
    }


}

﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSourceAdminCore.Presenter
{
    public class AssetDisposalQuote
    {
        private readonly globalserveEntities _dbCon;
        tblAssetDisposalQuote _assetdisposal;

        public AssetDisposalQuote()
        {
            _dbCon = new globalserveEntities();
            _assetdisposal = new tblAssetDisposalQuote();
        }

        #region Properties

        public tblAssetDisposalQuote AssetDisposal
        {
            get { return _assetdisposal; }
            set { _assetdisposal = value; }
        }

        #endregion
        #region Software Quotes

        public int Save()
        {
            int count;
            using (var transaction = _dbCon.Database.BeginTransaction())
            {
                if (_assetdisposal.AssertDisposalId > 0)
                {
                    _dbCon.Entry(_assetdisposal).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    _dbCon.tblAssetDisposalQuotes.Add(_assetdisposal);
                }

                count = _dbCon.SaveChanges();
                transaction.Commit();
            }

            return count;
        }

        public int Delete(int id)
        {
            int count;
            using (var transcation = _dbCon.Database.BeginTransaction())
            {
                var data = _dbCon.tblAssetDisposalQuotes.FirstOrDefault(x => x.AssertDisposalId == id);
                if (data != null)
                {
                    _dbCon.tblAssetDisposalQuotes.Remove(data);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    count = 0;
                }
                transcation.Commit();
            }
            return count;
        }

        public List<tblAssetDisposalQuote> GetAssetDisposalQuotes()
        {
            return _dbCon.tblAssetDisposalQuotes.ToList();
        }

        public List<AssetDisposalModel> Get()
        {
            var ad = (from S in _dbCon.tblAssetDisposalQuotes
                      join C in _dbCon.tblCountries on S.CountryId equals C.CountryID
                      //join CR in _dbCon.tblCurrencies on S.CurrencyId equals CR.CurrencyID
                      //join F in _dbCon.tblSFAs on S.SfaId equals F.SFAID
                      select new AssetDisposalModel
                      {
                          Attachment1 = S.Attachment1,
                          Attachment2 = S.Attachment2,
                          Attachment3 = S.Attachment3,
                          Attachment4 = S.Attachment4,
                          Attachment5 = S.Attachment5,
                          City = S.City,
                          Comments = S.Comments,
                          Country = C.CountryName,
                          CountryId = S.CountryId,
                          CraetedBy = S.CraetedBy,
                          CreatedDate = S.CreatedDate,
                          //Currency = CR.CurrencyName,
                          CurrencyId = S.CurrencyId,
                          PickUpAddress = S.PickUpAddress,
                          InvoiceAddress = S.InvoiceAddress,
                          CustomerName = S.CustomerName,
                          Description = S.Description,
                          ExpectedPricePoint = S.ExpectedPricePoint,
                          AssertDisposalId = S.AssertDisposalId,
                          Category = S.Category,
                          InvoiceContact = S.InvoiceContact,
                          PickUpContact = S.PickUpContact,
                          ProjectTimeline = S.ProjectTimeline,
                          Quantity = S.Quantity,
                          ResponedBy = S.ResponedBy,
                          ResponsedDate = S.ResponsedDate,
                          ResponsedId = S.ResponsedId,
                          AccountManagerName = S.AccountManagerName,
                          SfaId = S.SfaId,
                          LineItems = _dbCon.tblQuoteLineItems.Where(x => x.QuoteId == S.AssertDisposalId && x.QuoteTypeId == 3).Count()
                          //Supplier = F.SFAName,
                          //SupplierEmail = F.Email

                      }).ToList();

            return ad;
        }

        #endregion

    }
}

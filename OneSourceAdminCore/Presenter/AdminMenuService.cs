﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdminCore.Presenter
{
    public class AdminMenuService
    {
        private globalserveEntities dbCon;
        public AdminMenuService()
        {
            dbCon = new globalserveEntities();


        }

        public List<tblAdminMenu> GetMenuList()
        {
            var menus = dbCon.tblAdminMenus.ToList();
            return menus;
        }

        public tblAdminMenu GetMenuById(int id)
        {
            try
            {
                return dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuId == id);
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetMenuById");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetMenuById");
                }

                return new tblAdminMenu();
            }
        }

        public tblAdminMenu GetMenuByName(string name)
        {
            return dbCon.tblAdminMenus.FirstOrDefault(x => x.MenuName.ToLower().Trim().Replace(" ", "") == name);
        }

        public bool AddNewMenu(tblAdminMenu menu, AdminSetting currentSetting)
        {
            try
            {
                menu.LastUpdated = DateTime.Now;
                menu.UpdatedBy = currentSetting.UserID;
                menu.IsActive = true;
                dbCon.tblAdminMenus.Add(menu);
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddNewMenu");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddNewMenu");
                }
            }
            return false;
        }

        public bool EditMenu(tblAdminMenu menu, AdminSetting currentSetting)
        {
            try
            {
                menu.LastUpdated = DateTime.Now;
                menu.UpdatedBy = currentSetting.UserID;
                dbCon.Entry(menu).State = System.Data.Entity.EntityState.Modified;
                dbCon.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "EditMenu");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "EditMenu");
                }
            }
            return false;
        }


        public List<GenericList> GetUserGroupList()
        {
            var data = dbCon.tblUserGroups.Select(x => new GenericList()
            {

                Key = x.UserGroupID,
                Value = x.UserGroupName.Trim() + " (" + x.Description.Trim() + ")"
            }).ToList();

            return data;
        }

        public List<GenericList> GetUserGroupListByGroupName(string name)
        {
            var data = dbCon.tblUserGroups.Where(x => x.UserGroupName.ToLower() == name).Select(x => new GenericList()
            {

                Key = x.UserGroupID,
                Value = x.UserGroupName.Trim() + " (" + x.Description.Trim() + ")"
            }).ToList();

            return data;
        }

        public List<AdminRoleMenuMappingModel> GetRoleMenuMappingByRoleId(int roleId)
        {
            try
            {
                var getMenUlist = dbCon.tblAdminMenus.Where(x => x.IsActive && !x.IsMainMenu).ToList();

                var getExistingRoleMenuMapping = dbCon.tblMenuAndRoleMappings.Where(x => x.UserGroupId == roleId).ToList();


                var existingMenuIds = getExistingRoleMenuMapping.Select(x => x.MenuId).ToList();


                var menuList = new List<AdminRoleMenuMappingModel>();

                foreach (var item in getMenUlist.Where(x => !existingMenuIds.Contains(x.MenuId)))
                {
                    menuList.Add(new AdminRoleMenuMappingModel()
                    {
                        MenuId = item.MenuId,
                        MenuName = item.MenuName != null ? item.MenuName.Trim() : "",
                        AddEditAccess = false,
                        ViewOnlyAccess = false,
                        IsDefaultMenu = false,
                        UserGroupId = roleId,
                        UserGroupName = GetUserGroupNameWithDescById(roleId),
                        ParentMenu = GetParentMenuByMenuId(item.MenuId)
                    });
                }

                foreach (var item in getExistingRoleMenuMapping)
                {
                    menuList.Add(new AdminRoleMenuMappingModel()
                    {
                        MenuId = item.MenuId,
                        MenuName = GetMenuById(item.MenuId) != null ? GetMenuById(item.MenuId).MenuName : "",
                        AddEditAccess = item.AddEditAccess,
                        ViewOnlyAccess = item.ViewOnlyAccess,
                        IsDefaultMenu = item.IsDefaultMenu,
                        UserGroupId = roleId,
                        UserGroupName = GetUserGroupNameWithDescById(roleId),
                        ParentMenu = GetParentMenuByMenuId(item.MenuId)
                    });
                }
                return menuList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRoleMenuMappingByRoleId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRoleMenuMappingByRoleId");
                }
                return null;
            }
        }


        public string GetUserGroupNameWithDescById(int userGroupId)
        {
            try
            {
                var firstOrDefault = dbCon.tblUserGroups.FirstOrDefault(x => x.UserGroupID == userGroupId);
                var name = "";
                var desc = "";
                if (firstOrDefault != null)
                {
                    name = firstOrDefault.UserGroupName ?? "";
                    desc = firstOrDefault.Description ?? "";
                }

                return name + " (" + desc + ")";
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetRoleMenuMappingByRoleId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetRoleMenuMappingByRoleId");
                }

                return "-";
            }
        }

        public string GetParentMenuByMenuId(int menuId)
        {
            try
            {

                var menu = GetMenuById(menuId);
                if (menu != null && menu.IsActive)
                {
                    if (menu.IsActive)
                    {
                        if (!menu.IsMainMenu && menu.ParentMenuId != null && menu.ParentMenuId > 0)
                        {
                            return GetMenuById(Convert.ToInt32(menu.ParentMenuId)).MenuName ?? "-";
                        }
                    }
                }
                return "-";
            }

            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetParentMenuByMenuId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetParentMenuByMenuId");
                }

                return "-";
            }
        }

        public bool AddUpdateRoleMenuSettings(int menuId, int roleId, string accessType, bool accessValue, int currentUserId)
        {
            try
            {

                var chkExisting =
                    dbCon.tblMenuAndRoleMappings.FirstOrDefault(x => x.MenuId == menuId && x.UserGroupId == roleId);
                if (chkExisting != null)
                {
                    chkExisting.LastUpdated = DateTime.Now;
                    chkExisting.LastUpdatedBy = currentUserId;
                    if (accessType == "viewAccessChk")
                    {
                        chkExisting.ViewOnlyAccess = accessValue;
                        if (!accessValue)
                        {
                            chkExisting.AddEditAccess = false;
                        }
                    }
                    if (accessType == "AddEditAccess")
                    {
                        chkExisting.AddEditAccess = accessValue;
                        if (accessValue)
                        {
                            chkExisting.ViewOnlyAccess = true;
                        }
                    }
                    dbCon.Entry(chkExisting).State = System.Data.Entity.EntityState.Modified;
                    dbCon.SaveChanges();
                }
                else
                {

                    var addNewRoleMenu = new tblMenuAndRoleMapping
                    {
                        LastUpdated = DateTime.Now,
                        LastUpdatedBy = currentUserId,
                        MenuId = menuId,
                        UserGroupId = roleId
                    };
                    if (accessType == "viewAccessChk")
                    {
                        addNewRoleMenu.ViewOnlyAccess = accessValue;
                    }
                    if (accessType == "AddEditAccess")
                    {
                        addNewRoleMenu.AddEditAccess = accessValue;
                        if (accessValue)
                        {
                            addNewRoleMenu.ViewOnlyAccess = true;
                        }
                    }

                    dbCon.tblMenuAndRoleMappings.Add(addNewRoleMenu);
                    dbCon.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddUpdateRoleMenuSettings");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddUpdateRoleMenuSettings");
                }
                return false;
            }
        }


        public bool AddDeleteAllMenurightsByRoleId(List<CheckAllMenuRights> data, AdminSetting currentSettings)
        {
            try
            {
                foreach (var item in data)
                {
                    if (item.SelectAll)
                    {
                        AddUpdateRoleMenuSettings(item.MenuId, item.RoleId, "viewAccessChk", true, currentSettings.UserID);
                        AddUpdateRoleMenuSettings(item.MenuId, item.RoleId, "AddEditAccess", true, currentSettings.UserID);
                    }
                    else
                    {
                        AddUpdateRoleMenuSettings(item.MenuId, item.RoleId, "viewAccessChk", false, currentSettings.UserID);
                        AddUpdateRoleMenuSettings(item.MenuId, item.RoleId, "AddEditAccess", false, currentSettings.UserID);
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "AddDeleteAllMenurightsByRoleId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "AddDeleteAllMenurightsByRoleId");
                }
                return false;
            }

        }



        public List<GenericStringList> GetGroupTypes()
        {
            var typeList = dbCon.tblUserGroups.Where(x => x.UserGroupName.ToLower() != "customer" && x.UserGroupName.ToLower() != "finance").Select(x => new GenericStringList()
            {
                Key = x.UserGroupName.ToLower(),
                Value = x.UserGroupName.Trim()

            }).Distinct().ToList();
            return typeList;
        }

        public List<GenericList> GetEntityTypeListByTypeName(string typeName)
        {
            var list = new List<GenericList>();
            if (typeName.ToLower() == "sfa")
            {

                list = dbCon.tblSFAs.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.SFAID,
                    Value = x.SFAName.Trim()
                }).ToList();
                return list;
            }

            if (typeName.ToLower() == "manufacturer")
            {

                list = dbCon.tblManufacturers.Where(x => x.IsActive).Select(x => new GenericList()
                {
                    Key = x.ManufacturerID,
                    Value = x.ManufacturerName.Trim()
                }).ToList();
                return list;
            }

            if (typeName.ToLower() == "isd")
            {

                list = dbCon.tblPrimeContractors.Where(x => x.IsActive == 1).Select(x => new GenericList()
                {
                    Key = x.PrimeContractorID,
                    Value = x.PrimeContractorName.Trim()
                }).ToList();
                return list;
            }

            if (typeName.ToLower() == "subisd")
            {

                list =
                    dbCon.tblPrimeContractors.Where(x => x.IsActive == 1 && x.IsSubAdmin).Select(x => new GenericList()
                    {
                        Key = x.PrimeContractorID,
                        Value = x.PrimeContractorName.Trim()
                    }).ToList();
                return list;
            }


            return list;

        }

        public List<GenericList> GetUsersByRoleId(int grpId)
        {
            var data = dbCon.tblUsers.Where(x => x.IsActive == 1 && x.UserGroupID == grpId).Select(x => new GenericList()
            {
                Key = x.UserID,
                Value = x.UserName.Trim()

            }).ToList();

            return data;
        }


        public List<AdminRoleMenuMappingModel> GetMenuForUserrestrictionByRoleId(int roleId, int userId)
        {
            try
            {
                var getExistingRoleMenuMapping = dbCon.tblMenuAndRoleMappings.Where(x => x.UserGroupId == roleId && (x.AddEditAccess || x.ViewOnlyAccess)).ToList();

                var getRestrictedMenuIdsByUser =
                    dbCon.tblAdminRestrictedMenuByUsers.Where(
                        x => x.UserId == userId && x.IsRestrictedMenu && x.UserGroupId == roleId).Select(x => x.MenuId).ToList();

                var menuList = new List<AdminRoleMenuMappingModel>();

                foreach (var item in getExistingRoleMenuMapping)
                {
                    menuList.Add(new AdminRoleMenuMappingModel()
                    {
                        MenuId = item.MenuId,
                        MenuName = GetMenuById(item.MenuId).MenuName,
                        ViewOnlyAccess = (!getRestrictedMenuIdsByUser.Contains(item.MenuId)),
                        UserGroupId = roleId,
                        UserGroupName = GetUserGroupNameWithDescById(roleId),
                        ParentMenu = GetParentMenuByMenuId(item.MenuId)
                    });
                }
                return menuList;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetMenuForUserrestrictionByRoleId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetMenuForUserrestrictionByRoleId");
                }
                return null;
            }
        }

        public bool RestrictAdminMenuByUser(int menuId, int roleId, int userId, bool accessValue, string userType, int entityTypeId, int currentUserId)
        {
            try
            {
                if (accessValue)
                {

                    var chkExisting =
                        dbCon.tblAdminRestrictedMenuByUsers.FirstOrDefault(
                            x =>
                                x.MenuId == menuId && x.UserGroupId == roleId && x.UserId == userId &&
                                x.IsRestrictedMenu);
                    if (chkExisting != null)
                    {
                        dbCon.tblAdminRestrictedMenuByUsers.Remove(chkExisting);
                        dbCon.SaveChanges();
                        return true;
                    }
                }
                else
                {
                    var addNewRoleMenu = new tblAdminRestrictedMenuByUser
                    {
                        UpdateDate = DateTime.Now,
                        UpdatedBy = currentUserId,
                        MenuId = menuId,
                        UserGroupId = roleId,
                        UserId = userId,
                        IsRestrictedMenu = true,
                        UserTypeName = userType.Trim(),
                        UserTypeId = entityTypeId
                    };

                    dbCon.tblAdminRestrictedMenuByUsers.Add(addNewRoleMenu);
                    dbCon.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "RestrictAdminMenuByUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "RestrictAdminMenuByUser");
                }
                return false;
            }
        }

        public bool RestrictAllAdminMenuByUser(List<CheckAllUserRestrictions> items, int currentUserId)
        {
            try
            {
                foreach (var item in items)
                {
                    RestrictAdminMenuByUser(item.MenuId, item.RoleId, item.UserId, item.Accessiable, item.UserType, item.EntityTypeId, currentUserId);
                }
                return true;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "RestrictAllAdminMenuByUser");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "RestrictAllAdminMenuByUser");
                }
                return false;
            }
        }



        public AdminMenuAndAccessModel GetMenuByUserGroupIdAndUserId(int userGroupId, int userId)
        {
            try
            {
                var model = new AdminMenuAndAccessModel();
                var menuIdsOfGroup =
                    dbCon.tblMenuAndRoleMappings.Where(
                        x => x.UserGroupId == userGroupId && (x.AddEditAccess || x.ViewOnlyAccess)).ToList();

                var menuIds = menuIdsOfGroup.Select(x => x.MenuId);

                var menus = dbCon.tblAdminMenus.Where(x => menuIds.Contains(x.MenuId)).ToList();

                var parentMenuids = menus.Select(x => x.ParentMenuId).ToList();

                var parentmenuByIds = dbCon.tblAdminMenus.Where(x => parentMenuids.Contains(x.MenuId));

                menus.AddRange(parentmenuByIds);

                var checkRestrictedMenu =
                    dbCon.tblAdminRestrictedMenuByUsers.Where(
                        x => x.UserId == userId && x.IsRestrictedMenu && x.UserGroupId == userGroupId).ToList();


                if (checkRestrictedMenu != null && checkRestrictedMenu.Count > 0)
                {
                    var selectRestrictedId = checkRestrictedMenu.Select(x => x.MenuId);
                    menus = menus.Where(x => !selectRestrictedId.Contains(x.MenuId)).ToList();
                }

                //Added Distinct() keyword here to resolve multi tier menu duplication issue.
                model.AdminMenu = menus.Distinct().ToList();
                model.UserAccessCheck = menuIdsOfGroup.Select(x => new UserAccessCheckModel()
                {
                    MenuId = x.MenuId,
                    DataToken = (menus.FirstOrDefault(c => c.MenuId == x.MenuId) == null ? "" : menus.FirstOrDefault(c => c.MenuId == x.MenuId).DataTokenName),
                    AddEditAccess = x.AddEditAccess,
                    ViewAccess = x.ViewOnlyAccess
                }).ToList();

                var selectFinalMenuIds = menus.Select(x => x.MenuId).ToList();
                model.UserAccessCheck = model.UserAccessCheck.Where(x => selectFinalMenuIds.Contains(x.MenuId)).ToList();

                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetMenuByUserGroupIdAndUserId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetMenuByUserGroupIdAndUserId");
                }
                return new AdminMenuAndAccessModel();
            }
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 19 Feb 2018
        /// Created this new function rather than modifying GetMenuByUserGroupIdAndUserId
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <param name="userId"></param>
        /// <param name="parentId"></param>
        /// <returns>List of sub menu items for a particular parent</returns>
        public AdminMenuAndAccessModel GetSubMenuByUserGroupIdAndUserIdAndParentId(int userGroupId, int userId, int parentId)
        {
            try
            {
                var model = new AdminMenuAndAccessModel();
                var menuIdsOfGroup =
                    dbCon.tblMenuAndRoleMappings.Where(
                        x => x.UserGroupId == userGroupId && (x.AddEditAccess || x.ViewOnlyAccess)).ToList();

                var menuIds = menuIdsOfGroup.Select(x => x.MenuId);

                var menus = dbCon.tblAdminMenus.Where(x => menuIds.Contains(x.MenuId)).ToList();

                var parentMenuids = menus.Select(x => x.ParentMenuId).ToList();

                var parentmenuByIds = dbCon.tblAdminMenus.Where(x => parentMenuids.Contains(x.MenuId));

                menus.AddRange(parentmenuByIds);

                var checkRestrictedMenu =
                    dbCon.tblAdminRestrictedMenuByUsers.Where(
                        x => x.UserId == userId && x.IsRestrictedMenu && x.UserGroupId == userGroupId).ToList();


                if (checkRestrictedMenu != null && checkRestrictedMenu.Count > 0)
                {
                    var selectRestrictedId = checkRestrictedMenu.Select(x => x.MenuId);
                    menus = menus.Where(x => !selectRestrictedId.Contains(x.MenuId)).ToList();
                }
                model.AdminMenu = menus.Where(x => x.ParentMenuId == parentId && x.MenuType == "Dashboard Sub-Menu Item").Distinct().ToList();

                model.UserAccessCheck = menuIdsOfGroup.Select(x => new UserAccessCheckModel()
                {
                    MenuId = x.MenuId,
                    DataToken = (menus.FirstOrDefault(c => c.MenuId == x.MenuId) == null ? "" : menus.FirstOrDefault(c => c.MenuId == x.MenuId).DataTokenName),
                    AddEditAccess = x.AddEditAccess,
                    ViewAccess = x.ViewOnlyAccess
                }).ToList();

                var selectFinalMenuIds = menus.Select(x => x.MenuId).ToList();
                model.UserAccessCheck = model.UserAccessCheck.Where(x => selectFinalMenuIds.Contains(x.MenuId)).ToList();

                return model;
            }
            catch (Exception ex)
            {
                StaticHelper.LogException(ex.ToString(), "GetSubMenuByUserGroupIdAndUserIdAndParentId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetSubMenuByUserGroupIdAndUserIdAndParentId");
                }
                return new AdminMenuAndAccessModel();
            }
        }



    }
}

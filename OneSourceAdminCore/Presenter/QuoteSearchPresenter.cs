﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;

namespace OneSourceAdminCore.Presenter
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Presenter Class is responsible for providing quote tracking detail"
    /// </summary>
    public class QuoteSearchPresenter
    {
        private readonly globalserveEntities _dbCon;
        public QuoteSearchPresenter()
        {
            _dbCon = new globalserveEntities();
        }

        /// <summary>
        /// this method is responsible for providing model to search quote
        /// </summary>
        /// <param name="userGroupName"></param>
        /// <param name="customerId"></param>
        /// <param name="PrimeContractorID"></param>
        /// <param name="quoteType"></param>
        /// <param name="SFAID"></param>
        /// <returns></returns>
        public QuoteSearchModel GetQuoteSearchModel(string userGroupName, int customerId, string PrimeContractorID, string quoteType, string SFAID)
        {
            try
            {
                QuoteSearchModel obj = new Model.QuoteSearchModel();

                obj.Customers = _dbCon.sp_getCustomerQuoteSearch_AdminR(userGroupName, SFAID, PrimeContractorID, quoteType).ToList();
                obj.Customers.Insert(0, new Entity.sp_getCustomerQuoteSearch_AdminR_Result { customerid = 0, customername = "(None)" });

                obj.CategoryResponses = _dbCon.sp_getCategoryResponseQuoteSearch_AdminR(userGroupName, customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID, quoteType).OrderBy(x => x.partcategoryname).ToList();
                obj.CategoryResponses.Insert(0, new Entity.sp_getCategoryResponseQuoteSearch_AdminR_Result { partcategoryid = 0, partcategoryname = "(None)" });

                obj.Countries = _dbCon.sp_getCountryQuoteSearch_AdminR(userGroupName, customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID, quoteType).ToList();
                obj.Countries.Insert(0, new Entity.sp_getCountryQuoteSearch_AdminR_Result { countryid = 0, countryname = "(None)" });

                obj.ManufacturerResponses = _dbCon.sp_getManufacturerResponseQuoteSearch_AdminR(userGroupName, customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID, quoteType, PrimeContractorID).ToList();
                obj.ManufacturerResponses.Insert(0, new Entity.sp_getManufacturerResponseQuoteSearch_AdminR_Result { manufacturerid = 0, manufacturername = "(None)" });

                obj.RequestedBy = _dbCon.sp_getRequestedByQuoteSearch_AdminR(userGroupName, customerId == 0 ? string.Empty : Convert.ToString(customerId), SFAID, quoteType, PrimeContractorID).ToList();
                obj.RequestedBy.Insert(0, new Entity.sp_getRequestedByQuoteSearch_AdminR_Result { contactid = 0, contactname = "(None)" });

                if (quoteType == "new")
                {
                    obj.QuoteStatus = _dbCon.tblQuoteStatus.Select(x => new GenericList
                    {
                        Key = x.QuoteStatusID,
                        Value = x.QuoteStatus
                    }).OrderBy(x => x.Key).ToList();
                }
                else
                {
                    var statuses = _dbCon.tblPartQuoteStatus.Select(x => new GenericList
                    {
                        Key = x.PartQuoteStatusID,
                        Value = x.PartQuoteStatus
                    }).ToList();
                    obj.QuoteStatus = new List<Model.GenericList>();
                    obj.QuoteStatus.Insert(0, statuses.FirstOrDefault(x => x.Value == "New"));
                    obj.QuoteStatus.Insert(1, statuses.FirstOrDefault(x => x.Value == "In Progress"));
                    obj.QuoteStatus.Insert(2, statuses.FirstOrDefault(x => x.Value == "Complete"));
                    obj.QuoteStatus.Insert(3, statuses.FirstOrDefault(x => x.Value == "Cancelled"));


                }

                if (userGroupName == "ISD")
                {
                    obj.Suppliers = _dbCon.sp_getSupplierQuoteSearch_AdminR(userGroupName, customerId == 0 ? string.Empty : Convert.ToString(customerId), PrimeContractorID, quoteType).ToList();
                }
                obj.quoteType = quoteType;
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteSearchModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteSearchModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteSearchModel");
                }
                return new QuoteSearchModel();
            }
        }

        /// <summary>
        /// this method use for searched quotes
        /// </summary>
        /// <param name="model"></param>
        /// <param name="dateFormatId"></param>
        /// <param name="primeContractorId"></param>
        /// <param name="userGroupName"></param>
        /// <param name="sfaid"></param>
        /// <param name="requiredRecord"></param>
        /// <returns></returns>
        public QuoteListModel GetSearchedQuotes(QuoteSearchModel model, string dateFormatId, string primeContractorId, string userGroupName, string sfaid, int requiredRecord)
        {
            try
            {
                _dbCon.Database.CommandTimeout = 5000;
                QuoteListModel oslmodel = new Model.QuoteListModel();
                ObjectParameter totalCount = new ObjectParameter("TotalCount", typeof(Int32));

                List<sp_getQuoteSearch_AdminR_Result> obj = _dbCon.sp_getQuoteSearch_AdminR(Convert.ToInt32(dateFormatId), primeContractorId,
                    model.quoteNumber, model.quoteReference, model.customerid == 0 ? "" : model.customerid.ToString(), model.contactid == 0 ? "" : model.contactid.ToString(),
                    model.StrStatus, model.StrSuppliers, model.countryid == 0 ? "" : model.countryid.ToString(), model.manufacturerRequest,
                    model.manufacturerid == 0 ? "" : model.manufacturerid.ToString(), model.descriptionRequest, model.skuResponse, model.descriptionResponse,
                    model.partcategoryid == 0 ? "" : model.partcategoryid.ToString(),
                    model.requestedAfterDate, model.requestedBeforeDate, model.quoteType, userGroupName, sfaid, requiredRecord.ToString(), totalCount).ToList();

                oslmodel.LstQuotes = obj;
                oslmodel.quoteType = model.quoteType;
                if (obj.Count == 0)
                {
                    oslmodel.TotalRecord = 0;
                }

                else { oslmodel.TotalRecord = Convert.ToInt32(totalCount.Value); }
                return oslmodel;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetSearchedQuotes<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetSearchedQuotes");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetSearchedQuotes");
                }
                return new QuoteListModel();
            }
        }

        /// <summary>
        /// this method use for quote detail
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <param name="userGroupName"></param>
        /// <param name="SFAID"></param>
        /// <returns></returns>
        public QuoteDetailModel GetQuoteDetail(int quoteId, string quoteType, string userGroupName, int SFAID)
        {
            try
            {
                QuoteDetailModel obj = new Model.QuoteDetailModel();
                obj.QuoteType = quoteType;
                obj.QuoteId = quoteId;
                if (quoteType == "new")
                {
                    obj.NewQuoteDetail = _dbCon.sp_getNewQuoteDetail_AdminR(quoteId.ToString(), userGroupName, SFAID.ToString()).FirstOrDefault();

                    if (obj.NewQuoteDetail == null)
                    {
                        return null;
                    }

                    obj.CountryId = obj.NewQuoteDetail.countryid;
                    obj.CustomerId = obj.NewQuoteDetail.customerid;
                    obj.CurrencyId = obj.NewQuoteDetail.currencyId;

                    var refs = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                    if (refs != null)
                    {
                        obj.QuoteRef = refs.CustomerReference;
                        obj.SupplierRef = refs.SupplierReference;
                    }
                    if (obj.NewQuoteDetail.customerid > 0 && obj.NewQuoteDetail.countryid > 0)
                    {
                        obj.SFAs = _dbCon.sp_getSFAsByCustomerCountry_AdminR(obj.NewQuoteDetail.customerid, obj.NewQuoteDetail.countryid).ToList();
                    }

                    var totalpriceQuery = @"SELECT SUM((qr.Price  ) * qr.Quantity) as price
	                                        FROM tblQuoteResponses AS qr
	                                        INNER JOIN tblQuoteSummary AS qs ON qs.QuoteID = qr.QuoteID
	                                        INNER JOIN tblCustomers AS cust ON cust.CustomerID = qs.CustomerID
	                                        INNER JOIN tblSFAStandardUplifts AS sfaup ON sfaup.CustomerID = qs.CustomerID AND sfaup.CountryID = qs.CountryID AND sfaup.SFAID = qs.SFAID
	                                        WHERE qr.IsActive = 1 And qr.QuoteID =" + quoteId + @"
	                                        GROUP BY qs.QuoteID";
                    var totalprice = _dbCon.Database.SqlQuery<decimal?>(totalpriceQuery).FirstOrDefault();

                    if (totalprice != null)
                    {
                        obj.TotalPrice = totalprice.Value;
                    }
                    // obj.TotalPrice = _dbCon.sp_getNewQuoteTotalPrice_R(quoteId).FirstOrDefault();


                }
                else if (quoteType == "old")
                {
                    obj.OldQuoteDetail = _dbCon.sp_getOldQuoteDetail_AdminR(quoteId.ToString(), userGroupName, SFAID.ToString()).FirstOrDefault();
                    if (obj.OldQuoteDetail == null)
                    {
                        return null;
                    }

                    if (!string.IsNullOrEmpty(obj.OldQuoteDetail.currencysymbol))
                    {
                        var curr = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencySymbol == obj.OldQuoteDetail.currencysymbol);
                        if (curr != null)
                        {
                            obj.Symbol = curr.Symbol;
                        }
                    }


                    if (obj.OldQuoteDetail.customerid > 0 && obj.OldQuoteDetail.countryid > 0)
                    {
                        obj.SFAs = _dbCon.sp_getSFAsByCustomerCountry_AdminR(obj.OldQuoteDetail.customerid, obj.OldQuoteDetail.countryid).ToList();
                    }

                    var totalpriceQuery = @"SELECT SUM((qr.Price  ) * qr.Quantity) as price
                                            FROM tblPartQuoteResponses AS qr
                                            INNER JOIN tblPartQuotes AS qs ON qs.PartQuoteID = qr.PartQuoteID
                                            INNER JOIN tblCustomers AS cust ON cust.CustomerID = qs.CustomerID
                                            INNER JOIN tblSFAStandardUplifts AS sfaup ON sfaup.CustomerID = qs.CustomerID AND sfaup.CountryID = qs.CountryID AND sfaup.SFAID = qs.SFAID
                                            WHERE qr.IsActive = 1 And qr.PartQuoteID =" + quoteId + @"
                                            GROUP BY qs.PartQuoteID";
                    var totalprice = _dbCon.Database.SqlQuery<decimal?>(totalpriceQuery).FirstOrDefault();
                    if (totalprice != null)
                    {
                        obj.TotalPrice = totalprice.Value;
                    }

                    obj.CountryId = obj.OldQuoteDetail.countryid;
                    obj.CustomerId = obj.OldQuoteDetail.customerid;
                    obj.CurrencyId = obj.OldQuoteDetail.currencyid;
                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteDetail");
                }
                return new QuoteDetailModel();
            }
        }

        public QuoteDetailModel GetQuoteDetailAsCustomer(int quoteId, string quoteType, string userGroupName, int SFAID)
        {
            try
            {
                QuoteDetailModel obj = new Model.QuoteDetailModel();
                obj.QuoteType = quoteType;
                obj.QuoteId = quoteId;
                if (quoteType == "new")
                {
                    obj.NewQuoteDetail = _dbCon.sp_getNewQuoteDetail_AdminR(quoteId.ToString(), userGroupName, SFAID.ToString()).FirstOrDefault();

                    if (obj.NewQuoteDetail == null)
                    {
                        return null;
                    }

                    obj.CountryId = obj.NewQuoteDetail.countryid;
                    obj.CustomerId = obj.NewQuoteDetail.customerid;
                    obj.CurrencyId = obj.NewQuoteDetail.currencyId;

                    var refs = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                    if (refs != null)
                    {
                        obj.QuoteRef = refs.CustomerReference;
                        obj.SupplierRef = refs.SupplierReference;
                    }
                    if (obj.NewQuoteDetail.customerid > 0 && obj.NewQuoteDetail.countryid > 0)
                    {
                        obj.SFAs = _dbCon.sp_getSFAsByCustomerCountry_AdminR(obj.NewQuoteDetail.customerid, obj.NewQuoteDetail.countryid).ToList();
                    }

                    var totalpriceQuery = @"SELECT SUM((qr.Price  *(1 + IsNull(sfaup.SFAStandardUplift, 0) + IsNull(Coalesce(sfaup.PrimeContractorUplift, cust.PrimeContractorUplift), 0))) * qr.Quantity) as price
	                                        FROM tblQuoteResponses AS qr
	                                        INNER JOIN tblQuoteSummary AS qs ON qs.QuoteID = qr.QuoteID
	                                        INNER JOIN tblCustomers AS cust ON cust.CustomerID = qs.CustomerID
	                                        INNER JOIN tblSFAStandardUplifts AS sfaup ON sfaup.CustomerID = qs.CustomerID AND sfaup.CountryID = qs.CountryID AND sfaup.SFAID = qs.SFAID
	                                        WHERE qr.IsActive = 1 And qr.QuoteID =" + quoteId + @"
	                                        GROUP BY qs.QuoteID";
                    var totalprice = _dbCon.Database.SqlQuery<decimal?>(totalpriceQuery).FirstOrDefault();

                    if (totalprice != null)
                    {
                        obj.TotalPrice = totalprice.Value;
                    }
                    // obj.TotalPrice = _dbCon.sp_getNewQuoteTotalPrice_R(quoteId).FirstOrDefault();


                }
                else if (quoteType == "old")
                {
                    obj.OldQuoteDetail = _dbCon.sp_getOldQuoteDetail_AdminR(quoteId.ToString(), userGroupName, SFAID.ToString()).FirstOrDefault();
                    if (obj.OldQuoteDetail == null)
                    {
                        return null;
                    }

                    if (!string.IsNullOrEmpty(obj.OldQuoteDetail.currencysymbol))
                    {
                        var curr = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencySymbol == obj.OldQuoteDetail.currencysymbol);
                        if (curr != null)
                        {
                            obj.Symbol = curr.Symbol;
                        }
                    }


                    if (obj.OldQuoteDetail.customerid > 0 && obj.OldQuoteDetail.countryid > 0)
                    {
                        obj.SFAs = _dbCon.sp_getSFAsByCustomerCountry_AdminR(obj.OldQuoteDetail.customerid, obj.OldQuoteDetail.countryid).ToList();
                    }

                    var totalpriceQuery = @"SELECT SUM((qr.Price  ) * qr.Quantity) as price
                                            FROM tblPartQuoteResponses AS qr
                                            INNER JOIN tblPartQuotes AS qs ON qs.PartQuoteID = qr.PartQuoteID
                                            INNER JOIN tblCustomers AS cust ON cust.CustomerID = qs.CustomerID
                                            INNER JOIN tblSFAStandardUplifts AS sfaup ON sfaup.CustomerID = qs.CustomerID AND sfaup.CountryID = qs.CountryID AND sfaup.SFAID = qs.SFAID
                                            WHERE qr.IsActive = 1 And qr.PartQuoteID =" + quoteId + @"
                                            GROUP BY qs.PartQuoteID";
                    var totalprice = _dbCon.Database.SqlQuery<decimal?>(totalpriceQuery).FirstOrDefault();
                    if (totalprice != null)
                    {
                        obj.TotalPrice = totalprice.Value;
                    }

                    obj.CountryId = obj.OldQuoteDetail.countryid;
                    obj.CustomerId = obj.OldQuoteDetail.customerid;
                    obj.CurrencyId = obj.OldQuoteDetail.currencyid;
                }

                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteDetail");
                }
                return new QuoteDetailModel();
            }
        }

        /// <summary>
        /// this method use for getting sfa
        /// </summary>
        /// <param name="sfaId"></param>
        /// <returns></returns>
        public string GetSFAById(int sfaId)
        {
            var sfa = _dbCon.tblSFAs.FirstOrDefault(x => x.SFAID == sfaId);
            if (sfa != null)
            {
                return sfa.SFAName;
            }
            return string.Empty;
        }

        /// <summary>
        /// this method use for assigning supplier
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sfaid"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int AssignSupplier(int quoteId, int sfaid, int sessionUserId, string quoteType)
        {
            try
            {
                int count = 0;
                int count1 = 0;
                if (quoteType == "new")
                {
                    var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                    if (quoteSummary != null)
                    {
                        quoteSummary.SFAID = sfaid;
                        quoteSummary.LastUpdated = DateTime.Now;
                        quoteSummary.LastUpdatedByUserID = sessionUserId;
                        quoteSummary.CreateDate = DateTime.Now;
                        _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }
                    count1 = QuoteAssignSupplierEmail(quoteId, sessionUserId, quoteType);
                }
                else
                {
                    var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                    if (partQuote != null)
                    {
                        partQuote.SFAID = sfaid;
                        partQuote.LastUpdated = DateTime.Now;
                        partQuote.LastUpdatedByUserID = sessionUserId;
                        partQuote.CreateDate = DateTime.Now;
                        _dbCon.Entry(partQuote).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();
                    }
                    count1 = QuoteAssignSupplierEmail(quoteId, sessionUserId, quoteType);
                }
                return count1 == -1 ? -1 : count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>AssignSupplier<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>AssignSupplier");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>AssignSupplier");
                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for email for assigning supplier
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int QuoteAssignSupplierEmail(int quoteId, int sessionUserId, string quoteType)
        {

            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;



                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailcc = mailfrom;
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        mailto = quoteCustomerInfo.SupplierEmail;
                    }

                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine + Environment.NewLine;
                    mailbody = mailbody + "The following quote request comes from GlobalServe ONESOURCE" + Environment.NewLine + Environment.NewLine;

                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;

                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine;
                    mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to respond to the quote request." + Environment.NewLine;
                    mailbody = mailbody + "If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - New Quote Request - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailto))
                        {
                            mailto = mailto + item.Email;
                        }
                        else
                        {
                            if (!mailto.Contains(item.Email))
                            {
                                mailto = mailto + "; " + item.Email;
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(mailto.Trim()))
                    {
                        mailto = MissingEmail;
                    }


                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, "", mailbody, mailsubject);

                }
                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>QuoteAssignSupplierEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>QuoteAssignSupplierEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>QuoteAssignSupplierEmail");
                }
                return -1;
            }

        }

        /// <summary>
        /// this method use for changing quote status
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="status"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int ChangeQuoteStatus(int quoteId, string status, int sessionUserId, string quoteType)
        {
            try
            {
                int count = 0;
                int count1 = 0;
                if (status == "In Progress" && quoteType == "new")
                {
                    var quoteStatus = _dbCon.tblQuoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "P");
                    if (quoteStatus != null)
                    {
                        var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                        if (quoteSummary != null)
                        {
                            quoteSummary.QuoteStatusID = quoteStatus.QuoteStatusID;
                            quoteSummary.LastUpdated = DateTime.Now;
                            quoteSummary.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                            count1 = QuoteInProgressEmail(quoteId, sessionUserId, quoteType, quoteStatus.QuoteStatus);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (status == "Complete" && quoteType == "new")
                {
                    var quoteStatus = _dbCon.tblQuoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "C");
                    if (quoteStatus != null)
                    {
                        var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                        if (quoteSummary != null)
                        {
                            quoteSummary.QuoteStatusID = quoteStatus.QuoteStatusID;
                            quoteSummary.LastUpdated = DateTime.Now;
                            quoteSummary.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                            count1 = QuoteCompleteEmail(quoteId, sessionUserId, quoteType, quoteStatus.QuoteStatus);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (status == "In Progress" && quoteType == "old")
                {
                    var quoteStatus = _dbCon.tblPartQuoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "P");
                    if (quoteStatus != null)
                    {
                        var partQuotes = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                        if (partQuotes != null)
                        {
                            partQuotes.PartQuoteStatusID = quoteStatus.PartQuoteStatusID;
                            partQuotes.LastUpdated = DateTime.Now;
                            partQuotes.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(partQuotes).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                            count1 = QuoteInProgressEmail(quoteId, sessionUserId, quoteType, quoteStatus.PartQuoteStatus);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (status == "Complete" && quoteType == "old")
                {
                    var quoteStatus = _dbCon.tblPartQuoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "C");
                    if (quoteStatus != null)
                    {
                        var partQuotes = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                        if (partQuotes != null)
                        {
                            partQuotes.PartQuoteStatusID = quoteStatus.PartQuoteStatusID;
                            partQuotes.LastUpdated = DateTime.Now;
                            partQuotes.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(partQuotes).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                            count1 = QuoteCompleteEmail(quoteId, sessionUserId, quoteType, quoteStatus.PartQuoteStatus);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (status == "Cancelled" && quoteType == "new")
                {
                    var quoteStatus = _dbCon.tblQuoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "X");
                    if (quoteStatus != null)
                    {

                        var quoteResponses = _dbCon.tblQuoteResponses.FirstOrDefault(x => x.QuoteID == quoteId);
                        if (quoteResponses != null)
                        {
                            quoteResponses.LastUpdated = DateTime.Now;
                            quoteResponses.LastUpdatedByUserID = sessionUserId;
                            quoteResponses.IsActive = false;
                            _dbCon.Entry(quoteResponses).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }

                        var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                        if (quoteSummary != null)
                        {
                            quoteSummary.QuoteStatusID = quoteStatus.QuoteStatusID;
                            quoteSummary.LastUpdated = DateTime.Now;
                            quoteSummary.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                            count1 = QuoteCancelEmail(quoteId, sessionUserId, quoteType, quoteStatus.QuoteStatus);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (status == "Cancelled" && quoteType == "old")
                {
                    var quoteStatus = _dbCon.tblPartQuoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "X");
                    if (quoteStatus != null)
                    {

                        var quoteResponses = _dbCon.tblPartQuoteResponses.Where(x => x.PartQuoteID == quoteId).Select(x => x.CustomerPartID).ToList();
                        var customerCountryParts = _dbCon.tblCustomerCountryParts.Where(x => quoteResponses.Contains(x.CustomerPartID));

                        foreach (var customerCountryPart in customerCountryParts)
                        {
                            customerCountryPart.IsActive = false;
                            customerCountryPart.LastUpdated = DateTime.Now;
                            customerCountryPart.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(customerCountryPart).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }

                        var partquoteSummary = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                        if (partquoteSummary != null)
                        {
                            partquoteSummary.PartQuoteStatusID = quoteStatus.PartQuoteStatusID;
                            partquoteSummary.LastUpdated = DateTime.Now;
                            partquoteSummary.LastUpdatedByUserID = sessionUserId;
                            _dbCon.Entry(partquoteSummary).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();

                            count1 = QuoteCancelEmail(quoteId, sessionUserId, quoteType, quoteStatus.PartQuoteStatus);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                return count1 == -1 ? -1 : count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>ChangeQuoteStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>ChangeQuoteStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>ChangeQuoteStatus");
                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for email for quote inprogress
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <param name="quoteStatus"></param>
        /// <returns></returns>
        public int QuoteInProgressEmail(int quoteId, int sessionUserId, string quoteType, string quoteStatus)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string userName = string.Empty;


                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailbcc = mailfrom;

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.UserEmail))
                    {
                        mailto = quoteCustomerInfo.UserEmail;
                    }
                    else
                    {
                        mailto = string.Empty;
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = mailto + quoteCustomerInfo.NetpQuoteEmail;
                    }
                    else
                    {
                        if (!mailto.Contains(quoteCustomerInfo.NetpQuoteEmail ?? ""))
                        {
                            mailto = mailto + "; " + quoteCustomerInfo.NetpQuoteEmail;
                        }
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }


                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        mailcc = quoteCustomerInfo.SupplierEmail;
                    }

                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine + Environment.NewLine;
                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;
                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine;
                    mailbody = mailbody + "Status          : " + quoteStatus + Environment.NewLine;



                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == sessionUserId);
                    if (contact != null)
                    {
                        userName = (string.IsNullOrEmpty(contact.FirstName) ? "" : contact.FirstName) + " " + (string.IsNullOrEmpty(contact.LastName) ? "" : contact.LastName);
                        if (!string.IsNullOrEmpty(userName))
                        {
                            mailbody = mailbody + "Status Changed By : " + userName + Environment.NewLine;

                        }
                        if (string.IsNullOrEmpty(contact.Email))
                        {
                            mailcc = mailcc + contact.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(contact.Email))
                            {
                                mailcc = mailcc + "; " + contact.Email;
                            }
                        }
                    }

                    if (quoteCustomerInfo.CustomerID == 245)
                    {
                        mailbody = mailbody + "To view the quote request, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." + Environment.NewLine + Environment.NewLine;
                    }
                    else
                    {
                        mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to view the quote request." + Environment.NewLine;
                    }

                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Quote In Progress - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + item.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(item.Email))
                            {
                                mailcc = mailcc + "; " + item.Email;
                            }
                        }
                    }
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>QuoteInProgressEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>QuoteInProgressEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>QuoteInProgressEmail");
                }
                return -1;
            }

        }

        /// <summary>
        /// this method use for email for quote complete
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <param name="quoteStatus"></param>
        /// <returns></returns>
        public int QuoteCompleteEmail(int quoteId, int sessionUserId, string quoteType, string quoteStatus)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string userName = string.Empty;


                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailbcc = mailfrom;

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.UserEmail))
                    {
                        mailto = quoteCustomerInfo.UserEmail;
                    }
                    else
                    {
                        mailto = string.Empty;
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = mailto + quoteCustomerInfo.NetpQuoteEmail;
                    }
                    else
                    {
                        if (!mailto.Contains(quoteCustomerInfo.NetpQuoteEmail ?? ""))
                        {
                            mailto = mailto + "; " + quoteCustomerInfo.NetpQuoteEmail;
                        }
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }


                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        mailcc = quoteCustomerInfo.SupplierEmail;
                    }

                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine + Environment.NewLine;
                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;
                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine;
                    mailbody = mailbody + "Status          : " + quoteStatus + Environment.NewLine;



                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == sessionUserId);
                    if (contact != null)
                    {
                        userName = (string.IsNullOrEmpty(contact.FirstName) ? "" : contact.FirstName) + " " + (string.IsNullOrEmpty(contact.LastName) ? "" : contact.LastName);
                        if (!string.IsNullOrEmpty(userName))
                        {
                            mailbody = mailbody + "Status Changed By : " + userName + Environment.NewLine;

                        }
                        if (string.IsNullOrEmpty(contact.Email))
                        {
                            mailcc = mailcc + contact.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(contact.Email))
                            {
                                mailcc = mailcc + "; " + contact.Email;
                            }
                        }
                    }

                    if (quoteCustomerInfo.CustomerID == 245)
                    {
                        mailbody = mailbody + "To view the quote request, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." + Environment.NewLine + Environment.NewLine;
                    }
                    else
                    {
                        mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to view the quote request." + Environment.NewLine;
                    }

                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Quote Complete - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + item.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(item.Email))
                            {
                                mailcc = mailcc + "; " + item.Email;
                            }
                        }
                    }
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>QuoteCompleteEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>QuoteCompleteEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>QuoteCompleteEmail");
                }
                return -1;
            }

        }

        /// <summary>
        /// this method use for email for quote cancel
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <param name="quoteStatus"></param>
        /// <returns></returns>
        public int QuoteCancelEmail(int quoteId, int sessionUserId, string quoteType, string quoteStatus)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string userName = string.Empty;


                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailbcc = mailfrom;

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.UserEmail))
                    {
                        mailto = quoteCustomerInfo.UserEmail;
                    }
                    else
                    {
                        mailto = string.Empty;
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = mailto + quoteCustomerInfo.NetpQuoteEmail;
                    }
                    else
                    {
                        if (!mailto.Contains(quoteCustomerInfo.NetpQuoteEmail ?? ""))
                        {
                            mailto = mailto + "; " + quoteCustomerInfo.NetpQuoteEmail;
                        }
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }


                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        mailcc = quoteCustomerInfo.SupplierEmail;
                    }

                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine + Environment.NewLine;
                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;
                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine;
                    mailbody = mailbody + "Status          : " + quoteStatus + Environment.NewLine;



                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == sessionUserId);
                    if (contact != null)
                    {
                        userName = (string.IsNullOrEmpty(contact.FirstName) ? "" : contact.FirstName) + " " + (string.IsNullOrEmpty(contact.LastName) ? "" : contact.LastName);
                        if (!string.IsNullOrEmpty(userName))
                        {
                            mailbody = mailbody + "Cancelled By : " + userName + Environment.NewLine;

                        }
                        if (string.IsNullOrEmpty(contact.Email))
                        {
                            mailcc = mailcc + contact.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(contact.Email))
                            {
                                mailcc = mailcc + "; " + contact.Email;
                            }
                        }
                    }

                    if (quoteCustomerInfo.CustomerID == 245)
                    {
                        mailbody = mailbody + "To view the quote request, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." + Environment.NewLine + Environment.NewLine;
                    }
                    else
                    {
                        mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to view the quote request." + Environment.NewLine;
                    }

                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Quote Cancelled - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + item.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(item.Email))
                            {
                                mailcc = mailcc + "; " + item.Email;
                            }
                        }
                    }
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>QuoteCancelEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>QuoteCancelEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>QuoteCancelEmail");
                }
                return -1;
            }

        }

        /// <summary>
        /// this method use for updating quote detail
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteRef"></param>
        /// <param name="supplierRef"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int UpdateQuoteDetail(int quoteId, string quoteRef, string supplierRef, int sessionUserId, string quoteType)
        {
            try
            {
                int count = 0;
                var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                if (quoteSummary != null)
                {
                    quoteSummary.CustomerReference = quoteRef;
                    quoteSummary.SupplierReference = supplierRef;
                    quoteSummary.LastUpdated = DateTime.Now;
                    quoteSummary.LastUpdatedByUserID = sessionUserId;
                    _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();

                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>UpdateQuoteDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>UpdateQuoteDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>UpdateQuoteDetail");
                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for updating quote detail SFA
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="supplierRef"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int UpdateQuoteDetail_SFA(int quoteId, string supplierRef, int sessionUserId)
        {
            try
            {
                int count = 0;
                var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                if (quoteSummary != null)
                {
                    quoteSummary.SupplierReference = supplierRef;
                    quoteSummary.LastUpdated = DateTime.Now;
                    quoteSummary.LastUpdatedByUserID = sessionUserId;
                    _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                    count = _dbCon.SaveChanges();

                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>UpdateQuoteDetail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>UpdateQuoteDetail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>UpdateQuoteDetail");
                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for getting quote request response model
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <param name="dateTimeFormateID"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public QuoteRequestResponseModel GetQuoteRequestResponseModel(int quoteId, string quoteType, int dateTimeFormateID, string groupName, string sfaid)
        {
            try
            {
                QuoteRequestResponseModel obj = new Model.QuoteRequestResponseModel();
                if (quoteType == "new" && groupName == "ISD")
                {
                    obj.NewQuoteResponse = _dbCon.sp_getQuoteResponse_New_AdminR(quoteId.ToString(), dateTimeFormateID, true).ToList();
                    obj.QuoteRequests = _dbCon.tblQuoteRequests.Where(x => x.QuoteID == quoteId).ToList();

                    var quoteDetail = _dbCon.sp_getNewQuoteDetail_AdminR(quoteId.ToString(), groupName, sfaid).FirstOrDefault();
                    if (quoteDetail != null)
                    {
                        obj.QuoteStatusCode = quoteDetail.quotestatuscode;
                        obj.CustomerId = Convert.ToInt32(quoteDetail.customerid);
                        obj.CurrencySymbol = quoteDetail.currencysymbol;
                        obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(quoteDetail.customerid).ToList();


                    }
                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                    {
                        Key = x.ManufacturerID,
                        Value = x.ManufacturerName
                    }).OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                    obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });
                    //--getting bundle category id for new quotes (SUNNY GOHAR)
                    if (obj.QuoteRequests != null && obj.QuoteRequests.Count > 0)
                    {
                        var request = obj.QuoteRequests.Select(x => new QuoteRequestModel
                        {
                            Description = x.Description,
                            Manufacturer = x.Manufacturer,
                            SKU = x.SKU,
                            Quantity = x.Quantity,
                            CatalogStandardID = x.CatalogStandardID,
                            BundleCategoryId = GetPartCategoryId(Convert.ToInt32(x.CatalogStandardID)) // get bundle category id (SUNNY GOHAR)
                        }).ToList();

                        obj.QuoteRequestList = request;
                    }
                    //----
                }
                else if (quoteType == "new" && groupName == "SFA")
                {
                    obj.NewQuoteResponse_SFA = _dbCon.sp_getQuoteResponse_New_SFA_AdminR(quoteId.ToString(), dateTimeFormateID).ToList();
                    obj.QuoteRequests = _dbCon.tblQuoteRequests.Where(x => x.QuoteID == quoteId).ToList();

                    var quoteDetail = _dbCon.sp_getNewQuoteDetail_AdminR(quoteId.ToString(), groupName, sfaid).FirstOrDefault();
                    if (quoteDetail != null)
                    {
                        obj.QuoteStatusCode = quoteDetail.quotestatuscode;
                        obj.CustomerId = Convert.ToInt32(quoteDetail.customerid);
                        obj.CurrencySymbol = quoteDetail.currencysymbol;
                        obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(quoteDetail.customerid).ToList();


                    }
                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                    {
                        Key = x.ManufacturerID,
                        Value = x.ManufacturerName
                    }).OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                    obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });
                    //--getting bundle category id for new quotes (SUNNY GOHAR)
                    if (obj.QuoteRequests != null && obj.QuoteRequests.Count > 0)
                    {
                        var request = obj.QuoteRequests.Select(x => new QuoteRequestModel
                        {
                            Description = x.Description,
                            Manufacturer = x.Manufacturer,
                            SKU = x.SKU,
                            Quantity = x.Quantity,
                            CatalogStandardID = x.CatalogStandardID,
                            BundleCategoryId = GetPartCategoryId(Convert.ToInt32(x.CatalogStandardID)) // get bundle category id (SUNNY GOHAR)
                        }).ToList();

                        obj.QuoteRequestList = request;
                    }
                    //----
                }
                else
                {
                    obj.OldQuoteResponse = _dbCon.sp_getQuoteResponse_Old_AdminR(quoteId.ToString(), dateTimeFormateID).ToList();
                    obj.PartQuoteRequests = _dbCon.tblPartQuoteRequests.Where(x => x.PartQuoteID == quoteId).ToList();

                    var partQuoteDetail = _dbCon.sp_getOldQuoteDetail_AdminR(quoteId.ToString(), groupName, sfaid).FirstOrDefault();
                    if (partQuoteDetail != null)
                    {
                        obj.QuoteStatusCode = partQuoteDetail.partquotestatuscode;
                        obj.CustomerId = Convert.ToInt32(partQuoteDetail.customerid);
                        obj.CurrencySymbol = partQuoteDetail.currencysymbol;
                        obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(partQuoteDetail.customerid).ToList();

                    }
                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                    {
                        Key = x.ManufacturerID,
                        Value = x.ManufacturerName
                    }).OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                    obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });
                }



                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteRequestResponseModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteRequestResponseModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteRequestResponseModel");
                }
                return new QuoteRequestResponseModel();
            }
        }
        public QuoteRequestResponseModel GetQuoteRequestResponseModelAsCustomer(int quoteId, string quoteType, int dateTimeFormateID, string groupName, string sfaid)
        {
            try
            {
                QuoteRequestResponseModel obj = new Model.QuoteRequestResponseModel();
                if (quoteType == "new" && groupName == "ISD")
                {
                    obj.NewQuoteResponse = _dbCon.sp_getQuoteResponses_R(quoteId, quoteType).Select(x => new sp_getQuoteResponse_New_AdminR_Result
                    {
                        manufacturername = x.manufacturername,
                        sku = x.sku,
                        price = x.price,
                        eol = x.eol.ToString(),
                        quantity = x.quantity,
                        partcategoryname = x.partcategoryname,
                        leadtime = x.leadtime,
                        description = x.description,

                    }).ToList();
                    obj.QuoteRequests = _dbCon.tblQuoteRequests.Where(x => x.QuoteID == quoteId).ToList();

                    var quoteDetail = _dbCon.sp_getNewQuoteDetail_AdminR(quoteId.ToString(), groupName, sfaid).FirstOrDefault();
                    if (quoteDetail != null)
                    {
                        obj.QuoteStatusCode = quoteDetail.quotestatuscode;
                        obj.CustomerId = Convert.ToInt32(quoteDetail.customerid);
                        obj.CurrencySymbol = quoteDetail.currencysymbol;
                        obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(quoteDetail.customerid).ToList();


                    }
                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                    {
                        Key = x.ManufacturerID,
                        Value = x.ManufacturerName
                    }).OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                    obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });
                    //--getting bundle category id for new quotes (SUNNY GOHAR)
                    if (obj.QuoteRequests != null && obj.QuoteRequests.Count > 0)
                    {
                        var request = obj.QuoteRequests.Select(x => new QuoteRequestModel
                        {
                            Description = x.Description,
                            Manufacturer = x.Manufacturer,
                            SKU = x.SKU,
                            Quantity = x.Quantity,
                            CatalogStandardID = x.CatalogStandardID,
                            BundleCategoryId = GetPartCategoryId(Convert.ToInt32(x.CatalogStandardID)) // get bundle category id (SUNNY GOHAR)
                        }).ToList();

                        obj.QuoteRequestList = request;
                    }
                    //----
                }
                else if (quoteType == "new" && groupName == "SFA")
                {
                    obj.NewQuoteResponse_SFA = _dbCon.sp_getQuoteResponse_New_SFA_AdminR(quoteId.ToString(), dateTimeFormateID).ToList();
                    obj.QuoteRequests = _dbCon.tblQuoteRequests.Where(x => x.QuoteID == quoteId).ToList();

                    var quoteDetail = _dbCon.sp_getNewQuoteDetail_AdminR(quoteId.ToString(), groupName, sfaid).FirstOrDefault();
                    if (quoteDetail != null)
                    {
                        obj.QuoteStatusCode = quoteDetail.quotestatuscode;
                        obj.CustomerId = Convert.ToInt32(quoteDetail.customerid);
                        obj.CurrencySymbol = quoteDetail.currencysymbol;
                        obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(quoteDetail.customerid).ToList();


                    }
                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                    {
                        Key = x.ManufacturerID,
                        Value = x.ManufacturerName
                    }).OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                    obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });
                    //--getting bundle category id for new quotes (SUNNY GOHAR)
                    if (obj.QuoteRequests != null && obj.QuoteRequests.Count > 0)
                    {
                        var request = obj.QuoteRequests.Select(x => new QuoteRequestModel
                        {
                            Description = x.Description,
                            Manufacturer = x.Manufacturer,
                            SKU = x.SKU,
                            Quantity = x.Quantity,
                            CatalogStandardID = x.CatalogStandardID,
                            BundleCategoryId = GetPartCategoryId(Convert.ToInt32(x.CatalogStandardID)) // get bundle category id (SUNNY GOHAR)
                        }).ToList();

                        obj.QuoteRequestList = request;
                    }
                    //----
                }
                else
                {
                    obj.OldQuoteResponse = _dbCon.sp_getQuoteResponse_Old_AdminR(quoteId.ToString(), dateTimeFormateID).ToList();
                    obj.PartQuoteRequests = _dbCon.tblPartQuoteRequests.Where(x => x.PartQuoteID == quoteId).ToList();

                    var partQuoteDetail = _dbCon.sp_getOldQuoteDetail_AdminR(quoteId.ToString(), groupName, sfaid).FirstOrDefault();
                    if (partQuoteDetail != null)
                    {
                        obj.QuoteStatusCode = partQuoteDetail.partquotestatuscode;
                        obj.CustomerId = Convert.ToInt32(partQuoteDetail.customerid);
                        obj.CurrencySymbol = partQuoteDetail.currencysymbol;
                        obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(partQuoteDetail.customerid).ToList();

                    }
                    obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                    {
                        Key = x.ManufacturerID,
                        Value = x.ManufacturerName
                    }).OrderBy(x => x.Value).ToList();
                    obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                    obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });
                }



                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteRequestResponseModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteRequestResponseModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteRequestResponseModel");
                }
                return new QuoteRequestResponseModel();
            }
        }

        /// <summary>
        /// this method use for quote notification
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <param name="dateFormatID"></param>
        /// <returns></returns>
        public int QuoteNotification(int quoteId, int sessionUserId, string quoteType, int dateFormatID)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string userName = string.Empty;


                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailbcc = mailfrom;

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.UserEmail))
                    {
                        mailto = quoteCustomerInfo.UserEmail;
                    }
                    else
                    {
                        mailto = string.Empty;
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    if (string.IsNullOrEmpty(quoteCustomerInfo.NetpQuoteEmail))
                    {
                        mailcc = string.Empty;
                    }
                    else
                    {
                        mailcc = quoteCustomerInfo.NetpQuoteEmail;
                    }

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + quoteCustomerInfo.SupplierEmail;
                        }
                        else
                        {
                            mailcc = mailcc + "; " + quoteCustomerInfo.SupplierEmail;
                        }
                    }


                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine;
                    mailbody = mailbody + "The following quote revalidation comes from GlobalServe ONESOURCE" + quoteCustomerInfo.UserName + Environment.NewLine + Environment.NewLine;

                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Response Date   : " + DateTime.Now + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;
                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine + Environment.NewLine;

                    if (quoteType == "new")
                    {
                        var quoteItem = _dbCon.sp_getQuoteResponse_New_AdminR(quoteId.ToString(), dateFormatID, false).ToList();
                        int itemCount = 1;
                        foreach (var item in quoteItem)
                        {
                            mailbody = mailbody + "Quote Item " + itemCount + " : " + item.description + Environment.NewLine;
                            mailbody = mailbody + "Manufacturer : " + item.manufacturername + Environment.NewLine;
                            mailbody = mailbody + "SKU          : " + item.sku + Environment.NewLine;
                            mailbody = mailbody + "Category     : " + item.partcategoryname + Environment.NewLine;
                            mailbody = mailbody + "Quantity     : " + item.quantity + Environment.NewLine;
                            mailbody = mailbody + "Price        : " + item.price + Environment.NewLine;
                            mailbody = mailbody + "EOL          : " + item.eol + Environment.NewLine;
                            itemCount++;
                        }
                    }
                    else
                    {
                        var quoteItem = _dbCon.sp_getQuoteResponse_Old_AdminR(quoteId.ToString(), dateFormatID).ToList();
                        int itemCount = 1;
                        foreach (var item in quoteItem)
                        {
                            mailbody = mailbody + "Quote Item " + itemCount + " : " + item.description + Environment.NewLine;
                            mailbody = mailbody + "Manufacturer : " + item.manufacturername + Environment.NewLine;
                            mailbody = mailbody + "SKU          : " + item.sku + Environment.NewLine;
                            mailbody = mailbody + "Category     : " + item.partcategoryname + Environment.NewLine;
                            mailbody = mailbody + "Quantity     : " + item.quantity + Environment.NewLine;
                            mailbody = mailbody + "Price        : " + item.price + Environment.NewLine;
                            mailbody = mailbody + "EOL          : " + item.eol + Environment.NewLine;
                            itemCount++;
                        }
                    }



                    if (quoteCustomerInfo.CustomerID == 245)
                    {
                        mailbody = mailbody + "To view the quote updates, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." + Environment.NewLine + Environment.NewLine;
                    }
                    else
                    {
                        mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to view the quote response." + Environment.NewLine;
                    }

                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Quote revalidated - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + item.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(item.Email))
                            {
                                mailcc = mailcc + "; " + item.Email;
                            }
                        }
                    }
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>QuoteNotification<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>QuoteNotification");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>QuoteNotification");
                }
                return -1;
            }

        }

        /// <summary>
        /// this method use for uploading old quote response 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="quoteId"></param>
        /// <param name="sessionUser"></param>
        /// <param name="eol"></param>
        /// <param name="dateFormatID"></param>
        /// <param name="quoteType"></param>
        /// <param name="priceLimit"></param>
        /// <returns></returns>
        public int UploadQuoteResponsesOld(List<UploadQuoteResponseModel> model, int quoteId, int sessionUser, DateTime eol, int dateFormatID, string quoteType, string priceLimit)
        {
            int modelCount = 1;
            try
            {
                int PartQuoteResponseID = 0, manufacturerPartID, catalogPartID;
                var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                if (partQuote != null)
                {
                    foreach (var item in model.Where(x => x.ManufacturerIds != 0 && !string.IsNullOrEmpty(x.Description) && x.CategoryId != 0 && eol != null).ToList())
                    {
                        PartQuoteResponseID = Convert.ToInt32(_dbCon.sp_AddPartQuoteResponse_AdminR(quoteId, item.ManufacturerIds, item.SKU, item.Description, item.CategoryId, item.Quantity, item.Price, eol.ToString(), sessionUser, item.LeadTime).FirstOrDefault() ?? 0);
                        if (PartQuoteResponseID == 0)
                        {
                            System.Diagnostics.EventLog.WriteEntry("Application", "sp_AddPartQuoteResponse return 0 ");
                            StaticHelper.LogException("sp_AddPartQuoteResponse return 0", "UploadQuoteResponses old Quote");
                            return 0;
                        }
                        var manufacturerPart = _dbCon.tblManufacturerParts.FirstOrDefault(x => x.ManufacturerID == item.ManufacturerIds && x.ManufacturerSKU == item.SKU.Trim());
                        if (manufacturerPart != null)
                        {
                            manufacturerPartID = manufacturerPart.ManufacturerPartID;
                            if (manufacturerPart.IsActive == false)
                            {
                                manufacturerPart.IsActive = true;
                                _dbCon.Entry(manufacturerPart).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                        }
                        else
                        {
                            manufacturerPartID = Convert.ToInt32(_dbCon.sp_AddManufacturerPart_AdminR(item.ManufacturerIds, item.SKU, item.Description, item.CategoryId, sessionUser).FirstOrDefault() ?? 0);
                        }

                        var catalogPart = _dbCon.tblCatalogParts.FirstOrDefault(x => x.ManufacturerPartID == manufacturerPartID && x.CustomerID == partQuote.CustomerID && x.PriceCodeID == 2 && x.SFAID == partQuote.SFAID);
                        if (catalogPart != null)
                        {
                            catalogPartID = catalogPart.CatalogPartID;
                            if (catalogPart.IsActive == false)
                            {
                                catalogPart.IsActive = true;
                                _dbCon.Entry(catalogPart).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                        }
                        else
                        {
                            catalogPartID = Convert.ToInt32(_dbCon.sp_AddCatalogPart_AdminR("", item.Description, item.Price, partQuote.CurrencyID, partQuote.CustomerID, "43000000", partQuote.SFAID, item.CategoryId, 2, manufacturerPartID, sessionUser, 1).FirstOrDefault() ?? 0);
                            if (catalogPartID == 0) return 0;
                        }

                        bool isActive = false;
                        if (!string.IsNullOrEmpty(priceLimit))
                        {
                            int returnCount = CheckPartRules(quoteId, partQuote.CustomerID, item.Description, manufacturerPartID, item.Price, partQuote.CurrencyID, partQuote.SFAID, sessionUser, "", "old");

                            if (returnCount == 0)
                            {
                                isActive = false;
                            }
                            else if (returnCount == 1)
                            {
                                isActive = true;
                            }
                            else if (returnCount == 2)
                            {
                                isActive = false;
                            }

                        }
                        else
                        {
                            isActive = true;
                        }

                        var customerCountryParts = _dbCon.tblCustomerCountryParts.FirstOrDefault(x => x.CatalogPartID == catalogPartID && x.CustomerID == partQuote.CustomerID && x.DestinationCountryID == partQuote.CountryID);
                        if (customerCountryParts != null)
                        {
                            customerCountryParts.IsActive = isActive;
                            customerCountryParts.ManufacturerPartID = manufacturerPartID;
                            customerCountryParts.PartPrice = item.Price;
                            customerCountryParts.PartPriceCurrencyID = partQuote.CurrencyID;
                            customerCountryParts.Discount = 0;
                            customerCountryParts.NonDirectPurchaseUplift = 0;
                            customerCountryParts.PartTradeBlockUplift = null;
                            customerCountryParts.SFASpecialUplift = null;
                            customerCountryParts.PartDescription = item.Description.Trim();
                            customerCountryParts.UNSPSC = "43000000";
                            customerCountryParts.PriceCodeID = 2;
                            customerCountryParts.SFAID = partQuote.SFAID;
                            customerCountryParts.EOL = eol;
                            customerCountryParts.LastUpdated = DateTime.Now;
                            customerCountryParts.LastUpdatedByUserID = sessionUser;

                            _dbCon.Entry(customerCountryParts).State = System.Data.Entity.EntityState.Modified;
                            _dbCon.SaveChanges();
                        }
                        else
                        {
                            var CustomerPartID = _dbCon.sp_AddCustomerCountryPart(manufacturerPartID, partQuote.CustomerID, catalogPartID, item.Price, partQuote.CurrencyID, 0, 0, item.Description.Trim(), item.CategoryId, partQuote.CountryID, "43000000", 2, partQuote.SFAID, eol.ToString(), isActive == true ? 1 : 0, sessionUser);
                            if (CustomerPartID == 0)
                            {
                                return 0;
                            }
                            var partquoteResponses = _dbCon.tblPartQuoteResponses.FirstOrDefault(x => x.PartQuoteResponseID == PartQuoteResponseID);
                            if (partquoteResponses != null)
                            {
                                partquoteResponses.CustomerPartID = CustomerPartID;
                                _dbCon.Entry(partquoteResponses).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                        }
                        modelCount++;

                    }

                    Update_Old_QuoteStatus(quoteId, sessionUser);
                    NotifyQuoteResponse(quoteId, "old", dateFormatID);
                }

                return PartQuoteResponseID > 0 ? PartQuoteResponseID : 0;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>UploadQuoteResponsesOld<<<<<-- Error at Row =>" + modelCount + " Exception=>" + ex.ToString());
                StaticHelper.LogException("Quote Search Presenter =>Error at Row => " + modelCount + " Exception=> " + ex.ToString(), "UploadQuoteResponsesOld");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>UploadQuoteResponsesOld");

                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for uploading new quote response 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="quoteId"></param>
        /// <param name="sessionUser"></param>
        /// <param name="dateFormatID"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int UploadQuoteResponsesNew(List<UploadQuoteResponseModel> model, int quoteId, int sessionUser, int dateFormatID, string quoteType)
        {
            int modelCount = 1;
            try
            {
                int manufacturerPartID, catalogPartID;
                int QuoteResponseID = 0;
                var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                if (quoteSummary != null)
                {
                    foreach (var item in model.Where(x => x.ManufacturerIds != 0 && !string.IsNullOrEmpty(x.Description) && x.CategoryId != 0 && x.EOL != null).ToList())
                    {
                        QuoteResponseID = _dbCon.usp_AddQuoteResponse(quoteId, item.ManufacturerIds, item.SKU, item.Description, item.CategoryId, item.LeadTime, item.EOL, item.Quantity, item.Price, sessionUser);

                        var manufacturerPart = _dbCon.tblManufacturerParts.FirstOrDefault(x => x.ManufacturerID == item.ManufacturerIds && x.ManufacturerSKU == item.SKU.Trim());
                        if (manufacturerPart != null)
                        {
                            manufacturerPartID = manufacturerPart.ManufacturerPartID;
                            if (manufacturerPart.IsActive == false)
                            {
                                manufacturerPart.IsActive = true;
                                _dbCon.Entry(manufacturerPart).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                        }
                        else
                        {
                            manufacturerPartID = Convert.ToInt32(_dbCon.sp_AddManufacturerPart_AdminR(item.ManufacturerIds, item.SKU, item.Description, item.CategoryId, sessionUser).FirstOrDefault() ?? 0);
                        }

                        var catalogPart = _dbCon.tblCatalogParts.FirstOrDefault(x => x.ManufacturerPartID == manufacturerPartID && x.CustomerID == quoteSummary.CustomerID && x.PriceCodeID == 2 && x.SFAID == quoteSummary.SFAID);
                        if (catalogPart != null)
                        {
                            catalogPartID = catalogPart.CatalogPartID;
                            if (catalogPart.IsActive == false)
                            {
                                catalogPart.IsActive = true;
                                _dbCon.Entry(catalogPart).State = System.Data.Entity.EntityState.Modified;
                                _dbCon.SaveChanges();
                            }
                        }
                        else
                        {
                            catalogPartID = Convert.ToInt32(_dbCon.sp_AddCatalogPart_AdminR("", item.Description, item.Price, quoteSummary.CurrencyID, quoteSummary.CustomerID, "43000000", quoteSummary.SFAID, item.CategoryId, 2, manufacturerPartID, sessionUser, 1).FirstOrDefault() ?? 0);

                        }
                        CheckPartRules(quoteId, quoteSummary.CustomerID, item.Description, manufacturerPartID, item.Price, quoteSummary.CurrencyID, quoteSummary.SFAID, sessionUser, "", "new");
                        modelCount++;
                    }

                    var bresult = Update_New_QuoteStatus(quoteId, sessionUser);
                    NotifyQuoteResponse(quoteId, "new", dateFormatID);

                }
                return QuoteResponseID > 0 ? QuoteResponseID : 0;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>UploadQuoteResponsesNew<<<<<-- Error at Row =>" + modelCount + " Exception=>" + ex.ToString());
                StaticHelper.LogException("Quote Search Presenter =>Error at Row => " + modelCount + " Exception=> " + ex.ToString(), "UploadQuoteResponsesNew");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>UploadQuoteResponsesNew");
                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for checking part rules
        /// </summary>
        /// <param name="PartQuoteID"></param>
        /// <param name="CustomerID"></param>
        /// <param name="Description"></param>
        /// <param name="ManufacturerPartID"></param>
        /// <param name="Price"></param>
        /// <param name="CurrencyID"></param>
        /// <param name="SFAID"></param>
        /// <param name="UserID"></param>
        /// <param name="PriceLimit"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int CheckPartRules(int PartQuoteID, int CustomerID, string Description, int ManufacturerPartID, decimal Price, int CurrencyID, int SFAID, int UserID, string PriceLimit, string quoteType)
        {
            try
            {
                int iReturn = 0;
                string MissingEmail = string.Empty;
                string EmailTo = string.Empty;
                string EmailFrom = string.Empty;
                string EmailCC = string.Empty;
                decimal ExchangeRate = 0;
                string RequestedBy = string.Empty;
                string sMailBody = string.Empty;



                if (PartQuoteID == 0 || CustomerID == 0 || string.IsNullOrEmpty(Description) || Price == 0 || CurrencyID == 0 || SFAID == 0 || ManufacturerPartID == 0 || UserID == 0 || string.IsNullOrEmpty(PriceLimit))
                {
                    return 2;
                }
                var tblPrimeContractorExchangeRates = _dbCon.tblPrimeContractorExchangeRates.FirstOrDefault(x => x.IsActive == 1 && x.PrimeContractorID == 1);
                if (tblPrimeContractorExchangeRates != null)
                {
                    ExchangeRate = tblPrimeContractorExchangeRates.ExchangeRateToUsDollars;
                }
                if (ExchangeRate == 0)
                {
                    return 2;
                }
                if ((Price / ExchangeRate) > Convert.ToDecimal(PriceLimit))
                {
                    var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                    if (primecontractor == null)
                    {
                        return iReturn;
                    }

                    MissingEmail = primecontractor.PartQuoteEmail;

                    var customer = _dbCon.tblCustomers.FirstOrDefault(x => x.CustomerID == CustomerID);
                    if (customer != null)
                    {
                        var parentCompany = _dbCon.tblParentCompanies.FirstOrDefault(x => x.ParentCompanyID == customer.ParentCompanyID);
                        if (parentCompany != null)
                        {
                            EmailTo = string.IsNullOrEmpty(parentCompany.GSContactEmail) ? "" : parentCompany.GSContactEmail;
                        }
                        else
                        {
                            EmailTo = MissingEmail;
                        }
                    }
                    EmailFrom = EmailTo;
                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == UserID);
                    if (contact != null)
                    {
                        EmailCC = string.IsNullOrEmpty(contact.Email) ? "" : contact.Email;
                    }

                    if (quoteType == "old")
                    {
                        var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == PartQuoteID);
                        if (partQuote != null)
                        {
                            var customeruser = _dbCon.tblCustomerUsers.FirstOrDefault(x => x.UserID == partQuote.CreatedBy);
                            if (customeruser != null)
                            {
                                var tblcontact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == customeruser.UserID);
                                if (tblcontact != null)
                                {
                                    RequestedBy = (string.IsNullOrEmpty(tblcontact.FirstName) ? "" : tblcontact.FirstName) + (string.IsNullOrEmpty(tblcontact.LastName) ? "" : tblcontact.LastName);
                                }
                            }
                        }
                    }
                    else if (quoteType == "new")
                    {
                        var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == PartQuoteID);
                        if (quoteSummary != null)
                        {
                            var customeruser = _dbCon.tblCustomerUsers.FirstOrDefault(x => x.UserID == quoteSummary.CreatedBy);
                            if (customeruser != null)
                            {
                                var tblcontact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == customeruser.UserID);
                                if (tblcontact != null)
                                {
                                    RequestedBy = (string.IsNullOrEmpty(tblcontact.FirstName) ? "" : tblcontact.FirstName) + (string.IsNullOrEmpty(tblcontact.LastName) ? "" : tblcontact.LastName);
                                }
                            }
                        }
                    }

                    var partToAllow = _dbCon.sp_getPartToApproveQuoteResponse_AdminR(CustomerID, CurrencyID, SFAID, UserID, ManufacturerPartID).FirstOrDefault();
                    if (partToAllow != null)
                    {
                        sMailBody = "Customer: " + partToAllow.CustomerName + Environment.NewLine;
                        sMailBody = sMailBody + "Quote#: " + PartQuoteID + Environment.NewLine;
                        sMailBody = sMailBody + "Requested By: " + RequestedBy + Environment.NewLine + Environment.NewLine;
                        sMailBody = sMailBody + "The part below needs approval due to the price limit :" + Environment.NewLine + Environment.NewLine;
                        sMailBody = sMailBody + "Price Limit   : " + PriceLimit + " US Dollars" + Environment.NewLine;
                        sMailBody = sMailBody + "SKU           : " + partToAllow.ManufacturerSKU + Environment.NewLine;
                        sMailBody = sMailBody + "Manufacturer  : " + partToAllow.ManufacturerName + Environment.NewLine;
                        sMailBody = sMailBody + "Description   : " + Description + Environment.NewLine;
                        sMailBody = sMailBody + "Price         : " + Price + Environment.NewLine;
                        sMailBody = sMailBody + "Currency      : " + partToAllow.CurrencyName + Environment.NewLine;
                        sMailBody = sMailBody + "Supplier      : " + partToAllow.SFAName + Environment.NewLine;
                        sMailBody = sMailBody + "User          : " + partToAllow.UserName + Environment.NewLine + Environment.NewLine;


                        sMailBody = sMailBody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com";
                        OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(EmailTo, EmailFrom, EmailCC, "", sMailBody, "GlobalServe ONESOURCE - Part approval required - " + partToAllow.CustomerName);
                        iReturn = 0;
                    }
                    else
                    {
                        iReturn = 2;
                    }
                }
                else
                {
                    iReturn = 1;
                }
                return iReturn;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>CheckPartRules<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>CheckPartRules");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>CheckPartRules");
                }
                return 1;
            }
        }

        /// <summary>
        /// this method use for notification of quote response
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <param name="dateFormatID"></param>
        public void NotifyQuoteResponse(int quoteId, string quoteType, int dateFormatID)
        {

            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string userName = string.Empty;


                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailbcc = mailfrom;

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.UserEmail))
                    {
                        mailto = quoteCustomerInfo.UserEmail;
                    }
                    else
                    {
                        mailto = string.Empty;
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }

                    if (string.IsNullOrEmpty(quoteCustomerInfo.NetpQuoteEmail))
                    {
                        mailcc = string.Empty;
                    }
                    else
                    {
                        mailcc = quoteCustomerInfo.NetpQuoteEmail;
                    }

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + quoteCustomerInfo.SupplierEmail;
                        }
                        else
                        {
                            mailcc = mailcc + "; " + quoteCustomerInfo.SupplierEmail;
                        }
                    }


                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine;
                    mailbody = mailbody + "The following quote response comes from GlobalServe ONESOURCE" + Environment.NewLine + Environment.NewLine;

                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Response Date   : " + DateTime.Now + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;
                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine + Environment.NewLine;




                    if (quoteType == "new")
                    {
                        var quoteItem = _dbCon.sp_getQuoteResponse_New_AdminR(quoteId.ToString(), dateFormatID, false).ToList();
                        int itemCount = 1;
                        string currencySymbol = string.Empty;
                        var quotesummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                        if (quotesummary != null)
                        {
                            var currency = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == quotesummary.CurrencyID);
                            if (currency != null)
                            {
                                currencySymbol = currency.CurrencySymbol;
                            }
                        }
                        foreach (var item in quoteItem)
                        {

                            mailbody = mailbody + "Quote Item " + itemCount + " : " + item.description + Environment.NewLine;
                            mailbody = mailbody + "Manufacturer : " + item.manufacturername + Environment.NewLine;
                            mailbody = mailbody + "SKU          : " + item.sku + Environment.NewLine;
                            mailbody = mailbody + "Category     : " + item.partcategoryname + Environment.NewLine;
                            mailbody = mailbody + "Quantity     : " + item.quantity + Environment.NewLine;
                            mailbody = mailbody + "Price        : " + item.price + " " + currencySymbol + Environment.NewLine;
                            mailbody = mailbody + "EOL          : " + item.eol + Environment.NewLine + Environment.NewLine;
                            itemCount++;
                        }
                    }
                    else
                    {
                        var quoteItem = _dbCon.sp_getQuoteResponse_Old_AdminR(quoteId.ToString(), dateFormatID).ToList();
                        int itemCount = 1;
                        string currencySymbol = string.Empty;
                        var quotePart = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                        if (quotePart != null)
                        {
                            var currency = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == quotePart.CurrencyID);
                            if (currency != null)
                            {
                                currencySymbol = currency.CurrencySymbol;
                            }
                        }
                        foreach (var item in quoteItem)
                        {

                            mailbody = mailbody + "Quote Item " + itemCount + " : " + item.description + Environment.NewLine;
                            mailbody = mailbody + "Manufacturer : " + item.manufacturername + Environment.NewLine;
                            mailbody = mailbody + "SKU          : " + item.sku + Environment.NewLine;
                            mailbody = mailbody + "Category     : " + item.partcategoryname + Environment.NewLine;
                            mailbody = mailbody + "Quantity     : " + item.quantity + Environment.NewLine;
                            mailbody = mailbody + "Price        : " + item.price + " " + currencySymbol + Environment.NewLine;
                            mailbody = mailbody + "EOL          : " + item.eol + Environment.NewLine + Environment.NewLine;
                            itemCount++;
                        }
                    }

                    if (quoteCustomerInfo.CustomerID == 245)
                    {
                        mailbody = mailbody + "To view the quote updates, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." + Environment.NewLine + Environment.NewLine;
                    }
                    else
                    {
                        mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to view the quote response." + Environment.NewLine;
                    }

                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    mailsubject = "GlobalServe ONESOURCE - Quote Response - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + item.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(item.Email))
                            {
                                mailcc = mailcc + "; " + item.Email;
                            }
                        }
                    }
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
                //  return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>NotifyQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>NotifyQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>NotifyQuoteResponse");
                }
            }


        }

        /// <summary>
        /// this method use for updating new quote status
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool Update_New_QuoteStatus(int quoteId, int userId)
        {
            try
            {


                int NewID = 0;
                int CompleteID = 0;
                int CancelledID = 0;
                int InProgressID = 0;
                if (quoteId <= 0 && userId <= 0)
                {
                    return false;
                }
                var quoteStatus = _dbCon.tblQuoteStatus.Where(x => x.IsActive == true).ToList();
                if (quoteStatus != null)
                {
                    if (quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "N") != null)
                    {
                        NewID = quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "N").QuoteStatusID;
                    }
                    if (quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "C") != null)
                    {
                        CompleteID = quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "C").QuoteStatusID;
                    }
                    if (quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "X") != null)
                    {
                        CancelledID = quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "X").QuoteStatusID;
                    }
                    if (quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "P") != null)
                    {
                        InProgressID = quoteStatus.FirstOrDefault(x => x.QuoteStatusCode == "P").QuoteStatusID;
                    }
                    var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                    if (quoteSummary != null)
                    {
                        if (quoteSummary.QuoteStatusID == CancelledID)
                        {
                            return true;
                        }
                        else
                        {
                            var quoteResponse = _dbCon.tblQuoteResponses.Where(x => x.QuoteID == quoteId).ToList();
                            if (quoteResponse.Count > 0)
                            {
                                if (quoteSummary.QuoteStatusID != InProgressID)
                                {
                                    quoteSummary.QuoteStatusID = CompleteID;
                                    quoteSummary.LastUpdated = DateTime.Now;
                                    quoteSummary.LastUpdatedByUserID = userId;
                                    _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                                    int count = _dbCon.SaveChanges();
                                    if (count > 0) return true;
                                    else return false;
                                }


                            }
                            else if (quoteSummary.QuoteStatusID != NewID)
                            {
                                quoteSummary.QuoteStatusID = NewID;
                                quoteSummary.LastUpdated = DateTime.Now;
                                quoteSummary.LastUpdatedByUserID = userId;
                                _dbCon.Entry(quoteSummary).State = System.Data.Entity.EntityState.Modified;
                                int count = _dbCon.SaveChanges();
                                if (count > 0) return true;
                                else return false;
                            }

                        }

                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return false;

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>Update_New_QuoteStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>Update_New_QuoteStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>Update_New_QuoteStatus");
                }
                return false;
            }
        }

        public string GetQuoteStatusNew(int quoteId)
        {
            var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
            if (quoteSummary != null)
            {
                var status = _dbCon.tblQuoteStatus.FirstOrDefault(x => x.QuoteStatusID == quoteSummary.QuoteStatusID);
                if (status != null)
                {
                    return status.QuoteStatus;
                }
            }
            return "";
        }
        public string GetQuoteStatusOld(int quoteId)
        {
            var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
            if (partQuote != null)
            {
                var status = _dbCon.tblPartQuoteStatus.FirstOrDefault(x => x.PartQuoteStatusID == partQuote.PartQuoteStatusID);
                if (status != null)
                {
                    return status.PartQuoteStatus;
                }
            }
            return "";
        }

        /// <summary>
        /// this method use for updating old quote status
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool Update_Old_QuoteStatus(int quoteId, int userId)
        {
            try
            {
                int NewID = 0;
                int CompleteID = 0;
                int CancelledID = 0;
                int InProgressID = 0;
                if (quoteId <= 0 && userId <= 0)
                {
                    return false;
                }
                var partquoteStatus = _dbCon.tblPartQuoteStatus.Where(x => x.IsActive == true).ToList();
                if (partquoteStatus != null)
                {
                    if (partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "N") != null)
                    {
                        NewID = partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "N").PartQuoteStatusID;
                    }
                    if (partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "C") != null)
                    {
                        CompleteID = partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "C").PartQuoteStatusID;
                    }
                    if (partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "X") != null)
                    {
                        CancelledID = partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "X").PartQuoteStatusID;
                    }
                    if (partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "P") != null)
                    {
                        InProgressID = partquoteStatus.FirstOrDefault(x => x.PartQuoteStatusCode == "P").PartQuoteStatusID;
                    }
                    var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                    if (partQuote != null)
                    {
                        if (partQuote.PartQuoteStatusID == CancelledID)
                        {
                            return true;
                        }
                        else
                        {
                            var partQuoteResponse = _dbCon.tblPartQuoteResponses.Where(x => x.PartQuoteID == quoteId).Count();
                            if (partQuoteResponse > 0)
                            {
                                if (partQuote.PartQuoteStatusID != InProgressID)
                                {
                                    partQuote.PartQuoteStatusID = CompleteID;
                                    partQuote.LastUpdated = DateTime.Now;
                                    partQuote.LastUpdatedByUserID = userId;
                                    _dbCon.Entry(partQuote).State = System.Data.Entity.EntityState.Modified;
                                    int count = _dbCon.SaveChanges();
                                    if (count > 0) return true;
                                    else return false;
                                }

                            }
                            else if (partQuote.PartQuoteStatusID != NewID)
                            {
                                partQuote.PartQuoteStatusID = NewID;
                                partQuote.LastUpdated = DateTime.Now;
                                partQuote.LastUpdatedByUserID = userId;
                                _dbCon.Entry(partQuote).State = System.Data.Entity.EntityState.Modified;
                                int count = _dbCon.SaveChanges();
                                if (count > 0) return true;
                                else return false;
                            }
                        }

                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>Update_Old_QuoteStatus<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>Update_Old_QuoteStatus");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>Update_Old_QuoteStatus");

                }
                return false;
            }
        }

        /// <summary>
        /// this method use for getting new quote response
        /// </summary>
        /// <param name="quoteResponseId"></param>
        /// <param name="quoteId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public QuoteResponseEditModel GetNewQuoteResponse(int quoteResponseId, int quoteId, int customerId)
        {
            try
            {

                QuoteResponseEditModel obj = new QuoteResponseEditModel();
                obj.QuoteResponse = _dbCon.sp_getQuoteResponseById_AdminR(quoteResponseId, "new").FirstOrDefault() ?? new sp_getQuoteResponseById_AdminR_Result();
                obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.ManufacturerID,
                    Value = x.ManufacturerName
                }).OrderBy(x => x.Value).ToList();
                obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(customerId).ToList();
                obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });

                obj.QuoteId = quoteId;
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetNewQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetNewQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetNewQuoteResponse");

                }
                return new QuoteResponseEditModel();
            }

        }

        /// <summary>
        /// this method use for getting manufacturer
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetManufacturer()
        {
            try
            {
                var mfg = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.ManufacturerID,
                    Value = x.ManufacturerName
                }).ToList();
                mfg.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                return mfg;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetManufacturer<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetManufacturer");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetManufacturer");

                }
                return new List<GenericList>();
            }
        }

        /// <summary>
        /// this method use for getting quote request response categories
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public List<sp_getQuoteReqResCategories_AdminR_Result> GetQuoteReqResCategories(int quoteId, string quoteType)
        {
            try
            {
                if (quoteType == "new")
                {
                    var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                    if (quoteSummary != null)
                    {
                        return _dbCon.sp_getQuoteReqResCategories_AdminR(quoteSummary.CustomerID).ToList();
                    }
                }
                else
                {
                    var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                    if (partQuote != null)
                    {
                        return _dbCon.sp_getQuoteReqResCategories_AdminR(partQuote.CustomerID).ToList();
                    }
                }
                return new List<sp_getQuoteReqResCategories_AdminR_Result>();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteReqResCategories<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteReqResCategories");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteReqResCategories");

                }
                return new List<sp_getQuoteReqResCategories_AdminR_Result>();
            }
        }

        /// <summary>
        /// this method use for getting old quote response
        /// </summary>
        /// <param name="quoteResponseId"></param>
        /// <param name="quoteId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public QuoteResponseEditModel GetOldQuoteResponse(int quoteResponseId, int quoteId, int customerId)
        {
            try
            {

                QuoteResponseEditModel obj = new QuoteResponseEditModel();
                obj.PartQuoteResponse = _dbCon.sp_getQuoteResponseById_AdminR(quoteResponseId, "old").FirstOrDefault() ?? new sp_getQuoteResponseById_AdminR_Result();
                obj.Manufacturers = _dbCon.tblManufacturers.Where(x => x.IsActive == true).Select(x => new GenericList
                {
                    Key = x.ManufacturerID,
                    Value = x.ManufacturerName
                }).OrderBy(x => x.Value).ToList();
                obj.Manufacturers.Insert(0, new Model.GenericList { Value = "(None)", Key = 0 });
                obj.Categories = _dbCon.sp_getQuoteReqResCategories_AdminR(customerId).ToList();
                obj.Categories.Insert(0, new sp_getQuoteReqResCategories_AdminR_Result { partcategoryname = "(None)", partcategoryid = 0 });

                obj.QuoteId = quoteId;
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetOldQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetOldQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetOldQuoteResponse");

                }
                return new QuoteResponseEditModel();
            }

        }

        /// <summary>
        /// this method use for updating new quote response
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int UpdateNewQuoteResponse(sp_getQuoteResponseById_AdminR_Result obj, int quoteId, int sessionUserId)
        {
            try
            {
                int count = 0;
                int ResponseRevalidateCount, RequestRevalidateCount;
                var quoteReqRes = _dbCon.sp_getQuoteRequestResponseCount_AdminR(obj.responseId, "new").FirstOrDefault();
                if (quoteReqRes != null)
                {
                    RequestRevalidateCount = quoteReqRes.Request != null ? Convert.ToInt32(quoteReqRes.Request) : 0;
                    ResponseRevalidateCount = quoteReqRes.Response != null ? Convert.ToInt32(quoteReqRes.Response) : 0;

                    if (!(RequestRevalidateCount > 0) || (ResponseRevalidateCount == RequestRevalidateCount))
                    {
                        var quoteResponse = _dbCon.tblQuoteResponses.FirstOrDefault(x => x.QuoteResponseID == obj.responseId);
                        if (quoteResponse != null)
                        {
                            quoteResponse.ManufacturerID = Convert.ToInt32(obj.ManufacturerID);
                            quoteResponse.SKU = obj.SKU;
                            quoteResponse.Description = obj.description;
                            quoteResponse.PartCategoryID = Convert.ToInt32(obj.PartCategoryID);
                            quoteResponse.LeadTime = obj.LeadTime;
                            quoteResponse.EOL = Convert.ToDateTime(obj.EOL);
                            quoteResponse.Quantity = obj.Quantity;
                            quoteResponse.Price = obj.Price;
                            quoteResponse.LastUpdated = DateTime.Now;
                            quoteResponse.LastUpdatedByUserID = sessionUserId;


                            _dbCon.Entry(quoteResponse).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }

                    }
                    else if (RequestRevalidateCount > ResponseRevalidateCount)
                    {
                        var quoteResponse = _dbCon.tblQuoteResponses.FirstOrDefault(x => x.QuoteResponseID == obj.responseId);
                        quoteResponse.ManufacturerID = Convert.ToInt32(obj.ManufacturerID);
                        quoteResponse.SKU = obj.SKU;
                        quoteResponse.Description = obj.description;
                        quoteResponse.PartCategoryID = Convert.ToInt32(obj.PartCategoryID);
                        quoteResponse.LeadTime = obj.LeadTime;
                        quoteResponse.EOL = Convert.ToDateTime(obj.EOL);
                        quoteResponse.Quantity = obj.Quantity;
                        quoteResponse.Price = obj.Price;
                        quoteResponse.RevalidateDate = DateTime.Now;
                        quoteResponse.revalidateNum = quoteResponse.revalidateNum ?? (Convert.ToInt32(quoteResponse.revalidateNum) + 1);
                        quoteResponse.RevalidatebyUser = sessionUserId;

                        _dbCon.Entry(quoteResponse).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();

                        tblQuoteRevalidationHistory qrhistory = new Entity.tblQuoteRevalidationHistory();
                        qrhistory.QuoteID = quoteId;
                        qrhistory.QuoteResponseID = obj.responseId;
                        qrhistory.RevalidateRequest = false;
                        qrhistory.RevalidateDate = DateTime.Now;
                        qrhistory.ManufacturerID = obj.ManufacturerID;
                        qrhistory.SKU = obj.SKU;
                        qrhistory.description = obj.description;
                        qrhistory.PartCategoryID = obj.PartCategoryID;
                        qrhistory.LeadTime = obj.LeadTime;
                        qrhistory.EOL = obj.EOL;
                        qrhistory.Quantity = obj.Quantity;
                        qrhistory.Price = obj.Price;
                        qrhistory.RevalidateBy = sessionUserId;

                        _dbCon.tblQuoteRevalidationHistories.Add(qrhistory);
                        _dbCon.SaveChanges();


                    }


                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>UpdateNewQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>UpdateNewQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>UpdateNewQuoteResponse");

                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for updating old quote response
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int UpdateOldQuoteResponse(sp_getQuoteResponseById_AdminR_Result obj, int quoteId, int sessionUserId)
        {
            try
            {
                int count = 0;
                int ResponseRevalidateCount, RequestRevalidateCount;
                var quoteReqRes = _dbCon.sp_getQuoteRequestResponseCount_AdminR(obj.responseId, "old").FirstOrDefault();
                if (quoteReqRes != null)
                {
                    RequestRevalidateCount = quoteReqRes.Request != null ? Convert.ToInt32(quoteReqRes.Request) : 0;
                    ResponseRevalidateCount = quoteReqRes.Response != null ? Convert.ToInt32(quoteReqRes.Response) : 0;

                    if (!(RequestRevalidateCount > 0) || (ResponseRevalidateCount == RequestRevalidateCount))
                    {
                        var quoteResponse = _dbCon.tblPartQuoteResponses.FirstOrDefault(x => x.PartQuoteResponseID == obj.responseId);
                        if (quoteResponse != null)
                        {
                            quoteResponse.ManufacturerID = Convert.ToInt32(obj.ManufacturerID);
                            quoteResponse.SKU = obj.SKU;
                            quoteResponse.Description = obj.description;
                            quoteResponse.PartCategoryID = Convert.ToInt32(obj.PartCategoryID);
                            quoteResponse.LeadTime = obj.LeadTime;
                            quoteResponse.EOL = Convert.ToDateTime(obj.EOL);
                            quoteResponse.Quantity = Convert.ToInt32(obj.Quantity);
                            quoteResponse.Price = Convert.ToDecimal(obj.Price);
                            quoteResponse.LastUpdated = DateTime.Now;
                            quoteResponse.LastUpdatedByUserID = sessionUserId;


                            _dbCon.Entry(quoteResponse).State = System.Data.Entity.EntityState.Modified;
                            count = _dbCon.SaveChanges();
                        }

                    }
                    else if (RequestRevalidateCount > ResponseRevalidateCount)
                    {
                        var quoteResponse = _dbCon.tblPartQuoteResponses.FirstOrDefault(x => x.PartQuoteResponseID == obj.responseId);
                        quoteResponse.ManufacturerID = Convert.ToInt32(obj.ManufacturerID);
                        quoteResponse.SKU = obj.SKU;
                        quoteResponse.Description = obj.description;
                        quoteResponse.PartCategoryID = Convert.ToInt32(obj.PartCategoryID);
                        quoteResponse.LeadTime = obj.LeadTime;
                        quoteResponse.EOL = Convert.ToDateTime(obj.EOL);
                        quoteResponse.Quantity = Convert.ToInt32(obj.Quantity);
                        quoteResponse.Price = Convert.ToDecimal(obj.Price);
                        quoteResponse.RevalidateDate = DateTime.Now;
                        quoteResponse.RevalidateNum = quoteResponse.RevalidateNum ?? (Convert.ToInt32(quoteResponse.RevalidateNum) + 1);
                        quoteResponse.RevalidateByUser = sessionUserId;

                        _dbCon.Entry(quoteResponse).State = System.Data.Entity.EntityState.Modified;
                        count = _dbCon.SaveChanges();

                        tblPartQuoteRevalidationHistory qrhistory = new Entity.tblPartQuoteRevalidationHistory();
                        qrhistory.PartQuoteID = quoteId;
                        qrhistory.PartQuoteResponseID = obj.responseId;
                        qrhistory.RevalidateRequest = false;
                        qrhistory.RevalidateDate = DateTime.Now;
                        qrhistory.ManufacturerID = obj.ManufacturerID;
                        qrhistory.SKU = obj.SKU;
                        qrhistory.Description = obj.description;
                        qrhistory.PartCategoryID = obj.PartCategoryID;
                        qrhistory.LeadTime = obj.LeadTime;
                        qrhistory.EOL = obj.EOL;
                        qrhistory.Quantity = obj.Quantity;
                        qrhistory.Price = obj.Price;
                        qrhistory.RevalidateBy = sessionUserId;

                        _dbCon.tblPartQuoteRevalidationHistories.Add(qrhistory);
                        _dbCon.SaveChanges();


                    }


                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>UpdateOldQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>UpdateOldQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>UpdateOldQuoteResponse");

                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for deleting new quote response
        /// </summary>
        /// <param name="quoteResponseId"></param>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int DeleteNewQuoteResponse(string quoteResponseId, int quoteId, int sessionUserId)
        {
            try
            {
                int count = 0;

                var numbers = quoteResponseId.Split(',').Select(Int32.Parse).ToList();
                if (quoteResponseId != "" && quoteId > 0)
                {
                    var quoteResponse = _dbCon.tblQuoteResponses.Where(x => numbers.Contains(x.QuoteResponseID)).ToList();

                    if (quoteResponse != null)
                    {
                        _dbCon.tblQuoteResponses.RemoveRange(quoteResponse);
                        count = _dbCon.SaveChanges();
                        StaticHelper.LogException("Quote Response Item Deleted by User:(" + sessionUserId + ")" + " QuoteId:(" + quoteId + " QuoteResponseId:(" + quoteResponseId + ")", "DeleteNewQuoteResponse");
                        Update_New_QuoteStatus(quoteId, sessionUserId);

                    }
                }

                //if (quoteResponseId != "" && quoteId > 0)
                //{
                //    var quoteResponse = from x in _dbCon.tblQuoteResponses
                //                        where numbers.Contains(x.QuoteResponseID)
                //                        select x;
                //    //var quoteResponse = _dbCon.tblQuoteResponses.Select(x => x.QuoteResponseID == quoteResponseId);
                //    //var quoteResponse = _dbCon.tblQuoteResponses.Select(x => quoteResponseId.Contains(x.QuoteResponseID));
                //    if (quoteResponse != null)
                //    {
                //        _dbCon.tblQuoteResponses.RemoveRange(quoteResponse);
                //        count = _dbCon.SaveChanges();

                //        Update_New_QuoteStatus(quoteId, sessionUserId);
                //    }
                //}


                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>DeleteNewQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>DeleteNewQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>DeleteNewQuoteResponse");

                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for deleting old quote response
        /// </summary>
        /// <param name="quoteResponseId"></param>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <returns></returns>
        public int DeleteOldQuoteResponse(string quoteResponseId, int quoteId, int sessionUserId)
        {
            int count = 0;
            try
            {
                var numbers = quoteResponseId.Split(',').Select(Int32.Parse).ToList();
                //List<string> result = quoteResponseId.Split(',').ToList();
                if (quoteResponseId != "" && quoteId > 0)
                {
                    var quoteResponse = _dbCon.tblPartQuoteResponses.Where(x => numbers.Contains(x.PartQuoteResponseID))
                                                                    /*.Select(x => x.PartQuoteResponseID).AsEnumerable()*/.ToList();
                    //var quoteResponse = from x in _dbCon.tblQuoteResponses tqr
                    //                    where numbers.Contains(x.QuoteResponseID)
                    //                    select tqr.QuoteResponseID;
                    //var quoteResponse = _dbCon.tblQuoteResponses.Select(x => x.QuoteResponseID == quoteResponseId);
                    //var quoteResponse = _dbCon.tblQuoteResponses.Select(x => quoteResponseId.Contains(x.QuoteResponseID));
                    if (quoteResponse != null)
                    {
                        //var quoteResponse2 = _dbCon.tblQuoteResponses.Where(x => numbers.Contains(x.quoteResponse))
                        //                                          /*.Select(x => x.PartQuoteResponseID).AsEnumerable()*/.ToList();

                        _dbCon.tblPartQuoteResponses.RemoveRange(quoteResponse);
                        count = _dbCon.SaveChanges();
                        StaticHelper.LogException("Quote Response Item Deleted by User:(" + sessionUserId + ")" + " QuoteId:(" + quoteId + " QuoteResponseId:(" + quoteResponseId + ")", "DeleteNewQuoteResponse");
                        Update_New_QuoteStatus(quoteId, sessionUserId);
                    }
                }

                //if (quoteResponseId > 0 && quoteId > 0)
                //{
                //    var partquoteResponse = _dbCon.tblPartQuoteResponses.FirstOrDefault(x => x.PartQuoteResponseID == quoteResponseId);
                //    if (partquoteResponse != null)
                //    {
                //        _dbCon.tblPartQuoteResponses.Remove(partquoteResponse);
                //        count = _dbCon.SaveChanges();

                //        Update_Old_QuoteStatus(quoteId, sessionUserId);
                //    }
                //}


            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>DeleteOldQuoteResponse<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>DeleteOldQuoteResponse");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>DeleteOldQuoteResponse");

                }
                return -1;
            }


            return count;
        }

        /// <summary>
        /// this method use for getting new quote comment history model
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="dateFormatId"></param>
        /// <returns></returns>
        public QuoteCommentHistoryModel GetNewQuoteCommentHistoryModel(int quoteId, int dateFormatId)
        {
            try
            {
                QuoteCommentHistoryModel obj = new QuoteCommentHistoryModel();
                obj.QuoteHistory = _dbCon.sp_getQuoteHistory_AdminR(quoteId.ToString(), dateFormatId, "new").ToList();
                obj.GSQuoteComment = _dbCon.sp_getQuoteGSComment_AdminR(quoteId.ToString(), dateFormatId, "new").ToList();
                obj.QuoteId = quoteId;
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetNewQuoteCommentHistoryModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetNewQuoteCommentHistoryModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetNewQuoteCommentHistoryModel");

                }
                return new QuoteCommentHistoryModel();
            }
        }

        /// <summary>
        /// this method use for getting old quote comment history model
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="dateFormatId"></param>
        /// <returns></returns>
        public QuoteCommentHistoryModel GetOldQuoteCommentHistoryModel(int quoteId, int dateFormatId)
        {
            try
            {
                QuoteCommentHistoryModel obj = new QuoteCommentHistoryModel();
                obj.QuoteHistory = _dbCon.sp_getQuoteHistory_AdminR(quoteId.ToString(), dateFormatId, "old").ToList();
                obj.GSQuoteComment = _dbCon.sp_getQuoteGSComment_AdminR(quoteId.ToString(), dateFormatId, "old").ToList();
                obj.QuoteId = quoteId;
                return obj;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetOldQuoteCommentHistoryModel<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetOldQuoteCommentHistoryModel");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetOldQuoteCommentHistoryModel");

                }
                return new QuoteCommentHistoryModel();
            }
        }

        /// <summary>
        /// this method use for adding new quote comment
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="comments"></param>
        /// <param name="isGSOnly"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int AddNewQuoteComment(int quoteId, string comments, bool isGSOnly, int sessionUserId, string userName, bool IsUrgent = false, string JiraTicketNum = "")
        {
            try
            {
                int count = 0;

                var quoteSummary = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                if (quoteSummary == null) return 0;

                //Aurangzeb Khan
                //3-12-2018
                //Update quote ticket here

                var updatedQuote = quoteSummary;
                updatedQuote.JiraTicketNumber = JiraTicketNum;
                _dbCon.Entry(quoteSummary).CurrentValues.SetValues(updatedQuote);
                _dbCon.SaveChanges();

                if (isGSOnly)
                {
                    tblQuoteTracking quoteTracking = new Entity.tblQuoteTracking();
                    quoteTracking.QuoteID = quoteId;
                    quoteTracking.Comments = comments.Trim();
                    quoteTracking.LastUpdatedByUserID = sessionUserId;
                    quoteTracking.LastUpdated = DateTime.Now;
                    quoteTracking.GSComments = true;
                    quoteTracking.QuoteStatusID = quoteSummary.QuoteStatusID;
                    quoteTracking.QuoteActionName = "Add Comment";
                    _dbCon.tblQuoteTrackings.Add(quoteTracking);
                    count = _dbCon.SaveChanges();

                }
                else
                {
                    tblQuoteTracking quoteTracking = new Entity.tblQuoteTracking();
                    quoteTracking.QuoteID = quoteId;
                    quoteTracking.Comments = comments.Trim();
                    quoteTracking.LastUpdated = DateTime.Now;
                    quoteTracking.LastUpdatedByUserID = sessionUserId;
                    _dbCon.tblQuoteTrackings.Add(quoteTracking);
                    count = _dbCon.SaveChanges();
                    if (count > 0)
                    {
                        var quoteStatus = _dbCon.tblQuoteStatus.FirstOrDefault(x => x.QuoteStatusID == quoteSummary.QuoteStatusID);
                        if (quoteStatus != null)
                        {
                            QuoteCommentEmail(quoteId, sessionUserId, "new", comments, quoteStatus.QuoteStatus, IsUrgent);
                        }
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>AddNewQuoteComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>AddNewQuoteComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>AddNewQuoteComment");

                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for adding old quote comment
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="comments"></param>
        /// <param name="isGSOnly"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public int AddOldQuoteComment(int quoteId, string comments, bool isGSOnly, int sessionUserId, string userName, bool IsUrgent = false, string JiraTicketNum = "")
        {
            try
            {
                int count = 0;

                var partQuote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                if (partQuote == null) return 0;

                var updatedQuote = partQuote;
                updatedQuote.JiraTicketNumber = JiraTicketNum;
                _dbCon.Entry(partQuote).CurrentValues.SetValues(updatedQuote);
                _dbCon.SaveChanges();

                if (isGSOnly)
                {
                    tblPartQuoteTracking quoteTracking = new Entity.tblPartQuoteTracking();
                    quoteTracking.PartQuoteID = quoteId;
                    quoteTracking.Comments = comments.Trim();
                    quoteTracking.LastUpdatedByUserID = sessionUserId;
                    quoteTracking.LastUpdated = DateTime.Now;
                    quoteTracking.GSComments = true;
                    quoteTracking.PartQuoteStatusID = partQuote.PartQuoteStatusID;
                    quoteTracking.QuoteActionName = "Add Comment";
                    _dbCon.tblPartQuoteTrackings.Add(quoteTracking);
                    count = _dbCon.SaveChanges();
                }
                else
                {
                    tblPartQuoteTracking quoteTracking = new Entity.tblPartQuoteTracking();
                    quoteTracking.PartQuoteID = quoteId;
                    quoteTracking.Comments = comments.Trim();
                    quoteTracking.LastUpdated = DateTime.Now;
                    quoteTracking.LastUpdatedByUserID = sessionUserId;
                    _dbCon.tblPartQuoteTrackings.Add(quoteTracking);
                    count = _dbCon.SaveChanges();
                    if (count > 0)
                    {
                        var quoteStatus = _dbCon.tblPartQuoteStatus.FirstOrDefault(x => x.PartQuoteStatusID == partQuote.PartQuoteStatusID);
                        if (quoteStatus != null)
                        {
                            QuoteCommentEmail(quoteId, sessionUserId, "old", comments, quoteStatus.PartQuoteStatus, IsUrgent);
                        }
                    }
                }

                return count;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>AddOldQuoteComment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>AddOldQuoteComment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>AddOldQuoteComment");

                }
                return -1;
            }
        }

        /// <summary>
        /// this method use for email for quote comment
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sessionUserId"></param>
        /// <param name="quoteType"></param>
        /// <param name="comments"></param>
        /// <param name="quoteStatus"></param>
        /// <returns></returns>
        public int QuoteCommentEmail(int quoteId, int sessionUserId, string quoteType, string comments, string quoteStatus, bool IsUrgent = false)
        {
            try
            {
                string MissingEmail = string.Empty;
                string mailbody = string.Empty;
                string mailsubject = string.Empty;
                string mailfrom = string.Empty;
                string mailto = string.Empty;
                string mailcc = string.Empty;
                string mailbcc = string.Empty;
                string userName = string.Empty;
                int CustomerId;
                int CountryId;
                int SFAID;

                var primecontractor = _dbCon.tblPrimeContractors.FirstOrDefault(x => x.PrimeContractorName == "GlobalServe" && x.IsActive == 1);
                if (primecontractor != null)
                {
                    MissingEmail = primecontractor.PartQuoteEmail;
                }

                var quoteCustomerInfo = _dbCon.sp_getQuoteAndCustomerDetailForEmail_AdminR(quoteId, quoteType).FirstOrDefault();
                if (quoteCustomerInfo != null)
                {
                    CustomerId = Convert.ToInt32(quoteCustomerInfo.CustomerID);
                    CountryId = Convert.ToInt32(quoteCustomerInfo.CountryID);
                    SFAID = Convert.ToInt32(quoteCustomerInfo.SFAID);

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.CustomerEmail))
                    {
                        mailfrom = quoteCustomerInfo.CustomerEmail;
                    }
                    else
                    {
                        mailfrom = MissingEmail;
                    }
                    mailbcc = mailfrom;

                    if (!string.IsNullOrEmpty(quoteCustomerInfo.UserEmail))
                    {
                        mailto = quoteCustomerInfo.UserEmail;
                    }
                    else
                    {
                        mailto = string.Empty;
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = mailto + quoteCustomerInfo.NetpQuoteEmail;
                    }
                    else
                    {
                        if (!mailto.Contains(quoteCustomerInfo.NetpQuoteEmail))
                        {
                            mailto = mailto + "; " + quoteCustomerInfo.NetpQuoteEmail;
                        }
                    }

                    if (string.IsNullOrEmpty(mailto))
                    {
                        mailto = MissingEmail;
                    }


                    if (!string.IsNullOrEmpty(quoteCustomerInfo.SupplierEmail))
                    {
                        mailcc = quoteCustomerInfo.SupplierEmail;
                    }

                    mailbody = "GSP: " + quoteCustomerInfo.SFAName + Environment.NewLine;
                    mailbody = mailbody + "Customer: " + quoteCustomerInfo.CustomerName + Environment.NewLine;
                    mailbody = mailbody + "Requested By: " + quoteCustomerInfo.UserName + Environment.NewLine + Environment.NewLine;
                    mailbody = mailbody + "Quote #         : " + quoteId + Environment.NewLine;
                    mailbody = mailbody + "Request Date    : " + quoteCustomerInfo.CreateDate + Environment.NewLine;
                    mailbody = mailbody + "Country         : " + quoteCustomerInfo.CountryName + Environment.NewLine;
                    mailbody = mailbody + "Quote Reference : " + quoteCustomerInfo.CustomerRef + Environment.NewLine;
                    mailbody = mailbody + "Status          : " + quoteStatus + Environment.NewLine;

                    mailbody = mailbody + "GlobalServe Comments : ";

                    var contact = _dbCon.tblContacts.FirstOrDefault(x => x.UserID == sessionUserId);
                    if (contact != null)
                    {
                        userName = (string.IsNullOrEmpty(contact.FirstName) ? "" : contact.FirstName) + " " + (string.IsNullOrEmpty(contact.LastName) ? "" : contact.LastName);
                        if (!string.IsNullOrEmpty(userName))
                        {
                            mailbody = mailbody + "(From: " + userName + ")" + Environment.NewLine;

                        }
                        if (string.IsNullOrEmpty(contact.Email))
                        {
                            mailcc = mailcc + contact.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(contact.Email))
                            {
                                mailcc = mailcc + "; " + contact.Email;
                            }
                        }
                    }

                    mailbody = mailbody + comments + Environment.NewLine + Environment.NewLine;



                    if (quoteCustomerInfo.CustomerID == 245)
                    {
                        mailbody = mailbody + "To view the quote request, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." + Environment.NewLine + Environment.NewLine;
                    }
                    else
                    {
                        mailbody = mailbody + "Please logon to https://gsonesource.global-serve.com to view the quote request." + Environment.NewLine + Environment.NewLine;
                    }

                    mailbody = mailbody + "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:" + Environment.NewLine;
                    mailbody = mailbody + "EMEA" + Environment.NewLine;
                    mailbody = mailbody + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                     Environment.NewLine + "fax : 201-408-5275" + Environment.NewLine;
                    mailbody = mailbody + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com" + Environment.NewLine;

                    if (IsUrgent == true)
                    {
                        mailsubject = "Escalation! - GlobalServe ONESOURCE - GlobalServe Comments for Quote Request - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;
                    }
                    else
                    {
                        mailsubject = "GlobalServe ONESOURCE - GlobalServe Comments for Quote Request - (" + quoteCustomerInfo.CustomerName + ") - " + quoteId;
                    }

                    var contacts = _dbCon.sp_GetSFACustomerContactEmails_R(quoteCustomerInfo.CustomerID, quoteCustomerInfo.SFAID, quoteCustomerInfo.CountryID).ToList();

                    foreach (var item in contacts)
                    {
                        if (string.IsNullOrEmpty(mailcc))
                        {
                            mailcc = mailcc + item.Email;
                        }
                        else
                        {
                            if (!mailcc.Contains(item.Email))
                            {
                                mailcc = mailcc + "; " + item.Email;
                            }
                        }
                    }
                    OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(mailto, mailfrom, mailcc, mailbcc, mailbody, mailsubject);

                }
                return 1;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>QuoteCommentEmail<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>QuoteCommentEmail");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>QuoteCommentEmail");

                }
                return -1;
            }

        }

        /// <summary>
        /// this method use for getting quote attachment
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public QuoteAttachmentModel GetQuoteAttachments(int quoteId, string quoteType)
        {
            try
            {
                var model = new QuoteAttachmentModel();
                model.quoteType = quoteType;
                model.Attachments = _dbCon.sp_getQuoteAttachments_AdminR(quoteId.ToString(), quoteType).ToList();
                return model;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>GetQuoteAttachments<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>GetQuoteAttachments");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>GetQuoteAttachments");
                }
                return null;
            }
        }

        public bool DeActivateQuoteAttachment(int quoteId, string quoteType, int daId)
        {
            try
            {
                var msg = _dbCon.Sp_DeActivateQuoteAttachment_R(quoteId, quoteType, daId);
                return true;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "-->>>>>Quote Search Presenter =>DeActivateQuoteAttachment<<<<<--" + ex.ToString());
                StaticHelper.LogException(ex.ToString(), "Quote Search Presenter =>DeActivateQuoteAttachment");
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry("Application", "------INNER EX:" + ex.InnerException.ToString());
                    StaticHelper.LogException(ex.InnerException.ToString(), "Quote Search Presenter =>DeActivateQuoteAttachment");
                }
                return false;
            }
        }

        /// <summary>
        /// this method use for adding digital asset
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="docId"></param>
        /// <param name="fileExt"></param>
        /// <param name="fullPath"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="quoteId"></param>
        /// <param name="daid"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public int? AddDigitalAsset(string fileName, string docId, string fileExt, string fullPath, string url, int userId, int quoteId, int? daid, string quoteType)
        {
            try
            {
                if (quoteType == "new")
                {
                    return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                          null, null, null, null, null, null, null, null, quoteId, null
                          , null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
                }
                else
                {
                    return _dbCon.sp_AddDigitalAsset_R(fileName, docId, fileExt, fullPath, url, null, userId,
                          null, null, null, null, null, quoteId, null, null, null, null
                          , null, null, null, null, 0, null, 0, null, null, null, null, null).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "QuoteSearch Presenter In AddDigitalAsset Method" + ex.ToString());
                return 0;
            }
        }
        public string GetQuoteStatus(int quoteId, string quoteType)
        {
            if (quoteType == "new")
            {
                var quote = _dbCon.tblQuoteSummaries.FirstOrDefault(x => x.QuoteID == quoteId);
                if (quote != null)
                {
                    var status = _dbCon.tblQuoteStatus.FirstOrDefault(x => x.QuoteStatusID == quote.QuoteStatusID);
                    if (status != null)
                    {
                        return status.QuoteStatus;
                    }
                }
                return "New";
            }
            else
            {
                var quote = _dbCon.tblPartQuotes.FirstOrDefault(x => x.PartQuoteID == quoteId);
                if (quote != null)
                {
                    var status = _dbCon.tblPartQuoteStatus.FirstOrDefault(x => x.PartQuoteStatusID == quote.PartQuoteStatusID);
                    if (status != null)
                    {
                        return status.PartQuoteStatus;
                    }
                }
                return "New";
            }
        }


        public int GetPaymentTermDaysForQuote(int customerId, int countryId, int supplierId)
        {
            try
            {
                var query =
                    "SELECT top(1) NetPaymentDays FROM tblSFACustomerWA sfc join tblCustomerAddresses" +
                    " ca on ca.CustomerAddressID=sfc.CustomerDestinationID WHERE SFAID=" + supplierId + " AND " +
                    " sfc.CustomerID=" + customerId + " and  sfc.isActive=1 and offeringid=2 and ca.CountryID=" + countryId;
                var results = _dbCon.Database.SqlQuery<int>(query).FirstOrDefault();

                return results;

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "GetPaymentTermDaysForQuote" + ex.ToString());
                return 0;
            }
        }

        public string GetResellerAddress(int countryId, int supplierId)
        {
            try
            {
                int partyId = 0;
                var query =
                    " select partyid From tblSFAs sfa join tblParentCompanies pc  on pc.ParentCompanyID=sfa.ParentCompanyID where sfa.SFAID =" + supplierId;
                var results = _dbCon.Database.SqlQuery<int>(query).FirstOrDefault();

                partyId = results;

                if (partyId > 0)
                {
                    var query1 =
                        "select  pa.addressname+', '+IsNull(a.address1, '-') +', '+IsNull(a.stateprovinceregion, '-') +', '+IsNull(a.postalcode ,'-')+', '+ISNULL( a.City,'-')as Address" +
                        " from tblpartyaddress pa join tblparty p on p.partyid = pa.partyid and " +
                        " pa.partyid = " + partyId +
                        " join tblAddress a on pa.addressid = a.addressid and a.isActive = 1 and a.CountryID=" +
                        countryId;
                    var results1 = _dbCon.Database.SqlQuery<string>(query1).FirstOrDefault();
                    return results1 ?? "-";
                }
                else
                {
                    return "-";
                }

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", "GetResellerAddress" + ex.ToString());
                return "-";


            }
        }

        public string GetJIRATicketNumber(int quoteId, string quoteType)
        {
            string TicketNumber = "";

            if (quoteType == "new")
            {
                var quote = _dbCon.tblQuoteSummaries.Where(x => x.QuoteID == quoteId).FirstOrDefault();

                if (quote != null)
                {
                    TicketNumber = Convert.ToString(quote.JiraTicketNumber);
                }
            }
            else
            {
                var quote = _dbCon.tblPartQuotes.Where(x => x.PartQuoteID == quoteId).FirstOrDefault();

                if (quote != null)
                {
                    TicketNumber = Convert.ToString(quote.JiraTicketNumber);
                }
            }

            return TicketNumber;
        }


        #region Bundle Details (SUNNY GOHAR)
        /// <summary>
        /// Get bundle part category id
        /// </summary>
        /// <param name="CatalogStandardId"></param>
        /// <returns></returns>
        public int GetPartCategoryId(int CatalogStandardId)
        {
            try
            {
                var query1 = "select PartCategoryID from tblCatalogStandards where CatalogStandardID=" + CatalogStandardId;
                var catalogPartCategoryId = _dbCon.Database.SqlQuery<int>(query1).FirstOrDefault();
                return catalogPartCategoryId;

            }
            catch (Exception ex)
            {

                StaticHelper.LogException(ex.ToString(), "GetPartCategoryId");
                if (ex.InnerException != null)
                {
                    StaticHelper.LogException(ex.InnerException.ToString(), "GetPartCategoryId");
                }
                return 0;
            }
        }

        public List<sp_GetBundleDescription_R_Result> GetBundleDescription(int catalogStandardId)
        {
            return _dbCon.sp_GetBundleDescription_R(catalogStandardId).ToList();
        }

        public List<sp_GetPartsByBundleId_R_Result> GetPartsByBundleId(int catalogStandardId, int partCategoryId, int customerId, int countryId)
        {
            return _dbCon.sp_GetPartsByBundleId_R(partCategoryId, catalogStandardId, customerId, countryId).ToList();
        }

        public List<sp_GetBundleCatalogInfo_R_Result> GetBundleCatalogInfo(int customerId, int catalogStandardId, int countryId)
        {
            var a = _dbCon.sp_GetBundleCatalogInfo_R(customerId, catalogStandardId, countryId).ToList();
            return a;
        }
        public object GetBundleDetail(int partCategoryId, int catalogStandardId, BundleSettings currentSettings, string BundleImageRootUrl)
        {
            var obj = GetBundleDescription(catalogStandardId).FirstOrDefault();

            int customerId = 0;
            int countryId = 0;
            int.TryParse(currentSettings.CustomerId != null ? currentSettings.CustomerId.ToString() : "", out customerId);
            int.TryParse(currentSettings.CountryId != null ? currentSettings.CountryId.ToString() : "", out countryId);
            var bundleParts = GetPartsByBundleId(catalogStandardId, partCategoryId, customerId, countryId);
            var bundleDescription = obj.Description;
            var bundleComments = obj.Comments;
            var bundleImage = GetBundleImage(partCategoryId, catalogStandardId, BundleImageRootUrl);
            var currencySymbol = _dbCon.tblCurrencies.FirstOrDefault(x => x.CurrencyID == currentSettings.CurrencyId).Symbol;

            var bundle = new
            {
                CatalogStandardId = catalogStandardId,
                PartCategoryId = partCategoryId,
                Parts = bundleParts,
                Description = bundleDescription,
                Comments = bundleComments,
                BundleImage = bundleImage,
                CurrencySymbol = currencySymbol
            };

            return bundle;
        }
        //public double CalculatePartPrice(int CustomerPartID, int CustomerID, int CatalogPartID, int DestinationCountryID, int OrderCurrencyID, int Quantity, int SFAID, int DestinationAddressID, string spName, int RoundDecimalPlaces = 2)
        //{
        //    double Price = 0;

        //    var rdr = _dbCon.sp_CalculatePartPriceQty_R(CustomerID, DestinationCountryID,
        //         CatalogPartID, OrderCurrencyID, Quantity, SFAID, CustomerPartID, DestinationAddressID).ToList();

        //    foreach (var item in rdr)
        //    {
        //        Price = Convert.ToDouble(item.AdjustedPrice);
        //    }

        //    return Price;
        //}

        public string GetBundleImage(int partCategoryId, int catalogStandardId, string bundleImageRootUrl)
        {
            string image = null;

            image = _dbCon.tblCatalogStandards.FirstOrDefault(x => x.CatalogStandardID == catalogStandardId).ImageFile;

            if (string.IsNullOrEmpty(image))
            {
                image = _dbCon.tblPartCategories.FirstOrDefault(x => x.PartCategoryID == partCategoryId).DefaultImage;
            }

            if (string.IsNullOrEmpty(image))
            {
                //NoImage
                image = "I01881.jpg";
            }

            image = string.Format("{0}/{1}", bundleImageRootUrl, image);

            return image;
        }
        //public decimal CalculatePrice_Bundle(int catalogStandardId, int customerId, int quantity, double price, Settings currentSettings)
        //{
        //    Int32 customerPartId = 0;
        //    Int32 destinationCountryId = 0;
        //    Int32 orderCurrencyId = 0;
        //    Int32 destinationAddressId = 0;
        //    int catalogPartId = 0;
        //    bool isOption = false;


        //    var bundleCatalog = GetBundleCatalogInfo(currentSettings.CustomerId, catalogStandardId, currentSettings.CountryId);

        //    if (bundleCatalog != null)
        //    {
        //        price = 0;

        //    }

        //    destinationCountryId = currentSettings.CountryId;
        //    orderCurrencyId = currentSettings.CurrencyId;
        //    destinationAddressId = currentSettings.DestinationId;

        //    if (bundleCatalog != null)
        //    {
        //        foreach (var item in bundleCatalog)
        //        {
        //            catalogPartId = item.CatalogPartID;
        //            quantity = item.Quantity;
        //            isOption = item.IsOption;
        //            customerPartId = item.CustomerPartID;

        //            if (!item.IsOption)
        //            {
        //                price = price + CalculatePartPrice(customerPartId, customerId, catalogPartId, destinationCountryId, orderCurrencyId, quantity, 0, destinationAddressId, "");
        //            }

        //        }

        //    }

        //    return Convert.ToDecimal(Math.Round(price, 2));
        //}

        #endregion
    }
}

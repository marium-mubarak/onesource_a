﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Models
{
    public class ITServicesLineItemModel
    {
        [Required(ErrorMessage = "Please enter number of resources")]
        public int NumberofResources { get; set; }       
        [Required(ErrorMessage = "Please enter detailed scope of work")]
        public string ScopeofWork { get; set; }
        [Required(ErrorMessage = "Please enter hardware equipment details")]
        public string EquipmentDetails { get; set; }
        [Required(ErrorMessage = "Please select service type", AllowEmptyStrings = false)]
        public string ServiceTypeId { get; set; }
        [Required(ErrorMessage = "Please select sla", AllowEmptyStrings = false)]
        public string SALId { get; set; }
        [Required(ErrorMessage = "Please select engineer availability", AllowEmptyStrings = false)]
        public string EngineerAvailability { get; set; }

        public List<SelectListItem> ServiceTypeList { get; set; }
        public List<SelectListItem> SLAList { get; set; }
        public List<SelectListItem> EngineerAvailablityList { get; set; }

        public ITServicesLineItemModel()
        {
            var _serviceType = new List<SelectListItem>();
            var _sla = new List<SelectListItem>();
            var _engineerava = new List<SelectListItem>();

            _serviceType.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _sla.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });

            _engineerava.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _engineerava.Add(new SelectListItem { Value = "1", Text = "Dispatch Onsite", Selected = false });
            _engineerava.Add(new SelectListItem { Value = "2", Text = "FTE (Full time Engineer)", Selected = false });

            EngineerAvailablityList = _engineerava;

            ITServiceQuotePresenter iqoute = new ITServiceQuotePresenter();

            var serviceType = iqoute.GetServiceType();

            foreach (var item in serviceType.OrderBy(x => x.ServiceTypeName))
            {
                _serviceType.Add(new SelectListItem { Value = item.ServiceTypeId.ToString(), Text = item.ServiceTypeName });
            }

            ServiceTypeList = _serviceType;

            var sla = iqoute.GetSLA();

            foreach (var item in sla)
            {
                _sla.Add(new SelectListItem { Value = item.SLATypeId.ToString(), Text = item.SLAName });
            }

            SLAList = _sla;

        }
    }
}
﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class HardwareResponseModel
    {
        public OneSourceAdminCore.Model.HardwareQuoteModel HardwareQuotes { get; set; }
        public List<tblQuoteLineItem> LineItemes { get; set; }
        public tblHardwareQuoteResponse HardwareQuoteResponse { get; set; }
        public List<tblResponseQuoteLineItem> ResponseLineItemes { get; set; }
        public List<GenericList> Manufacturers { get; set; }
        public List<tblPartCategory> Category { get; set; }
        public List<ResponseLineItemModel> EntryLineModel { get; set; }
        public List<tblCurrency> CurrencyList { get; set; }
        public HardwareResponseModel()
        {
            EntryLineModel = new List<ResponseLineItemModel>();
            HardwareQuotes = new OneSourceAdminCore.Model.HardwareQuoteModel();
            LineItemes = new List<tblQuoteLineItem>();
            HardwareQuoteResponse = new tblHardwareQuoteResponse();
            ResponseLineItemes = new List<tblResponseQuoteLineItem>();
            Manufacturers = new List<GenericList>();
            Category = new List<tblPartCategory>();
            EntryLineModel = new List<ResponseLineItemModel>();
            CurrencyList = new List<tblCurrency>();
        }
    }
}
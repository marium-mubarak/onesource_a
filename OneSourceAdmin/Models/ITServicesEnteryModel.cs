﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class ITServicesEnteryModel
    {
        public ITServicesLineItemModel LineItems { get; set; }
        public ITServiceModel ServiceModel { get; set; }

    }
}
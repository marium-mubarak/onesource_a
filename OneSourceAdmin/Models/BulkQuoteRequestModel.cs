﻿
using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class BulkQuoteRequestModel
    {
        public string CustomerRef { get; set; }
        public int CustomerID { get; set; }
        public int CurrencyID { get; set; }

        public int CountryID { get; set; }
        public int  CustomerUserID { get; set; }
        public int SupplierID { get; set; }
        public int IsdUserID { get; set; }

        public DataTable ErrorDataTable { get; set; }
        public DataTable UploadDataTable { get; set; }

        public List<GenericList> CustomerList  { get; set; }
    }
}
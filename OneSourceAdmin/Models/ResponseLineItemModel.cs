﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Models
{
    public class ResponseLineItemModel
    {
        public int ResponseQuoteId { get; set; }
        public int ResonseQuoteType { get; set; }
        public int ManfuacturerId { get; set; }
        public string SKU { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { get; set;}
        public double UnitCost { get; set; }
        public double FinalPrice { get; set; }
        public double Tax { get; set; }
        public double PriceWithTax { get; set; }
        public double GSPMargin { get; set; }

        //public List<SelectListItem> ManufactrerList { get; set; }
        //public List<SelectListItem> CategoryList { get; set; }

        //public ResponseLineItemModel()
        //{
        //    var _manufactrer = new List<SelectListItem>();
        //    var _category = new List<SelectListItem>();

        //    OEMPresenter _oem = new OEMPresenter();
        //    HardwareResponsePresenter _hardwareresponse = new HardwareResponsePresenter();
        //    _manufactrer.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
        //    _category.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });

        //    foreach (var item in _oem.GetManufacturers())
        //    {
        //        _manufactrer.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value, Selected = false });
        //    }

        //    ManufactrerList = _manufactrer;

        //    var category = _hardwareresponse.GetCategory().Where(x => x.IsActive == true).OrderBy(x => x.PartCategoryName).ToList();

        //    foreach (var item in category)
        //    {
        //        _category.Add(new SelectListItem { Value = item.ParentCategoryID.ToString(), Text = item.PartCategoryName, Selected = false });
        //    }

        //    CategoryList = _category;
        //}

    }    

    public class ResponseLineItemsEntry
    {
        public List<ResponseLineItemModel> LineItmes { get; set; }

        public ResponseLineItemsEntry()
        {
            LineItmes = new List<ResponseLineItemModel>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class AssetDisposalLineItem
    {
        [Required(ErrorMessage = "Please enter Quantity")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Please enter description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter category")]
        public string Category { get; set; }
        
    }
}
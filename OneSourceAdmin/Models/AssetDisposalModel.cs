﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Models
{
    public class AssetDisposalModel
    {
        public string AssetDisposalId { get; set; }

        [Required(ErrorMessage = "Please enter customer name")]
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "Please enter pick-up address")]
        public string PickAddress { get; set; }
        [Required(ErrorMessage = "Please enter account manager name")]
        public string AccountManagerName { get; set; }
       
        //[Required(ErrorMessage = "Please enter pick-up contact")]
        public string PickupContact { get; set; }
        //[Required(ErrorMessage = "Please enter invoice address")]
        public string InvoiceAddress { get; set; }
        //[Required(ErrorMessage = "Please enter invoice contact")]
        public string InvoiceContact { get; set; }
        [Required(ErrorMessage = "Please select country", AllowEmptyStrings = false)]
        public string CountryId { get; set; }
        //[Required(ErrorMessage = "Please select currency", AllowEmptyStrings = false)]
        public string CurrencyId { get; set; }
        [Required(ErrorMessage = "Please enter city")]
        public string City { get; set; }
        //[Required(ErrorMessage = "Please enter project timeline")]
        public string ProjectTimeLine { get; set; }
        
        public string ExpectedPricePoint { get; set; }
        [Required(ErrorMessage = "Please enter quantity")]
        public int Quantity { get; set; }
        [Required(ErrorMessage = "Please enter description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Please enter category")]
        public string Category { get; set; }
        //[Required(ErrorMessage = "Please enter comments")]
        public string Comments { get; set; }
        //[Required(ErrorMessage = "Please select supplier", AllowEmptyStrings = false)]
        public string SupplierID { get; set; }

        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        //public List<SelectListItem> SupplierList { get; set; }

        public AssetDisposalModel()
        {
            var _country = new List<SelectListItem>();
            var _currency = new List<SelectListItem>();
            //var _suppliers = new List<SelectListItem>();
           

            HQRequestPresenter hp = new HQRequestPresenter();
            //SFAsPresenter sfas = new SFAsPresenter();

            var _countrylist = hp.Countries();
            var _currencylist = hp.Currencies();
            //var _supplierlist = sfas.GetSFAs();

            _country.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _currency.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            //_suppliers.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });

            foreach (var item in _countrylist.OrderBy(x => x.CountryName))
            {
                _country.Add(new SelectListItem { Value = item.CountryID.ToString(), Text = item.CountryName });
            }

            CountryList = _country;
            var currencySymbol = string.Empty;

            foreach (var item in _currencylist.OrderBy(x => x.CurrencyName))
            {
                _currency.Add(new SelectListItem { Value = item.CurrencyID.ToString(), Text = string.Format("{0} ({1})", item.CurrencyName, item.Symbol) });
            }

            CurrencyList = _currency;

            //foreach (var item in _supplierlist.OrderBy(x => x.SFAName))
            //{
            //    _suppliers.Add(new SelectListItem { Value = item.SFAID.ToString(), Text = item.SFAName });
            //}

            //SupplierList = _suppliers;
        }
    }
}
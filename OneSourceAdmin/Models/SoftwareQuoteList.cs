﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class SoftwareQuoteList
    {
        public List<OneSourceAdminCore.Model.SoftwareQuoteModel> SoftwareQList { get; set;}
        public List<tblSFA> Suppliers { get; set; }
    }
}
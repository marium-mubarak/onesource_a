﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Models
{
    public class ITServiceModel
    {
        public string ITServiceQuoteId { get; set; }

        [Required(ErrorMessage = "Please enter customer name")]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Please Enter Account Manager Name")]
        public string AccountManagerName { get; set; }

        [Required(ErrorMessage = "Please enter site address")]
        public string SiteAddres { get; set; }
        //[Required(ErrorMessage = "Please enter site contact")]
        public string SiteContact { get; set; }
        //[Required(ErrorMessage = "Please enter invoice address")]
        public string InvoiceAddress { get; set; }
        //[Required(ErrorMessage = "Please enter invoice contact")]
        public string InvoiceContact { get; set; }
        [Required(ErrorMessage = "Please select country", AllowEmptyStrings =false)]
        public string CountryId { get; set; }
        //[Required(ErrorMessage = "Please select currency", AllowEmptyStrings = false)]
        public string CurrencyId { get; set; }
        [Required(ErrorMessage = "Please enter city")]
        public string City { get; set; }
        //[Required(ErrorMessage = "Please enter project name")]
        public string ProjectName { get; set; }
        //[Required(ErrorMessage = "Please enter project start date")]
        public string ProjectStartDate { get; set; }
        //[Required(ErrorMessage = "Please enter project expected duration")]
        public string ExpectedDuration { get; set; }
        //[Required(ErrorMessage = "Please enter expected rates point")]
        public string ExpectedRatePoint { get; set; }
        [Required(ErrorMessage = "Please enter number of resources")]
        public int NumberofResources { get; set; }
        //[Required(ErrorMessage = "Please enter project overview")]
        public string Overview { get; set; }
        [Required(ErrorMessage = "Please enter detailed scope of work")]
        public string ScopeofWork { get; set; }
        [Required(ErrorMessage = "Please enter hardware equipment details")]
        public string EquipmentDetails { get; set; }
        [Required(ErrorMessage = "Please select service type", AllowEmptyStrings =false)]
        public string ServiceTypeId { get; set; }
        [Required(ErrorMessage = "Please select sla", AllowEmptyStrings = false)]
        public string SALId { get; set; }
        [Required(ErrorMessage = "Please select engineer availability", AllowEmptyStrings = false)]
        public string EngineerAvailability { get; set; }
        [Required(ErrorMessage = "Please select rates", AllowEmptyStrings = false)]
        public string Rates { get; set; }
        //[Required(ErrorMessage = "Please select supplier", AllowEmptyStrings = false)]
        public string SupplierID { get; set; }
        public string Comments { get; set; }

        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public List<SelectListItem> ServiceTypeList { get; set; }
        public List<SelectListItem> SLAList { get; set; }
        public List<SelectListItem> EngineerAvailablityList { get; set; }
        public List<SelectListItem> RateList { get; set; }
        //public List<SelectListItem> SupplierList { get; set; }

        public ITServiceModel()
        {
            var _country = new List<SelectListItem>();
            var _currency = new List<SelectListItem>();
            var _serviceType = new List<SelectListItem>();
            var _sla = new List<SelectListItem>();
            var _engineerava = new List<SelectListItem>();
            var _rate = new List<SelectListItem>();
            //var _suppliers = new List<SelectListItem>();

            HQRequestPresenter hp = new HQRequestPresenter();
            //SFAsPresenter sfas = new SFAsPresenter();

            var _countrylist = hp.Countries();
            var _currencylist = hp.Currencies();
            //var _supplierlist = sfas.GetSFAs();

            _country.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _currency.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _serviceType.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _sla.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _rate.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            //_suppliers.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });

            _engineerava.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _engineerava.Add(new SelectListItem { Value = "1", Text = "Dispatch Onsite", Selected = false });
            _engineerava.Add(new SelectListItem { Value = "2", Text = "FTE (Full time Engineer)", Selected = false });

            EngineerAvailablityList = _engineerava;

            foreach (var item in _countrylist.OrderBy(x => x.CountryName))
            {
                _country.Add(new SelectListItem { Value = item.CountryID.ToString(), Text = item.CountryName });
            }

            CountryList = _country;
            var currencySymbol = string.Empty;

            foreach (var item in _currencylist.OrderBy(x => x.CurrencyName))
            {
                _currency.Add(new SelectListItem { Value = item.CurrencyID.ToString(), Text = string.Format("{0} ({1})", item.CurrencyName, item.Symbol) });
            }

            CurrencyList = _currency;

            ITServiceQuotePresenter iqoute = new ITServiceQuotePresenter();

            var serviceType = iqoute.GetServiceType();

            foreach (var item in serviceType.OrderBy(x => x.ServiceTypeName))
            {
                _serviceType.Add(new SelectListItem { Value = item.ServiceTypeId.ToString(), Text = item.ServiceTypeName });
            }

            ServiceTypeList = _serviceType;

            var sla = iqoute.GetSLA();

            foreach (var item in sla)
            {
                _sla.Add(new SelectListItem { Value = item.SLATypeId.ToString(), Text = item.SLAName });
            }

            SLAList = _sla;

            var rate = iqoute.GetRates();

            foreach (var item in rate)
            {
                _rate.Add(new SelectListItem { Value = item.RateId.ToString(), Text = item.ReteType });
            }

            RateList = _rate;

            //foreach (var item in _supplierlist.OrderBy(x => x.SFAName))
            //{
            //    _suppliers.Add(new SelectListItem { Value = item.SFAID.ToString(), Text = item.SFAName });
            //}

            //SupplierList = _suppliers;

        }


    }
}
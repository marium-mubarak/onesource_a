﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class ITServiceListModel
    {
        public List<OneSourceAdminCore.Model.ITServiceQuoteModel> ITServiceList { get; set; }
        public List<tblSFA> Suppliers { get; set; }
        public List<tblSLAType> SLATypes { get; set; }
        public List<tblServiceType> ServiceType { get; set; }
    }
}
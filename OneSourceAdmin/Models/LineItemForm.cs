﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class LineItemForm
    {
        [Required(ErrorMessage = "Please enter Quantity")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Please enter description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter manufacturer")]
        public string Manufacturer { get; set; }       
        public string SKU { get; set; }
        public string SerailNumber { get; set; }
    }
}
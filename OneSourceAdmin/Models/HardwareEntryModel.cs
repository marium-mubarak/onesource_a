﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class HardwareEntryModel
    {
        public LineItemForm LineItems { get; set; }
        public HardwareQuoteModel QuoteModel { get; set; }
    }
}
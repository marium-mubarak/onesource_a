﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class SoftwareEntryModel
    {
        public LineItemForm LineItems { get; set; }
        public SoftwareQuoteModel SoftwareQuote { get; set; }
    }
}
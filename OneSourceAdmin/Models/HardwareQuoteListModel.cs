﻿using OneSourceAdminCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class HardwareQuoteListModel
    {
        public List<OneSourceAdminCore.Model.HardwareQuoteModel> HardwareQuotes { get; set; }
        public List<tblSFA> Suppliers { get; set; }
    }
}
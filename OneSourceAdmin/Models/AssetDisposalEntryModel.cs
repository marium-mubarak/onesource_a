﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class AssetDisposalEntryModel
    {
        public AssetDisposalLineItem LineItems { get; set; }
        public AssetDisposalModel AssetDisposalQuote { get; set; }
    }
}
﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Models
{
    public class HardwareQuoteModel
    {
        public int HarwareQuoteId { get; set; }

        [Required(ErrorMessage = "Please Enter Customer Name")]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Please Enter Account Manager Name")]
        public string AccountManagerName { get; set; }

        [Required(ErrorMessage = "Please Enter Delivery Address")]
        public string DeliveryAddress { get; set; }

        //[Required(ErrorMessage = "Please Enter Invoice Address")]
        public string InvoiceAddress { get; set; }

        //[Required(ErrorMessage = "Please select currency", AllowEmptyStrings = false)]
        public string CurrencyId { get; set; }

        [Required(ErrorMessage = "Please select country", AllowEmptyStrings = false)]
        public string CountryId { get; set; }
        //[Required(ErrorMessage = "Please select supplier", AllowEmptyStrings = false)]
        public string SupplierID { get; set; }

        [Required(ErrorMessage = "Please enter city")]
        public string City { get; set; }

        //[Required(ErrorMessage = "Please enter project timeline")]
        public string Timeline { get; set; }
        public string PricePoint { get; set; }

        [Required(ErrorMessage = "Please enter quantity")]
        public int Qunaitity { get; set; }

        [Required(ErrorMessage = "Please enter description")]
        public string Description { get; set; }
        public string Sku { get; set; }

        [Required(ErrorMessage = "Please enter manufacturer")]
        public string Manufacturer { get; set; }
        [Required(ErrorMessage = "Please select deal type", AllowEmptyStrings = false)]
        public string OptionId { get; set; }

        //[Required(ErrorMessage = "Please enter comments")]
        public string Comments { get; set; }

        public string OptionalComment { get; set; }
        public string GlobalDealId { get; set; }
        public string OEMAccountManager { get; set; }
        public string EndUserAccountDetails { get; set; }


        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public List<SelectListItem> OptionList { get; set; }
        //public List<SelectListItem> SupplierList { get; set; }

        public HardwareQuoteModel()
        {
            var _country = new List<SelectListItem>();
            var _currency = new List<SelectListItem>();
            var _options = new List<SelectListItem>();
            //var _suppliers = new List<SelectListItem>();

            HQRequestPresenter hp = new HQRequestPresenter();
            SFAsPresenter sfas = new SFAsPresenter();
            var _countrylist = hp.Countries();
            var _currencylist = hp.Currencies();
            var _optionlist = hp.HQOptions();
            //var _supplierlist = sfas.GetSFAs();

            _country.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            _currency.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });
            //_suppliers.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = true });

            foreach (var item in _countrylist.OrderBy(x => x.CountryName))
            {
                _country.Add(new SelectListItem { Value = item.CountryID.ToString(), Text = item.CountryName });
            }

            CountryList = _country;
            var currencySymbol = string.Empty;

            foreach (var item in _currencylist.OrderBy(x => x.CurrencyName))
            {
                _currency.Add(new SelectListItem { Value = item.CurrencyID.ToString(), Text = string.Format("{0} ({1})", item.CurrencyName, item.Symbol) });
            }

            CurrencyList = _currency;

            foreach (var item in _optionlist)
            {
                _options.Add(new SelectListItem { Value = item.HQOptionId.ToString(), Text = item.HQOptionName });
            }

            OptionList = _options;

            //foreach (var item in _supplierlist.OrderBy(S => S.SFAName))
            //{
            //    _suppliers.Add(new SelectListItem { Value = item.SFAID.ToString(), Text = item.SFAName });
            //}

            //SupplierList = _suppliers;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneSourceAdmin.Models
{
    public class TempLineItem
    {
        public int Id { get; set; }
        public int Qunantity { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string SerailNumber { get; set; }
        public string Categroy { get; set; }
        public int SLAId { get; set; }
        public string SLAType { get; set; }
        public int ServiceId { get; set; }
        public string ServiceType { get; set; }
        public string EngAv { get; set; }
        public int EngAvId { get; set; }
        public int Resources { get; set; }
        public string HardwareDetails { get; set; }
        public int QuoteTypeId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData.ModelProviders;
using System.Web.Mvc;
using System.Web.Routing;

namespace OneSourceAdmin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.LowercaseUrls = true;
            routes.MapMvcAttributeRoutes();

            //Shipping Cost Start
            routes.MapRoute(
   name: "shipping-cost",
   url: "shipping-cost",
   defaults:
       new
       {
           controller = "ShippingCost",
           action = "Index/{customerId}/{supplierId}/{categoryId}/{shippingcostId}",
           id = UrlParameter.Optional,
           customerId = UrlParameter.Optional,
           supplierId = UrlParameter.Optional,
           categoryId = UrlParameter.Optional,
           shippingcostId = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "ShippingCost";
            //Shipping Cost End

            #region SOC Shifts

            routes.MapRoute(
   name: "socshifts",
   url: "soc-shifts",
   defaults:
       new
       {
           controller = "SOCShift",
           action = "SOCShift"
       }
   ).DataTokens["AuthFilter"] = "SOCShifts";

            #endregion

            #region Software Quotes


            routes.MapRoute(
   name: "softwarequotes",
   url: "search/software-quotes",
   defaults:
       new
       {
           controller = "SoftwareQuotes",
           action = "SoftwareQuotes"
       }
   ).DataTokens["AuthFilter"] = "SoftwareQuotes";


            #endregion

            #region AssetDisposition

            routes.MapRoute(
   name: "Asset-Disposition-Dashboard",
   url: "asset-disposition-dashboard",
   defaults:
       new
       {
           controller = "AssetDisposition",
           action = "Dashboard"
       }
   ).DataTokens["AuthFilter"] = "AssetDisposition";

            routes.MapRoute(
     name: "Asset-Disposition",
     url: "Reports/AssetDisposition/{id}",
     defaults:
         new
         {
             controller = "AssetDisposition",
             action = "Report",
             id = UrlParameter.Optional
         }
     ).DataTokens["AuthFilter"] = "AssetDisposition";


            #endregion

            #region Price Desk

            routes.MapRoute(
   name: "Pricedesk-Reports",
   url: "Reports/PriceDesk/{id}/{fromBack}",
   defaults:
       new
       {
           controller = "PriceDeskReports",
           action = "PriceDeskReport",
           id = UrlParameter.Optional,
           fromBack = UrlParameter.Optional,
       }
   ).DataTokens["AuthFilter"] = "PriceDeskReports";


            #endregion


            //smbquotes Start

            routes.MapRoute(
   name: "smbquotes",
   url: "smb-quotes",
   defaults:
       new
       {
           controller = "SMBQuotes",
           action = "SMBQuotes",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "SMBQuotes";

            //smbquotes ends


            //payment search start

            routes.MapRoute(
   name: "paymentsearch",
   url: "paymentsearch",
   defaults:
       new
       {
           controller = "Payment",
           action = "PaymentSearch",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "PaymentSearch";

            //payment search end


            //ProcurementDashboard Start

            routes.MapRoute(
   name: "report_scheduler",
   url: "report_scheduler",
   defaults:
       new
       {
           controller = "ReportScheduler",
           action = "Index",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "ReportScheduler";


            //ProcurementDashboard End


            //ProcurementDashboard Start

            routes.MapRoute(
   name: "procurement-dashboard",
   url: "procurement-dashboard",
   defaults:
       new
       {
           controller = "ProcurementDashboard",
           action = "Index",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "ProcurementDashboard";


            //ProcurementDashboard End

            //Software Settings Start

            routes.MapRoute(
   name: "upload-software-catalog",
   url: "upload-software-catalog",
   defaults:
       new
       {
           controller = "SoftwareCatalog",
           action = "UploadCatalog",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "UploadSoftwareCatalog";


            routes.MapRoute(
name: "CustomerSoftwareSettings",
url: "software/customer-settings/{customerId}/{settingId}",
defaults:
new
{
    controller = "SoftwareCatalog",
    action = "CustomersSoftwareSettings",
    customerId = UrlParameter.Optional,
    settingId = UrlParameter.Optional
}
).DataTokens["AuthFilter"] = "CustomersSoftwareSettings";




            //Software Settings End

            //AD Upload Start

            routes.MapRoute(
   name: "AD_Upload",
   url: "ad-upload",
   defaults:
       new
       {
           controller = "ADUpload",
           action = "AssetDisposition",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "ADUpload";


            //AD Upload End


            //Asset serial number uploader Start
            routes.MapRoute(
                name: "Asset_SerialNumberUpload",
                url: "asset/serial-number-upload",
                defaults:
                    new
                    {
                        controller = "Asset",
                        action = "AssetSerialNumberUploader",
                        id = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "AssetSerialNumberUploader";


            //Asset serial number uploader End

            

            //SLA Report Start

            routes.MapRoute(
   name: "SLA-Reports",
   url: "Reports/SLAReport/{id}",
   defaults:
       new
       {
           controller = "SLAReports",
           action = "Report",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "SLAReports";

            //SLA Reports End

            //Support Reports Start

            routes.MapRoute(
   name: "Support-Reports",
   url: "Reports/Support/{id}",
   defaults:
       new
       {
           controller = "SupportReports",
           action = "Report",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "SupportReports";

            //Support Reports End

            //Asset Reports Start

            routes.MapRoute(
   name: "Asset-Reports",
   url: "Reports/Asset/{id}",
   defaults:
       new
       {
           controller = "AssetReports",
           action = "Report",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "AssetReports";

            //           routes.MapRoute(
            //name: "Asset-Reports-AssetsByLocation",
            //url: "Reports/Asset/AssetsByLocation",
            //defaults:
            //    new
            //    {
            //        controller = "AssetReports",
            //        action = "GetReportData"
            //    }
            //).DataTokens["AuthFilter"] = "AssetReports";

            //Asset Reports End

            //Finance Reports Start

            routes.MapRoute(
   name: "Finance-Reports-index",
   url: "Reports/Finance-Report",
   defaults:
       new
       {
           controller = "FinanceReports",
           action = "Index"
       }
   ).DataTokens["AuthFilter"] = "FinanceReports";


            //Finance Reports End


            routes.MapRoute(
name: "supplierNameDetail",
url: "locale-settings/supplier-name-details",
defaults:
new
{
    controller = "SupplierSettings",
    action = "SupplierRoleName",
    back = UrlParameter.Optional
}
).DataTokens["AuthFilter"] = "supplierNameDetails";

            //Catalog Reports

            routes.MapRoute(
  name: "Catalog-Reports",
  url: "Reports/Catalog/{id}",
  defaults:
      new
      {
          controller = "CatalogReports",
          action = "Index",
          id = UrlParameter.Optional
      }
  ).DataTokens["AuthFilter"] = "CatalogReports";
            #region Catalog-Reports-Expired-Products by Adnan on 18-june-2019


            routes.MapRoute(
 name: "Catalog-Reports-Expired-Products",
 url: "Reports/Catalog/{id}",
 defaults:
     new
     {
         controller = "CatalogReports",
         action = "Index",
         id = UrlParameter.Optional
     }
 ).DataTokens["AuthFilter"] = "CatalogReportExpiredProduct";
            #endregion

            #region OEM Wise customer product report by Adnan on 19-june-2019


            routes.MapRoute(
 name: "Catalog-Reports-OEM-Wise-Customer-Product",
 url: "Reports/Catalog/{id}",
 defaults:
     new
     {
         controller = "CatalogReports",
         action = "Index",
         id = UrlParameter.Optional
     }
 ).DataTokens["AuthFilter"] = "OEMWiseCustomerProduct";
            #endregion
            //Catalog Reports end



            //ProcurementReports Start


            routes.MapRoute(
   name: "Procurement-Reports",
   url: "Reports/Procurement/{id}",
   defaults:
       new
       {
           controller = "ProcurementReports",
           action = "ProcurementReport",
           id = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "ProcurementReports";

            routes.MapRoute(
   name: "Procurement-Reports-PreferredSupplierByCountry",
   url: "Reports/Procurement-PreferredSupplierByCountry-Report",
   defaults:
       new
       {
           controller = "ProcurementReports",
           action = "PreferredSupplierByCountry"
       }
   ).DataTokens["AuthFilter"] = "ProcurementReports";

            routes.MapRoute(
                name: "Procurement-PartDetail-Report",
                url: "Reports/Procurement-PartDetail-Report",
                defaults:
                new
                {
                    controller = "ProcurementReports",
                    action = "PartDetailReport"
                }
            ).DataTokens["AuthFilter"] = "PartDetailReport";

            routes.MapRoute(
                name: "Shipped-Orders",
                url: "Reports/Shipped-Orders",
                defaults:
                new
                {
                    controller = "ProcurementReports",
                    action = "ShippedOrdersReport"
                }
            ).DataTokens["AuthFilter"] = "ShippedOrdersReport";

            routes.MapRoute(
  name: "Customer-Orders-Report",
  url: "Reports/Customer-Orders-Report",
  defaults:
      new
      {
          controller = "ProcurementReports",
          action = "CustomerOrdersReport"
      }
  ).DataTokens["AuthFilter"] = "CustomerOrdersReport";

            // Added by Adnan 13-june-19
            routes.MapRoute(
                        name: "Procurement-Addresses-By-Customer",
                        url: "reports/addresses-by-customer",
                        defaults:
                        new
                        {
                            controller = "ProcurementReports",
                            action = "AddressesByCustomer"
                        }
                    ).DataTokens["AuthFilter"] = "AddressesByCustomerReport";

            //

            //ProcurementReports End

            //Customer Address Report Start
            routes.MapRoute(
              name: "Customer-Address-Report",
              url: "Reports/CustomerAddress-Report",
              defaults:
                  new
                  {
                      controller = "CustomerAddressReport",
                      action = "Index"
                  }
              ).DataTokens["AuthFilter"] = "CustomerAddressReport";

            //Customer Address Report End

            //Quote Summary Report Start
            routes.MapRoute(
              name: "Quote-Summary-Report",
              url: "Reports/QuoteSummary-Report",
              defaults:
                  new
                  {
                      controller = "ProcurementReports",
                      action = "QuoteSummaryReport"
                  }
              ).DataTokens["AuthFilter"] = "QuoteSummaryReport";

            //Quote Summary Report End

            //ISD Amount Spent By User Report Start
            routes.MapRoute(
              name: "ISD-AmountSpentByUser-Report",
              url: "reports/isdamountspentbyuser-report",
              defaults:
                  new
                  {
                      controller = "ProcurementReports",
                      action = "CustomerSpendingReport"
                  }
              ).DataTokens["AuthFilter"] = "ISDAmountSpendByUserReport";

            //ISD Amount Spent By User Report End




            //Test Report Prototype Start

            routes.MapRoute(
                   name: "dashboard-submenu",
                   url: "dashboard-submenu/{id}",
                   defaults:
                       new
                       {
                           controller = "Report",
                           action = "Index"
                       }
                   ).DataTokens["AuthFilter"] = "dashboardsubmenu";

            //Test Report Prototype End


            //Invoices start

            routes.MapRoute(
   name: "invoice-search",
   url: "finance/invoice-search",
   defaults:
       new
       {
           controller = "Invoice",
           action = "Index",
           back = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "invoices";



            routes.MapRoute(
name: "invoice-list",
url: "finance/invoices",
defaults:
new
{
    controller = "Invoice",
    action = "GetInvoiceList",
    search = UrlParameter.Optional
}
).DataTokens["AuthFilter"] = "invoices";



            routes.MapRoute(
name: "invoice-detail",
url: "finance/invoices/details/{invoiceId}",
defaults:
new
{
    controller = "Invoice",
    action = "InvoiceDetails",
    invoiceId = UrlParameter.Optional
}
).DataTokens["AuthFilter"] = "invoices";

            routes.MapRoute(
name: "invoice-wizard",
url: "finance/invoice-wizard",
defaults:
new
{
    controller = "Invoice",
    action = "InvoiceWizard",
    back = UrlParameter.Optional
}
).DataTokens["AuthFilter"] = "invoices";

            //Invoices end



            //services start

            routes.MapRoute(
   name: "service-list",
   url: "support/services",
   defaults:
       new
       {
           controller = "Services",
           action = "Index",
           search = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "services";


            //services end

            //Scoreboard start

            routes.MapRoute(
   name: "scoreboard-list",
   url: "support/scoreboard",
   defaults:
       new
       {
           controller = "Scoreboard",
           action = "Index",
           search = UrlParameter.Optional
       }
   ).DataTokens["AuthFilter"] = "scoreboard";


            //scoreboard end

            //Support Workflow start


            routes.MapRoute(
         name: "support-workflow",
         url: "support/workflow/{search}",
         defaults:
             new
             {
                 controller = "Workflows",
                 action = "Index",
                 search = UrlParameter.Optional
             }
         ).DataTokens["AuthFilter"] = "supportWorkflow";

            //Support Workflow end



            //Support Actions  start


            routes.MapRoute(
                name: "support-actions",
                url: "support/actions",
                defaults:
                    new
                    {
                        controller = "Actions",
                        action = "Index",

                    }
                ).DataTokens["AuthFilter"] = "supportActions";


            routes.MapRoute(
                name: "support-actions-new",
                url: "support/actions/update",
                defaults:
                    new
                    {
                        controller = "Actions",
                        action = "NewAction",
                    }
                ); //.DataTokens["AuthFilter"] = "supportActions";

            //Support Actions End


            //Customer Catalog start

            routes.MapRoute(
                name: "customer-Catalog-crud",
                url: "admin/customer/catalog/{companyId}/{partyId}/{customerId}/{partId}/{priceCode}",
                defaults:
                    new
                    {
                        controller = "Company",
                        action = "CustomerCatalogAddEdit",
                        companyId = 0,
                        partyId = 0,
                        customerId = 0,
                        partId = 0,
                        priceCode = 0
                    }
                ); //.DataTokens["AuthFilter"] = "customerCatalog^add";


            routes.MapRoute(
                name: "customer-Catalog-countrified-crud",
                url:
                    "admin/customer/catalog/countrified/{companyId}/{partyId}/{customerId}/{partId}/{priceCode}/{countryId}",
                defaults:
                    new
                    {
                        controller = "Company",
                        action = "CustomerCatalogCountrifyPartAddEdit",
                        companyId = 0,
                        partyId = 0,
                        customerId = 0,
                        partId = 0,
                        priceCode = 0,
                        countryId = 0,
                    }
                ); //.DataTokens["AuthFilter"] = "customerCatalog^add";



            routes.MapRoute(
                name: "customer-bundle-crud",
                url: "admin/customer/bundle/{companyId}/{partyId}/{customerId}/{catalogStdId}/{priceCode}",
                defaults:
                    new
                    {
                        controller = "Company",
                        action = "CustomerBundleCatalogAddEdit",
                        companyId = 0,
                        partyId = 0,
                        customerId = 0,
                        catalogStdId = 0,
                        priceCode = 0
                    }
                ); //.DataTokens["AuthFilter"] = "customerCatalog^add";


            routes.MapRoute(
    name: "customer-bundlecategory-sort",
    url: "admin/customer/bundlecategorysort/{companyId}/{partyId}/{customerId}",
    defaults:
        new
        {
            controller = "Company",
            action = "BundleCategorySort",
            customerId = 0
        }
    ); //.DataTokens["AuthFilter"] = "customerCatalog^add";

            //Customer Catalog End

            //Search Start            

            #region Order Search Start

            routes.MapRoute(
                name: "OrderSearch",
                url: "search-order/{customerId}/{countryId}",
                defaults: new { controller = "OrderSearch", action = "OrderSearch", customerId = 0, countryId = 0 }
                ).DataTokens["AuthFilter"] = "OrderSearch";

            routes.MapRoute(
                name: "SearchOrderList",
                url: "searched-order-list/{requiredRecord}/{requiredPageSize}/{StracNoToDelFromList}",
                defaults:
                    new
                    {
                        controller = "OrderSearch",
                        action = "SearchOrderList",
                        requiredRecord = 500,
                        requiredPageSize = 50,
                        StracNoToDelFromList = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "OrderSearch";

            routes.MapRoute(
                name: "OrderDetail",
                url: "order-detail/{sessionTrackingNo}/{tabId}",
                defaults:
                    new
                    {
                        controller = "OrderSearch",
                        action = "OrderDetail",
                        sessionTrackingNo = UrlParameter.Optional,
                        tabId = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "OrderDetail^Add";

            routes.MapRoute(
                name: "OrderResend",
                url: "ResendOrder/{sessionTrackingNo}/{orderTrackingNo}/{destinationTrackingNo}",
                defaults:
                    new { controller = "OrderSearch", action = "ResendOrder", sessionTrackingNo = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "OrderDetail^Add";

            #endregion

            #region Quote Search Start

            routes.MapRoute(
                name: "QuoteSearch",
                url: "search-quote/{customerId}/{quoteType}",
                defaults: new { controller = "QuoteSearch", action = "QuoteSearch", customerId = 0, quoteType = "new" }
                ).DataTokens["AuthFilter"] = "QuoteSearch";

            routes.MapRoute(
                name: "SearchQuoteList",
                url: "searched-quote-list/{requiredRecord}/{requiredPageSize}/{QuoteIdToDelFromList}",
                defaults:
                    new
                    {
                        controller = "QuoteSearch",
                        action = "SearchQuoteList",
                        requiredRecord = 500,
                        requiredPageSize = 50,
                        QuoteIdToDelFromList = 0
                    }
                ).DataTokens["AuthFilter"] = "QuoteSearch";

            routes.MapRoute(
                name: "QuoteDetail",
                url: "quote-detail/{quoteId}/{quoteType}",
                defaults:
                    new
                    {
                        controller = "QuoteSearch",
                        action = "QuoteDetail",
                        quoteId = UrlParameter.Optional,
                        quoteType = UrlParameter.Optional,
                        tabId = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "QuoteDetail^Add";

            #endregion Quote Search End

            #region Ticket Search Start

            routes.MapRoute(
                name: "TicketSearch",
                url: "search-ticket/{companyId}",
                defaults: new { controller = "TicketSearch", action = "TicketSearch", companyId = 0 }
                ).DataTokens["AuthFilter"] = "TicketSearch";

            routes.MapRoute(
                name: "SearchTicketList",
                url: "searched-ticket-list/{requiredRecord}/{requiredPageSize}/{TicketIdToDelFromList}",
                defaults:
                    new
                    {
                        controller = "TicketSearch",
                        action = "SearchTicketList",
                        requiredRecord = 500,
                        requiredPageSize = 50,
                        TicketIdToDelFromList = 0
                    }
                ).DataTokens["AuthFilter"] = "TicketSearch";

            routes.MapRoute(
                name: "TicketDetail",
                url: "ticket-detail/{ticketId}",
                defaults: new { controller = "TicketSearch", action = "TicketDetail", ticketId = 0 }
                ).DataTokens["AuthFilter"] = "TicketDetail^Add";

            #endregion

            #region Asset Search Start

            routes.MapRoute(
                name: "AssetSearch",
                url: "search-asset/{customerId}",
                defaults: new { controller = "AssetSearch", action = "AssetSearch", customerId = 0 }
                ).DataTokens["AuthFilter"] = "AssetSearch";

            routes.MapRoute(
                name: "SearchAssetList",
                url: "searched-asset-list/{requiredRecord}/{requiredPageSize}/{AssetIdToDelFromList}",
                defaults:
                    new
                    {
                        controller = "AssetSearch",
                        action = "SearchAssetList",
                        requiredRecord = 500,
                        requiredPageSize = 50,
                        AssetIdToDelFromList = 0
                    }
                ).DataTokens["AuthFilter"] = "TicketSearch";

            routes.MapRoute(
                name: "AssetDetail",
                url: "asset-detail/{assetId}",
                defaults: new { controller = "AssetSearch", action = "AssetDetail", ticketId = 0 }
                ).DataTokens["AuthFilter"] = "AssetDetail^Add";

            #endregion

            #region sfa catalog search
            routes.MapRoute(
             name: "SFACatalog",
            url: "catalog",
            defaults: new { controller = "Catalog", action = "SFACatalogSearch", customerId = 0 }
            ).DataTokens["AuthFilter"] = "SFATrackCatalog";

            routes.MapRoute(
            name: "SFADomesticCatalog",
           url: "domestic-catalog",
           defaults: new { controller = "Catalog", action = "SFADomesticCatalogSearch", customerId = 0 }
           ).DataTokens["AuthFilter"] = "SFATrackDomesticCatalog";

            routes.MapRoute(
            name: "SFABundleCatalog",
           url: "catalog-bundle",
           defaults: new { controller = "Catalog", action = "SFACatalogBundleSearch", customerId = 0 }
           ).DataTokens["AuthFilter"] = "SFACatalogBundle";
            #endregion
            //Search End

            //sfa setting
            #region Sfa setting
            routes.MapRoute(
                name: "change-password",
                url: "change-password",
                defaults: new { controller = "SFAPreferences", action = "ChangePassword", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "ChangePassword";

            routes.MapRoute(
                name: "contact-info",
                url: "contact-info",
                defaults: new { controller = "SFAPreferences", action = "ContactInfo", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "ContactInfo";

            routes.MapRoute(
                name: "accreditations",
                url: "accreditations",
                defaults: new { controller = "SFAPreferences", action = "Accreditations", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "Accreditations";

            routes.MapRoute(
                name: "preferences",
                url: "preferences",
                defaults: new { controller = "SFAPreferences", action = "Preferences", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "Preferences";

            routes.MapRoute(
                name: "addresses",
                url: "addresses",
                defaults: new { controller = "SFAPreferences", action = "Addresses", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "Addresses";

            routes.MapRoute(
                name: "address",
                url: "address/{addressId}",
               defaults: new { controller = "SFAPreferences", action = "Address", addressId = 0 }
                ).DataTokens["AuthFilter"] = "Addresses";

            routes.MapRoute(
                name: "sfaservices",
                url: "services",
                defaults: new { controller = "SFAPreferences", action = "Services", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "SFAServices";

            routes.MapRoute(
                name: "upliftsduties",
                url: "uplifts-duties",
                defaults: new { controller = "SFAPreferences", action = "UpliftsDuties", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "UpliftsDuties";
            #endregion
            //

            //admin menu start
            routes.MapRoute(
                name: "CreateAdminMenu",
                url: "admin-menu/manage/{id}",
                defaults: new { controller = "AdminMenuSetup", action = "Index", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "CreateAdminMenu";


            routes.MapRoute(
                name: "AdminMenuRoleMapping",
                url: "admin-menu/role-mapping/{id}",
                defaults: new { controller = "AdminMenuSetup", action = "MenuRoleSetup", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "RoleMenuSetup";


            routes.MapRoute(
                name: "AdminUserSpecificMenu",
                url: "admin-menu/customer-user-menu/{typeName}/{roleId}/{typeId}/{userId}",
                defaults: new
                {
                    controller = "AdminMenuSetup",
                    action = "CustomerUserMenuRestriction",
                    roleId = UrlParameter.Optional,
                    typeId = UrlParameter.Optional,
                    userId = UrlParameter.Optional

                }).DataTokens["AuthFilter"] = "CustomerUserMenuSetup";


            //admin menu end

            //Company Controller Start
            routes.MapRoute(
                name: "AdminCompanySearch",
                url: "admin/search-company",
                defaults: new { controller = "Company", action = "Index" }
                ).DataTokens["AuthFilter"] = "AdminCompany";

            routes.MapRoute(
                name: "CompanyWiseSearch",
                url: "admin/company/{companyId}/{partyId}/{companyRole}",
                defaults: new { controller = "Company", action = "Company", companyId = -1, partyId = 0, companyRole = 0 }
                ).DataTokens["AuthFilter"] = "AdminCompany^Add";

            //company controller end

            //Country Controller start
            routes.MapRoute(
                name: "localeSettingCountries",
                url: "locale-settings/country/{countryName}",
                defaults: new { controller = "Country", action = "CountryListing", countryName = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "localeCountries";


            routes.MapRoute(
                name: "localeSettingIsoCountries",
                url: "locale-settings/iso-country/{countryName}",
                defaults:
                    new { controller = "Country", action = "IsoCountryListing", countryName = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "LocaleIsoCountries";


            routes.MapRoute(
                name: "CountryAlert",
                url: "countryalert",
                defaults:
                    new { controller = "Country", action = "Index" }
                ).DataTokens["AuthFilter"] = "CountryAlert";

            //Country Controller end

            //vendor code start

            routes.MapRoute(
                name: "VendorCodes",
                url: "vendor-codes",
                defaults:
                    new { controller = "VendorCode", action = "Index" }
                ).DataTokens["AuthFilter"] = "VendorCodes";

            routes.MapRoute(
                name: "VendorCode",
                url: "vendor-code/{VCode}",
                defaults:
                    new { controller = "VendorCode", action = "VendorCode", VCode = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "VendorCodes^Add";

            //vendor code end

            //Product Maintenance start
            //Part categories start
            routes.MapRoute(
                name: "PartCategories",
                url: "product-maintenance/part-categories",
                defaults:
                    new { controller = "ProductMaintenance", action = "PartCategories" }
                ).DataTokens["AuthFilter"] = "PartCategories";

            routes.MapRoute(
                name: "PartCategory",
                url: "product-maintenance/part-category/{PCId}",
                defaults:
                    new { controller = "ProductMaintenance", action = "PartCategory", PCId = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "PartCategories^Add";
            //Part categories end

            //Freight carrier start
            routes.MapRoute(
                name: "FreightCarriers",
                url: "product-maintenance/freight-carriers",
                defaults:
                    new { controller = "ProductMaintenance", action = "FreightCarriers" }
                ).DataTokens["AuthFilter"] = "FreightCarriers";

            routes.MapRoute(
                name: "FreightCarrier",
                url: "product-maintenance/freight-carrier/{FCId}",
                defaults:
                    new { controller = "ProductMaintenance", action = "FreightCarrier", PCId = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "FreightCarriers^Add";
            //Freight carrier end

            //Product Maintenance end

            //--Support & Service Maintenance start--//
            //service start
            routes.MapRoute(
                name: "Services",
                url: "support-service-maintenance/services",
                defaults:
                    new { controller = "SupportServiceMaintenance", action = "Services" }
                ).DataTokens["AuthFilter"] = "Services";

            routes.MapRoute(
                name: "Service",
                url: "support-service-maintenance/service/{SId}",
                defaults:
                    new { controller = "SupportServiceMaintenance", action = "Service", SId = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "Services^Add";
            //service end

            //Request Type/Ticket Status start
            routes.MapRoute(
                name: "ActionRequestTicketStatus",
                url: "support-service-maintenance/actionrequest-ticketstatus/{companyId}/{editType}/{isVerbose}",
                defaults:
                    new
                    {
                        controller = "SupportServiceMaintenance",
                        action = "ActionRequestTicketStatus",
                        companyId = UrlParameter.Optional,
                        editType = UrlParameter.Optional,
                        isVerbose = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "ActionRequestTicketStatus";


            routes.MapRoute(
                name: "ActionRequestDetail",
                url: "support-service-maintenance/actionrequest/{artypeId}/{companyId}/{isVerbose}",
                defaults:
                    new
                    {
                        controller = "SupportServiceMaintenance",
                        action = "ActionRequestDetail",
                        artypeId = UrlParameter.Optional,
                        companyId = UrlParameter.Optional,
                        isVerbose = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "ActionRequestTicketStatus^Add";

            //Request Type/Ticket Status end

            //--Support & Service Maintenance end--//

            //Primecontractor start


            routes.MapRoute(
                name: "primeContractor",
                url: "prime-contractor/{name}",
                defaults: new { controller = "PrimeContractor", action = "Index", name = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "LocalePrimeContractor";


            routes.MapRoute(
                name: "primeContractorAdd",
                url: "prime-contractor-profile/{pcId}",
                defaults:
                    new
                    {
                        controller = "PrimeContractor",
                        action = "AddUpdatePrimeContractor",
                        pcId = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "LocalePrimeContractor^Add";


            //primecontractor end



            //Currency start
            routes.MapRoute(
                name: "CurrencyAlert",
                url: "currencyalert",
                defaults:
                    new { controller = "Currency", action = "Index" }
                ).DataTokens["AuthFilter"] = "CurrencyAlert";

            routes.MapRoute(
                name: "LocaleCurrencies",
                url: "locale-settings/currency",
                defaults:
                    new { controller = "Currency", action = "CurrencyListing" }
                ).DataTokens["AuthFilter"] = "LocaleCurrencies";

            routes.MapRoute(
                name: "LocaleIsoCurrencies",
                url: "locale-settings/isocurrency",
                defaults:
                    new { controller = "Currency", action = "ISOCurrencyListing" }
                ).DataTokens["AuthFilter"] = "LocaleIsoCurrencies";
            //Currency end
            //catalog start
            routes.MapRoute(
                name: "CatalogAlert",
                url: "catalogalert",
                defaults:
                    new { controller = "Catalog", action = "Index" }
                ).DataTokens["AuthFilter"] = "CatalogAlert";
            //catalog end

            //customer start

            routes.MapRoute(
                name: "CustomerAlert",
                url: "customeralert",
                defaults:
                    new { controller = "Customer", action = "Index" }
                ).DataTokens["AuthFilter"] = "CustomerAlert";

            //customer end

            //exchange rate start
            routes.MapRoute(
                name: "ExchangeRateAlert",
                url: "exchange-rate-alert",
                defaults:
                    new { controller = "ExchangeRate", action = "Index" }
                ).DataTokens["AuthFilter"] = "ExchangeRateAlert";

            //exchange rate end

            //oem start
            routes.MapRoute(
                name: "OemAlert",
                url: "oemalert",
                defaults:
                    new { controller = "OEM", action = "Index" }
                ).DataTokens["AuthFilter"] = "OemAlert";
            //oem end

            //category start
            routes.MapRoute(
                name: "OsCategorySetup",
                url: "categorysetup",
                defaults:
                    new { controller = "OstCustomerCategory", action = "Index" }
                ).DataTokens["AuthFilter"] = "OsCategorySetup";

            routes.MapRoute(
                name: "OsCategoryMapping",
                url: "categorymapping",
                defaults:
                    new { controller = "CategoryMapping", action = "Index" }
                ).DataTokens["AuthFilter"] = "OsCategoryMapping";

            //category end

            //manufacturer start
            routes.MapRoute(
                name: "OsManufacturerSetup",
                url: "manufacturersetup",
                defaults:
                    new { controller = "OstCustomerManufacturer", action = "Index" }
                ).DataTokens["AuthFilter"] = "OsManufacturerSetup";

            routes.MapRoute(
                name: "OsManufacturerMapping",
                url: "manufacturermapping",
                defaults:
                    new { controller = "ManufacturerMapping", action = "Index" }
                ).DataTokens["AuthFilter"] = "OsManufacturerMapping";

            //manufacturer end

            //catalog/product start
            routes.MapRoute(
                name: "OsCatalogSetup",
                url: "catalogsetup/{id}",
                defaults:
                    new { controller = "OstCustomerProduct", action = "Index", id = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "OsCatalogSetup";
            //catalog/product end

            //supplier start
            routes.MapRoute(
                name: "OsSupplierSettings",
                url: "supplier-setting",
                defaults:
                    new { controller = "SFAs", action = "OneStreetUplifts" }
                ).DataTokens["AuthFilter"] = "OsSupplierSettings";
            //

            //user start
            routes.MapRoute(
                name: "OsUserSetup",
                url: "usersetup",
                defaults:
                    new { controller = "User", action = "Index" }
                ).DataTokens["AuthFilter"] = "OsUserSetup";
            //

            //AM Communication start
            routes.MapRoute(
                name: "AmCommunicationSettings",
                url: "am-communication",
                defaults:
                    new { controller = "AM", action = "Index" }
                ).DataTokens["AuthFilter"] = "AmCommunicationSettings";
            //AM Communication end

            //Attachments/Case study start
            routes.MapRoute(
                name: "AttachmentsSettings",
                url: "casestudy",
                defaults:
                    new { controller = "CaseStudyAttachement", action = "Index" }
                ).DataTokens["AuthFilter"] = "AttachmentsSettings";
            //Attachments/Case study end

            //disclaimer start
            routes.MapRoute(
                name: "DisclaimerSettings",
                url: "disclaimersetting",
                defaults:
                    new { controller = "DisclaimerSetting", action = "Index" }
                ).DataTokens["AuthFilter"] = "DisclaimerSettings";
            //disclaimer end

            //image upload
            routes.MapRoute(
                name: "ImageUploadSettings",
                url: "image-upload",
                defaults:
                    new { controller = "Upload", action = "Index" }
                ).DataTokens["AuthFilter"] = "ImageUploadSettings";

            //

            //Request start
            routes.MapRoute(
                name: "RequestSettings",
                url: "requestsetting",
                defaults:
                    new { controller = "RequestSetting", action = "Index" }
                ).DataTokens["AuthFilter"] = "RequestSettings";

            routes.MapRoute(
                name: "RequestEngineerSettings",
                url: "request-engineer-mapping",
                defaults:
                    new { controller = "RequestEngineerLevel", action = "Index" }
                ).DataTokens["AuthFilter"] = "RequestEngineerSettings";
            //request end

            //Region start
            routes.MapRoute(
                name: "RegionSettings",
                url: "region",
                defaults:
                    new { controller = "Region", action = "Index" }
                ).DataTokens["AuthFilter"] = "RegionSettings";
            //Region end


            //SLA setting 
            routes.MapRoute(
                name: "SlaSettings",
                url: "slasetting",
                defaults:
                    new { controller = "SLASetting", action = "Index" }
                ).DataTokens["AuthFilter"] = "SlaSettings";

            routes.MapRoute(
                name: "LocaleOrderQuoteSla",
                url: "other/order-quoteSLA",
                defaults:
                    new { controller = "SLASetting", action = "OrderQuoteSLA" }
                ).DataTokens["AuthFilter"] = "LocaleOrderQuoteSla";

            routes.MapRoute(
                name: "OEMSpecificSLAs",
                url: "other/oem-specific-sla",
                defaults:
                    new { controller = "SLASetting", action = "OEMSpecificSLAs" }
                ).DataTokens["AuthFilter"] = "OEMSpecificSLAs";

            routes.MapRoute(
                name: "LocalTaxes",
                url: "other/local-taxes",
                defaults:
                    new { controller = "SLASetting", action = "LocalTaxes" }
                ).DataTokens["AuthFilter"] = "LocalTaxes";

            //

            //Holiday
            routes.MapRoute(
                name: "LocaleNationalHolidays",
                url: "locale-settings/holidays",
                defaults:
                    new { controller = "Holiday", action = "Index" }
                ).DataTokens["AuthFilter"] = "LocaleNationalHolidays";
            //

            //Budget
            routes.MapRoute(
                name: "CustomerSiteBudget",
                url: "customer-site-budget",
                defaults:
                    new { controller = "SiteBudget", action = "Index" }
                ).DataTokens["AuthFilter"] = "CustomerSiteBudget";

            routes.MapRoute(
                name: "CustomerCountryBudget",
                url: "customer-country-budget",
                defaults:
                    new { controller = "Budget", action = "Index" }
                ).DataTokens["AuthFilter"] = "CustomerCountryBudget";

            //




            //Quote start

            routes.MapRoute(
                name: "AssetDisposalList",
                url: "asset-disposal",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "AssetDisposalList",

                    }
                ).DataTokens["AuthFilter"] = "QuoteAssetDisposal";


            routes.MapRoute(
                name: "AssetDisposalAdd",
                url: "asset-disposal-quote",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "AssetDisposalQuote",
                        pcId = UrlParameter.Optional
                    }
                ).DataTokens["AuthFilter"] = "QuoteAssetDisposal^Add";


            routes.MapRoute(
                name: "hardwaresList",
                url: "hardwares",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "GetHardwareQuotes",

                    }
                ).DataTokens["AuthFilter"] = "QuoteHardware";


            routes.MapRoute(
                name: "hardwaresAdd",
                url: "hardware-quote",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "Index",

                    }
                ).DataTokens["AuthFilter"] = "QuoteHardware^Add";



            routes.MapRoute(
                name: "softwaresList",
                url: "softwares",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "Softwares",

                    }
                ).DataTokens["AuthFilter"] = "QuoteSoftware";


            routes.MapRoute(
                name: "softwaresAdd",
                url: "software-quote",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "SWQuoteRequest",

                    }
                ).DataTokens["AuthFilter"] = "QuoteSoftware^Add";



            routes.MapRoute(
                name: "itservicesList",
                url: "it-services",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "ITServices",

                    }
                ).DataTokens["AuthFilter"] = "QuoteItServices";

            routes.MapRoute(
                name: "itservicesAdd",
                url: "it-service-quote",
                defaults:
                    new
                    {
                        controller = "Quotes",
                        action = "ITServiceQuote",

                    }
                ).DataTokens["AuthFilter"] = "QuoteItServices^Add";

            //Quote End

            //Contacts Starts
            routes.MapRoute(
                name: "ContactsMain",
                url: "contacts/{searchStr}",
                defaults: new { controller = "Contacts", action = "Index", searchStr = UrlParameter.Optional }
                ).DataTokens["AuthFilter"] = "contactsMain";


            routes.MapRoute(
                name: "ContactsMainAdd",
                url: "contact/add",
                defaults: new { controller = "Contacts", action = "AddContact" }
                ).DataTokens["AuthFilter"] = "contactsMain^Add";



            //Contacts End

            //Customer Menu Settings Start


            routes.MapRoute(
                name: "customerMenuSettings",
                url: "customer-menu",
                defaults: new { controller = "CustomerMenuSetup", action = "Index" }
                ).DataTokens["AuthFilter"] = "customerMenu";


            //Customer Menu Settings End

            //Manufacturer Parts Starts

            routes.MapRoute(
                name: "manufacturer-partssearch",
                url: "product-maintenance/manufacturer-parts",
                defaults: new { controller = "ProductMaintenance", action = "ManufacturerParts" }
                ).DataTokens["AuthFilter"] = "manufacturerParts";


            routes.MapRoute(
                name: "manufacturer-partEdit",
                url: "product-maintenance/manufacturer-part/{partId}",
                defaults: new { controller = "ProductMaintenance", action = "ManufacturerPartsAddEdit" }
                ).DataTokens["AuthFilter"] = "manufacturerParts^Add";

            //Manufacturer Parts End



            routes.MapRoute(
                name: "PageNotFound",
                url: "PageNotFound",
                defaults: new { controller = "Account", action = "PageNotFound" }
                );
            routes.MapRoute(
                name: "InternalServerError",
                url: "InternalServerError",
                defaults: new { controller = "Account", action = "InternalServerError" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
                ).DataTokens["checkFilter"] = "ad";



        }
    }
}

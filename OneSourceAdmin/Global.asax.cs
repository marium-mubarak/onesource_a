﻿
using OneSourceAdminCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OneSourceAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            StaticHelper.LogException(ex.ToString(), "Application_Error");
            if (ex.InnerException != null)
            {
                StaticHelper.LogException("Inner Ex:\n" + ex.InnerException, "Application_Error");
            }
            if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 404)
            {
                Response.Redirect("~/Account/PageNotFound");
                return;
            }
            else if (ex is HttpException && (((HttpException)ex).GetHttpCode() == 505 || ((HttpException)ex).GetHttpCode() == 500))
            {
                Response.Redirect("~/Account/InternalServerError");
                return;
            }

            Response.Redirect("~/Account/Error");
            return;
        }
    }
}

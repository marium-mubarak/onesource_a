﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OneSourceAdmin.Startup))]
namespace OneSourceAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Data;
using System.Data.OleDb;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : July 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing request/ticket support"
    /// </summary>
    public class SupportRequestController : BaseController
    {
        private SupportRequestPresenter srp;
        public SupportRequestController()
        {
            srp = new SupportRequestPresenter();
        }
        // GET: SupportRequest

        /// <summary>
        /// this action is responsible for redirecting ISD and SFA to specific view and action
        /// </summary>
        /// <param name="filterId"></param>
        /// <returns></returns>
        [Route("requests/{filterId?}")]
        public ActionResult Index(string filterId = "")
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    return RedirectToAction("NewRequest");
                }
                else
                {
                    var model = srp.GetSupportRequestList(Convert.ToInt32(Session["UserID"]),
                        Convert.ToInt32(Session["ParentCompanyID"]), filterId, Convert.ToString(Session["UserGroupName"]), Convert.ToString(Session["SFAID"]), Convert.ToString(Session["ContactID"]));

                    return View("~/Views/SupportRequest/SFA/SupportRequestList.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing NewRequest View For New request
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult NewRequest(int companyId = 0)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    var model = srp.GetSupportRequestSeach(companyId == 0 ? Convert.ToInt32(Session["ParentCompanyId"]) : companyId, Convert.ToInt32(Session["ContactID"]), Session["UserGroupName"].ToString(), 0);
                    return View("~/Views/SupportRequest/ISD/NewRequest.cshtml", model);
                }
                else
                {
                    var model = srp.GetSupportRequestSeach(companyId == 0 ? Convert.ToInt32(Session["ParentCompanyId"]) : companyId, Convert.ToInt32(Session["ContactID"]), Session["UserGroupName"].ToString(), Convert.ToInt32(Session["SFAID"]));
                    return View("~/Views/SupportRequest/SFA/NewRequest.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for the creation of new request
        /// </summary>
        /// <param name="model"></param>
        /// <param name="attachFiles"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddRequest(SupportRequestSearchModel model, HttpPostedFileBase[] attachFiles)
        {
            try
            {
                Session["FinalSerialNumber"] = null;
                Session["FinalAssetId"] = null;
                Session["FinalAssetTag"] = null;
                Session["FinalAssetDescription"] = null;
                Session["FinalAssetLocation"] = null;

                var ticketId = srp.AddRequest(model, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["ContactID"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToString(Session["UserName"]), Session["UserGroupName"].ToString());
                foreach (var attachFile in attachFiles)
                {

                    if (attachFile != null && attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                        if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" ||
                            fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" ||
                            fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" ||
                            fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".msg" ||
                            fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".png")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                string fileNameM = docId + "^" + fileName;
                                var path =
                                    Path.Combine(
                                        Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"),
                                        fileNameM);
                                attachFile.SaveAs(path);



                                int? daid = srp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path,
                                    ConfigurationManager.AppSettings["UploadURL"] + "/" + fileNameM,
                                    int.Parse(Session["UserID"].ToString()), null);
                                if (daid <= 0)
                                {
                                    if (System.IO.File.Exists(path))
                                    {
                                        System.IO.File.Delete(path);
                                    }

                                }
                                else
                                {
                                    int a = srp.AddTicketInfo(ticketId.ToString(), daid.ToString(),
                                        DateTime.Now, Session["UserID"].ToString());

                                }
                            }
                        }

                    }
                }

                if (ticketId == -1)
                {
                    TempData["ErrorMessage"] = "Error Occured.";
                }
                else if (ticketId == -10)
                {
                    TempData["ErrorMessage"] = "Please select an existing asset.";
                }
                else if (ticketId > 0)
                {
                    TempData["Message"] = @"<p>Your request has been successfully submitted and a ticket has been issued. Click on the ticket number to track the status of your request.<p>
                <p>Your ticket number is <a style='color:green' href='/ticket-detail/" + ticketId + "'>" + ticketId + "</a></p>";
                }
              

                return RedirectToAction("NewRequest");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for Searching of asset tag
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public ActionResult GetAssetTagSearchData(int parentCompanyId)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            var model = srp.GetAssetTagSearchPopupData(parentCompanyId, Session["UserGroupName"].ToString(), 0);
                            return PartialView("~/Views/SupportRequest/_assetTagSearchPopup.cshtml", model);
                        }
                        else
                        {
                            var model = srp.GetAssetTagSearchPopupData(parentCompanyId, Session["UserGroupName"].ToString(), Convert.ToInt32(Session["SFAID"]));
                            return PartialView("~/Views/SupportRequest/_assetTagSearchPopup.cshtml", model);
                        }


                    }
                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }
                        return Json("0", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for providing asset information view 
        /// </summary>
        /// <param name="modelRecieve"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAssetGrid(AssetPopupModel modelRecieve)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {

                        if (Session["AssetBackUrl"] == null)
                        {
                            if (Request.UrlReferrer != null)
                            {
                                Session["AssetBackUrl"] = Request.UrlReferrer;
                            }
                        }
                        Session["serialNumber"] = modelRecieve.SerialNumber ?? "";
                        Session["assetLocationId"] = modelRecieve.AssetLocationId;
                        Session["assetStatusId"] = modelRecieve.AssetStatusId;
                        Session["assetCategoryId"] = modelRecieve.AssetCategoryIds ?? "";
                        Session["assetManufactureId"] = modelRecieve.AssetManufactureIds ?? "";
                        Session["AssetPartNumber"] = modelRecieve.AssetPartNumber ?? "";
                        Session["AssetTag"] = modelRecieve.AssetTag ?? "";

                        return RedirectToAction("AssetGridActionResult", new { @companyId = modelRecieve.ParentCompanyId });
                    }

                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }

                        return View("~/Views/Shared/ErrorPage.cshtml");

                    }
                }
                else
                    return RedirectToAction("Login", "Account");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for asset grid
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult AssetGridActionResult(int companyId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.BackUrl = Session["AssetBackUrl"];
                    var model = new AssetDataGridModel();
                    model.CompanyId = companyId;
                    var userGroupName = Session["UserGroupName"].ToString();
                    var serialNumber = Session["serialNumber"].ToString();
                    var assetLocationId = Session["assetLocationId"].ToString();
                    var assetStatusId = Session["assetStatusId"].ToString();
                    var assetCategoryId = Session["assetCategoryId"].ToString();
                    var assetManufactureId = Session["assetManufactureId"].ToString();
                    var assetPartNumber = Session["AssetPartNumber"].ToString();
                    var assetTag = Session["AssetTag"].ToString();
                    Session["assetCategoryId"] = Session["assetCategoryId"].ToString();
                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        model.AssetDataGridDataList = srp.GetAssetDataGridDataList(companyId.ToString(), userGroupName, serialNumber, assetLocationId,
                        assetStatusId, assetCategoryId, assetManufactureId, assetPartNumber, assetTag, "0");
                    }
                    else
                    {
                        model.AssetDataGridDataList = srp.GetAssetDataGridDataList(companyId.ToString(), userGroupName, serialNumber, assetLocationId,
                        assetStatusId, assetCategoryId, assetManufactureId, assetPartNumber, assetTag, Session["SFAID"].ToString());
                    }
                    return View("AssetDataGrid", model);

                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }

                    return View("~/Views/Shared/ErrorPage.cshtml");

                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This action is responsible for asset request data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult GetAssetRequestData(AssetDataGridModel model)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
                        Session["FinalSerialNumber"] = model.SerialNumber ?? "";
                        Session["FinalAssetId"] = model.AssetId;
                        Session["FinalAssetTag"] = model.AssetTag ?? "";
                        Session["FinalAssetDescription"] = model.AssetDescription ?? "";
                        Session["FinalAssetLocation"] = model.AssetLocation ?? "";

                        return RedirectToAction("NewRequest", new { @companyId = model.CompanyId });
                    }

                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }

                        return View("~/Views/Shared/ErrorPage.cshtml");

                    }
                }
                else
                    return RedirectToAction("Login", "Account");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for making default request filter
        /// </summary>
        /// <param name="filterValue"></param>
        /// <returns></returns>
        public JsonResult MakeDefaultFilter(string filterValue)
        {
            try
            {
                int count = srp.MakeDefaultFilter( filterValue, Convert.ToInt32(Session["UserID"]));
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for providing new request filter view
        /// </summary>
        /// <returns></returns>
        [Route("newrequestfilter")]
        public ActionResult NewRequestFilter()
        {
            try
            {
                if (Convert.ToString(Session["UserGroupName"]) == "SFA")
                {
                    ViewBag.Message = TempData["Message"];
                    NewRequestFilterModel newmodel = new NewRequestFilterModel();
                    var model = srp.GetNewRequestFilterModel(Convert.ToString(Session["UserGroupName"]), Convert.ToInt32(Session["SFAID"]), newmodel);
                    return View("~/Views/SupportRequest/SFA/NewRequestFilter.cshtml", model);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for providing partial view of change info on the new request filter page
        /// </summary>
        /// <param name="newmodel"></param>
        /// <returns></returns>
        [Route("createrequestfilter/{selectedCustomerId?}/{selectedTypes?}/{name?}/{dateId?}/{filterType?}")]
        public ActionResult NewRequestFilter(NewRequestFilterModel newmodel)
        {
            try
            {
               
                var model = srp.GetNewRequestFilterModel(Convert.ToString(Session["UserGroupName"]), Convert.ToInt32(Session["SFAID"]), newmodel);
                return PartialView("~/Views/SupportRequest/SFA/_newRequestFilter.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for adding new request filter
        /// </summary>
        /// <param name="newmodel"></param>
        /// <returns></returns>
        public ActionResult AddRequestFilter(NewRequestFilterModel newmodel)
        {
            try
            {
                int count = srp.AddRequestFilter(newmodel, Convert.ToInt32(Session["UserID"]));
                if (count > 0)
                {
                    TempData["Message"] = "Request filter has been created successfully.";
                    return RedirectToAction("Index");
                }
                TempData["Message"] = "No record was updated successfully.";
                return RedirectToAction("NewRequestFilter");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
    }
}
﻿using OneSourceAdmin.ViewModels;
using OneSourceAdminCore.Presenter;
using System;
using System.Data;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Customer's product detail"
    /// </summary>
    public class OstCustomerProductController : BaseController
    {
        CustomerProductPresenter ccp;
        CustomerCategoriesPresenter ccatp;
        OneStreetPropertyViewModel model;
        public OstCustomerProductController() //Constructor
        {
            ccp = new CustomerProductPresenter();
            ccatp = new CustomerCategoriesPresenter();
            model = new OneStreetPropertyViewModel();
        }

        #region Catalog Setup View
        // GET: /OstCustomerProduct/
        /// <summary>
        /// Date Modified : 23 Oct 2019
        /// Created by : Muhammad Waleed Nasir
        /// This Action is responsible for providing Customer,Manufacturer & Category mapping"
        /// </summary>
        public ActionResult Index(int? id, int? manufacturerId, int? catId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    ViewBag.Message = TempData["Message"];
                    ViewBag.ManufacturerId = manufacturerId == null ? 0 : manufacturerId;
                    ViewBag.CustomerId = id == null ? 0 : id;
                    ViewBag.CatId = catId == null ? 0 : catId;
                    ViewBag.AddEditAccess = isAddEditAllow("OsCatalogSetup");
                    model.CustomerName = cp.GetMyCustomers();
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                        LogException(ex.InnerException.ToString(), routeData);
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }
        #endregion

        #region Get Categories
        // To get categories with respect to manufacturer
        public JsonResult GetCategories(int manufacturerId, int customerId) 
        {
            try
            {
                var categories = ccp.GetCategories(manufacturerId, customerId);
                var checkSelect = ccp.CheckSelectCriteria(customerId, manufacturerId);
                var data = new { categories, checkSelect };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException(ex.InnerException.ToString(), routeData);
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Manufacturer Category Mapping
        /// <summary>
        /// Date Modified : 28 Oct 2019
        /// Created by : Muhammad Waleed Nasir
        /// This Action is responsible for mapping"
        /// </summary>
        [HttpPost]
        public JsonResult UpdateCustomerProductSetting(OneStreetPropertyViewModel model, FormCollection fm)
        {
            bool check = false;
            if (model.CategoryId == null)
            {
                check = false;
                model.CategoryId = "-1";
            }
            else
            {
                check = model.CategoryId.Contains("on");
                model.CategoryId = model.CategoryId.Replace("on", "");
                model.CategoryId = model.CategoryId.Trim().TrimStart(',');
            }            
            if (model.CustomerId > 0)
            {
                int a;
                model.LastUpdated = DateTime.Now;
                model.UpdatedBy = ((int)Session["userid"]);
                if (check)
                    a = ccp.UpdateCustomerProductSetting(model.CustomerId, model.ManufacturerId, "", model.UpdatedBy, model.LastUpdated);
                else
                    a = ccp.UpdateCustomerProductSetting(model.CustomerId, model.ManufacturerId, model.CategoryId, model.UpdatedBy, model.LastUpdated);
                var data = TempData["message"] = a > 0 ? "true" : "false";
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = TempData["message"] = "please provide correct data";
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Catalog Setup Mapped Report
        /// <summary>
        /// Date Modified : 05 Nov 2019
        /// Created by : Muhammad Waleed Nasir
        /// This Action is responsible for Getting CustomerId to export report"
        /// </summary>
        public void GetDataToExport(FormCollection col)
        {
            try
            {
                var customerId = Convert.ToInt32(col["customerIdForExport"]);                
                DataTable dt = null;
                CustomerPresenter cp = new CustomerPresenter();
                if (customerId > 0)
                {
                    dt =  ccp.GetReportData(customerId);
                    if (dt != null && dt.Rows.Count > 0)
                        ExportToExcel(dt);
                    else
                        ViewBag.Message = "Error";              
                }               
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException(ex.InnerException.ToString(), routeData);
            }
        }

        #region Get DataTable to export in excel
        /// <summary>
        /// Date Modified : 05 Nov 2019
        /// Created by : Muhammad Waleed Nasir
        /// This Action is responsible for exporting mapped manufacturers & categories"
        /// </summary>
        public void ExportToExcel(DataTable dt)
        {
            try
            {
                Response.Buffer = true;
                var gridview = new GridView();
                gridview.DataSource = dt;
                gridview.DataBind();
                // Clear all the content from the current response
                Response.ClearContent();

                Response.Buffer = true;
                Response.BufferOutput = true;
                // set the header
                Response.AddHeader("content-disposition", "attachment;filename = Mapped_Onestreet_Catalog_By_ManufacturerAndCategory_Report.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";

                // create HtmlTextWriter object with StringWriter
                using (var sw = new StringWriter())
                {
                    using (var htw = new HtmlTextWriter(sw))
                    {
                        // render the GridView to the HtmlTextWriter
                        gridview.RenderControl(htw);
                        // Output the GridView content saved into StringWriter
                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException("from -->>GetCatalogSetupReport \n-->> " + ex.InnerException.ToString(), routeData);
            }
        }
        #endregion
        #endregion


        #region Commented Code
        //public string GetProducts(int categoryId, int manufacturerId, int customerId)
        //{
        //    try
        //    {
        //        var data = ccp.GetProducts(customerId, manufacturerId, categoryId);
        //        TempData["DataCount"] = data.Count();
        //        var serilized = JsonConvert.SerializeObject(data);
        //        return serilized;
        //    }
        //    catch (Exception ex)
        //    {
        //        var routeData = this.ControllerContext.RouteData;
        //        LogException(ex.Message, routeData);
        //        if (ex.InnerException != null)
        //            LogException(ex.InnerException.ToString(), routeData);
        //        return "";
        //    }
        //}


        //public JsonResult GetCategories(int manufacturerId, int customerId)
        //{
        //    try
        //    {
        //        var data = ccp.GetCategories(manufacturerId, customerId);
        //        return Json(data, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        var routeData = this.ControllerContext.RouteData;
        //        LogException(ex.Message, routeData);
        //        if (ex.InnerException != null)
        //            LogException(ex.InnerException.ToString(), routeData);
        //        return Json("0", JsonRequestBehavior.AllowGet);
        //    }
        //}

        /// <summary>
        /// This  Action is responsible for updating the customer product record
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="manufacturerId"></param>
        /// <param name="categoryTreeFamilyID"></param>
        /// <param name="productIds"></param>
        /// <returns></returns>
        //[HttpPost,ValidateAntiForgeryToken]
        //public ActionResult UpdateCustomerProductSetting(OneStreetPropertyViewModel model, string productIds,string catIds,FormCollection fm)
        //{
        //    int count = Convert.ToInt16(fm["TotalProductCount"]);
        //    if(Convert.ToInt16(TempData["DataCount"]) == count)
        //    {

        //    }
        //    if (model.CustomerId > 0 && (model.ManufacturerId == 0 || model.CategoryId == 0))
        //    {
        //        int a = ccp.UpdateCustomerProductSetting(model.CustomerId, model.ManufacturerId, model.CategoryId, productIds, Convert.ToInt32(Session["UserID"]));
        //        TempData["Message"] = a > 0 ? "Record successfully updated" : "No record was updated";
        //        return RedirectToAction("Index");
        //    }
        //    else if (!string.IsNullOrEmpty(catIds))
        //    {
        //         ccp.UpdateCustomerProductManufacturerWise(model.CustomerId, model.ManufacturerId, catIds);
        //        TempData["Message"] = "Record successfully updated";
        //    }
        //    else
        //    {
        //        TempData["Message"] = "Please provide correct data";
        //    }

        //    return RedirectToAction("Index");
        //}
        #endregion
    }
}
﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 5 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Disclaimer of customer"
    /// </summary>
    public class DisclaimerSettingController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of Disclaimer
        /// </summary>
        /// <returns></returns>
      
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    CustomerModel model = new CustomerModel
                    {
                        Customers = cp.GetCustomers()
                    };
                    @ViewBag.AddEditAccess = isAddEditAllow("DisclaimerSettings");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action Return Json result of disclaimer by Customer Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetDisclaimer(int customerId)
        {
            DisclaimerPresenter relp = new DisclaimerPresenter();
            var data = relp.GetDisclaimer(customerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///  This Action is responsible for updating disclaimer 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="disclaimer"></param>
        /// <returns></returns>
        public JsonResult UpdateDisclaimer(int customerId,string disclaimer)
        {
            DisclaimerPresenter relp = new DisclaimerPresenter();
            var count = relp.UpdateDisclaimer(customerId,disclaimer,Convert.ToInt32(Session["UserID"]));
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for deleting the disclaimer of the customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult DeleteDisclaimer(int customerId)
        {
            DisclaimerPresenter relp = new DisclaimerPresenter();
            var count = relp.DeleteDisclaimer(customerId, Convert.ToInt32(Session["UserID"]));
            return Json(count, JsonRequestBehavior.AllowGet);
        }
    }
}
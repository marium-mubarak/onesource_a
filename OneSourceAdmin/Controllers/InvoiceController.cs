﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Data;
using iTextSharp.text.html.simpleparser;
using System.IO;
using System.Reflection;
using OneSourceAdminCore.Entity;

namespace OneSourceAdmin.Controllers
{
    public class InvoiceController : BaseController
    {

        private InvoiceService _service;
        public static List<InvoiceLineItemDetailsModel> InvoiceLineItems = new List<InvoiceLineItemDetailsModel>();
        public static int LineItemSeqNum = 0;
        public InvoiceController()
        {
            _service = new InvoiceService();
        }

        [HttpGet]
        // GET: Invoice
        public ActionResult Index(int? back)
        {
            try
            {

                var model = new InvoiceModel();

                model.SentToCompanyId = model.StatusId = model.TypeId = model.OnBehlafCompanyId = 0;

                if (back == 1)
                {
                    model = (InvoiceModel)Session["invoiceSearch"];
                }

                model.OnBehalfOfList = _service.GetOnBehalfOfList();
                model.SentToList = _service.GetSentToList();
                model.StatusList = _service.GetInvoiceStatusList();
                model.TypeList = _service.GetInvoiceTypeList();


                return View("~/Views/Invoice/SearchView.cshtml", model);


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult Index(InvoiceModel data)
        {

            if (data != null)
            {
                Session["invoiceSearch"] = data;

                return RedirectToAction("GetInvoiceList");
            }
            return RedirectToAction("Index");



        }

        /// <summary>
        /// Go to Invoice Wizard Page
        /// </summary>
        /// <param name="back"></param>
        /// <returns></returns>
        public ActionResult InvoiceWizard(int? back)
        {
            try
            {
                Session["AddInvoice"] = null;

                var model = new NewInvoiceModel();
                model.InvoiceTypeList = _service.GetInvoiceTypeList().Select(x => new SelectListItem()
                {
                    Text = x.Value,
                    Value = x.Key.ToString()
                }).ToList();
                model.SelectedTab = "process-type";

                if (back == 1)
                {
                    model = (NewInvoiceModel)Session["invoiceWizard"];
                }
                model.InvoiceTypeActiveOrInActiveList = _service.GetInvoiceTypeActiveOrInActiveList();

                if (back == 1)
                {
                    model = (NewInvoiceModel)Session["invoiceWizard"];
                }
                model.SelectedProcessType = "template";

                return View("~/Views/Invoice/InvoiceWizard.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpGet]
        public ActionResult CreateOrUpdateInvoice(string invoiceTypeId, string selectedType,string invoiceTemplateName=null)
        {
            try
            {
                var model = new NewInvoiceModel();
                if (Session["invoiceWizard"] != null)
                {
                    model = (NewInvoiceModel)Session["invoiceWizard"];
                }

                model.InvoiceTypeId = Convert.ToInt32(invoiceTypeId);
                model.SelectedProcessType = selectedType;
                model.SelectedTab = "create-update";

                if (selectedType == "template")
                {
                    Session["AddInvoice"] = "template";
                }

                prepareModel(model, invoiceTemplateName);

                Session["invoiceWizard"] = model;
                Session["InvoiceLineItems"] = null;
                LineItemSeqNum = 0;
                InvoiceLineItems = new List<InvoiceLineItemDetailsModel>();
                return View("~/Views/Invoice/InvoiceWizard.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult CreateOrUpdateInvoice(NewInvoiceModel model)
        {
            try
            {
                if(model.SelectedProcessType== "template")
                {
                    Session["AddInvoice"] = "template";
                }
                prepareModel(model);
                return View("~/Views/Invoice/InvoiceWizard.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public JsonResult AddNewInvoice(NewInvoiceModel col)
        {
            try
            {
                var invoiceCurrencyId = 0;
                Int32.TryParse(col.invoiceCurrencyDrp, out invoiceCurrencyId);
                var billToAddressId = 0;
                Int32.TryParse(col.invoiceBillToAddressDrp.ToString(), out billToAddressId);
                var billFromAddressId = 0;
                Int32.TryParse(col.invoiceBillFromAddressDrp.ToString(), out billFromAddressId);
                var billFromCompanyId = 0;
                Int32.TryParse(col.invoiceBillFromCompanyDrp.ToString(), out billFromCompanyId);
                var billToCompanyId = 0;
                Int32.TryParse(col.invoiceBillToCompanyDrp.ToString(), out billToCompanyId);
                var payToAddressId = 0;
                Int32.TryParse(col.invoicePayToAddressListDrp.ToString(), out payToAddressId);
                var userId = 0;
                Int32.TryParse(Session["UserID"].ToString(), out userId);
                var prefPaymentId = 0;
                Int32.TryParse(col.invoicePaymentMethodListDrp.ToString(), out prefPaymentId);
                var paymentTermId = 0;
                Int32.TryParse(col.invoicePaymentTermDrp.ToString(), out paymentTermId);
                var invoiceTypeId = 0;
                Int32.TryParse(col.InvoiceTypeId.ToString(), out invoiceTypeId);
                int invoiceRecipientValue = 0;
                Int32.TryParse(col.invoiceRecipientListDrp.ToString(), out invoiceRecipientValue);

                var invoiceOwner = 0;
                Int32.TryParse(col.invoiceOwnerDrp.ToString(), out invoiceOwner);
                var invoiceOBO = 0;
                Int32.TryParse(col.invoiceOBOCompanyDrp.ToString(), out invoiceOBO);

                double vatrate = 0.00;
                double.TryParse(col.InvoiceVatRate, out vatrate);
                decimal VATReg = decimal.Zero;
                decimal.TryParse(col.VATReg, out VATReg);

                decimal InvoiceTotal = decimal.Zero;
                decimal.TryParse(col.InvoiceTotalPayment, out InvoiceTotal);

                decimal InvoiceTotalUsd = decimal.Zero;
                decimal.TryParse(col.InvoiceTotalUsd, out InvoiceTotalUsd);
                //dynamic sendDate = null;
                //if (col.sendDate != null && col.sendDate != "")
                //{
                //    sendDate = DateTime.Parse(col.sendDate.ToString());
                //}
                dynamic invoicePeriodStart = null;
                if (col.StartDate != null && col.StartDate != "")
                {
                    invoicePeriodStart = DateTime.Parse(col.StartDate);
                }
                dynamic invoicePeriodEnd = null;
                if (col.EndDate != null && col.EndDate != "")
                {
                    invoicePeriodEnd = DateTime.Parse(col.EndDate);
                }
                dynamic taxDate = null;
                if (col.TaxDate != null && col.TaxDate != "")
                {
                    taxDate = DateTime.Parse(col.TaxDate);
                }
                //dynamic recdDate = null;
                //if (col.recdDate"] != null && col.recdDate"] != "")
                //{
                //    recdDate = DateTime.Parse(col.recdDate.ToString());
                //}
                dynamic dueDate = null;
                if (col.DueDate != null && col.DueDate != "")
                {
                    dueDate = DateTime.Parse(col.DueDate);
                }
                dynamic createdDate = null;
                if (col.CreateDate != null && col.CreateDate != "")
                {
                    createdDate = DateTime.Parse(col.CreateDate);
                }
                if (vatrate > 0)
                {
                    vatrate = vatrate / 100;
                }
                var paymentinstr = col.PaymentInstructions;
                var comments = col.Comments;
                var po = col.PO;

                int? invoiceRecipient = null;
                if (invoiceRecipientValue > 0)
                {
                    invoiceRecipient = invoiceRecipientValue;
                }
                var invoiceId = _service.AddInvoice(adminSetting, companyinvoiceid: "0", companyid: billFromCompanyId, billtocompanyid: billToCompanyId, invoicetypeid: invoiceTypeId, invoicecurrencyid: invoiceCurrencyId, invoiceperiodstart: invoicePeriodStart, invoiceperiodend: invoicePeriodEnd,
                    vatreg: VATReg.ToString(), taxdate: taxDate, billtoaddressid: billToAddressId, billfromaddressid: billFromAddressId, preferredpaymentmethodid: prefPaymentId, paymenttoaddressid: payToAddressId, paymentinstr: paymentinstr, invoicetermid: paymentTermId, invoicetotal: InvoiceTotal, invoicetotalusd: InvoiceTotalUsd,
                  comments: comments, duedate: dueDate, userid: userId, createdate: createdDate, vatrate: vatrate, po: po, obocompanyid: invoiceOBO, senddate: null, invoicerecipient: 0, invoiceowner: 0);

                if (invoiceId > 0)
                {
                    ViewBag.Message = "Invoice Added Successfully";
                }
                else
                {
                    ViewBag.Message = "Something went wrong, please try again later";
                }

                return Json(new { invoiceId = invoiceId, invoiceTypeId = invoiceTypeId, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return Json(new { invoiceId = 0, invoiceTypeId = 0, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateInvoiceTemplate(tblInvoiceType col)
        {
            try
            {
                var updated = _service.UpdateInvoiceType(col, col.InvoiceTypeID, Convert.ToInt32(Session["UserID"]));
                if (updated)
                {
                    ViewBag.Message = "Invoice Tempate Updated";
                }
                else
                {
                    ViewBag.Message = "Something went wrong while updating,please try again later";
                }


                return Json(new { updated = updated, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return Json(new { invoiceId = 0, invoiceTypeId = 0, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public JsonResult AddNewInvoiceLineItem(List<InvoiceLineItemDetailsModel> values,int invoiceId, int invoiceTypeId)
        //{
        //    try
        //    {
        //        if (invoiceId>0)
        //        {
        //            var lineItems = values;
        //            var count = _service.AddInvoiceLineItem(lineItems, invoiceId, invoiceTypeId);
        //            if (count > 0)
        //            {
        //                ViewBag.Message = "Invoice Added Successfully";
        //            }

        //        }
        //        else
        //        {
        //            ViewBag.Message = "Something went wrong, please try again later";
        //        }

        //        return Json(new { data = invoiceId, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        var routeData = this.ControllerContext.RouteData;
        //        LogException(ex.Message, routeData);
        //        if (ex.InnerException != null)
        //        {
        //            LogException(ex.InnerException.ToString(), routeData);
        //        }

        //        return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult GetInvoiceList()
        {
            try
            {
                var data = new InvoiceModel();
                if (TempData["ErrorMessage"] != null)
                {
                    ViewBag.ErrorMessage = TempData["ErrorMessage"].ToString();
                }

                if (Session["invoiceSearch"] != null)
                {
                    data = (InvoiceModel)Session["invoiceSearch"];
                }
                else
                {
                    return RedirectToAction("Index");
                }

                var dataList = _service.GetIGetInvoicesListnvoicesList(data, adminSetting);
                if (dataList.Count <= 0)
                {
                    ViewBag.ErrorMessage = "No Records found, please change your search criteria and try again";
                    data.OnBehalfOfList = _service.GetOnBehalfOfList();
                    data.SentToList = _service.GetSentToList();
                    data.StatusList = _service.GetInvoiceStatusList();
                    data.TypeList = _service.GetInvoiceTypeList();
                    return View("~/Views/Invoice/SearchView.cshtml", data);

                }
                return View("~/Views/Invoice/InvoicesListView.cshtml", dataList);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public ActionResult InvoiceDetails(int invoiceId)
        {
            try
            {
                var model = _service.GetInvoiceDetails(invoiceId);

                if (model.InvoiceInfo != null)
                {
                    TempData["ErrorMessage"] = null;
                    //Sunny Gohar (13th Aug 2018)
                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        model.InvoiceOBOCompanies = _service.GetCompanyList();
                        model.InvoiceOBOCompanies.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                        model.InvoiceOwnerList = _service.GetInvoiceOwnerList(Session["UserGroupName"].ToString(), model.InvoiceInfo.billfromcoid, invoiceId);
                        model.InvoiceOwnerList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                    }
                    var billFromPartyId = 0;
                    Int32.TryParse(model.InvoiceInfo.billfrompartyid != null ? model.InvoiceInfo.billfrompartyid.ToString() : "0", out billFromPartyId);
                    model.BillfromAddressList = _service.GetPartyLocationList(Session["UserGroupName"].ToString(), billFromPartyId, model.InvoiceInfo.billfromaddress);
                    model.BillfromAddressList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                    var billToPartyId = 0;
                    Int32.TryParse(model.InvoiceInfo.billtopartyid != null ? model.InvoiceInfo.billtopartyid.ToString() : "0", out billToPartyId);

                    model.BillToAddressList = _service.GetPartyLocationList(Session["UserGroupName"].ToString(), billToPartyId, model.InvoiceInfo.billtoaddress);
                    model.BillToAddressList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    model.PayToAddressList = _service.GetPartyLocationList(Session["UserGroupName"].ToString(), billFromPartyId, model.InvoiceInfo.paytoaddress);
                    model.PayToAddressList.Add(new SelectListItem { Text = "--Select--", Value = "0" });



                    model.CurrencyList = _service.GetCurrencies(model.InvoiceInfo.invoiceCurrencyID);
                    model.CurrencyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                    model.PaymentMethodList = _service.GetPaymentMethodsList(model.InvoiceInfo.preferredPaymentMethodID);
                    model.PaymentMethodList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                    model.PaymentTermsList = _service.GetPaymentTermsList(model.InvoiceInfo.invoiceTermID);
                    model.PaymentTermsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });


                    var contactId = 0;
                    int.TryParse(Session["ContactID"].ToString(), out contactId);
                    var userSecurityLevel = 0;
                    int.TryParse(Session["UserSecurityLevel"].ToString(), out userSecurityLevel);

                    var statusObj = _service.GetStatusList(contactId, userSecurityLevel, model.InvoiceInfo.invoiceStatusID, Session["UserGroupName"].ToString());
                    model.StatusList = statusObj.List;
                    model.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                    model.EnableStatusDropDown = statusObj.EnableDropdown;
                    model.GspMonthlyId = _service.GetGspMonthlyId();
                    model.InvoiceTotalPayment = _service.GetTotalInvoiceTotalPayment(invoiceId);
                    Session["invoiceDetails"] = model;
                    return View("~/Views/Invoice/InvoiceDetails.cshtml", model);
                }
                else
                {
                    TempData["ErrorMessage"] = "No information could be found for this invoice";
                    return RedirectToAction("GetInvoiceList");
                }

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Get Invoice Line Items by type and id
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="invoiceTypeId"></param>
        /// <returns></returns>
        public JsonResult GetInvoiceLineItemsDetail(int invoiceId, int invoiceTypeId, string vatRate, string vatTotal, string total)
        {
            try
            {
                var dt = _service.GetInvoiceDetailData(invoiceId, invoiceTypeId, vatRate, vatTotal, total);
                var data = "";
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        data = JsonConvert.SerializeObject(dt);
                    }
                    else
                    {
                        ViewBag.Message = "No Invoice Line Items Available";
                    }
                }
                else
                {
                    ViewBag.Message = "No Invoice Line Items Available";
                }

                var userType = "customer";
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    userType = "admin";
                }

                return Json(new { data = data, msg = ViewBag.Message, userType = userType }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteLineItemDetail(int invoiceId, string seqNum)
        {
            try
            {
                var invoiceTotal = _service.DeleteLineItemDetail(invoiceId, Convert.ToInt32(seqNum));
                var item = InvoiceLineItems.Where(x => x.LineItemSeqNum == seqNum).FirstOrDefault();
                InvoiceLineItems.Remove(item);

                return Json(new { data = invoiceTotal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Update Invoice Information 
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public JsonResult UpdateInvoice(FormCollection col)
        {
            try
            {
                var invoiceId = 0;
                Int32.TryParse(col["invoiceId"].ToString(), out invoiceId);
                var invoiceCurrencyId = 0;
                Int32.TryParse(col["invoiceCurrencyId"].ToString(), out invoiceCurrencyId);
                var billToAddressId = 0;
                Int32.TryParse(col["billToAddressId"].ToString(), out billToAddressId);
                var billFromAddressId = 0;
                Int32.TryParse(col["billFromAddressId"].ToString(), out billFromAddressId);
                var payToAddressId = 0;
                Int32.TryParse(col["payToAddressId"].ToString(), out payToAddressId);
                var invoiceStatusId = 0;
                Int32.TryParse(col["invoiceStatusId"].ToString(), out invoiceStatusId);
                var userId = 0;
                Int32.TryParse(Session["UserID"].ToString(), out userId);
                var prefPaymentId = 0;
                Int32.TryParse(col["prefPaymentId"].ToString(), out prefPaymentId);
                var paymentTermId = 0;
                Int32.TryParse(col["paymentTermId"].ToString(), out paymentTermId);
                var invoiceTypeId = 0;
                Int32.TryParse(col["invoiceTypeId"].ToString(), out invoiceTypeId);
                double vatrate = 0.00;
                double.TryParse(col["vatrate"].ToString(), out vatrate);
                dynamic sendDate = null;
                if (col["sendDate"] != null && col["sendDate"] != "")
                {
                    sendDate = DateTime.Parse(col["sendDate"].ToString());
                }
                dynamic invoicePeriodStart = null;
                if (col["invoicePeriodStart"] != null && col["invoicePeriodStart"] != "")
                {
                    invoicePeriodStart = DateTime.Parse(col["invoicePeriodStart"].ToString());
                }
                dynamic invoicePeriodEnd = null;
                if (col["invoicePeriodEnd"] != null && col["invoicePeriodEnd"] != "")
                {
                    invoicePeriodEnd = DateTime.Parse(col["invoicePeriodEnd"].ToString());
                }
                dynamic taxDate = null;
                if (col["taxDate"] != null && col["taxDate"] != "")
                {
                    taxDate = DateTime.Parse(col["taxDate"].ToString());
                }
                dynamic recdDate = null;
                if (col["recdDate"] != null && col["recdDate"] != "")
                {
                    recdDate = DateTime.Parse(col["recdDate"].ToString());
                }
                dynamic dueDate = null;
                if (col["dueDate"] != null && col["dueDate"] != "")
                {
                    dueDate = DateTime.Parse(col["dueDate"].ToString());
                }
                if (vatrate > 0)
                {
                    vatrate = vatrate / 100;
                }
                var count = _service.UpdateInvoice(invoiceId, invoiceCurrencyId, billToAddressId,
                     billFromAddressId, payToAddressId, sendDate, invoiceStatusId, invoicePeriodStart, invoicePeriodEnd, col["vatReg"].ToString(),
                     taxDate, userId, prefPaymentId, recdDate, col["paymentInstr"].ToString(), paymentTermId, col["comments"].ToString(), dueDate,
                     vatrate, col["po"].ToString(), invoiceTypeId);
                if (count > 0)
                {
                    var invoiceOwnerId = 0;
                    Int32.TryParse(col["invoiceOwnerId"].ToString(), out invoiceOwnerId);

                    if (invoiceOwnerId > 0)
                    {
                        _service.DeleteInvoiceOwnerByInvoiceId(invoiceId);
                        _service.AddInvoiceOwner(invoiceId, invoiceOwnerId, userId);
                    }

                    ViewBag.Message = "Invoice Updated Successfully";

                }
                else
                {
                    ViewBag.Message = "Something went wrong, please try again later";
                }

                return Json(new { data = count, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add content to PDF table to be export
        /// </summary>
        /// <param name="tableLayout"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, InvoicedetailsModel model)
        {
            float[] headers = { 15, 35, 15, 35 };  //Header Widths
            tableLayout.SetWidths(headers);        //Set the pdf headers
            tableLayout.WidthPercentage = 95;       //Set the PDF File witdh percentage
            int cols = 0;
            //Add Title to the PDF file at the top
            tableLayout.AddCell(new PdfPCell(new Phrase("Finance - Invoices - " + model.InvoiceInfo.companyInvoiceID, new Font(Font.FontFamily.HELVETICA, 13, 1, new iTextSharp.text.BaseColor(153, 51, 0)))) { Colspan = 4, Border = 0, PaddingBottom = 20, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 10, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 4, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Invoice Ref:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.companyInvoiceID, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Status:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.invoiceStatusName, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Amount:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.invoicetotal.ToString("F"), new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Invoice Owner:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceOwnerList.FirstOrDefault(x => x.Selected == true) != null ? model.InvoiceOwnerList.FirstOrDefault(x => x.Selected == true).Text : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("PO #:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.po, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Received:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.recdDate != null ? model.InvoiceInfo.recdDate.Value.ToShortDateString() : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Sent:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.sendDate != null ? model.InvoiceInfo.sendDate.Value.ToShortDateString() : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Tax Date:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.taxDate != null ? model.InvoiceInfo.taxDate.Value.ToShortDateString() : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


            tableLayout.AddCell(new PdfPCell(new Phrase("Due:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.dueDate != null ? model.InvoiceInfo.dueDate.ToShortDateString() : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Period End:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.invoicePeriodEnd != null ? model.InvoiceInfo.invoicePeriodEnd.Value.ToShortDateString() : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });


            tableLayout.AddCell(new PdfPCell(new Phrase("Period Begin:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.invoicePeriodStart != null ? model.InvoiceInfo.invoicePeriodStart.Value.ToShortDateString() : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Bill To Company:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.billtoco.ToString(), new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Bill From Company:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.billfromco.ToString(), new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Bill To Address:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.BillToAddressList.FirstOrDefault(x => x.Selected == true) != null ? model.BillfromAddressList.FirstOrDefault(x => x.Selected == true).Text : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Bill-From Address:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.BillfromAddressList.FirstOrDefault(x => x.Selected == true) != null ? model.BillfromAddressList.FirstOrDefault(x => x.Selected == true).Text : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Payment Terms:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.PaymentTermsList.FirstOrDefault(x => x.Selected == true) != null ? model.PaymentTermsList.FirstOrDefault(x => x.Selected == true).Text : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Pay-To Address:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.PayToAddressList.FirstOrDefault(x => x.Selected == true) != null ? model.PayToAddressList.FirstOrDefault(x => x.Selected == true).Text : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Pref. Payment Method:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.PaymentMethodList.FirstOrDefault(x => x.Selected == true) != null ? model.PaymentMethodList.FirstOrDefault(x => x.Selected == true).Text : "", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Vat Reg #:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.vatReg, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Created By:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.createdby, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Create Date:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.createDate.ToShortDateString(), new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Updated By:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.updatedby, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Last Updated:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.lastUpdate.ToShortDateString(), new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase("Comments:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.comments, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Payment Instructions:", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            tableLayout.AddCell(new PdfPCell(new Phrase(model.InvoiceInfo.paymentInstr, new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 8, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 4, Border = 0, PaddingTop = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 4, Border = 1, PaddingBottom = 10, HorizontalAlignment = Element.ALIGN_LEFT });

            var dt = _service.GetInvoiceDetailData(model.InvoiceId, model.InvoiceInfo.invoiceTypeID, model.InvoiceInfo.vatrate.ToString(), model.InvoiceInfo.vatTotal.ToString(), model.InvoiceInfo.invoicetotal.ToString());
            PdfPTable requestItmes = new PdfPTable(dt.Columns.Count);
            List<float> requestheaders = new List<float>();// { 50, 25, 25 };

            var hasDesc = false;
            var totalWidth = 100;

            if (dt.Rows.Count > 0)
            {

                foreach (DataColumn item in dt.Columns)
                {
                    AddCellToHeader(requestItmes, item.ColumnName);
                    if (item.ColumnName == "Description")
                    {
                        hasDesc = true;
                        requestheaders.Add(40);
                        totalWidth = totalWidth - 40;
                    }
                    else
                    {
                        var width = totalWidth / dt.Columns.Count;

                        if (hasDesc)
                        {
                            width = totalWidth / (dt.Columns.Count - 1);
                        }
                        requestheaders.Add(width);
                    }

                }
                requestItmes.SetWidths(requestheaders.ToArray());

                foreach (DataRow item in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (item[dt.Columns[i].ColumnName].ToString().Contains("<b>"))
                        {
                            AddCellToBodyWithBoldValues(requestItmes, item[dt.Columns[i].ColumnName].ToString().Replace("<b>", "").Replace("</b>", ""));
                        }
                        else
                        {
                            AddCellToBody(requestItmes, item[dt.Columns[i].ColumnName].ToString());
                        }
                    }
                }

                tableLayout.AddCell(new PdfPCell(requestItmes) { Colspan = 4, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            }

            

            return tableLayout;
        }

        // Method to add single cell to the header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 0, iTextSharp.text.BaseColor.WHITE))) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 2, BackgroundColor = new iTextSharp.text.BaseColor(0, 51, 102) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 0, iTextSharp.text.BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = iTextSharp.text.BaseColor.WHITE });
        }

        // Method to add single cell to the body with bold values
        private static void AddCellToBodyWithBoldValues(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = iTextSharp.text.BaseColor.WHITE });
        }
        /// <summary>
        /// Export invoice details to PDF
        /// </summary>
        /// <param name="invoiceCompanyId"></param>
        /// <returns></returns>
        public ActionResult ExportToPdf(string invoiceCompanyId) // added customerExportForAdmin by sunny gohar
        {
            var model = (InvoicedetailsModel)Session["invoiceDetails"];
            //Create PDF Table
            PdfPTable tableLayout = new PdfPTable(4);

            var content = Add_Content_To_PDF(tableLayout, model);

            iTextSharp.text.Image logoImg =
                    iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
            logoImg.ScaleAbsolute(250, 45);


            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {

                string filename = "Finance - Invoices -" + invoiceCompanyId;

                Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
                document.Add(logoImg);
                document.Add(new Chunk("\n"));
                document.Add(content);
                document.Add(new Chunk("\n"));

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ".pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
            }


            return null;
        }


        public JsonResult AddInvoiceLineItemDetails(NewInvoiceModel model) // added customerExportForAdmin by sunny gohar
        {
            try
            {
                decimal orderTotal = decimal.Zero;
                decimal.TryParse(model.OrderTotal, out orderTotal);

                decimal gsFee = decimal.Zero;
                decimal.TryParse(model.GSFee, out gsFee);

                decimal vat = decimal.Zero;
                decimal.TryParse(model.VAT, out vat);

                var obj = new InvoiceLineItemDetailsModel();

                if (!String.IsNullOrEmpty(model.Description))
                {
                    obj.Description = model.Description;
                }
                if (!String.IsNullOrEmpty(model.OrderNumber))
                {
                    obj.OrderNumber = model.OrderNumber;
                }
                if (!String.IsNullOrEmpty(model.OrderTotal))
                {
                    obj.OrderTotal = model.OrderTotal;
                }
                if (!String.IsNullOrEmpty(model.GSFee))
                {
                    obj.GSFee = model.GSFee;
                }
                if (!String.IsNullOrEmpty(model.PO))
                {
                    obj.PO = model.PO;
                }
                if (!String.IsNullOrEmpty(model.Quantity))
                {
                    obj.Quantity = model.Quantity;
                }
                if (!String.IsNullOrEmpty(model.Rate))
                {
                    obj.Rate = model.Rate;
                }
                if (!String.IsNullOrEmpty(model.VAT))
                {
                    obj.VAT = model.VAT;
                }
                if (!String.IsNullOrEmpty(model.Amount))
                {
                    obj.Amount = model.Amount;
                }
                if (!String.IsNullOrEmpty(model.Ticket))
                {
                    obj.Ticket = model.Ticket;
                }
                if (model.RowCount == 0)
                {
                    InvoiceLineItems = new List<InvoiceLineItemDetailsModel>();
                    LineItemSeqNum = 0;
                }
                LineItemSeqNum++;
                obj.LineItemSeqNum = LineItemSeqNum.ToString();
                InvoiceLineItems.Add(obj);


                Session["InvoiceLineItems"] = InvoiceLineItems;

                decimal invoiceTotal = _service.AddInvoiceLineItem(obj, model.InvoiceId, model.InvoiceTypeId, Convert.ToInt32(Session["UserID"]), LineItemSeqNum);
                if (invoiceTotal > 0)
                {
                    ViewBag.Message = "Invoice Added Successfully";
                }

                var columnNamesList = model.ColumnNames.Split(',').ToArray();

                var dt = ToDataTable(InvoiceLineItems);
                dt.Columns.Add("Action");
                foreach (DataRow row in dt.Rows)
                {
                    var value = "<a onclick='deleteRow(this," + row["LineItemSeqNum"].ToString() + "," + model.InvoiceId + ")' id='deleteItem' style='color:#3b9ad2;'> Delete </a>";
                    row["Action"] = value;
                }

                var data = JsonConvert.SerializeObject(dt);
                ViewBag.Message = "Line item added successfully";

                return Json(new { data = data, msg = ViewBag.Message, columnNames = columnNamesList, invoiceTotal = invoiceTotal }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);

                dataTable.Columns.Add(prop.Name, type);


            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public NewInvoiceModel prepareModel(NewInvoiceModel model,string invoiceTemplateName=null)
        {
            try
            {
                model.InvoiceTypeList = _service.GetInvoiceTypeList().Select(x => new SelectListItem()
                {
                    Text = x.Value,
                    Value = x.Key.ToString(),
                    Selected = x.Key == model.InvoiceTypeId ? true : false
                }).ToList();
                model.InvoiceTypeActiveOrInActiveList = _service.GetInvoiceTypeActiveOrInActiveList();
                model.BillFromCompanyList = _service.GetCompanyList(Convert.ToInt32(model.invoiceBillFromCompanyDrp));
                model.BillToCompanyList = _service.GetCompanyList(Convert.ToInt32(model.invoiceBillToCompanyDrp));
                model.InvoiceOBOCompanies = _service.GetCompanyList();
                model.CurrencyList = _service.GetCurrencies();
                model.CurrencyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.InvoiceOwnerList = _service.GetInvoiceOwnerList(Session["UserGroupName"].ToString());
                model.InvoiceOwnerList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.PayToAddressList = _service.GetPartyLocationList(Session["UserGroupName"].ToString());
                model.PayToAddressList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.BillfromAddressList = _service.GetPartyLocationList(Session["UserGroupName"].ToString(), parentCompanyId: Convert.ToInt32(model.invoiceBillFromCompanyDrp));
                model.BillfromAddressList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.BillToAddressList = _service.GetPartyLocationList(Session["UserGroupName"].ToString(), parentCompanyId: Convert.ToInt32(model.invoiceBillToCompanyDrp));
                model.BillToAddressList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.PaymentTermsList = _service.GetPaymentTermsList();
                model.PaymentTermsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.PaymentMethodList = _service.GetPaymentMethodsList();
                model.PaymentMethodList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.InvoiceTypeinfo = _service.GetInvoiceTypeDetailByInvoiceTypeId(model.InvoiceTypeId);
                model.InvoiceRecipientList = _service.GetInvoiceRecipientList(invoiceTypeId: model.InvoiceTypeId);
                model.InvoiceDetailColumnsList = _service.GetInvoiceDetailColumnNames(model.InvoiceTypeId);
                model.ReOccursList = _service.GetReOccurs();
                model.ReOccursList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                model.InvoiceType = _service.GetInvoiceType(model.InvoiceTypeId);
                model.InvoiceAmountUSD = model.InvoiceType.InvoiceAmountUSD!=null? model.InvoiceType.InvoiceAmountUSD.ToString():"";
                if (!String.IsNullOrEmpty(invoiceTemplateName))
                {
                    model.InvoiceType.InvoiceTypeName = invoiceTemplateName;
                }
                var invoiceAmount = decimal.Zero;
                Decimal.TryParse(model.InvoiceAmount, out invoiceAmount);
                var invoiceCurrency = 0;
                Int32.TryParse(model.invoiceCurrencyDrp, out invoiceCurrency);
                model.InvoiceAmountUSD = _service.GetInvoiceAmountUSD(invoiceCurrency, invoiceAmount);

                return model;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

    }
}
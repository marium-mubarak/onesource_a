﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : July 15 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Exchange Rate alert detail"
    /// </summary>
    public class ExchangeRateController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of Exchange Rate alert
        /// </summary>
        /// <returns></returns>

      
        public ActionResult Index()
        {

            if (Session["UserID"] != null)
            {
                try
                {
                    ExchageRatePresenter cp = new ExchageRatePresenter();
                    CustomerPresenter custp = new CustomerPresenter();
                    ExchageRateModel model = new ExchageRateModel
                    {
                        Countries = cp.GetCountries(),
                        ExchangeRateAlerts = cp.GetExchageRateAlert(),
                        Customers = custp.GetCustomers()
                    };
                    @ViewBag.AddEditAccess = isAddEditAllow("ExchangeRateAlert");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for adding and updating the exchange rate alert record
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddExchangeRateAlert(HttpPostedFileBase attachFile, tblExchangeRateAlert model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!ValidateFields(model, attachFile)) return RedirectToAction("Index");


                    if (attachFile != null && attachFile.ContentLength > 0)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException("File found", routeData);

                        var fileName = Path.GetFileName(attachFile.FileName).Replace("^","");
                        var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                        string path = string.Empty;
                        if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".msg" || fileExtension == ".png" || fileExtension == ".csv" || fileExtension == ".txt")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {


                                var docId = System.Guid.NewGuid().ToString();
                                //path =
                                //    Path.Combine(ConfigurationManager.AppSettings["UploadURLExchangeRate"] + "/",
                                //        docId + "^" + fileName);
                                path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURLExchangeRate"] + "/"), docId + "^" + fileName);

                                //using (var newUserContext = new Impersonator(ConfigurationManager.AppSettings["UploaderUserName"], ConfigurationManager.AppSettings["UploadDomain"], ConfigurationManager.AppSettings["UploaderPassword"]))
                                //{
                                attachFile.SaveAs(path);
                                //}
                                model.FileName = docId + "^" + fileName;
                                model.FilePath = path;

                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                                return RedirectToAction("Index");
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";
                            return RedirectToAction("Index");
                        }
                    }
                    else if (string.IsNullOrEmpty(model.FileName))
                    {
                        model.FileName = null;
                    }



                    ExchageRatePresenter cp = new ExchageRatePresenter();
                    int count;
                    //For Edit
                    if (model.ExchangeRateAlertId != 0)
                    {
                        count = cp.EditExchageRateAlert(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Alert has been edited";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddExchageRateAlert(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Alert has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This method is responsible for checking exchange rate alert data comming from request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ValidateFields(tblExchangeRateAlert model, HttpPostedFileBase file)
        {


            if (model.CountryId == 0)
            {
                TempData["Message"] = "Please Provide Country";
                return false;
            }
            if (Convert.ToDateTime(model.StartDate).Date.Year == 1)
            {
                TempData["Message"] = "Please Provide Start Date";
                return false;
            }
            if (Convert.ToDateTime(model.EndDate).Date.Year > 1)
            {
                if (model.StartDate.Date > Convert.ToDateTime(model.EndDate).Date)
                {
                    TempData["Message"] = "Start Date must be less than End Date";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(model.Alert))
            {
                TempData["Message"] = "Please Provide Alert";
                return false;
            }

            return true;
        }

        /// <summary>
        /// This Action is responsible for deleting the exchange rate alert record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteAlert(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int exchangeRateId;
                    if (!int.TryParse(id, out exchangeRateId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    ExchageRatePresenter cp = new ExchageRatePresenter();
                    int count = cp.DeleteExchageRateAlert(exchangeRateId);
                    if (count > 0) TempData["Message"] = "Alert has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This Action is responsible for downloading the attachment
        /// </summary>
        /// <param name="csaId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownloadAttachement(string csaId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(csaId))
                    {
                        int cSAId;
                        if (int.TryParse(csaId, out cSAId))
                        {
                            ExchageRatePresenter csp = new ExchageRatePresenter();
                            string filepath = csp.GetFilePathById(cSAId);
                            string contentType = "application/octet-stream";

                            if (!System.IO.File.Exists(filepath))
                            {
                                return View("~/Views/Shared/PageNotFound.cshtml");
                            }
                            return File(filepath, contentType, Path.GetFileName(filepath));
                        }
                        return RedirectToAction("Index");
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            return RedirectToAction("Login", "Account");
        }
    }
    public class Impersonator : IDisposable
    {
        public Impersonator(string userName, string domainName, string password)
        {
            ImpersonateValidUser(userName, domainName, password);
        }

        public void Dispose()
        {
            UndoImpersonation();
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern int LogonUser(string lpszUserName, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int DuplicateToken(IntPtr hToken, int impersonationLevel, ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern bool CloseHandle(IntPtr handle);

        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_PROVIDER_DEFAULT = 0;

        private WindowsImpersonationContext _impersonationContext = null;

        private void ImpersonateValidUser(string userName, string domain, string password)
        {
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            try
            {
                if (!RevertToSelf())
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }

                if (LogonUser(userName, domain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, ref token) == 0)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }
                if (DuplicateToken(token, 2, ref tokenDuplicate) == 0)
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }
                var tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                _impersonationContext = tempWindowsIdentity.Impersonate();
            }
            finally
            {
                if (token != IntPtr.Zero)
                {
                    CloseHandle(token);
                }
                if (tokenDuplicate != IntPtr.Zero)
                {
                    CloseHandle(tokenDuplicate);
                }
            }
        }

        private void UndoImpersonation()
        {
            if (_impersonationContext != null)
            {
                _impersonationContext.Undo();
            }
        }
    }
}

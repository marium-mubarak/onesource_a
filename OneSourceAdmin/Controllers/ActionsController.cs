﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
 

namespace OneSourceAdmin.Controllers
{
    public class ActionsController : BaseController
    {
        private ActionsService aService;

        public ActionsController()
        {
            aService = new ActionsService();
        }

        public ActionResult Index()
        {
            try
            {
                var model = aService.GetActionsList();
                return View("~/Views/Actions/List.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public JsonResult GetActionDetails(int actionId)
        {
            try
            {
                var data = aService.GetActionDetailsById(actionId);

                if (data != null)
                {

                    var newData = new
                    {

                        StartDate = data.StartDate.Value.ToString("G"),
                        EndDate = data.EndDate.Value.ToString("G"),
                        ActionName = data.ActionName,
                        IsError = data.IsError != null && data.IsError.Value ? "Yes" : "No",
                        TicketId = data.TicketId != null ? data.TicketId.ToString() : "-",
                        UserName = data.UserName,
                        ActionStatusName = data.ActionStatusName,
                        WorkflowName = data.WorkflowName,
                        ActionId = data.ActionId,
                        Comments = data.Comments,
                        WorkflowId = data.WorkflowId

                    };

                    return Json(newData, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NewAction()
        {
            try
            {
                var model = new ActionsModel();
                model.ActionsList = aService.GetActionsListToModify();
                return View("~/Views/Actions/NewAction.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult GetFieldsByAction(int actionId)
        {
            try
            {
                var data = aService.GetFieldsByActionId(actionId);
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetActionListsByName(string glistName)
        {
            try
            {
                var data = aService.GetActionListsByName(glistName);
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetActionNotificationsFieldsSample()
        {
            try
            {
                var data = aService.GetFieldTypeRelationList();
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult AddUpdateAction(FormCollection fmCollection)
        {
            try
            {
                var data = aService.AddUpdateActionFields(fmCollection, adminSetting);

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RunAction(int actionId)
        {
            try
            {
                if (actionId > 0)
                {
                    var chkStat = aService.RunAction(actionId, adminSetting);

                    return Json(chkStat, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }






    }
}
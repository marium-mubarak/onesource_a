﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Data;
using System.Data.OleDb;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html;
using System.Text.RegularExpressions;

namespace OneSourceAdmin.Controllers

{
    /// <summary>
    /// Date created : June 19 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing ticket tracking detail"
    /// </summary>
    public class TicketSearchController : BaseController
    {
        private TicketSearchPresenter tsp;
        public TicketSearchController()
        {
            tsp = new TicketSearchPresenter();
        }
        // GET: TicketSearch
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This action is responsible for providing ticket search view
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult TicketSearch(int companyId)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    TicketSearchModel model = tsp.GetTicketSearchModel("ISD", companyId, string.Empty, Session["UserID"].ToString());
                    return View("~/Views/TicketSearch/TicketSearchISD.cshtml", model);
                }
                if (Session["UserGroupName"].ToString() == "SFA")
                {
                    TicketSearchModel model = tsp.GetTicketSearchModel("SFA", companyId, Session["SFAID"].ToString(), Session["UserID"].ToString());
                    return View("~/Views/TicketSearch/TicketSearchSFA.cshtml", model);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for Ticket Search request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult TicketSearchRequest(TicketSearchModel model)
        {
            try
            {
                Session["TicketSearchModel"] = model;
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    var data = tsp.GetSearchedTickets(model, Session["DateFormatID"].ToString(), Session["UserGroupName"].ToString(), model.SFAId.ToString(), Session["UserID"].ToString(), 500);
                    if (data.LstTicket.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("TicketSearch", new { companyId = model.CompanyId });
                    }
                    Session["SearchTicketResult"] = data;
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    var data = tsp.GetSearchedTickets(model, Session["DateFormatID"].ToString(), Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), 500);
                    if (data.LstTicket.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("TicketSearch", new { companyId = model.CompanyId });
                    }
                    Session["SearchTicketResult"] = data;
                }
                return RedirectToAction("SearchTicketList");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for providing ticket list view
        /// </summary>
        /// <param name="requiredRecord"></param>
        /// <param name="requiredPageSize"></param>
        /// <param name="TicketIdToDelFromList"></param>
        /// <returns></returns>
        public ActionResult SearchTicketList(int requiredRecord, int requiredPageSize, int TicketIdToDelFromList)
        {
            try
            {
                if (requiredPageSize > requiredRecord)
                {
                    requiredPageSize = 50;
                    requiredRecord = 500;
                }

                ViewBag.PageSize = requiredPageSize;
                ViewBag.RequiredRecord = requiredRecord;

                if (requiredRecord > 5000)
                {
                    requiredRecord = 5000;
                    ViewBag.RequiredRecord = 5000;
                }
                if (requiredPageSize > 1000)
                {
                    ViewBag.PageSize = 1000;
                    requiredPageSize = 1000;
                }



                TicketListModel model = new TicketListModel();


                if (Session["SearchTicketResult"] != null)
                {
                    model = (TicketListModel)Session["SearchTicketResult"];

                    if (TicketIdToDelFromList > 0)
                    {
                        var item = model.LstTicket.FirstOrDefault(x => x.ticketid == TicketIdToDelFromList);
                        model.LstTicket.Remove(item);
                    }

                    if (model.LstTicket.Count != requiredRecord)
                    {
                        if (requiredRecord == 50001)
                        {
                            ViewBag.RequiredRecord = 50001;
                            if (Session["UserGroupName"].ToString() == "ISD")
                            {
                                var smodel = (TicketSearchModel)Session["TicketSearchModel"];
                                model = tsp.GetSearchedTickets(smodel, Session["DateFormatID"].ToString(), Session["UserGroupName"].ToString(), smodel.SFAId.ToString(), Session["UserID"].ToString(), model.TotalRecord);
                            }
                            else if (Session["UserGroupName"].ToString() == "SFA")
                            {
                                var smodel = (TicketSearchModel)Session["TicketSearchModel"];
                                model = tsp.GetSearchedTickets(smodel, Session["DateFormatID"].ToString(), Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), model.TotalRecord);

                            }
                        }
                        else
                        {
                            if (Session["UserGroupName"].ToString() == "ISD")
                            {
                                var smodel = (TicketSearchModel)Session["TicketSearchModel"];
                                model = tsp.GetSearchedTickets(smodel, Session["DateFormatID"].ToString(), Session["UserGroupName"].ToString(), smodel.SFAId.ToString(), Session["UserID"].ToString(), requiredRecord);
                            }
                            else if (Session["UserGroupName"].ToString() == "SFA")
                            {
                                var smodel = (TicketSearchModel)Session["TicketSearchModel"];
                                model = tsp.GetSearchedTickets(smodel, Session["DateFormatID"].ToString(), Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), requiredRecord);

                            }

                        }
                        Session["SearchTicketResult"] = model;
                    }

                }

                ViewBag.TotalCount = model.TotalRecord;
                return View("~/Views/TicketSearch/TicketList.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for ticket detail view 
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public ActionResult TicketDetail(int ticketId)
        {
            try
            {

                Session["ticketId"] = ticketId;
                Session["DispositionBackUrl"] = null;
                Session["AssetBackUrl"] = null;
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    ViewBag.Message = TempData["Message"];
                    var detailModel = tsp.GetTicketDetail(ticketId, "ISD", 0, Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]));
                    return View("~/Views/TicketSearch/TicketDetail.cshtml", detailModel);
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    ViewBag.Message = TempData["Message"];
                    var detailModel = tsp.GetTicketDetail(ticketId, "SFA", Convert.ToInt32(Session["SFAID"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]));
                    return View("~/Views/TicketSearch/TicketDetail.cshtml", detailModel);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for ticket update
        /// </summary>
        /// <param name="detailModel"></param>
        /// <param name="addUModel"></param>
        /// <returns></returns>
        public ActionResult UpdateTicket(sp_getTicketDetail_AdminR_Result detailModel, TicketAdditionalUpdateModel addUModel)
        {
            try
            {
                if (detailModel != null && addUModel != null)
                {
                    int count = tsp.UpdateTicket(detailModel, addUModel, Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["ContactID"]), Session["UserGroupName"].ToString());
                    TempData["Message"] = count == 0 ? "No record was updated." : "Record updated successfully.";
                    Session["FinalSerialNumber"] = null;
                    Session["FinalAssetId"] = null;
                    Session["FinalAssetTag"] = null;
                    Session["FinalAssetDescription"] = null;
                    Session["FinalAssetLocation"] = null;

                    if (count > 0)
                    {
                        if (Session["SearchTicketResult"] != null)
                        {
                            var status = tsp.GetStatus(Convert.ToInt32(detailModel.tktstatusid), detailModel.parentcompanyid);
                            string closeDate = string.Empty;
                            var priority = tsp.GetPriority(detailModel.priorityid, detailModel.parentcompanyid);
                            if (detailModel.closedate != null)
                            {
                                closeDate = Convert.ToDateTime(detailModel.closedate).ToString("MM/dd/yyyy");
                            }
                            var model = (TicketListModel)Session["SearchTicketResult"];
                            model.LstTicket.Where(x => x.ticketid == detailModel.ticketid).ToList().ForEach(x =>
                            {
                                x.tktstatusname = status;
                                x.CloseDate = closeDate;
                                x.priorityname = priority;
                            });
                        }
                    }

                    return RedirectToAction("TicketDetail", new { @ticketId = detailModel.ticketid });
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for ticket status schecdule
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public ActionResult GetTicketStatusSchedule(int ticketId)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
                        return PartialView("~/Views/TicketSearch/_ticketStatusSchedulePopup.cshtml", tsp.GetTicketStatusSchedule(ticketId));
                    }
                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }
                        return Json("Error Occured", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Error Occured", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for updation of ticket status schedule
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult UpdateTicketStatusSchedule(List<tblTktStatusSchedule> model)
        {
            try
            {
                if (model.Count > 0)
                {
                    int count = tsp.UpdateTicketStatusSchedule(model, Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]), Session["UserName"].ToString());
                    TempData["Message"] = count == 0 ? "No record was updated." : "Record updated successfully.";
                    return RedirectToAction("TicketDetail", new { @ticketId = model[0].ticketid });
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for Searching of asset tag
        /// </summary>
        /// <param name="parentCompanyId"></param>
        /// <returns></returns>
        public ActionResult GetAssetTagSearchData(int parentCompanyId)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            var model = tsp.GetAssetTagSearchPopupData(parentCompanyId, Session["UserGroupName"].ToString(), 0);
                            return PartialView("~/Views/TicketSearch/_assetTagSearchPopup.cshtml", model);
                        }
                        else
                        {
                            var model = tsp.GetAssetTagSearchPopupData(parentCompanyId, Session["UserGroupName"].ToString(), Convert.ToInt32(Session["SFAID"]));
                            return PartialView("~/Views/TicketSearch/_assetTagSearchPopup.cshtml", model);
                        }


                    }
                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }
                        return Json("0", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for providing asset information view 
        /// </summary>
        /// <param name="modelRecieve"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAssetGrid(AssetPopupModel modelRecieve)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {

                        if (Session["AssetBackUrl"] == null)
                        {
                            if (Request.UrlReferrer != null)
                            {
                                Session["AssetBackUrl"] = Request.UrlReferrer;
                            }
                        }
                        Session["serialNumber"] = modelRecieve.SerialNumber ?? "";
                        Session["assetLocationId"] = modelRecieve.AssetLocationId;
                        Session["assetStatusId"] = modelRecieve.AssetStatusId;
                        Session["assetCategoryId"] = modelRecieve.AssetCategoryIds ?? "";
                        Session["assetManufactureId"] = modelRecieve.AssetManufactureIds ?? "";
                        Session["AssetPartNumber"] = modelRecieve.AssetPartNumber ?? "";
                        Session["AssetTag"] = modelRecieve.AssetTag ?? "";

                        return RedirectToAction("AssetGridActionResult", new { @companyId = modelRecieve.ParentCompanyId });
                    }

                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }

                        return View("~/Views/Shared/ErrorPage.cshtml");

                    }
                }
                else
                    return RedirectToAction("Login", "Account");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for asset grid
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult AssetGridActionResult(int companyId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.BackUrl = Session["AssetBackUrl"];
                    var model = new AssetDataGridModel();
                    var userGroupName = Session["UserGroupName"].ToString();
                    var serialNumber = Session["serialNumber"].ToString();
                    var assetLocationId = Session["assetLocationId"].ToString();
                    var assetStatusId = Session["assetStatusId"].ToString();
                    var assetCategoryId = Session["assetCategoryId"].ToString();
                    var assetManufactureId = Session["assetManufactureId"].ToString();
                    var assetPartNumber = Session["AssetPartNumber"].ToString();
                    var assetTag = Session["AssetTag"].ToString();
                    Session["assetCategoryId"] = Session["assetCategoryId"].ToString();
                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        model.AssetDataGridDataList = tsp.GetAssetDataGridDataList(companyId.ToString(), userGroupName, serialNumber, assetLocationId,
                        assetStatusId, assetCategoryId, assetManufactureId, assetPartNumber, assetTag, "0");
                    }
                    else
                    {
                        model.AssetDataGridDataList = tsp.GetAssetDataGridDataList(companyId.ToString(), userGroupName, serialNumber, assetLocationId,
                        assetStatusId, assetCategoryId, assetManufactureId, assetPartNumber, assetTag, Session["SFAID"].ToString());
                    }
                    return View("AssetDataGrid", model);

                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }

                    return View("~/Views/Shared/ErrorPage.cshtml");

                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This action is responsible for asset request data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult GetAssetRequestData(AssetDataGridModel model)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
                        Session["FinalSerialNumber"] = model.SerialNumber ?? "";
                        Session["FinalAssetId"] = model.AssetId;
                        Session["FinalAssetTag"] = model.AssetTag ?? "";
                        Session["FinalAssetDescription"] = model.AssetDescription ?? "";
                        Session["FinalAssetLocation"] = model.AssetLocation ?? "";

                        return RedirectToAction("TicketDetail", new { @ticketId = Convert.ToInt32(Session["ticketId"]) });
                    }

                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }

                        return View("~/Views/Shared/ErrorPage.cshtml");

                    }
                }
                else
                    return RedirectToAction("Login", "Account");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for providing disposition assets
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDispositionAssets()
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    try
                    {
                        if (Session["DispositionBackUrl"] == null)
                        {
                            if (Request.UrlReferrer != null)
                            {
                                Session["DispositionBackUrl"] = Request.UrlReferrer;
                            }
                        }

                        ViewBag.Message = TempData["Message"];
                        return View("~/Views/TicketSearch/AssetDisposition.cshtml", tsp.GetDispositionAssets(Convert.ToInt32(Session["ticketId"])));
                    }
                    catch (Exception ex)
                    {
                        var routeData = this.ControllerContext.RouteData;
                        LogException(ex.Message, routeData);
                        if (ex.InnerException != null)
                        {
                            LogException(ex.InnerException.ToString(), routeData);
                        }
                        return Json("Error Occured", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Error Occured", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for adding disposition asset summary
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddDispositionAssetSummary(TicketDispositionAssetsModel model)
        {
            try
            {
                int count = tsp.AddDispositionAssetSummary(Convert.ToInt32(Session["ticketId"]), model.CurrencyId, model.PreTaxInvoiceAmount, Convert.ToInt32(Session["UserID"]));
                TempData["Message"] = count > 0 ? "Record successfully updated" : "No record was updated";
                return RedirectToAction("GetDispositionAssets");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for uploading disposition assets in bulk
        /// </summary>
        /// <param name="attachFile"></param>
        /// <param name="currencyId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadBulkDispositionAssets(HttpPostedFileBase attachFile, int currencyId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName);
                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                                attachFile.SaveAs(path);
                                int? daid = tsp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path, ConfigurationManager.AppSettings["UploadURL"] + "/" + docId + "^" + fileName, Convert.ToInt32(Session["UserID"]));

                                DataSet ds = GetExcelFileData(path);
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {

                                        List<string> columns = new List<string>() { "model","serial_number","asset_tag" ,"oem",
                                                          "category","screen_size","processor","ram","hard_drive","service_type","graphics_card_part_number" ,"graphics_card_serial_number",
                                                          "route_switch_processor_part_number","route_switch_processor_serial_number","optical_drive_part_number","optical_drive_serial_number","comments","remarketing_amount"};


                                        int count = 0;
                                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                                        {
                                            string isfound = columns.Where(x => x == ds.Tables[0].Columns[i].ColumnName.ToLower().Trim()).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(isfound))
                                            {
                                                count++;
                                            }
                                        }
                                        if (count != 18)
                                        {
                                            TempData["Message"] = "File Data Is Mismatch. Please Use Given Template";
                                            return RedirectToAction("GetDispositionAssets");
                                        }

                                        TicketDispositionAssetBulkUploadModel result = GetListOfUploadTicketDispositionAssets(ds.Tables[0]);

                                        if (result.UploadTicketDispositionAssets != null)
                                        {
                                            UploadDispositionAssets(result.UploadTicketDispositionAssets, currencyId, Convert.ToInt32(Session["ticketId"]), daid);
                                        }
                                        else
                                        {
                                            TempData["Message"] = result.ErrorMsg;
                                        }



                                        return RedirectToAction("GetDispositionAssets");

                                    }
                                    else
                                    {
                                        TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                    }
                                }
                                else
                                {
                                    TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                }

                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                            }

                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .xls,.xlsx";
                        }

                    }
                    return RedirectToAction("GetDispositionAssets");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    TempData["Message"] = "Invalid file.";
                    return RedirectToAction("GetDispositionAssets");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }

        /// <summary>
        /// This action is responsible for providing list of disposition assets for upload
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private TicketDispositionAssetBulkUploadModel GetListOfUploadTicketDispositionAssets(DataTable dt)
        {

            TicketDispositionAssetBulkUploadModel result = new TicketDispositionAssetBulkUploadModel();
            List<UploadTicketDispositionAssets> lst = new List<UploadTicketDispositionAssets>();
            UploadTicketDispositionAssets obj;
            int count = 0;
            foreach (DataRow row in dt.Rows)
            {
                obj = new UploadTicketDispositionAssets();
                DateTime datetime;

                if (string.IsNullOrEmpty(Convert.ToString(row["MODEL"]))) { result.ErrorMsg = "Incorrect Model in excel file on Row No. " + (count + 1); result.UploadTicketDispositionAssets = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["SERIAL_NUMBER"]))) { result.ErrorMsg = "Incorrect Serial Number in excel file on Row No. " + (count + 1); result.UploadTicketDispositionAssets = null; return result; }


                obj.Model = row["MODEL"].ToString();
                obj.SerialNumber = row["SERIAL_NUMBER"].ToString();
                obj.AssetTag = row["ASSET_TAG"].ToString();
                obj.OEM = row["OEM"].ToString();
                obj.Category = row["CATEGORY"].ToString();
                obj.ScreenSize = row["SCREEN_SIZE"].ToString();
                obj.Processor = row["PROCESSOR"].ToString();
                obj.RAM = row["RAM"].ToString();
                obj.HardDrive = row["HARD_DRIVE"].ToString();
                obj.ServiceType = row["SERVICE_TYPE"].ToString();
                obj.GraphicsCardPartNumber = row["GRAPHICS_CARD_PART_NUMBER"].ToString();
                obj.GraphicsCardSerialNumber = row["GRAPHICS_CARD_SERIAL_NUMBER"].ToString();
                obj.RouteSwitchProcessorPartNumber = row["ROUTE_SWITCH_PROCESSOR_PART_NUMBER"].ToString();
                obj.RouteSwitchProcessorSerialNumber = row["ROUTE_SWITCH_PROCESSOR_SERIAL_NUMBER"].ToString();
                obj.OpticalDrivePartNumber = row["OPTICAL_DRIVE_PART_NUMBER"].ToString();
                obj.OpticalDriveSerialNumber = row["OPTICAL_DRIVE_SERIAL_NUMBER"].ToString();
                obj.Comments = row["COMMENTS"].ToString();
                obj.RemarketingAmount = row["REMARKETING_AMOUNT"].ToString();

                lst.Add(obj);
                count++;
            }
            result.UploadTicketDispositionAssets = lst;
            return result;
        }

        /// <summary>
        /// this action is responsible for getting data from excel file to dataset
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private DataSet GetExcelFileData(string filePath)
        {

            string connString = string.Empty;

            var fileExt = System.IO.Path.GetExtension(filePath);

            if (fileExt == ".xlsx")
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";";
            // string a =   @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";";
            else if (fileExt == ".xls")
                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=Excel 8.0";

            OleDbConnection oledbConn = new OleDbConnection(connString);
            oledbConn.Open();

            DataTable Sheets = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            DataSet ds = new DataSet();
            foreach (DataRow dr in Sheets.Rows)
            {
                string sht = dr[2].ToString().Replace("'", "");
                if (sht.Contains('$'))
                {
                    OleDbDataAdapter dataAdapter = new OleDbDataAdapter("select * from [" + sht + "]", oledbConn);
                    dataAdapter.Fill(ds);
                }

            }

            return ds;


        }

        /// <summary>
        /// This action is responsible for uploading disposition assets
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currencyId"></param>
        /// <param name="ticketId"></param>
        /// <param name="daid"></param>
        /// <returns></returns>
        public ActionResult UploadDispositionAssets(List<UploadTicketDispositionAssets> model, int currencyId, int ticketId, int? daid)
        {
            try
            {
                if (model != null)
                {

                    int count = tsp.UploadDispositionAssets(model, currencyId, ticketId, daid, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["DateFormatID"]));
                    TempData["Message"] = count > 0 ? "Disposition Asset(s) has been added" : "No record has been added";
                }
                else
                {
                    TempData["Message"] = "Provide provide correct data";
                }
                return RedirectToAction("GetDispositionAssets");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for downloading template
        /// </summary>
        /// <returns></returns>
        public ActionResult DownloadTemplate()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    string filepath = string.Empty;
                    filepath = Server.MapPath(ConfigurationManager.AppSettings["TempAssetURL"]);
                    string contentType = "application/octet-stream";
                    return File(filepath, contentType, Path.GetFileName(filepath));
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }

        /// <summary>
        /// this action is reponsible for deleting disposition asset
        /// </summary>
        /// <param name="dispositionAssetIds"></param>
        /// <returns></returns>
        public JsonResult DeleteDispositionAssets(string dispositionAssetIds)
        {
            int count = tsp.DeleteDispositionAssets(dispositionAssetIds);
            return Json(count > 0 ? 1 : count, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is responsible for deleting attachment
        /// </summary>
        /// <param name="daid"></param>
        /// <returns></returns>
        public JsonResult DeleteAttachment(int daid)
        {
            if (daid > 0)
            {
                try
                {
                    int count = 0;
                    var url = tsp.GetRequestedAttachmentUrl(daid);
                    if (!string.IsNullOrEmpty(url))
                    {
                        string path = Server.MapPath(url);
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                        count = tsp.DeleteAttachment(daid);
                    }
                    return Json(count > 0 ? 1 : -1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// This action is responsible for marking ticket attachment Isactive false
        /// </summary>
        /// <returns></returns>
        public JsonResult MarkIsActiveTicketAttachment(int daid)//Adnan 19-june-2019
        {
            if (daid > 0)
            {
                try
                {
                    int count = 0;
                   
                    count = tsp.MarkIsActiveTicketAttachment(daid, Convert.ToInt32(Session["UserID"]));
                   
                    return Json(count > 0 ? 1 : -1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is responsible for getting ticket history
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTicketHistory()
        {
            try
            {
                var model = tsp.GetTicketHistory(Convert.ToInt32(Session["ticketId"]));
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    return PartialView("~/Views/TicketSearch/ISD/_TicketHistory.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/TicketSearch/SFA/_TicketHistory.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for updating ticket log
        /// </summary>
        /// <param name="ticketLogID"></param>
        /// <param name="isPublic"></param>
        /// <returns></returns>
        public JsonResult UpdateTicketLog(int ticketLogID, bool isPublic)
        {
            int count = tsp.UpdateTicketLog(ticketLogID, Convert.ToInt32(Session["UserID"]), isPublic);
            return Json(count > 0 ? 1 : count, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is responsible for updating ticket comments
        /// </summary>
        /// <param name="comments"></param>
        /// <returns></returns>
        public ActionResult UpdateTicketComment(string hcomments)
        {
            int count = tsp.UpdateTicketComment(Convert.ToInt32(Session["ticketId"]), Convert.ToInt32(Session["ContactID"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]), hcomments);
            TempData["Message"] = count > 0 ? "Comments has been added successfully" : "No comments has been added";
            tsp.AddTicketCommentNotification(Convert.ToInt32(Session["ticketId"]), Convert.ToInt32(Session["UserID"]), hcomments);
            return RedirectToAction("TicketDetail", new { @ticketId = Convert.ToInt32(Session["ticketId"]) });
        }

        /// <summary>
        /// This action is responsible for updating ticket resolution
        /// </summary>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public ActionResult UpdateTicketResolution(string resolution)
        {
            int count = tsp.UpdateTicketResolution(Convert.ToInt32(Session["ticketId"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]), resolution);
            TempData["Message"] = count > 0 ? "Resolution has been added successfully" : "No comments has been added";
            return RedirectToAction("TicketDetail", new { @ticketId = Convert.ToInt32(Session["ticketId"]) });
        }

        /// <summary>
        /// This action is responsible for getting ticket attachments
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTicketAttachment()
        {
            try
            {
                var model = tsp.GetTicketAttachment(Convert.ToInt32(Session["ticketId"]));
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    return PartialView("~/Views/TicketSearch/ISD/_TicketAttachment.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/TicketSearch//SFA/_TicketAttachment.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for adding ticket attachment
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="attachFile"></param>
        /// <returns></returns>
        public JsonResult AddTicketAttachment(int ticketId, HttpPostedFileBase attachFile)
        {
            var msg = string.Empty;
            var isSuccess = false;
            try
            {
                if (attachFile != null && attachFile.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(attachFile.FileName).Replace("^", "");
                    var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                    string path = string.Empty;
                    if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".csv" || fileExtension == ".png" || fileExtension == ".txt")
                    {
                        if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                        {
                            var docId = System.Guid.NewGuid().ToString();
                            path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                            attachFile.SaveAs(path);
                            int? count = tsp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path, ConfigurationManager.AppSettings["UploadURL"] + "/" + docId + "^" + fileName, Convert.ToInt32(Session["UserID"]), ticketId);
                            if (count <= 0)
                            {
                                msg = "File Attaching Error";
                            }
                            else
                            {
                                string tomail = string.Empty;
                                var arTypeName = tsp.GetArtypeName(ticketId);
                                if (arTypeName.Contains("Asset Disposition"))
                                {
                                    string to = string.Empty, from = string.Empty;
                                    var ADAtt = tsp.GetADAttachmentDetail(ticketId);
                                    if (ADAtt == null)
                                    {
                                        tomail = ConfigurationManager.AppSettings["AssetDispAttachToEmail"];
                                    }
                                    else
                                    {
                                        tomail = ADAtt.ConEmail;
                                        if (!(ADAtt.CountryId > 1)) //US
                                        {
                                            if (ADAtt.CompanyId == 1609)
                                            {
                                                tsp.SendAssetDispAttachEmail(tomail, ConfigurationManager.AppSettings["AssetDispAttachFromEmail"],
                                                    ConfigurationManager.AppSettings["AssetDispAttachCCEmail"], ConfigurationManager.AppSettings["AssetDispAttachBCCEmail"],
                                                    Convert.ToString(Session["UserGroupName"]), ticketId, Convert.ToString(Session["UserName"]), Convert.ToString(Session["SFAName"]), path, fileName);
                                            }
                                        }
                                    }
                                }
                                msg = "Attachment has been added successfully";
                            }

                            isSuccess = true;
                        }
                        else
                        {
                            msg = "Invalid file size. Maximum allowed size is 10MB.";
                        }
                    }
                    else
                    {
                        msg = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";
                    }
                }
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = "Error Occured";
                isSuccess = false;
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This action is responsible for getting ticket emails
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public ActionResult GetTicketEmail(int ticketId)
        {
            try
            {
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    return PartialView("~/Views/TicketSearch/ISD/_TicketEmail.cshtml");
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    return PartialView("~/Views/TicketSearch/SFA/_TicketEmail.cshtml");
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for sending ticket email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="emailMessage"></param>
        /// <returns></returns>
        public JsonResult SendTicketEmail(string email, string emailMessage)
        {
            int count = tsp.SendTicketEmail(Convert.ToInt32(Session["ticketId"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]), Session["UserName"].ToString(), email, emailMessage);
            var message = count > 0 ? "Your request has been sent successfully" : "No email has been sent";
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ResetTicketDetail()
        {
            try
            {
                Session["FinalSerialNumber"] = null;
                Session["FinalAssetId"] = null;
                Session["FinalAssetTag"] = null;
                Session["FinalAssetDescription"] = null;
                Session["FinalAssetLocation"] = null;
                return Json("Done", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 5th September 2018
        /// This function is used for exporting ticket details to pdf.
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public ActionResult ExportTicketDetailToPdf(string ticketId)
        {
            try
            {
                var detailModel = new TicketDetailModel();
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    detailModel = tsp.GetTicketDetail(Convert.ToInt32(ticketId), "ISD", 0, Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]));
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    detailModel = tsp.GetTicketDetail(Convert.ToInt32(ticketId), "SFA", Convert.ToInt32(Session["SFAID"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]), Convert.ToInt32(Session["UserID"]));
                }

                iTextSharp.text.Image logoImg =
                      iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                logoImg.ScaleAbsolute(250, 45);

                PdfPTable pdfTable = FillRequestDetailPdfDetails(detailModel);

                var attachmentsName = tsp.GetTicketAttachment(Convert.ToInt32(ticketId)).Select(x => x.daname).ToList();
                PdfPTable attachmentsTbl = AddExportAttachments(attachmentsName);

                var history = tsp.GetTicketHistory(Convert.ToInt32(ticketId));

                PdfPTable historyTbl = new PdfPTable(5);
                var columnNames = new string[] { "Log Date", "Updated By", "Event", "Status", "Comment" };
                float[] columnsWitdh = new float[] { 20f, 20f, 20f, 10f, 30f };
                MakePdfFileDataGridHeader(historyTbl, columnNames, columnsWitdh, 100);

                HistoryPdfFileDataGridBody(historyTbl, history.LstTicketHistory);

                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                {
                    Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();
                    document.Add(logoImg);

                    document.Add(new Chunk("\n"));
                    document.Add(pdfTable);

                    if (attachmentsName.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Attachments"));
                        document.Add(new Paragraph("\n"));
                        document.Add(attachmentsTbl);
                        document.Add(new Chunk("\n"));
                    }

                    if (history.LstTicketHistory.Count > 0) 
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("History"));
                        document.Add(new Paragraph("\n"));
                        document.Add(historyTbl);
                    }

                 
                    document.Close();
                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();

                    Response.Clear();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=TicketDetails - " + ticketId + ".pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public void HistoryPdfFileDataGridBody(PdfPTable pdfTable, List<TicketHistory> data)
        {
            try
            {

                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Logdate.ToString() != null ? item.Logdate.ToString() : "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.UserName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.ActionName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.TktStatusName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Comments ?? "-"), backgrounColor));


                }

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }

        public static void MakePdfFileDataGridHeader(PdfPTable pdfTable, string[] columnsNames, float[] columnsWitdh, int widthPercentage)
        {
            pdfTable.SetWidths(columnsWitdh);
            pdfTable.WidthPercentage = widthPercentage;
            foreach (var item in columnsNames)
            {
                PdfPCell cell = new PdfPCell(new Phrase(item, new Font(null, 8f, Font.BOLD))) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                BaseColor myColor = WebColors.GetRGBColor("#3B9AD2");
                cell.BackgroundColor = myColor;
                pdfTable.AddCell(cell);
            }

        }
        public PdfPTable AddExportAttachments(List<string> data)
        {
            try
            {

                PdfPTable pdfTable = new PdfPTable(1);
                var columnNames = new string[] { "File name" };

                float[] columnsWitdh = new float[] { 30f };

                MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return pdfTable;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item ?? "-"), backgrounColor));
                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public static PdfPCell AddDatatoPdfCell(Chunk pdfChunk, BaseColor backgrounColor)
        {
            BaseColor dataGridBodyFontCOlor = WebColors.GetRGBColor("#333333");
            Font dataGridBodyFOnt = new Font(null, 8, Font.NORMAL, color: dataGridBodyFontCOlor);
            BaseColor dataBodyborderColor = WebColors.GetRGBColor("#ddd");

            var cell =
                new PdfPCell(new Paragraph(new Chunk(pdfChunk.ToString()) { Font = dataGridBodyFOnt }) { Font = dataGridBodyFOnt })
                {
                    BorderColor = dataBodyborderColor,
                    BorderWidthLeft = 0,
                    BorderWidthRight = 0,
                    BackgroundColor = backgrounColor,
                    BorderWidthTop = 0,
                    PaddingTop = 4,
                    PaddingBottom = 4

                };

            return cell;
        }

        public PdfPTable FillRequestDetailPdfDetails(TicketDetailModel data)
        {
            try
            {

                PdfPTable pdfTable = new PdfPTable(6);

                float[] columnsWitdh = new float[] { 12f, 22f, 12f, 22f, 10f, 22f };
                pdfTable.SetWidths(columnsWitdh);
                pdfTable.WidthPercentage = 100;

                iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
                PdfPCell cell = new PdfPCell();
                BaseColor backgrounColor = WebColors.GetRGBColor("#eeeeee");
                cell.BackgroundColor = backgrounColor;

                //row 0
                //cell 1
                cell = new PdfPCell(new Phrase("Ticket#:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(Convert.ToString(data.TicketId), fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase("", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase("", fontNormal))
                {
                    Border = 0,
                    BorderWidthRight = 1,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);



                //row 1
                //cell 1
                cell = new PdfPCell(new Phrase("Submitted By:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    BorderWidthTop = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.TicketDetail.username ?? "-", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Affected User:", fontLabel))
                {
                    Border = 0,
                    BorderWidthTop = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase((data.TicketDetail.firstname ?? "") + " " + (data.TicketDetail.lastname ?? "") ?? "-", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Customer Ticket:", fontLabel))
                {
                    Border = 0,
                    BorderWidthTop = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(data.TicketDetail.customerticketid ?? "-", fontNormal))
                {
                    Border = 0,
                    BorderWidthRight = 1,
                    BorderWidthTop = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 2
                //cell 1
                cell = new PdfPCell(new Phrase("Location:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell.BackgroundColor = backgrounColor;

                Paragraph p = new Paragraph();
                p.Font = fontNormal;
                // p.Alignment = Element.ALIGN_CENTER;

                var add2 = (!string.IsNullOrWhiteSpace(data.TicketDetail.address2) ? data.TicketDetail.address2 + "\n" : "");
                var region = (!string.IsNullOrWhiteSpace(data.TicketDetail.stateprovinceregion) ? data.TicketDetail.stateprovinceregion + "\n" : "");
                var postalcode = (!string.IsNullOrWhiteSpace(data.TicketDetail.postalcode) ? data.TicketDetail.postalcode + "\n" : "");
                var city = (!string.IsNullOrWhiteSpace(data.TicketDetail.city) ? data.TicketDetail.city + "\n" : "");
                var country = (!string.IsNullOrWhiteSpace(data.TicketDetail.custcountry) ? data.TicketDetail.custcountry + "\n" : "");
                p.Add(new Phrase(data.TicketDetail.condensedaddress + "\n" + data.TicketDetail.address1 + "\n" + add2 + city + region + postalcode + country));


                cell.AddElement(p);
                cell.VerticalAlignment = 1;
                cell.PaddingBottom = 5;
                //cell.PaddingTop = 5;
                //cell.HorizontalAlignment = 1;
                cell.Border = 0;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Ticket Contact Name:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell =
                    new PdfPCell(new Phrase(Convert.ToString(data.TicketDetail.ticketcontactname ?? "-"), fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Ticket Contact Phone:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.TicketDetail.ticketcontactphonevoice ?? "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 3
                //cell 1
                cell = new PdfPCell(new Phrase("Request Date:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                string timezone = "";
                timezone = tsp.GetTimeZoneByTimeZoneId(data.TimeZoneId);
                cell = new PdfPCell(new Phrase((data.TicketDetail.requestdate.ToString() ?? "") + (timezone ?? "-"), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Status:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4


                var statusName = data.TicketDetail.tktstatusname ?? "-";
                cell = new PdfPCell(new Phrase(statusName, fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Close Date", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.TicketDetail.closedate.ToString() ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 4
                //cell 1
                cell = new PdfPCell(new Phrase("Request Type:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase((data.TicketDetail.artypename ?? "-") + (timezone ?? "-"), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Asset Tag:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.TicketDetail.assettagnum ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Priority:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.TicketDetail.priorityname ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 5
                //cell 1
                cell = new PdfPCell(new Phrase("Asset Model:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.tmpmodel ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Service Levels:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                var slaName = "-";
                if (data.ServiceLevel.FirstOrDefault() != null)
                {
                    slaName = data.ServiceLevel.FirstOrDefault().SLAName;
                }
                cell = new PdfPCell(new Phrase(slaName ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Worked On Serial:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.TicketDetail.workedonserial ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 6
                //cell 1
                cell = new PdfPCell(new Phrase("Serial Number:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase((data.TicketDetail.replaceserial ?? "-"), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Customer PO#:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.TicketDetail.customerpo ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Asset Manufacturer:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6

                var assetMfg = data.AssetManufacturer.FirstOrDefault(x => x.Key == data.tmpmfgid);
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(assetMfg.Value ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 7
                //cell 1
                cell = new PdfPCell(new Phrase("Travel Time:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase((("HH:") + "" + (data.TicketDetail.hourstravel.ToString() != "" ? data.TicketDetail.hourstravel.ToString() : "00") + "" + ("  MM:") + "" + (data.TicketDetail.minstravel.ToString() != "" ? data.TicketDetail.minstravel.ToString() : "00")), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Supplier Ticket#:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.TicketDetail.sfaticketid ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("ARepl. Serial Number:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.TicketDetail.replaceserial ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 8
                //cell 1
                cell = new PdfPCell(new Phrase("Elapsed Time Onsite:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase((("HH:") + "" + (data.TicketDetail.hoursonsite.ToString() != "" ? data.TicketDetail.hoursonsite.ToString() : "00") + "" + ("  MM:") + "" + (data.TicketDetail.minsonsite.ToString() != "" ? data.TicketDetail.minsonsite.ToString() : "00")), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Appointment Date:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.TicketDetail.appointmentdate.ToString() ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Summary:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.TicketDetail.arname ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 9
                //cell 1
                cell = new PdfPCell(new Phrase("Description:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 0,
                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.TicketDetail.ardesc ?? "-"), fontNormal))
                    {
                        Border = 0,

                        BorderWidthBottom = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Ticket SLA:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 0,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase((""), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 0,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 0,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(""), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 9
                //cell 1
                cell = new PdfPCell(new Phrase("", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 1,
                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase((""), fontNormal))
                    {
                        Border = 0,

                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 1,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase("", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 1,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(""), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return null;
            }
        }
    }
}
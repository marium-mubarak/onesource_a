﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using OneSourceAdminCore.Model;
using System.Configuration;
using System.Net.Mail;
using System.Diagnostics;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace OneSourceAdmin.Controllers
{
    /// <summary>   
    /// Date created : Feb 04 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is Base Controller Inherited with Controller Class(built-in)"
    /// </summary>
    public class BaseController : Controller
    {
        protected AdminSetting adminSetting { get; set; }

        public BaseController()
        {
            adminSetting = new AdminSetting();
        }

        protected ActionResult RedirectToLogin()
        {
            return RedirectToActionPermanent("Login", "Account");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (controllerName.ToLower() != "account")
            {
                if (Session["UserID"] == null)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.HttpContext.Response.StatusCode = 403;
                        filterContext.Result = new JsonResult { Data = "LogOut" };
                    }
                    else
                    {

                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                            {
                            {"controller", "Account"},
                            {"action", "Login"}
                            });
                    }
                }
            }



            var chkAuthFilterValue = filterContext.RouteData.DataTokens["AuthFilter"];

            if (chkAuthFilterValue != null && chkAuthFilterValue != "")
            {
                if (Session["UserAuthTokens"] != null)
                {

                    List<UserAccessCheckModel> userAuthTokens = (List<UserAccessCheckModel>)Session["UserAuthTokens"];
                    var selectToken = userAuthTokens.Select(x => x.DataToken);

                    if (chkAuthFilterValue.ToString().Contains("^"))
                    {

                        var dataTokenAddCheck = chkAuthFilterValue.ToString().Split('^')[0];
                        ViewBag.currentPageClassName = dataTokenAddCheck;
                        if (userAuthTokens.Any(x => x.DataToken == dataTokenAddCheck.ToString() && !x.AddEditAccess))
                        {
                            if (!selectToken.Contains(chkAuthFilterValue.ToString()))
                            {

                                filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary
                            {
                            {"controller", "Account"},
                            {"action", "AccessDenied"}
                            });
                            }
                        }
                    }
                    else
                    {
                        ViewBag.currentPageClassName = chkAuthFilterValue;
                        if (!selectToken.Contains(chkAuthFilterValue.ToString()))
                        {

                            filterContext.Result = new RedirectToRouteResult(
                                new RouteValueDictionary
                            {
                            {"controller", "Account"},
                            {"action", "AccessDenied"}
                            });
                        }
                    }

                }

            }


            adminSetting = new AdminSetting
            {
                UserID = Convert.ToInt32(Session["UserID"]),
                UserName = Convert.ToString(Session["UserName"]),
                UserGroupID = Convert.ToInt32(Session["UserGroupID"]),
                UserGroupName = Convert.ToString(Session["UserGroupName"]),
                UserSecurityLevel = Convert.ToInt32(Session["UserSecurityLevel"]),
                LanguageID = Convert.ToInt32(Session["LanguageID"]),
                DateFormatID = Convert.ToInt32(Session["DateFormatID"]),
                TimeZoneID = Convert.ToInt32(Session["TimeZoneID"]),
                CustomUserGroupID = Convert.ToInt32(Session["CustomUserGroupID"]),
                ParentCompanyID = Convert.ToInt32(Session["ParentCompanyID"]),
                PrimeContractorID = Convert.ToInt32(Session["PrimeContractorID"]),
                PrimeContractorName = Convert.ToString(Session["PrimeContractorName"]),
                ContactId = Convert.ToInt32(Session["ContactID"])
            };

            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            if (actionName == "QuoteDetail")
            {
                int paramsCount = Request.Url.Segments.Count();
                if (paramsCount < 4 || Session["QuoteType"] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary
                                {
                            {"controller", "AdminDashboard"},
                            {"action", "Index"}
                                });
                }
            }

            base.OnActionExecuting(filterContext);
        }

        protected void LogException(string errorMessage, RouteData route)
        {
            string path = Server.MapPath("~/Exception/AllExceptions.txt");

            string controller = route.Values["controller"].ToString();
            string action = route.Values["action"].ToString();

            using (StreamWriter w = System.IO.File.AppendText(path))
            {
                w.Write("\r\nLog Entry : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                w.WriteLine("  :");
                w.WriteLine(string.Format("Controller Name: {0} , Action Name: {1}", controller, action));
                w.WriteLine("  :{0}", errorMessage);
                w.WriteLine("-------------------------------");
            }

        }
        [HttpGet]
        public ActionResult GiveMeExceptionFile()
        {
            string filepath = Server.MapPath("~/Exception/AllExceptions.txt");
            string content = string.Empty;

            try
            {
                using (var stream = new StreamReader(filepath))
                {
                    content = stream.ReadToEnd().Replace("Log Entry : ", "<br><br>Log Entry : ");
                }
            }
            catch (Exception)
            {
                return Content("File not found!");
            }

            return Content(content);
        }
        [HttpGet]
        public ActionResult EmptyExceptionFile()
        {
            string filepath = Server.MapPath("~/Exception/AllExceptions.txt");
            string content = string.Empty;

            try
            {
                using (StreamWriter writer = new StreamWriter(Server.MapPath("~/Exception/AllExceptions.txt")))
                {
                    writer.WriteLine("");
                }
            }
            catch (Exception)
            {
                return Content("File not found!");
            }

            return Content(content);
        }

        public void SendEmail(string to, string messagebody, string subject)
        {

            string fromm = ConfigurationManager.AppSettings["mailFrom"];
            string bcc = ConfigurationManager.AppSettings["mailBCC"];
            string bcc2 = ConfigurationManager.AppSettings["mailBCC2"];
            string mailServer = ConfigurationManager.AppSettings["mailServer"];

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromm, "GlobalServe Inc.");
            //SmtpClient client = new SmtpClient(mailServer);
            SmtpClient client = new SmtpClient
            {
                Host = mailServer,
                Port = 25,
                // EnableSsl = true,
                // DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                // Credentials = new NetworkCredential(userName, password)
            };
            mailMessage.To.Add(new MailAddress(to));
            mailMessage.CC.Add(new MailAddress("it@global-serve.com"));

            mailMessage.Subject = subject.Trim();
            mailMessage.Body = messagebody.Trim();
            mailMessage.IsBodyHtml = true;

            try
            {
                client.Send(mailMessage);
            }

            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.ToString(), routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.ToString(), routeData);
                }
            }
        }

        public void SendEmailWithFrom(string to, string messagebody, string subject, string emailFrom)
        {

            string fromm = emailFrom;

            string mailServer = ConfigurationManager.AppSettings["mailServer"];

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromm, "GlobalServe Inc.");
            //SmtpClient client = new SmtpClient(mailServer);
            SmtpClient client = new SmtpClient
            {
                Host = mailServer,
                Port = 25,
                // EnableSsl = true,
                // DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                // Credentials = new NetworkCredential(userName, password)
            };
            mailMessage.To.Add(new MailAddress(to));

            mailMessage.Subject = subject.Trim();
            mailMessage.Body = messagebody.Trim();
            mailMessage.IsBodyHtml = true;

            try
            {
                client.Send(mailMessage);
            }

            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.ToString(), routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.ToString(), routeData);
                }
            }
        }

        public bool isAddEditAllow(string dataToken)
        {
            var chkAddAccess = (List<UserAccessCheckModel>)Session["UserAuthTokens"];
            if (chkAddAccess.Any(x => x.DataToken == dataToken && x.AddEditAccess))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Added by adnan
        /// Generic function to retreive the data from Store Procedure
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="spName"></param>
        /// <returns>datatable</returns>
        public DataTable GetDataFromSP(SqlParameter[] parameter, string spName)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand(spName, con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddRange(parameter);


                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetDataFromSP \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
    }
}
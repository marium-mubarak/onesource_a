﻿using OneSourceAdmin.Models;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class ResponseQuoteController : BaseController
    {
        HQRequestPresenter _hardwareQuotes;
        HardwareResponsePresenter _hardwareresponse;
        OEMPresenter _oem;
        public ResponseQuoteController()
        {
            _hardwareQuotes = new HQRequestPresenter();
            _hardwareresponse = new HardwareResponsePresenter();
            _oem = new OEMPresenter();
        }

        [Route("hardware-response")]
        public ActionResult HardwareResponseList()
        {
            return View("");
        }

        [Route("hardware-quote-response")]
        public ActionResult HardwareResponseById(int id)
        {
            HardwareResponseModel Hrm = new HardwareResponseModel();

            Hrm.HardwareQuotes = _hardwareQuotes.Get().FirstOrDefault(x => x.HardwareQuoteId == id);
            var response = _hardwareresponse.GetHardwareResponseQuote().FirstOrDefault(x => x.HardwareQuoteId == id);
            int quoteid = 0;
            if (response != null)
            {
                 quoteid = response.HardwareResponseID;
            }
            
            Hrm.LineItemes = _hardwareQuotes.GetQuoteLineItemByQuoteId(id, 1);
            if (quoteid > 0)
            {
                Hrm.ResponseLineItemes = _hardwareresponse.GetRsponseLineItem().Where(x => x.QuoteTypeId == 1 && x.ResponseQuoteId == quoteid).ToList();
            }           
            Hrm.Manufacturers = _oem.GetManufacturers();
            Hrm.Category = _hardwareresponse.GetCategory().Where(x => x.IsActive == true).OrderBy(x => x.PartCategoryName).ToList();
            Hrm.CurrencyList = _hardwareQuotes.Currencies().OrderBy(x => x.CurrencyName).ToList();
            Hrm.HardwareQuoteResponse = _hardwareresponse.GetHardwareResponseQuote().FirstOrDefault(x => x.HardwareQuoteId == id);
            return View("HardwareQuoteResponse", Hrm);
        }

        [HttpPost]
        public ActionResult SaveHardwareResponse(FormCollection col)
        {
            var deliverycharges = col["txtDeliveryCharges"];
            var leadtime = col["txtLeadTime"];
            var pricevaliditydate = col["txtPriceValidityDate"];
            var paymentterms = col["txtPaymentTerms"];
            var gspquotereference = col["txtGSPRef"];
            var gspdetails = col["txtGSPDetails"];
            var details = col["txtDetails"];
            var additionalinfo = col["txtAdditionalInfo"];
            var specialinstrucation = col["txtSpecailIns"];
            var quouteid = col["hdHardwareQuoteId"];
            var currencyid = col["drCurrency"];
            var responseQuoteId = col["hdResponseQuoteId"];

            var _hresponse = _hardwareresponse.HardwareResponseQuote;

            if (responseQuoteId != "0")
            {
                _hresponse.HardwareResponseID = Convert.ToInt32(responseQuoteId);
            }

            _hresponse.AdditionalInformation = additionalinfo;
            _hresponse.CreatedBy = adminSetting.UserID;
            _hresponse.CreatedDate = DateTime.Now;
            _hresponse.DeliveryCharges = Convert.ToDouble(deliverycharges);
            _hresponse.Details = details;
            _hresponse.GSPDetails = gspdetails;
            _hresponse.GSPQuoteReferenceNo = gspquotereference;
            _hresponse.HardwareQuoteId = Convert.ToInt32(quouteid);
            _hresponse.LeadTime = leadtime;
            _hresponse.CurrencyId = Convert.ToInt32(currencyid);
            _hresponse.PaymentTerms = paymentterms;
            _hresponse.PriceValidityDate = pricevaliditydate;
            _hresponse.SpecialInstructions = specialinstrucation;

            _hardwareresponse.SaveHardwareQuote();

            var quote = _hardwareresponse.GetHardwareResponseQuote().OrderByDescending(x => x.HardwareResponseID).FirstOrDefault();

            return Json(quote, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveResponeLineItem(FormCollection col)
        {
            string[] _sku = Request.Form["txtSKU"].Split(new char[] { ',' });
            string[] _category = Request.Form["drCategory"].Split(new char[] { ',' });
            string[] _manufacturer = Request.Form["drManufacturer"].Split(new char[] { ',' });
            string[] _quantity = Request.Form["txtQuauntity"].Split(new char[] { ',' });
            string[] _unitprice = Request.Form["txtUnitPrice"].Split(new char[] { ',' });
            string[] _finalprice = Request.Form["txtFinalPrice"].Split(new char[] { ',' });
            string[] _tax = Request.Form["txtTax"].Split(new char[] { ',' });
            string[] _pricewithtax = Request.Form["txtPriceWithTax"].Split(new char[] { ',' });

            var quoteid = Request.Form["hdQuoteId"];
            var responseId = Request.Form["hdResponseId"];
            var responsetype = Request.Form["hdResponseTypeId"];
            

            for (int i = 0; i < _sku.Length; i++)
            {
                var lineItem = _hardwareresponse.LineItems;
                lineItem.CategoryId = Convert.ToInt32(_category[i].ToString());
                lineItem.CreatedBy = adminSetting.UserID;
                lineItem.CreatedDate = DateTime.Now;
                lineItem.FinalPriceWithoutTax = Convert.ToDouble(_finalprice[i].ToString());
                lineItem.FinalPriceWithTax = Convert.ToDouble(_pricewithtax[i].ToString());
                lineItem.ManufacturerId = Convert.ToInt32(_manufacturer[i].ToString());
                lineItem.Quantity = Convert.ToInt32(_quantity[i].ToString());
                lineItem.QuoteTypeId = Convert.ToInt32(responsetype);
                lineItem.ResponseQuoteId = Convert.ToInt32(responseId);
                lineItem.SKU = _sku[i].ToString();
                lineItem.Tax = Convert.ToDouble(_tax[i].ToString());
                lineItem.UnitCostPrice = Convert.ToDouble(_unitprice[i].ToString());
                
                _hardwareresponse.SaveLineItem();
                
            }

            var responslineitem = _hardwareresponse.GetRsponseLineItem().Where(x => x.ResponseQuoteId == Convert.ToInt32(responseId) && x.QuoteTypeId == Convert.ToInt32(responsetype)).ToList();
            string tbl = string.Empty;

            tbl = "<table class='table table-striped'>";
            tbl += "<thead><tr>";
            tbl += "<th>Manufacturer</th>";
            tbl += "<th>SKU</th>";
            tbl += "<th>Category</th>";
            tbl += "<th>Quantity</th>";
            tbl += "<th>Unit Price</th>";
            tbl += "<th>Final Price</th>";
            tbl += "<th>TAX/VAT</th>";
            tbl += "<th>Total TAX/VAT</th>";
            tbl += "</tr></thead><tbody>";

            foreach (var item in responslineitem)
            {
                tbl += "<tr>";
                tbl += "<td>" + _oem.GetManufacturers().FirstOrDefault(x => x.Key == item.ManufacturerId).Value + "</td>";
                tbl += "<td>" +item.SKU + "</td>";
                tbl += "<td>" + _hardwareresponse.GetCategory().FirstOrDefault(x => x.PartCategoryID == item.CategoryId).PartCategoryName + "</td>";
                tbl += "<td>" + item.Quantity + "</td>";
                tbl += "<td>" + item.UnitCostPrice + "</td>";
                tbl += "<td>" + item.FinalPriceWithoutTax + "</td>";
                tbl += "<td>" + item.Tax + "</td>";
                tbl += "<td>" + item.FinalPriceWithTax + "</td>";
                tbl += "</tr>";
            }

            tbl += "</tbody></table>";

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveResponseLineItems(HardwareResponseModel lineitems)
        {
            var formValues = lineitems.EntryLineModel;
            var responseId = 0;
            var responsetype = 0;
            foreach (var item in formValues)
            {
                var lineItem = _hardwareresponse.LineItems;
                responseId = item.ResponseQuoteId;
                responsetype = item.ResonseQuoteType;
                lineItem.CategoryId = item.CategoryId;
                lineItem.CreatedBy = adminSetting.UserID;
                lineItem.CreatedDate = DateTime.Now;
                lineItem.FinalPriceWithoutTax = item.FinalPrice;
                lineItem.FinalPriceWithTax = item.PriceWithTax;
                lineItem.ManufacturerId = item.ManfuacturerId;
                lineItem.Quantity = item.Quantity;
                lineItem.QuoteTypeId = item.ResonseQuoteType;
                lineItem.ResponseQuoteId = item.ResponseQuoteId;
                lineItem.SKU = item.SKU;
                lineItem.Tax = item.Tax;
                lineItem.UnitCostPrice = item.UnitCost;
                lineItem.GSPMargin = item.GSPMargin;



                _hardwareresponse.SaveLineItem();
            }

            var responslineitem = _hardwareresponse.GetRsponseLineItem().Where(x => x.ResponseQuoteId == Convert.ToInt32(responseId) && x.QuoteTypeId == Convert.ToInt32(responsetype)).ToList();
            string tbl = string.Empty;

            tbl = "<table class='table table-striped'>";
            tbl += "<thead><tr>";
            tbl += "<th>Manufacturer</th>";
            tbl += "<th>SKU</th>";
            tbl += "<th>Category</th>";
            tbl += "<th>Quantity</th>";
            tbl += "<th>Unit Price</th>";
            tbl += "<th>Final Price</th>";
            tbl += "<th>TAX/VAT</th>";
            tbl += "<th>Total TAX/VAT</th>";
            tbl += "<th></th>";
            tbl += "</tr></thead><tbody>";

            foreach (var item in responslineitem)
            {
                tbl += "<tr>";
                tbl += "<td>" + _oem.GetManufacturers().FirstOrDefault(x => x.Key == item.ManufacturerId).Value + "</td>";
                tbl += "<td>" + item.SKU + "</td>";
                tbl += "<td>" + _hardwareresponse.GetCategory().FirstOrDefault(x => x.PartCategoryID == item.CategoryId).PartCategoryName + "</td>";
                tbl += "<td>" + item.Quantity + "</td>";
                tbl += "<td>" + item.UnitCostPrice + "</td>";
                tbl += "<td>" + item.FinalPriceWithoutTax + "</td>";
                tbl += "<td>" + item.Tax + "</td>";
                tbl += "<td>" + item.FinalPriceWithTax + "</td>";
                tbl += "<td><a onclick='DeleteLineItem(" + item.ResponseLineItemId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                tbl += "</tr>";
            }

            tbl += "</tbody></table>";

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [Route("delete-response-lineitem")]
        public ActionResult DeleteLineItem(int id)
        {
            var litem = _hardwareresponse.GetRsponseLineItem().FirstOrDefault(x => x.ResponseLineItemId == id);
            var responseId = litem.ResponseQuoteId;
            var quotetypeid = litem.QuoteTypeId;
            _hardwareresponse.DeleteResponseLineItem(id);
            var responslineitem = _hardwareresponse.GetRsponseLineItem().Where(x => x.ResponseQuoteId == responseId && x.QuoteTypeId == quotetypeid).ToList();
            string tbl = string.Empty;

            tbl = "<table class='table table-striped'>";
            tbl += "<thead><tr>";
            tbl += "<th>Manufacturer</th>";
            tbl += "<th>SKU</th>";
            tbl += "<th>Category</th>";
            tbl += "<th>Quantity</th>";
            tbl += "<th>Unit Price</th>";
            tbl += "<th>Final Price</th>";
            tbl += "<th>TAX/VAT</th>";
            tbl += "<th>Total TAX/VAT</th>";
            tbl += "<th></th>";
            tbl += "</tr></thead><tbody>";

            foreach (var item in responslineitem)
            {
                tbl += "<tr>";
                tbl += "<td>" + _oem.GetManufacturers().FirstOrDefault(x => x.Key == item.ManufacturerId).Value + "</td>";
                tbl += "<td>" + item.SKU + "</td>";
                tbl += "<td>" + _hardwareresponse.GetCategory().FirstOrDefault(x => x.PartCategoryID == item.CategoryId).PartCategoryName + "</td>";
                tbl += "<td>" + item.Quantity + "</td>";
                tbl += "<td>" + item.UnitCostPrice + "</td>";
                tbl += "<td>" + item.FinalPriceWithoutTax + "</td>";
                tbl += "<td>" + item.Tax + "</td>";
                tbl += "<td>" + item.FinalPriceWithTax + "</td>";
                tbl += "<td><a onclick='DeleteLineItem(" + item.ResponseLineItemId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                tbl += "</tr>";
            }

            tbl += "</tbody></table>";

            return Json(tbl, JsonRequestBehavior.AllowGet);

        }
    }
}
﻿using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html;
using OneSourceAdminCore.Entity;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;

namespace OneSourceAdmin.Controllers
{
    public class SupportReportsController : BaseController
    {
        AssetReportsPresenter assetReportService;
        CustomerPresenter customerService;
        AdminMenuService adminMenuService;
        SupportReportsPresenter supportReportService;
        SFAsPresenter sfaPresenter;

        public SupportReportsController()
        {
            assetReportService = new AssetReportsPresenter();
            customerService = new CustomerPresenter();
            adminMenuService = new AdminMenuService();
            supportReportService = new SupportReportsPresenter();
            sfaPresenter = new SFAsPresenter();
        }

        // GET: SupportReports
        public ActionResult Report(int id)
        {
            try
            {
                var route = adminMenuService.GetMenuById(id);
                if (route != null)
                {
                    string name = route.MenuName.Trim().ToLower().Replace(" ", "");
                    var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));

                    Session["routeId"] = route.MenuId;
                    Session["routeName"] = name;
                    Session["reportName"] = route.MenuName;
                    Session["parentRouteId"] = parentRoute.MenuId;
                    Session["parentName"] = parentRoute.MenuName;
                    Session["parentRoute"] = parentRoute.MenuLink;

                    var model = new SupportReportModel();

                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                    model.SFAsList = sfaPresenter.GetIsActiveSFAs();//Adnan 24-june-2019
                    model.ManufacturerList = supportReportService.GetManufactures();
                  
                    Session["TempSupportReportModel"] = model;
                    Session["TempSupportReportTable"] = null;
                    return View("Report", model);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        #region SFA Address by Adnan 24-6-2019

       
        public ActionResult ExportSFAaddressReportToExcel(FormCollection col)
        {
            try
            {
                var parentCompanyID = Convert.ToInt32(col["parentcompanyid"]);
               
                Response.Buffer = true;
                var model = new SupportReportModel();
                DataTable dt = null;
                if (Session["reportName"].ToString() == "SFA Address")
                {
                    dt = GetSFAaddressReport(parentCompanyID);


                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                    model.SFAsList = sfaPresenter.GetIsActiveSFAs();
                  

                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, Session["reportName"].ToString(), Response);
                }
                else if (dt != null && dt.Rows.Count == 0)
                {
                    ViewBag.Message = "NoData";
                }
                else
                {
                    ViewBag.Message = "Error";
                }


                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public DataTable GetSFAaddressReport(int parentCompanyID)//Adnan 24-june-2019
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_GetSFAaddressByParentCompanyID_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ParentCompanyID", SqlDbType.Int));

                    command.Parameters[0].Value = parentCompanyID;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    

                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetSFAaddressReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        #endregion

        #region Customer Buyer Address AssociationDetail by Adnan 10-7-2019


        public ActionResult ExportCustomerBuyerAddressAssociationDetailToExcel(FormCollection col)//Adnan 10-7-2019
        {
            try
            {
                var parentCompanyID = Convert.ToInt32(col["parentcompanyid"]);
                var CustomerID = Convert.ToInt32(col["customerId"]);

                Response.Buffer = true;
                var model = new SupportReportModel();
                DataTable dt = null;
                if (Session["reportName"].ToString() == "Customer Buyer Address Association Detail")
                {
                    var tempCustomerIds = "";
                    var customerIds = customerService.GetCustomerWithParentCompanyId(parentCompanyID, CustomerID);
                    if (customerIds != null && customerIds.Count > 0)
                    {
                        tempCustomerIds = string.Join(",", customerIds.Select(n => n.ToString()).ToArray());
                    }
                    dt = GetCustomerBuyerAssociationAddressReport(tempCustomerIds);
                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                    model.SFAsList = sfaPresenter.GetIsActiveSFAs();

                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, Session["reportName"].ToString(), Response);
                }
                else if (dt != null && dt.Rows.Count == 0)
                {
                    ViewBag.Message = "NoData";
                }
                else
                {
                    ViewBag.Message = "Error";
                }


                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public DataTable GetCustomerBuyerAssociationAddressReport(string CustomerIds)//Adnan 10-july-2019
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_BuyerAddressAssociationRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerIds", SqlDbType.NVarChar));

                    command.Parameters[0].Value = CustomerIds;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetCustomerBuyerAssociationAddressReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        #endregion
        /// <summary>
        /// Get Report Data to Grid
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        #region Distribution Uplifts by OEM by Adnan on 26-07-2019
        public ActionResult ExportDistributionUpliftbyOEMReportToExcel(FormCollection col)
        {
            try
            {
                var ManufacturerId = Convert.ToInt32(col["ManufacturerId"]);

                Response.Buffer = true;
                var model = new SupportReportModel();
                DataTable dt = null;
                if (Session["reportName"].ToString() == "Distribution Uplift by OEM")
                {
                    dt = ExportDistributionUpliftbyOEMReport(ManufacturerId);


                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                    model.SFAsList = sfaPresenter.GetIsActiveSFAs();
                    model.ManufacturerList = supportReportService.GetManufactures();

                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, Session["reportName"].ToString(), Response);
                }
                else if (dt != null && dt.Rows.Count == 0)
                {
                    ViewBag.Message = "NoData";
                }
                else
                {
                    ViewBag.Message = "Error";
                }


                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");

            }
        }
        public DataTable ExportDistributionUpliftbyOEMReport(int manufacturerId)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_DistributionUpliftbyOEMRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ManufacturerId", SqlDbType.Int));

                    command.Parameters[0].Value = manufacturerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>ExportDistributionUpliftbyOEMReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        public ActionResult GetReportData(SupportReportModel model)
        {
            try
            {
                var buttonName = Request.Form["submit"];
                if (buttonName == "Reset")
                {
                    Session["TempSupportReportTable"] = null;
                    Session["TempSupportReportModel"] = null;
                    Session["ErrorMessage"] = null;
                    return RedirectToAction("Report", new { id = Convert.ToInt32(Session["routeId"]) });
                }

                object data = "";

                DateTime FromDate = DateTime.Parse(model.StartDate).Date;
                DateTime ToDate = DateTime.Parse(model.EndDate).Date;

                if (FromDate > ToDate)
                {
                    ViewBag.Message = "Start date should not be greater than end date";
                }
                else
                {
                    var companyId = 0;
                    if (model.CustomerId > 0)
                    {
                        companyId = assetReportService.GetCompanyIdByCustomerId(model.CustomerId);
                    }
                    model.CompanyId = companyId;

                    var dt = supportReportService.GetReportData(Session["routeId"].ToString(), Session["reportName"].ToString(), model);
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        Session["ErrorMessage"] = null;


                        if (Session["routeName"].ToString() == "requestsbysupplier")
                        {
                            DataView dv = dt.DefaultView;
                            dv.Sort = "FSC asc";
                            dt = dv.ToTable();

                            var list = ConvertDataTable<sp_Rpt_ISD_RequestsBySFA_R_Result>(dt);

                            var DistinctFSC = list.Select(x => x.FSC).Distinct();

                            foreach (var record in DistinctFSC)
                            {
                                var TotalPerFSC = list.Where(x => x.FSC == record);

                                var closedWithinSLA = TotalPerFSC.Where(x => x.Resolution == "Closed Within SLA").Count();
                                var exceptions = TotalPerFSC.Where(x => x.Resolution == "Exception").Count();
                                var breaches = TotalPerFSC.Where(x => x.Resolution == "Breach").Count();
                                var avgResolutionTime = TotalPerFSC.Average(x => Convert.ToDouble(x.HoursToResolition));

                                DataRow dr = dt.NewRow();
                                dr["Address"] = " ";

                                DataRow dr1 = dt.NewRow();
                                dr1["Address"] = record;
                                dr1["GSTicketNo"] = "Closed Within SLA:";
                                dr1["Type"] = closedWithinSLA.ToString() + " (" + (closedWithinSLA / Convert.ToDouble(TotalPerFSC.Count()) * 100).ToString("0.00") + "%)";

                                DataRow dr2 = dt.NewRow();
                                dr2["GSTicketNo"] = "Exceptions:";
                                dr2["Type"] = exceptions.ToString() + " (" + (exceptions / Convert.ToDouble(TotalPerFSC.Count()) * 100).ToString("0.00") + "%)";

                                DataRow dr3 = dt.NewRow();
                                dr3["GSTicketNo"] = "Breaches:";
                                dr3["Type"] = breaches.ToString() + " (" + (breaches / Convert.ToDouble(TotalPerFSC.Count()) * 100).ToString("0.00") + "%)";

                                DataRow dr4 = dt.NewRow();
                                dr4["GSTicketNo"] = "Avg. Resolution Time:";
                                dr4["Type"] = avgResolutionTime.ToString("0.00") + " Hours";

                                DataRow dr5 = dt.NewRow();
                                dr5["Address"] = " ";

                                var rows = dt.Select("FSC='" + record + "'");
                                var indexOfRow = dt.Rows.IndexOf(rows[rows.Count() - 1]);
                                dt.Rows.InsertAt(dr, indexOfRow + 1);
                                dt.Rows.InsertAt(dr1, indexOfRow + 2);
                                dt.Rows.InsertAt(dr2, indexOfRow + 3);
                                dt.Rows.InsertAt(dr3, indexOfRow + 4);
                                dt.Rows.InsertAt(dr4, indexOfRow + 5);
                                dt.Rows.InsertAt(dr5, indexOfRow + 6);
                            }

                            var closedWithinSLATotal = list.Where(x => x.Resolution == "Closed Within SLA").Count();
                            var exceptionsTotal = list.Where(x => x.Resolution == "Exception").Count();
                            var breachesTotal = list.Where(x => x.Resolution == "Breach").Count();
                            var avgResolutionTimeTotal = list.Average(x => Convert.ToDouble(x.HoursToResolition));

                            DataRow drTotal = dt.NewRow();
                            drTotal["Address"] = " ";

                            DataRow dr1Total = dt.NewRow();
                            dr1Total["Address"] = "Grand Total";
                            dr1Total["GSTicketNo"] = "Closed Within SLA:";
                            dr1Total["Type"] = closedWithinSLATotal.ToString() + " (" + (closedWithinSLATotal / Convert.ToDouble(list.Count) * 100).ToString("0.00") + "%)";

                            DataRow dr2Total = dt.NewRow();
                            dr2Total["GSTicketNo"] = "Exceptions:";
                            dr2Total["Type"] = exceptionsTotal.ToString() + " (" + (exceptionsTotal / Convert.ToDouble(list.Count) * 100).ToString("0.00") + "%)";

                            DataRow dr3Total = dt.NewRow();
                            dr3Total["GSTicketNo"] = "Breaches:";
                            dr3Total["Type"] = breachesTotal.ToString() + " (" + (breachesTotal / Convert.ToDouble(list.Count) * 100).ToString("0.00") + "%)";

                            DataRow dr4Total = dt.NewRow();
                            dr4Total["GSTicketNo"] = "Avg. Resolution Time:";
                            dr4Total["Type"] = avgResolutionTimeTotal.ToString("0.00") + " Hours";

                            DataRow dr5Total = dt.NewRow();
                            dr5Total["Address"] = " ";


                            dt.Rows.Add(drTotal);
                            dt.Rows.Add(dr1Total);
                            dt.Rows.Add(dr2Total);
                            dt.Rows.Add(dr3Total);
                            dt.Rows.Add(dr4Total);
                            dt.Rows.Add(dr5Total);

                        }

                        if (Session["routeName"].ToString() == "requestsvs.slasummary(byrequest)")
                        {
                            DataView dv = dt.DefaultView;
                            dv.Sort = "SLA asc";
                            dt = dv.ToTable();

                            var list = ConvertDataTable<usp_Rpt_ISD_RequestSLASummary_R_Result>(dt);

                            var DistinctSLA = list.Select(x => x.SLA).Distinct();

                            foreach (var record in DistinctSLA)
                            {
                                var TotalPerSLA = list.Where(x => x.SLA == record);

                                var totalRequests = TotalPerSLA.Sum(x => Convert.ToInt32(x.TotalRequests));
                                var closedWithinSLA = TotalPerSLA.Sum(x => Convert.ToInt32(x.ClosedWithinSLA));
                                var exceptions = TotalPerSLA.Sum(x => Convert.ToInt32(x.Exceptions));
                                var breaches = TotalPerSLA.Sum(x => Convert.ToInt32(x.Breaches));
                                var avgResolutionTime = TotalPerSLA.Average(x => Convert.ToDouble(x.AverageResolutionTime)).ToString("0.00");
                                var commitment = TotalPerSLA.Average(x => Convert.ToDouble(x.Commitment));
                                var actual = TotalPerSLA.Average(x => Convert.ToDouble(x.Actual));
                                var variance = TotalPerSLA.Average(x => Convert.ToDouble(x.Variance));

                                DataRow dr = dt.NewRow();
                                dr["SLA"] = " ";

                                DataRow dr1 = dt.NewRow();
                                dr1["SLA"] = record;
                                dr1["TotalRequests"] = totalRequests;
                                dr1["ClosedWithinSLA"] = closedWithinSLA.ToString() + " (" + ((closedWithinSLA / Convert.ToDouble(totalRequests)) * 100).ToString("0.00") + "%)";
                                dr1["Exceptions"] = exceptions.ToString() + " (" + ((exceptions / Convert.ToDouble(totalRequests)) * 100).ToString("0.00") + "%)";
                                dr1["Breaches"] = breaches.ToString() + " (" + ((breaches / Convert.ToDouble(totalRequests)) * 100).ToString("0.00") + "%)";
                                dr1["AverageResolutionTime"] = avgResolutionTime;
                                dr1["Commitment"] = commitment.ToString("0.00") + "%";
                                dr1["Actual"] = actual.ToString("0.00") + "%";
                                dr1["Variance"] = variance.ToString("0.00") + "%";

                                DataRow dr2 = dt.NewRow();
                                dr2["SLA"] = " ";

                                var rows = dt.Select("SLA='" + record + "'");
                                var indexOfRow = dt.Rows.IndexOf(rows[rows.Count() - 1]);
                                dt.Rows.InsertAt(dr, indexOfRow + 1);
                                dt.Rows.InsertAt(dr1, indexOfRow + 2);
                                dt.Rows.InsertAt(dr2, indexOfRow + 3);

                            }

                            var totalRequestsTotal = list.Sum(x => Convert.ToInt32(x.TotalRequests));
                            var closedWithinSLATotal = list.Sum(x => Convert.ToInt32(x.ClosedWithinSLA));
                            var exceptionsTotal = list.Sum(x => Convert.ToInt32(x.Exceptions));
                            var breachesTotal = list.Sum(x => Convert.ToInt32(x.Breaches));
                            var avgResolutionTimeTotal = list.Average(x => Convert.ToDouble(x.AverageResolutionTime)).ToString("0.00");
                            var commitmentTotal = list.Average(x => Convert.ToDouble(x.Commitment));
                            var actualTotal = list.Average(x => Convert.ToDouble(x.Actual));
                            var varianceTotal = list.Average(x => Convert.ToDouble(x.Variance));

                            DataRow drTotal = dt.NewRow();
                            drTotal["SLA"] = " ";

                            DataRow dr1Total = dt.NewRow();
                            dr1Total["SLA"] = "Grand Total";
                            dr1Total["TotalRequests"] = totalRequestsTotal;
                            dr1Total["ClosedWithinSLA"] = closedWithinSLATotal.ToString() + " (" + ((closedWithinSLATotal / Convert.ToDouble(totalRequestsTotal)) * 100).ToString("0.00") + "%)";
                            dr1Total["Exceptions"] = exceptionsTotal.ToString() + " (" + ((exceptionsTotal / Convert.ToDouble(totalRequestsTotal)) * 100).ToString("0.00") + "%)";
                            dr1Total["Breaches"] = breachesTotal.ToString() + " (" + ((breachesTotal / Convert.ToDouble(totalRequestsTotal)) * 100).ToString("0.00") + "%)";
                            dr1Total["AverageResolutionTime"] = avgResolutionTimeTotal;
                            dr1Total["Commitment"] = commitmentTotal.ToString("0.00") + "%";
                            dr1Total["Actual"] = actualTotal.ToString("0.00") + "%";
                            dr1Total["Variance"] = varianceTotal.ToString("0.00") + "%";

                            DataRow dr2Total = dt.NewRow();
                            dr2Total["SLA"] = " ";

                            dt.Rows.Add(drTotal);
                            dt.Rows.Add(dr1Total);
                            dt.Rows.Add(dr2Total);


                        }

                        if (Session["routeName"].ToString() == "requestsbycsm")
                        {
                            DataView dv = dt.DefaultView;
                            dv.Sort = "Company asc";
                            dt = dv.ToTable();

                            var list = ConvertDataTable<sp_Rpt_ISD_RequestsByCSR_R_Result>(dt);

                            var DistinctCompanies = list.Where(x => x.CSR != null && x.CSR != "").Select(x => x.Company).Distinct();

                            foreach (var record in DistinctCompanies)
                            {
                                var TotalPerCompany = list.Where(x => x.Company == record);

                                var closedWithinSLA = TotalPerCompany.Where(x => x.Resolution == "Closed Within SLA").Count();
                                var exceptions = TotalPerCompany.Where(x => x.Resolution == "Exception").Count();
                                var breaches = TotalPerCompany.Where(x => x.Resolution == "Breach").Count();
                                var avgResolutionTime = TotalPerCompany.Average(x => Convert.ToDouble(x.HoursToResolition));

                                DataRow dr = dt.NewRow();
                                dr["CSR"] = " ";

                                DataRow dr1 = dt.NewRow();
                                dr1["Address"] = record;
                                dr1["GSTicketNo"] = "Closed Within SLA:";
                                dr1["Type"] = closedWithinSLA.ToString() + " (" + (closedWithinSLA / Convert.ToDouble(TotalPerCompany.Count()) * 100).ToString("0.00") + "%)";

                                DataRow dr2 = dt.NewRow();
                                dr2["GSTicketNo"] = "Exceptions:";
                                dr2["Type"] = exceptions.ToString() + " (" + (exceptions / Convert.ToDouble(TotalPerCompany.Count()) * 100).ToString("0.00") + "%)";

                                DataRow dr3 = dt.NewRow();
                                dr3["GSTicketNo"] = "Breaches:";
                                dr3["Type"] = breaches.ToString() + " (" + (breaches / Convert.ToDouble(TotalPerCompany.Count()) * 100).ToString("0.00") + "%)";

                                DataRow dr4 = dt.NewRow();
                                dr4["GSTicketNo"] = "Avg. Resolution Time:";
                                dr4["Type"] = avgResolutionTime.ToString("0.00") + " Hours";

                                DataRow dr5 = dt.NewRow();
                                dr5["CSR"] = " ";

                                var rows = dt.Select("Company='" + record + "'");
                                var indexOfRow = dt.Rows.IndexOf(rows[rows.Count() - 1]);
                                dt.Rows.InsertAt(dr, indexOfRow + 1);
                                dt.Rows.InsertAt(dr1, indexOfRow + 2);
                                dt.Rows.InsertAt(dr2, indexOfRow + 3);
                                dt.Rows.InsertAt(dr3, indexOfRow + 4);
                                dt.Rows.InsertAt(dr4, indexOfRow + 5);
                                dt.Rows.InsertAt(dr5, indexOfRow + 6);
                            }

                            var closedWithinSLATotal = list.Where(x => x.Resolution == "Closed Within SLA").Count();
                            var exceptionsTotal = list.Where(x => x.Resolution == "Exception").Count();
                            var breachesTotal = list.Where(x => x.Resolution == "Breach").Count();
                            var avgResolutionTimeTotal = list.Average(x => Convert.ToDouble(x.HoursToResolition));

                            DataRow drTotal = dt.NewRow();
                            drTotal["CSR"] = " ";

                            DataRow dr1Total = dt.NewRow();
                            dr1Total["Address"] = "Grand Total";
                            dr1Total["GSTicketNo"] = "Closed Within SLA:";
                            dr1Total["Type"] = closedWithinSLATotal.ToString() + " (" + (closedWithinSLATotal / Convert.ToDouble(list.Count) * 100).ToString("0.00") + "%)";

                            DataRow dr2Total = dt.NewRow();
                            dr2Total["GSTicketNo"] = "Exceptions:";
                            dr2Total["Type"] = exceptionsTotal.ToString() + " (" + (exceptionsTotal / Convert.ToDouble(list.Count) * 100).ToString("0.00") + "%)";

                            DataRow dr3Total = dt.NewRow();
                            dr3Total["GSTicketNo"] = "Breaches:";
                            dr3Total["Type"] = breachesTotal.ToString() + " (" + (breachesTotal / Convert.ToDouble(list.Count) * 100).ToString("0.00") + "%)";

                            DataRow dr4Total = dt.NewRow();
                            dr4Total["GSTicketNo"] = "Avg. Resolution Time:";
                            dr4Total["Type"] = avgResolutionTimeTotal.ToString("0.00") + " Hours";

                            DataRow dr5Total = dt.NewRow();
                            dr5Total["CSR"] = " ";


                            dt.Rows.Add(drTotal);
                            dt.Rows.Add(dr1Total);
                            dt.Rows.Add(dr2Total);
                            dt.Rows.Add(dr3Total);
                            dt.Rows.Add(dr4Total);
                            dt.Rows.Add(dr5Total);
                        }

                        if (Session["routeName"].ToString() == "replacementserialrequests" || Session["routeName"].ToString() == "requestsrawdata" || Session["routeName"].ToString() == "requestsbycsm")
                        {
                            if (dt.Rows.Count > 1000)
                            {
                                ViewBag.Message = "DownloadExcel";
                                Session["TempSupportReportTable"] = dt;
                                return Json(new { data = "", msg = ViewBag.Message });
                            }
                        }

                        Session["TempSupportReportTable"] = dt;
                    }
                    else
                    {
                        ViewBag.Message = "NoRecordsFound";
                        Session["TempSupportReportTable"] = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                    Session["TempSupportReportModel"] = model;

                    //JavaScriptSerializer jsJson = new JavaScriptSerializer();
                    //jsJson.MaxJsonLength = 2147483644;

                    //data = jsJson.Serialize(dt);
                    data = JsonConvert.SerializeObject(dt);
                }

                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        private List<T> ConvertDataTable<T>(DataTable dt)
        {

            try
            {
                List<T> data = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    if (Session["routeName"].ToString() == "requestsbysupplier" || Session["routeName"].ToString() == "requestsbycsm")
                    {
                        if (row[0].ToString() != "")
                        {
                            T item = GetItem<T>(row);
                            data.Add(item);
                        }
                    }
                    else
                    {
                        T item = GetItem<T>(row);
                        data.Add(item);
                    }
                }
                return data;

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        private T GetItem<T>(DataRow dr)
        {
            try
            {
                Type temp = typeof(T);
                T obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in dr.Table.Columns)
                {
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {

                        if (pro.Name == column.ColumnName)
                        {
                            if (dr[column.ColumnName].ToString() != "")
                            {
                                pro.SetValue(obj, dr[column.ColumnName], null);
                            }
                            else
                            {
                                pro.SetValue(obj, null, null);
                            }
                        }
                        else
                            continue;
                    }
                }
                return obj;
            }


            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                T obj = Activator.CreateInstance<T>();
                return obj;
            }
        }

        public ActionResult ExportSupportReportToExcel()
        {
            try
            {
                var model = new SupportReportModel();
                string filename = Session["reportName"].ToString().Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");
                var temp = (SupportReportModel)Session["TempSupportReportModel"];
                if (temp != null)
                {
                    model = temp;
                }
                if (Session["TempSupportReportTable"] != null)
                {
                    var dt = (DataTable)Session["TempSupportReportTable"];
                    ReportHelper.ExportToExcel(dt, filename, Response);
                }
                else
                {
                    ViewBag.Message = "NoData";
                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                }

                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ExportSupportReportToPdf()
        {
            try
            {
                var model = new SupportReportModel();
                var temp = (SupportReportModel)Session["TempSupportReportModel"];
                if (temp != null)
                {
                    model = temp;
                }
                if (Session["TempSupportReportTable"] != null)
                {
                    var dt = (DataTable)Session["TempSupportReportTable"];
                    string filename = Session["reportName"].ToString().Trim();
                    filename = filename.Replace(" ", "");
                    filename = filename.Replace(",", "");

                    PdfPTable pdfTable = FillDataTableToPdf(dt);
                    ReportHelper.ExportToPdf(dt, pdfTable, filename, Response, Server);
                }
                else
                {
                    ViewBag.Message = "NoData";
                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });


                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");

                }
                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult RedirectToParent()
        {

            Response.Redirect("~/" + Session["parentRoute"].ToString() + "/" + Session["parentRouteId"].ToString());
            return null;
        }

        public JsonResult GetReportChartData(SupportReportModel model, string type = null)
        {
            try
            {
                GetReportData(model);
                var dt = (DataTable)Session["TempSupportReportTable"];
                if (dt != null)
                {
                    if (Session["routeName"].ToString() == "requestsbycountry")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_RequestSLASummaryByCountry_R_Result>(dt);
                        var distinctCountries = list.Select(x => x.Country).Distinct().ToList();

                        foreach (var country in distinctCountries)
                        {
                            var day = 0;

                            int[] days = new int[distinctCountries.Count()];

                            var requestsForCountry = list.Where(x => x.Country == country).Select(x => x.TotalRequests).ToList();

                            foreach (var item in requestsForCountry)
                            {
                                day += Convert.ToInt32(item);
                            }

                            for (int i = 0; i < distinctCountries.Count(); i++)
                            {
                                if (distinctCountries[i].ToString() == country)
                                {
                                    days[i] = day;
                                }
                                else
                                {
                                    days[i] = 0;
                                }

                            }
                            var data = new
                            {
                                name = country,
                                data = days.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            seriesList = distinctCountries,
                            dataList = FinalList.ToArray(),
                            leftLabel = "Total Requests"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (Session["routeName"].ToString() == "requestsbysupplier")
                    {
                        var list = ConvertDataTable<sp_Rpt_ISD_RequestsBySFA_R_Result>(dt);

                        if (type == "country")
                        {
                            List<object> FinalList = new List<object>();
                            var distinctYears = list.Select(x => DateTime.Parse(x.CloseDate.ToString()).Year).Distinct().ToList();
                            var distinctCountries = list.Where(x => x.Country != null).Select(x => x.Country).Distinct().ToList();

                            foreach (var country in distinctCountries)
                            {
                                List<double> stack = new List<double>();
                                for (int i = 0; i < distinctYears.Count(); i++)
                                {
                                    //var manufacturerForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total).FirstOrDefault();
                                    //stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths) : 0.0);

                                    var temp = list.Where(x => DateTime.Parse(x.CloseDate.ToString()).Year.ToString() == distinctYears[i].ToString() && x.Country == country && x.Resolution == "Closed Within SLA").Count();

                                    stack.Add(temp != null ? Convert.ToDouble(temp.ToString("0.00")) : 0.0);

                                }

                                var data = new
                                {
                                    name = country,
                                    data = stack.ToArray()
                                };

                                FinalList.Add(data);
                            }

                            var obj = new
                            {
                                dataList = FinalList.ToArray(),
                                seriesList = distinctYears.ToArray(),
                                leftLabel = "Total Closed Resolutions"
                            };

                            return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                        }

                        if (type == "supplier")
                        {
                            List<object> FinalList = new List<object>();
                            var distinctSuppliers = list.Select(x => x.FSC).Distinct().ToList();
                            var distinctResolutionTypes = list.Where(x => x.Resolution != null).Select(x => x.Resolution).Distinct().ToList();

                            foreach (var resolutionType in distinctResolutionTypes)
                            {
                                List<double> stack = new List<double>();
                                for (int i = 0; i < distinctSuppliers.Count(); i++)
                                {
                                    //var manufacturerForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total).FirstOrDefault();
                                    //stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths) : 0.0);

                                    var temp = list.Where(x => x.FSC == distinctSuppliers[i].ToString() && x.Resolution == resolutionType).Count();
                                    stack.Add(temp != null ? Convert.ToDouble(temp.ToString("0.00")) : 0.0);

                                }

                                var data = new
                                {
                                    name = resolutionType,
                                    data = stack.ToArray()
                                };

                                FinalList.Add(data);
                            }

                            var obj = new
                            {
                                dataList = FinalList.ToArray(),
                                seriesList = distinctSuppliers.ToArray(),
                                leftLabel = "Total Closed Resolutions"
                            };

                            return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (Session["routeName"].ToString() == "requestsbycsm")
                    {
                        var list = ConvertDataTable<sp_Rpt_ISD_RequestsByCSR_R_Result>(dt);

                        List<object> FinalList = new List<object>();
                        var distinctCSR = list.Where(x => x.CSR != null && x.CSR.Replace(" ", "") != "").Select(x => x.CSR).Distinct().ToList();
                        var distinctResolutionTypes = list.Where(x => x.Resolution != null && x.Resolution.Replace(" ", "") != "").Select(x => x.Resolution).Distinct().ToList();

                        foreach (var resolutionType in distinctResolutionTypes)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctCSR.Count(); i++)
                            {
                                //var manufacturerForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total).FirstOrDefault();
                                //stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths) : 0.0);

                                var temp = list.Where(x => x.CSR == distinctCSR[i].ToString() && x.Resolution == resolutionType).Count();
                                stack.Add(temp != null ? Convert.ToDouble(temp.ToString("0.00")) : 0.0);

                            }

                            var data = new
                            {
                                name = resolutionType,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctCSR.ToArray(),
                            leftLabel = "Total Closed Resolutions"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);

                    }
                }
                return Json(new { data = "null", msg = "No records found" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public PdfPTable FillDataTableToPdf(DataTable data)
        {
            try
            {
                var dt = data;
                var columnsCount = dt.Columns.Count;
                BaseColor color = WebColors.GetRGBColor("#ffffff");
                iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD, color);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
                BaseColor backgrounColor = WebColors.GetRGBColor("#3b9ad2");

                PdfPTable table = new PdfPTable(data.Columns.Count);
                PdfPRow row = null;
                float[] widths = new float[data.Columns.Count];

                for (int i = 0; i < data.Columns.Count; i++)
                {
                    widths[i] = 15f;
                }

                table.SetWidths(widths);

                table.WidthPercentage = 100;
                int iCol = 0;
                string colname = "";

                int[] rowItems = new int[columnsCount];

                for (int i = 0; i < columnsCount; i++)
                {
                    rowItems[i] = i;
                }

                foreach (DataColumn c in dt.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(c.ColumnName, fontLabel));
                    cell.BackgroundColor = backgrounColor;
                    table.AddCell(cell);
                }

                foreach (DataRow r in dt.Rows)
                {
                    for (int i = 0; i < rowItems.Count(); i++)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            table.AddCell(new Phrase(r[i].ToString(), fontNormal));
                        }
                    }
                }

                return table;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public JsonResult GetRequestTypesByCustomerId(string customerId)
        {
            try
            {
                var requestTypesList = supportReportService.GetRequestTypesByCustomerId(customerId);
                var requestTypes = requestTypesList.Select(x => new
                {
                    Value = x.artypeId.ToString(),
                    Text = x.requestType
                }).ToList();

                return Json(requestTypes.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class ShippingCostController : BaseController
    {
        /// <summary>
        /// new module Shipping Cost added by adnan 
        /// </summary>
      
        AdminMenuService _adminMenuService;
        ShippingCostPresenter _shippingCostService;

        #region Constructor
        public ShippingCostController()
        {
           
            _adminMenuService = new AdminMenuService();
            _shippingCostService = new ShippingCostPresenter();
        }
        #endregion

        // GET: Index
        /// <summary>
        /// append view base on filters
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="supplierId"></param>
        /// <param name="categoryId"></param>
        /// <param name="shippingcostId"></param>
        /// <returns></returns>
        public ActionResult Index(int customerId = 0, int supplierId=0, int categoryId = 0, int shippingcostId= 0)
        {
            try
            {
                var model = _shippingCostService.GetShippingCostModel(customerId, supplierId, categoryId, shippingcostId);
                TempData["Message"] = "";
                return View(model);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
         
        }

        /// <summary>
        /// save the shipping cost model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult AddEditShippingCost(ShippingCostModel model)
        {
            try
            {
                var result = _shippingCostService.AddandUpdateShippingCost(model, adminSetting);
               
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json( new ResultModel { ID = 0, Message = "Error Occured", Result = false } , JsonRequestBehavior.AllowGet);
            }
         
        }

        /// <summary>
        /// delete the shipping cost by id
        /// </summary>
        /// <param name="shippingCostId"></param>
        /// <returns></returns>
        public JsonResult DeleteShippingCost(int shippingCostId)
        {
            try
            {
                if (shippingCostId > 0)
                {
                    var result = _shippingCostService.DeleteShippingCost(shippingCostId, adminSetting);
                 
                    return Json( result , JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new ResultModel { ID = 0, Message ="Error Occured" , Result=false} , JsonRequestBehavior.AllowGet);
                }
              
            }
            catch (Exception ex)
            {


                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = new ResultModel { ID = 0, Message = "Error Occured", Result = false } }, JsonRequestBehavior.AllowGet);
            }
           
        }
    }
}
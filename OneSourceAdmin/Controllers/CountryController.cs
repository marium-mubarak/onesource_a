﻿using System.Web.Mvc;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;

namespace OneSourceAdmin.Controllers
{
    /// <summary>   
    /// Date created : Feb 04 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for Adding, Editing, Listing of Country Alert"
    /// </summary>

    public class CountryController : BaseController
    {
        ///<summary>
        /// This Action is responsible For providing View Of Country Alert (Alert>>Country)
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>




        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CountryPresenter cp = new CountryPresenter();
                    CustomerPresenter custp = new CustomerPresenter();
                    CountryModel model = new CountryModel
                    {
                        Countries = cp.GetCountries(),
                        CountryAlerts = cp.GetCountryAlert(),
                        Customers = custp.GetCustomers()

                    };

                    @ViewBag.AddEditAccess = isAddEditAllow("CountryAlert");

                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Adding and Editing of Country Alert
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        [HttpPost]
        public ActionResult AddCountryAlert(tblCountryAlert model)
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    CountryPresenter cp = new CountryPresenter();
                    int count;
                    //For Edit
                    if (model.CountryAlertId != 0)
                    {
                        count = cp.EditCountryAlert(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Alert has been edited";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddCountryAlert(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Alert has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Deleting selected country 
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult DeleteAlert(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int countryId;
                    if (!int.TryParse(id, out countryId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    CountryPresenter cp = new CountryPresenter();
                    int count = cp.DeleteCountryAlert(countryId);
                    if (count > 0) TempData["Message"] = "Alert has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This Action is responsible For Validating Data which has been post to AddCountryAlert Action
        /// </summary>
        /// <returns> 
        /// Return boolean result
        /// </returns>
        public bool ValidateFields(tblCountryAlert model)
        {
            if (model.CountryId == 0)
            {
                TempData["Message"] = "Please Provide Country";
                return false;
            }
            if (Convert.ToDateTime(model.StartDate).Date.Year == 1)
            {
                TempData["Message"] = "Please Provide Start Date";
                return false;
            }
            if (Convert.ToDateTime(model.EndDate).Date.Year > 1)
            {
                if (model.StartDate.Date > Convert.ToDateTime(model.EndDate).Date)
                {
                    TempData["Message"] = "Start Date must be less than End Date";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(model.Alert))
            {
                TempData["Message"] = "Please Provide Alert";
                return false;
            }

            return true;
        }


        #region  Country CRUD Operations
        //12-28-2016 by Danyal Manzoor
        /// <summary>
        /// Show the list of countries
        /// </summary>
        /// <returns>View</returns>
        //[Route("locale-settings/country/{countryName?}")]
        public ActionResult CountryListing(string countryName = "")
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var modal = new CountryCrudModal();
                    var chkAddAccess = (List<UserAccessCheckModel>)Session["UserAuthTokens"];
                    if (chkAddAccess.Any(x => x.DataToken == "localeCountries" && x.AddEditAccess))
                    {
                        @ViewBag.AddEditAccess = true;
                    }
                    modal.CountryListing = countryService.GetCountriesListing(countryName);
                    modal.DateFormatsList = countryService.GetDateFormatesList();
                    modal.TimeZonesList = countryService.GetTimeZonesList();
                    modal.IsoCountriesList = countryService.GetIsoCountryListToAdd();

                    return View(modal);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public JsonResult UpdateCountryStatus(int countryId, bool isActive)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var userId = Convert.ToInt32(Session["UserID"]);
                    var data = countryService.UpdateCountryStatus(countryId, isActive, userId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCountryById(int countryId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();

                    var country = countryService.GetCountryById(countryId);
                    var countryData = new
                    {
                        CountryName = country.CountryName,
                        DefaultDateFormatID = country.DefaultDateFormatID,
                        DefaultTimeZoneID = country.DefaultTimeZoneID,
                        IsActive = country.IsActive,
                        LastUpdated = country.LastUpdated.ToString(),
                        LastUpdatedByUserID = country.LastUpdatedByUserID,
                        CountryCodeID = country.CountryCodeID,
                    };

                    var data = new
                    {
                        country = countryData,
                        userName = countryService.GetUserNameByUserId(countryData.LastUpdatedByUserID),
                        isoCountryName = countryService.GetIsoCountryNameByCountryCode(countryData.CountryCodeID)
                    };

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddEditCountry(tblCountry country)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var userId = int.Parse(Session["UserID"].ToString());
                    if (country.CountryID != null && country.CountryID != 0)
                    {
                        TempData["crudEditMsg"] = countryService.UpdateCountry(country, userId);
                    }
                    else
                    {
                        TempData["crudMsg"] = countryService.AddNewCountry(country, userId);
                    }
                    return RedirectToAction("CountryListing");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public JsonResult CheckCountryNameExist(string countryName, int countryId)
        {
            var _dbCon = new globalserveEntities();
            var country = _dbCon.tblCountries.Any(x => x.CountryName.ToLower().Trim() == countryName.ToLower().Trim() && x.CountryID != countryId);
            return Json(country, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWorkshiftsByCountryId(int countryId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var data = countryService.GetWorkShiftsByCountry(countryId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult AddNewWorkShiftByCountry(CountryWorkShiftsModel workShift)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var userId = int.Parse(Session["UserID"].ToString());
                    var data = countryService.AddCountryWorkShifts(workShift, userId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateShiftByCountryId(CountryWorkShiftsModel workShift)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var userId = int.Parse(Session["UserID"].ToString());
                    var data = countryService.UpdateCountryWorkshift(workShift, userId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeletCountryWorkShiftByShiftId(int shiftId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var data = countryService.DeletWorkShiftByShiftId(shiftId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }

        //[Route("locale-settings/iso-country/{countryName?}")]
        public ActionResult IsoCountryListing(string isoCountryName = "")
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var modal = new IsoCountryCrudModal();
                    modal.IsoCountryListing = countryService.GetIsoCountriesListing(isoCountryName);
                    modal.CurrencyCodesList = countryService.GetCurrencyCodesList();

                    var chkAddAccess = (List<UserAccessCheckModel>)Session["UserAuthTokens"];
                    if (chkAddAccess.Any(x => x.DataToken == "LocaleIsoCountries" && x.AddEditAccess))
                    {
                        @ViewBag.AddEditAccess = true;
                    }

                    return View(modal);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public JsonResult CheckIsoAlreadyExist(string checkFor, string value, int isoCountryId)
        {
            var dbCon = new globalserveEntities();
            bool flag = false;
            value = value.Trim();

            if (isoCountryId != null && isoCountryId != 0)
            {
                if (checkFor == "ISOCountryName")
                {
                    flag =
                        dbCon.tblCountryCodes.Any(
                            x => x.ISOCountryName.ToLower().Trim() == value.ToLower() && x.CountryCodeID != isoCountryId);
                }
                else if (checkFor == "ISOCountryCodeA2")
                {
                    flag =
                        dbCon.tblCountryCodes.Any(
                            x =>
                                x.ISOCountryCodeA2.ToLower().Trim() == value.ToLower() &&
                                x.CountryCodeID != isoCountryId);

                }
                else if (checkFor == "ISOCountryCodeA3")
                {
                    flag =
                        dbCon.tblCountryCodes.Any(
                            x =>
                                x.ISOCountryCodeA3.ToLower().Trim() == value.ToLower() &&
                                x.CountryCodeID != isoCountryId);
                }
                else if (checkFor == "ISOCountryCodeNumber")
                {
                    flag =
                        dbCon.tblCountryCodes.Any(
                            x =>
                                x.ISOCountryCodeNumber.ToLower().Trim() == value.ToLower() &&
                                x.CountryCodeID != isoCountryId);
                }
            }
            else
            {
                if (checkFor == "ISOCountryName")
                {
                    flag = dbCon.tblCountryCodes.Any(x => x.ISOCountryName.ToLower().Trim() == value.ToLower());
                }
                else if (checkFor == "ISOCountryCodeA2")
                {
                    flag = dbCon.tblCountryCodes.Any(x => x.ISOCountryCodeA2.ToLower().Trim() == value.ToLower());

                }
                else if (checkFor == "ISOCountryCodeA3")
                {
                    flag = dbCon.tblCountryCodes.Any(x => x.ISOCountryCodeA3.ToLower().Trim() == value.ToLower());
                }
                else if (checkFor == "ISOCountryCodeNumber")
                {
                    flag = dbCon.tblCountryCodes.Any(x => x.ISOCountryCodeNumber.ToLower().Trim() == value.ToLower());
                }
            }

            return Json(flag, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddEditISoCountry(tblCountryCode isoCountry)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var userId = int.Parse(Session["UserID"].ToString());
                    if (isoCountry.CountryCodeID != null && isoCountry.CountryCodeID != 0)
                    {
                        TempData["crudEditMsg"] = countryService.UpdateIsoCountry(isoCountry, userId);
                    }
                    else
                    {
                        TempData["crudMsg"] = countryService.AddNewIsoCountry(isoCountry, userId);
                    }
                    return RedirectToAction("IsoCountryListing");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public JsonResult UpdateIsoCountryStatus(int isoCountryId, bool isActive)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();
                    var userId = Convert.ToInt32(Session["UserID"]);
                    var data = countryService.UpdateIsoCountry(isoCountryId, isActive, userId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetIsoCountryById(int isoCountryId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var countryService = new CountryPresenter();

                    var isoCountry = countryService.GetIsoCountryById(isoCountryId);
                    var isoCountryData = new
                    {
                        CountryName = isoCountry.ISOCountryName,
                        CountryCodeA3 = isoCountry.ISOCountryCodeA3.Trim(),
                        CountryCodeA2 = isoCountry.ISOCountryCodeA2.Trim(),
                        CountryCode = isoCountry.ISOCountryCodeNumber,
                        CurrencyId = isoCountry.CurrencyCodeID,
                        IsActive = isoCountry.IsActive,
                        LastUpdated = isoCountry.LastUpdated.ToString(),
                        LastUpdatedByUserID = isoCountry.LastUpdatedByUserID,
                        CountryCodeID = isoCountry.CountryCodeID,
                    };

                    var data = new
                    {
                        country = isoCountryData,
                        userName = countryService.GetUserNameByUserId(isoCountryData.LastUpdatedByUserID),
                    };

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("sessionOut", JsonRequestBehavior.AllowGet);
            }
        }



        #endregion
    }


}

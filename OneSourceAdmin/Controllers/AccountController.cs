﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>   
    /// Date created : Feb 04 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for Login and Logout of Authentic user"
    /// </summary>
    public class AccountController : BaseController
    {

        /// <summary>
        /// This Action is responsible for returning Index View of Login Page
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.error = "";
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        /// <summary>
        /// This Action is responsible for Login authentic user
        /// </summary>
        /// <param name="col"></param>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of wrong username or password
        /// Return to Country view of Alert
        /// </returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateInput(false)]
        public async Task<ActionResult> Login(FormCollection col)
        {
            try
            {
                string userName = col["username"];
                string password = col["password"];

                if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
                {
                    ViewBag.error = "Enter any username and password.";
                    return View();
                }

                var up = new UserPresenter();
                sp_getUserInfo_AdminR_Result usr = up.LoginAdmin(userName, password);
                if (usr == null)
                {
                    ViewBag.error = "Invalid username or password.";
                    return View();
                }
                else
                {

                    tblContact contact = up.GetContactInfo(usr.UserID);

                    Session["FirstName"] = contact.FirstName;
                    Session["LastName"] = contact.LastName;
                    Session["ContactID"] = contact.ContactID;
                    Session["ContactEmail"] = contact.Email;
                    Session["UserID"] = usr.UserID;
                    Session["UserName"] = usr.UserName;
                    Session["UserGroupID"] = usr.UserGroupID;
                    Session["UserGroupName"] = usr.UserGroupName;
                    Session["UserSecurityLevel"] = usr.UserSecurityLevel;

                    Session["LanguageID"] = usr.LanguageID;
                    Session["DateFormatID"] = usr.DateFormatID;
                    Session["TimeZoneID"] = usr.TimeZoneID;
                    Session["CustomUserGroupID"] = usr.CustomUserGroupID;
                    Session["UserEmail"] = contact.Email;
                    if (usr.UserGroupName.ToUpper() == "ISD")
                    {
                        sp_getPrimeContractorInfo_AdminR_Result PCInfo = up.GetPrimeContractorInfo(usr.UserID);

                        if (usr.LanguageID == null || usr.TimeZoneID == null || usr.DateFormatID == null)
                        {
                            var pCServiceLocationInfo = up.GetPrimeContractorserviceLocationInfo(PCInfo.PrimeContractorLocationID);
                            Session["LanguageID"] = pCServiceLocationInfo.LanguageID;
                            Session["DateFormatID"] = pCServiceLocationInfo.DateFormatID;
                            Session["TimeZoneID"] = pCServiceLocationInfo.TimeZoneID;
                        }

                        Session["PrimeContractorID"] = PCInfo.PrimeContractorID;
                        Session["PrimeContractorName"] = PCInfo.PrimeContractorName;
                        Session["ParentCompanyID"] = PCInfo.ParentCompanyID;

                    }
                    else if (usr.UserGroupName.ToUpper() == "SFA")
                    {
                        sp_getSFAInfo_AdminR_Result SFAInfo = up.GetSFAInfo(usr.UserID);
                        if (SFAInfo != null)
                        {
                            Session["CountryID"] = SFAInfo.CountryID;
                            Session["UserEmail"] = SFAInfo.Email;
                            Session["ParentCompanyID"] = SFAInfo.ParentCompanyID;
                            Session["DocMgtVisible"] = SFAInfo.DocMgtVisible;
                            Session["SFAID"] = SFAInfo.SFAID;
                            Session["SFAName"] = SFAInfo.SFAName;
                            Session["SFACountryID"] = SFAInfo.CountryID;

                            var sfa = up.GetSFA(SFAInfo.SFAID);
                            if (sfa != null)
                            {

                                Session["LanguageID"] = usr.LanguageID == null ? sfa.LanguageID : usr.LanguageID;
                                Session["DateFormatID"] = usr.DateFormatID == null ? sfa.DateFormatID : usr.DateFormatID;
                                Session["TimeZoneID"] = usr.TimeZoneID == null ? sfa.TimeZoneID : usr.TimeZoneID;
                            }
                        }
                    }
                    string homePage = up.GetUserHomePage(usr.UserID);
                    return RedirectToAction(homePage.Split('-')[1], homePage.Split('-')[0]);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                ViewBag.error = "Issue while connecting please try later.";
                return View();
            }

        }

        /// <summary>
        /// This Action is responsible for clearing all the current user sessions and redirect to Login Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {

            Session.Abandon();

            return RedirectToAction("Login", "Account");
        }


        public ActionResult PageNotFound()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.Message = "Page Not Found";
            ViewBag.ErrorType = "Error. 404";
            return View("~/Views/Shared/PageNotFound.cshtml");
        }

        [Route("access-denied")]
        public ActionResult AccessDenied()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.Message = "You are not authorised for the requested page. Please contact administrator.";
            ViewBag.ErrorType = "Access Denied";
            return View("~/Views/account/AccessDeniedView.cshtml");
        }

        public ActionResult InternalServerError()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.ErrorType = "Error. 505";
            ViewBag.Message = "Internal Server Error";
            return View("~/Views/Shared/PageNotFound.cshtml");
        }

        public ActionResult Error()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.Message = "An error occurred while processing your request, please try again later";
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class MasterController : BaseController
    {

        private AdminMenuService menuService;
        public MasterController()
        {
            menuService = new AdminMenuService();
        }

        public ActionResult RenderCurrentUserMenu()
        {
            var model = new AdminMenuAndAccessModel();
            if (adminSetting != null && adminSetting.UserID > 0 && adminSetting.UserGroupID > 0)
            {

                model = menuService.GetMenuByUserGroupIdAndUserId(adminSetting.UserGroupID, adminSetting.UserID);
                Session["UserAuthTokens"] = model.UserAccessCheck;

            }

            return PartialView("~/Views/Master/_userMenuPartial.cshtml", model.AdminMenu);
        }
    }
}
﻿using OneSourceAdminCore;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 28 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Category Mapping detail between one source and one street"
    /// </summary>
    public class CategoryMappingController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of Category Mapping
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var mp = new MappingPresenter();
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("OsCategoryMapping");
                    return View(mp.GetOneStreetCategoryMappingDetails(0, true));
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);

                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }

                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for updating category mapping record
        /// </summary>
        /// <param name="onesourceCat"></param>
        /// <param name="onestreetCat"></param>
        /// <returns></returns>
        public ActionResult UpdateCategoryMapping(string onesourceCat, string[] onestreetCat)
        {
            try
            {
                var message = "Please provide correct data";

                if (Session["UserID"] != null)
                {
                    if (!string.IsNullOrEmpty(onesourceCat))
                    {
                        var mp = new MappingPresenter();
                        int a = mp.UpdateCategoryMapping(onesourceCat, onestreetCat, Convert.ToInt32(Session["UserID"]));
                        message = a > 0 ? "Categories successfully updated !" : "No record was updated";
                    }
                }

                TempData["Message"] = message;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        ///  This Action is responsible for deleting mapping 
        /// </summary>
        /// <param name="osrCatId"></param>
        /// <param name="ostCatId"></param>
        /// <returns></returns>
        public ActionResult DeleteMapping(int osrCatId, int ostCatId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (osrCatId == 0 || ostCatId == 0)
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    MappingPresenter mp = new MappingPresenter();
                    int count = mp.DeleteCategoryMapping(osrCatId, ostCatId);
                    if (count > 0) TempData["Message"] = "Mapping has been deleted successfully.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This action is responsible for returning Json Resul of One Street categories and unmapped categories
        /// </summary>
        /// <returns></returns>
        public void ExportCategoryMappingReportToExcel()
        {
            try
            {
                var mp = new MappingPresenter();

                var temp = mp.GetMapUnmapCategories().MappingDetail.Select(x => new
                {
                    OneSourceCategoryName = x.PartCategoryName,
                    OneStreetCategoryName = x.TreeClassClassificationName
                }).ToList();

                if (temp != null)
                {
                    var gridview = new GridView();
                    gridview.DataSource = temp;
                    gridview.DataBind();

                    // Clear all the content from the current response
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.BufferOutput = true;
                    // set the header
                    Response.AddHeader("content-disposition", "attachment;filename = One_Category_Mapped_Manufacturers.xls");

                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";

                    // create HtmlTextWriter object with StringWriter
                    using (var sw = new StringWriter())
                    {
                        using (var htw = new HtmlTextWriter(sw))
                        {
                            // render the GridView to the HtmlTextWriter
                            gridview.RenderControl(htw);
                            // Output the GridView content saved into StringWriter
                            Response.Output.Write(sw.ToString());
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "NoData";
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                //return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for returning Json Result of One Street unmapped categories
        /// </summary>
        /// <returns></returns>
        public void ExportToExcel()
        {
            try
            {
                var mp = new MappingPresenter();

                var unmappedOneStreetCategories = mp.GetMapUnmapCategories().UnmappedCategories
                    .Select(x => new
                    {
                        OneStreetCategoryName = x.TreeClassClassificationName
                    })
                    .ToList();

                if (unmappedOneStreetCategories != null)
                {
                    var gridview = new GridView();
                    gridview.DataSource = unmappedOneStreetCategories;
                    gridview.DataBind();

                    var fileName = string.Format("UnmappedOneStreetCategories_{0}_{1}.xls",
                        DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());

                    Response.DownloadExcel(gridview, fileName, "application/ms-excel");
                }
                else
                {
                    ViewBag.Message = "No data found";
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                //return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for the retrieval of unmapped onestreet categories against one source category
        /// </summary>
        /// <param name="oneSourceCategoryId"></param>
        /// <returns></returns>
        public JsonResult GetOneStreetCategoryMappingDetails(int oneSourceCategoryId)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    var mp = new MappingPresenter();
                    var data = mp.GetCategoriesMappingDetailByOSCategoryId(oneSourceCategoryId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Nov 25 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for managing upload image view"
    /// </summary>
    public class UploadController : BaseController
    {
        /// <summary>
        /// this action is responsible for providing Upload image View
        /// </summary>
        /// <returns></returns>
     
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("ImageUploadSettings");
                    return View();
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        /// <summary>
        /// this action is responsible for uploading images
        /// </summary>
        /// <param name="attachFiles"></param>
        /// <param name="filesNames"></param>
        /// <returns></returns>
        public ActionResult UploadFiles(HttpPostedFileBase[] attachFiles, string filesNames)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    string[] fileArr = filesNames.Split('*');
                    int fileuploadCount = 0;
                    TempData["Message"] = "File(s) fail to upload:<br> ";
                    int errorCount = 0;
                    foreach (var attachFile in attachFiles)
                    {
                        if (attachFile != null && attachFile.ContentLength > 0)
                        {
                            var routeData = this.ControllerContext.RouteData;                          

                            var fileName = Path.GetFileName(attachFile.FileName);
                            var fileExtension = Path.GetExtension(attachFile.FileName);
                            string path = string.Empty;

                            
                            if (fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".png" || fileExtension == ".JPG" || fileExtension == ".JPEG" || fileExtension == ".PNG")
                            {
                                
                                if (fileArr.Contains(fileName))
                                {
                                    
                                    try
                                    {
                                        path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["ImageUpload"] + "/"), fileName);
                                        attachFile.SaveAs(path);
                                        fileuploadCount++;
                                    }
                                    catch (Exception ex)
                                    {
                                        errorCount++;
                                        TempData["Message"] = TempData["Message"]+ errorCount.ToString() + ") "+fileName +" , ";

                                       
                                        LogException(ex.Message, routeData);
                                        if (ex.InnerException != null)
                                        {
                                            LogException(ex.InnerException.ToString(), routeData);
                                        }
                                    }

                                }
                              
                            }
                            else
                            {
                                TempData["Message"] = "Invalid file. Allowed extensions are: .jpg, .jpeg, .png";

                            }
                        }
                    }
                    if (fileuploadCount == fileArr.Length) TempData["Message"] = "Files upload successfully";
                    var tempValue = TempData["Message"].ToString();
                    if (tempValue.Contains("<br>")) TempData["Message"] = tempValue.Substring(0, tempValue.Length - 2);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}
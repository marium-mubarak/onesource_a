﻿using Newtonsoft.Json;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class SOCShiftController : BaseController
    {
        // GET: SOCShift
        public ActionResult SOCShift(int defaultSOCShiftId = 0)
        {
            CountryPresenter cp = new CountryPresenter();
            SOCShiftPresenter ssp = new SOCShiftPresenter();
            var model = new SOCShiftModel();
            model.ListSOCShifts = ssp.GetSOCShifts();
            model.ListSOCShifts.Add(new GenericList() { Key = -1, Value = "--Select--" });
            model.ListSOCShifts = model.ListSOCShifts.OrderBy(x => x.Key).ToList();


            if (defaultSOCShiftId != 0)
            {
                model.ListSOCShiftWiseCountry = ssp.GetSOCShiftWiseCountry(defaultSOCShiftId);
                model.SOCShiftId = defaultSOCShiftId;
                var firstOrDefault = model.ListSOCShifts.FirstOrDefault(x => x.Key == defaultSOCShiftId);
                if (firstOrDefault != null)
                    model.SOCShiftName = firstOrDefault.Value;
            }

            if (model.ListSOCShiftWiseCountry != null)
            {
                if (model.ListSOCShiftWiseCountry.Count <= 0)
                {
                   // ViewBag.Message = "No country mappings available!";
                }
            }
            else
            {
                ViewBag.Message = "No country mappings available!";
            }

            if (defaultSOCShiftId == -1)
            {
                TempData["Message"] = "No country mappings available, please select a SOC Shift to find shift countries";
            }

            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }

            return View(model);
        }

        public ActionResult AddSOCShift(SOCShiftModel SOCShift)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    SOCShiftPresenter ssp = new SOCShiftPresenter();
                    TempData["Message"] = ssp.AddSOCShift(SOCShift, adminSetting.UserID);

                    return RedirectToAction("SOCShift", new { @defaultSOCShiftId = SOCShift.DefaultSOCShiftId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Adding mapping of country and shift
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult AddSOCShiftCountryMap(SOCShiftModel mapping)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    SOCShiftPresenter ssp = new SOCShiftPresenter();
                    TempData["Message"] = ssp.AddSOCShiftCountryMapping(mapping, adminSetting.UserID);
                    return RedirectToAction("SOCShift", new { @defaultSOCShiftId = mapping.SOCShiftId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }


        /// <summary>
        /// This Action is responsible For Deleting selected Country shift Mapping 
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult DeleteSOCShiftCountryMap(string countrySOCShiftMappingId, string socShiftId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int countrySOCShiftMapping;
                    if (!int.TryParse(countrySOCShiftMappingId, out countrySOCShiftMapping))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    SOCShiftPresenter ssp = new SOCShiftPresenter();
                    int count = ssp.DeleteSOCShiftCountryMapping(countrySOCShiftMapping, adminSetting.UserID);
                    if (count > 0) TempData["Message"] = "Country SOC Shift mapping has been deleted";
                    return RedirectToAction("SOCShift", new { @defaultSOCShiftId = socShiftId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        //public JsonResult GetSOCShiftCountries(int shiftId)
        //{
        //    SOCShiftPresenter ssp = new SOCShiftPresenter();

        //    try
        //    {
        //        var countries = ssp.GetSOCShiftCountries(shiftId);
        //        var timezones = ssp.GetSOCShiftTimeZones(shiftId);
        //        var data = new
        //        {
        //            Countries = countries.ToArray(),
        //            TimeZones = timezones.ToArray()
        //        };
        //        return Json(data, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        var routeData = this.ControllerContext.RouteData;
        //        LogException(ex.Message, routeData);
        //        if (ex.InnerException != null)
        //        {
        //            LogException(ex.InnerException.ToString(), routeData);
        //        }
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public JsonResult GetCountriesByTimeZone(string timeZone, int shiftId)
        {
            SOCShiftPresenter ssp = new SOCShiftPresenter();

            try
            {
                var countries = ssp.GetCountriesByTimeZone(timeZone.Replace('*','+'),shiftId);

                var data = new
                {
                    Countries = countries.ToArray()
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSOCShiftTimeZones(int shiftId)
        {
            SOCShiftPresenter ssp = new SOCShiftPresenter();

            try
            {
                var timezones = ssp.GetSOCShiftTimeZones(shiftId);
                var data = new
                {
                    TimeZones = timezones.ToArray()
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ExportToExcel()
        {
            SOCShiftPresenter ssp = new SOCShiftPresenter();

            try
            {
                var mappings = ssp.GetAllSocShiftMappings();

                var dt = ToDataTable(mappings);
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.Columns["SocShift"].ColumnName = "SOC Shift";
                    dt.Columns["TimeZone"].ColumnName = "Time Zone";

                    ReportHelper.ExportToExcel(dt, "SocShifts", Response);
                    return null;
                }
                else
                {
                    TempData["Message"] = "No records for export";
                    return RedirectToAction("SOCShift");
                }

                
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);

                dataTable.Columns.Add(prop.Name, type);

            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


    }
}
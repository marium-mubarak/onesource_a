﻿using System;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>   
    /// Date created : Feb 08 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for Adding, Editing, Listing of Currency Alert"
    /// </summary>
    public class CurrencyController : BaseController
    {
        ///<summary>
        /// This Action is responsible For providing View Of Currency Alert (Alert>>Country)
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
     
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    CustomerPresenter custp = new CustomerPresenter();
                    CurrencyModel model = new CurrencyModel
                    {
                        Currencies = cp.GetCurrencies(),
                        CurrencyAlerts = cp.GetCurrencyAlert(),
                        Customers = custp.GetCustomers()
                    };
                    @ViewBag.AddEditAccess = isAddEditAllow("CurrencyAlert");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Adding and Editing of Currency Alert
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        [HttpPost]
        public ActionResult AddCurrencyAlert(tblCurrencyAlert model)
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    CurrencyPresenter cp = new CurrencyPresenter();
                    int count;
                    //For Edit
                    if (model.CurrencyAlertId != 0)
                    {
                        count = cp.EditCurrencyAlert(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Alert has been edited";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddCurrencyAlert(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Alert has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Deleting selected currency 
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult DeleteAlert(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int countryId;
                    if (!int.TryParse(id, out countryId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    CurrencyPresenter cp = new CurrencyPresenter();
                    int count = cp.DeleteCurrencyAlert(countryId);
                    if (count > 0) TempData["Message"] = "Alert has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This Action is responsible For Validating Data which has been post to AddCurrencyAlert Action
        /// </summary>
        /// <returns> 
        /// Return boolean result
        /// </returns>
        public bool ValidateFields(tblCurrencyAlert model)
        {
            if (model.CurrencyId == 0)
            {
                TempData["Message"] = "Please Provide Country";
                return false;
            }
            if (Convert.ToDateTime(model.StartDate).Date.Year == 1)
            {
                TempData["Message"] = "Please Provide Start Date";
                return false;
            }
            if (Convert.ToDateTime(model.EndDate).Date.Year > 1)
            {
                if (model.StartDate.Date > Convert.ToDateTime(model.EndDate).Date)
                {
                    TempData["Message"] = "Start Date must be less than End Date";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(model.Alert))
            {
                TempData["Message"] = "Please Provide Alert";
                return false;
            }

            return true;
        }


        #region  CURRENCY CRUD Operations
        //12-28-2016 by Muhammad Wasim
        /// <summary>
        /// Show the list of currencies
        /// </summary>
        /// <returns>View</returns>
       
        public ActionResult CurrencyListing()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("LocaleCurrencies");
                    CurrencyPresenter cp = new CurrencyPresenter();
                    CurrencySetupModel cs = new CurrencySetupModel();
                    cs = cp.GetCurrencySetupModel(0);
                    return View(cs);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        public JsonResult GetISOCurrencies(int currencyId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    var data = new
                    {
                        IsoCurrencies = cp.GetISOCurrencies(currencyId),
                        currenciesInfo = cp.GetCurrencyInfo(currencyId)
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetISOCurrenciesForAdd()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    var data = cp.GetISOCurrencies(0);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetNewISOInfo(int currencyCodeID)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    var data = cp.GetNewISOInfo(currencyCodeID);

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddEditCurrency(tblCurrency currencies)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    int count;
                    //For Edit
                    if (currencies.CurrencyID != 0)
                    {
                        count = cp.EditCurrency(currencies, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Currency has been edited";
                        return RedirectToAction("CurrencyListing");
                    }
                    //

                    count = cp.AddCurrency(currencies, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Currency has been added";
                    return RedirectToAction("CurrencyListing");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public JsonResult SetActiveStatus(int currencyId, bool isActive)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    int count = cp.SetActiveStatus(currencyId, isActive, Convert.ToInt32(Session["UserID"]));
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region  ISO CURRENCY CRUD Operations
  
        public ActionResult ISOCurrencyListing()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("LocaleIsoCurrencies");                    
                    CurrencyPresenter cp = new CurrencyPresenter();
                    ISOCurrencySetupModel cs = new ISOCurrencySetupModel();
                    cs = cp.GetISOCurrencySetupModel(0);
                    return View(cs);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public JsonResult SetISOActiveStatus(int isocurrencyId, bool isActive)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    int count = cp.SetISOActiveStatus(isocurrencyId, isActive, Convert.ToInt32(Session["UserID"]));
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AddEditISOCurrency(tblCurrencyCode currencies)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CurrencyPresenter cp = new CurrencyPresenter();
                    int count;
                    //For Edit
                    if (currencies.CurrencyCodeID != 0)
                    {
                        count = cp.EditISOCurrency(currencies, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "ISO Currency has been edited";
                        else TempData["Message"] = "Your changes could NOT be saved at this time. Check for duplicates.";
                        return RedirectToAction("ISOCurrencyListing");
                    }
                    //

                    count = cp.AddISOCurrency(currencies, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "ISO Currency has been added";
                    else TempData["Message"] = "Your changes could NOT be saved at this time. Check for duplicates.";
                    return RedirectToAction("ISOCurrencyListing");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
    }
}
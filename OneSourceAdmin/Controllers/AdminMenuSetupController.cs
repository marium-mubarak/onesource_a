﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class AdminMenuSetupController : BaseController
    {

        private AdminMenuService menuService;
        public AdminMenuSetupController()
        {
            menuService = new AdminMenuService();
        }


        // GET: /AdminMenuSetup/
        //[Route("admin-menu/manage/{id?}")]
        public ActionResult Index(int? id)
        {

            var model = new AdminMenuModel();

            model.MenuList = menuService.GetMenuList();
            model.AdminMenu = new tblAdminMenu();

            if (id > 0)
            {
                model.AdminMenu = menuService.GetMenuById(Convert.ToInt32(id));
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AddUpdateMenu(tblAdminMenu adminMenu)
        {
            try
            {
                if (adminMenu.MenuId > 0)
                {
                    var edit = menuService.EditMenu(adminMenu, adminSetting);
                    TempData["editMsg"] = edit.ToString();
                }
                else
                {
                    var add = menuService.AddNewMenu(adminMenu, adminSetting);
                    TempData["addMsg"] = add.ToString();
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        //[Route("admin-menu/role-mapping/{id?}")]
        public ActionResult MenuRoleSetup(int? id)
        {
            try
            {
                var model = new AdminMenuModel();
                @ViewBag.currentRoleId = id ?? 0;
                model.UserGroupList = menuService.GetUserGroupList();
                if (id > 0)
                {
                    model.RoleMenuList = menuService.GetRoleMenuMappingByRoleId(Convert.ToInt32(id));
                }

                return View("MenuRoleSetup", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        public JsonResult AddEditRoleMenuSettings(int menuId, int roleId, string accessType, bool accessValue)
        {
            try
            {
                var data = menuService.AddUpdateRoleMenuSettings(menuId, roleId, accessType, accessValue,
                    adminSetting.UserID);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateAllRoleMenu(List<CheckAllMenuRights> items)
        {
            try
            {
                var data = menuService.AddDeleteAllMenurightsByRoleId(items, adminSetting);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        // GET: /AdminMenuSetup/
        //[Route("admin-menu/customer-user-menu/{typeName}/{roleId?}/{typeId?}/{userId?}")]
        public ActionResult CustomerUserMenuRestriction(string typeName, int? roleId, int? typeId, int? userId)
        {
            var model = new AdminMenuModel();

            model.UserGroupType = menuService.GetGroupTypes();
            @ViewBag.typeName = typeName;
            if (!string.IsNullOrWhiteSpace(typeName) && typeName != "null")
            {
                model.EntityTypeList = menuService.GetEntityTypeListByTypeName(typeName);
            }

            if (!string.IsNullOrWhiteSpace(typeName) && typeName != "null")
            {
                model.UserGroupList = menuService.GetUserGroupListByGroupName(typeName);

            }

            if (roleId > 0 && typeId > 0)
            {
                model.EntityUsersList = menuService.GetUsersByRoleId(Convert.ToInt32(roleId));
                @ViewBag.typeId = typeId;
                @ViewBag.roleId = roleId;
            }

            model.RoleMenuList = new List<AdminRoleMenuMappingModel>();
            @ViewBag.userId = userId;
            if (roleId > 0 && userId > 0)
            {
                model.RoleMenuList = menuService.GetMenuForUserrestrictionByRoleId(Convert.ToInt32(roleId), Convert.ToInt32(userId));
                //@ViewBag.CurrentUserId = userId;
                //@ViewBag.CurrentRoleId = roleId;
                //@ViewBag.CurrentGrpType = typeName;
                //@ViewBag.CurrentEntityTypeId = typeId;
            }

            return View("UserAccessSetupView", model);
        }

        public JsonResult RestrictAdminMenuByUser(int menuId, int roleId, int userId, bool accessiable, string userType, int entityTypeId)
        {
            try
            {
                var data = menuService.RestrictAdminMenuByUser(menuId, roleId, userId, accessiable, userType, entityTypeId, adminSetting.UserID);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RestrictAllAdminMenuByUser(List<CheckAllUserRestrictions> items)
        {
            try
            {
                var data = menuService.RestrictAllAdminMenuByUser(items, adminSetting.UserID);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
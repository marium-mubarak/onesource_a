﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{

    public class PrimeContractorController : BaseController
    {

        private globalserveEntities dbCon;
        private PrimeContractorService pcService;

        public PrimeContractorController()
        {
            dbCon = new globalserveEntities();
            pcService = new PrimeContractorService();
        }

        //
        // GET: /PrimeContractor/
        // [Route("prime-contractor/{name?}")]
        public ActionResult Index(string name)
        {
            try
            {
                var model = new PrimeContractorModel();
                ViewBag.searchTxt = name ?? "All";

                model.PrimeContractorList = pcService.GetPrimeContractorList(name);
                model.PrimeContractorListForDrpDown = pcService.GetPrimeContractorList(null);
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpGet]
        //[Route("prime-contractor/profile/{pcId?}")]
        public ActionResult AddUpdatePrimeContractor(int? pcId)
        {
            try
            {
                var countryService = new CountryPresenter();

                var model = new PrimeContractorModel();

                model.CountryList = countryService.GetCountries();
                model.CompanyList = pcService.GetParentCompanyList();
                model.OrderFormatList = pcService.GetOrderFormatList();

                model.PrimeContractor = new tblPrimeContractor();

                ViewBag.pcAddmsg = TempData["pcAddmsg"];
                ViewBag.pcEditmsg = TempData["pcEditmsg"];
                ViewBag.ViewFor = "Add";
                Session["CurrentPrimeContractorName"] = null;
                if (pcId != null && pcId != 0)
                {
                    @ViewBag.PrimeContractorId = pcId;
                    model.PrimeContractor = pcService.GetPrimeContractorById(Convert.ToInt32(pcId));
                    if (model.PrimeContractor != null)
                    {
                        Session["CurrentPrimeContractorName"] = model.PrimeContractor.PrimeContractorName;
                        model.LastUpdatedByUser =
                            countryService.GetUserNameByUserId(model.PrimeContractor.LastUpdatedByUserID);
                        ViewBag.ViewFor = "Edit";
                        model.LocationsList =
                            pcService.GetLocationsByPrimeContractorId(model.PrimeContractor.PrimeContractorID);
                        model.PcExchangeRatesCurrencyList = pcService.GetPcExchangeRatesCurrencyList();
                        model.PcUsersList = pcService.GetPcUsersByPcId(model.PrimeContractor.PrimeContractorID);
                    }
                }

                return View("AddEditPrimeContractorView", model);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult AddUpdatePrimeContractorPost(tblPrimeContractor primeContractor)
        {
            try
            {
                var model = new PrimeContractorModel();

                if (primeContractor.PrimeContractorID != null && primeContractor.PrimeContractorID > 0)
                {
                    var edit = pcService.UpdatePrimeContractor(primeContractor, adminSetting.UserID);
                    TempData["pcEditmsg"] = edit.ToString();
                }
                else
                {
                    var add = pcService.AddNewPrimeContractor(primeContractor, adminSetting.UserID);
                    TempData["pcAddmsg"] = add.ToString();
                }

                return RedirectToAction("AddUpdatePrimeContractor");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpGet]
        [Route("prime-contractor/location/{id?}/{pcId?}")]
        public ActionResult AddEditPcLocation(int id, int pcId)
        {

            try
            {
                var countryService = new CountryPresenter();
                var model = new PrimeContractorModel();

                model.CountryList = countryService.GetCountries();
                model.DateFormatList = pcService.GetDateFormatList();
                model.PcLocation = new tblPrimeContractorserviceLocation();
                if (pcId != null && pcId > 0)
                {
                    model.PcLocation.PrimeContractorID = pcId;
                }
                ViewBag.pcAddmsg = TempData["pcAddmsg"];
                ViewBag.pcEditmsg = TempData["pcEditmsg"];
                ViewBag.ViewFor = "Add";

                if (id != null && id != 0)
                {
                    model.PcLocation = pcService.GetPcLocationById(id);
                    if (model.PcLocation != null)
                    {
                        model.LastUpdatedByUser =
                            countryService.GetUserNameByUserId(model.PcLocation.LastUpdatedByUserID ?? 0);
                        ViewBag.ViewFor = "Edit";
                    }
                }

                return View("PcLocationCrudView", model);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        [HttpPost]
        [ActionName("AddEditlocation")]
        public ActionResult AddEditPcLocationPost(tblPrimeContractorserviceLocation pcLocation)
        {
            try
            {
                if (pcLocation.PrimeContractorLocationID != null && pcLocation.PrimeContractorLocationID > 0)
                {
                    var edit = pcService.UpdatePcLocation(pcLocation, adminSetting.UserID);
                    TempData["editMsg"] = edit.ToString();
                }
                else
                {
                    var add = pcService.AddNewPcLocation(pcLocation, adminSetting.UserID);
                    TempData["Addmsg"] = add.ToString();
                }

                return RedirectToAction("AddEditPcLocation", new { id = 0, pcId = pcLocation.PrimeContractorID });

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public JsonResult GetExchangeRatesByCurrencyId(int currencyId, int pcId)
        {
            try
            {
                var getExRates = pcService.GetPcExchangeRatesByCurrencyIdAndPcId(currencyId, pcId);
                if (getExRates != null)
                {
                    var data = new
                    {
                        Rates = getExRates.ExchangeRateToUsDollars,
                        currencyId = getExRates.CurrencyID,
                        pcId = getExRates.PrimeContractorID,
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("notFound", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddUpdateExchageRates(int currencyId, int pcId, string exRates)
        {
            try
            {
                var getExRates = pcService.AddUpdatePcExchangeRatesByCurrencyIdAndPcId(currencyId, pcId, exRates,
                    adminSetting.UserID);
                if (getExRates)
                {
                    return Json("true", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("false", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult AddCurrentTabIdToSession(string tabId)
        {
            try
            {
                Session["currentTabId"] = tabId;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateUserStatus(int userId, bool isActive)
        {
            try
            {
                var currentUserId = Convert.ToInt32(Session["UserID"]);
                var data = pcService.UpdateUserStatus(userId, isActive, currentUserId);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult UpdateUserAccess(int userId, int securityLevel, string userGroupName)
        {
            try
            {
                var currentUserId = Convert.ToInt32(Session["UserID"]);
                var data = pcService.UpdateUserAccess(userId, securityLevel, adminSetting.UserID, userGroupName);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        [Route("prime-contractor/user/{id?}/{pcId?}")]
        public ActionResult AddEditPcUser(int id, int pcId)
        {

            try
            {
                if (pcId > 0)
                {
                    var countryService = new CountryPresenter();
                    var companyService = new CompanyPresenter();
                    var model = new PrimeContractorModel();

                    if (pcId > 0)
                    {
                        model.LocationsList =
                            pcService.GetLocationsByPrimeContractorId(pcId).Where(x => x.IsActive == 1).ToList();
                        model.DateFormatList = pcService.GetDateFormatList();

                        @ViewBag.CompanyId = pcService.GetPrimeContractorById(pcId).ParentCompanyID;
                    }
                    ViewBag.pcAddmsg = TempData["pcAddmsg"];
                    ViewBag.pcEditmsg = TempData["pcEditmsg"];
                    ViewBag.ViewFor = "Add";
                    model.PrimeContractorUser = new PrimeContractorUserModel();

                    model.PrimeContractorUser.PrimeContractorId = pcId;
                    if (id != 0)
                    {
                        @ViewBag.userContactId = companyService.GetUserContactIdbyUserId(id);
                        model.PrimeContractorUser = pcService.GetPcUserById(id);
                        if (model.PrimeContractorUser != null)
                        {
                            model.LastUpdatedByUser =
                                countryService.GetUserNameByUserId(model.PrimeContractorUser.LastUpdateByUser);
                            ViewBag.ViewFor = "Edit";
                        }
                    }

                    return View("AddEditUserView", model);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        [HttpPost]
        [Route("prime-contractor/add-user/")]
        public ActionResult AddEditPcUserPost(PrimeContractorUserModel pcUser)
        {

            try
            {
                if (pcUser.PrimeContractorId > 0)
                {
                    var companyService = new CompanyPresenter();
                    if (!companyService.ChecktUserByUsername(pcUser.UserName, pcUser.UserId))
                    {

                        if (pcUser.UserId > 0)
                        {
                            var add = pcService.EditPcUser(pcUser, adminSetting);
                            TempData["editMsg"] = add.ToString();
                        }
                        else
                        {
                            var add = pcService.AddNewPcUser(pcUser, adminSetting);
                            TempData["addMsg"] = add.ToString();
                        }

                    }
                    else
                    {
                        TempData["addMsg"] = "userExist";
                    }
                    return RedirectToAction("AddEditPcUser", new { id = pcUser.UserId, pcId = pcUser.PrimeContractorId });

                }

                TempData["editMsg"] = "False";
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
    }
}
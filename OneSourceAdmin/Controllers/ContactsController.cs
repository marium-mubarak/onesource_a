﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class ContactsController : BaseController
    {

        private ContactService contactService;

        public ContactsController()
        {
            contactService = new ContactService();

        }


        public ActionResult Index(string searchStr = "")
        {
            try
            {
                var model = new ContactModel();
                @ViewBag.source = "cd";
                model.ContactList = contactService.GetContactList(15, adminSetting.UserID, 0, searchStr);
                ViewBag.Message = TempData["Message"];
                Session["CurrentCompanyRole"] = null;
                Session["currentCompanyName"] = null;
                return View("Index", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                    TempData["Message"] = ex.InnerException.Message.ToString();
                    //  return Redirect("/company/contact/" + contactId + "/" + model.companyid);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult AddContact()
        {
            return RedirectToAction("Contact", "Company", new { source = "cd", contactId = "0", companyId = "0" });
        }

    }
}
﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Helper;

namespace OneSourceAdmin.Controllers
{
    public class ReportController : BaseController
    {
        private readonly AdminMenuService menuService;


        public ReportController()
        {
            menuService = new AdminMenuService();
        }


        public ActionResult Index(int id)
         {
            try
            {
                var model = new AdminMenuAndAccessModel();

                if (adminSetting != null && adminSetting.UserID > 0 && adminSetting.UserGroupID > 0 && id > 0)
                {
                    model = menuService.GetSubMenuByUserGroupIdAndUserIdAndParentId(adminSetting.UserGroupID, adminSetting.UserID, id.ConvertObjectToInt32());
                    var dbContext = new globalserveEntities();
                    ViewBag.MenuName = dbContext.tblAdminMenus.FirstOrDefault(x => x.MenuId == id)?.MenuName;
                }

                return View(model.AdminMenu);
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return null;
            }
        }


        public ActionResult RouteToURL(string route, int typeId, int id)
        {
            try
            {
                var menu = menuService.GetMenuById(typeId);

                Response.Redirect("~/Reports/" + menu.MenuName.Trim() + "/" + id);
                return null;
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return null;
            }
        }
    }
}
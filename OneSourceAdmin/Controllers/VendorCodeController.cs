﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing vendor code detail"
    /// </summary>
    public class VendorCodeController : BaseController
    {
        /// <summary>
        /// this action is responsible for providing view to user
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                VendorCodePresenter vc = new VendorCodePresenter();
                @ViewBag.AddEditAccess = isAddEditAllow("VendorCodes");
                return View(vc.GetVendorCodes());
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for providing specific vendor code
        /// </summary>
        /// <param name="VCode"></param>
        /// <returns></returns>
        public ActionResult VendorCode(int VCode)
        {
            try
            {
                VendorCodePresenter vc = new VendorCodePresenter();
                ViewBag.Message= TempData["Message"];
               
                return View("~/Views/VendorCode/AddEditVendorCode.cshtml", vc.GetVendorCodeInfo(VCode));
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for add edit vendor code
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public ActionResult AddEditVendorCode(tblXrefSFA model)
        {
           VendorCodePresenter vc = new VendorCodePresenter();           
           var data=vc.AddUpdateVendorCode(model, adminSetting.UserID);
           TempData["Message"] = data.GetType().GetProperty("msg").GetValue(data).ToString();
           return RedirectToAction("VendorCode",new { @Vcode=model.XrefSFAsID});
        }
    }
}
﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.html;
using System.Text.RegularExpressions;

namespace OneSourceAdmin.Controllers
{

    /// <summary>
    /// Date created : March 2 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing order tracking detail"
    /// </summary>
    public class OrderSearchController : BaseController
    {

        private OrderSearchPresenter osp;
        public OrderSearchController()
        {
            osp = new OrderSearchPresenter();
        }

        /// <summary>
        /// This action is responsible for providing view for search the order
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public ActionResult OrderSearch(int customerId, int countryId)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    OrderSearchModel model = osp.GetOrderSearchModel("ISD", countryId, customerId, string.Empty);
                    return View("~/Views/OrderSearch/OrderSearchISD.cshtml", model);
                }
                if (Session["UserGroupName"].ToString() == "SFA")
                {
                    OrderSearchModel model = osp.GetOrderSearchModel("SFA", countryId, customerId, Session["SFAID"].ToString());
                    return View("~/Views/OrderSearch/OrderSearchSFA.cshtml", model);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing searched result
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult OrderSearchRequest(OrderSearchModel model)
        {
            try
            {
                Session["OrderSearchModel"] = model;

                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    var data = osp.GetSearchedOrders_ISD(model, Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(), 500);
                    if (data.ISDOrderSearchlst.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("OrderSearch", new { customerId = model.CustomerID, countryId = model.CountryId });
                    }
                    Session["SearchOrderResult"] = data;
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    var data = osp.GetSearchedOrders_SFA(model, Session["DateFormatID"].ToString(), Session["SFAID"].ToString(), 500);
                    if (data.SFAOrderSearchlst.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("OrderSearch", new { customerId = model.CustomerID, countryId = model.CountryId });
                    }
                    Session["SearchOrderResult"] = data;
                }
                return RedirectToAction("SearchOrderList");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for listing the searched result
        /// </summary>
        /// <param name="requiredRecord"></param>
        /// <param name="requiredPageSize"></param>
        /// <param name="StracNoToDelFromList"></param>
        /// <returns></returns>
        public ActionResult SearchOrderList(int requiredRecord, int requiredPageSize, string StracNoToDelFromList)
        {
            try
            {
                if (requiredPageSize > requiredRecord)
                {
                    requiredPageSize = 50;
                    requiredRecord = 500;
                }

                ViewBag.PageSize = requiredPageSize;
                ViewBag.RequiredRecord = requiredRecord;

                if (requiredRecord > 5000)
                {
                    requiredRecord = 5000;
                    ViewBag.RequiredRecord = 5000;
                }
                if (requiredPageSize > 1000)
                {
                    ViewBag.PageSize = 1000;
                    requiredPageSize = 1000;
                }


                OrderListModel model = new OrderListModel();


                if (Session["SearchOrderResult"] != null)
                {
                    model = (OrderListModel)Session["SearchOrderResult"];

                    if (Session["UserGroupName"].ToString() == "ISD")
                    {

                        if (!string.IsNullOrEmpty(StracNoToDelFromList))
                        {
                            var item = model.ISDOrderSearchlst.FirstOrDefault(x => x.TrackingNumber == StracNoToDelFromList);
                            model.ISDOrderSearchlst.Remove(item);
                        }
                        if (model.ISDOrderSearchlst.Count != requiredRecord)
                        {
                            if (requiredRecord == 50001)
                            {
                                ViewBag.RequiredRecord = 50001;
                                model = osp.GetSearchedOrders_ISD((OrderSearchModel)Session["OrderSearchModel"], Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(), model.TotalRecord);
                            }
                            else
                            {
                                model = osp.GetSearchedOrders_ISD((OrderSearchModel)Session["OrderSearchModel"], Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(), requiredRecord);
                            }
                            Session["SearchOrderResult"] = model;
                        }

                    }
                    else if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        if (!string.IsNullOrEmpty(StracNoToDelFromList))
                        {
                            var item = model.SFAOrderSearchlst.FirstOrDefault(x => x.TrackingNumber == StracNoToDelFromList);
                            model.SFAOrderSearchlst.Remove(item);
                        }
                        if (model.SFAOrderSearchlst.Count != requiredRecord)
                        {
                            if (requiredRecord == 50001)
                            {
                                ViewBag.RequiredRecord = 50001;
                                model = osp.GetSearchedOrders_SFA((OrderSearchModel)Session["OrderSearchModel"], Session["DateFormatID"].ToString(), Session["SFAID"].ToString(), model.TotalRecord);
                            }
                            else
                            {
                                model = osp.GetSearchedOrders_SFA((OrderSearchModel)Session["OrderSearchModel"], Session["DateFormatID"].ToString(), Session["SFAID"].ToString(), requiredRecord);
                            }
                            Session["SearchOrderResult"] = model;
                        }
                    }


                }




                //if (requiredRecord > 500 && Session["OrderSearchModel"]!=null)
                //{
                //    Session["SearchOrderResult"] = osp.GetSearchedOrders((OrderSearchModel)Session["OrderSearchModel"], Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(),requiredRecord);
                //}
                //OrderListModel model = new OrderListModel();
                //if (Session["SearchOrderResult"] != null)
                //{
                //    model = (OrderListModel)Session["SearchOrderResult"];
                //}
                ViewBag.TotalCount = model.TotalRecord;
                return View("~/Views/OrderSearch/OrderList.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for provide the detail of specific order
        /// </summary>
        /// <param name="sessionTrackingNo"></param>
        /// <param name="tabId"></param>
        /// <returns></returns>
        public ActionResult OrderDetail(string sessionTrackingNo, string tabId = "order-detail")
        {
            try
            {
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    ViewBag.Message = TempData["Message"];
                    var orderDetailModel = osp.GetOrderDetail(sessionTrackingNo, "ISD", 0);
                    return View("~/Views/OrderSearch/ISD/OrderDetail.cshtml", orderDetailModel);
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    ViewBag.Message = TempData["Message"];
                    var orderDetailModel = osp.GetOrderDetail(sessionTrackingNo, "SFA", Convert.ToInt32(Session["SFAID"]));
                    return View("~/Views/OrderSearch/SFA/OrderDetail_SFA.cshtml", orderDetailModel);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating the order status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateOrderStatus(OrderStatusModel model)
        {
            try
            {
                if (model != null)
                {
                    string sessionTrackingId = osp.UpdateOrderStatus(model, Convert.ToInt32(Session["UserID"]));


                    if (Session["SearchOrderResult"] != null)
                    {
                        var data = (OrderListModel)Session["SearchOrderResult"];
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            var currentStatus = osp.GetOrderStatusCode(model.OrderStatusId);
                            if (!string.IsNullOrEmpty(currentStatus))
                            {
                                data.ISDOrderSearchlst.Where(x => x.TrackingNumber == (model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber)).ToList()
                                    .ForEach(x => { x.Status = currentStatus; });
                            }
                        }
                        else
                        {

                        }
                    }
                    return Json(sessionTrackingId, JsonRequestBehavior.AllowGet);

                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsilbe for changing the order status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ChangeOrderStatus(OrderStatusModel model)
        {
            try
            {
                if (model != null)
                {
                    object data = osp.ChangeOrderStatus(model, Convert.ToInt32(Session["UserID"]));
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for updating the order detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult UpdateOrderDetail(OrderDetailUpdateModel model)
        {
            try
            {
                if (model != null && model.OrderDate != null && model.SessionTrackingNumber > 0 && model.OrderTrackingNumber > 0 && model.DestinationTrackingNumber > 0)
                {
                    int a = osp.UpdateOrderUpdate(model, Session["UserGroupName"].ToString());
                    if (a > 0 && Session["SearchOrderResult"] != null)
                    {
                        var data = (OrderListModel)Session["SearchOrderResult"];
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            data.ISDOrderSearchlst.Where(x => x.TrackingNumber == (model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber)).ToList()
                                .ForEach(x => { x.OrderPO = model.OrderPO; x.OrderDate = model.OrderDate.ToString("MM/dd/yyyy"); });
                        }
                        else
                        {

                        }
                    }
                    TempData["Message"] = a > 0 ? "Order Detail Updated Successfully" : "No record were updated";
                }
                if (model != null)
                {
                    return RedirectToAction("OrderDetail", new { @sessionTrackingNo = model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber });
                }
                return RedirectToAction("OrderSearch");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for changes and cancellation of request
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="Comments"></param>
        /// <returns></returns>
        public ActionResult ChangesAndCancellationRequest(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string Comments)
        {
            try
            {
                if (SessionTrackingNumber > 0 && OrderTrackingNumber > 0 && DestinationTrackingNumber > 0 && !string.IsNullOrEmpty(Comments))
                {
                    var count = osp.ChangesAndCancellationRequest(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, Comments, Session["UserName"].ToString());
                    TempData["Message"] = count > 0 ? "Changes & Cancellation Request has been send" : "No request were send";
                    return RedirectToAction("OrderDetail", new { @sessionTrackingNo = SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber });
                }
                return RedirectToAction("OrderSearch");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for resend the order
        /// </summary>
        /// <param name="sessionTrackingNo"></param>
        /// <param name="orderTrackingNo"></param>
        /// <param name="destinationTrackingNo"></param>
        /// <returns></returns>
        public ActionResult ResendOrder(int sessionTrackingNo, int orderTrackingNo, int destinationTrackingNo)
        {
            try
            {
                if (sessionTrackingNo > 0 && orderTrackingNo > 0 && destinationTrackingNo > 0)
                {
                    try
                    {
                        osp.ResendOrder(sessionTrackingNo, orderTrackingNo, destinationTrackingNo);
                        TempData["Message"] = "Order has been resend";
                        return RedirectToAction("OrderDetail", new { @sessionTrackingNo = sessionTrackingNo + "-" + orderTrackingNo + "-" + destinationTrackingNo });
                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "Error occured in resending Order";
                        return RedirectToAction("OrderDetail", new { @sessionTrackingNo = sessionTrackingNo + "-" + orderTrackingNo + "-" + destinationTrackingNo });
                    }
                }
                return RedirectToAction("OrderSearch");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this acition is responsible for getting order items
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="CustomerName"></param>
        /// <param name="Status"></param>
        /// <param name="DateAcceptedBySupplier"></param>
        /// <returns></returns>
        public ActionResult GetOrderItems(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string CustomerName, string Status, DateTime? DateAcceptedBySupplier)
        {
            try
            {
                if (SessionTrackingNumber > 0 && OrderTrackingNumber > 0 && DestinationTrackingNumber > 0)
                {
                    var model = osp.GetOrderItems(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, CustomerName, Status, DateAcceptedBySupplier, Session["DateFormatID"].ToString());
                    return PartialView("~/Views/OrderSearch/_OrderItem.cshtml", model);
                }
                return RedirectToAction("OrderSearch");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for cancellation of part
        /// </summary>
        /// <param name="OrderItemID"></param>
        /// <param name="Comments"></param>
        /// <returns></returns>
        public JsonResult PartCancellationRequest(int OrderItemID, string Comments)
        {
            if (OrderItemID > 0 && !string.IsNullOrEmpty(Comments))
            {
                try
                {
                    osp.PartCancellationRequest(OrderItemID, Convert.ToInt32(Session["UserID"]), Comments, Session["UserName"].ToString());
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for updating the order item detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult UpdateOrderItems(OrderItemUpdateModel model)
        {
            if (model != null)
            {
                try
                {
                    int count = osp.UpdateOrderItems(model, Convert.ToInt32(Session["UserID"]), Session["UserGroupName"].ToString(), Session["UserName"].ToString());

                    if (Session["SearchOrderResult"] != null)
                    {
                        var data = (OrderListModel)Session["SearchOrderResult"];
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {

                            var currentStatus = osp.GetOrderStatusCode(osp.GetOrderStatusIdByOrderId(model.SessionTrackingNumber));
                            if (!string.IsNullOrEmpty(currentStatus))
                            {
                                data.ISDOrderSearchlst.Where(x => x.TrackingNumber == (model.SessionTrackingNumber + "-" + model.OrderTrackingNumber + "-" + model.DestinationTrackingNumber)).ToList()
                                    .ForEach(x => { x.Status = currentStatus; });
                            }
                        }
                        else
                        {

                        }
                    }
                    if (count == 0)
                    {
                        return Json("-1", JsonRequestBehavior.AllowGet);
                    }
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for getting the order comments
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <returns></returns>
        public ActionResult GetOrderComments(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber)
        {
            try
            {
                if (SessionTrackingNumber > 0 && OrderTrackingNumber > 0 && DestinationTrackingNumber > 0)
                {
                    int SupplierId;
                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        SupplierId = 0;
                    }
                    else
                    {
                        SupplierId = Convert.ToInt32(Session["SFAID"]);
                    }

                    var model = osp.GetOrderComments(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, Convert.ToInt32(Session["DateFormatID"]), SupplierId);
                    model.ListSuppliers = osp.GetSupplierListByOrder(Convert.ToInt32(SessionTrackingNumber));
                    return PartialView("~/Views/OrderSearch/_OrderComment.cshtml", model);
                }
                return RedirectToAction("OrderSearch");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for adding new comments to the specific order
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="Comments"></param>
        /// <param name="isGSOnly"></param>
        /// <returns></returns>
        public JsonResult AddOrderComment(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string Comments, int SupplierId, string SupplierName, bool isGSOnly = false, bool IsUrgent = false, string JiraTicketNum = "")
        {
            if (SessionTrackingNumber > 0 && OrderTrackingNumber > 0 && DestinationTrackingNumber > 0)
            {
                try
                {
                    int supplierId;
                    string supplierName = "";
                    bool isPCComment = false;

                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        supplierId = SupplierId;
                        supplierName = SupplierName;
                        isPCComment = true;
                    }
                    else
                    {
                        supplierId = Convert.ToInt32(Session["SFAID"]);
                        supplierName = Session["SFAName"].ToString();
                        isPCComment = false;
                    }

                    int count = osp.AddOrderComment(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, Comments, isGSOnly, Convert.ToInt32(Session["UserID"]), Session["UserName"].ToString(), supplierId, supplierName, isPCComment, IsUrgent, JiraTicketNum);
                    if (count == 0)
                    {
                        return Json("-1", JsonRequestBehavior.AllowGet);
                    }
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for getting order attachments
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <returns></returns>
        public ActionResult GetOrderAttachments(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber)
        {
            try
            {
                if (SessionTrackingNumber > 0 && OrderTrackingNumber > 0 && DestinationTrackingNumber > 0)
                {
                    var model = osp.GetOrderAttachments(SessionTrackingNumber);
                    return PartialView("~/Views/OrderSearch/_OrderAttachment.cshtml", model);
                }
                return RedirectToAction("OrderSearch");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsilbe for adding order attachments
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="attachFile"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddOrderAttachment(string sessionTrackingNumber, HttpPostedFileBase attachFile)
        {
            var msg = string.Empty;
            var isSuccess = false;
            try
            {
                if (attachFile != null && attachFile.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(attachFile.FileName).Replace("^", "");
                    var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                    string path = string.Empty;
                    if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".csv" || fileExtension == ".png" || fileExtension == ".txt")
                    {
                        if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                        {
                            var docId = System.Guid.NewGuid().ToString();
                            path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                            attachFile.SaveAs(path);
                            int? count = osp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path, ConfigurationManager.AppSettings["UploadURL"] + "/" + docId + "^" + fileName, Convert.ToInt32(Session["UserID"]), sessionTrackingNumber, null);
                            if (count <= 0)
                            {
                                msg = "File Attaching Error";
                            }
                            else
                            {
                                msg = "Attachment has been added successfully";
                            }

                            isSuccess = true;
                        }
                        else
                        {
                            msg = "Invalid file size. Maximum allowed size is 10MB.";
                        }
                    }
                    else
                    {
                        msg = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";
                    }
                }
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = "Error Occured";
                isSuccess = false;
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsilbe for deleting the attachments
        /// </summary>
        /// <param name="daid"></param>
        /// <returns></returns>
        public JsonResult DeleteAttachment(int daid)
        {
            if (daid > 0)
            {
                try
                {
                    int count = 0;
                    var url = osp.GetRequestedAttachmentUrl(daid);
                    if (!string.IsNullOrEmpty(url))
                    {
                        string path = Server.MapPath(url);
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                        count = osp.DeleteAttachment(daid);
                    }
                    return Json(count > 0 ? 1 : -1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for adding adding reason for delay to order
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="Reason"></param>
        /// <returns></returns>
        public JsonResult AddReasonForDelay(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, string Reason)
        {
            if (SessionTrackingNumber > 0 && OrderTrackingNumber > 0 && DestinationTrackingNumber > 0 && !string.IsNullOrEmpty(Reason))
            {
                try
                {
                    int count = osp.ReasonForDelayISD(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, Reason, Session["UserName"].ToString());
                    if (count == 0)
                    {
                        return Json("-1", JsonRequestBehavior.AllowGet);
                    }
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderStatusCode(string sessionTrackingNo)
        {
            if (!string.IsNullOrEmpty(sessionTrackingNo))
            {
                sessionTrackingNo = (sessionTrackingNo.Trim().Split('-'))[0];
            }

            var result = osp.GetOrderStatusCode(sessionTrackingNo);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAssetDisQuoteDetail(int quoteId)
        {
            try
            {
                var data = osp.GetAssetDisQuoteDetail(quoteId);
                if (data != null)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("-1", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 3rd September 2018  
        /// This function is used for exporting order details to pdf.
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <returns></returns>
        public ActionResult ExportOrderDetailToPdf(string sessionTrackingNumber)
        {
            try
            {
                var detail = new OrderDetailModel();
                var sessionTrackingNo = "";
                if (!string.IsNullOrEmpty(sessionTrackingNumber))
                {
                    sessionTrackingNo = (sessionTrackingNumber.Trim().Split('-'))[0];
                    var orderTrackingNo = (sessionTrackingNumber.Trim().Split('-'))[1];
                    var destinationTrackingNo = (sessionTrackingNumber.Trim().Split('-'))[2];

                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        detail = osp.GetOrderDetail(sessionTrackingNumber, "ISD", 0);
                    }
                    else if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        detail = osp.GetOrderDetail(sessionTrackingNumber, "SFA", Convert.ToInt32(Session["SFAID"]));
                    }

                    if (detail.OrderDetail.Status == "N" || detail.StatusCode == "R")
                    {
                        detail.OrderDetail.Status = "New";
                    }
                    if (detail.OrderDetail.Status == "P")
                    {
                        detail.OrderDetail.Status = "In Progress";
                    }
                    if (detail.OrderDetail.Status == "D")
                    {
                        detail.OrderDetail.Status = "Delivered";
                    }
                    if (detail.OrderDetail.Status == "S")
                    {
                        detail.OrderDetail.Status = "Shipped";
                    }
                    if (detail.OrderDetail.Status == "C")
                    {
                        detail.OrderDetail.Status = "Shipped Complete";
                    }
                    if (detail.OrderDetail.Status == "X")
                    {
                        detail.OrderDetail.Status = "Cancelled";
                    }
                    var shippingMethod = detail.ShippingMethod;

                    var orderItems = osp.GetOrderItems(Convert.ToInt32(sessionTrackingNo), Convert.ToInt32(orderTrackingNo), Convert.ToInt32(destinationTrackingNo), detail.OrderDetail.CustomerName, detail.StatusCode, detail.DateAcceptedBySupplier, Session["DateFormatID"].ToString());

                    var attachmentsName = osp.GetOrderAttachments(Convert.ToInt32(sessionTrackingNo)).Select(x => x.daname).ToList();

                    int SupplierId;
                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        SupplierId = 0;
                    }
                    else
                    {
                        SupplierId = Convert.ToInt32(Session["SFAID"]);
                    }

                    var orderComments = osp.GetOrderComments(Convert.ToInt32(sessionTrackingNo), Convert.ToInt32(orderTrackingNo), Convert.ToInt32(destinationTrackingNo), Convert.ToInt32(Session["DateFormatID"]), SupplierId);

                    iTextSharp.text.Image logoImg = iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                    logoImg.ScaleAbsolute(250, 45);

                    var titleFont = FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.BOLD);
                    var title = new Paragraph("Order Summary (" + sessionTrackingNo + " - 1 - 1 )", titleFont);
                    title.Alignment = Element.ALIGN_CENTER;
                    PdfPTable pdfTable = FillOrderDetailPdfDetails(detail, shippingMethod);

                    var redLabelFont = FontFactory.GetFont("Arial", 8, 0, BaseColor.RED);
                    var redLabel = new Paragraph("Payment is due 30 days from date of invoice.", redLabelFont);




                    var itemTblwidth = 10;
                    if (detail.OrderDetail.CustomerName != null && (detail.OrderDetail.CustomerName.ToLower().Contains("xl catlin")))
                    {
                        itemTblwidth = 11;
                    }



                    float[] itemTableColumnsWitdh = new float[] { 12f, 4f, 18f, 18.5f, 18f, 20.5f, 9f };
                    var columnNames = new string[] { "Description", "SKU", "OEM", "Price", "Qty", "Total", "Supplier",
                    "Expected Ship Date", "Date Shipped","MFG Tracking #"};
                    float[] columnsWitdh = new float[] { 10f, 5f, 4f, 5f, 4f, 5f, 5f, 5f, 5f, 5f };
                    if (detail.OrderDetail.CustomerName != null && (detail.OrderDetail.CustomerName.ToLower().Contains("xl catlin")))
                    {

                        columnNames = new string[] { "Description", "SKU","Category", "OEM", "Price", "Qty", "Total", "Supplier",
                    "Expected Ship Date", "Date Shipped","MFG Tracking #"};
                        columnsWitdh = new float[] { 9f, 4f, 4f, 4f, 4f, 4f, 4f, 5f, 5f, 5f, 5f };
                    }



                    PdfPTable OrderItemsTbl = new PdfPTable(itemTblwidth);

                    MakePdfFileDataGridHeader(OrderItemsTbl, columnNames, columnsWitdh, 100);

                    OrderItemsPdfFileDataGridBody(OrderItemsTbl, orderItems, detail.OrderDetail.CustomerName, detail.OrderDetail.Symbol);

                    PdfPTable attachmenttabl = new PdfPTable(1);

                    PdfPTable commentsTblAdmin = AddExportComments(orderComments.OrderCommentsbySupplier.Where(x => x.GSOnly == false && x.PCComment == true).ToList(), sessionTrackingNo);
                    PdfPTable commentsTblSupplier = AddExportComments(orderComments.OrderCommentsbySupplier.Where(x => x.GSOnly == false && x.PCComment == false).ToList(), sessionTrackingNo);
                    PdfPTable commentsTblGSOnly = AddExportComments(orderComments.OrderCommentsbySupplier.Where(x => x.GSOnly == true).ToList(), sessionTrackingNo);


                    PdfPTable disClaimerTbl = new PdfPTable(1);
                    var disClaimerCol = new string[] { "" };

                    float[] columnsWitdh1 = new float[] { 100f };

                    attachmenttabl.WidthPercentage = 100;
                    AddAttachmentstoPdfTable(attachmenttabl, attachmentsName);

                    using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                    {
                        Document document = new Document(PageSize.A2, 10, 10, 20, 10);
                        PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                        document.Open();
                        if (detail.OrderDetail.CustomerName.ToLower() != "abbott")
                        {
                            document.Add(logoImg);
                        }
                        document.Add(title);
                        document.Add(new Paragraph(""));
                        document.Add(new Chunk("\n"));
                        document.Add(pdfTable);

                        document.Add(new Chunk("\n"));
                        document.Add(OrderItemsTbl);

                        document.Add(new Chunk("\n"));
                        document.Add(redLabel);
                       

                        if (attachmentsName.Count > 0)
                        {
                            document.Add(new Chunk("\n"));
                            document.Add(new Paragraph("Attachments:"));
                            document.Add(new Chunk("\n"));
                            document.Add(attachmenttabl);

                        }
                        if (orderComments.OrderCommentsbySupplier != null && orderComments.OrderCommentsbySupplier.Count > 0)
                        {
                            document.Add(new Chunk("\n"));
                            document.Add(new Paragraph("Comments:"));
                            document.Add(new Chunk("\n"));

                            if (orderComments.OrderCommentsbySupplier.Where(x => x.GSOnly == false && x.PCComment == true).Count() > 0)
                            {
                                document.Add(new Paragraph("Admin:", FontFactory.GetFont("Arial", 10, 0, BaseColor.BLACK)));
                                document.Add(new Chunk("\n"));
                                document.Add(commentsTblAdmin);
                                document.Add(new Chunk("\n"));
                            }

                            if (orderComments.OrderCommentsbySupplier.Where(x => x.GSOnly == false && x.PCComment == false).Count() > 0)
                            {
                                document.Add(new Paragraph("Supplier:", FontFactory.GetFont("Arial", 10, 0, BaseColor.BLACK)));
                                document.Add(new Chunk("\n"));
                                document.Add(commentsTblSupplier);
                                document.Add(new Chunk("\n"));
                            }

                            if (orderComments.OrderCommentsbySupplier.Where(x => x.GSOnly == true).Count() > 0)
                            {
                                document.Add(new Paragraph("GS Only:", FontFactory.GetFont("Arial", 10, 0, BaseColor.BLACK)));
                                document.Add(new Chunk("\n"));
                                document.Add(commentsTblGSOnly);
                                document.Add(new Chunk("\n"));
                            }
                        }

                        document.Close();
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();

                        Response.Clear();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", "attachment; filename=OrderDetails - " + sessionTrackingNumber + ".pdf");
                        Response.ContentType = "application/pdf";
                        Response.Buffer = true;
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        public static void MakePdfFileDataGridHeader(PdfPTable pdfTable, string[] columnsNames, float[] columnsWitdh, int widthPercentage)
        {
            pdfTable.SetWidths(columnsWitdh);
            pdfTable.WidthPercentage = widthPercentage;
            foreach (var item in columnsNames)
            {
                PdfPCell cell = new PdfPCell(new Phrase(item, new Font(null, 8f, Font.BOLD))) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                BaseColor myColor = WebColors.GetRGBColor("#3B9AD2");
                cell.BackgroundColor = myColor;
                pdfTable.AddCell(cell);
            }

        }

        public PdfPTable AddExportComments(List<sp_getCommentsBySupplier_AdminR_Result> model, string sessionTrackingNo)
        {
            try
            {
                PdfPTable pdfTable = new PdfPTable(5);
                var columnNames = new string[] { "Supplier Name", "Log Date", "Logged By", "Company", "Comments" };

                float[] columnsWitdh = new float[] { 15f, 10f, 15f, 15f, 30f };

                MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


                if (model.Count == 0)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return pdfTable;
                }

                int count = 0;
                foreach (var item in model)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(Convert.ToString(item.SupplierName != null ? item.SupplierName : "-")), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(Convert.ToString(item.LogDate)), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(Convert.ToString(item.CreatedBy)), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(Convert.ToString(item.Company)), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(Convert.ToString(item.Comments)), backgrounColor));
                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public static PdfPCell AddDatatoPdfCell(Chunk pdfChunk, BaseColor backgrounColor)
        {
            BaseColor dataGridBodyFontCOlor = WebColors.GetRGBColor("#333333");
            Font dataGridBodyFOnt = new Font(null, 8, Font.NORMAL, color: dataGridBodyFontCOlor);
            BaseColor dataBodyborderColor = WebColors.GetRGBColor("#ddd");

            var cell =
                new PdfPCell(new Paragraph(new Chunk(pdfChunk.ToString()) { Font = dataGridBodyFOnt }) { Font = dataGridBodyFOnt })
                {
                    BorderColor = dataBodyborderColor,
                    BorderWidthLeft = 0,
                    BorderWidthRight = 0,
                    BackgroundColor = backgrounColor,
                    BorderWidthTop = 0,
                    PaddingTop = 4,
                    PaddingBottom = 4

                };

            return cell;
        }
        public static string RemoveHtmlTags(string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                return Regex.Replace(input, "<.*?>", String.Empty);
            }

            return "";
        }
        public void OrderItemsPdfFileDataGridBody(PdfPTable pdfTable, OrderItemsModel data, string customerName, string currencySymbol)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data.OrderItems)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    //string date = string.Format("{0:MM/dd/yy}", item.OrderDate);
                    //string etsDate = string.Format("{0:MM/dd/yy}", Convert.ToDateTime(item.ETS));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(RemoveHtmlTags(item.PartDescription) ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.ManufacturerSKU ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.ManufacturerName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(currencySymbol + " " + Convert.ToDecimal(item.LineItemPrice).ToString("#,##0.00")), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Quantity.ToString()), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(currencySymbol + " " + Convert.ToDecimal(item.LineItemTotal).ToString("#,##0.00")), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.SFAName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.ETA == null ? "-" : Convert.ToDateTime(item.ETA).ToString()), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.DateShipped == null ? "-" : Convert.ToDateTime(item.DateShipped).ToString()), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.MfgTrackingID ?? "-"), backgrounColor));
                }

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }

        public PdfPTable FillOrderDetailPdfDetails(OrderDetailModel data, string shippingMethod)
        {
            try
            {
                PdfPTable pdfTable = new PdfPTable(6);

                float[] columnsWitdh = new float[] { 12f, 22f, 12f, 22f, 10f, 22f };
                var columnNames = new string[] { " ", " ", "  ", " ", " ", "   " };
                pdfTable.SetWidths(columnsWitdh);
                pdfTable.WidthPercentage = 100;

                iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
                iTextSharp.text.Font fontRed = new iTextSharp.text.Font(null, 8f, 0, iTextSharp.text.BaseColor.RED);
                PdfPCell cell = new PdfPCell();
                BaseColor backgrounColor = WebColors.GetRGBColor("#eeeeee");
                cell.BackgroundColor = backgrounColor;


                //row 1
                //cell 1
                cell = new PdfPCell(new Phrase("Order Po:", fontLabel)) { Border = 0, BorderWidthLeft = 1, BorderWidthTop = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.OrderDetail.OrderPO ?? "-", fontNormal)) { Border = 0, BorderWidthTop = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Customer:", fontLabel)) { Border = 0, BorderWidthTop = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(data.OrderDetail.CustomerName ?? "-", fontNormal)) { Border = 0, BorderWidthTop = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Reference:", fontLabel)) { Border = 0, BorderWidthTop = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(data.OrderDetail.DestinationPO ?? "-", fontNormal)) { Border = 0, BorderWidthRight = 1, BorderWidthTop = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 2
                //cell 1
                cell = new PdfPCell(new Phrase("Order By:", fontLabel)) { Border = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.OrderDetail.UserName ?? "-", fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Shipping Status:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(data.OrderDetail.Status ?? "-", fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Invoice Number:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(data.OrderDetail.InvoiceNumber ?? "-", fontNormal)) { Border = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 3
                //cell 1
                cell = new PdfPCell(new Phrase("Order Date:", fontLabel)) { Border = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(Convert.ToString(data.OrderDetail.OrderDate), fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Created By:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(data.OrderDetail.UserName ?? "-", fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Shipped Date:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(Convert.ToString(data.OrderDetail.DateShipped), fontNormal)) { Border = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 4
                //cell 1
                cell = new PdfPCell(new Phrase("Country:", fontLabel)) { Border = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2

                cell = new PdfPCell(new Phrase(Convert.ToString(data.OrderDetail.OrderCountry), fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //Paragraph p = new Paragraph();
                //p.Font = fontNormal;
                //// p.Alignment = Element.ALIGN_CENTER;
                //p.Add(new Phrase(" " + data.OrderDetail.OrderCountry));
                //cell.AddElement(p);
                //cell.VerticalAlignment = 0;
                //cell.PaddingBottom = 5;
                //cell.PaddingTop = 5;
                //cell.HorizontalAlignment = 1;
                //cell.Border = 0;
                //pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Accepted Date:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(Convert.ToString(data.DateAcceptedBySupplier), fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Currency:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(data.OrderDetail.Currency ?? "-", fontNormal)) { Border = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 5
                //cell 1
                cell = new PdfPCell(new Phrase("Delivery Address:", fontLabel)) { Border = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.OrderDetail.ShippingCondensedAddress ?? "-", fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Order Total:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(data.OrderDetail.Symbol + " " + Convert.ToDouble(data.OrderDetail.Total).ToString("#,##0.00"), fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Invoice Address:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(Convert.ToString(data.OrderDetail.BillingCondensedAddress), fontNormal)) { Border = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 6
                //cell 1
                cell = new PdfPCell(new Phrase("Delivery Contact:", fontLabel)) { Border = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.OrderDetail.ShippingContact ?? "-", fontNormal)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Shipping Method:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(shippingMethod ?? "-", fontRed)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Invoice Contact:", fontLabel)) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(data.OrderDetail.BillingContact, fontNormal)) { Border = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 7
                //cell 1
                cell = new PdfPCell(new Phrase("Acc. Code:", fontLabel)) { Border = 0, BorderWidthBottom = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase("-", fontNormal)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Total VAT:", fontLabel)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(Convert.ToDecimal(data.OrderDetail.VAT).ToString() ?? "-", fontNormal)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Shipping Charges:", fontLabel)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase("-", fontNormal)) { Border = 0, BorderWidthBottom = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //if (!string.IsNullOrWhiteSpace(data.OrderDetail.CustomerName) && data.OrderDetail.CustomerName.ToLower().Contains("xl catlin"))
                //{//for xl catling customer only
                //    //row 9
                //    //cell 1
                //    cell = new PdfPCell(new Phrase("Cost Centre:", fontLabel)) { Border = 0, BorderWidthBottom = 0, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 2
                //    cell = new PdfPCell(new Phrase(data.Cost, fontNormal)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 3
                //    cell = new PdfPCell(new Phrase("OBU:", fontLabel)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 4
                //    cell = new PdfPCell(new Phrase(data.OBU, fontNormal)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 5
                //    cell = new PdfPCell(new Phrase("BLIC:", fontLabel)) { Border = 0, BorderWidthBottom = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 6
                //    cell = new PdfPCell(new Phrase(data.BLIC, fontNormal)) { Border = 0, BorderWidthBottom = 0, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //row 8
                //    //cell 1
                //    cell = new PdfPCell(new Phrase("GL:", fontLabel)) { Border = 0, BorderWidthBottom = 1, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 2
                //    cell = new PdfPCell(new Phrase(data.GL, fontNormal)) { Border = 0, BorderWidthBottom = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 3
                //    cell = new PdfPCell(new Phrase("", fontLabel)) { Border = 0, BorderWidthBottom = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 4
                //    cell = new PdfPCell(new Phrase("", fontNormal)) { Border = 0, BorderWidthBottom = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 5
                //    cell = new PdfPCell(new Phrase("", fontLabel)) { Border = 0, BorderWidthBottom = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);

                //    //cell 6
                //    cell = new PdfPCell(new Phrase("", fontNormal)) { Border = 0, BorderWidthBottom = 1, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                //    cell.BackgroundColor = backgrounColor;
                //    pdfTable.AddCell(cell);


                //}

                if (!string.IsNullOrWhiteSpace(data.OrderDetail.CustomerName) && data.OrderDetail.CustomerName.ToLower().Contains("russel"))
                {
                    //row 8
                    //cell 1
                    cell = new PdfPCell(new Phrase("Shipping Contact Phone:", fontLabel)) { Border = 0, BorderWidthBottom = 1, BorderWidthLeft = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.OrderDetail.ShippingContact, fontNormal)) { Border = 0, BorderWidthBottom = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("", fontLabel)) { Border = 0, BorderWidthBottom = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell = new PdfPCell(new Phrase("", fontNormal)) { Border = 0, BorderWidthBottom = 1, BorderWidthRight = 1, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                }




                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }

        public void AddAttachmentstoPdfTable(PdfPTable pdfTable, List<string> data)
        {
            try
            {

                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item ?? "-"), backgrounColor));
                }

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }

        #region SFA
        /// <summary>
        /// this action is responsible for adding invoice detail
        /// </summary>
        /// <param name="SessionTrackingNumber"></param>
        /// <param name="OrderTrackingNumber"></param>
        /// <param name="DestinationTrackingNumber"></param>
        /// <param name="SFAInvoiceDate"></param>
        /// <param name="SFAInvoiceNumber"></param>
        /// <returns></returns>
        public JsonResult UpdateInvoiceDetail(int SessionTrackingNumber, int OrderTrackingNumber, int DestinationTrackingNumber, DateTime SFAInvoiceDate, string SFAInvoiceNumber)
        {
            try
            {
                if (SessionTrackingNumber > 0)
                {
                    int count = osp.UpdateInvoiceDetail(SessionTrackingNumber, OrderTrackingNumber, DestinationTrackingNumber, SFAInvoiceDate, SFAInvoiceNumber, Convert.ToInt32(Session["UserID"]));
                    if (count == 0)
                    {
                        return Json("0", JsonRequestBehavior.AllowGet);
                    }
                    return Json(SessionTrackingNumber + "-" + OrderTrackingNumber + "-" + DestinationTrackingNumber, JsonRequestBehavior.AllowGet);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("-1", JsonRequestBehavior.AllowGet);
            }

        }


        #endregion


    }
}
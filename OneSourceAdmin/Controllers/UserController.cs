﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class UserController : BaseController
    {
        /// <summary>
        /// Date created : Oct 14 2016
        /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
        /// This Controller Class is responsible for providing User detail"
        /// </summary>
      
        public ActionResult Index(int? customerId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    UserPresenter up = new UserPresenter();
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("OsUserSetup");
                    return View(up.GetCustomerUserSetting());
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        /// <summary>
        /// This action is reponsible for returning the customers in json format
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetUsers(int customerId)
        {
            UserPresenter up = new UserPresenter();
            return Json(up.GetUserByCustomer(customerId), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// This action is responsible for updating customer record
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ActionResult UpdateCustomerUserSetting(tblOneStreetUser user)
        {

            if (Session["UserID"] != null)
            {
                try
                {
                    UserPresenter up = new UserPresenter();
                    int count = up.UpdateCustomerUserSetting(user);
                    if (count > 0) TempData["Message"] = "Record has been updated";
                    return RedirectToAction("Index", new { @customerId = user.CustomerID, val= user.OnstreetAccess==true?"true":"false" });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public JsonResult GetSelectedCustomerUser(int customerID,int userID)
        {
            UserPresenter up = new UserPresenter();
            return Json(up.GetSelectedCustomerUser(customerID, userID), JsonRequestBehavior.AllowGet);
        }
    }
}
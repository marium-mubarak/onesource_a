﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : feb 13 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for product maintenance"
    /// </summary>
    public class ProductMaintenanceController : BaseController
    {
        // GET: ProductMaintenance

        private ProductMaintenancePresent pmService;
        public ProductMaintenanceController()
        {
            pmService = new ProductMaintenancePresent();
        }

        #region partcategories

        /// <summary>
        /// this action is responsible for providing part categories view
        /// </summary>
        /// <returns></returns>
        public ActionResult PartCategories()
        {
            try
            {
                ProductMaintenancePresent vc = new ProductMaintenancePresent();
                @ViewBag.AddEditAccess = isAddEditAllow("PartCategories");
                return View(vc.GetPartCategories());
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing part category by ID
        /// </summary>
        /// <param name="PCId"></param>
        /// <returns></returns>
        public ActionResult PartCategory(int PCId)
        {
            try
            {
                ProductMaintenancePresent vc = new ProductMaintenancePresent();
                ViewBag.Message = TempData["Message"];

                return View("~/Views/ProductMaintenance/AddEditPartCategory.cshtml", vc.GetPartCategoryInfo(PCId));
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for add edit part category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditPartCategory(tblPartCategory model)
        {
            ProductMaintenancePresent vc = new ProductMaintenancePresent();
            var data = vc.AddUpdatePartCategories(model, adminSetting.UserID);
            TempData["Message"] = data.GetType().GetProperty("msg").GetValue(data).ToString();
            return RedirectToAction("PartCategory", new { @PCId = model.PartCategoryID });
        }

        #endregion

        #region Freight Carriers

        /// <summary>
        /// this action is responsible for providing Freight Carriers
        /// </summary>
        /// <returns></returns>
        public ActionResult FreightCarriers()
        {
            try
            {
                ProductMaintenancePresent vc = new ProductMaintenancePresent();
                @ViewBag.AddEditAccess = isAddEditAllow("FreightCarriers");
                return View(vc.FreightCarriers());
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing specific FreightCarrier
        /// </summary>
        /// <param name="FCId"></param>
        /// <returns></returns>
        public ActionResult FreightCarrier(int FCId)
        {
            try
            {
                ProductMaintenancePresent vc = new ProductMaintenancePresent();
                ViewBag.Message = TempData["Message"];

                return View("~/Views/ProductMaintenance/AddEditFreightCarrier.cshtml", vc.GetFreightCarriersInfo(FCId));
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for Add Edit Freight Carrier
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditFreightCarrier(tblFreightCarrier model)
        {
            ProductMaintenancePresent vc = new ProductMaintenancePresent();
            var data = vc.AddUpdateFreightCarriers(model, adminSetting.UserID);
            TempData["Message"] = data.GetType().GetProperty("msg").GetValue(data).ToString();
            return RedirectToAction("FreightCarrier", new { @FCId = model.FreightCarrierID });
        }

        #endregion


        //Manufacturer Parts Start
        #region   Manufacturer Parts

        /// <summary>
        /// this action is responsible for providing manufacturer parts
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult ManufacturerParts(ManufacturerPartsModel data)
        {
            try
            {
                var companyService = new CompanyPresenter();
                if (data != null && data.Posted)
                {
                    Session["postedData"] = data;
                }
                if (Session["postedData"] != null)
                {
                    data.ManfPartList = pmService.GetManufPartList((ManufacturerPartsModel)Session["postedData"]);
                    data = (ManufacturerPartsModel)Session["postedData"];
                }

                data.ManfList = companyService.GetManufacturersList("").OrderBy(x => x.Value).ToList();
                data.CategoryList = pmService.GetActiveCategoryList();

                return View("ManufacturerPartsListView", data);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ManufacturerPartsAddEdit(int partId)
        {
            try
            {
                var companyService = new CompanyPresenter();
                var countryService = new CountryPresenter();
                var model = new ManufacturerPartsModel();
                model.ManfList = companyService.GetManufacturersList("").OrderBy(x => x.Value).ToList();
                model.CategoryList = pmService.GetActiveCategoryList();

                if (partId > 0)
                {
                    model.ManfPart = pmService.GetManfPartById(partId);
                    model.LastUpdateByUser = countryService.GetUserNameByUserId(model.ManfPart.LastUpdatedByUserID);
                }
                else
                {
                    model.SupplierList = pmService.GetSupplierList();
                    model.UnSpsList = pmService.GetUnsPsCodesList();
                    model.PriceCodeList = pmService.GetPriceCodeList();
                    model.AvailabilityList = pmService.GetAvailabilityList();
                    model.ManfPart = new tblManufacturerPart();
                }

                return View("ManufacturerPartsAddEditView", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult AddUpdateManfPart(tblManufacturerPart data)
        {
            try
            {

                if (data.ManufacturerPartID > 0)
                {
                    var chkMsg = pmService.UpdateManfPart(data, adminSetting.UserID);
                    TempData["editMsg"] = chkMsg.ToString();
                }
                else
                {
                    var chkMsg = pmService.AddManfPart(data, adminSetting.UserID);
                    if (chkMsg > 0)
                    {
                        data.ManufacturerPartID = chkMsg;
                        TempData["addMsg"] = "True";
                    }
                    else
                    {
                        TempData["addMsg"] = "False";
                    }

                }

                return RedirectToAction("ManufacturerPartsAddEdit", new { partId = data.ManufacturerPartID });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult ChkDuplicateManfPartBySku(string sku, int manfId, int manfPartId)
        {
            var chk = pmService.CheckManfPartBySku(sku, manfId, manfPartId);
            return Json(chk, JsonRequestBehavior.AllowGet);
        }


        #endregion
        //Manufacturer Parts End
    }
}
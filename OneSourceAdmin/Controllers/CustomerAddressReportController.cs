﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html;

namespace OneSourceAdmin.Controllers
{
    public class CustomerAddressReportController : BaseController
    {
        private CustomerPresenter cust;
        private CustomerAddressReportPresenter custAddressReport;
        private BudgetPresenter budget;

        public CustomerAddressReportController()
        {
            custAddressReport = new CustomerAddressReportPresenter();
            cust = new CustomerPresenter();
            budget = new BudgetPresenter();
        }

        // GET: CustomerAddressReport
        public ActionResult Index()
        {
            var model = new CustomerAddressReportModel();

            Session["CustomerAddressData"] = null;

            model.ListCustomers = cust.GetCustomersWithPriority();
            model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

            model.CustomerId = 0;
            model.CountryId = 0;
            model.AddressTypeId = 0;

            return View(model);
        }

        public ActionResult AddressSearchRequest(CustomerAddressReportModel model)
        {
            var buttonName = Request.Form["submit"];
            var tempModel = (CustomerAddressReportModel)Session["CustomerAddressData"];

            if (buttonName == "Generate")
            {

                if (model.CustomerId != 0)
                {
                    if (Session["CustomerAddressData"] != null)
                    {
                        if (tempModel.CustomerId != model.CustomerId || tempModel.CountryId != model.CountryId || tempModel.AddressTypeId != model.AddressTypeId)
                        {
                            tempModel.ListOutputData = custAddressReport.getData(model.CustomerId, model.CountryId, model.AddressTypeId);
                            tempModel.CustomerId = model.CustomerId;
                            tempModel.CountryId = model.CountryId;
                            tempModel.AddressTypeId = model.AddressTypeId;
                        }
                        model = tempModel;
                        Session["CustomerAddressData"] = tempModel;
                    }
                    else
                    {
                        Session["CustomerAddressData"] = model;
                        model.ListOutputData = custAddressReport.getData(model.CustomerId, model.CountryId, model.AddressTypeId);
                        model.ListCustomers = cust.GetCustomersWithPriority();
                        model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    }
                }
                else
                {
                    ViewBag.Message = "NoCustomerError";
                    model.ListCustomers = cust.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    Session["CustomerAddressData"] = null;
                }
            }
            else if (buttonName == "Reset")
            {
                return RedirectToAction("Index");
            }
            else
            {
                if (tempModel != null)
                {
                    model = tempModel;
                    Session["CustomerAddressData"] = tempModel;
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }

            return View("~/Views/CustomerAddressReport/Index.cshtml", model);
        }

        public ActionResult GetExcelFileDataTable()
        {
            var model = new CustomerAddressReportModel();
            var tempModel = (CustomerAddressReportModel)Session["CustomerAddressData"];

            //var listOutput = custAddressReport.getData(customerId, countryId, addressTypeId);
            if (tempModel != null && tempModel.ListOutputData.Count > 0)
            {
                var listOutput = tempModel.ListOutputData;

                var dt = ToDataTable(listOutput);


                GridView gridview = new GridView();
                gridview.DataSource = dt;
                gridview.DataBind();

                for (int i = 0; i < gridview.Rows.Count; i++)
                {
                    GridViewRow row = gridview.Rows[i];
                    gridview_RowDataBound(gridview, new GridViewRowEventArgs(row));
                    foreach (TableCell cell in gridview.Columns)
                    {
                        cell.Attributes.CssStyle["text-align"] = "center";
                    }
                }

                // Clear all the content from the current response
                Response.ClearContent();
                Response.Buffer = true;
                // set the header
                Response.AddHeader("content-disposition", "attachment;filename=CustomerAttendanceReport.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        gridview.RenderControl(htw);
                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            else
            {
                ViewBag.Message = "NoData";
                model.ListCustomers = cust.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
            }

            return View("Index", model);
        }

        public ActionResult ExportDataTableToPdf()
        {
            try
            {
                var tempModel = (CustomerAddressReportModel)Session["CustomerAddressData"];

                //var listOutput = custAddressReport.getData(customerId, countryId, addressTypeId);
                if (tempModel != null && tempModel.ListOutputData.Count > 0)
                {
                    var listOutput = tempModel.ListOutputData;
                    var dt = ToDataTable(listOutput);



                    iTextSharp.text.Image logoImg =
                    iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                    logoImg.ScaleAbsolute(250, 45);

                    var isPreffered = dt.Columns["Standard Uplift IsPreffered"];
                    var index = -1;
                    if (isPreffered != null)
                    {
                        index = dt.Columns["Standard Uplift IsPreffered"].Ordinal;
                    }

                    PdfPTable pdfTable = FillDataTableToPdf(dt);

                    List<string> columnNames = new List<string>();
                    List<float> columnsWitdh = new List<float>();
                    foreach (DataColumn column in dt.Columns)
                    {
                        columnNames.Add(column.ColumnName);
                        columnsWitdh.Add(15f);
                    }

                    using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                    {
                        Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                        PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                        document.Open();
                        document.Add(logoImg);

                        document.Add(new Paragraph("\n"));
                        document.Add(new Paragraph("\n"));
                        document.Add(pdfTable);
                        document.Close();
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();

                        Response.Clear();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", "attachment; filename=CustomerAddressReport.pdf");
                        Response.ContentType = "application/pdf";
                        Response.Buffer = true;
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(bytes);
                        Response.End();
                    }
                    return null;
                }
                else
                {
                    var model = new CustomerAddressReportModel();
                    ViewBag.Message = "NoData";
                    model.ListCustomers = cust.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    return View("Index", model);
                }

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public PdfPTable FillDataTableToPdf(DataTable data)
        {
            var dt = data;
            var columnsCount = dt.Columns.Count;
            BaseColor color = WebColors.GetRGBColor("#ffffff");
            iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD, color);
            iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
            BaseColor backgrounColor = WebColors.GetRGBColor("#3b9ad2");

            PdfPTable table = new PdfPTable(data.Columns.Count);
            PdfPRow row = null;
            float[] widths = new float[data.Columns.Count];

            for (int i = 0; i < data.Columns.Count; i++)
            {
                widths[i] = 15f;
            }

            table.SetWidths(widths);

            table.WidthPercentage = 100;
            int iCol = 0;
            string colname = "";

            int[] rowItems = new int[columnsCount];

            for (int i = 0; i < columnsCount; i++)
            {
                rowItems[i] = i;
            }

            foreach (DataColumn c in dt.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(c.ColumnName, fontLabel));
                cell.BackgroundColor = backgrounColor;
                table.AddCell(cell);
            }

            foreach (DataRow r in dt.Rows)
            {
                for (int i = 0; i < rowItems.Count(); i++)
                {
                    if (dt.Rows.Count > 0)
                    {
                        table.AddCell(new Phrase(r[i].ToString(), fontNormal));
                    }
                }
            }

            return table;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// This table converts a list to datatable
        /// </summary>
        /// <param name="T">generic parameter</param>
        /// <returns>Datatable</returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void gridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
            }
        }

        public JsonResult GetCountryByCustomerId(int customerId, int selectedId)
        {
            try
            {
                var countriesList = budget.GetCountryByCustomer(customerId);
                var countries = countriesList.Select(x => new
                {
                    Value = x.CountryID.ToString(),
                    Text = x.CountryName,
                    SelectedId = !countriesList.Select(s => s.CountryID).Contains(selectedId) ? 0 : selectedId
                }).ToList();

                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }



    }
}
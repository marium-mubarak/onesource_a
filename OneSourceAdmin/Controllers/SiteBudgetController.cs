﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Sep 23 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Site Budget detail"
    /// </summary>
    public class SiteBudgetController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of Site Budget
        /// </summary>
        /// <returns></returns>
       
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    CurrencyPresenter cup = new CurrencyPresenter();
                    BudgetModel model = new BudgetModel
                    {
                        Customers = cp.GetCustomers(),
                        Currencies = cup.GetCurrencies()
                    };
                    @ViewBag.AddEditAccess = isAddEditAllow("CustomerSiteBudget");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for returning Json result of countries by customer
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public JsonResult GetCountryByCustomer(int value)
        {
            if (value == 0) return Json(new List<GenericList>(), JsonRequestBehavior.AllowGet);
            BudgetPresenter bp = new BudgetPresenter();
            var record = bp.GetCountryByCustomer(value);
            return Json(record, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for returning Json result of Total Amount
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetTotalAmountByCountryCustomer(int countryId, int customerId)
        {
            if (countryId == 0) return Json(new List<string>(), JsonRequestBehavior.AllowGet);
            BudgetPresenter bp = new BudgetPresenter();
            var amount = bp.GetTotalAmountByCountryCustomer(countryId, customerId);
            var currencyId = bp.GetCurrencyByCountryCustomer(countryId, customerId);            
            var listOfAddresses = bp.GetAddressByCustomerCountry(countryId, customerId);
            var data = new {
                amount,
                currencyId,
                listOfAddresses
            };
          
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for returning Json result of Updated Site Budget.        
        /// </summary>
        /// <param name="customerBudget"></param>
        /// <returns></returns>
        public JsonResult UpdateCustomerSiteBudget(List<OneSourceAdminCore.Entity.tblCustomerSiteBudget> customerBudget)
        {
            if (customerBudget == null) return Json("0", JsonRequestBehavior.AllowGet);
            BudgetPresenter bp = new BudgetPresenter();
            int a;
            if (customerBudget[0].SiteID == 0 && customerBudget[0].CurrencyID==0 && customerBudget[0].CountryID==0)
            {
                a = bp.DeleteCustomerSiteBudget(customerBudget[0], Convert.ToInt32(Session["UserID"]));
                return Json(a, JsonRequestBehavior.AllowGet);
            }
            a = bp.UpdateCustomerSiteBudget(customerBudget, Convert.ToInt32(Session["UserID"]));
           return Json(a, JsonRequestBehavior.AllowGet);
           
        }

    }
}
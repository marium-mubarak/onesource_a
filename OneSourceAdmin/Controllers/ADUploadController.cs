﻿using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using System.IO;
using System.Data.OleDb;

namespace OneSourceAdmin.Controllers
{
    public class ADUploadController : BaseController
    {
        private ADUploadPresenter adUploadService;
        private BudgetPresenter budget;
        private CustomerPresenter customerService;

        public ADUploadController()
        {
            adUploadService = new ADUploadPresenter();
            budget = new BudgetPresenter();
            customerService = new CustomerPresenter();
        }

        // GET: ADUpload
        public ActionResult AssetDisposition()
        {
            try
            {
                var model = new ADUploadModel();
                model.listADUpload = adUploadService.GetADFileInfo();
                return View("AssetDisposition", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpGet]
        public ActionResult FileDetailView(int id)
        {
            try
            {
                var model = new ADUploadModel();
                model.ListFileInfo = adUploadService.GetFileDataByFileID(id);
                return View("FileDetailView", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpGet]
        public ActionResult ViewAuditFile()
        {
            try
            {
                var model = new ADUploadModel();
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                return View("ViewAuditFile", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult ViewAuditFile(ADUploadModel model)
        {
            try
            {
                object data;
                var list = adUploadService.GetADFileData(model.SupplierId, Convert.ToInt32(model.CustomerId), model.TicketId);

                if (list.Count > 0)
                {
                    var dt = ToDataTable<ADFileDetailInfo>(list);
                    data = JsonConvert.SerializeObject(dt);
                }
                else
                {
                    data = null;
                    ViewBag.Message = "No records found";
                }
                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        [HttpGet]
        public ActionResult UploadAD()
        {
            try
            {
                Session["Model"] = null;
                var model = new ADUploadModel();
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                model.ADPickUpDate = DateTime.Now.ToString("MM/dd/yyyy");

                return View("UploadAD", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult UploadAD(ADUploadModel model)
        {
            try
            {
                int resInt = 0, rowNo = 0;
                double resDbl = 0;
                bool match;

                List<string> UnitIDs = new List<string>();
                DataSet dsData = new DataSet();
                DataSet dsError = new DataSet();
                DataTable dtErr = new DataTable();
                OleDbDataAdapter dp;

                //Add Columns in Error dataset
                dtErr.Columns.Add("RowNo", typeof(int));
                dtErr.Columns.Add("Column", typeof(string));
                dtErr.Columns.Add("Value", typeof(string));
                dtErr.Columns.Add("ErrDesc", typeof(string));
                TempData["Message"] = "";

                var fileType = Request.Form["fileType"];
                var file = Request.Files[0];
                var fileExtenstion = Path.GetExtension(file.FileName);
                var folderPath = Server.MapPath("~/uploads/AD_Upload");
                int fileVer = adUploadService.getFileVersion(model.SupplierId, Convert.ToInt32(model.CustomerId));


                bool allOk = true;

                var fileName = model.SupplierName + " - " + model.CustomerName + " Ver" + fileVer + fileExtenstion;
                var path = Path.Combine(folderPath, fileName);

                //Check if the path exists or not
                if (!Directory.Exists(folderPath))
                    System.IO.Directory.CreateDirectory(folderPath);

                file.SaveAs(path);

                string conStr = String.Empty;
                string cmd = string.Empty;

                if (fileExtenstion == "xls")
                {
                    //Then make connection string for xls
                    conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + "; Extended Properties=Excel 8.0";
                }
                else
                {
                    //Else make connection string for xlsx
                    conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + "; Extended Properties=Excel 8.0";
                }

                cmd = "Select * from [Sheet1$]";
                dp = new OleDbDataAdapter(cmd, conStr);

                //Fill the dataset with excel data
                dp.Fill(dsData);

                var count = dsData.Tables[0].Rows.Count;

                if (dsData.Tables[0].Rows.Count == 0)
                {
                    //Then set error label text
                    TempData["Message"] = "Uploaded file is empty, please enter data and upload again.";
                }


                //Loop to iterate through records in dataset
                foreach (DataRow row in dsData.Tables[0].Rows)
                {
                    match = false;

                    //Make increment in row number
                    rowNo++;

                    //Check if it is second row
                    if (rowNo == 2)
                    {
                        if (dsData.Tables[0].Rows[rowNo - 2]["Unit_ID"].ToString().Trim().Length > 1)
                        {
                            UnitIDs.Add(dsData.Tables[0].Rows[rowNo - 2]["Unit_ID"].ToString().Trim());
                        }
                    }

                    //Get current row's UnitID into variable
                    string chk = row["Unit_ID"].ToString().Trim();

                    //Check if UnitID is not valid
                    if (int.TryParse(row["Unit_ID"].ToString().Trim(), out resInt) == false)
                    {
                        //Check if UnitID is not provided
                        if (row["Unit_ID"].ToString().Trim().Length < 1)
                        {
                            //Add row in error table with appropriate message
                            dtErr.Rows.Add(rowNo, "Unit_ID", row["Unit_ID"].ToString().Trim(), "Please provide UnitID");
                        }
                        else
                        {
                            //Add row in error table with appropriate message
                            dtErr.Rows.Add(rowNo, "Unit_ID", row["Unit_ID"].ToString().Trim(), "Please enter an integer value for UnitID");
                        }
                    }

                    //Iterate through the UnitIDs list
                    foreach (string ID in UnitIDs)
                    {
                        //Check if UnitID matches with current row's UnitID
                        if (ID == row["Unit_ID"].ToString().Trim())
                        {
                            //Add error description row error data table
                            dtErr.Rows.Add(rowNo, "Unit_ID", row["Unit_ID"].ToString().Trim(), "This UnitID already exists in file");

                            //Make match flag true
                            match = true;

                            //Break the loop because it is duplicate ID
                            break;
                        }
                    }

                    if (!match && rowNo > 1)
                    {
                        UnitIDs.Add(row["Unit_ID"].ToString().Trim());
                    }

                    //Check if category is empty
                    if (row["Category"].ToString().Trim().Length < 1)
                    {
                        //Add error description row error data table
                        dtErr.Rows.Add(rowNo, "Category", row["Category"].ToString().Trim(), "Please provide category");
                    }

                    //Check if status is neither Recycled nor Remarketable
                    if (string.Compare(row["Status"].ToString().Trim(), "Recycled", true) != 0 && string.Compare(row["Status"].ToString().Trim(), "Remarketable", true) != 0)
                    {
                        //Check if status is empty
                        if (row["Status"].ToString().Trim().Length < 1)
                        {
                            //Add empty status error message
                            dtErr.Rows.Add(rowNo, "Status", row["Status"].ToString().Trim(), "Please provide Status");
                        }
                        else
                        {
                            //Add invalid status error message
                            dtErr.Rows.Add(rowNo, "Status", row["Status"].ToString().Trim(), "Status can either be Recycled or Remarketable");
                        }
                    }

                    //Check if processing fees is not valid
                    if (int.TryParse(row["Processing_Fees"].ToString().Trim(), out resInt) == false && double.TryParse(row["Processing_Fees"].ToString().Trim(), out resDbl) == false)
                    {
                        //Check if processing fees is not provided
                        if (row["Processing_Fees"].ToString().Trim().Length < 1)
                        {
                            //Add row in error table with appropriate message
                            dtErr.Rows.Add(rowNo, "Processing_Fees", row["Processing_Fees"].ToString().Trim(), "Please provide Processing Fees");
                        }
                        else
                        {
                            //Add row in error table with appropriate message
                            dtErr.Rows.Add(rowNo, "Processing_Fees", row["Processing_Fees"].ToString().Trim(), "Please enter a valid value for Processing Fees");
                        }
                    }
                    //Else check if processing fees is zero
                    else if (row["Processing_Fees"].ToString().Trim() == "0")
                    {
                        //Add row in error table with appropriate message
                        dtErr.Rows.Add(rowNo, "Processing_Fees", row["Processing_Fees"].ToString().Trim(), "Processing Fees should be greater than zero");
                    }

                    //Check if AR value is not valid
                    if (int.TryParse(row["AR_Value"].ToString().Trim(), out resInt) == false && double.TryParse(row["AR_Value"].ToString().Trim(), out resDbl) == false)
                    {
                        //Check if AR value is not provided
                        if (row["AR_Value"].ToString().Trim().Length < 1)
                        {
                            //Add row in error table with appropriate message
                            dtErr.Rows.Add(rowNo, "AR_Value", row["AR_Value"].ToString().Trim(), "Please provide AR value");
                        }
                        else
                        {
                            //Add row in error table with appropriate message
                            dtErr.Rows.Add(rowNo, "AR_Value", row["AR_Value"].ToString().Trim(), "Please enter a valid value for AR Value");
                        }
                    }
                }

                dsError.Tables.Add(dtErr);

                if (dsError.Tables[0].Rows.Count > 0)
                {
                    model.UploadDataTable = null;
                    model.ErrorDataTable = dsError.Tables[0];
                }
                else
                {
                    model.UploadDataTable = dsData.Tables[0];
                    model.ErrorDataTable = null;
                }

                model.VersionNumber = fileVer;
                model.FileName = fileName;

                //Add current model to session
                Session.Add("Model", model);

                return View("UploadAD", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ProceedUpload()
        {
            try
            {
                var model = new ADUploadModel();
                model = (ADUploadModel)Session["Model"];

                var supplierUnmappedCategories = adUploadService.MapCategoriesAndGetUnmappedCategories(model.UploadDataTable);

                if (supplierUnmappedCategories.Count > 0)
                {
                    var unmappedCategories = supplierUnmappedCategories.Select(x => new GenericStringList
                    {
                        Key = x.ToString(),
                        Value = x.ToString()
                    }).ToList();

                    model.UnmappedCategoiriesList = unmappedCategories;
                    model.OSCategoriesList = adUploadService.GetOSCategories();
                    return View("UploadAD", model);
                }
                else
                {
                    bool res = adUploadService.SaveFileDataToDb(model, Convert.ToInt32(Session["UserID"]));
                    if (res == true)
                    {
                        Session["Model"] = null;
                        TempData["Message"] = "Records uploaded successfully";
                    }
                    else
                    {
                        TempData["Message"] = "Some error occured";
                    }
                }

                return RedirectToAction("UploadAD");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult AddCategories(FormCollection formCollection)
        {
            try
            {
                int OSCategoryId = Convert.ToInt32(formCollection["CategoryId"]);
                string SupplierCategory = formCollection["supplierMultipleCategories"];


                if (adUploadService.AddCategory(OSCategoryId, SupplierCategory))
                {
                    TempData["Message"] = "Added Successfully";
                }
                else
                {
                    TempData["Message"] = "Some error occured";
                }

                return RedirectToAction("ProceedUpload");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult GetSuppliersByCustomerID(int customerId)
        {
            try
            {
                var suppliers = adUploadService.GetSuppliersByCustomerID(customerId);
                suppliers.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                var suppliersList = suppliers.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();

                return Json(suppliersList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTicketNumbers(int customerId, int supplierId)
        {
            try
            {
                var tickets = adUploadService.GetTicketNumbers(customerId, supplierId);
                tickets.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                var ticketsList = tickets.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();

                return Json(ticketsList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTicketNumbersForUpload(int customerId, int supplierId)
        {
            try
            {
                var tickets = adUploadService.GetTicketNumbersForUpload(customerId, supplierId);
                tickets.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                var ticketsList = tickets.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();

                return Json(ticketsList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOperatingCompanies(int customerId)
        {
            try
            {
                var companies = adUploadService.GetOperatingCompanies(customerId);
                companies.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                var companiesList = companies.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();

                return Json(companiesList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetBusinessSegment(int customerId)
        {
            try
            {
                var segment = adUploadService.GetBusinessSegment(customerId);
                segment.Insert(0, new GenericStringList { Key = "0", Value = "--Select--" });

                var segmentsList = segment.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();

                return Json(segmentsList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}
﻿using System;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>   
    /// Date created : Feb 10 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for Adding, Editing, Listing of Region"
    /// </summary>
    public class RegionController : BaseController
    {
        ///<summary>
        /// This Action is responsible For providing View Of Region (Setting>>Region)
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>

        public ActionResult Index(int defaultRegionId = 0, int customerId = 0)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    RegionPresenter rp = new RegionPresenter();
                    CountryPresenter cp = new CountryPresenter();

                    //sunny gohar on 9th Jan 2018
                    CustomerPresenter cust = new CustomerPresenter();

                    RegionModel model = new RegionModel();
                    model.LstRegion = rp.GetRegions();
                    model.LstCountries = cp.GetCountries();
                    //sunny gohar on 9th Jan 2018
                    model.ListCustomers = cust.GetCustomersWithPriority();

                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("RegionSettings");

                    //sunny gohar on 9th Jan 2018 ==========================================

                    //if (defaultRegionId == 0)
                    //{
                    //    if (model.LstRegion != null && model.LstRegion.Count > 0)
                    //    {
                    //        model.LstRegionWiseCountry = rp.GetRegionWiseCountry(Convert.ToInt32(model.LstRegion[0].Key));
                    //        model.RegionId = Convert.ToInt32(model.LstRegion[0].Key);
                    //        model.RegionName = model.LstRegion[0].Value;
                    //    }
                    //}
                    //else
                    //{
                    //    model.LstRegionWiseCountry = rp.GetRegionWiseCountry(defaultRegionId);
                    //    model.RegionId = defaultRegionId;
                    //    var firstOrDefault = model.LstRegion.FirstOrDefault(x => x.Key == defaultRegionId);
                    //    if (firstOrDefault != null)
                    //        model.RegionName = firstOrDefault.Value;
                    //}
                    if (defaultRegionId != 0 && customerId != 0)
                    {
                        model.LstRegionWiseCountry = rp.GetRegionAndCustomerWiseCountry(defaultRegionId, customerId);
                        model.RegionId = defaultRegionId;
                        model.CustomerId = customerId;
                        var firstOrDefault = model.LstRegion.FirstOrDefault(x => x.Key == defaultRegionId);
                        var customer = model.ListCustomers.FirstOrDefault(x => x.CustomerId == customerId);
                        if (firstOrDefault != null)
                            model.RegionName = firstOrDefault.Value;
                        if (customer != null)
                            model.CustomerName = customer.CustomerName;
                    }

                    if (model.LstRegionWiseCountry != null)
                    {
                        if (model.LstRegionWiseCountry.Count <= 0)
                        {
                            ViewBag.Message = "No country mappings available!";
                        }
                    }
                    else
                    {
                        ViewBag.Message = "No country mappings available!";
                    }
                    if (TempData["Message"] != null)
                    {
                        ViewBag.Message = TempData["Message"].ToString();
                    }
                    model.RegionId = defaultRegionId;
                    model.CustomerId = customerId;
                    //=========================================================

                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Adding mapping of country and region
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult AddRegionCountryMap(tblCountryRegionMapping mapping)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    RegionPresenter rp = new RegionPresenter();
                    ParentCountryRegionMapping parent = rp.AddRegionCountryMapping(mapping, adminSetting.UserID);

                    //sunny gohar 9th Jan 2018
                    if (parent.RegionWiseCountry != null) TempData["Message"] = parent.RegionWiseCountry.CountryName + " Already Mapped on " + parent.RegionWiseCountry.RegionName;
                    if (parent.Count > 0) TempData["Message"] = "Region Country mapping has been added";
                    return RedirectToAction("Index", new { @defaultRegionId = mapping.RegionId, @customerId = mapping.CustomerID });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Deleting selected Country Region Mapping 
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult DeleteRegionCountryMap(string countryRegionMappingId, string currentRegionId, string currentCustomerId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int countryRegionMapping;
                    if (!int.TryParse(countryRegionMappingId, out countryRegionMapping))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    RegionPresenter cp = new RegionPresenter();
                    int count = cp.DeleteRegionCountryMapping(countryRegionMapping,adminSetting.UserID);
                    if (count > 0) TempData["Message"] = "Country Region mapping has been deleted";
                    return RedirectToAction("Index", new { @defaultRegionId = currentRegionId, @customerId = currentCustomerId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public ActionResult AddRegion(RegionModel region)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    RegionPresenter rp = new RegionPresenter();
                    TempData["Message"] = rp.AddRegion(region,adminSetting.UserID);

                    return RedirectToAction("Index", new { @defaultRegionId = region.DefaultRegionId, @customerId = region.CustomerId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}
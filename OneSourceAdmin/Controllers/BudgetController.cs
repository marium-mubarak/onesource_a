﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing country wise Budget detail"
    /// </summary>
    public class BudgetController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of country Budget
        /// </summary>
        /// <returns></returns>


        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    CustomerPresenter cp = new CustomerPresenter();
                    CurrencyPresenter cup = new CurrencyPresenter();
                    BudgetModel model = new BudgetModel
                    {
                        Customers = cp.GetCustomers(),
                        Currencies = cup.GetCurrencies()
                    };

                    @ViewBag.AddEditAccess = isAddEditAllow("CustomerCountryBudget");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action return json value of Customer Budget Record
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public JsonResult GetCustomerBudgetRecord(int value)
        {
            if (value == 0) return Json(new List<sp_GetCustomerBudgetRecord_R_Result>(), JsonRequestBehavior.AllowGet);
            BudgetPresenter bp = new BudgetPresenter();
            CurrencyPresenter cup = new CurrencyPresenter();
            var record = bp.GetCustomerBudgetRecord(value).GroupBy(x => x.CountryName, (key, g) => new { country = key, Currencies = g.ToList() }).ToList();

            var totalbudgetData = bp.GetCustomerCurrencyAmountByCountry(value);

            var data = new
            {
                record,
                totalbudgetData,
                currencies = cup.GetCurrencies()
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action Update Customer Budget
        /// </summary>
        /// <param name="customerBudget"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateCustomerBudget(List<OneSourceAdminCore.Entity.tblCustomerCountryBudget> customerBudget)
        {
            if (customerBudget == null) return Json("0", JsonRequestBehavior.AllowGet);
            BudgetPresenter bp = new BudgetPresenter();
            int a = bp.UpdateCustomerBudget(customerBudget, Convert.ToInt32(Session["UserID"]));
            return Json(a, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for given Total Amount by parameter
        /// </summary>
        /// <param name="currencyId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetTotalAmountByCurrencyCustomer(int currencyId, int customerId)
        {
            if (currencyId == 0) return Json(new List<string>(), JsonRequestBehavior.AllowGet);
            BudgetPresenter bp = new BudgetPresenter();
            var data = bp.GetTotalAmountByCurrencyCustomer(currencyId, customerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
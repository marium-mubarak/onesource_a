﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : July 15 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Case Study Attachments detail"
    /// </summary>
    /// <returns></returns>
    public class CaseStudyAttachementController : BaseController
    {

        /// <summary>
        /// This Action is responsible for returning Index View of Case Study Attachment
        /// </summary>
        /// <returns></returns>
        
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CaseStudyAttachementPresenter csp = new CaseStudyAttachementPresenter();
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("AttachmentsSettings");
                    return View(csp.GetAttachements());
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// This Action is responsible for Delete the case study attachment 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteAttachement(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int caseStudyId;
                    if (!int.TryParse(id, out caseStudyId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    CaseStudyAttachementPresenter cp = new CaseStudyAttachementPresenter();
                    int count = cp.DeleteCaseStudyAttachement(caseStudyId);
                    if (count > 0) TempData["Message"] = "Attachment has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This Action is responsible for adding the case study attachment 
        /// </summary>
        /// <param name="attachFile"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddCaseStudyAttachement(HttpPostedFileBase attachFile, tblCaseStudyAttachement model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    if (attachFile != null && attachFile.ContentLength > 0)
                    {
                         
                        var fileName = Path.GetFileName(attachFile.FileName).Replace("^","");
                        var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                        string path = string.Empty;
                        if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".msg" || fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".png")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                //path =
                                //    Path.Combine(ConfigurationManager.AppSettings["UploadURLAttachement"] + "/",
                                //        docId + "^" + fileName);
                                path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURLAttachement"] + "/"), docId + "^" + fileName);
                             
                                //using (var newUserContext = new Impersonator(ConfigurationManager.AppSettings["UploaderUserName"], ConfigurationManager.AppSettings["UploadDomain"],ConfigurationManager.AppSettings["UploaderPassword"] ))
                                //{
                                attachFile.SaveAs(path);
                                //}
                                model.FileName = docId + "^" + fileName;
                                model.FilePath = path;

                                CaseStudyAttachementPresenter csp = new CaseStudyAttachementPresenter();

                                int count = csp.AddCaseStudyAttachement(model, Convert.ToInt32(Session["UserID"]));
                                if (count > 0) TempData["Message"] = "Attachment has been added";
                                return RedirectToAction("Index");
                                
                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                                return RedirectToAction("Index");
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Please provide file";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This method is responsible for checking case study attachment data comming from request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ValidateFields(tblCaseStudyAttachement model)
        {
            if (string.IsNullOrEmpty(model.Title))
            {
                TempData["Message"] = "Please Provide Title";
                return false;
            }
          
            return true;
        }

        /// <summary>
        /// This Action is responsible for Downloading the case study Attachement
        /// </summary>
        /// <param name="csaId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownloadAttachement(string csaId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(csaId))
                    {
                        int cSAId;
                        if (int.TryParse(csaId, out cSAId))
                        {
                            CaseStudyAttachementPresenter csp = new CaseStudyAttachementPresenter();
                            string filepath = csp.GetFilePathById(cSAId);
                            string contentType = "application/octet-stream";

                            if (!System.IO.File.Exists(filepath))
                            {
                                return View("~/Views/Shared/PageNotFound.cshtml");
                            }
                            return File(filepath, contentType, Path.GetFileName(filepath));
                        }
                        return RedirectToAction("Index");
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            return RedirectToAction("Login", "Account");
        }
    }
}
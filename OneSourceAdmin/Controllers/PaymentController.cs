﻿using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using System.IO;
using System.Web.Routing;

namespace OneSourceAdmin.Controllers
{
    public class PaymentController : BaseController
    {
        PaymentPresenter paymentService;
        CurrencyPresenter currencyService;
        public PaymentController()
        {
            paymentService = new PaymentPresenter();
            currencyService = new CurrencyPresenter();
        }
        
        public ActionResult PaymentSearch()
        {
            try
            {
                Session["SearchResult"] = null;
                PaymentModel model = new PaymentModel();
                model.PaidByCompanyList = paymentService.GetPaidByCompaniesList();
                model.PaidToCompanyList = paymentService.GetPaidToCompaniesList();
                model.PaymentMethod = paymentService.GetPaymentMethodList();
                model.PaymentTxType = paymentService.GetTxTypeList();
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpGet]
        public ActionResult NewPayment()
        {
            try
            {
                PaymentModel model = new PaymentModel();
                model.newPaymentModel = new tblPayment();
                model.PaidByCompanyList = paymentService.GetPaidByCompaniesList();
                model.PaidByCompanyList.RemoveAt(0);
                model.PaidToCompanyList = paymentService.GetPaidToCompaniesList();
                model.PaidToCompanyList.RemoveAt(0);
                model.PaymentMethod = paymentService.GetPaymentMethodList();
                model.PaymentMethod.RemoveAt(0);
                model.PaymentTxType = paymentService.GetTxTypeList();
                model.PaymentTxType.RemoveAt(0);
                model.CurrencyList = currencyService.GetCurrencies();
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult NewPayment(PaymentModel model)
        {
            try
            {
                model.newPaymentModel.IsActive = true;
                model.newPaymentModel.LastUpdate = DateTime.Now;
                model.newPaymentModel.LastUpdateUserID = Convert.ToInt32(Session["UserID"]);

                //Add new payment here
                var paymentId = paymentService.NewPayment(model.newPaymentModel);
                return RedirectToAction("PaymentDetail", new { id = paymentId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult Search(PaymentModel model)
        {
            try
            {
                model.ParentCompanyId = Convert.ToInt32(Session["ParentCompanyID"]);
                model.SearchRecords = paymentService.SearchPayments(model);
                Session["SearchResult"] = model.SearchRecords;
                return RedirectToAction("SearchResult");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult SearchResult()
        {
            try
            {
                if (Session["SearchResult"] != null)
                {
                    PaymentModel model = new PaymentModel();
                    model.SearchRecords = (List<sp_SearchPayment_AdminR_Result>)Session["SearchResult"];
                    return View("SearchList", model);
                }
                else
                {
                    return RedirectToAction("PaymentSearch");
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult PaymentDetail(string id)
        {
            try
            {
                if (id != null)
                {
                    var model = new PaymentModel();
                    model.ParentCompanyId = Convert.ToInt32(Session["ParentCompanyID"]);
                    model.PaymentId = id;
                    model.SelectedRecord = paymentService.SearchPayments(model).FirstOrDefault();
                    if (model.SelectedRecord != null)
                    {
                        //Get invoices here
                        var a = paymentService.GetCurrentInvoices(Convert.ToInt32(model.SelectedRecord.Payment_), Convert.ToInt32(model.SelectedRecord.CurrencyId), (int)model.SelectedRecord.Paid_By_Id, (int)model.SelectedRecord.Paid_To_Id, model.SelectedRecord.TxType);
                        model.AllInvoices = a.AllInvoices;
                        model.NewInvoices = a.NewInvoices;
                        model.PaymentTotal = a.PaymentTotal;
                        model.SelectedInvoices = a.SelectedInvoices;

                        return View(model);
                    }
                    else
                    {
                        return RedirectToAction("SearchResult");
                    }
                }
                else
                {
                    return RedirectToAction("SearchResult");
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public ActionResult UpdateComment(PaymentModel model)
        {
            try
            {
                model.SelectedRecord.Last_Updated = DateTime.Now.ToString();
                model.SelectedRecord.Updated_By = Session["UserID"].ToString();

                if (paymentService.UpdateComment(model.SelectedRecord))
                {

                }
                else
                {

                }

                return RedirectToAction("PaymentDetail", new RouteValueDictionary(new { action = "PaymentDetail", id = model.SelectedRecord.Payment_ }));
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult AddPayment(FormCollection Fc)
        {
            try
            {
                var invoices = Fc["invoiceid"];
                var amounts = Fc["invoiceamount"];
                var descriptions = Fc["description"];
                var paymentId = Convert.ToInt32(Fc["SelectedRecord.Payment_"]);
                var userId = Convert.ToInt32(Session["UserID"]);

                if (paymentService.AddPayment(invoices, amounts, descriptions, paymentId, userId))
                { }
                else { }

                return RedirectToAction("PaymentDetail", new RouteValueDictionary(new { action = "PaymentDetail", id = paymentId }));
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }



    }
}
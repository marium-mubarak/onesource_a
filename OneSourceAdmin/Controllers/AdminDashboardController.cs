﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using OneSourceAdminCore.Entity;

namespace OneSourceAdmin.Controllers
{
    public class AdminDashboardController : BaseController
    {


        private AdminMenuService menuService;
        public AdminDashboardController()
        {
            menuService = new AdminMenuService();
        }

        // GET: /AdminDashboard/
        [Route("admin-dashboard")]
        public ActionResult Index()
        {
            var model = new AdminMenuAndAccessModel();
            if (adminSetting != null && adminSetting.UserID > 0 && adminSetting.UserGroupID > 0)
            {
                model = menuService.GetMenuByUserGroupIdAndUserId(adminSetting.UserGroupID, adminSetting.UserID);
                CompanyPresenter cp = new CompanyPresenter();
                ViewBag.CompanyRoles = cp.GetRolesList(adminSetting.ParentCompanyID);
            }
            return View("DasboardView", model.AdminMenu);
        }
    }
}
﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Entity;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.OleDb;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html;
using HtmlAgilityPack;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : March 29 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing quote tracking detail"
    /// </summary>
    public class QuoteSearchController : BaseController
    {

        private QuoteSearchPresenter osp;
        
        public QuoteSearchController()
        {
            osp = new QuoteSearchPresenter();
        }

        /// <summary>
        /// this action is responsible for providing view to search quote
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public ActionResult QuoteSearch(int customerId, string quoteType)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    QuoteSearchModel model = osp.GetQuoteSearchModel("ISD", customerId, Session["PrimeContractorID"].ToString(), quoteType, string.Empty);
                    return View("~/Views/QuoteSearch/QuoteSearchISD.cshtml", model);
                }
                if (Session["UserGroupName"].ToString() == "SFA")
                {
                    QuoteSearchModel model = osp.GetQuoteSearchModel("SFA", customerId, string.Empty, quoteType, Session["SFAID"].ToString());
                    return View("~/Views/QuoteSearch/QuoteSearchSFA.cshtml", model);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for search the quote by filters
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult QuoteSearchRequest(QuoteSearchModel model)
        {
            try
            {
                Session["QuoteSearchModel"] = model;
                Session["QuoteType"] = model.quoteType;
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    var data = osp.GetSearchedQuotes(model, Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(), Session["UserGroupName"].ToString(), string.Empty, 500);
                    if (data.LstQuotes.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("QuoteSearch", new { customerId = model.customerid, quoteType = model.quoteType });
                    }
                    Session["SearchQuoteResult"] = data;
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    var data = osp.GetSearchedQuotes(model, Session["DateFormatID"].ToString(), string.Empty, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), 500);
                    if (data.LstQuotes.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("QuoteSearch", new { customerId = model.customerid, quoteType = model.quoteType });
                    }
                    Session["SearchQuoteResult"] = data;
                }
                return RedirectToAction("SearchQuoteList");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is reponsible for managing the list after the search
        /// </summary>
        /// <param name="requiredRecord"></param>
        /// <param name="requiredPageSize"></param>
        /// <param name="QuoteIdToDelFromList"></param>
        /// <returns></returns>
        public ActionResult SearchQuoteList(int requiredRecord, int requiredPageSize, int QuoteIdToDelFromList)
        {
            try
            {
                if (requiredPageSize > requiredRecord)
                {
                    requiredPageSize = 50;
                    requiredRecord = 500;
                }

                ViewBag.PageSize = requiredPageSize;
                ViewBag.RequiredRecord = requiredRecord;

                if (requiredRecord > 5000)
                {
                    requiredRecord = 5000;
                    ViewBag.RequiredRecord = 5000;
                }
                if (requiredPageSize > 1000)
                {
                    ViewBag.PageSize = 1000;
                    requiredPageSize = 1000;
                }

                QuoteListModel model = new QuoteListModel();

                if (Session["SearchQuoteResult"] != null)
                {
                    model = (QuoteListModel)Session["SearchQuoteResult"];

                    if (QuoteIdToDelFromList > 0)
                    {
                        var item = model.LstQuotes.FirstOrDefault(x => x.QuoteId == QuoteIdToDelFromList);
                        model.LstQuotes.Remove(item);
                    }
                    if (model.LstQuotes.Count != requiredRecord)
                    {
                        if (requiredRecord == 50001)
                        {
                            ViewBag.RequiredRecord = 50001;
                            if (Session["UserGroupName"].ToString() == "ISD")
                            {
                                model = osp.GetSearchedQuotes((QuoteSearchModel)Session["QuoteSearchModel"], Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(), Session["UserGroupName"].ToString(), string.Empty, model.TotalRecord);
                            }
                            else if (Session["UserGroupName"].ToString() == "SFA")
                            {
                                model = osp.GetSearchedQuotes((QuoteSearchModel)Session["QuoteSearchModel"], Session["DateFormatID"].ToString(), string.Empty, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), model.TotalRecord);
                            }
                        }
                        else
                        {
                            if (Session["UserGroupName"].ToString() == "ISD")
                            {
                                model = osp.GetSearchedQuotes((QuoteSearchModel)Session["QuoteSearchModel"], Session["DateFormatID"].ToString(), Session["PrimeContractorID"].ToString(), Session["UserGroupName"].ToString(), string.Empty, requiredRecord);
                            }
                            else if (Session["UserGroupName"].ToString() == "SFA")
                            {
                                model = osp.GetSearchedQuotes((QuoteSearchModel)Session["QuoteSearchModel"], Session["DateFormatID"].ToString(), string.Empty, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), requiredRecord);
                            }

                        }
                        Session["SearchQuoteResult"] = model;
                    }

                }

                ViewBag.TotalCount = model.TotalRecord;
                return View("~/Views/QuoteSearch/QuoteList.cshtml", model);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing detail of specific quote 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        public ActionResult QuoteDetail(string quoteId, string quoteType)
        {
            try
            {
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    ViewBag.Message = TempData["Message"];
                    var orderDetailModel = osp.GetQuoteDetail(Convert.ToInt32(quoteId), quoteType, "ISD", 0);
                    //return View("~/Views/QuoteSearch/QuoteDetail.cshtml", orderDetailModel); Commented (SUNNY GOHAR)

                    //Added below (SUNNY GOHAR)
                    if (orderDetailModel.NewQuoteDetail != null || orderDetailModel.OldQuoteDetail != null)
                    {
                        var obj = new BundleSettings
                        {
                            CustomerId = orderDetailModel.CustomerId,
                            CountryId = orderDetailModel.CountryId,
                            CurrencyId = orderDetailModel.CurrencyId
                        };
                        Session["BundleSettings"] = obj;
                        return View("~/Views/QuoteSearch/QuoteDetail.cshtml", orderDetailModel);
                    }
                    else
                    {
                        return RedirectToAction("Index", "AdminDashboard");
                    }
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    ViewBag.Message = TempData["Message"];
                    var orderDetailModel = osp.GetQuoteDetail(Convert.ToInt32(quoteId), quoteType, "SFA", Convert.ToInt32(Session["SFAID"]));
                    //return View("~/Views/QuoteSearch/QuoteDetail.cshtml", orderDetailModel); //Commented (SUNNY GOHAR)
                    //Added below (SUNNY GOHAR)
                    if (orderDetailModel.NewQuoteDetail != null || orderDetailModel.OldQuoteDetail != null)
                    {
                        var obj = new BundleSettings
                        {
                            CustomerId = orderDetailModel.CustomerId,
                            CountryId = orderDetailModel.CountryId,
                            CurrencyId = orderDetailModel.CurrencyId
                        };
                        Session["BundleSettings"] = obj;
                        return View("~/Views/QuoteSearch/QuoteDetail.cshtml", orderDetailModel);
                    }
                    else
                    {
                        return RedirectToAction("Index", "AdminDashboard");
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for assign supplier to specific quote
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="sfaid"></param>
        /// <returns></returns>
        public JsonResult AssignSupplier(int quoteId, int sfaid, string quoteType)
        {
            try
            {
                int count = osp.AssignSupplier(quoteId, sfaid, Convert.ToInt32(Session["UserID"]), quoteType);
                if (count > 0)
                {

                    if (Session["SearchQuoteResult"] != null)
                    {
                        var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                        quotes.LstQuotes.ForEach(x =>
                        {
                            if (x.QuoteId == quoteId)
                            {
                                x.GSP = osp.GetSFAById(sfaid);
                            }
                        });

                    }
                }
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// this action is responsible for changing the quote status
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public JsonResult ChangeQuoteStatus(int quoteId, string status, string quoteType)
        {
            //this action change status for both isd and sfa -> new and old quotes
            try
            {
                int count = osp.ChangeQuoteStatus(quoteId, status, Convert.ToInt32(Session["UserID"]), quoteType);
                if (count > 0)
                {
                    if (Session["SearchQuoteResult"] != null)
                    {
                        var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                        quotes.LstQuotes.ForEach(x =>
                        {
                            if (x.QuoteId == quoteId)
                            {
                                x.Status = status;
                            }
                        });

                    }
                }
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// this action is responsible for updating quote detail ISD
        /// </summary>
        /// <param name="quoteRef"></param>
        /// <param name="supplierRef"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult UpdateQuoteDetail(string quoteRef, string supplierRef, int quoteId, string quoteType)
        {
            try
            {
                if (quoteId > 0)
                {
                    int count = osp.UpdateQuoteDetail(quoteId, quoteRef, supplierRef, Convert.ToInt32(Session["UserID"]), quoteType);
                    if (count > 0)
                    {
                        TempData["Message"] = "Quote detail updated successfully";


                        if (Session["SearchQuoteResult"] != null)
                        {
                            var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                            quotes.LstQuotes.ForEach(x =>
                            {
                                if (x.QuoteId == quoteId)
                                {
                                    x.QuoteRef = quoteRef;
                                }
                            });

                        }
                    }
                }
                else
                {
                    TempData["Message"] = "Please provide correct data";
                }

                return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating the quote detail for SFA
        /// </summary>
        /// <param name="supplierRef"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult UpdateQuoteDetail_SFA(string supplierRef, int quoteId, string quoteType)
        {
            try
            {
                if (quoteId > 0)
                {
                    int count = osp.UpdateQuoteDetail_SFA(quoteId, supplierRef, Convert.ToInt32(Session["UserID"]));
                    if (count > 0)
                    {
                        TempData["Message"] = "Quote detail updated successfully";

                        //if (Session["SearchQuoteResult"] != null){
                        //    var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                        //    quotes.LstQuotes.ForEach(x =>
                        //    {
                        //        if (x.QuoteId == quoteId){x.QuoteRef = quoteRef;}
                        //    });
                        //}
                    }
                }
                else
                {
                    TempData["Message"] = "Please provide correct data";
                }

                return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for getting the quote's requests and responses
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult GetQuoteRequestResponse(int quoteId, string quoteType)
        {
            try
            {
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    if (quoteType == "new")
                    {
                        var model = osp.GetQuoteRequestResponseModel(quoteId, quoteType, Convert.ToInt32(Session["DateFormatID"]), "ISD", "0");
                        model.quoteType = quoteType;
                        return PartialView("~/Views/QuoteSearch/ISD/NewQuote/_QuoteRequestResponse.cshtml", model);
                    }
                    else
                    {
                        var model = osp.GetQuoteRequestResponseModel(quoteId, quoteType, Convert.ToInt32(Session["DateFormatID"]), "ISD", "0");
                        model.quoteType = quoteType;
                        return PartialView("~/Views/QuoteSearch/ISD/OldQuote/_QuoteRequestResponse.cshtml", model);
                    }
                }
                else
                {
                    if (quoteType == "new")
                    {
                        var model = osp.GetQuoteRequestResponseModel(quoteId, quoteType, Convert.ToInt32(Session["DateFormatID"]), "SFA", Session["SFAID"].ToString());
                        model.quoteType = quoteType;
                        return PartialView("~/Views/QuoteSearch/SFA/NewQuote/_QuoteRequestResponse.cshtml", model);
                    }
                    else
                    {
                        var model = osp.GetQuoteRequestResponseModel(quoteId, quoteType, Convert.ToInt32(Session["DateFormatID"]), "SFA", Session["SFAID"].ToString());
                        model.quoteType = quoteType;
                        return PartialView("~/Views/QuoteSearch/SFA/OldQuote/_QuoteRequestResponse.cshtml", model);
                    }

                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                //return View("~/Views/Shared/ErrorPage.cshtml"); Commented (SUNNY GOHAR)
                return null;
            }
        }

        /// <summary>
        /// this action is responsible for sending the quote notification
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public JsonResult QuoteNotification(int quoteId, string quoteType)
        {
            try
            {
                int count = osp.QuoteNotification(quoteId, Convert.ToInt32(Session["UserID"]), quoteType, Convert.ToInt32(Session["DateFormatID"]));
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(-1, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for upload quote responses
        /// </summary>
        /// <param name="model"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult UploadQuoteResponses(List<UploadQuoteResponseModel> model, int quoteId, string quoteType)
        {
            try
            {
                if (quoteId > 0 && model != null)
                {

                    int count = osp.UploadQuoteResponsesNew(model, quoteId, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["DateFormatID"]), "new");
                    if (count > 0)
                    {
                        if (Session["SearchQuoteResult"] != null)
                        {
                            var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                            quotes.LstQuotes.ForEach(x =>
                            {
                                if (x.QuoteId == quoteId)
                                {
                                    x.Status = osp.GetQuoteStatusNew(quoteId);
                                }
                            });

                        }
                    }
                    TempData["Message"] = count > 0 ? "Quote Response(s) has been added" : "No record has been added";
                }
                else
                {
                    TempData["Message"] = "Please provide correct data";
                }
                return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for upload old quote responses
        /// </summary>
        /// <param name="model"></param>
        /// <param name="EOL"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult UploadOldQuoteResponses(List<UploadQuoteResponseModel> model, DateTime EOL, int quoteId, string quoteType)
        {
            try
            {
                if (quoteId > 0 && model != null)
                {
                    int count = osp.UploadQuoteResponsesOld(model, quoteId, Convert.ToInt32(Session["UserID"]), EOL, Convert.ToInt32(Session["DateFormatID"]), "", "new");
                    if (count > 0)
                    {
                        if (Session["SearchQuoteResult"] != null)
                        {
                            var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                            quotes.LstQuotes.ForEach(x =>
                            {
                                if (x.QuoteId == quoteId)
                                {
                                    x.Status = osp.GetQuoteStatusOld(quoteId);
                                }
                            });

                        }
                    }
                    TempData["Message"] = count > 0 ? "Quote Response(s) has been added" : "No record has been added";

                }
                else
                {
                    TempData["Message"] = "Provide provide correct data";
                }
                return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for getting quote response by responseid
        /// </summary>
        /// <param name="quoteRId"></param>
        /// <param name="quoteId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult GetQuoteResponse(int quoteRId, int quoteId, int customerId, string quoteType)
        {
            try
            {
                if (quoteType == "new")
                {
                    var model = osp.GetNewQuoteResponse(quoteRId, quoteId, customerId);
                    return PartialView("~/Views/QuoteSearch/ISD/NewQuote/_QuoteResponseEdit.cshtml", model);
                }
                else
                {
                    var model = osp.GetOldQuoteResponse(quoteRId, quoteId, customerId);
                    return PartialView("~/Views/QuoteSearch/ISD/OldQuote/_QuoteResponseEdit.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is resposible for updating quote response
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult UpdateQuoteResponse(sp_getQuoteResponseById_AdminR_Result obj, int quoteId, string quoteType)
        {
            try
            {
                if (quoteType == "new")
                {
                    int count = osp.UpdateNewQuoteResponse(obj, quoteId, Convert.ToInt32(Session["UserID"]));
                    TempData["Message"] = count > 0 ? "Quote Response has been updated successfully" : "No record were updated.";
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                else
                {
                    int count = osp.UpdateOldQuoteResponse(obj, quoteId, Convert.ToInt32(Session["UserID"]));
                    TempData["Message"] = count > 0 ? "Quote Response has been updated successfully" : "No record were updated.";
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
            
        public JsonResult DeleteResponse(string quoteRId, int quoteId, string quoteType)
        {
            try
            {
                if (quoteType == "new")
                {
                    int count = osp.DeleteNewQuoteResponse(quoteRId, quoteId, Convert.ToInt32(Session["UserID"]));
                    if (count > 0)
                    {
                        if (Session["SearchQuoteResult"] != null)
                        {
                            var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                            quotes.LstQuotes.ForEach(x =>
                            {
                                if (x.QuoteId == quoteId)
                                {
                                    x.Status = osp.GetQuoteStatus(quoteId, "new");
                                }
                            });

                        }
                    }
                    var msg = TempData["Message"] = count > 0 ? "Quote Response has been deleted successfully." : "No Quote Response has been deleted.";
                    return Json(msg,JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                else if (quoteType == "old")
                {
                    int count = osp.DeleteOldQuoteResponse(quoteRId, quoteId, Convert.ToInt32(Session["UserID"]));
                    if (count > 0)
                    {
                        if (Session["SearchQuoteResult"] != null)
                        {
                            var quotes = (QuoteListModel)Session["SearchQuoteResult"];
                            quotes.LstQuotes.ForEach(x =>
                            {
                                if (x.QuoteId == quoteId)
                                {
                                    x.Status = osp.GetQuoteStatus(quoteId, "old");
                                }
                            });
                        }
                    }
                    var msg = TempData["Message"] = count > 0 ? "Quote Response has been deleted successfully." : "No Quote Response has been deleted.";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                
                return Json("error", JsonRequestBehavior.AllowGet);
                //return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for deleting the response
        /// </summary>
        /// <param name="quoteRId"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        //public ActionResult DeleteResponse(int quoteRId, int quoteId, string quoteType)
        //{
        //    try
        //    {
        //        if (quoteType == "new")
        //        {
        //            int count = osp.DeleteNewQuoteResponse(quoteRId, quoteId, Convert.ToInt32(Session["UserID"]));
        //            if (count > 0)
        //            {
        //                if (Session["SearchQuoteResult"] != null)
        //                {
        //                    var quotes = (QuoteListModel)Session["SearchQuoteResult"];
        //                    quotes.LstQuotes.ForEach(x =>
        //                    {
        //                        if (x.QuoteId == quoteId)
        //                        {
        //                            x.Status = osp.GetQuoteStatus(quoteId, "new");
        //                        }
        //                    });

        //                }
        //            }
        //            TempData["Message"] = count > 0 ? "Quote Response has been deleted successfully." : "No Quote Response has been deleted.";
        //            return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
        //        }
        //        else if (quoteType == "old")
        //        {
        //            int count = osp.DeleteOldQuoteResponse(quoteRId, quoteId, Convert.ToInt32(Session["UserID"]));
        //            if (count > 0)
        //            {
        //                if (Session["SearchQuoteResult"] != null)
        //                {
        //                    var quotes = (QuoteListModel)Session["SearchQuoteResult"];
        //                    quotes.LstQuotes.ForEach(x =>
        //                    {
        //                        if (x.QuoteId == quoteId)
        //                        {
        //                            x.Status = osp.GetQuoteStatus(quoteId, "old");
        //                        }
        //                    });

        //                }
        //            }
        //            TempData["Message"] = count > 0 ? "Quote Response has been deleted successfully." : "No Quote Response has been deleted.";
        //            return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        var routeData = this.ControllerContext.RouteData;
        //        LogException(ex.Message, routeData);
        //        if (ex.InnerException != null)
        //        {
        //            LogException(ex.InnerException.ToString(), routeData);
        //        }
        //        return View("~/Views/Shared/ErrorPage.cshtml");
        //    }
        //}

        /// <summary>
        /// this action is responsible for getting quote history comment
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult GetQuoteHistoryComment(int quoteId, string quoteType)
        {
            try
            {
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    if (quoteType == "new")
                    {
                        var model = osp.GetNewQuoteCommentHistoryModel(quoteId, Convert.ToInt32(Session["DateFormatID"]));
                        model.quoteType = quoteType;
                        model.JiraTicketNumber = osp.GetJIRATicketNumber(quoteId, quoteType);
                        return PartialView("~/Views/QuoteSearch/ISD/NewQuote/_QuoteHistoryComments.cshtml", model);
                    }
                    else
                    {
                        var model = osp.GetOldQuoteCommentHistoryModel(quoteId, Convert.ToInt32(Session["DateFormatID"]));
                        model.quoteType = quoteType;
                        model.JiraTicketNumber = osp.GetJIRATicketNumber(quoteId, quoteType);
                        return PartialView("~/Views/QuoteSearch/ISD/OldQuote/_QuoteHistoryComments.cshtml", model);
                    }
                }
                else
                {
                    if (quoteType == "new")
                    {
                        var model = osp.GetNewQuoteCommentHistoryModel(quoteId, Convert.ToInt32(Session["DateFormatID"]));
                        model.quoteType = quoteType;
                        return PartialView("~/Views/QuoteSearch/SFA/NewQuote/_QuoteHistoryComments.cshtml", model);
                    }
                    else
                    {
                        var model = osp.GetOldQuoteCommentHistoryModel(quoteId, Convert.ToInt32(Session["DateFormatID"]));
                        model.quoteType = quoteType;
                        return PartialView("~/Views/QuoteSearch/SFA/OldQuote/_QuoteHistoryComments.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for adding quote comment
        /// 
        /// Aurangzeb Khan
        /// 25/10/2018
        /// Added isurgent check for the email subject change
        /// 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="comments"></param>
        /// <param name="isGSOnly"></param>
        /// <returns></returns>
        public JsonResult AddQuoteComment(int quoteId, string comments, string quoteType, bool isGSOnly = false, bool IsUrgent = false, string JiraTicketNum = "")
        {
            if (quoteId > 0 && !string.IsNullOrEmpty(comments))
            {
                try
                {
                    int count = 0;
                    if (quoteType == "new")
                    {
                        count = osp.AddNewQuoteComment(quoteId, comments, isGSOnly, Convert.ToInt32(Session["UserID"]), Session["UserName"].ToString(), IsUrgent, JiraTicketNum);
                    }
                    else
                    {
                        count = osp.AddOldQuoteComment(quoteId, comments, isGSOnly, Convert.ToInt32(Session["UserID"]), Session["UserName"].ToString(), IsUrgent, JiraTicketNum);
                    }

                    if (count == 0)
                    {
                        return Json("-1", JsonRequestBehavior.AllowGet);
                    }
                    return Json(count, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// this action is responsible for getting quote attachment
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult GetQuoteAttachments(int quoteId, string quoteType)
        {
            if (quoteId > 0)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        if (quoteType == "new")
                        {
                            var model = osp.GetQuoteAttachments(quoteId, "new");
                            return PartialView("~/Views/QuoteSearch/ISD/NewQuote/_QuoteAttachments.cshtml", model);
                        }
                        else
                        {
                            var model = osp.GetQuoteAttachments(quoteId, "old");
                            return PartialView("~/Views/QuoteSearch/ISD/OldQuote/_QuoteAttachments.cshtml", model);
                        }
                    }
                    else
                    {
                        if (quoteType == "new")
                        {
                            var model = osp.GetQuoteAttachments(quoteId, "new");
                            return PartialView("~/Views/QuoteSearch/SFA/NewQuote/_QuoteAttachments.cshtml", model);
                        }
                        else
                        {
                            var model = osp.GetQuoteAttachments(quoteId, "old");
                            return PartialView("~/Views/QuoteSearch/SFA/OldQuote/_QuoteAttachments.cshtml", model);
                        }
                    }

                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for adding quote attachment
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="attachFile"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddQuoteAttachment(int quoteId, HttpPostedFileBase attachFile, string quoteType)
        {
            var msg = string.Empty;
            var isSuccess = false;
            try
            {
                if (attachFile != null && attachFile.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(attachFile.FileName).Replace("^", "");
                    var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                    string path = string.Empty;
                    if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".png")
                    {
                        if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                        {
                            var docId = System.Guid.NewGuid().ToString();
                            path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                            attachFile.SaveAs(path);
                            int? count = osp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path, ConfigurationManager.AppSettings["UploadURL"] + "/" + docId + "^" + fileName, Convert.ToInt32(Session["UserID"]), quoteId, null, quoteType);
                            if (count <= 0)
                            {
                                msg = "File Attaching Error";
                            }
                            else
                            {
                                msg = "Attachment has been added successfully";
                            }

                            isSuccess = true;
                        }
                        else
                        {
                            msg = "Invalid file size. Maximum allowed size is 10MB.";
                        }
                    }
                    else
                    {
                        msg = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";

                    }
                }
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = "Error Occured";
                isSuccess = false;
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeActivateQuoteAttachment(int quoteId, string quoteType, int daId)
        {
            try
            {
                var getStatus = osp.DeActivateQuoteAttachment(quoteId, quoteType, daId);
                return Json(getStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for downloading the template for adding responses
        /// </summary>
        /// <returns></returns>
        public ActionResult DownloadTemplate(string quoteType)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    string filepath = string.Empty;

                    if (Session["UserGroupName"].ToString() == "ISD")
                    {
                        if (quoteType == "new")
                        {
                            filepath = Server.MapPath(ConfigurationManager.AppSettings["TempNewQuoteURL"]);
                        }
                        else
                        {
                            filepath = Server.MapPath(ConfigurationManager.AppSettings["TempOldQuoteURL"]);
                        }
                    }
                    else
                    {
                        if (quoteType == "new")
                        {
                            filepath = Server.MapPath(ConfigurationManager.AppSettings["TempSFANewQuoteURL"]);
                        }
                        else
                        {
                            filepath = Server.MapPath(ConfigurationManager.AppSettings["TempOldQuoteURL"]);
                        }
                    }


                    string contentType = "application/octet-stream";
                    return File(filepath, contentType, Path.GetFileName(filepath));
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }

        /// <summary>
        /// this action is responsible for uploading new quote response for ISD
        /// </summary>
        /// <param name="attachFile"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadBulkNewQuoteResponseISD(HttpPostedFileBase attachFile, int quoteId, string quoteType)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName);
                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                                attachFile.SaveAs(path);
                                DataSet ds = GetExcelFileData(path);
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {

                                        List<string> columns = new List<string>() { "manufacturer","sku","description" ,"category",
                                                          "leadtime","eol (mm/dd/yyyy)","price","quantity","totalprice"};
                                        int count = 0;
                                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                                        {
                                            string isfound = columns.Where(x => x == ds.Tables[0].Columns[i].ColumnName.Replace("_", "").ToLower()).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(isfound))
                                            {
                                                count++;
                                            }
                                        }
                                        if (count != 9)
                                        {
                                            TempData["Message"] = "File Data Is Mismatch. Please Use Given Template";
                                            return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                                        }

                                        QuoteResponseBulkUploadModel result = GetListOfUploadNewQuoteResponseISD(ds.Tables[0], quoteId, quoteType);

                                        if (result.UploadQuoteResponse != null && result.UploadQuoteResponse.Count > 0)
                                        {
                                            UploadQuoteResponses(result.UploadQuoteResponse, quoteId, quoteType);
                                        }
                                        else if (result.UploadQuoteResponse != null && result.UploadQuoteResponse.Count == 0)
                                        {
                                            TempData["Message"] = "No response found in xls. Please add response first then try to upload.";
                                        }
                                        else
                                        {
                                            TempData["Message"] = result.ErrorMsg;
                                        }



                                        return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });

                                    }
                                    else
                                    {
                                        TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                    }
                                }
                                else
                                {
                                    TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                }

                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                            }

                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .xls,.xlsx";
                        }

                    }
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    TempData["Message"] = "Invalid file.";
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }

        /// <summary>
        /// this action is responsible for uploading old quote response for ISD
        /// </summary>
        /// <param name="attachFile"></param>
        /// <param name="quoteId"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public ActionResult UploadBulkOldQuoteResponseISD(HttpPostedFileBase attachFile, int quoteId, DateTime eol, string quoteType)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName);
                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                                attachFile.SaveAs(path);
                                DataSet ds = GetExcelFileData(path);
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {

                                        List<string> columns = new List<string>() { "manufacturer", "sku", "description", "category", "leadtime", "quantity", "unitcost", "totalcost" };
                                        int count = 0;
                                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                                        {
                                            string isfound = columns.Where(x => x == ds.Tables[0].Columns[i].ColumnName.Replace("_", "").ToLower()).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(isfound))
                                            {
                                                count++;
                                            }
                                        }
                                        if (count != 8)
                                        {
                                            TempData["Message"] = "File Data Is Mismatch. Please Use Given Template";
                                            return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                                        }

                                        QuoteResponseBulkUploadModel result = GetListOfUploadOldQuoteResponseISD(ds.Tables[0], quoteId, eol, quoteType);

                                        if (result.UploadQuoteResponse != null && result.UploadQuoteResponse.Count > 0)
                                        {
                                            UploadOldQuoteResponses(result.UploadQuoteResponse, Convert.ToDateTime(eol), quoteId, quoteType);
                                        }
                                        else if (result.UploadQuoteResponse != null && result.UploadQuoteResponse.Count == 0)
                                        {
                                            TempData["Message"] = "No response found in xls. Please add response first then try to upload.";
                                        }
                                        else
                                        {
                                            TempData["Message"] = result.ErrorMsg;
                                        }



                                        return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });

                                    }
                                    else
                                    {
                                        TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                    }
                                }
                                else
                                {
                                    TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                }

                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                            }

                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .xls,.xlsx";
                        }

                    }
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    TempData["Message"] = "Invalid file.";
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }

        /// <summary>
        /// this action is responsible for getting data from excel file to dataset
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private DataSet GetExcelFileData(string filePath)
        {
            string connString = string.Empty;

            var fileExt = System.IO.Path.GetExtension(filePath);

            if (fileExt == ".xlsx")
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";";
            // string a =   @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";";
            else if (fileExt == ".xls")
                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=Excel 8.0";

            OleDbConnection oledbConn = new OleDbConnection(connString);
            oledbConn.Open();

            DataTable Sheets = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            DataSet ds = new DataSet();
            foreach (DataRow dr in Sheets.Rows)
            {
                string sht = dr[2].ToString().Replace("'", "");
                if (sht.Contains('$'))
                {
                    OleDbDataAdapter dataAdapter = new OleDbDataAdapter("select * from [" + sht + "]", oledbConn);
                    dataAdapter.Fill(ds);
                }

            }

            return ds;
        }

        /// <summary>
        ///  this action is responsible for getting list of response after verify record from datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        private QuoteResponseBulkUploadModel GetListOfUploadNewQuoteResponseISD(DataTable dt, int quoteId, string quoteType)
        {
            QuoteResponseBulkUploadModel result = new QuoteResponseBulkUploadModel();
            List<UploadQuoteResponseModel> lst = new List<UploadQuoteResponseModel>();
            UploadQuoteResponseModel obj;
            int count = 0;

            var mfgList = osp.GetManufacturer();
            var catList = osp.GetQuoteReqResCategories(quoteId, quoteType);

            foreach (DataRow row in dt.Rows)
            {
                if ((string.IsNullOrEmpty(Convert.ToString(row["EOL (mm/dd/yyyy)"])) && string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"])) && string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["PRICE"])) && string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["SKU"])) //&& string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))
                    )
                    && (Convert.ToString(row["TOTAL_PRICE"]) == "0")
                    )
                {

                }
                else
                {

                    obj = new UploadQuoteResponseModel();
                    DateTime datetime;
                    if (!DateTime.TryParse(row["EOL (mm/dd/yyyy)"].ToString(), out datetime))
                    {
                        result.ErrorMsg = "Incorrect EOL in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"]))) { result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"]))) { result.ErrorMsg = "Description should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"]))) { result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["PRICE"]))) { result.ErrorMsg = "Incorrect Price in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"]))) { result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["SKU"]))) { result.ErrorMsg = "SKU should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    //else if (string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))) { result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }


                    var manufacturer = mfgList.FirstOrDefault(x => x.Value.ToLower() == row["MANUFACTURER"].ToString().ToLower());

                    if (manufacturer == null)
                    {
                        result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    var category = catList.FirstOrDefault(x => x.partcategoryname.ToLower() == row["CATEGORY"].ToString().ToLower());
                    if (category == null)
                    {
                        result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    decimal price;
                    if (!decimal.TryParse(row["PRICE"].ToString(), out price))
                    {
                        result.ErrorMsg = "Incorrect Price in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    int quantity;
                    if (!int.TryParse(row["QUANTITY"].ToString(), out quantity))
                    {
                        result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"])))
                    {
                        int leadtime;
                        if (!int.TryParse(row["LEAD_TIME"].ToString(), out leadtime))
                        {
                            result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1);
                            result.UploadQuoteResponse = null; return result;
                        }
                        obj.LeadTime = leadtime;
                    }
                    else
                    {
                        obj.LeadTime = null;
                    }


                    obj.ManufacturerIds = manufacturer.Key;
                    obj.EOL = datetime;
                    obj.SKU = row["SKU"].ToString();
                    obj.Description = row["DESCRIPTION"].ToString();
                    obj.CategoryId = Convert.ToInt32(category.partcategoryid);
                    obj.Price = price;
                    obj.Quantity = quantity;

                    lst.Add(obj);
                    count++;



                }


            }
            result.UploadQuoteResponse = lst;
            return result;
        }

        /// <summary>
        ///  this action is responsible for getting list of response after verify record from datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        private QuoteResponseBulkUploadModel GetListOfUploadOldQuoteResponseISD(DataTable dt, int quoteId, DateTime eol, string quoteType)
        {
            QuoteResponseBulkUploadModel result = new QuoteResponseBulkUploadModel();
            List<UploadQuoteResponseModel> lst = new List<UploadQuoteResponseModel>();
            UploadQuoteResponseModel obj;
            int count = 0;

            var mfgList = osp.GetManufacturer();
            var catList = osp.GetQuoteReqResCategories(quoteId, quoteType);

            foreach (DataRow row in dt.Rows)
            {

                if ((string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"])) && string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"])) && string.IsNullOrEmpty(Convert.ToString(row["UNIT_COST"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"])) && string.IsNullOrEmpty(Convert.ToString(row["SKU"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))) && (Convert.ToString(row["TOTAL_Cost"]) == "0")
                    )
                {

                }
                else
                {
                    obj = new UploadQuoteResponseModel();

                    if (string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"]))) { result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"]))) { result.ErrorMsg = "Description should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"]))) { result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["UNIT_COST"]))) { result.ErrorMsg = "Incorrect unit cost in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"]))) { result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["SKU"]))) { result.ErrorMsg = "SKU should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))) { result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }


                    var manufacturer = mfgList.FirstOrDefault(x => x.Value.ToLower() == row["MANUFACTURER"].ToString().ToLower());

                    if (manufacturer == null)
                    {
                        result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    var category = catList.FirstOrDefault(x => x.partcategoryname.ToLower() == row["CATEGORY"].ToString().ToLower());
                    if (category == null)
                    {
                        result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    decimal price;
                    if (!decimal.TryParse(row["UNIT_COST"].ToString(), out price))
                    {
                        result.ErrorMsg = "Incorrect unit cost in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    int quantity;
                    if (!int.TryParse(row["QUANTITY"].ToString(), out quantity))
                    {
                        result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"])))
                    {
                        int leadtime;
                        if (!int.TryParse(row["LEAD_TIME"].ToString(), out leadtime))
                        {
                            result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1);
                            result.UploadQuoteResponse = null; return result;
                        }
                        obj.LeadTime = leadtime;
                    }
                    else
                    {
                        obj.LeadTime = null;
                    }

                    obj.ManufacturerIds = manufacturer.Key;
                    obj.EOL = eol;
                    obj.SKU = row["SKU"].ToString();
                    obj.Description = row["DESCRIPTION"].ToString();
                    obj.CategoryId = Convert.ToInt32(category.partcategoryid);
                    obj.Price = price;
                    obj.Quantity = quantity;

                    lst.Add(obj);
                    count++;
                }
            }
            result.UploadQuoteResponse = lst;
            return result;
        }


        //SFA
        /// <summary>
        /// this action is responsible for uploading new quote response for SFA
        /// </summary>
        /// <param name="attachFile"></param>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult UploadBulkNewQuoteResponseSFA(HttpPostedFileBase attachFile, int quoteId, string quoteType)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName);
                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                                attachFile.SaveAs(path);
                                DataSet ds = GetExcelFileData(path);
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {

                                        List<string> columns = new List<string>() { "manufacturer","sku","description" ,"category",
                                                          "leadtime","eol (mm/dd/yyyy)","price","quantity","totalprice"};
                                        int count = 0;
                                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                                        {
                                            string isfound = columns.Where(x => x == ds.Tables[0].Columns[i].ColumnName.Replace("_", "").ToLower()).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(isfound))
                                            {
                                                count++;
                                            }
                                        }
                                        if (count != 9)
                                        {
                                            TempData["Message"] = "File Data Is Mismatch. Please Use Given Template";
                                            return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                                        }

                                        QuoteResponseBulkUploadModel result = GetListOfUploadNewQuoteResponseSFA(ds.Tables[0], quoteId, quoteType);

                                        if (result.UploadQuoteResponse != null && result.UploadQuoteResponse.Count > 0)
                                        {
                                            UploadQuoteResponses(result.UploadQuoteResponse, quoteId, quoteType);
                                        }
                                        else if (result.UploadQuoteResponse != null && result.UploadQuoteResponse.Count == 0)
                                        {
                                            TempData["Message"] = "No response found in xls. Please add response first then try to upload.";
                                        }
                                        else
                                        {
                                            TempData["Message"] = result.ErrorMsg;
                                        }



                                        return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });

                                    }
                                    else
                                    {
                                        TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                    }
                                }
                                else
                                {
                                    TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                }

                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                            }

                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .xls,.xlsx";
                        }

                    }
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    TempData["Message"] = "Invalid file.";
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }



        }

        /// <summary>
        ///  this action is responsible for getting list of response after verify record from datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        private QuoteResponseBulkUploadModel GetListOfUploadNewQuoteResponseSFA(DataTable dt, int quoteId, string quoteType)
        {
            QuoteResponseBulkUploadModel result = new QuoteResponseBulkUploadModel();
            List<UploadQuoteResponseModel> lst = new List<UploadQuoteResponseModel>();
            UploadQuoteResponseModel obj;
            int count = 0;

            var mfgList = osp.GetManufacturer();
            var catList = osp.GetQuoteReqResCategories(quoteId, quoteType);

            foreach (DataRow row in dt.Rows)
            {
                if ((string.IsNullOrEmpty(Convert.ToString(row["EOL (mm/dd/yyyy)"])) && string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"])) && string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["PRICE"])) && string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"])) &&
                    string.IsNullOrEmpty(Convert.ToString(row["SKU"])) && string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))) && (Convert.ToString(row["TOTAL_PRICE"]) == "0")
                    )
                {

                }
                else
                {

                    obj = new UploadQuoteResponseModel();
                    DateTime datetime;
                    if (!DateTime.TryParse(row["EOL (mm/dd/yyyy)"].ToString(), out datetime))
                    {
                        result.ErrorMsg = "Incorrect EOL in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"]))) { result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"]))) { result.ErrorMsg = "Description should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"]))) { result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["PRICE"]))) { result.ErrorMsg = "Incorrect Price in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"]))) { result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["SKU"]))) { result.ErrorMsg = "SKU should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                    else if (string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))) { result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }


                    var manufacturer = mfgList.FirstOrDefault(x => x.Value.ToLower() == row["MANUFACTURER"].ToString().ToLower());

                    if (manufacturer == null)
                    {
                        result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    var category = catList.FirstOrDefault(x => x.partcategoryname.ToLower() == row["CATEGORY"].ToString().ToLower());
                    if (category == null)
                    {
                        result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    decimal price;
                    if (!decimal.TryParse(row["PRICE"].ToString(), out price))
                    {
                        result.ErrorMsg = "Incorrect Price in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }

                    int quantity;
                    if (!int.TryParse(row["QUANTITY"].ToString(), out quantity))
                    {
                        result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1);
                        result.UploadQuoteResponse = null; return result;
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"])))
                    {
                        int leadtime;
                        if (!int.TryParse(row["LEAD_TIME"].ToString(), out leadtime))
                        {
                            result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1);
                            result.UploadQuoteResponse = null; return result;
                        }
                        obj.LeadTime = leadtime;
                    }
                    else
                    {
                        obj.LeadTime = null;
                    }


                    obj.ManufacturerIds = manufacturer.Key;
                    obj.EOL = datetime;
                    obj.SKU = row["SKU"].ToString();
                    obj.Description = row["DESCRIPTION"].ToString();
                    obj.CategoryId = Convert.ToInt32(category.partcategoryid);
                    obj.Price = price;
                    obj.Quantity = quantity;

                    lst.Add(obj);
                    count++;



                }


            }
            result.UploadQuoteResponse = lst;
            return result;
        }

        /// <summary>
        /// this action is responsible for uploading old quote response for SFA
        /// </summary>
        /// <param name="attachFile"></param>
        /// <param name="quoteId"></param>
        /// <param name="eol"></param>
        /// <returns></returns>
        public ActionResult UploadBulkOldQuoteResponseSFA(HttpPostedFileBase attachFile, int quoteId, DateTime eol, string quoteType)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName);
                        if (fileExtension == ".xls" || fileExtension == ".xlsx")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                                attachFile.SaveAs(path);
                                DataSet ds = GetExcelFileData(path);
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {

                                        List<string> columns = new List<string>() { "manufacturer", "sku", "description", "category", "leadtime", "price", "quantity" };
                                        int count = 0;
                                        for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                                        {
                                            string isfound = columns.Where(x => x == ds.Tables[0].Columns[i].ColumnName.Replace("_", "").ToLower()).FirstOrDefault();
                                            if (!string.IsNullOrEmpty(isfound))
                                            {
                                                count++;
                                            }
                                        }
                                        if (count != 7)
                                        {
                                            TempData["Message"] = "File Data Is Mismatch. Please Use Given Template";
                                            return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                                        }

                                        QuoteResponseBulkUploadModel result = GetListOfUploadOldQuoteResponseSFA(ds.Tables[0], quoteId, eol, quoteType);

                                        if (result.UploadQuoteResponse != null)
                                        {
                                            UploadOldQuoteResponses(result.UploadQuoteResponse, Convert.ToDateTime(eol), quoteId, quoteType);
                                        }
                                        else
                                        {
                                            TempData["Message"] = result.ErrorMsg;
                                        }



                                        return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });

                                    }
                                    else
                                    {
                                        TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                    }
                                }
                                else
                                {
                                    TempData["Message"] = "The data in xls is not in required format. Please use the specified format.";
                                }

                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                            }

                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .xls,.xlsx";
                        }

                    }
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    TempData["Message"] = "Invalid file.";
                    return RedirectToAction("QuoteDetail", new { @quoteId = quoteId, @quoteType = quoteType });
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }

        /// <summary>
        ///  this action is responsible for getting list of response after verify record from datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="quoteId"></param>
        /// <param name="quoteType"></param>
        /// <returns></returns>
        private QuoteResponseBulkUploadModel GetListOfUploadOldQuoteResponseSFA(DataTable dt, int quoteId, DateTime eol, string quoteType)
        {
            QuoteResponseBulkUploadModel result = new QuoteResponseBulkUploadModel();
            List<UploadQuoteResponseModel> lst = new List<UploadQuoteResponseModel>();
            UploadQuoteResponseModel obj;
            int count = 0;

            var mfgList = osp.GetManufacturer();
            var catList = osp.GetQuoteReqResCategories(quoteId, quoteType);

            foreach (DataRow row in dt.Rows)
            {
                obj = new UploadQuoteResponseModel();

                if (string.IsNullOrEmpty(Convert.ToString(row["MANUFACTURER"]))) { result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["DESCRIPTION"]))) { result.ErrorMsg = "Description should not be empty in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["CATEGORY"]))) { result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["PRICE"]))) { result.ErrorMsg = "Incorrect Price in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["QUANTITY"]))) { result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["SKU"]))) { result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }
                else if (string.IsNullOrEmpty(Convert.ToString(row["LEAD_TIME"]))) { result.ErrorMsg = "Incorrect lead time in excel file on Row No. " + (count + 1); result.UploadQuoteResponse = null; return result; }

                var manufacturer = mfgList.FirstOrDefault(x => x.Value.ToLower() == row["MANUFACTURER"].ToString().ToLower());

                if (manufacturer == null)
                {
                    result.ErrorMsg = "Incorrect Manufacturer in excel file on Row No. " + (count + 1);
                    result.UploadQuoteResponse = null; return result;
                }

                var category = catList.FirstOrDefault(x => x.partcategoryname.ToLower() == row["CATEGORY"].ToString().ToLower());
                if (category == null)
                {
                    result.ErrorMsg = "Incorrect Part Category in excel file on Row No. " + (count + 1);
                    result.UploadQuoteResponse = null; return result;
                }

                decimal price;
                if (!decimal.TryParse(row["PRICE"].ToString(), out price))
                {
                    result.ErrorMsg = "Incorrect Price in excel file on Row No. " + (count + 1);
                    result.UploadQuoteResponse = null; return result;
                }

                int quantity;
                if (!int.TryParse(row["QUANTITY"].ToString(), out quantity))
                {
                    result.ErrorMsg = "Incorrect quantity in excel file on Row No. " + (count + 1);
                    result.UploadQuoteResponse = null; return result;
                }


                obj.ManufacturerIds = manufacturer.Key;
                obj.EOL = eol;
                obj.SKU = row["SKU"].ToString();
                obj.Description = row["DESCRIPTION"].ToString();
                obj.CategoryId = Convert.ToInt32(category.partcategoryid);
                obj.Price = price;
                obj.Quantity = quantity;
                obj.LeadTime = Convert.ToInt32(row["LEAD_TIME"]);

                lst.Add(obj);
                count++;
            }
            result.UploadQuoteResponse = lst;
            return result;
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 4th September 2018
        /// This function is used for exporting quote details to pdf.
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public ActionResult ExportQuoteDetailToPdf(string quoteId, string quoteType)
        {
            try
            {
                string customerName = "";
                string symbol = "";
                QuoteDetailModel quoteDetailModel = new QuoteDetailModel();
                QuoteRequestResponseModel modelRequestResponse = new QuoteRequestResponseModel();
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    quoteDetailModel = osp.GetQuoteDetail(Convert.ToInt32(quoteId), quoteType, "ISD", 0);
                    modelRequestResponse = osp.GetQuoteRequestResponseModel(Convert.ToInt32(quoteId), quoteType, Convert.ToInt32(Session["DateFormatID"]), "ISD", "0");
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    quoteDetailModel = osp.GetQuoteDetail(Convert.ToInt32(quoteId), quoteType, "SFA", Convert.ToInt32(Session["SFAID"]));
                    modelRequestResponse = osp.GetQuoteRequestResponseModel(Convert.ToInt32(quoteId), quoteType, Convert.ToInt32(Session["DateFormatID"]), "SFA", Session["SFAID"].ToString());
                }

                if (quoteDetailModel.NewQuoteDetail != null)
                {
                    customerName = quoteDetailModel.NewQuoteDetail.customername.ToString();
                    symbol = quoteDetailModel.NewQuoteDetail.Symbol;
                }
                else
                {
                    customerName = quoteDetailModel.OldQuoteDetail.customername.ToString();
                    symbol = quoteDetailModel.OldQuoteDetail.currencysymbol;
                }



                iTextSharp.text.Image logoImg =
                        iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                logoImg.ScaleAbsolute(250, 45);

                PdfPTable pdfTable = FillQuoteDetailPdfDetails(quoteDetailModel);


                PdfPTable requestItemsTbl = new PdfPTable(4);
                var columnNames = new string[] { "Part Description", "Manufacturer", "SKU", "Required Quantity" };

                float[] columnsWitdh = new float[] { 50f, 20f, 20f, 10f };

                dynamic requestItems;
                dynamic reponseItem;
                PdfPTable responseItemsTbl = new PdfPTable(8);

                if (quoteType == "new")
                {
                    requestItems = new List<QuoteRequestModel>();
                    reponseItem = new List<sp_getQuoteResponse_New_AdminR_Result>();
                    requestItems = modelRequestResponse.QuoteRequestList;

                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        reponseItem = modelRequestResponse.NewQuoteResponse_SFA;
                    }
                    else
                    {
                        reponseItem = modelRequestResponse.NewQuoteResponse;
                    }


                    if (requestItems != null && requestItems.Count > 0)
                    {
                        MakePdfFileDataGridHeader(requestItemsTbl, columnNames, columnsWitdh, 100);
                        QuoteItemsPdfFileDataGridBody(requestItemsTbl, requestItems);
                    }
                    if (reponseItem != null && reponseItem.Count > 0)
                    {
                        columnNames = new string[] { "Manufacturer", "SKU", "Description", "Category", "Lead Time", "EOL", "Quantity", "Unit Price" };
                        columnsWitdh = new float[] { 10f, 10f, 30f, 10f, 5f, 15f, 10f, 10f };
                        MakePdfFileDataGridHeader(responseItemsTbl, columnNames, columnsWitdh, 100);

                        if (Session["UserGroupName"].ToString() == "SFA")
                        {
                            QuoteResponseItemsPdfFileDataGridBodySFA(responseItemsTbl, reponseItem, symbol, quoteType);
                        }
                        else
                        {
                            QuoteResponseItemsPdfFileDataGridBody(responseItemsTbl, reponseItem, symbol, quoteType);
                        }
                    }
                }
                else
                {
                    requestItems = new List<tblPartQuoteRequest>();
                    reponseItem = new List<sp_getQuoteResponse_Old_AdminR_Result>();
                    requestItems = modelRequestResponse.PartQuoteRequests;
                    reponseItem = modelRequestResponse.OldQuoteResponse;

                    if (requestItems != null && requestItems.Count > 0)
                    {
                        MakePdfFileDataGridHeader(requestItemsTbl, columnNames, columnsWitdh, 100);
                        QuoteItemsPdfFileDataGridBodyOld(requestItemsTbl, requestItems);

                    }


                    if (reponseItem != null && reponseItem.Count > 0)
                    {
                        responseItemsTbl = new PdfPTable(7);
                        columnNames = new string[] { "Manufacturer", "SKU", "Description", "Category", "EOL", "Quantity", "Unit Price" };
                        columnsWitdh = new float[] { 10f, 10f, 30f, 10f, 15f, 10f, 10f };
                        MakePdfFileDataGridHeader(responseItemsTbl, columnNames, columnsWitdh, 100);
                        QuoteResponseItemsPdfFileDataGridBodyOld(responseItemsTbl, reponseItem, symbol, quoteType);
                    }
                }



                var attachments = osp.GetQuoteAttachments(Convert.ToInt32(quoteId), quoteType);
                PdfPTable AttachmentsTbl = AddExportAttachments(attachments.Attachments);

                dynamic comments;

                if (quoteType == "new")
                {
                    comments = new QuoteCommentHistoryModel();
                    comments = osp.GetNewQuoteCommentHistoryModel(Convert.ToInt32(quoteId), Convert.ToInt32(Session["DateFormatID"]));
                }
                else
                {
                    comments = new QuoteCommentHistoryModel();
                    comments = osp.GetOldQuoteCommentHistoryModel(Convert.ToInt32(quoteId), Convert.ToInt32(Session["DateFormatID"]));
                }

                PdfPTable commentsTbl = null;

                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    commentsTbl = AddExportComments(comments.GSQuoteComment);
                }

                PdfPTable commentsHistoryTbl = AddExportHistoryComments(comments.QuoteHistory);

                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                {
                    Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();

                    if (customerName.ToLower() != "abbott")
                    {
                        document.Add(logoImg);
                    }
                    document.Add(new Chunk("\n"));
                    document.Add(pdfTable);
                    document.Add(new Chunk("\n"));

                    if (requestItems != null && requestItems.Count > 0)
                    {
                        document.Add(new Paragraph("Requests"));
                        document.Add(new Paragraph("\n"));
                        document.Add(requestItemsTbl);
                    }

                    if (attachments.Attachments.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Attachments"));
                        document.Add(new Paragraph("\n"));
                        document.Add(AttachmentsTbl);
                    }
                    if (reponseItem != null && reponseItem.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Responses"));
                        document.Add(new Paragraph("\n"));
                        document.Add(responseItemsTbl);
                    }

                    if (comments.GSQuoteComment.Count > 0 || comments.QuoteHistory.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Comments:"));
                        document.Add(new Paragraph("\n"));


                        if (comments.QuoteHistory.Count > 0)
                        {

                            document.Add(new Paragraph("History:", FontFactory.GetFont("Arial", 10, 0, BaseColor.BLACK)));
                            document.Add(new Paragraph("\n"));
                            document.Add(commentsHistoryTbl);
                            document.Add(new Paragraph("\n"));
                        }


                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            if (comments.GSQuoteComment.Count > 0)
                            {
                                document.Add(new Paragraph("GS Only:", FontFactory.GetFont("Arial", 10, 0, BaseColor.BLACK)));
                                document.Add(new Paragraph("\n"));
                                document.Add(commentsTbl);
                                document.Add(new Paragraph("\n"));
                            }
                        }
                    }


                    ////Only for abbott

                    //if (quoteDetailModel.NewQuoteDetail.customername.ToLower() == "abbott")
                    //{
                    //    //disclaimer 
                    //    document.Add(new Chunk("\n"));

                    //    document.Add(new Paragraph(""));
                    //    document.Add(new Paragraph("\n"));
                    //    document.Add(disClaimerTbl);

                    //}
                    //if (quoteDetailModel.NewQuoteDetail.customername.ToLower() == "abbott")
                    //{
                    //    document.Add(logoImg);
                    //}

                    document.Close();
                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();

                    Response.Clear();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=QuoteDetails - " + quoteId + ".pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                }
                return null;

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        public ActionResult ExportQuoteDetailToPdfAsCustomer(string quoteId, string quoteType, int customerId,string customerName)//Adnan
        {
            try
            {
                var CustomerName = customerName;
                var CustomerId = customerId;
                var detail = GetQuoteDetailsByQuoteIdToExport(int.Parse(quoteId), CustomerId);//pass customer id
                var requestItems = detail.lstQuoteRequest;
                var reponseItem = detail.lstQuoteResponses;

                iTextSharp.text.Image logoImg =
                   iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                logoImg.ScaleAbsolute(250, 45);

                PdfPTable pdfTable = FillQuoteDetailPdfDetailsAsCustomer(detail, CustomerName, CustomerId);
                PdfPTable requestItemsTbl = new PdfPTable(4);
                var columnNames = new string[] { "Part Description", "Manufacturer", "SKU", "Required Quantity" };

                float[] columnsWitdh = new float[] { 50f, 20f, 20f, 10f };
                MakePdfFileDataGridHeader(requestItemsTbl, columnNames, columnsWitdh, 100);

                QuoteItemsPdfFileDataGridBody(requestItemsTbl, requestItems);


                PdfPTable responseItemsTbl = new PdfPTable(8);
                if (reponseItem.Count > 0)
                {
                    if (Session["quoteType"].ToString() == "new")
                    {

                        columnNames = new string[] { "Manufacturer", "SKU", "Description", "Category", "Lead Time", "EOL", "Quantity", "Unit Price" };
                        columnsWitdh = new float[] { 10f, 10f, 30f, 10f, 5f, 15f, 10f, 10f };
                        MakePdfFileDataGridHeader(responseItemsTbl, columnNames, columnsWitdh, 100);
                    }
                    else
                    {
                        responseItemsTbl = new PdfPTable(7);
                        columnNames = new string[] { "Manufacturer", "SKU", "Description", "Category", "EOL", "Quantity", "Unit Price" };
                        columnsWitdh = new float[] { 10f, 10f, 30f, 10f, 15f, 10f, 10f };
                        MakePdfFileDataGridHeader(responseItemsTbl, columnNames, columnsWitdh, 100);
                    }
                    QuoteResponseItemsPdfFileDataGridBody(responseItemsTbl, reponseItem);
                   
                    if (CustomerName != null && CustomerName.ToLower() == "abbvie")
                    {
                        BaseColor backgrounColor = WebColors.GetRGBColor("#eeeeee");

                        iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 9f, iTextSharp.text.Font.BOLD);

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk(""), backgrounColor));

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk(""), backgrounColor));

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk(""), backgrounColor));

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk(""), backgrounColor));

                        if (Session["quoteType"].ToString() == "new")
                            responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk(""), backgrounColor));

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk(""), backgrounColor));

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk("Total Price:", fontLabel), backgrounColor));

                        responseItemsTbl.AddCell(AddDatatoPdfCell(new Chunk((Convert.ToDecimal(detail.TotalPrice) == 0 ? "-" : (detail.lstQuoteDetail != null && detail.lstQuoteDetail.Count > 0 ? detail.lstQuoteDetail[0].currencysymbol : "") + " " + string.Format("{0:0.00}", Convert.ToDecimal(detail.TotalPrice)))), backgrounColor));
                    }

                }
                var getResellerName = "";

                if (detail != null && detail.lstQuoteDetail != null && detail.lstQuoteDetail.FirstOrDefault() != null)
                {
                    getResellerName = detail.lstQuoteDetail.FirstOrDefault().sfaname ?? "-";
                }

                PdfPTable commentsTbl = AddExportCommentsAsCustomer(detail.lstQuoteComment);

                var attachments = detail.lstQuoteAttachments.ToList();
                PdfPTable AttachmentsTbl = AddExportAttachmentsAsCustomer(attachments);




                PdfPTable disClaimerTbl = new PdfPTable(1);
                var disClaimerCol = new string[] { "" };

                float[] columnsWitdh1 = new float[] { 100f };

                RenderDisclaimerData(disClaimerTbl, disClaimerCol, columnsWitdh1, 100, getResellerName);
                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                {
                    Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();

                    if (CustomerName.ToLower() != "abbott")
                    {
                        document.Add(logoImg);
                    }
                    document.Add(new Chunk("\n"));
                    document.Add(pdfTable);
                    document.Add(new Chunk("\n"));
                    document.Add(new Paragraph("Requests"));
                    document.Add(new Paragraph("\n"));
                    document.Add(requestItemsTbl);

                    if (attachments.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(AttachmentsTbl);
                    }
                    if (reponseItem.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Responses"));
                        document.Add(new Paragraph("\n"));
                        document.Add(responseItemsTbl);
                    }
                    document.Add(new Chunk("\n"));
                    if (detail.lstQuoteComment.Count > 0 && (CustomerName.ToLower() != "abbvie"))
                    {
                        document.Add(new Paragraph("Comments"));
                        document.Add(new Paragraph("\n"));
                        document.Add(commentsTbl);
                    }

                    if (CustomerName.ToLower() == "abbott")
                    {
                        //disclaimer 
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph(""));
                        document.Add(new Paragraph("\n"));
                        document.Add(disClaimerTbl);

                    }
                    if (CustomerName.ToLower() == "abbott")
                    {
                        document.Add(logoImg);
                    }
                    document.Close();
                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();

                    Response.Clear();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=QuoteDetails.pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                }
                return null;

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public void RenderDisclaimerData(PdfPTable pdfTable, string[] columnsNames, float[] columnsWitdh, int widthPercentage, string resellerName)
        {
            pdfTable.SetWidths(columnsWitdh);
            pdfTable.WidthPercentage = widthPercentage;
            foreach (var item in columnsNames)
            {
                PdfPCell cell = new PdfPCell(new Phrase(item, new Font(null, 8f, Font.BOLD))) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                BaseColor myColor = WebColors.GetRGBColor("#fff");
                cell.BackgroundColor = myColor;
                pdfTable.AddCell(cell);
            }



            int count = 0;
            string colorVal;

            colorVal = "#FFFFFF";


            BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
            BaseColor dataGridBodyFontCOlor = WebColors.GetRGBColor("#333333");
            BaseColor fontColorRed = WebColors.GetRGBColor("#f00");
            Font dataGridBodyFOnt = new Font(null, 8, Font.NORMAL, color: dataGridBodyFontCOlor);
            Font dataGridBodyFOntBold = new Font(null, 8, Font.BOLD, color: dataGridBodyFontCOlor);
            Font fontRedAndBold = new Font(null, 8, Font.BOLD, color: fontColorRed);


            var phrase = new Phrase();

            phrase.Add(new Chunk("GlobalServe Solutions Provider - Assumptions, Terms and Conditions: \n", dataGridBodyFOntBold));

            phrase.Add(new Chunk("\n- Please note that if the order is processed, a Purchase Order and credit application will be required to process it further.", dataGridBodyFOnt));
            phrase.Add(new Chunk("\n- PO must be addressed to the “Reseller” ", dataGridBodyFOnt));
            phrase.Add(new Chunk("(" + resellerName + ").", fontRedAndBold));

            phrase.Add(new Chunk("\n- To process this order, please send Purchase Order to", dataGridBodyFOnt));
            phrase.Add(new Chunk(" abbott@global-serve.com ", fontRedAndBold));
            phrase.Add(new Chunk("and they will facilitate placement of this order with the reseller.", dataGridBodyFOnt));
            phrase.Add(new Chunk("\n- Shipping/ Delivery cost. - Will depend on the dimensions of the total shipment in order to calculate the sea freight." +
                                                                       "\n- Items are subject to availability. First come, first serve basis. " +
                                                                       "\n- OEM's Terms & Conditions apply.- Orders once processed cannot be cancelled/refunded." +
                                                                       "\n- Prices are subject to change due to fluctuations of exchange rates." +
                                                                       "\n- Delivery times may vary according to the date of placing the order." +
                                                                       "\n- In case of delivery outside the local area,shipping costs will be quoted separately. ", dataGridBodyFOnt));

            var cell1 = new PdfPCell(phrase);
            pdfTable.AddCell(cell1);

        }

        public PdfPTable AddExportAttachmentsAsCustomer(List<sp_quoteAttachement_Result> data)
        {

            PdfPTable pdfTable = new PdfPTable(1);
            var columnNames = new string[] { "Attachments" };

            float[] columnsWitdh = new float[] { 30f };

            MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


            if (data == null)
            {
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                return pdfTable;
            }

            int count = 0;
            foreach (var item in data)
            {
                string colorVal;
                if (count == 0)
                {
                    colorVal = "#eeeeee";
                    count = 1;
                }
                else
                {
                    colorVal = "#FFFFFF";
                    count = 0;
                }

                BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.daname.ToString() ?? "-"), backgrounColor));
            }
            return pdfTable;
        }

        public void QuoteResponseItemsPdfFileDataGridBody(PdfPTable pdfTable, List<sp_getQuoteResponses_R_Result> data)
        {

            if (data == null)
            {
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                return;
            }

            int count = 0;
            foreach (var item in data)
            {
                string colorVal;
                if (count == 0)
                {
                    colorVal = "#eeeeee";
                    count = 1;
                }
                else
                {
                    colorVal = "#FFFFFF";
                    count = 0;
                }

                BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.manufacturername ?? "-"), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.sku ?? "-"), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.description ?? "-"), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.partcategoryname), backgrounColor));

                if (Session["quoteType"].ToString() == "new")
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.leadtime.ToString()), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.eol.ToString()), backgrounColor));
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.quantity.ToString()), backgrounColor));
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.currencyname + " " + Convert.ToDecimal(item.price).ToString("#,##0.00")), backgrounColor));

            }

        }
        public void QuoteItemsPdfFileDataGridBody(PdfPTable pdfTable, List<sp_quoteRequest_R_Result> data)
        {

            if (data == null)
            {
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                return;
            }

            int count = 0;
            foreach (var item in data)
            {
                string colorVal;
                if (count == 0)
                {
                    colorVal = "#eeeeee";
                    count = 1;
                }
                else
                {
                    colorVal = "#FFFFFF";
                    count = 0;
                }

                BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Part_Description ?? "-"), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Manufacturer ?? "-"), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.SKU ?? "-"), backgrounColor));

                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Required_Quantity.ToString()), backgrounColor));


            }

        }
        public QuoteDetailModel GetQuoteDetailsByQuoteIdToExport(int quoteId, int customerId)
        {
            var qmodel = new QuoteDetailModel();
            var qp = new QuotePresenter();

            var lstQuoteAttachments = qp.GetQuoteAttachments(quoteId);
            var lstQuoteComment = qp.GetQuoteComments(quoteId);
            var lstQuoteDetail = qp.GetQuoteDetail(quoteId, customerId);
            var lstQuoteRequest = qp.GetQuoteRequest(quoteId).Select(x => new sp_quoteRequest_R_Result
            {
                Manufacturer = string.IsNullOrEmpty(x.Manufacturer) ? "-" : x.Manufacturer,
                Part_Description = string.IsNullOrEmpty(x.Part_Description) ? "-" : x.Part_Description,
                Required_Quantity = x.Required_Quantity,
                SKU = string.IsNullOrEmpty(x.SKU) ? "-" : x.SKU,
            }).ToList();

            if (lstQuoteDetail != null && lstQuoteDetail.Count > 0)
            {
                qmodel.lstQuoteAttachments = lstQuoteAttachments;
                qmodel.lstQuoteComment = lstQuoteComment;
                qmodel.lstQuoteDetail = lstQuoteDetail;
                qmodel.lstQuoteRequest = lstQuoteRequest;
                qmodel.QuoteId = quoteId;
                qmodel.lstQuoteResponses = qp.GetQuoteResponses(quoteId, "new");

            }
            else
            {
                qmodel.lstQuoteDetail = qp.GetOldQuoteDetail(quoteId, customerId);
                if (qmodel.lstQuoteDetail != null && qmodel.lstQuoteDetail.Count > 0)
                {
                    qmodel.lstQuoteAttachments = qp.GetOldQuoteAttachments(quoteId);
                    qmodel.lstQuoteComment = qp.GetOldQuoteComments(quoteId);
                    qmodel.lstQuoteRequest = qp.GetOldQuoteRequest(quoteId);
                    qmodel.QuoteId = quoteId;
                    qmodel.lstQuoteResponses = qp.GetQuoteResponses(quoteId, "old");
                }
            }
            return qmodel;
        }
        public PdfPTable FillQuoteDetailPdfDetailsAsCustomer(QuoteDetailModel data,string customerName,int customerId)
        {
            PdfPTable pdfTable = new PdfPTable(6);
            QuotePresenter quotePresenter = new QuotePresenter();
            float[] columnsWitdh = new float[] { 12f, 22f, 12f, 22f, 10f, 22f };
            pdfTable.SetWidths(columnsWitdh);
            pdfTable.WidthPercentage = 100;
            data.TotalPrice = quotePresenter.GetQuoteTotalPrice(data.QuoteId);
            iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
            PdfPCell cell = new PdfPCell();
            BaseColor backgrounColor = WebColors.GetRGBColor("#eeeeee");
            cell.BackgroundColor = backgrounColor;

            //pass customerId
            var paymentTerms = quotePresenter.GetPaymentTermDaysForQuote(customerId, data.lstQuoteDetail.FirstOrDefault().countryid, data.lstQuoteDetail.FirstOrDefault().SFAID);

            var resellerAddress = quotePresenter.GetResellerAddress(data.lstQuoteDetail.FirstOrDefault().countryid, data.lstQuoteDetail.FirstOrDefault().SFAID, customerName);

            iTextSharp.text.Image flagImg =
                iTextSharp.text.Image.GetInstance(
                    Server.MapPath("/Content/images_onesource/images_content/flags/flags/24/" +
                                   data.lstQuoteDetail.Select(x => x.countryid.ToString()).FirstOrDefault() +
                                   ".png"));
            flagImg.ScaleAbsolute(16, 16);

            //row 1
            //cell 1
            cell = new PdfPCell(new Phrase("Quote:", fontLabel))
            {
                Border = 0,
                BorderWidthLeft = 1,
                BorderWidthTop = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 2
            cell = new PdfPCell(new Phrase(data.QuoteId.ToString(), fontNormal))
            {
                Border = 0,
                BorderWidthTop = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 3
            cell = new PdfPCell(new Phrase("", fontLabel))
            {
                Border = 0,
                BorderWidthTop = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 4
            cell = new PdfPCell(new Phrase("", fontNormal))
            {
                Border = 0,
                BorderWidthTop = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 5
            cell = new PdfPCell(new Phrase("Total Price:", fontLabel))
            {
                Border = 0,
                BorderWidthTop = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 6
            cell = new PdfPCell(new Phrase((Convert.ToDecimal(data.TotalPrice) == 0 ? "-" : (data.lstQuoteDetail != null && data.lstQuoteDetail.Count > 0 ? data.lstQuoteDetail[0].currencysymbol : "") + " " + string.Format("{0:0.00}", Convert.ToDecimal(data.TotalPrice))), fontNormal))
            {
                Border = 0,
                BorderWidthRight = 1,
                BorderWidthTop = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //row 2
            //cell 1
            cell = new PdfPCell(new Phrase("Country:", fontLabel))
            {
                Border = 0,
                BorderWidthLeft = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 2
            cell.BackgroundColor = backgrounColor;

            Paragraph p = new Paragraph();
            p.Font = fontNormal;
            // p.Alignment = Element.ALIGN_CENTER;
            p.Add(new Chunk(flagImg, 0, 0));
            p.Add(new Phrase(" " + data.lstQuoteDetail.FirstOrDefault().countryname));
            cell.AddElement(p);
            cell.VerticalAlignment = 0;
            cell.PaddingBottom = 5;
            cell.PaddingTop = 5;
            cell.HorizontalAlignment = 1;
            cell.Border = 0;
            pdfTable.AddCell(cell);

            //cell 3
            cell = new PdfPCell(new Phrase("Currency:", fontLabel))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 4
            cell =
                new PdfPCell(new Phrase(Convert.ToString(data.lstQuoteDetail.FirstOrDefault().currencyname), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 5
            cell = new PdfPCell(new Phrase("Supplier:", fontLabel))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 6
            cell =
                new PdfPCell(new Phrase(data.lstQuoteDetail.Select(x => x.sfaname).FirstOrDefault() ?? "-", fontNormal))
                {
                    Border = 0,
                    BorderWidthRight = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);


            //row 3
            //cell 1
            cell = new PdfPCell(new Phrase("Status:", fontLabel))
            {
                Border = 0,
                BorderWidthLeft = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 2
            cell = new PdfPCell(new Phrase(data.lstQuoteDetail.FirstOrDefault().quotestatus ?? "-", fontNormal))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 3
            cell = new PdfPCell(new Phrase("Customer Reference:", fontLabel))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 4

            cell = new PdfPCell(new Phrase(data.lstQuoteDetail.FirstOrDefault().CustomerReference ?? "-", fontNormal))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 5
            cell = new PdfPCell(new Phrase("Supplier Reference:", fontLabel))
            {
                Border = 0,
                VerticalAlignment = 1,
                BorderWidthBottom = 0,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 6
            cell =
                new PdfPCell(new Phrase(
                    Convert.ToString(data.lstQuoteDetail.FirstOrDefault().SupplierReference ?? "-"), fontNormal))
                {
                    Border = 0,
                    BorderWidthRight = 1,
                    BorderWidthBottom = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);


            //row 4
            //cell 1
            cell = new PdfPCell(new Phrase("Payment Terms :", fontLabel))
            {
                Border = 0,
                BorderWidthLeft = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 2
            cell = new PdfPCell(new Phrase(paymentTerms + " day(s)", fontNormal))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 3
            cell = new PdfPCell(new Phrase("Reseller Address :", fontLabel))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 4

            cell = new PdfPCell(new Phrase(resellerAddress ?? "-", fontNormal))
            {
                Border = 0,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                BorderWidthBottom = 0,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 5
            cell = new PdfPCell(new Phrase(" ", fontLabel))
            {
                Border = 0,
                VerticalAlignment = 1,
                BorderWidthBottom = 0,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 6
            cell =
                new PdfPCell(new Phrase(
                    Convert.ToString(""), fontNormal))
                {
                    Border = 0,
                    BorderWidthRight = 1,
                    BorderWidthBottom = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);



            //row 5
            //cell 1
            cell = new PdfPCell(new Phrase("", fontLabel))
            {
                Border = 0,
                BorderWidthLeft = 1,
                BorderWidthBottom = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 2
            cell = new PdfPCell(new Phrase("", fontNormal))
            {
                Border = 0,
                BorderWidthBottom = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 3
            cell = new PdfPCell(new Phrase("", fontLabel))
            {
                Border = 0,
                BorderWidthBottom = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 4
            cell = new PdfPCell(new Phrase("", fontNormal))
            {
                Border = 0,
                BorderWidthBottom = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 5
            cell = new PdfPCell(new Phrase("", fontLabel))
            {
                Border = 0,
                BorderWidthBottom = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);

            //cell 6
            cell = new PdfPCell(new Phrase("", fontNormal))
            {
                Border = 0,
                BorderWidthRight = 1,
                BorderWidthBottom = 1,
                VerticalAlignment = 1,
                PaddingBottom = 5,
                PaddingTop = 5
            };
            cell.BackgroundColor = backgrounColor;
            pdfTable.AddCell(cell);


            return pdfTable;
        }
        public PdfPTable AddExportComments(List<sp_getQuoteGSComment_AdminR_Result> data)
        {
            try
            {
                //data.ForEach(x => x.comments = ConvertHtml(x.comments));

                PdfPTable pdfTable = new PdfPTable(4);
                var columnNames = new string[] { "Log Date", "Logged By", "Company", "Comments" };

                float[] columnsWitdh = new float[] { 20f, 20f, 10f, 30f };

                MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return pdfTable;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.logdate.ToString() ?? "-"), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.loggedby ?? "-"), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.company ?? "-"), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.comments.Trim() ?? "-"), backgrounColor));
                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        public string ConvertHtml(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            StringWriter sw = new StringWriter();
            ConvertTo(doc.DocumentNode, sw);
            sw.Flush();
            return sw.ToString();
        }
        private void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }
        public void ConvertTo(HtmlNode node, TextWriter outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    html = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(html))
                        break;

                    // check the text is meaningful and not a bunch of whitespaces
                    if (html.Trim().Length > 0)
                    {
                        outText.Write(HtmlEntity.DeEntitize(html));
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                        case "span":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }
        public PdfPTable AddExportCommentsAsCustomer(List<sp_quoteComment_R_Result> data)
        {
            data.ForEach(x => x.Comments = ConvertHtml(x.Comments));

            PdfPTable pdfTable = new PdfPTable(4);
            var columnNames = new string[] { "Log Date", "Logged By", "Company", "Comments" };

            float[] columnsWitdh = new float[] { 20f, 20f, 10f, 30f };

            MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


            if (data == null)
            {
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                return pdfTable;
            }

            int count = 0;
            foreach (var item in data)
            {
                string colorVal;
                if (count == 0)
                {
                    colorVal = "#eeeeee";
                    count = 1;
                }
                else
                {
                    colorVal = "#FFFFFF";
                    count = 0;
                }

                BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Log_Date.ToString() ?? "-"), backgrounColor));
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Logged_By ?? "-"), backgrounColor));
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Company ?? "-"), backgrounColor));
                pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Comments.Trim() ?? "-"), backgrounColor));
            }
            return pdfTable;
        }
        public PdfPTable AddExportHistoryComments(List<sp_getQuoteHistory_AdminR_Result> data)
        {
            try
            {
                //data.ForEach(x => x.comments = ConvertHtml(x.comments));

                PdfPTable pdfTable = new PdfPTable(4);
                var columnNames = new string[] { "Log Date", "Logged By", "Company", "Comments" };

                float[] columnsWitdh = new float[] { 20f, 20f, 10f, 30f };

                MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return pdfTable;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.logdate.ToString() ?? "-"), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.loggedby ?? "-"), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.company ?? "-"), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.comments.Trim() ?? "-"), backgrounColor));
                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }


        public PdfPTable AddExportAttachments(List<sp_getQuoteAttachments_AdminR_Result> data)
        {

            try
            {
                PdfPTable pdfTable = new PdfPTable(1);
                var columnNames = new string[] { "File name" };

                float[] columnsWitdh = new float[] { 30f };

                MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return pdfTable;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.daname.ToString() ?? "-"), backgrounColor));
                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public void QuoteResponseItemsPdfFileDataGridBody(PdfPTable pdfTable, List<sp_getQuoteResponse_New_AdminR_Result> data, string currencySymbol, string quoteType)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.manufacturername ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.sku ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.description ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.partcategoryname), backgrounColor));

                    if (quoteType == "new")
                        pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.leadtime.ToString()), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.eol.ToString()), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.quantity.ToString()), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(currencySymbol + " " + Convert.ToDecimal(item.price).ToString("#,##0.00")), backgrounColor));

                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }

        }
        public void QuoteResponseItemsPdfFileDataGridBodySFA(PdfPTable pdfTable, List<sp_getQuoteResponse_New_SFA_AdminR_Result> data, string currencySymbol, string quoteType)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.manufacturername ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.sku ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.description ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.partcategoryname), backgrounColor));

                    if (quoteType == "new")
                        pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.leadtime.ToString()), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.eol.ToString()), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.quantity.ToString()), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(currencySymbol + " " + Convert.ToDecimal(item.price).ToString("#,##0.00")), backgrounColor));

                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }

        }
        public void QuoteResponseItemsPdfFileDataGridBodyOld(PdfPTable pdfTable, List<sp_getQuoteResponse_Old_AdminR_Result> data, string currencySymbol, string quoteType)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.manufacturername ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.sku ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.description ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.partcategoryname), backgrounColor));

                    if (quoteType == "new")
                        pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.leadtime.ToString()), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.eol.ToString()), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.quantity.ToString()), backgrounColor));
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(currencySymbol + " " + Convert.ToDecimal(item.price).ToString("#,##0.00")), backgrounColor));

                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }

        }
        public static PdfPCell AddDatatoPdfCell(Chunk pdfChunk, BaseColor backgrounColor)
        {
            BaseColor dataGridBodyFontCOlor = WebColors.GetRGBColor("#333333");
            Font dataGridBodyFOnt = new Font(null, 8, Font.NORMAL, color: dataGridBodyFontCOlor);
            BaseColor dataBodyborderColor = WebColors.GetRGBColor("#ddd");

            var cell =
                new PdfPCell(new Paragraph(new Chunk(pdfChunk.ToString()) { Font = dataGridBodyFOnt }) { Font = dataGridBodyFOnt })
                {
                    BorderColor = dataBodyborderColor,
                    BorderWidthLeft = 0,
                    BorderWidthRight = 0,
                    BackgroundColor = backgrounColor,
                    BorderWidthTop = 0,
                    PaddingTop = 4,
                    PaddingBottom = 4

                };

            return cell;
        }
        public void QuoteItemsPdfFileDataGridBody(PdfPTable pdfTable, List<QuoteRequestModel> data)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Description ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Manufacturer ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.SKU ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Quantity.ToString()), backgrounColor));


                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }
        public void QuoteItemsPdfFileDataGridBodyOld(PdfPTable pdfTable, List<tblPartQuoteRequest> data)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Description ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Manufacturer ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.SKU ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Quantity.ToString()), backgrounColor));


                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }
        public static void MakePdfFileDataGridHeader(PdfPTable pdfTable, string[] columnsNames, float[] columnsWitdh, int widthPercentage)
        {
            pdfTable.SetWidths(columnsWitdh);
            pdfTable.WidthPercentage = widthPercentage;
            foreach (var item in columnsNames)
            {
                PdfPCell cell = new PdfPCell(new Phrase(item, new Font(null, 8f, Font.BOLD))) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                BaseColor myColor = WebColors.GetRGBColor("#3B9AD2");
                cell.BackgroundColor = myColor;
                pdfTable.AddCell(cell);
            }

        }

        public PdfPTable FillQuoteDetailPdfDetails(QuoteDetailModel data)
        {
            try
            {
                PdfPTable pdfTable = new PdfPTable(6);

                float[] columnsWitdh = new float[] { 12f, 22f, 12f, 22f, 10f, 22f };
                pdfTable.SetWidths(columnsWitdh);
                pdfTable.WidthPercentage = 100;
                iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
                PdfPCell cell = new PdfPCell();
                BaseColor backgrounColor = WebColors.GetRGBColor("#eeeeee");
                cell.BackgroundColor = backgrounColor;


                if (data.NewQuoteDetail != null)
                {
                    var paymentTerms = osp.GetPaymentTermDaysForQuote((int)data.NewQuoteDetail.customerid, (int)data.NewQuoteDetail.countryid, (int)data.NewQuoteDetail.SFAID);

                    var resellerAddress = osp.GetResellerAddress((int)data.NewQuoteDetail.countryid, (int)data.NewQuoteDetail.SFAID);

                    //row 1
                    //cell 1
                    cell = new PdfPCell(new Phrase("Quote:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.NewQuoteDetail.quoteid.ToString(), fontNormal))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("Currency", fontLabel))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4
                    cell = new PdfPCell(new Phrase(data.NewQuoteDetail.currencyname.ToString(), fontNormal))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("Total Price:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell = new PdfPCell(new Phrase((Convert.ToDecimal(data.TotalPrice) == 0 ? "-" : data.NewQuoteDetail.Symbol + " " + string.Format("{0:0.00}", Convert.ToDecimal(data.TotalPrice))), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //row 2
                    //cell 1
                    cell = new PdfPCell(new Phrase("Country:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.NewQuoteDetail.countryname ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("Customer Reference:", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4

                    cell = new PdfPCell(new Phrase(data.NewQuoteDetail.CustomerReference ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("Supplier:", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell =
                        new PdfPCell(new Phrase(data.NewQuoteDetail.sfaname ?? "-", fontNormal))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            VerticalAlignment = 1,
                            PaddingBottom = 5,
                            PaddingTop = 5
                        };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                    //row 3
                    //cell 1
                    cell = new PdfPCell(new Phrase("Status:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.NewQuoteDetail.quotestatus ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("Reseller Address :", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4

                    cell = new PdfPCell(new Phrase(resellerAddress ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("Supplier Reference:", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        BorderWidthBottom = 0,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell =
                        new PdfPCell(new Phrase(
                            Convert.ToString(data.SupplierRef ?? "-"), fontNormal))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthBottom = 0,
                            VerticalAlignment = 1,
                            PaddingBottom = 5,
                            PaddingTop = 5
                        };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                    //row 4
                    //cell 1
                    cell = new PdfPCell(new Phrase("Payment Terms :", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(paymentTerms + " day(s)", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                    //cell 3
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);
                    //cell 5
                    cell = new PdfPCell(new Phrase(" ", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        BorderWidthBottom = 0,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell =
                        new PdfPCell(new Phrase(
                            Convert.ToString(""), fontNormal))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthBottom = 0,
                            VerticalAlignment = 1,
                            PaddingBottom = 5,
                            PaddingTop = 5
                        };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);



                    //row 5
                    //cell 1
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                }
                else
                {
                    var paymentTerms = osp.GetPaymentTermDaysForQuote((int)data.OldQuoteDetail.customerid, (int)data.OldQuoteDetail.countryid, (int)data.OldQuoteDetail.SFAID);

                    var resellerAddress = osp.GetResellerAddress((int)data.OldQuoteDetail.countryid, (int)data.OldQuoteDetail.SFAID);

                    //row 1
                    //cell 1
                    cell = new PdfPCell(new Phrase("Quote:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.OldQuoteDetail.partquoteid.ToString(), fontNormal))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("Currency", fontLabel))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4
                    cell = new PdfPCell(new Phrase(data.OldQuoteDetail.currencyname.ToString(), fontNormal))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("Total Price:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell = new PdfPCell(new Phrase((Convert.ToDecimal(data.TotalPrice) == 0 ? "-" : data.OldQuoteDetail.currencysymbol + " " + string.Format("{0:0.00}", Convert.ToDecimal(data.TotalPrice))), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthTop = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //row 2
                    //cell 1
                    cell = new PdfPCell(new Phrase("Country:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.OldQuoteDetail.countryname ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("Customer Reference:", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4

                    cell = new PdfPCell(new Phrase(data.OldQuoteDetail.customerref ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("Supplier:", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell =
                        new PdfPCell(new Phrase(data.OldQuoteDetail.sfaname ?? "-", fontNormal))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            VerticalAlignment = 1,
                            PaddingBottom = 5,
                            PaddingTop = 5
                        };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                    //row 3
                    //cell 1
                    cell = new PdfPCell(new Phrase("Status:", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(data.OldQuoteDetail.partquotestatus ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("Reseller Address :", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4

                    cell = new PdfPCell(new Phrase(resellerAddress ?? "-", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("Supplier Reference:", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        BorderWidthBottom = 0,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell =
                        new PdfPCell(new Phrase(
                            Convert.ToString(data.SupplierRef ?? "-"), fontNormal))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthBottom = 0,
                            VerticalAlignment = 1,
                            PaddingBottom = 5,
                            PaddingTop = 5
                        };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                    //row 4
                    //cell 1
                    cell = new PdfPCell(new Phrase("Payment Terms :", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase(paymentTerms + " day(s)", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        BorderWidthBottom = 0,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                    //cell 3
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);
                    //cell 5
                    cell = new PdfPCell(new Phrase(" ", fontLabel))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        BorderWidthBottom = 0,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell =
                        new PdfPCell(new Phrase(
                            Convert.ToString(""), fontNormal))
                        {
                            Border = 0,
                            BorderWidthRight = 1,
                            BorderWidthBottom = 0,
                            VerticalAlignment = 1,
                            PaddingBottom = 5,
                            PaddingTop = 5
                        };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);



                    //row 5
                    //cell 1
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        BorderWidthLeft = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 2
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 3
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 4
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 5
                    cell = new PdfPCell(new Phrase("", fontLabel))
                    {
                        Border = 0,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);

                    //cell 6
                    cell = new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                    cell.BackgroundColor = backgrounColor;
                    pdfTable.AddCell(cell);


                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        #region Bundle Details (SUNNY GOHAR)

        /// <summary>
        /// Get Bundle Details to show popup on quote detail page
        /// </summary>
        /// <param name="catalogStandardId"></param>
        /// <param name="partCategoryId"></param>
        /// <returns></returns>
        public JsonResult GetBundleDetail(int catalogStandardId, int partCategoryId)
        {
            try
            {
                var settings = (BundleSettings)Session["BundleSettings"];
                var bundleImageRootUrl = ConfigurationManager.AppSettings["Bundle_ImageURL"].ToString();
                var bundle = osp.GetBundleDetail(partCategoryId, catalogStandardId, settings, bundleImageRootUrl);
                return Json(bundle, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion
    
    }
}



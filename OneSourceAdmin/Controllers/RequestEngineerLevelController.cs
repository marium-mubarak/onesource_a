﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Request Engineer level detail"
    /// </summary>
    public class RequestEngineerLevelController : BaseController
    {
        /// <summary>
        /// This Action is responsible for providing index view of Request Engineer level
        /// </summary>
        /// <returns></returns>

           
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    RequestEngineerLevelPresenter relp = new RequestEngineerLevelPresenter();
                    RequestEngineerLevelModel model = new RequestEngineerLevelModel
                    {
                        Customers = cp.GetCustomersWithPriority(),
                        EngineeringLevelData = relp.GetEngineerLevel()
                    };
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("RequestEngineerSettings");
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for deleting Request Engineer level record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int engineerlevelmappingId;
                    if (!int.TryParse(id, out engineerlevelmappingId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    RequestEngineerLevelPresenter cp = new RequestEngineerLevelPresenter();
                 
                    int count = cp.Delete(engineerlevelmappingId);
                    if (count > 0) TempData["Message"] = "Record has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This Action is responsible for providing Priorities and Requests of customer in Json
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public JsonResult GetPriorityRequestType(int customerId,int companyId)
        {
            RequestEngineerLevelPresenter relp = new RequestEngineerLevelPresenter();
            var requesttypes = relp.GetRequestsTypesListByCompanyId(companyId);
            var priorities = relp.GetPriorityListByCompanyId(companyId);
            var data = new
            {
                priorities,
                requesttypes
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// This Action is responsible for updating request engineer level
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult UpdateRequestEngineerLevel(tblEngineerLevelMapping model)
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    RequestEngineerLevelPresenter cp = new RequestEngineerLevelPresenter();
                    int count;
                    //For Edit
                    if (model.EngineerLevelMappingId != 0)
                    {
                        count = cp.EditRequestEngineerLevel(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Request Engineer Level has been updated";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddRequestEngineerLevel(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Request Engineer Level has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        //This Action is responsible for checking the valid data from posted model to update action
        public bool ValidateFields(tblEngineerLevelMapping model)
        {
           
            if (model.PriorityId == 0)
            {
                TempData["Message"] = "Please provide priority";
                return false;
            }
            if (model.RequestId == 0)
            {
                TempData["Message"] = "Please provide request type";
                return false;
            }

            return true;
        }
    }
}
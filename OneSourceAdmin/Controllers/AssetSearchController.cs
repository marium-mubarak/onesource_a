﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using OneSourceAdminCore.Entity;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using OneSourceAdmin.Models;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : July 12 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing asset tracking detail"
    /// </summary>
    public class AssetSearchController : BaseController
    {
        private AssetSearchPresenter asp;
        public AssetSearchController()
        {
            asp = new AssetSearchPresenter();
        }

        // GET: AssetSearch
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This action is responsible for providing asset search view
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult AssetSearch(int customerId)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    AssetSearchModel model = asp.GetAssetSearchModel("ISD", customerId, string.Empty, Session["UserID"].ToString());
                    return View("~/Views/AssetSearch/AssetSearchISD.cshtml", model);
                }
                if (Session["UserGroupName"].ToString() == "SFA")
                {
                    AssetSearchModel model = asp.GetAssetSearchModel("SFA", customerId, Session["SFAID"].ToString(), Session["UserID"].ToString());
                    return View("~/Views/AssetSearch/AssetSearchSFA.cshtml", model);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for asset Search request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AssetSearchRequest(AssetSearchModel model)
        {
            try
            {
                Session["AssetSearchModel"] = model;

                //if (Session["UserGroupName"].ToString() == "ISD")
                //{
                //    var data = asp.GetSearchedAssets(model, Session["UserGroupName"].ToString(), "", Session["UserID"].ToString(), 500);
                //    if (data.LstAsset.Count == 0)
                //    {
                //        TempData["Message"] = "No record found";
                //        return RedirectToAction("AssetSearch", new { customerId = model.CustomerId });
                //    }
                //    Session["SearchAssetResult"] = data;
                //}
                //else if (Session["UserGroupName"].ToString() == "SFA")
                //{
                //    var data = asp.GetSearchedAssets(model, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), 500);
                //    if (data.LstAsset.Count == 0)
                //    {
                //        TempData["Message"] = "No record found";
                //        return RedirectToAction("AssetSearch", new { customerId = model.CustomerId });
                //    }
                //    Session["SearchAssetResult"] = data;
                //}
                return RedirectToAction("SearchAssetList");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for providing asset list view
        /// </summary>
        /// <param name="requiredRecord"></param>
        /// <param name="requiredPageSize"></param>
        /// <param name="AssetIdToDelFromList"></param>
        /// <returns></returns>
        public ActionResult SearchAssetList(int requiredRecord, int requiredPageSize, int AssetIdToDelFromList)
        {
            try
            {
                if (requiredPageSize > requiredRecord)
                {
                    requiredPageSize = 50;
                    requiredRecord = 500;
                }

                ViewBag.PageSize = requiredPageSize;
                ViewBag.RequiredRecord = requiredRecord;

                if (requiredRecord > 5000)
                {
                    requiredRecord = 5000;
                    ViewBag.RequiredRecord = 5000;
                }
                if (requiredPageSize > 1000)
                {
                    ViewBag.PageSize = 1000;
                    requiredPageSize = 1000;
                }

                AssetListModel model = new AssetListModel();

                var searchModel = new AssetSearchModel();

                if (Session["AssetSearchModel"] == null)
                {
                    TempData["Message"] = "No record found";
                    return RedirectToAction("AssetSearch");
                }

                searchModel = (AssetSearchModel)Session["AssetSearchModel"];


                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    var data = asp.GetSearchedAssets(searchModel, Session["UserGroupName"].ToString(), "", Session["UserID"].ToString(), 500);
                    if (data.LstAsset.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("AssetSearch", new { customerId = searchModel.CustomerId });
                    }
                    model = data;
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    var data = asp.GetSearchedAssets(searchModel, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), 500);
                    if (data.LstAsset.Count == 0)
                    {
                        TempData["Message"] = "No record found";
                        return RedirectToAction("AssetSearch", new { customerId = searchModel.CustomerId });
                    }
                    model = data;
                }


                if (AssetIdToDelFromList > 0)
                {
                    var item = model.LstAsset.FirstOrDefault(x => x.assetid == AssetIdToDelFromList);
                    model.LstAsset.Remove(item);
                }

                if (model.LstAsset.Count != requiredRecord)
                {
                    if (requiredRecord == 50001)
                    {
                        ViewBag.RequiredRecord = 50001;
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            var smodel = (AssetSearchModel)Session["AssetSearchModel"];
                            model = asp.GetSearchedAssets(smodel, Session["UserGroupName"].ToString(), "", Session["UserID"].ToString(), model.TotalRecord);
                        }
                        else if (Session["UserGroupName"].ToString() == "SFA")
                        {
                            var smodel = (AssetSearchModel)Session["AssetSearchModel"];
                            model = asp.GetSearchedAssets(smodel, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), model.TotalRecord);
                        }
                    }
                    else
                    {
                        if (Session["UserGroupName"].ToString() == "ISD")
                        {
                            var smodel = (AssetSearchModel)Session["AssetSearchModel"];
                            model = asp.GetSearchedAssets(smodel, Session["UserGroupName"].ToString(), "", Session["UserID"].ToString(), requiredRecord);
                        }
                        else if (Session["UserGroupName"].ToString() == "SFA")
                        {
                            var smodel = (AssetSearchModel)Session["AssetSearchModel"];
                            model = asp.GetSearchedAssets(smodel, Session["UserGroupName"].ToString(), Session["SFAID"].ToString(), Session["UserID"].ToString(), requiredRecord);

                        }

                    }
                    //Session["SearchAssetResult"] = model;
                }

                //sunny gohar on 18th Jan 2018 ======== For restricting bulk upload option to Customer Demo & Mars ParentCompanies =======
                Session["ParentCompanyName"] = asp.GetParentCompanyNameByParentCompanyId(searchModel.CustomerId);
                //=========================================================================================================================

                ViewBag.TotalCount = model.TotalRecord;
                return View("~/Views/AssetSearch/AssetList.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for asset detail view 
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public ActionResult AssetDetail(int assetId)
        {
            try
            {

                Session["assetId"] = assetId;
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    ViewBag.Message = TempData["Message"];
                    var detailModel = asp.GetAssetDetail(assetId, "ISD", 0);
                    if (Session["FDetailModel"] != null)
                    {
                        var data = (sp_getAssetDetail_AdminR_Result)Session["FDetailModel"];
                        detailModel.AssetDetail = data;
                        Session["FDetailModel"] = null;
                    }

                    return View("~/Views/AssetSearch/AssetDetail.cshtml", detailModel);
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    ViewBag.Message = TempData["Message"];
                    var detailModel = asp.GetAssetDetail(assetId, "SFA", Convert.ToInt32(Session["SFAID"]));

                    if (Session["FDetailModel"] != null)
                    {
                        var data = (sp_getAssetDetail_AdminR_Result)Session["FDetailModel"];
                        detailModel.AssetDetail = data;
                        Session["FDetailModel"] = null;
                    }

                    return View("~/Views/AssetSearch/AssetDetail.cshtml", detailModel);
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for updating asset detail
        /// </summary>
        /// <param name="detailModel"></param>
        /// <param name="AssetId"></param>
        /// <returns></returns>
        public ActionResult UpdateAsset(sp_getAssetDetail_AdminR_Result detailModel, int AssetId)
        {
            try
            {

                GenericList result = asp.UpdateAsset(detailModel, AssetId, Convert.ToInt32(Session["UserID"]));
                if (result.Key == -1 || result.Key == -2)
                {
                    Session["FDetailModel"] = detailModel;
                    TempData["Message"] = result.Value;
                }
                else if (result.Key > 0)
                {
                    Session["FDetailModel"] = null;
                    if (Session["SearchAssetResult"] != null)
                    {
                        var data = (AssetListModel)Session["SearchAssetResult"];

                        var manufacturerName = asp.getMfgNameById(detailModel.manufacturerid ?? 0);
                        var categoryName = asp.getCategoryNameById(detailModel.customerpartcategoryid ?? 0);
                        var locationName = asp.getLocationNameById(detailModel.customeraddressid ?? 0);

                        data.LstAsset.Where(x => x.assetid == AssetId).ToList().ForEach(x =>
                        {
                            x.assettypeid = detailModel.SKU;
                            x.description = detailModel.description;
                            x.serialnum = detailModel.serialnum;
                            x.assettagnum = detailModel.assettagnum;
                            x.manufacturer = manufacturerName;
                            x.partcategoryname = categoryName;
                            x.condensedaddress = locationName;
                        });
                    }
                    TempData["Message"] = "Record updated successfully";
                }

                return RedirectToAction("AssetDetail", new { assetId = AssetId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for getting asset history
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAssetHistory()
        {
            try
            {
                var model = asp.GetAssetHistory(Convert.ToInt32(Session["assetId"]));
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    return PartialView("~/Views/AssetSearch/ISD/_AssetHistory.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/AssetSearch/SFA/_AssetHistory.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for updating asset log
        /// </summary>
        /// <param name="assetLogID"></param>
        /// <param name="isPublic"></param>
        /// <returns></returns>
        public JsonResult UpdateAssetLog(int assetLogID, bool isPublic)
        {
            int count = asp.UpdateAssetLog(assetLogID, Convert.ToInt32(Session["UserID"]), isPublic);
            return Json(count > 0 ? 1 : count, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is responsible for updating asset comments
        /// </summary>
        /// <param name="hcomments"></param>
        /// <returns></returns>
        public ActionResult UpdateAssetComment(string hcomments)
        {
            try
            {
                if (!string.IsNullOrEmpty(hcomments))
                {
                    int count = asp.UpdateAssetComment(Convert.ToInt32(Session["assetId"]), Convert.ToInt32(Session["UserID"]), hcomments);
                    TempData["Message"] = count > 0 ? "Comments has been added successfully" : "No comments has been added";
                }
                else
                {
                    TempData["Message"] = "Please provide comments.";
                }

                return RedirectToAction("AssetDetail", new { assetId = Convert.ToInt32(Session["assetId"]) });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for getting asset attachments
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAssetAttachment()
        {
            try
            {
                var model = asp.GetAssetAttachment(Convert.ToInt32(Session["assetId"]));
                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    return PartialView("~/Views/AssetSearch/ISD/_AssetAttachment.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/AssetSearch/SFA/_AssetAttachment.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This action is responsible for marking asset attachment Isactive false
        /// </summary>
        /// <returns></returns>
        public JsonResult MarkIsActiveAssetAttachment(int daid)//Adnan 19-june-2019
        {
            if (daid > 0)
            {
                try
                {
                    int count = 0;

                    count = asp.MarkIsActiveAssetAttachment(daid, Convert.ToInt32(Session["UserID"]));

                    return Json(count > 0 ? 1 : -1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is responsible for adding asset attachment
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="attachFile"></param>
        /// <returns></returns>
        public JsonResult AddAssetAttachment(int assetId, HttpPostedFileBase attachFile)
        {
            var msg = string.Empty;
            var isSuccess = false;
            try
            {
                if (attachFile != null && attachFile.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(attachFile.FileName).Replace("^", "");
                    var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                    string path = string.Empty;
                    if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" || fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" || fileExtension == ".gif" || fileExtension == ".csv" || fileExtension == ".png" || fileExtension == ".txt")
                    {
                        if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                        {
                            var docId = System.Guid.NewGuid().ToString();
                            path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), docId + "^" + fileName);
                            attachFile.SaveAs(path);
                            int? count = asp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path, ConfigurationManager.AppSettings["UploadURL"] + "/" + docId + "^" + fileName, Convert.ToInt32(Session["UserID"]), assetId);
                            if (count <= 0)
                            {
                                msg = "File Attaching Error";
                            }
                            else
                            {
                                msg = "Attachment has been added successfully";
                            }

                            isSuccess = true;
                        }
                        else
                        {
                            msg = "Invalid file size. Maximum allowed size is 10MB.";
                        }
                    }
                    else
                    {
                        msg = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";
                    }
                }
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = "Error Occured";
                isSuccess = false;
                return Json(new { msg, isSuccess }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CheckTicket(int ticketId)
        {
            return Json(asp.CheckTicket(ticketId), JsonRequestBehavior.AllowGet);
        }

        //sunny gohar on 16th Jan 2018 =============== Update Asset Detail Bulk Upload =====================
        public JsonResult UploadAssetDetail()
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    var fileName = file.FileName;
                    if (fileName.Contains('\\'))
                    {
                        fileName = fileName.Split('\\')[fileName.Split('\\').Length - 1];
                    }
                    string filePath = string.Format("{0}{1}{2}", Server.MapPath("~/uploads/"),
                        System.Guid.NewGuid().ToString(), "^" + fileName);
                    file.SaveAs(filePath);

                    var count = GetExcelFileDataForAssetDetailsAndUpdate(filePath);

                    return Json(new { count, errorList = ViewBag.ErrorList }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Aurangzeb Khan
        /// 6th September 2018  
        /// This function is used for exporting asset details to pdf.
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public ActionResult ExportAssetDetailToPdf(string assetId)
        {
            try
            {
                var detailModel = new AssetDetailModel();

                if (Session["UserGroupName"].ToString() == "ISD")
                {
                    detailModel = asp.GetAssetDetail(Convert.ToInt32(assetId), "ISD", 0);
                }
                else if (Session["UserGroupName"].ToString() == "SFA")
                {
                    detailModel = asp.GetAssetDetail(Convert.ToInt32(assetId), "SFA", Convert.ToInt32(Session["SFAID"]));
                }

                iTextSharp.text.Image logoImg =
                      iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                logoImg.ScaleAbsolute(250, 45);

                PdfPTable pdfTable = FillAssetDetailPdfDetails(detailModel);

                var attachments = asp.GetAssetAttachment(Convert.ToInt32(assetId));
                PdfPTable AttachmentsTbl = AddExportAttachments(attachments);

                var assetItems = detailModel.PartsPeripherals;
                PdfPTable assetItemsTbl = new PdfPTable(6);
                var columnNames = new string[] { "Category", "Manufacturer", "Model", "Description", "Serial No", "Asset" };
                float[] columnsWitdh = new float[] { 15f, 15f, 15f, 25f, 15f, 15f };
                MakePdfFileDataGridHeader(assetItemsTbl, columnNames, columnsWitdh, 100);
                AssetItemsPdfFileDataGridBody(assetItemsTbl, assetItems);

                var historyList = asp.GetAssetHistory(Convert.ToInt32(assetId));
                PdfPTable historyTbl = new PdfPTable(5);
                var columnNames1 = new string[] { "Log Date", "Updated By", "Event", "Status", "Comment" };
                float[] columnsWitdh1 = new float[] { 20f, 20f, 20f, 10f, 30f };
                MakePdfFileDataGridHeader(historyTbl, columnNames1, columnsWitdh1, 100);
                AssetHistoryPdfFileDataGridBody(historyTbl, historyList);

                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                {
                    Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();
                    document.Add(logoImg);

                    document.Add(new Chunk("\n"));
                    document.Add(pdfTable);

                    if (attachments.Count > 0)
                    {

                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Attachments:"));
                        document.Add(new Paragraph("\n"));
                        document.Add(AttachmentsTbl); 
                    }


                    if (assetItems.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("Parts/Peripherals:"));
                        document.Add(new Paragraph("\n"));
                        document.Add(assetItemsTbl);

                    }

                    if (historyList.Count > 0)
                    {
                        document.Add(new Chunk("\n"));
                        document.Add(new Paragraph("History:"));
                        document.Add(new Paragraph("\n"));
                        document.Add(historyTbl); 
                    }

                    document.Close();
                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();

                    Response.Clear();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=AssetDetails - " + assetId + ".pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                }

                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public PdfPTable AddExportAttachments(List<sp_GetAttachments_R_Result> data)
        {
            try
            {
                PdfPTable pdfTable = new PdfPTable(1);
                var columnNames = new string[] { "File name" };

                float[] columnsWitdh = new float[] { 30f };

                MakePdfFileDataGridHeader(pdfTable, columnNames, columnsWitdh, 100);


                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return pdfTable;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.daname.ToString() ?? "-"), backgrounColor));
                }
                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }


        public void AssetHistoryPdfFileDataGridBody(PdfPTable pdfTable, List<AssetHistory> data)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Logdate.ToString() ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.UserName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.ActionName ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Status ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Comments ?? "-"), backgrounColor));


                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }

        }

        public void AssetItemsPdfFileDataGridBody(PdfPTable pdfTable, List<AssetPartsPeripherals> data)
        {

            try
            {
                if (data == null)
                {
                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk("No Record Found."), BaseColor.WHITE));
                    return;
                }

                int count = 0;
                foreach (var item in data)
                {
                    string colorVal;
                    if (count == 0)
                    {
                        colorVal = "#eeeeee";
                        count = 1;
                    }
                    else
                    {
                        colorVal = "#FFFFFF";
                        count = 0;
                    }

                    BaseColor backgrounColor = WebColors.GetRGBColor(colorVal);

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Category ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Manufacturer ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Model ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Description ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.Serial ?? "-"), backgrounColor));

                    pdfTable.AddCell(AddDatatoPdfCell(new Chunk(item.AssetTagNum ?? "-"), backgrounColor));


                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }

        }

        public static void MakePdfFileDataGridHeader(PdfPTable pdfTable, string[] columnsNames, float[] columnsWitdh, int widthPercentage)
        {
            pdfTable.SetWidths(columnsWitdh);
            pdfTable.WidthPercentage = widthPercentage;
            foreach (var item in columnsNames)
            {
                PdfPCell cell = new PdfPCell(new Phrase(item, new Font(null, 8f, Font.BOLD))) { Border = 0, VerticalAlignment = 1, PaddingBottom = 5, PaddingTop = 5 };
                BaseColor myColor = WebColors.GetRGBColor("#3B9AD2");
                cell.BackgroundColor = myColor;
                pdfTable.AddCell(cell);
            }

        }

        public static PdfPCell AddDatatoPdfCell(Chunk pdfChunk, BaseColor backgrounColor)
        {
            BaseColor dataGridBodyFontCOlor = WebColors.GetRGBColor("#333333");
            Font dataGridBodyFOnt = new Font(null, 8, Font.NORMAL, color: dataGridBodyFontCOlor);
            BaseColor dataBodyborderColor = WebColors.GetRGBColor("#ddd");

            var cell =
                new PdfPCell(new Paragraph(new Chunk(pdfChunk.ToString()) { Font = dataGridBodyFOnt }) { Font = dataGridBodyFOnt })
                {
                    BorderColor = dataBodyborderColor,
                    BorderWidthLeft = 0,
                    BorderWidthRight = 0,
                    BackgroundColor = backgrounColor,
                    BorderWidthTop = 0,
                    PaddingTop = 4,
                    PaddingBottom = 4

                };

            return cell;
        }
        public PdfPTable FillAssetDetailPdfDetails(AssetDetailModel data)
        {
            try
            {
                PdfPTable pdfTable = new PdfPTable(6);

                float[] columnsWitdh = new float[] { 12f, 22f, 12f, 22f, 10f, 22f };
                pdfTable.SetWidths(columnsWitdh);
                pdfTable.WidthPercentage = 100;

                Font fontLabel = new Font(null, 8f, Font.BOLD);
                Font fontNormal = new Font(null, 8f);
                PdfPCell cell = new PdfPCell();
                BaseColor backgrounColor = WebColors.GetRGBColor("#eeeeee");
                cell.BackgroundColor = backgrounColor;

                //row 1
                //cell 1
                cell = new PdfPCell(new Phrase("Customer:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.AssetDetail.customername ?? "-", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5,
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Description:", fontLabel))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                cell = new PdfPCell(new Phrase(data.AssetDetail.description ?? "-", fontNormal))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Manufacturer:", fontLabel))
                {
                    Border = 0,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                var manufacturerName = "-";
                var chkManufacturer = data.Manufacturers.FirstOrDefault(x => x.Key == data.AssetDetail.manufacturerid);
                if (chkManufacturer != null)
                {
                    manufacturerName = chkManufacturer.Value;
                }
                cell = new PdfPCell(new Phrase(manufacturerName, fontNormal))
                {
                    Border = 0,
                    BorderWidthRight = 1,
                    BorderWidthTop = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 2
                //cell 1
                cell = new PdfPCell(new Phrase("Part #:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.AssetDetail.SKU ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Category:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4
                //var catName = "-";
                //var chkCategory = data.PartCategoryList.FirstOrDefault(x => x.PartCategoryID == data.SearchAssetDetail.CustomerPartCategoryID);
                //if (chkCategory != null)
                //{
                //    catName = chkCategory.PartCategoryName;
                //}

                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.Category, fontNormal))
                    {
                        Border = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Status:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                //var statusName = "-";
                //var chkStatus = data.Statuses.FirstOrDefault(x => x.Key == data.AssetDetail.status);
                //if (chkStatus != null)
                //{
                //    statusName = chkStatus.status;
                //}
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.status, fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 3
                //cell 1
                cell = new PdfPCell(new Phrase("Serial #:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.AssetDetail.serialnum ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Model:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4


                cell = new PdfPCell(new Phrase(data.AssetDetail.model ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Location:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6

                var addressData = data.AssetDetail;
                var add2 = (!string.IsNullOrWhiteSpace(addressData.address2) ? addressData.address2 + "\n" : "");
                var region = (!string.IsNullOrWhiteSpace(addressData.stateprovinceregion) ? addressData.stateprovinceregion + "\n" : "");
                var postalcode = (!string.IsNullOrWhiteSpace(addressData.postalcode) ? addressData.postalcode + "\n" : "");
                var city = (!string.IsNullOrWhiteSpace(addressData.city) ? addressData.city + "\n" : "");
                var country = (!string.IsNullOrWhiteSpace(addressData.countryname) ? addressData.countryname + "\n" : "");

                cell =
                    new PdfPCell(new Phrase(addressData.customeraddressname + "\n" + addressData.address1 + "\n"
                        + add2 + city + region + postalcode + country, fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 4
                //cell 1
                cell = new PdfPCell(new Phrase("Supported By:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.AssetDetail.sfaname ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Current Owner:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4


                var owner = "-";
                if (data.AssetDetail.currentownerid != null)
                {
                    var chkowner = data.CurrentOwnerList.Where(x => x.Key == data.AssetDetail.currentownerid).FirstOrDefault();
                    owner = chkowner.Value;
                }
                cell = new PdfPCell(new Phrase(owner, fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //cell 5
                cell = new PdfPCell(new Phrase("Warranty:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell = new PdfPCell(new Phrase(data.AssetDetail.warrantytypename ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthRight = 1,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 5
                //cell 1
                cell = new PdfPCell(new Phrase("Warranty Start Date:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(Convert.ToDateTime(data.AssetDetail.warrantystartdate).ToString() != "" ? Convert.ToDateTime(data.AssetDetail.warrantystartdate).ToString() : "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Cost Center:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.costcenter ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Old Asset Name:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.OldAssetName ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 6
                //cell 1
                cell = new PdfPCell(new Phrase("Old Asset Model tag:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase((data.AssetDetail.OldAssetTag ?? "-"), fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Globalserve Imaging:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.Imaging.HasValue ? "Yes" : "No", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("White Glove Delivery:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.WhiteGloveDelivery.HasValue ? "Yes" : "No"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 7
                //cell 1
                cell = new PdfPCell(new Phrase("Asset Rental Amount:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.AssetDetail.AssetRentalAmount.ToString() != "" ? data.AssetDetail.AssetRentalAmount.ToString() : "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Asset Model:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.OldAssetModel ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Old Asset Model Number:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.OldAssetModelNumber ?? "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 8
                //cell 1
                cell = new PdfPCell(new Phrase("Scheduled Return Date:", fontLabel))
                {
                    Border = 0,
                    BorderWidthLeft = 1,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2
                cell = new PdfPCell(new Phrase(data.AssetDetail.ScheduledReturnDate.ToString() != "" ? data.AssetDetail.ScheduledReturnDate.ToString() : "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Lease Start Date: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.leaseStartDate.ToString() != "" ? data.AssetDetail.leaseStartDate.ToString() : "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Lease End Date:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.leaseEndDate.ToString() != "" ? data.AssetDetail.leaseEndDate.ToString() : "-"), fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 9
                //cell 1
                cell = new PdfPCell(new Phrase("Install Date:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.InstallDate.ToString() != null ? data.AssetDetail.InstallDate.ToString() : "-"), fontNormal))
                    {
                        Border = 0,


                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Order #:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.sessiontrackingnumber.ToString() != "" ? data.AssetDetail.sessiontrackingnumber.ToString() : "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Customer PO: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.ponumber ?? "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 10
                //cell 1
                cell = new PdfPCell(new Phrase("Asset Tag:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.assettagnum ?? "-"), fontNormal))
                    {
                        Border = 0,


                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Model #:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.model ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Coverage: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.coveragename ?? "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 11
                //cell 1
                cell = new PdfPCell(new Phrase("System Name:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.systemname ?? "-"), fontNormal))
                    {
                        Border = 0,


                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Warranty End Date:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.warrantyenddate.ToString() ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("Ticket#: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.ticketid.ToString() != "" ? data.AssetDetail.ticketid.ToString() : "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 12
                //cell 1
                cell = new PdfPCell(new Phrase("Region:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.region ?? "-"), fontNormal))
                    {
                        Border = 0,


                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Old Asset Purchase:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.OldLeasedorPurchased ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("New Asset Purchase: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.NewLeasedorPurchased ?? "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 13
                //cell 1
                cell = new PdfPCell(new Phrase("Invoice Number:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.InvoiceNumber ?? "-"), fontNormal))
                    {
                        Border = 0,


                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("User Logon Name:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.UserLoginName ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("User Email Address: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.UserEmailAddress ?? "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 14
                //cell 1
                cell = new PdfPCell(new Phrase("User First Name:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.UserFirstName ?? "-"), fontNormal))
                    {
                        Border = 0,


                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("User Last Name:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.UserLastName ?? "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,

                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("User Phone Number:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,

                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase(data.AssetDetail.UserPhoneNumber ?? "-", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,

                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);


                //row 15
                //cell 1
                cell = new PdfPCell(new Phrase("Purchase Date:", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 0,
                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase(
                        Convert.ToString(data.AssetDetail.Purchase_Date.ToString() != "" ? data.AssetDetail.Purchase_Date.ToString() : "-"), fontNormal))
                    {
                        Border = 0,

                        BorderWidthBottom = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell(new Phrase("Price: ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 0,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell(new Phrase(data.AssetDetail.price.ToString() != "" ? data.AssetDetail.price.ToString() : "-", fontNormal))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 0,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell(new Phrase("", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 0,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 0,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //row 16
                //cell 1
                cell = new PdfPCell(new Phrase(" ", fontLabel))
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 1,
                    BorderWidthLeft = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 2 
                cell =
                    new PdfPCell(new Phrase("", fontNormal))
                    {
                        Border = 0,

                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 3
                cell = new PdfPCell()
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 1,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 4

                cell = new PdfPCell()
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    PaddingBottom = 5,
                    BorderWidthBottom = 1,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 5
                cell = new PdfPCell()
                {
                    Border = 0,
                    VerticalAlignment = 1,
                    BorderWidthBottom = 1,
                    PaddingBottom = 5,
                    PaddingTop = 5
                };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                //cell 6
                cell =
                    new PdfPCell()
                    {
                        Border = 0,
                        BorderWidthRight = 1,
                        BorderWidthBottom = 1,
                        VerticalAlignment = 1,
                        PaddingBottom = 5,
                        PaddingTop = 5
                    };
                cell.BackgroundColor = backgrounColor;
                pdfTable.AddCell(cell);

                return pdfTable;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        #region Utilities

        //sunny gohar on 17th Jan 2018 ============= ASSET BULK UPLOAD =======================================
        private int GetExcelFileDataForAssetDetailsAndUpdate(string filePath)
        {
            try
            {
                bool error = false;

                string connString = string.Empty;

                var fileExt = System.IO.Path.GetExtension(filePath);

                if (fileExt == ".xlsx")
                    connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath +
                                 ";Extended Properties=Excel 12.0";
                else if (fileExt == ".xls")
                    connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath +
                                 ";Extended Properties=Excel 8.0";

                OleDbConnection oledbConn = new OleDbConnection(connString);
                oledbConn.Open();
                OleDbCommand cmd1 = new OleDbCommand("SELECT * FROM [Sheet1$] where Serial_Number <> NULL", oledbConn);
                OleDbDataAdapter oleda1 = new OleDbDataAdapter();
                oleda1.SelectCommand = cmd1;

                DataSet ds1 = new DataSet();

                oleda1.Fill(ds1, "AssetDetail");

                DataTable objRS = ds1.Tables["AssetDetail"];

                OleDbCommand cmd2 = new OleDbCommand("SELECT Serial_Number, Count([Serial_Number]) as CountOfSerialNumber FROM [Sheet1$] GROUP BY Serial_Number ORDER BY Count(Serial_Number) DESC", oledbConn);
                OleDbDataAdapter oleda2 = new OleDbDataAdapter();
                oleda2.SelectCommand = cmd2;

                DataSet ds2 = new DataSet();

                oleda2.Fill(ds2, "SerialNumbersTable");

                DataTable rsColone = ds2.Tables["SerialNumbersTable"];

                var errorList = new List<string>();

                foreach (DataRow dr in rsColone.Rows)
                {
                    var rsItem = new AssetBulkUploadModel();
                    int count = 0;
                    var countOfSerialNumber = (int.TryParse(dr["CountOfSerialNumber"].ToString(), out count) ? count : 0);
                    rsItem.serialnum = dr["Serial_Number"].ToString();

                    if (countOfSerialNumber > 1)
                    {
                        errorList.Add(countOfSerialNumber + " duplicate serial number ( " + rsItem.serialnum + " ) found.<br/>");
                        error = true;
                    }

                }

                var list = new List<AssetBulkUploadModel>();
                var rowCount = 0;
                DateTime w_startDate = DateTime.Now;
                DateTime w_endDate = DateTime.Now;
                DateTime l_startDate = DateTime.Now;
                DateTime l_endDate = DateTime.Now;
                DateTime s_Date = DateTime.Now;
                DateTime in_Date = DateTime.Now;
                var imaging = false;
                var whitegoldservice = false;
                var ticketId = 0;
                var acustomer = 0;
                var assetId = 0;
                var assetCount = 0;

                foreach (DataRow dr in objRS.Rows)
                {
                    rowCount++;

                    if (!error)
                    {

                        var item = new AssetBulkUploadModel();

                        decimal amount;
                        item.serialnum = dr["Serial_Number"].ToString() ?? "";
                        item.description = dr["Description"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.description))
                        {
                            errorList.Add(" Description should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.Mfg = dr["Manufacturer"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.Mfg))
                        {
                            errorList.Add(" Manufacturer should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.SKU = dr["Part_No"].ToString() ?? "";
                        item.Category = dr["Category"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.Category))
                        {
                            errorList.Add(" Category should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.status = dr["Status"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.status))
                        {
                            errorList.Add(" Status should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.assettagnum = dr["Asset_Tag"].ToString() ?? "";
                        item.model = dr["Model"].ToString() ?? "";
                        item.modelnumber = dr["Model_Number"].ToString() ?? "";
                        item.Location = dr["Location"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.Location))
                        {
                            errorList.Add(" Location should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.SupportedBy = dr["Supported_by"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.SupportedBy))
                        {
                            errorList.Add(" Supported By should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.customername = dr["Current_Owner"].ToString() ?? "";

                        if (String.IsNullOrEmpty(item.customername))
                        {
                            errorList.Add(" Current Owner By should not be empty on row no =" + rowCount);
                            error = true;
                        }

                        item.systemname = dr["System_Name"].ToString() ?? "";
                        item.warrantytypename = dr["Warranty"].ToString() ?? "";
                        item.warrantystartdate = (DateTime.TryParse(dr["Warranty_Start_Date"].ToString(), out w_startDate) ? w_startDate : (DateTime?)null);
                        item.warrantyenddate = (DateTime.TryParse(dr["Warranty_End_Date"].ToString(), out w_startDate) ? w_startDate : (DateTime?)null);
                        item.ErrorMessage = dr["Error_Message"].ToString() ?? "";
                        item.costcenter = dr["Cost_Center"].ToString() ?? "";
                        item.ticketid = (int.TryParse(dr["Ticket_No"].ToString(), out ticketId) ? ticketId : 0);
                        item.OldAssetName = dr["Old_Asset_Name"].ToString() ?? "";
                        item.region = dr["Region"].ToString() ?? "";
                        item.OldAssetTag = dr["Old_Asset_Tag"].ToString() ?? "";
                        item.OldLeasedorPurchased = dr["Old_Asset_Purchase_Type"].ToString() ?? "";
                        item.Imaging = (bool.TryParse(dr["Globalserve_Imaging"].ToString(), out imaging) ? imaging : false);
                        item.NewLeasedorPurchased = dr["New_Asset_Purchase_Type"].ToString() ?? "";
                        item.WhiteGloveDelivery = (bool.TryParse(dr["White_Glove_Delivery_Service"].ToString(), out whitegoldservice) ? whitegoldservice : false);
                        item.AssetRentalAmount = (decimal.TryParse(dr["Asset_Rental_Amount"].ToString(), out amount) ? amount : 0);
                        item.OldAssetModel = dr["Old_Asset_Model"].ToString() ?? "";
                        item.OldAssetModelNumber = dr["Old_Asset_Model_Number"].ToString() ?? "";
                        item.ScheduledReturnDate = (DateTime.TryParse(dr["Scheduled_Return_Date"].ToString(), out s_Date) ? s_Date : (DateTime?)null);
                        item.leaseStartDate = (DateTime.TryParse(dr["Lease_Start_Date"].ToString(), out l_startDate) ? l_startDate : (DateTime?)null);
                        item.leaseEndDate = (DateTime.TryParse(dr["Lease_End_Date"].ToString(), out l_endDate) ? l_endDate : (DateTime?)null);
                        item.InstallDate = (DateTime.TryParse(dr["Install_Date"].ToString(), out in_Date) ? in_Date : (DateTime?)null);
                        item.UserLogonName = dr["User_Logon_Name"].ToString() ?? "";
                        item.UserEmailAddress = dr["User_Email_Address"].ToString() ?? "";
                        item.UserFirstName = dr["User_First_Name"].ToString() ?? "";
                        item.UserLastName = dr["User_Last_Name"].ToString() ?? "";
                        item.UserPhoneNumber = dr["User_Phone_Number"].ToString() ?? "";
                        item.Comments = dr["Comments"].ToString() ?? "";
                        item.Errors = dr["Errors"].ToString() ?? "";


                        if (!String.IsNullOrEmpty(item.serialnum))
                        {

                            acustomer = asp.GetCustomerIdBySerialNumber(item.serialnum.ToString());
                            assetId = 0;

                            if (acustomer > 0)
                            {
                                assetId = asp.GetAssetIdBySerialNumberAndCustomerId(item.serialnum.ToString(), acustomer);
                                if (assetId == 0)
                                {
                                    errorList.Add(" Asset ID Not Found For Serial No " + item.serialnum + "<br>");
                                    error = true;
                                }
                            }

                        }
                        else
                        {
                            errorList.Add(" Serial number should not be empty on row no = " + rowCount + "<br>");
                            error = true;
                        }

                        if (assetId > 0)
                        {

                            //Dropdown Ids
                            item.manufacturerid = asp.GetManufacturerIdByName(item.Mfg);
                            item.customerpartcategoryid = asp.GetCategoryIdByName(item.Category);
                            item.assetstatusid = asp.GetStatusIdByStatusNameAndAssetId(item.status, assetId);
                            item.customeraddressid = asp.GetLocationIdByLocation(item.Location);
                            item.supportedbysfaid = asp.GetSupplierIdByName(item.SupportedBy);
                            item.currentownerid = asp.GetContactId(item.customername, acustomer);
                            item.warrantytypeid = asp.GetWarrantyIdByName(item.warrantytypename);

                        }

                        if (item.manufacturerid <= 0)
                        {
                            errorList.Add(" Selected manufacturer not found. Please select correct manufacturer on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.customerpartcategoryid <= 0)
                        {
                            errorList.Add(" Selected category not found. Please select correct Category on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.assetstatusid <= 0)
                        {
                            errorList.Add(" Selected status not found. Please select correct status on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.customeraddressid <= 0)
                        {
                            errorList.Add(" Selected location not found. Please select correct location on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.supportedbysfaid <= 0)
                        {
                            errorList.Add(" Selected supported by not found. Please select correct support by on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.currentownerid <= 0)
                        {
                            errorList.Add(" Selected current owner not found. Please select correct current owner on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.warrantytypeid <= 0)
                        {
                            errorList.Add(" Selected warranty not found. Please select correct warranty on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.warrantystartdate != null)
                        {
                            if (!IsDateTime(item.warrantystartdate.ToString()))
                            {
                                errorList.Add(" Invalid warranty start date on row no = " + rowCount + "<br>");
                                error = true;
                            }
                        }
                        else
                        {
                            errorList.Add(" Invalid warranty start date on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.warrantyenddate != null)
                        {
                            if (!IsDateTime(item.warrantyenddate.ToString()))
                            {
                                errorList.Add(" Invalid warranty end date on row no = " + rowCount + "<br>");
                                error = true;
                            }
                        }
                        else
                        {
                            errorList.Add(" Invalid warranty end date on row no = " + rowCount + "<br>");
                            error = true;
                        }
                        if (item.warrantystartdate != null && item.warrantyenddate != null)
                        {

                            if (IsDateTime(item.warrantystartdate.ToString()) && IsDateTime(item.warrantyenddate.ToString()))
                            {
                                DateTime startDate = DateTime.Parse(item.warrantystartdate.ToString());
                                DateTime endDate = DateTime.Parse(item.warrantyenddate.ToString());

                                if (startDate > endDate)
                                {
                                    errorList.Add(" Warranty start date cannot be after the Warranty end date on row no = " + rowCount + "<br>");
                                    error = true;
                                }
                            }
                        }

                        if (item.ScheduledReturnDate != null)
                        {
                            if (!IsDateTime(item.ScheduledReturnDate.ToString()))
                            {
                                errorList.Add(" Invalid scheduled return date on row no = " + rowCount + "<br>");
                                error = true;
                            }
                        }
                        else
                        {
                            errorList.Add(" Invalid scheduled return date on row no = " + rowCount + "<br>");
                            error = true;
                        }

                        if (item.leaseStartDate != null)
                        {
                            if (!IsDateTime(item.leaseStartDate.ToString()))
                            {
                                errorList.Add(" Invalid lease start date on row no = " + rowCount + "<br>");
                                error = true;
                            }
                        }
                        else
                        {
                            errorList.Add(" Invalid lease start date on row no = " + rowCount + "<br>");
                            error = true;
                        }

                        if (item.leaseEndDate != null)
                        {
                            if (!IsDateTime(item.leaseEndDate.ToString()))
                            {
                                errorList.Add(" Invalid lease end date on row no = " + rowCount + "<br>");
                                error = true;
                            }
                        }
                        else
                        {
                            errorList.Add(" Invalid lease end date on row no = " + rowCount + "<br>");
                            error = true;
                        }

                        if (item.InstallDate == null || !IsDateTime(item.InstallDate.ToString()))
                        {
                            errorList.Add(" Invalid install date on row no = " + rowCount + "<br>");
                            error = true;
                        }

                        if (item.leaseStartDate != null && item.leaseEndDate != null)
                        {

                            if (IsDateTime(item.leaseStartDate.ToString()) && IsDateTime(item.leaseEndDate.ToString()))
                            {
                                DateTime startDate = DateTime.Parse(item.leaseStartDate.ToString());
                                DateTime endDate = DateTime.Parse(item.leaseEndDate.ToString());

                                if (startDate > endDate)
                                {
                                    errorList.Add(" Lease start date cannot be after the Lease end date on row no = " + rowCount + "<br>");
                                    error = true;
                                }
                            }
                        }

                        if (String.IsNullOrEmpty(item.UserEmailAddress))
                        {
                            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                            Match match = regex.Match(item.UserEmailAddress);
                            if (!match.Success)
                            {
                                errorList.Add(" Invalid user email address on row no =" + rowCount);
                                error = true;
                            }
                        }

                        if (String.IsNullOrEmpty(item.UserPhoneNumber))
                        {
                            Regex regex = new Regex(@"^\+?\d*\-?\d*$");
                            Match match = regex.Match(item.UserPhoneNumber);
                            if (!match.Success)
                            {
                                errorList.Add(" Invalid phone number on row no =" + rowCount);
                                error = true;
                            }
                        }

                        if (item.ticketid != null)
                        {
                            Regex regex = new Regex(@"([0-9]+$)");
                            Match match = regex.Match(item.ticketid.ToString());
                            if (!match.Success)
                            {
                                errorList.Add(" Invalid ticket number on row no =" + rowCount);
                                error = true;
                            }
                        }

                        if (String.IsNullOrEmpty(item.AssetRentalAmount.ToString()))
                        {
                            Regex regex = new Regex(@"^\d*\.?\d*$");
                            Match match = regex.Match(item.AssetRentalAmount.ToString());
                            if (!match.Success)
                            {
                                errorList.Add(" Invalid value for Asset rental amount on row no =" + rowCount);
                                error = true;
                            }
                        }

                        Regex nameRegex = new Regex(@"^[a-zA-Z]'?([a-zA-Z]|\.| |-)+$");

                        if (String.IsNullOrEmpty(item.UserFirstName))
                        {
                            Match match = nameRegex.Match(item.UserFirstName);
                            if (!match.Success)
                            {
                                errorList.Add(" Only characters allowed for First Name on row no =" + rowCount);
                                error = true;
                            }
                        }
                        if (String.IsNullOrEmpty(item.UserLastName))
                        {
                            Match match = nameRegex.Match(item.UserLastName);
                            if (!match.Success)
                            {
                                errorList.Add(" Only characters allowed for Last Name on row no =" + rowCount);
                                error = true;
                            }
                        }

                        var ticketCount = 0;

                        if (item.ticketid != null)
                        {
                            if (Convert.ToInt32(item.ticketid) > 0)
                            {
                                ticketCount = asp.GetTicketCount(item.ticketid.ToString());
                                if (ticketCount <= 0)
                                {
                                    errorList.Add(" Invalid ticket ID Or ticket not found on row no =" + rowCount);
                                    error = true;
                                }
                            }
                        }

                        if (assetId > 0)
                        {
                            acustomer = asp.GetCustomerIdByAssetId(assetId);
                            assetCount = asp.GetAssetCountByCustomerIdAssetIdAndSerialNumber(acustomer, assetId, item.serialnum.ToString());
                            if (assetCount > 0)
                            {
                                errorList.Add(" An asset with serial number: " + item.serialnum.ToString().ToUpper() + " already exists on row no = " + rowCount + " <br>");
                                error = true;
                            }
                        }
                        else
                        {
                            errorList.Add(" Asset ID not found for serial Number: " + item.serialnum.ToString().ToUpper() + " on row no = " + rowCount + " <br>");
                            error = true;
                        }

                        list.Add(item);

                    }


                }
                ViewBag.ErrorList = errorList;
                var id = 0;

                var userId = (int.TryParse(Session["UserID"].ToString(), out id) ? id : 0);


                var result = 0;

                if (errorList.Count <= 0)
                {
                    foreach (var item in list)
                    {
                        result += asp.UpdateAssetDetail(item, assetId, userId);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return 0;
            }
        }

        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }


        #endregion
        //==============================================================================================

    }
}
﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Request Setting (engineer level) detail"
    /// </summary>
    public class RequestSettingController : BaseController
    {
        // GET: RequestSetting

        /// <summary>
        /// This Action is responsible for returning Index View of Request Setting
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
       
        public ActionResult Index(string customerId = null)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    RequestSettingPresenter relp = new RequestSettingPresenter();
                    RequestSettingModel model = relp.GetRequestSettingModel();
                    model.CustomerId = customerId ?? "";
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("RequestSettings");
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for adding Request Setting (engineer level) data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEngineerLevel(tblGenericPriority model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    RequestSettingPresenter relp = new RequestSettingPresenter();
                    int count = relp.AddEngineerLevel(model, Convert.ToInt32(Session["UserID"]));
                    TempData["Message"] = count > 0 == true ? "Engineer level has been added" : "Engineer level already exists";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for returning Json result of customer's priorities
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public JsonResult GetPriorities(int customerId, int companyId)
        {
            RequestSettingPresenter relp = new RequestSettingPresenter();
            RequestSettingModel data = relp.GetPriorities(customerId, companyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for Adding Engineer Level to the customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEngineerLevelToCustomer(EngineerLevelModel model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    RequestSettingPresenter relp = new RequestSettingPresenter();
                    int count = relp.AddEngineerLevelCustomerWise(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Engineer level has been set";
                    return RedirectToAction("Index", new { customerId = model.CustomerId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public ActionResult DeleteRequest(string hCompanyId, string RequestIds, string hCustomerId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(hCompanyId) && !string.IsNullOrEmpty(RequestIds))
                    {
                        RequestSettingPresenter relp = new RequestSettingPresenter();
                        RequestDeleteModel obj = relp.DeleteRequest(hCompanyId, RequestIds);
                        if (obj.Count > 0) TempData["Message"] = "Customer Engineer level has been deleted";
                        if (obj.Count == -1) TempData["Message"] = "Ticket reference is attached with '"+obj.PriorityName+"' priority, Delete is not allowed.";
                        return RedirectToAction("Index", new { customerId = hCustomerId });

                    }
                    else
                    {
                        TempData["Message"] = "Please provide correct info!";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }


    }
}
﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : dec 22 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for national holiday"
    /// </summary>
    public class HolidayController : BaseController
    {
        // GET: Holiday
        /// <summary>
        /// this action is responsible for providing holiday view
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public ActionResult Index(int? countryId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("LocaleNationalHolidays");
                    CountryPresenter countryPresent = new CountryPresenter();
                    return View(countryPresent.GetCountries());

                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// this action is responsible for getting holidays by country to view
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public JsonResult GetHolidays(int countryId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    HolidayPresenter hp = new HolidayPresenter();
                    return Json(hp.GetHolidays(countryId), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for updating holiday
        /// </summary>
        /// <param name="holiday"></param>
        /// <returns></returns>
        public ActionResult UpdateHoliday(HolidayModel holiday)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    HolidayPresenter hp = new HolidayPresenter();
                    int count = hp.AddHoliday(holiday, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Holiday has been added";
                    return RedirectToAction("Index", new { @countryId = holiday.CountryID });

                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for deleting the holiday by country
        /// </summary>
        /// <param name="holidayId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public ActionResult DeleteHoliday(int holidayId, int countryId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    HolidayPresenter hp = new HolidayPresenter();
                    int count = hp.DeleteHoliday(countryId, holidayId);
                    if (count > 0) TempData["Message"] = "Holiday has been deleted";
                    return RedirectToAction("Index", new { @countryId = countryId });

                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    TempData["Message"] = "Error while deleting";
                    return RedirectToAction("Index", new { @countryId = countryId });
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}
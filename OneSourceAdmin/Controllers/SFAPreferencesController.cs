﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : july 21 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for managing sfa preferences"
    /// </summary>
    public class SFAPreferencesController : BaseController
    {
        SFAPreferencesPresenter sfaPreferences;

        public SFAPreferencesController()
        {
            sfaPreferences = new SFAPreferencesPresenter();
        }
        // GET: SFAPreferences

        /// <summary>
        /// this action is responsible for view of password change
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePassword()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    return View();
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for performing password change process
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <param name="confirmPassword"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePassword(string oldPassword, string newPassword, string confirmPassword)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(oldPassword) && !string.IsNullOrEmpty(newPassword) && !string.IsNullOrEmpty(confirmPassword))
                    {
                        if (newPassword == confirmPassword)
                        {
                            var user = sfaPreferences.GetUser(Convert.ToInt32(Session["UserID"]));

                            if (user.UserPassword == newPassword)
                            {
                                ViewBag.Message = "Same Password are not alowed.";
                            }
                            else if (user.UserPassword == oldPassword)
                            {
                                int count = sfaPreferences.UpdateUserPassword(oldPassword, newPassword, confirmPassword, user.UserID);
                                ViewBag.Message = count > 0 ? "Your changes have not been saved." : "Your changes have been saved.";
                            }
                            else
                            {
                                ViewBag.Message = "The password entered does not match the existing password.";
                            }


                        }
                        else
                        {
                            ViewBag.Message = "The new password entered does not match the confirmed password.";
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Please provide all required fields";
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for providing view of contact information
        /// </summary>
        /// <returns></returns>
        public ActionResult ContactInfo()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var model = sfaPreferences.GetContactInfo(Convert.ToInt32(Session["ContactID"]), Convert.ToInt32(Session["ParentCompanyID"]));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for contact information update process
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ContactInfo(sp_getCompanyContactInfo_AdminR_Result model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int count = sfaPreferences.UpdateContactInfo(model, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["ContactID"]), Session["UserGroupName"].ToString());
                    ViewBag.Message = count > 0 ? "Contact info has been updated successfully." : "Contact info has not been updated.";
                    model = sfaPreferences.GetContactInfo(Convert.ToInt32(Session["ContactID"]), Convert.ToInt32(Session["ParentCompanyID"]));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// this action is responsible for providing accreditation view
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public ActionResult Accreditations(string service)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    ViewBag.Message = TempData["Message"];
                    var model = cp.GetSupplierAccreditation(Convert.ToInt32(Session["ParentCompanyID"]), sfaPreferences.GetCustServiceId(service ?? "PROCURE"));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for updating supplier accreditation
        /// </summary>
        /// <param name="mfgIds"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        public ActionResult UpdateSupplierAccreditation(string mfgIds, string service)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    int count = cp.UpdateSupplierAccreditation(sfaPreferences.GetCustServiceId(service), Convert.ToInt32(Session["ParentCompanyID"]), Convert.ToInt32(Session["UserID"]), mfgIds);
                    TempData["Message"] = count > 0 ? "Record updated successfully" : "Your changes could not be saved at this time.";
                    return RedirectToAction("Accreditations");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for providing preferences view
        /// </summary>
        /// <returns></returns>
        public ActionResult Preferences()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    var model = sfaPreferences.GetPreferenceModel(Session["UserGroupName"].ToString(), Convert.ToInt32(Session["UserId"]));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for updating preference
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult ChangePreference(PreferenceModel model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int count = sfaPreferences.ChangePreference(model, Convert.ToInt32(Session["UserId"]));
                    TempData["Message"] = count > 0 ? "Record updated successfully." : "No record was updated.";
                    return RedirectToAction("Preferences");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for providing services view
        /// </summary>
        /// <returns></returns>
        public ActionResult Services()
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    ViewBag.Message = TempData["Message"];
                    var model = sfaPreferences.GetServices(Convert.ToInt32(Session["SFAID"]));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for updating supplier services
        /// </summary>
        /// <param name="serviceIds"></param>
        /// <returns></returns>
        public ActionResult UpdateSupplierService(string serviceIds)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    int count = cp.UpdateSupplierService(Convert.ToInt32(Session["ParentCompanyId"]), Convert.ToInt32(Session["UserID"]), serviceIds);
                    TempData["Message"] = count > 0 ? "Record updated successfully" : "Your changes could not be saved at this time.";
                    return RedirectToAction("Services");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for providing uplifts duties view
        /// </summary>
        /// <returns></returns>
        public ActionResult UpliftsDuties()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var model = sfaPreferences.GetUpliftsDuties(Convert.ToInt32(Session["ParentCompanyId"]));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for providing address view
        /// </summary>
        /// <returns></returns>
        public ActionResult Addresses()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    var model = sfaPreferences.GetAddresses(Convert.ToInt32(Session["ParentCompanyId"]));
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for updating address
        /// </summary>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public ActionResult Address(int addressId)
        {
            try
            {
                ViewBag.CurrentAddressId = TempData["CurrentAddressId"];
                ViewBag.Message = TempData["Message"];

                CompanyPresenter cp = new CompanyPresenter();
                CompanyManufacturerAddressModel model = cp.GetAddressInfo(addressId);
                if (Session["AddrModel"] != null)
                {
                    model.AddressInfo = (sp_GetAddressInfo_AdminR_Result)Session["AddrModel"];
                    Session["AddrModel"] = null;
                }
                model.PartyId = sfaPreferences.GetPartyId(Convert.ToInt32(Session["ParentCompanyId"]));

                ViewBag.CorporateAddressId = Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]);
                return View("~/Views/SFAPreferences/Address.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating and inserting new address
        /// </summary>
        /// <param name="addrModel"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public ActionResult AddEditAddress(sp_GetAddressInfo_AdminR_Result addrModel, int partyId)
        {
            try
            {
                if (addrModel != null)
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    if (addrModel.AddressId == 0)
                    {
                        var data = cp.CreateAddress(addrModel, Convert.ToInt32(Session["UserID"]), partyId, Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]));
                        if (data != null)
                        {
                            var addressId = data.GetType().GetProperty("addressId").GetValue(data).ToString();
                            var partylocationId = data.GetType().GetProperty("partylocationId").GetValue(data).ToString();
                            var addCount = Convert.ToInt32(data.GetType().GetProperty("addCount").GetValue(data));

                            if (addCount > 0)
                            {
                                TempData["Message"] = "Address has been added successfully";
                                return RedirectToAction("Addresses");
                            }
                            else if (addCount == -1)
                            {
                                TempData["Message"] = "Please setup one address of type Corporate Headquarter";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                Session["AddrModel"] = addrModel;

                            }
                            else if (addCount == -2)
                            {
                                TempData["Message"] = "Only one address can be of type Corporate Headquaters";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                Session["AddrModel"] = addrModel;
                            }



                            return RedirectToAction("Address", new { @addressId = addrModel.AddressId });

                        }
                        else
                        {
                            TempData["Message"] = "Error Occured";
                            return RedirectToAction("Address", new { @addressId = 0 });
                        }
                    }
                    else
                    {
                        var data = cp.UpdateAddress(addrModel, Convert.ToInt32(Session["UserID"]), partyId, Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]));
                        if (data != null)
                        {
                            var addressId = data.GetType().GetProperty("addressId").GetValue(data).ToString();
                            var updateCount = Convert.ToInt32(data.GetType().GetProperty("updateCount").GetValue(data));
                            if (updateCount > 0)
                                TempData["Message"] = "Address has been updated successfully";
                            else if (updateCount == -2)
                            {
                                TempData["Message"] = "Only one address can be of type Corporate Headquaters";
                                var previousAddressTypeId = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                addrModel.AddressTypeId = previousAddressTypeId;
                                TempData["CurrentAddressId"] = previousAddressTypeId;
                                addressId = addrModel.AddressId.ToString();
                            }
                            else if (updateCount == -1)
                            {
                                TempData["Message"] = "Please setup one address of type Corporate Headquarter";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                addressId = addrModel.AddressId.ToString();
                            }


                            return RedirectToAction("Address", new { @addressId = addrModel.AddressId });

                        }
                        else
                        {
                            TempData["Message"] = "Error Occured";
                            return RedirectToAction("Address", new { @addressId = 0 });
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult DeleteAddress(int partyId, int addressId)
        {
            try
            {
                CompanyPresenter cp = new CompanyPresenter();
                var Count = cp.DeleteAddress(partyId, addressId);
                TempData["Message"] = Count > 0 ? "Address has been deleted successfully" : "Error Occured";

                if (Count > 0)
                {
                    return RedirectToAction("Addresses");
                }
                else
                {
                    return RedirectToAction("Address", new { @addressId = addressId });
                }



            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
    }
}
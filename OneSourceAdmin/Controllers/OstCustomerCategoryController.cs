﻿using OneSourceAdmin.ViewModels;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 20 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Customer's category detail"
    /// </summary>
    public class OstCustomerCategoryController : BaseController
    {
        CustomerCategoriesPresenter ccp;
        OneStreetPropertyViewModel model;
        public OstCustomerCategoryController() //Constructor
        {
            ccp = new CustomerCategoriesPresenter();
            model = new OneStreetPropertyViewModel();
        }

        // GET: /OstCustomerProduct/
        /// <summary>
        /// This Action is responsible for returning Index View of Customer's Category
        /// </summary>
        /// <returns></returns>              
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("OsCategorySetup");
                    return View(cp.GetCustomers());
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for returning json result of Customer's Manufacturer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        ///
        public JsonResult GetManufacturers(int customerId)
        {
            return Json(ccp.GetManufacturers(customerId), JsonRequestBehavior.AllowGet);
        }


        [Route("OstCustomerCategory/GetManufacturersCatalogSetup/{customerId}")]
        public JsonResult GetManufacturersCatalogSetup(int customerId)
        {
            model.ManufacturerName = ccp.GetManufacturersCatalogSetup(customerId);
            return Json(model.ManufacturerName, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for returning json result of categories by customer and Manufacturer
        /// </summary>
        /// <param name="manufacturerId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetCategories(int manufacturerId, int customerId)
        {
            return Json(ccp.GetCategories(manufacturerId, customerId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Action is responsible for update the customer's categories
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="manufacturerId"></param>
        /// <param name="categoriesIds"></param>
        /// <returns></returns>
        public ActionResult UpdateCustomerCategorySetting(int customerId, int manufacturerId, string categoriesIds)
        {
            if (customerId > 0)
            {
                int a = ccp.UpdateCustomerCategorySetting(customerId, manufacturerId, categoriesIds, Convert.ToInt32(Session["UserID"]));
                TempData["Message"] = a > 0 ? "Record successfully updated" : "No record was updated";
                return RedirectToAction("Index", new { @customerId = customerId, @manufacturerId = manufacturerId });
            }
            TempData["Message"] = "Please provide correct data";
            return RedirectToAction("Index", new { @customerId = customerId, @manufacturerId = manufacturerId });
        }
    }
}
﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : feb 20 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing support service maintenance"
    /// </summary>
    public class SupportServiceMaintenanceController : BaseController
    {
        SupportServiceMaintenancePresenter ssm;
        public SupportServiceMaintenanceController()
        {
            ssm = new SupportServiceMaintenancePresenter();
        }
        #region service

        /// <summary>
        /// this action is responsible for providing service view
        /// </summary>
        /// <returns></returns>
        public ActionResult Services()
        {
            List<SSM_ServiceModel> model = ssm.GetServices();
            @ViewBag.AddEditAccess = isAddEditAllow("Services");
            return View(model);
        }

        /// <summary>
        /// this action is responsible for providing specific service view
        /// </summary>
        /// <param name="SId"></param>
        /// <returns></returns>
        public ActionResult Service(int SId)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                return View("~/Views/SupportServiceMaintenance/AddEditService.cshtml", ssm.GetServiceInfo(SId));
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for add edit service
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditService(tblService model)
        {
            var data = ssm.AddUpdateService(model, adminSetting.UserID);
            TempData["Message"] = data.GetType().GetProperty("msg").GetValue(data).ToString();
            return RedirectToAction("Service", new { @SId = model.ServiceID });
        }
        #endregion

        #region Request Type/Ticket Status

        /// <summary>
        /// this action is responsible for action request ticket status
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="editType"></param>
        /// <param name="isVerbose"></param>
        /// <returns></returns>
        public ActionResult ActionRequestTicketStatus(int? companyId, int? editType, bool isVerbose = false)
        {
            @ViewBag.IsVerbose = isVerbose;
            ViewBag.Message = TempData["Message"];
            Session["ArTypeCompanyId"] = companyId;
            ActionRequestTicketStatusModel model = ssm.GetActionRequestTicketStatusModel(Convert.ToString(companyId), Convert.ToString(editType));
            model.CompanyId = Convert.ToInt32(companyId);
            model.editType = Convert.ToInt32(editType);
            return View(model);
        }

        /// <summary>
        /// this action is responsible for returing the ticket status modal
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult TicketStatusModal(int? Id)
        {
            var data = ssm.GetTicketStatusModal(Id);
            return PartialView("~/Views/SupportServiceMaintenance/_ticketStatusModal.cshtml", data);
        }

        /// <summary>
        /// this action is responsible for action request modal
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult ActionRequestModal(int? Id)
        {
          
            var data = ssm.GetActionRequestModal(Id);
            tblActionRequestType model = (tblActionRequestType)data.GetType().GetProperty("actionRequestType").GetValue(data);
            ViewBag.UserName = data.GetType().GetProperty("userName").GetValue(data).ToString();
            return PartialView("~/Views/SupportServiceMaintenance/_actionRequestModal.cshtml", model);
        }

        /// <summary>
        /// this action is responsible for providing detail of action request 
        /// </summary>
        /// <param name="artypeId"></param>
        /// <param name="companyId"></param>
        /// <param name="isVerbose"></param>
        /// <returns></returns>
        public ActionResult ActionRequestDetail(int artypeId, int companyId, bool isVerbose = false)
        {
            ViewBag.Message = TempData["Message"];
            @ViewBag.IsVerbose = isVerbose;
            Session["ArTypeCompanyId"] = companyId;
            return View(ssm.GetActionDetailModel(artypeId, companyId));
        }

        /// <summary>
        /// this action is responsible for add and edit ticket status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="XRefStatusCode"></param>
        /// <param name="XRefStatusName"></param>
        /// <param name="hcompanyId"></param>
        /// <param name="hartypeId"></param>
        /// <param name="ticketIsActive"></param>
        /// <returns></returns>
        public ActionResult AddEditTicketStatus(tblTicketStatu model,string XRefStatusCode,string XRefStatusName,int? hcompanyId, int? hartypeId,bool ticketIsActive)
        {
            var data = ssm.AddEditTicketStatus(model, XRefStatusCode, XRefStatusName, hcompanyId, hartypeId,ticketIsActive, adminSetting.UserID);
            TempData["Message"] = data.GetType().GetProperty("msg").GetValue(data).ToString();            

            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            return RedirectToAction("ActionRequestTicketStatus");

        }

        /// <summary>
        /// this action is responsible for add edit action request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditActionRequest(tblActionRequestType model)
        {
            var data = ssm.AddEditActionRequestType(model, adminSetting.UserID);
            TempData["Message"] = data.GetType().GetProperty("msg").GetValue(data).ToString();
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            return RedirectToAction("ActionRequestTicketStatus");
        }


        #endregion

    }
}
﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Sep 21 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing AM Communication detail"
    /// </summary>
    public class AMController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of AM Communication
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    AMPresenter amp = new AMPresenter();
                    AMModel model = new AMModel
                    {
                        Customers = cp.GetCustomers(),
                        AMDataList= amp.GetAMData()
                    };
                    @ViewBag.AddEditAccess = isAddEditAllow("AmCommunicationSettings");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for adding and new AM record as well edit previous records
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddAMData(tblAMCommunication model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    AMPresenter cp = new AMPresenter();
                    int count;
                    //For Edit
                    if (model.tblAMCommunicationId != 0)
                    {
                        count = cp.EditAMData(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "AM Communication has been edited";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddAMData(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "AM Communication has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for deleting the AM Record by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int countryId;
                    if (!int.TryParse(id, out countryId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    AMPresenter cp = new AMPresenter();
                    int count = cp.Delete(countryId);
                    if (count > 0) TempData["Message"] = "AM Record has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This method is responsible for checking am communication data comming from request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ValidateFields(tblAMCommunication model)
        {          
         
            if (model.DurationStartDate==null)
            {
                TempData["Message"] = "Please Provide start date";
                return false;
            }
            if (model.DurationEndDate == null)
            {
                TempData["Message"] = "Please Provide end date";
                return false;
            }
            if (model.PhoneCalls<=0)
            {
                TempData["Message"] = "Please Provide Phone Calls";
                return false;
            }
            if (model.Emails <= 0)
            {
                TempData["Message"] = "Please Provide Emails";
                return false;
            }
            if (model.LiveChats <= 0)
            {
                TempData["Message"] = "Please Provide Live Chats";
                return false;
            }
            //if (model.Year <= 0)
            //{
            //    TempData["Message"] = "Please Provide Year";
            //    return false;
            //}
            return true;
        }
    }
}
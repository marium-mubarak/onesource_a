﻿using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;

namespace OneSourceAdmin.Controllers
{
    public class SLAReportsController : BaseController
    {
        private AdminMenuService adminMenuService;
        private SLAReportPresenter slaReportService;
        private BudgetPresenter budget;
        private CustomerPresenter customerService;

        public SLAReportsController()
        {
            adminMenuService = new AdminMenuService();
            slaReportService = new SLAReportPresenter();
            budget = new BudgetPresenter();
            customerService = new CustomerPresenter();
        }
        public SLAReportModel GetSLAReportsModel(int id)
        {
            var route = adminMenuService.GetMenuById(id);
            if (route != null)
            {
                string name = route.MenuName.Trim().ToLower().Replace(" ", "");
                var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));

                

                var model = new SLAReportModel();

                model.routeId = route.MenuId;
                model.routeName = name;
                model.reportName = route.MenuName;
                model.parentRouteId = parentRoute.MenuId;
                model.parentName = parentRoute.MenuName;
                model.parentRoute = parentRoute.MenuLink;

                //model.ListCustomers = slaReportService.GetCustomerList();
                //model.ListCustomers.Insert(0, new GenericList { Key = 0, Value = "--Select--"});

                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");

                model.ListManufacturers = slaReportService.GetManufacturers();
                return model;
            }
            return null;
        }
        // GET: SLAReports
        public ActionResult Report(int id)
        {
            try
            {
                //var route = adminMenuService.GetMenuById(id);
                var model = GetSLAReportsModel(id);
                if (model != null)
                {
                 //   string name = route.MenuName.Trim().ToLower().Replace(" ", "");
                 //   var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));

                 //   //Session["routeId"] = route.MenuId;
                 //   //Session["routeName"] = name;
                 //   //Session["reportName"] = route.MenuName;
                 //   //Session["parentRouteId"] = parentRoute.MenuId;
                 //   //Session["parentName"] = parentRoute.MenuName;
                 //   //Session["parentRoute"] = parentRoute.MenuLink;

                 ////   var model = new SLAReportModel();

                 //   model.routeId = route.MenuId;
                 //   model.routeName = name;
                 //   model.reportName = route.MenuName;
                 //   model.parentRouteId = parentRoute.MenuId;
                 //   model.parentName = parentRoute.MenuName;
                 //   model.parentRoute = parentRoute.MenuLink;

                 //   //model.ListCustomers = slaReportService.GetCustomerList();
                 //   //model.ListCustomers.Insert(0, new GenericList { Key = 0, Value = "--Select--"});

                 //   model.ListCustomers = customerService.GetCustomersWithPriority();
                 //   model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                 //   model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                 //   model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");

                 //   model.ListManufacturers = slaReportService.GetManufacturers();

                    TempData["SLAReportModel"] = model;
                    return View("Report", model);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public ActionResult GetReportData(SLAReportModel model)
        {
            try
            {
                object data = "";

                DateTime FromDate = DateTime.Parse(model.StartDate).Date;
                DateTime ToDate = DateTime.Parse(model.EndDate).Date;

                if (FromDate > ToDate)
                {
                    ViewBag.Message = "Start date should not be greater than end date";
                }
                else
                {
                    var dt = slaReportService.GetReportData(model.routeId.ToString(), model.reportName, "Summary", model);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        TempData["ReportType"] = "Summary";

                        if (model.reportName != "Order Acceptance" && model.reportName != "Quote Response")
                        {
                            //fill up the manufacturer list
                            model.ListManufacturers = new List<GenericList>();
                            var manufacturerIds = model.ManufacturerIds.Split(',');
                            var manufacturerNames = model.ManufacturerNames.Split(',');
                            for (int i = 0; i < manufacturerIds.Length; i++)
                            {
                                GenericList manufacturerslist = new GenericList();
                                manufacturerslist.Key = Convert.ToInt32(manufacturerIds[i]);
                                manufacturerslist.Value = manufacturerNames[i];

                                model.ListManufacturers.Add(manufacturerslist);
                            }

                            //fill up the categories list
                            model.ListCategories = new List<GenericList>();
                            var categoriesIds = model.CategoryIds.Split(',');
                            var categoriesNames = model.Categorynames.Split(',');
                            for (int i = 0; i < categoriesIds.Length; i++)
                            {
                                GenericList categories = new GenericList();
                                categories.Key = Convert.ToInt32(categoriesIds[i]);
                                categories.Value = categoriesNames[i];

                                model.ListCategories.Add(categories);
                            }
                        }


                        //fill up the countries list
                        model.ListCountries = new List<GenericList>();
                        var countryIds = model.CountryIds.Split(',');
                        var countryNames = model.CountryNames.Split(',');
                        for (int i = 0; i < countryIds.Length; i++)
                        {
                            GenericList countries = new GenericList();
                            countries.Key = Convert.ToInt32(countryIds[i]);
                            countries.Value = countryNames[i];

                            model.ListCountries.Add(countries);
                        }

                        //fill up the subcompanies list
                        model.ListSubCompanies = new List<GenericList>();
                        var subcompanyIds = model.CustomerIds.Split(',');
                        var subcompanyNames = model.SubCompanyNames.Split(',');
                        for (int i = 0; i < subcompanyIds.Length; i++)
                        {
                            GenericList subcompany = new GenericList();
                            subcompany.Key = Convert.ToInt32(subcompanyIds[i]);
                            subcompany.Value = subcompanyNames[i];

                            model.ListSubCompanies.Add(subcompany);
                        }



                        ViewBag.Message = "RedirectToResult";
                        TempData["SLAReportModel"] = model;
                    }
                    else
                    {

                        ViewBag.Message = "NoRecordsFound";
                        TempData["SLAReportModel"] = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }

                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        public ActionResult ReportResult(int routeId)
        {
            try
            {
                if (TempData["SLAReportModel"] != null)
                {
                    var model = (SLAReportModel)TempData["SLAReportModel"];
                    return View("ReportResult", model);
                }
                else
                {
                    return RedirectToAction("Report", new { id = routeId });
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult GetSummaryReportData(SLAReportModel model)
        {
            try
            {
                object data;
             //   var model = (SLAReportModel)TempData["SLAReportModel"];
                DataTable finalDT = new DataTable();

                var dt = slaReportService.GetReportData(model.routeId.ToString(), model.reportName, model.ReportType, model);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (model.ReportType == "Summary")
                    {
                        TempData["TempSupportReportModel"] = model;

                        dt.Columns.Add("Score", typeof(System.String));

                        if (model.reportName == "Quote Response")
                        {
                            DataView dv = dt.DefaultView;
                            dv.Sort = "Country asc";
                            dt = dv.ToTable();

                            foreach (DataRow row in dt.Rows)
                            {
                                int score = (int)(Convert.ToDouble(row["withinSLA"]) / Convert.ToDouble(row["total"]) * 100);
                                row["Score"] = score + "%";   // or set it to some other value
                            }
                        }
                        else
                        {
                            DataView dv = dt.DefaultView;
                            dv.Sort = "CountryName asc";
                            dt = dv.ToTable();

                            foreach (DataRow row in dt.Rows)
                            {
                                int score = (int)(Convert.ToDouble(row["withinSLA"]) / Convert.ToDouble(row["Total"]) * 100);
                                row["Score"] = score + "%";   // or set it to some other value
                            }
                        }





                        if (model.reportName == "Quote Response")
                        {
                            var list = ConvertDataTable<RE_rptQuoteResponseSummary_AdminR_Result>(dt);
                            var DistinctCountries = list.Select(x => x.Country).Distinct();

                            //make datatable
                            finalDT.Columns.Add("Country", typeof(System.String));
                            finalDT.Columns.Add("Month-Year", typeof(System.String));
                            finalDT.Columns.Add("SLA Standard", typeof(System.String));
                            finalDT.Columns.Add("Score", typeof(System.String));

                            DataRow rowAfterHeader = finalDT.NewRow();
                            rowAfterHeader["Country"] = " ";
                            finalDT.Rows.Add(rowAfterHeader);

                            int count = 0;
                            //Prepare the datatable
                            foreach (var country in DistinctCountries)
                            {

                                var listOfDates = list.Where(x => x.Country == country).OrderBy(x => x.Country).Select(x => x.Create_date).Distinct();

                                if (count > 0)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = " ";
                                    finalDT.Rows.Add(dr);
                                    count = 0;
                                }

                                foreach (var date in listOfDates)
                                {
                                    var record = list.Where(x => x.Create_date == date && x.Country == country).FirstOrDefault();

                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = count == 0 ? country : "";
                                    dr["Month-Year"] = date;
                                    dr["SLA Standard"] = record.SLA;
                                    dr["Score"] = record.Score;
                                    count++;
                                    finalDT.Rows.Add(dr);
                                }
                            }
                        }

                        if (model.reportName == "Order Acceptance")
                        {
                            var list = ConvertDataTable<RE_rptSLAOrderAcceptanceSummary_AdminR_Result>(dt);
                            var DistinctCountries = list.Select(x => x.CountryName).Distinct();

                            //make datatable
                            finalDT.Columns.Add("Country", typeof(System.String));
                            finalDT.Columns.Add("Month-Year", typeof(System.String));
                            finalDT.Columns.Add("SLA Standard", typeof(System.String));
                            finalDT.Columns.Add("Score", typeof(System.String));

                            DataRow rowAfterHeader = finalDT.NewRow();
                            rowAfterHeader["Country"] = " ";
                            finalDT.Rows.Add(rowAfterHeader);

                            int count = 0;
                            //Prepare the datatable
                            foreach (var country in DistinctCountries)
                            {

                                var listOfDates = list.Where(x => x.CountryName == country).OrderBy(x => x.Orderdate).Select(x => x.Orderdate).Distinct();

                                if (count > 0)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = " ";
                                    finalDT.Rows.Add(dr);
                                    count = 0;
                                }

                                foreach (var date in listOfDates)
                                {
                                    var record = list.Where(x => x.Orderdate == date && x.CountryName == country).FirstOrDefault();

                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = count == 0 ? country : "";
                                    dr["Month-Year"] = date;
                                    dr["SLA Standard"] = record.SLA;
                                    dr["Score"] = record.Score;
                                    count++;
                                    finalDT.Rows.Add(dr);
                                }
                            }
                        }

                        //If the report is from Delivery Metric
                        if (model.reportName != "Order Acceptance" && model.reportName != "Quote Response")
                        {
                            var list = ConvertDataTable<RE_rptSLADeliverySummary_Result>(dt);
                            var DistinctCountries = list.Select(x => x.CountryName).Distinct();
                            var DistinctOEM = list.Select(x => x.ManufacturerName).Distinct();

                            //make datatable
                            finalDT.Columns.Add("Country", typeof(System.String));
                            finalDT.Columns.Add("Month-Year", typeof(System.String));

                            DataRow headerRow = finalDT.NewRow();
                            foreach (var item in DistinctOEM)
                            {
                                finalDT.Columns.Add(item + " - SLA Standard", typeof(System.String));
                                finalDT.Columns.Add(item + " - Score", typeof(System.String));

                                headerRow[item + " - SLA Standard"] = item;
                                headerRow[item + " - Score"] = "Score";
                            }
                            finalDT.Rows.Add(headerRow);

                            DataRow rowAfterHeader = finalDT.NewRow();
                            rowAfterHeader["Country"] = " ";
                            finalDT.Rows.Add(rowAfterHeader);

                            int count = 0;
                            //Prepare the datatable
                            foreach (var country in DistinctCountries)
                            {

                                var listOfDates = list.Where(x => x.CountryName == country).Select(x => x.Orderdate).Distinct();

                                if (count > 0)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = " ";
                                    finalDT.Rows.Add(dr);
                                    count = 0;
                                }

                                foreach (var date in listOfDates)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = count == 0 ? country : "";
                                    dr["Month-Year"] = date;

                                    foreach (var OEM in DistinctOEM)
                                    {
                                        var record = list.Where(x => x.ManufacturerName == OEM && x.Orderdate == date).FirstOrDefault();

                                        if (record != null)
                                        {
                                            dr[OEM + " - SLA Standard"] = record.SLA;
                                            dr[OEM + " - Score"] = record.Score;
                                        }
                                        else
                                        {
                                            dr[OEM + " - SLA Standard"] = "-";
                                            dr[OEM + " - Score"] = "N/A";
                                        }
                                    }

                                    count++;
                                    finalDT.Rows.Add(dr);
                                }
                            }
                        }
                    }
                    else if (model.ReportType == "Detail")
                    {
                        if (model.reportName == "Quote Response")
                        {
                            var list = ConvertDataTable<RE_rptQuoteResponseDetail_AdminR_Result>(dt);
                            var DistinctCountries = list.Select(x => x.Country).Distinct();

                            //make datatable
                            finalDT.Columns.Add("Country", typeof(System.String));
                            finalDT.Columns.Add("Month-Year", typeof(System.String));
                            finalDT.Columns.Add("Quote ID", typeof(System.String));
                            finalDT.Columns.Add("Create Date", typeof(System.String));
                            finalDT.Columns.Add("Response Date", typeof(System.String));
                            finalDT.Columns.Add("Quote Response (Days)", typeof(System.String));

                            DataRow rowAfterHeader = finalDT.NewRow();
                            rowAfterHeader["Country"] = " ";
                            finalDT.Rows.Add(rowAfterHeader);

                            int count = 0;
                            //Prepare the datatable
                            foreach (var country in DistinctCountries)
                            {

                                var listOfDates = list.Where(x => x.Country == country).OrderBy(x => x.Monthly).Select(x => x.Monthly).Distinct();

                                if (count > 0)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = " ";
                                    finalDT.Rows.Add(dr);
                                    count = 0;
                                }

                                foreach (var date in listOfDates)
                                {
                                    var record = list.Where(x => x.Monthly == date && x.Country == country);
                                    var responseAverageForDate = record.Average(x => x.QuoteResponse);

                                    foreach (var item in record)
                                    {
                                        DataRow dr = finalDT.NewRow();

                                        dr["Country"] = count == 0 ? country : "";
                                        dr["Month-Year"] = date;
                                        dr["Quote ID"] = item.Quote_ID;
                                        dr["Create Date"] = ((DateTime)item.CreateDate).ToShortDateString();
                                        dr["Response Date"] = ((DateTime)item.first_Response).ToShortDateString();
                                        dr["Quote Response (Days)"] = item.QuoteResponse;
                                        count++;
                                        finalDT.Rows.Add(dr);
                                    }

                                    DataRow drAverage = finalDT.NewRow();
                                    drAverage["Quote Response (Days)"] = date + " Avg = " + Convert.ToInt32(responseAverageForDate);
                                    finalDT.Rows.Add(drAverage);
                                }
                            }
                        }


                        if (model.reportName == "Order Acceptance")
                        {
                            var list = ConvertDataTable<RE_rptOrderAcceptanceDetail_AdminR_Result>(dt);
                            var DistinctCountries = list.Select(x => x.CountryName).Distinct();

                            //make datatable
                            finalDT.Columns.Add("Country", typeof(System.String));
                            finalDT.Columns.Add("Month-Year", typeof(System.String));
                            finalDT.Columns.Add("Tracking Number", typeof(System.String));
                            finalDT.Columns.Add("Order Date", typeof(System.String));
                            finalDT.Columns.Add("Accepted Date", typeof(System.String));
                            finalDT.Columns.Add("Order Shipped", typeof(System.String));
                            finalDT.Columns.Add("Order Acceptance (Days)", typeof(System.String));

                            DataRow rowAfterHeader = finalDT.NewRow();
                            rowAfterHeader["Country"] = " ";
                            finalDT.Rows.Add(rowAfterHeader);

                            int count = 0;
                            //Prepare the datatable
                            foreach (var country in DistinctCountries)
                            {

                                var listOfDates = list.Where(x => x.CountryName == country).OrderBy(x => x.Monthly).Select(x => x.Monthly).Distinct();

                                if (count > 0)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = " ";
                                    finalDT.Rows.Add(dr);
                                    count = 0;
                                }

                                foreach (var date in listOfDates)
                                {
                                    var record = list.Where(x => x.Monthly == date && x.CountryName == country);
                                    var acceptanceAverageForDate = record.Average(x => x.OrderAcceptance);

                                    foreach (var item in record)
                                    {
                                        DataRow dr = finalDT.NewRow();

                                        dr["Country"] = count == 0 ? country : "";
                                        dr["Month-Year"] = date;
                                        dr["Tracking Number"] = item.TrackingNumber;
                                        dr["Order Date"] = ((DateTime)item.OrderDate).ToShortDateString();
                                        dr["Accepted Date"] = ((DateTime)item.DateAcceptedbySupplier).ToShortDateString();
                                        dr["Order Shipped"] = ((DateTime)item.OrderShipped).ToShortDateString();
                                        dr["Order Acceptance (Days)"] = item.OrderAcceptance;
                                        count++;
                                        finalDT.Rows.Add(dr);
                                    }

                                    DataRow drAverage = finalDT.NewRow();
                                    drAverage["Order Acceptance (Days)"] = date + " Avg = " + Convert.ToInt32(acceptanceAverageForDate);
                                    finalDT.Rows.Add(drAverage);
                                }
                            }
                        }

                        if (model.reportName != "Order Acceptance" && model.reportName != "Quote Response")
                        {
                            var list = ConvertDataTable<RE_RptSLADeliveryDetail_AdminR_Result>(dt);
                            var DistinctCountries = list.Select(x => x.CountryName).Distinct();

                            //make datatable
                            finalDT.Columns.Add("Country", typeof(System.String));
                            finalDT.Columns.Add("Month-Year", typeof(System.String));
                            finalDT.Columns.Add("Tracking Number", typeof(System.String));
                            finalDT.Columns.Add("Order Date", typeof(System.String));
                            finalDT.Columns.Add("Accepted Date", typeof(System.String));
                            finalDT.Columns.Add("Order Shipped", typeof(System.String));
                            finalDT.Columns.Add("Order Delivery (Days)", typeof(System.String));

                            DataRow rowAfterHeader = finalDT.NewRow();
                            rowAfterHeader["Country"] = " ";
                            finalDT.Rows.Add(rowAfterHeader);

                            int count = 0;
                            //Prepare the datatable
                            foreach (var country in DistinctCountries)
                            {

                                var listOfDates = list.Where(x => x.CountryName == country).OrderBy(x => x.Monthly).Select(x => x.Monthly).Distinct();

                                if (count > 0)
                                {
                                    DataRow dr = finalDT.NewRow();
                                    dr["Country"] = " ";
                                    finalDT.Rows.Add(dr);
                                    count = 0;
                                }

                                foreach (var date in listOfDates)
                                {
                                    var record = list.Where(x => x.Monthly == date);
                                    var acceptanceAverageForDate = list.Where(x => x.Monthly == date).Average(x => x.OrderDelivery);

                                    foreach (var item in record)
                                    {
                                        DataRow dr = finalDT.NewRow();

                                        dr["Country"] = count == 0 ? country : "";
                                        dr["Month-Year"] = date;
                                        dr["Tracking Number"] = item.TrackingNumber;
                                        dr["Order Date"] = ((DateTime)item.OrderDate).ToShortDateString();
                                        dr["Accepted Date"] = ((DateTime)item.DateAcceptedbySupplier).ToShortDateString();
                                        dr["Order Shipped"] = ((DateTime)item.DateShipped).ToShortDateString();
                                        dr["Order Delivery (Days)"] = item.OrderDelivery;
                                        count++;
                                        finalDT.Rows.Add(dr);
                                    }

                                    DataRow drAverage = finalDT.NewRow();
                                    drAverage["Order Delivery (Days)"] = date + " Avg = " + Convert.ToInt32(acceptanceAverageForDate);
                                    finalDT.Rows.Add(drAverage);
                                }
                            }
                        }
                    }
                }
                else
                {
                    TempData["TempSupportReportModel"] = null;
                    return RedirectToAction("Report", new { id = model.routeId });
                }
                data = JsonConvert.SerializeObject(finalDT);
                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        private List<T> ConvertDataTable<T>(DataTable dt)
        {

            try
            {
                List<T> data = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    if (row[0].ToString() != "")
                    {
                        T item = GetItem<T>(row);
                        data.Add(item);
                    }
                }
                return data;

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        private T GetItem<T>(DataRow dr)
        {
            try
            {
                Type temp = typeof(T);
                T obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in dr.Table.Columns)
                {
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {

                        if (pro.Name == column.ColumnName)
                        {
                            if (dr[column.ColumnName].ToString() != "")
                            {
                                pro.SetValue(obj, dr[column.ColumnName], null);
                            }
                            else
                            {
                                pro.SetValue(obj, null, null);
                            }
                        }
                        else
                            continue;
                    }
                }
                return obj;
            }


            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                T obj = Activator.CreateInstance<T>();
                return obj;
            }
        }

        public ActionResult RedirectToParent(string parentRoute, int parentRouteId)
        {
            Response.Redirect("~/" + parentRoute + "/" + parentRouteId);
            return null;
        }

        public ActionResult RedirectToReport(int routeId)
        {
            //if (TempData["ReportType"].ToString() == "Detail")
            //{
            //    TempData["ReportType"] = "Summary";
            //    return RedirectToAction("ReportResult", new { routeId = routeId });
            //}
            //else
            //{
                return RedirectToAction("Report", new { id = routeId });
            //}
        }

        public ActionResult GetCountryDetails(int routeId)
        {
            TempData["ReportType"] = "Detail";
            var model = (SLAReportModel)TempData["TempSupportReportModel"];
            TempData["SLAReportModel"] = model; 
            return RedirectToAction("ReportResult", new { routeId = routeId });
        }



        public JsonResult GetSubcompaniesByCustomerID(int customerId)
        {
            try
            {
                var companies = slaReportService.GetSubcompaniesByCustomerID(customerId);
                if (companies != null)
                {
                    var companiesList = companies.Select(x => new
                    {
                        Value = x.Key.ToString(),
                        Text = x.Value
                    }).ToList();
                    return Json(companiesList.ToArray(), JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetBusinessSegmentsByCustomerID(int customerId)
        {
            try
            {
                var businessSegments = slaReportService.GetBusinessSegmentsByCustomerID(customerId);
                if (businessSegments != null)
                {
                    var segmentList = businessSegments.Select(x => new SelectListItem
                    {
                        Value = x.ToString(),
                        Text = x.ToString()
                    }).ToList();

                    return Json(segmentList.ToArray(), JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRegionsByCustomerId(int customerId)
        {
            try
            {
                var regions = slaReportService.GetRegionsByCustomerID(customerId);
                if (regions != null)
                {
                    var regionsList = regions.Select(x => new SelectListItem
                    {
                        Value = x.Key.ToString(),
                        Text = x.Value
                    }).ToList();
                    return Json(regionsList.ToArray(), JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetProductCategoryByCustomerID(int customerId, string reportname)
        {
            try
            {
                var categories = slaReportService.GetProductCategoryByCustomerID(customerId);
                if (categories != null)
                {
                    if (reportname == "Delivery Metric PC & Server")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = (x.Value == "Desktops" || x.Value == "Laptops" || x.Value == "Servers") ? true : false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }
                    else if (reportname == "Delivery Metric PC")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = (x.Value == "Desktops" || x.Value == "Laptops") ? true : false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }
                    else if (reportname == "Delivery Metric Servers")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = (x.Value == "Servers") ? true : false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }

                    else if (reportname == "Delivery Metric Peripherals")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCountryByCustomerId(int customerId)
        {
            try
            {
                var countriesList = budget.GetCountryByCustomer(customerId);
                var countries = countriesList.Select(x => new
                {
                    Value = x.CountryID.ToString(),
                    Text = x.CountryName
                }).ToList();

                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
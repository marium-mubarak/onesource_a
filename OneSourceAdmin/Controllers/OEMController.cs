﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : July 15 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing OEM alert detail"
    /// </summary>
    public class OEMController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of OEM Alert
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    OEMPresenter cp = new OEMPresenter();
                    CustomerPresenter custObj = new CustomerPresenter();
                    OEMModel model = new OEMModel
                    {
                        manufacturers = cp.GetManufacturers(),
                        OEMAlerts = cp.GetOEMAlert(),
                        customers= custObj.GetCustomers()
                    };
                    @ViewBag.AddEditAccess = isAddEditAllow("OemAlert");
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for adding and editing the OEM Alert record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddOEMAlert(tblOEMAlert model)
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    OEMPresenter cp = new OEMPresenter();
                    int count;
                    //For Edit
                    if (model.OEMAlertId != 0)
                    {
                        count = cp.EditOEMAlert(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Alert has been edited";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddOEMAlert(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Alert has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for deleting the alert by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteAlert(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int oemId;
                    if (!int.TryParse(id, out oemId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    OEMPresenter cp = new OEMPresenter();
                    int count = cp.DeleteOEMAlert(oemId);
                    if (count > 0) TempData["Message"] = "Alert has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This method is responsible for checking OEM data comming from request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ValidateFields(tblOEMAlert model)
        {
            if (model.ManufacturerId == 0)
            {
                TempData["Message"] = "Please Provide Manufacturer";
                return false;
            }
            if (Convert.ToDateTime(model.StartDate).Date.Year == 1)
            {
                TempData["Message"] = "Please Provide Start Date";
                return false;
            }
            if (Convert.ToDateTime(model.EndDate).Date.Year > 1)
            {
                if (model.StartDate.Date > Convert.ToDateTime(model.EndDate).Date)
                {
                    TempData["Message"] = "Start Date must be less than End Date";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(model.Alert))
            {
                TempData["Message"] = "Please Provide Alert";
                return false;
            }

            return true;
        }




	}
}
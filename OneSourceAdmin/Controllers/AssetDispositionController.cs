﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace OneSourceAdmin.Controllers
{
    public class AssetDispositionController : BaseController
    {
        // GET: AssetDisposition

        AssetDispositionPresenter _assetDisposition;
        AdminMenuService _adminMenuService;

        #region Constructor
        public AssetDispositionController()
        {
            // _userService = new UserPresenter();
            //_reportService = new AssetReportsPresenter();
            _assetDisposition = new AssetDispositionPresenter();
            //_budgetService = new BudgetPresenter();
            _adminMenuService = new AdminMenuService();
        }

        #endregion

        #region Report Data & pdf Export

        /// <summary>
        /// Report page
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fromBack"></param>
        /// <returns></returns>
        public ActionResult Report(int id, bool? fromBack = null)
        {
            try
            {
                if (fromBack == true)
                {
                    return RedirectToAction("ReportResult", new { fromDetailsBack = true });
                }

                var route = _adminMenuService.GetMenuById(id);

                string name = route.MenuName.ToLower().Trim().Replace(" ", "");
                var parentRoute = _adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));
                //if (route != null)
                //{
                //    string name = route.MenuName.Trim().ToLower().Replace(" ", "");
                //    var parentId = 0;
                //    Int32.TryParse(route.ParentMenuId.ToString(), out parentId);
                //    LogException("MenuId : " + id + ",ParentMenuId : " + parentId, this.ControllerContext.RouteData);
                //    var parentRoute = _adminMenuService.GetMenuById(parentId);

                //Session["routeId"] = route.MenuId;
                //Session["routeName"] = name;
                //Session["reportName"] = route.MenuName;
                //Session["parentRouteId"] = parentRoute.MenuId;
                //Session["parentName"] = parentRoute.MenuName;
                //Session["parentRoute"] = parentRoute.MenuLink;
                //var cusId = TempData["CustomerId"];
                //if (cusId == null)
                //    TempData["CustomerId"] = 0;
                //var regionId = TempData["CountryNames"];
                //if (regionId == null)
                //    TempData["CountryNames"] = 0;

                var model = new AssetDispositionModel();
                model.routeId = route.MenuId;
                model.routeName = name;
                model.reportName = route.MenuName;
                model.parentRouteId = parentRoute.MenuId;
                model.parentName = parentRoute.MenuName;
                model.parentRoute = parentRoute.MenuLink;

                model.ListCustomers = _assetDisposition.GetCustomers();
                model.ListAssetTypes = _assetDisposition.GetAssetTypes();
                model.ListProductCategories = _assetDisposition.GetProductCategories();

                model.ListCountries.Insert(0, new SelectListItem { Text = "--Select--", Value = "-1" });
                model.ListRegions.Insert(0, new SelectListItem { Text = "--Select--", Value = "-1" });

                //Session["TempAssetDispositionModel"] = model;
                //Session["TempAssetDispositionTable"] = null;
                model.TempAssetDispositionModel = model;
                model.TempAssetDispositionTable = null;
                //}
                TempData["TempModel"] = model;
                return View(model);
                //}
                //return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Get Reports Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetReportData(AssetDispositionModel model)
        {
            try
            {
                if (model.AssetTypes == null)
                {
                     model = (AssetDispositionModel)TempData["TempModel"];
                     model.SubType = "Details";
                }                    
                object data = null;
                //var routename = Session["routeName"].ToString();
                var routename = model.routeName;
                #region asset summary report
                if (routename == "assetsummaryreport")
                {
                    if (model.SubType == "Details")
                    {
                        //model = (AssetDispositionModel)Session["TempAssetDispositionModel"];
                        model.SubType = "Details";
                        model.RegionCountry = "RegionWise";
                    }
                    if (!String.IsNullOrWhiteSpace(model.AssetTypes))
                    {
                        model.AssetTypes = model.AssetTypes.TrimEnd(',');
                    }

                    //var reportName = Session["reportName"].ToString();
                    var reportName = model.reportName;

                    var partCategories = model.PartCategoryIds;

                    if (partCategories == null || partCategories == "")
                    {
                        var allCats = _assetDisposition.GetProductCategories();
                        for (int i = 0; i < allCats.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + ",";
                            }
                        }

                        model.CategoryNames = model.CategoryNames.TrimEnd(',');
                    }

                    if (model.CountryIds == null || model.CountryIds == "")
                    {
                        var allConts = _assetDisposition.GetCountries(Convert.ToInt32(model.CustomerId));
                        for (int i = 0; i < allConts.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + ", ";
                            }
                        }
                        model.CountryNames = model.CountryNames.TrimEnd(',');
                    }

                    if (model.AssetTypes == null || model.AssetTypes == "")
                    {
                        var allassets = _assetDisposition.GetAssetTypes();
                        for (int i = 0; i < allassets.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + ",";
                            }
                        }
                        model.AssetTypeNames = model.AssetTypeNames.TrimEnd(',');
                    }

                    if (model.RegionNames != null && model.RegionNames != "")
                    {
                        model.RegionNames = model.RegionNames.TrimEnd(',');
                    }



                    var asset = 0;
                    DataTable dt = null;
                    if (model.RegionCountry == "CountryWise")
                    {
                        var totalAssetDt = _assetDisposition.GetTotalAssetsByCountry(model.reportName, model.CustomerId, model.CountryIds
                       , model.StartDate, model.EndDate, model.AssetTypes, partCategories);
                        model.TotalAssets = Convert.ToInt32(totalAssetDt.Rows[0].ItemArray.FirstOrDefault());
                    }
                    else
                    {
                        var totalAssetDt = _assetDisposition.GetTotalAssetsByRegion(model.reportName, model.CustomerId, model.RegionIds
                     , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                        model.TotalAssets = Convert.ToInt32(totalAssetDt.Rows[0].ItemArray.FirstOrDefault());
                    }
                    //dt = _assetDisposition.GetAssetDispositionReportData(Session["reportName"].ToString(), model.CustomerId, model.CountryIds, model.RegionIds
                    //      , model.StartDate, model.EndDate, model.AssetTypes, partCategories, model.RegionCountry, model.SubType);

                    dt = _assetDisposition.GetAssetDispositionReportData(model.reportName, model.CustomerId, model.CountryIds, model.RegionIds
                            , model.StartDate, model.EndDate, model.AssetTypes, partCategories, model.RegionCountry, model.SubType);
                    if (dt.Rows.Count > 0)
                    {
                        if (model.SubType == "Main")
                        {
                            int index = dt.Rows[0].Table.Columns["TotalAssets"].Ordinal;
                            dt.Columns.Add("%").SetOrdinal(index + 1);

                            foreach (DataRow row in dt.Rows)
                            {
                                //int totalasset = dt.Rows[0].Table.Columns["TotalAssets"].Ordinal;
                                decimal totalasset = 0;
                                Decimal.TryParse(row["TotalAssets"].ToString(), out totalasset);
                                row["%"] = Math.Round((totalasset / model.TotalAssets) * 100, 2);
                                //dt.Columns.Add("Remarketable").SetOrdinal(index + 2);
                                decimal remarketableValue = 0;
                                Decimal.TryParse(row["Remarketable"].ToString(), out remarketableValue);
                                row["Remarketable"] = Math.Round(remarketableValue, 2);
                                //dt.Columns.Add("Recycled").SetOrdinal(index +3);
                                decimal recycledValue = 0;
                                Decimal.TryParse(row["Recycled"].ToString(), out recycledValue);
                                row["Recycled"] = Math.Round(recycledValue, 2);
                            }
                        }
                        else
                        {
                            int remarketableIndex = dt.Rows[0].Table.Columns["Remarketable"].Ordinal;
                            //int remarketablePercIndex = dt.Rows[0].Table.Columns["RemarketablePerc"].Ordinal;
                            int recycledIndex = dt.Rows[0].Table.Columns["Recycled"].Ordinal;
                            //int recycledPercIndex = dt.Rows[0].Table.Columns["RecycledPerc"].Ordinal;

                            dt.Columns["Remarketable"].SetOrdinal(recycledIndex);
                            //dt.Columns["RemarketablePerc"].SetOrdinal(recycledPercIndex);
                            dt.Columns["Recycled"].SetOrdinal(remarketableIndex);
                            //dt.Columns["RecycledPerc"].SetOrdinal(remarketablePercIndex);

                            //foreach (DataRow row in dt.Rows)
                            //{
                            //    decimal remarketablePercValue = 0;
                            //    Decimal.TryParse(row["RemarketablePerc"].ToString(), out remarketablePercValue);
                            //    row["RemarketablePerc"] = Math.Round(remarketablePercValue, 2);

                            //    decimal recycledPercValue = 0;
                            //    Decimal.TryParse(row["RecycledPerc"].ToString(), out recycledPercValue);
                            //    row["RecycledPerc"] = Math.Round(recycledPercValue, 2);
                            //}
                        }
                    }

                    if (model.SubType == "Details")
                    {
                        //Session["TempAssetDispositionDetailTable"] = dt;
                    }
                    else
                    {
                        //Session["TempAssetDispositionMainTable"] = dt;
                    }

                    //Session["TempAssetDispositionModel"] = model;
                    model.TempAssetDispositionModel = model;

                    if (ViewBag.Message != "DateError")
                    {
                        ViewBag.Message = "Redirect";
                    }
                    model.TempAssetDispositionTable = dt;
                    //model.TotalAssets = asset;
                    
                    //data = model;
                }

                #endregion

                #region asset collection report and asset financial report
                else if (routename == "assetcollectionreport" || routename == "assetfinancialreport")
                {
                    if (model.SubType == "Details")
                    {
                        //model = (AssetDispositionModel)Session["TempAssetDispositionModel"];
                        model.SubType = "Details";
                        model.RegionCountry = "RegionWise";
                    }
                    if (!String.IsNullOrWhiteSpace(model.AssetTypes))
                    {
                        model.AssetTypes = model.AssetTypes.TrimEnd(',');
                    }
                    //var reportName = Session["reportName"].ToString();
                    var reportName = model.reportName;

                    var partCategories = model.PartCategoryIds;


                    if (partCategories == null || partCategories == "")
                    {
                        var allCats = _assetDisposition.GetProductCategories();
                        for (int i = 0; i < allCats.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + ",";
                            }
                        }

                        model.CategoryNames = model.CategoryNames.TrimEnd(',');
                    }

                    if (model.CountryIds == null || model.CountryIds == "")
                    {
                        var allConts = _assetDisposition.GetCountries(Convert.ToInt32(model.CustomerId));
                        for (int i = 0; i < allConts.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + ", ";
                            }
                        }
                        model.CountryNames = model.CountryNames.TrimEnd(',');
                    }

                    if (model.AssetTypes == null || model.AssetTypes == "")
                    {
                        var allassets = _assetDisposition.GetAssetTypes();
                        for (int i = 0; i < allassets.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + ",";
                            }
                        }
                        model.AssetTypeNames = model.AssetTypeNames.TrimEnd(',');
                    }

                    DataTable dt = null;

                    if (routename == "assetcollectionreport")
                    {
                        dt = _assetDisposition.GetAssetCollectionDispositionReportData(model.CustomerId, model.CountryIds
                         , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                    }
                    else if (routename == "assetfinancialreport")
                    {
                        dt = _assetDisposition.GetAssetfinancialDispositionReportData(model.CustomerId, model.CountryIds
                         , model.StartDate, model.EndDate, model.AssetTypes, partCategories);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        if (model.SubType == "Main")
                        {
                            int totalassets = 0;
                            int totalitems = 0;
                            foreach (DataRow row in dt.Rows)
                            {
                                if (routename == "assetcollectionreport")
                                {
                                    int TotalItem = 0;
                                    int.TryParse(row["TotalItems"].ToString(), out TotalItem);
                                    totalitems += TotalItem;
                                }
                                else if (routename == "assetfinancialreport")
                                {
                                    int TotalAsset = 0;
                                    int.TryParse(row["TotalAssets"].ToString(), out TotalAsset);
                                    totalassets += TotalAsset;
                                }

                            }
                            if (routename == "assetfinancialreport")
                            {
                                dt.Columns.Remove("TotalAssets");
                            }
                            model.TotalAssets = totalassets;
                            model.TotalItems = totalitems;
                        }

                        //DataTable table = dataSet.Tables["dt"];

                        //// Declare an object variable.
                        //object sumObject;
                        //sumObject = table.Compute("Sum(Amount)", string.Empty);

                    }

                    //data = dt;

                    if (model.SubType == "Details")
                    {
                        model.TempAssetDispositionTable = dt;
                        //Session["TempAssetDispositionDetailTable"] = dt;
                    }
                    else
                    {
                        model.TempAssetDispositionTable = dt;
                        //Session["TempAssetDispositionMainTable"] = dt;
                    }

                    //Session["TempAssetDispositionModel"] = model;

                    if (ViewBag.Message != "DateError")
                    {
                        ViewBag.Message = "Redirect";
                    }
                }
                #endregion

                TempData["TempModel"] = model;

                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        /// <summary>
        /// Getting report result for main and detail page both
        /// </summary>
        /// <param name="fromDetailsBack"></param>
        /// <returns></returns>
        public ActionResult ReportResult(bool? fromDetailsBack = null)
        {
            try
            {
                
                //model.AssetTypes = Request.Params["assetTypes"];
                //var model = _model.TempAssetDispositionModel;
                var model = (AssetDispositionModel)TempData["TempModel"];
                if (model != null)
                {
                    model.AssetTypeNames = model.AssetTypes != null ? model.AssetTypes.Replace("'", "").ToString() : "";
                }
                if (fromDetailsBack == true)
                {
                    model.SubType = "Main";
                }
                TempData["TempModel"] = model;
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Getting report results
        /// </summary>
        /// <returns></returns>
        public JsonResult GetReportResults()
        {
            try
            {
                var model = (AssetDispositionModel)TempData["TempModel"];
                var msg = "";
                DataTable dt = null;

                if (model.SubType == "Details")
                {
                    msg = "FromDetails";
                    //dt = (DataTable)Session["TempAssetDispositionDetailTable"];
                    //dt = model.TempAssetDispositionTable;
                    //dt = ()TempData["DataTable"]
                }
                else
                {
                    //dt = (DataTable)Session["TempAssetDispositionMainTable"];
                    dt = model.TempAssetDispositionTable;
                }
                var data = JsonConvert.SerializeObject(dt);
                TempData["TempModel"] = model;

                if (dt.Rows.Count > 0)
                {

                    return Json(new { data = data, msg = msg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = data, msg = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetReportDataInPDF(AssetDispositionModel model)
        {
            try
            {
                //object data = null;

                //var model = (AssetDispositionModel)TempData["TempModel"];
                //var routename = Session["routeName"].ToString();
                //var model = _model.TempAssetDispositionModel;
                var routename = model.routeName;
                #region asset summary report
                if (routename == "assetsummaryreport")
                {
                    if (model.SubType == "Details")
                    {
                        //model = (AssetDispositionModel)Session["TempAssetDispositionModel"];
                        model.SubType = "Details";
                        model.RegionCountry = "RegionWise";
                    }
                    if (!String.IsNullOrWhiteSpace(model.AssetTypes))
                    {
                        model.AssetTypes = model.AssetTypes.TrimEnd(',');
                    }

                    var reportName = model.reportName;

                    var partCategories = model.PartCategoryIds;


                    if (partCategories == null || partCategories == "")
                    {
                        var allCats = _assetDisposition.GetProductCategories();
                        for (int i = 0; i < allCats.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + ",";
                            }
                        }

                        model.CategoryNames = model.CategoryNames.TrimEnd(',');
                    }

                    if (model.CountryIds == null || model.CountryIds == "")
                    {
                        var allConts = _assetDisposition.GetCountries(Convert.ToInt32(model.CustomerId));
                        for (int i = 0; i < allConts.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + ", ";
                            }
                        }
                        model.CountryNames = model.CountryNames.TrimEnd(',');
                    }

                    if (model.AssetTypes == null || model.AssetTypes == "")
                    {
                        var allassets = _assetDisposition.GetAssetTypes();
                        for (int i = 0; i < allassets.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + ",";
                            }
                        }
                        model.AssetTypeNames = model.AssetTypeNames.TrimEnd(',');
                    }

                    if (model.RegionNames != null && model.RegionNames != "")
                    {
                        model.RegionNames = model.RegionNames.TrimEnd(',');
                    }




                    DataTable dt = null;
                    if (model.RegionCountry == "CountryWise")
                    {
                        var totalAssetDt = _assetDisposition.GetTotalAssetsByCountry(model.reportName, model.CustomerId, model.CountryIds
                       , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                        model.TotalAssets = Convert.ToInt32(totalAssetDt.Rows[0].ItemArray.FirstOrDefault());
                    }
                    else
                    {
                        var totalAssetDt = _assetDisposition.GetTotalAssetsByRegion(model.reportName, model.CustomerId, model.RegionIds
                     , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                        model.TotalAssets = Convert.ToInt32(totalAssetDt.Rows[0].ItemArray.FirstOrDefault());
                    }
                    dt = _assetDisposition.GetAssetDispositionReportData(model.reportName, model.CustomerId, model.CountryIds, model.RegionIds
                          , model.StartDate, model.EndDate, model.AssetTypes, partCategories, model.RegionCountry, model.SubType);

                    if (dt.Rows.Count > 0)
                    {
                        if (model.SubType == "Main")
                        {
                            int index = dt.Rows[0].Table.Columns["TotalAssets"].Ordinal;
                            dt.Columns.Add("%").SetOrdinal(index + 1);
                            foreach (DataRow row in dt.Rows)
                            {
                                //int totalasset = dt.Rows[0].Table.Columns["TotalAssets"].Ordinal;
                                decimal totalasset = 0;
                                Decimal.TryParse(row["TotalAssets"].ToString(), out totalasset);
                                row["%"] = Math.Round((totalasset / model.TotalAssets) * 100, 2);
                                //dt.Columns.Add("Remarketable").SetOrdinal(index + 2);
                                decimal remarketableValue = 0;
                                Decimal.TryParse(row["Remarketable"].ToString(), out remarketableValue);
                                if (row["Remarketable"].ToString().Contains(".0"))
                                {
                                    var val = Decimal.Truncate(remarketableValue);
                                    row["Remarketable"] = val;
                                }                                   
                                else
                                    row["Remarketable"] = Math.Round(remarketableValue, 2);
                                //dt.Columns.Add("Recycled").SetOrdinal(index +3);
                                decimal recycledValue = 0;
                                Decimal.TryParse(row["Recycled"].ToString(), out recycledValue);
                                if (row["Recycled"].ToString().Contains(".0"))
                                {
                                    var val = Decimal.Truncate(recycledValue);
                                    row["Recycled"] = val;
                                }                                   
                                else
                                    row["Recycled"] = Math.Round(recycledValue, 2);
                                decimal totalFees = 0;
                                Decimal.TryParse(row["TotalFees"].ToString(), out totalFees);
                                if (row["TotalFees"].ToString().Contains(".0"))
                                {
                                    var val = Decimal.Truncate(totalFees);
                                    row["TotalFees"] = val;
                                }
                                decimal remarketingValue = 0;
                                Decimal.TryParse(row["RemarketingValue"].ToString(), out remarketingValue);
                                if (row["RemarketingValue"].ToString().Contains(".0"))
                                {
                                    var val = Decimal.Truncate(remarketingValue);
                                    row["RemarketingValue"] = val;
                                }
                            }
                            //int index = dt.Rows[0].Table.Columns["TotalAssets"].Ordinal;
                            //dt.Columns.Add("%").SetOrdinal(index + 1);

                            //foreach (DataRow row in dt.Rows)
                            //{
                            //    row["%"] = "100";

                            //    decimal remarketableValue = 0;
                            //    Decimal.TryParse(row["Remarketable"].ToString(), out remarketableValue);
                            //    row["Remarketable"] = Math.Round(remarketableValue, 2);

                            //    decimal recycledValue = 0;
                            //    Decimal.TryParse(row["Recycled"].ToString(), out recycledValue);
                            //    row["Recycled"] = Math.Round(recycledValue, 2);
                            //}
                        }
                        else
                        {
                            int remarketableIndex = dt.Rows[0].Table.Columns["Remarketable"].Ordinal;
                            int remarketablePercIndex = dt.Rows[0].Table.Columns["RemarketablePerc"].Ordinal;
                            int recycledIndex = dt.Rows[0].Table.Columns["Recycled"].Ordinal;
                            int recycledPercIndex = dt.Rows[0].Table.Columns["RecycledPerc"].Ordinal;

                            dt.Columns["Remarketable"].SetOrdinal(recycledIndex);
                            dt.Columns["RemarketablePerc"].SetOrdinal(recycledPercIndex);
                            dt.Columns["Recycled"].SetOrdinal(remarketableIndex);
                            dt.Columns["RecycledPerc"].SetOrdinal(remarketablePercIndex);

                            foreach (DataRow row in dt.Rows)
                            {
                                decimal remarketablePercValue = 0;
                                Decimal.TryParse(row["RemarketablePerc"].ToString(), out remarketablePercValue);
                                row["RemarketablePerc"] = Math.Round(remarketablePercValue, 2);

                                decimal recycledPercValue = 0;
                                Decimal.TryParse(row["RecycledPerc"].ToString(), out recycledPercValue);
                                row["RecycledPerc"] = Math.Round(recycledPercValue, 2);
                            }
                        }
                    }

                    if (model.SubType == "Details")
                    {
                        //Session["TempAssetDispositionDetailTable"] = dt;
                    }
                    else
                    {
                        //Session["TempAssetDispositionMainTable"] = dt;
                    }

                    //Session["TempAssetDispositionModel"] = model;

                    if (ViewBag.Message != "DateError")
                    {
                        ViewBag.Message = "Redirect";
                    }
                    model.dt = dt;
                }

                #endregion
                #region asset collection report and asset financial report
                else if (routename == "assetcollectionreport" || routename == "assetfinancialreport")
                {
                    if(routename == "assetfinancialreport")
                    {
                        double netValue = Convert.ToDouble(Request.Params["netGainOrLoss"]);
                        if (netValue > 0)
                        {
                            TempData["netGainOrlossName"] = "Net Gain :";
                            TempData["totalNetLossPerAssetName"] = "Net Gain Per Asset :";
                        }
                        else
                        {
                            TempData["netGainOrlossName"] = "Net Loss :";
                            netValue = Math.Abs(netValue);
                            TempData["totalNetLossPerAssetName"] = "Net Loss Per Asset :";
                        }
                        TempData["getTotalFee"] = Convert.ToDouble(Request.Params["TotalFees"]);
                        TempData["totalAr"] = Convert.ToDouble(Request.Params["totalAr"]);
                        TempData["netGainOrLoss"] = netValue;
                        TempData["totalNetLossPerAsset"] = Convert.ToDouble(Request.Params["totalNetLossPerAsset"]);
                    }
                    if (model.SubType == "Details")
                    {
                        //model = (AssetDispositionModel)Session["TempAssetDispositionModel"];
                        model.SubType = "Details";
                        model.RegionCountry = "RegionWise";
                    }
                    if (!String.IsNullOrWhiteSpace(model.AssetTypes))
                    {
                        model.AssetTypes = model.AssetTypes.TrimEnd(',');
                    }
                    //var reportName = Session["reportName"].ToString();
                    var reportName = model.reportName;
                    var partCategories = model.PartCategoryIds;


                    if (partCategories == null || partCategories == "")
                    {
                        var allCats = _assetDisposition.GetProductCategories();
                        for (int i = 0; i < allCats.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CategoryNames = model.CategoryNames + allCats[i].Text + ",";
                            }
                        }

                        model.CategoryNames = model.CategoryNames.TrimEnd(',');
                    }

                    if (model.CountryIds == null || model.CountryIds == "")
                    {
                        var allConts = _assetDisposition.GetCountries(Convert.ToInt32(model.CustomerId));
                        for (int i = 0; i < allConts.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.CountryNames = model.CountryNames + allConts[i].Text + ", ";
                            }
                        }
                        model.CountryNames = model.CountryNames.TrimEnd(',');
                    }

                    if (model.AssetTypes == null || model.AssetTypes == "")
                    {
                        var allassets = _assetDisposition.GetAssetTypes();
                        for (int i = 0; i < allassets.Count; i++)
                        {
                            if (i == 5)
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + "...";
                                break;
                            }
                            else
                            {
                                model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + ",";
                            }
                        }
                        model.AssetTypeNames = model.AssetTypeNames.TrimEnd(',');
                    }
                    DataTable dt = null;

                    if (routename == "assetcollectionreport")
                    {
                        dt = _assetDisposition.GetAssetCollectionDispositionReportData(model.CustomerId, model.CountryIds
                         , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                    }
                    else if (routename == "assetfinancialreport")
                    {
                        dt = _assetDisposition.GetAssetfinancialDispositionReportData(model.CustomerId, model.CountryIds
                         , model.StartDate, model.EndDate, model.AssetTypes, partCategories);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        if (model.SubType == "Main")
                        {
                            int totalassets = 0;
                            int totalitems = 0;
                            foreach (DataRow row in dt.Rows)
                            {
                                if (routename == "assetcollectionreport")
                                {
                                    int TotalItem = 0;
                                    int.TryParse(row["TotalItems"].ToString(), out TotalItem);
                                    totalitems += TotalItem;
                                }
                                else if (routename == "assetfinancialreport")
                                {
                                    int TotalAsset = 0;
                                    int.TryParse(row["TotalAssets"].ToString(), out TotalAsset);
                                    totalassets += TotalAsset;
                                    decimal Fee = 0;
                                    Decimal.TryParse(row["Fee"].ToString(), out Fee);
                                    if (row["Fee"].ToString().Contains(".0"))
                                    {
                                        var val = Decimal.Truncate(Fee);
                                        row["Fee"] = val;
                                    }
                                    decimal assetRecovery = 0;
                                    Decimal.TryParse(row["AssetRecovery"].ToString(), out assetRecovery);
                                    if (row["AssetRecovery"].ToString().Contains(".0"))
                                    {
                                        var val = Decimal.Truncate(assetRecovery);
                                        row["AssetRecovery"] = val;
                                    }
                                }
                            }
                            model.TotalAssets = totalassets;
                            model.TotalItems = totalitems;
                        }
                    }

                    if (model.SubType == "Details")
                    {
                        //Session["TempAssetDispositionDetailTable"] = dt;
                        model.TempAssetDispositionTable = dt;                   
                    }
                    else
                    {                        
                        //Session["TempAssetDispositionMainTable"] = dt;
                    }
                    //Session["TempAssetDispositionModel"] = model;
                    model.TempAssetDispositionModel = model;
                    
                    if (ViewBag.Message != "DateError")
                    {
                        ViewBag.Message = "Redirect";
                    }
                    model.dt = dt;
                }
                #endregion

                var viewToString = StaticHelper.RenderViewToString(ControllerContext, "~/Views/AssetDisposition/ReportResultPDF.cshtml", model, true);

                var filename = routename + ".pdf";
                // return resulted pdf document
                FileResult fileResult = new FileContentResult(PdfSharpConvert(viewToString), "application/pdf");
                fileResult.FileDownloadName = routename + ".pdf";
                return fileResult;

                // return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        #endregion

        #region Utilities

        public ActionResult RedirectToParent(AssetReportModel model)
        {
            Response.Redirect("~/" + model.parentRoute + "/" + model.parentRouteId);
            return null;
        }

        /// <summary>
        /// Redirecting to report page
        /// </summary>
        /// <param name="goBack"></param>
        /// <returns></returns>
        public ActionResult RedirectToReport(AssetDispositionModel model, bool? goBack = null)
        {
            try
            {
                //var model = (AssetDispositionModel)TempData["TempModel"];
                var id = Convert.ToInt32(model.routeId);
                //TempData["StartDate"] = model.StartDate;
                //TempData["EndDate"] = model.EndDate;
                //TempData["CustomerId"] = model.CustomerId;
                //TempData["AssetTypeNames"] = model.AssetTypeNames;
                //TempData["CountryNames"] = model.CountryNames;
                //TempData["PartCategoryIds"] = model.PartCategoryIds;
                //TempData["RegionIds"] = model.RegionIds;
                //TempData["RegionName"] = model.RegionName;
                //TempData["CountryNames"] = model.CountryNames;
                return RedirectToAction("Report", new { id = id, fromBack = goBack });
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Get Segments
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetSegments(int customerId)
        {
            try
            {
                var segments = _assetDisposition.GetSegments(customerId);
                return Json(new { data = segments }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        /// <summary>
        /// Get Regions Countries
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult GetRegionsCountries(int customerId, string type)
        {
            try
            {
                if (type == "Region")
                {
                    var regions = _assetDisposition.GetRegions(customerId);
                    return Json(new { data = regions }, JsonRequestBehavior.AllowGet);
                }
                else if (type == "Country")
                {
                    var countries = _assetDisposition.GetCountries(customerId);
                    return Json(new { data = countries }, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public byte[] PdfSharpConvert(String html)
        {
            byte[] res = null;
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(html);
                Document pdfDoc = new Document(PageSize.LETTER);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                res = stream.ToArray();
            }
            return res;
            //Byte[] res = null;
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.Letter);
            //    pdf.Save(ms);
            //    res = ms.ToArray();
            //}
            //return res;
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(GridHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Grid.pdf");
            }
        }

        /// <summary>
        /// Add region with params
        /// </summary>
        /// <param name="countryIds"></param>
        /// <param name="customerId"></param>
        /// <param name="regionName"></param>
        /// <param name="selectedCountries"></param>
        /// <returns></returns>
        public JsonResult AddRegion(string countryIds, string customerId, string regionName, string selectedCountries)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var selectedCountriesArray = jss.Deserialize<RegionCountries[]>(selectedCountries);
                var userId = Convert.ToInt32(Session["UserID"].ToString());
                var CustomerID = Convert.ToInt32(customerId);
                var regions = _assetDisposition.GetRegionsByCustomerIdAndRegionName(CustomerID, regionName);
                if (regions.Count > 0)
                {
                    return Json(new { data = regions, msg = " Region: " + regions.FirstOrDefault().regionName + " is already defined." }, JsonRequestBehavior.AllowGet);
                }
                int[] countryIDs = Array.ConvertAll(countryIds.Split(','), int.Parse);
                var msg = _assetDisposition.GetRegionCountriesByCustomerId(CustomerID, countryIDs);
                if (!String.IsNullOrWhiteSpace(msg))
                {
                    return Json(new { data = regions, msg = msg }, JsonRequestBehavior.AllowGet);
                }
                int count = _assetDisposition.AddRegion(CustomerID, userId, regionName, selectedCountriesArray);
                if (count > 0)
                {
                    return Json(new { data = count, msg = "Region saved successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = count, msg = "" }, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// update region
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="selectedCountries"></param>
        /// <returns></returns>
        public JsonResult UpdateRegion(string regionId, string selectedCountries)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var selectedCountriesArray = jss.Deserialize<RegionCountries[]>(selectedCountries);
                var userId = Convert.ToInt32(Session["UserID"].ToString());
                int result = 0;
                _assetDisposition.DeleteRegionCountries(Convert.ToInt32(regionId));
                result = _assetDisposition.AddRegionCountries(Convert.ToInt32(regionId), selectedCountriesArray);
                if (result > 0)
                {
                    return Json(new { data = result, msg = "Region updated successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = result, msg = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Delete region
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult DeleteRegion(string regionId, string customerId)
        {
            try
            {
                var result = _assetDisposition.DeleteRegion(Convert.ToInt32(customerId), Convert.ToInt32(regionId));
                if (result > 0)
                {
                    return Json(new { data = result, msg = " Region has been removed" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = result, msg = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// get regions countries
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        public JsonResult GetRegionCountryIds(string regionId)
        {
            try
            {
                var countryIds = _assetDisposition.GetCountryIdsByRegionId(Convert.ToInt32(regionId));
                if (countryIds.Count() > 0)
                {
                    return Json(new { data = countryIds, msg = "Country Ids found" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { data = countryIds, msg = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// get suppliers
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetSuppliers(int customerId)
        {
            try
            {
                var suppliers = _assetDisposition.fillSuppliers(customerId);
                return Json(new { data = suppliers }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public JsonResult GetTicketNumber(int CustomerId, int SupplierId, string StartDate, string EndDate, bool HasAttachment)
        {
            try
            {
                var list = _assetDisposition.fillTicketNumbers(CustomerId, SupplierId, HasAttachment, StartDate, EndDate);
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        public ActionResult ExportPendingTicketstoExcel(int CustomerId, int SupplierId, string StartDate, string EndDate, bool HasAttachment)
        {
            try
            {
                DateTime dtfrom = Convert.ToDateTime(StartDate);
                DateTime dtTo = Convert.ToDateTime(EndDate);
                string cus = _assetDisposition.GetCustomers().Where(x => x.Value == CustomerId.ToString()).Select(x => x.Text).FirstOrDefault();
                string supplier = _assetDisposition.fillSuppliers(Convert.ToInt32(CustomerId)).Where(x => x.Value == SupplierId.ToString()).Select(x => x.Text).FirstOrDefault();
                var filename = "PendingTickets_" + cus + "_" + supplier + ".csv";
                var sb = _assetDisposition.getPendingTickets(CustomerId, SupplierId, dtfrom, dtTo, HasAttachment);
                return File(new System.Text.UTF8Encoding().GetBytes(sb), "text/csv", filename);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        public DataTable GetPendingTicketsReport(int CustomerId, int SupplierId, DateTime StartDate, DateTime EndDate, bool HasAttachment)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_RE_PendingTicketsExport", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@SupplierID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@SD", SqlDbType.DateTime));
                    command.Parameters.Add(new SqlParameter("@ED", SqlDbType.DateTime));
                    command.Parameters.Add(new SqlParameter("@hasattachment", SqlDbType.Bit));

                    command.Parameters[0].Value = CustomerId;
                    command.Parameters[1].Value = SupplierId;
                    command.Parameters[2].Value = StartDate;
                    command.Parameters[3].Value = EndDate;
                    command.Parameters[4].Value = HasAttachment;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetPendingTicketsReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        #endregion

        #region Dashboard


        /// <summary>
        /// Dashboard
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult Dashboard(string customerId = null)
        {
            var model = new AssetDispositionModel();
            model.ListCustomers = _assetDisposition.GetCustomers();
            if (customerId != null)
            {
                model.CustomerId = customerId;
            }
            else
            {
                model.CustomerId = null;
            }
            Session["ADDashboardModel"] = model;
            return View(model);
        }


        /// <summary>
        /// get Dashboard data
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetData(int customerId)
        {
            try
            {
                object assetTrendingData = null;
                object financialTrendingData = null;
                object assetCollectionData = null;
                DataTable assetTrendingDetailTable = null;
                var dt_AssetTrending = _assetDisposition.GetAssetTrendinglist(customerId);
                var dt_FinancialTrending = _assetDisposition.GetFinancialTrendinglist(customerId);
                var dt_AssetCollectionSummary = _assetDisposition.GetAssetCollectionSummary(customerId);

                if (dt_AssetTrending != null)
                {
                    var list = dt_AssetTrending.AsEnumerable().Distinct().ToArray();
                    List<ResultData> dataList = new List<ResultData>();

                    var distinctMonths = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[2].ToString())).Select(a => a.ItemArray[2].ToString()).OrderBy(x => x).Distinct().ToArray();
                    var sortedMonths = distinctMonths
                        .Select(x => new { Name = x.ToString(), Sort = DateTime.ParseExact(x.ToString().Split('-')[0].ToString(), "MMM", CultureInfo.InvariantCulture) })
                        .OrderBy(x => x.Sort.Month)
                        .Select(x => x.Name)
                        .ToArray();
                    var distinctCategories = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[3].ToString())).Select(m => m.ItemArray[3].ToString()).Distinct().ToArray();

                    var quantity = 0;

                    foreach (var categories in distinctCategories)
                    {
                        List<int> quantities = new List<int>();

                        for (int i = 0; i < sortedMonths.Count(); i++)
                        {
                            quantity = list.Where(x => x.ItemArray[3].ToString() == categories && x.ItemArray[2].ToString() == sortedMonths[i].ToString()).Select(x => Convert.ToInt32(x.ItemArray[4].ToString())).FirstOrDefault();

                            quantities.Add(quantity);
                        }

                        if (quantities.Count() > 0)
                        {
                            var data = new ResultData
                            {
                                name = categories,
                                data = quantities.ToArray()
                            };

                            dataList.Add(data);
                        }
                    }

                    var obj = new
                    {
                        data = dataList.Distinct().ToArray(),
                        categories = sortedMonths,
                        name = "Asset Trending Review"

                    };



                    // detail table
                    DataTable detail_AssetTrending = new DataTable("AssetTrendingDetails");

                    detail_AssetTrending.Columns.Add(" ");
                    foreach (var name in sortedMonths)
                    {
                        detail_AssetTrending.Columns.Add(name);
                    }

                    foreach (var item in dataList)
                    {
                        DataRow dr = detail_AssetTrending.NewRow();
                        dr[0] = item.name;
                        for (int i = 1; i <= sortedMonths.Count(); i++)
                        {
                            dr[i] = item.data[i - 1];
                        }
                        detail_AssetTrending.Rows.Add(dr);
                    }
                    //=====

                    object objData = obj;
                    if (dataList.Count <= 0)
                    {
                        objData = "";
                    }

                    assetTrendingData = objData;

                    Session["AssetTrendingDetailTable"] = detail_AssetTrending;

                }

                if (dt_FinancialTrending != null)
                {
                    var list = dt_FinancialTrending.AsEnumerable().Distinct().ToArray();
                    var columns = dt_FinancialTrending.Columns.Cast<DataColumn>().Where(x => !x.ColumnName.Contains("Mon") && !x.ColumnName.Contains("Year"))
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                    List<FinancialResultData> dataList = new List<FinancialResultData>();

                    var distinctMonths = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[2].ToString())).Select(a => a.ItemArray[2].ToString()).Distinct().ToArray();
                    var sortedMonths = distinctMonths
                       .Select(x => new { Name = x.ToString(), Sort = DateTime.ParseExact(x.ToString().Split('-')[0].ToString(), "MMM", CultureInfo.InvariantCulture) })
                       .OrderBy(x => x.Sort.Month)
                       .Select(x => x.Name)
                       .ToArray();

                    var distinctCategories = dt_FinancialTrending.Columns.Cast<DataColumn>().Where(x => !x.ColumnName.Contains("Mon") && !x.ColumnName.Contains("Year"))
                                 .Select(x => x.ColumnName).Distinct()
                                 .ToArray();

                    dynamic quantity = 0;

                    foreach (var category in distinctCategories)
                    {
                        List<object> quantities = new List<object>();

                        for (int i = 0; i < sortedMonths.Count(); i++)
                        {
                            var value = string.Empty;
                            if (category == "TotalAssets")
                            {
                                quantity = list.Where(x => x.ItemArray[3].ToString() == dt_FinancialTrending.Rows[i][category].ToString() && x.ItemArray[2].ToString() == distinctMonths[i].ToString()).Select(x => Convert.ToInt32(x.ItemArray[3].ToString())).FirstOrDefault();
                            }
                            else if (category == "Fees")
                            {
                                quantity = list.Where(x => x.ItemArray[4].ToString() == dt_FinancialTrending.Rows[i][category].ToString() && x.ItemArray[2].ToString() == distinctMonths[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[4].ToString())).FirstOrDefault();

                            }
                            else if (category == "Sales")
                            {
                                quantity = list.Where(x => x.ItemArray[5].ToString() == dt_FinancialTrending.Rows[i][category].ToString() && x.ItemArray[2].ToString() == distinctMonths[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[5].ToString())).FirstOrDefault();

                            }
                            else if (category == "Net")
                            {
                                quantity = list.Where(x => x.ItemArray[6].ToString() == dt_FinancialTrending.Rows[i][category].ToString() && x.ItemArray[2].ToString() == distinctMonths[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[6].ToString())).FirstOrDefault();

                            }
                            else if (category == "NetPerAsset")
                            {
                                quantity = list.Where(x => x.ItemArray[7].ToString() == dt_FinancialTrending.Rows[i][category].ToString() && x.ItemArray[2].ToString() == distinctMonths[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[7].ToString())).FirstOrDefault();

                            }

                            quantities.Add(quantity);
                        }

                        if (quantities.Count() > 0)
                        {
                            var data = new FinancialResultData
                            {
                                name = category,
                                data = quantities.ToArray()
                            };

                            dataList.Add(data);
                        }
                    }

                    var obj = new
                    {
                        data = dataList.Distinct().ToArray(),
                        categories = sortedMonths,
                        name = "Financial Trending Review"

                    };

                    // detail table
                    DataTable detail_FinancialTrending = new DataTable("FinancialTrendingDetails");

                    detail_FinancialTrending.Columns.Add(" ");
                    foreach (var name in sortedMonths)
                    {
                        detail_FinancialTrending.Columns.Add(name);
                    }

                    foreach (var item in dataList)
                    {
                        DataRow dr = detail_FinancialTrending.NewRow();
                        dr[0] = item.name;
                        for (int i = 1; i <= sortedMonths.Count(); i++)
                        {
                            dr[i] = item.data[i - 1];
                        }
                        detail_FinancialTrending.Rows.Add(dr);
                    }
                    //=====

                    object objData = obj;
                    if (dataList.Count <= 0)
                    {
                        objData = "";
                    }

                    financialTrendingData = objData;

                    Session["FinancialTrendingDetailTable"] = detail_FinancialTrending;
                }

                if (dt_AssetCollectionSummary != null)
                {
                    var list = dt_AssetCollectionSummary.AsEnumerable().Distinct().ToArray();
                    List<FinancialResultData> dataList = new List<FinancialResultData>();

                    var distinctRegions = list.Where(x => !String.IsNullOrEmpty(x.ItemArray[0].ToString())).Select(a => a.ItemArray[0].ToString()).Distinct().ToArray();
                    var distinctCategories = dt_AssetCollectionSummary.Columns.Cast<DataColumn>().Where(x => !x.ColumnName.Contains("Region"))
                                 .Select(x => x.ColumnName).Distinct()
                                 .ToArray();

                    dynamic quantity = 0;

                    foreach (var category in distinctCategories)
                    {
                        List<object> quantities = new List<object>();

                        for (int i = 0; i < distinctRegions.Count(); i++)
                        {
                            var value = string.Empty;
                            if (category == "TotalPickups")
                            {
                                quantity = list.Where(x => x.ItemArray[1].ToString() == dt_AssetCollectionSummary.Rows[i][category].ToString() && x.ItemArray[0].ToString() == distinctRegions[i].ToString()).Select(x => Convert.ToInt32(x.ItemArray[1].ToString())).FirstOrDefault();
                            }
                            else if (category == "TotalAssets")
                            {
                                quantity = list.Where(x => x.ItemArray[2].ToString() == dt_AssetCollectionSummary.Rows[i][category].ToString() && x.ItemArray[0].ToString() == distinctRegions[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[2].ToString())).FirstOrDefault();

                            }
                            else if (category == "RemarketablePerc")
                            {
                                quantity = list.Where(x => x.ItemArray[3].ToString() == dt_AssetCollectionSummary.Rows[i][category].ToString() && x.ItemArray[0].ToString() == distinctRegions[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[3].ToString())).FirstOrDefault();

                            }
                            else if (category == "RecycledPerc")
                            {
                                quantity = list.Where(x => x.ItemArray[4].ToString() == dt_AssetCollectionSummary.Rows[i][category].ToString() && x.ItemArray[0].ToString() == distinctRegions[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[4].ToString())).FirstOrDefault();

                            }
                            else if (category == "TotalFees")
                            {
                                quantity = list.Where(x => x.ItemArray[5].ToString() == dt_AssetCollectionSummary.Rows[i][category].ToString() && x.ItemArray[0].ToString() == distinctRegions[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[5].ToString())).FirstOrDefault();

                            }
                            else if (category == "RemarketingValue")
                            {
                                quantity = list.Where(x => x.ItemArray[6].ToString() == dt_AssetCollectionSummary.Rows[i][category].ToString() && x.ItemArray[0].ToString() == distinctRegions[i].ToString()).Select(x => Convert.ToDecimal(x.ItemArray[6].ToString())).FirstOrDefault();

                            }

                            quantities.Add(quantity);
                        }

                        if (quantities.Count() > 0)
                        {
                            var data = new FinancialResultData
                            {
                                name = category,
                                data = quantities.ToArray()
                            };

                            dataList.Add(data);
                        }
                    }

                    var obj = new
                    {
                        data = dataList.Distinct().ToArray(),
                        categories = distinctRegions,
                        name = "Asset Collection Summary"

                    };

                    object objData = obj;
                    if (dataList.Count <= 0)
                    {
                        objData = "";
                    }

                    assetCollectionData = objData;

                    Session["ACSDetailTable"] = dt_AssetCollectionSummary;
                }

                return Json(new
                {
                    assetTrendingData = assetTrendingData,
                    financialTrendingData = financialTrendingData,
                    assetCollectionData = assetCollectionData,
                    assetTrendingDetailTable = assetTrendingDetailTable
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// get table data
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public JsonResult GetTableData(string name)
        {
            try
            {
                DataTable dt = null;
                var result = "";
                if (name == "Asset Trending Review")
                {
                    dt = (DataTable)Session["AssetTrendingDetailTable"];
                }
                else if (name == "Financial Trending Review")
                {
                    dt = (DataTable)Session["FinancialTrendingDetailTable"];
                }
                else if (name == "Asset Collection Summary")
                {
                    dt = (DataTable)Session["ACSDetailTable"];
                }

                result = JsonConvert.SerializeObject(dt);
                return Json(new
                {
                    data = result
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// go to detail pages
        /// </summary>
        /// <param name="name"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult GoToDetails(string name, string customerId)
        {
            try
            {
                var model = (AssetDispositionModel)Session["ADDashboardModel"];
                if (model == null)
                {
                    model = new AssetDispositionModel();
                }
                model.DetailName = name;
                if (!String.IsNullOrEmpty(customerId))
                {
                    model.CustomerId = customerId;
                }

                return View("DashboardDetails", model);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}

﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class SoftwareQuotesController : BaseController
    {
        SoftwareQuotesSearchPresenter _softwareQuotesPresenter;
        string location = ConfigurationManager.AppSettings["SoftwareQuoteAttachement"];
        public SoftwareQuotesController()
        {
            _softwareQuotesPresenter = new SoftwareQuotesSearchPresenter();
        }
        public ActionResult SoftwareQuotes(bool fromMenu = false, bool fromBack = false)
        {
            try
            {
                var model = (SoftwareQuotesSearchModel)Session["TempModel"];

                if (fromMenu)
                {
                    Session["TempModel"] = null;
                }

                if (Session["TempModel"] == null)
                {
                    model = new SoftwareQuotesSearchModel();
                }

                if (fromBack)
                {
                    TempData["RefreshData"] = true;
                }
                else
                {
                    TempData["RefreshData"] = null;
                }

                model.ListCustomers = FillCustomersDropdown();
                model.SoftwareProgramsList = GetSoftwareProgramList();

                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult SoftwareQuotes(SoftwareQuotesSearchModel model)
        {
            try
            {
                string programInfo = model.ProgramId + "-" + model.ProgramName;

                if (model.ProgramName.Contains("Adobe"))
                {
                    Session["ProgramName"] = programInfo + "-" + model.ProgramType;
                }
                else
                {
                    Session["ProgramName"] = programInfo;
                }

                var buttonName = Request.Form["submit"];
                if (buttonName == "Reset")
                {
                    Session["TempModel"] = null;
                    return RedirectToAction("SoftwareQuotes");
                }
                if (model.CustomerId > 0)
                {
                    DateTime? start = null;
                    DateTime? end = null;

                    if (!string.IsNullOrEmpty(model.StartDate))
                    {
                        start = Convert.ToDateTime(model.StartDate);
                    }
                    if (!string.IsNullOrEmpty(model.EndDate))
                    {
                        end = Convert.ToDateTime(model.EndDate);
                    }

                    if (start != null && end != null)
                    {
                        if (start.Value.Date > end.Value.Date)
                        {
                            ViewBag.Message = "DateError";
                        }
                    }
                    var supplierId = 0;
                    if (Session["UserGroupName"].ToString().ToUpper() == "SFA")
                    {
                        supplierId = Convert.ToInt32(Session["SFAID"].ToString());
                    }

                    model.SoftwareQuotes = _softwareQuotesPresenter.GetSoftwareQuotes(model.CustomerId, supplierId, start, end,model.ProgramId); // added program id (SUNNY GOHAR: 7th Jan 2019)
                    Session["TempModel"] = model;
                }
                else
                {
                    ViewBag.Message = "DateError";

                }

                model.ListCustomers = FillCustomersDropdown();
                model.SoftwareProgramsList = GetSoftwareProgramList();

                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public List<GenericList> FillCustomersDropdown()
        {
            var list = new List<GenericList>();

            if (Session["UserGroupName"].ToString().ToUpper() == "SFA")
            {
                list = _softwareQuotesPresenter.GetSoftwareQuoteCustomersByUser(Convert.ToInt32(Session["SFAID"].ToString()));
            }
            else
            {
                list = _softwareQuotesPresenter.GetSoftwareQuoteCustomersByUser();
            }
            list.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

            return list;
        }

        /// <summary>
        ///  Get Software Program List (SUNNY GOHAR : 7th Jan 2019)
        /// </summary>
        /// <returns></returns>
        public List<GenericList> GetSoftwareProgramList()
        {
            var list = new List<GenericList>();

            list = _softwareQuotesPresenter.GetSoftwareProgramList();

            list.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

            return list;
        }

        public ActionResult ResetData()
        {
            try
            {
                Session["TempModel"] = null;
                return RedirectToAction("SoftwareQuotes");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                dynamic obj = null;
                if (Session["UserGroupName"].ToString().ToUpper() == "SFA")
                {
                    obj = _softwareQuotesPresenter.GetSoftwareQuoteDetails(id, Convert.ToInt32(Session["SFAID"].ToString()), true);
                }
                else
                {
                    obj = _softwareQuotesPresenter.GetSoftwareQuoteDetails(id, Convert.ToInt32(Session["UserID"].ToString()));
                }
                var model = obj;

                if (model == null)
                {
                    return RedirectToAction("SoftwareQuotes");
                }
                ViewBag.Details = "Info";
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult GetComment(int id)
        {
            try
            {
                var commment = _softwareQuotesPresenter.GetComment(id);

                return Json(new { data = commment }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet); ;
            }
        }

        [HttpPost]
        public ActionResult SaveResponseLineItem(SoftwareQuotesResponseLineItem model)
        {
            try
            {
                var result = 0;

                if (ModelState.IsValid)
                {
                    model.UserId = Convert.ToInt32(Session["UserID"]);
                    if (Session["UserGroupName"].ToString().ToUpper() == "SFA")
                    {
                        model.UserId = Convert.ToInt32(Session["SFAID"]);
                        result = _softwareQuotesPresenter.SaveResponseLineItem(model, false);
                    }
                    else
                    {
                        result = _softwareQuotesPresenter.SaveResponseLineItem(model);
                    }
                }

                if (result > 0)
                {
                    TempData["Message"] = "Reponse item added succesfully";
                    if (model.ResponseItemId > 0)
                    {
                        TempData["Message"] = "Reponse item updated succesfully";
                    }
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = model.SoftwareQuoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult AddComment(SoftwareQuoteComments model)
        {
            try
            {
                var result = 0;

                if (ModelState.IsValid)
                {
                    var userId = Convert.ToInt32(Session["UserID"]);
                    if (Session["UserGroupName"].ToString().ToUpper() == "SFA")
                    {
                        userId = Convert.ToInt32(Session["SFAID"]);
                        result = _softwareQuotesPresenter.AddComment(model, userId, false);
                    }
                    else
                    {
                        result = _softwareQuotesPresenter.AddComment(model, userId);
                    }
                }

                if (result > 0)
                {
                    TempData["Message"] = "Comment added succesfully";
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = model.SoftwareQuoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult DeleteComment(int id, int quoteId)
        {
            try
            {
                var result = 0;

                if (id > 0)
                {
                    result = _softwareQuotesPresenter.DeleteComment(id);
                }
                if (result > 0)
                {
                    TempData["Message"] = "Comment deleted succesfully";
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = quoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult DeleteLineItem(int id, int quoteId)
        {
            try
            {
                var result = 0;

                if (id > 0)
                {
                    result = _softwareQuotesPresenter.DeleteLineItem(id);
                }
                if (result > 0)
                {
                    TempData["Message"] = "Reponse item deleted succesfully";
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = quoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public ActionResult DeleteAttachment(int id, int quoteId)
        {
            try
            {
                var result = 0;

                if (id > 0)
                {
                    result = _softwareQuotesPresenter.DeleteAttachment(id);
                }
                if (result > 0)
                {
                    TempData["Message"] = "Attachment deleted succesfully";
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = quoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public JsonResult GetManufacturers()
        {
            try
            {
                var manufacturers = _softwareQuotesPresenter.GetManufacturers();
                return Json(new { data = manufacturers }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UploadAttachment(SoftwareQuotesDetailModel model)
        {
            try
            {
                var fileName = string.Empty;
                var result = 0;
                if (model.QuoteId > 0)
                {
                    if (Request.Files.AllKeys.Any())
                    {
                        if (Request.Files.Count > 0)
                        {
                            HttpPostedFileBase file = Request.Files[0];
                            fileName = file.FileName;
                            if (fileName.Contains('\\'))
                            {
                                fileName = fileName.Split('\\')[fileName.Split('\\').Length - 1];
                            }
                            var id = System.Guid.NewGuid().ToString();
                            fileName = id + "___" + fileName;
                            var fileSavePath = Server.MapPath("~/" + location + "/" + fileName);
                            file.SaveAs(fileSavePath);

                            var userId = Convert.ToInt32(Session["UserID"]);
                            if (Session["UserGroupName"].ToString().ToUpper() == "SFA")
                            {
                                userId = Convert.ToInt32(Session["SFAID"]);
                                result = _softwareQuotesPresenter.AddAttachment(model.QuoteId, file, userId, id, true);
                            }
                            else
                            {
                                result = _softwareQuotesPresenter.AddAttachment(model.QuoteId, file, userId, id);
                            }
                        }
                    }
                    if (result > 0)
                    {
                        TempData["Message"] = "Attachment uploaded succesfully";
                    }
                    else
                    {
                        TempData["Message"] = "Something went wrong, please try again later";
                    }
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = model.QuoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult UpdateStatus(int quoteId, string status)
        {
            try
            {
                var result = 0;

                if (quoteId > 0 && !String.IsNullOrWhiteSpace(status))
                {
                    result = _softwareQuotesPresenter.UpdateStatus(quoteId, status);
                }
                if (result > 0)
                {
                    TempData["Message"] = "Status updated succesfully";
                }
                else
                {
                    TempData["Message"] = "Something went wrong, please try again later";
                }

                return RedirectToAction("Details", new { id = quoteId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public ActionResult RedirectToList()
        {
            try
            {
                return RedirectToAction("SoftwareQuotes", new { @fromMenu = false, @fromBack = true });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
    }
}
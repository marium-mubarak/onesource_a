﻿using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : jan 27 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for making sfa uplifts"
    /// </summary>
    public class SFAsController : BaseController
    {
        SFAsPresenter obj;

        public SFAsController()
        {
            obj = new SFAsPresenter();
        }


        // GET: SFAs
        /// <summary>
        /// this action is responsible for providing one street uplift view
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        public ActionResult OneStreetUplifts(int? supplierId, int? country, int customerId = 0, int OneStreetUplift = 0)//added customerid(SUNNY GOHAR)
        {
            ViewBag.Message = TempData["Message"];
            @ViewBag.AddEditAccess = isAddEditAllow("OsSupplierSettings");
            var model = obj.GetSFAModel(supplierId, country, customerId);
            if (OneStreetUplift > 0)
            {
                var exist = obj.CheckOneStreetUpliftExist(OneStreetUplift);
                supplierId = (int)exist.SupplierID;
                country = (int)exist.Country;
                customerId = (int)exist.CustomerId;

                model = obj.GetSFAModel(supplierId, country, customerId);
            }

            return View(model);  
        }

        /// <summary>
        /// this action is responsible for providing add edit view for uplifts
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditSFAUplifts(tblOneStreetUplift model)
        {

          
            int count = obj.AddSFAsUplift(model, adminSetting);
            if (count > 0) TempData["Message"] = "Supplier setting successfully updated";
            else TempData["Message"] = "Error Occured";
            return RedirectToAction("OneStreetUplifts", new { @customerId = model.CustomerId });
        }

        /// <summary>
        /// this action is responsible for providing uplift info
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public ActionResult OneStreetUpliftInfo(int supplierId, int countryId, int customerId) //added customerId (SUNNY GOHAR)
        {

            var data = obj.OneStreetUpliftInfo(supplierId, countryId, customerId); //added customerId (SUNNY GOHAR)

            if (data == null)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [Route("delete-sfa-settings/{settingId}/{countryId}/{supplierId}")]
        public ActionResult DeleteOnestreetUplifts(int settingId, int countryId, int supplierId)
        {
            var model = new tblOneStreetUplift();
            var getExistingSetting = obj.GetSfaSettingsById(settingId);
            if (settingId > 0)
            {
                model.OneStreetUpliftID = settingId;
                model.Country = countryId;
                model.SupplierID = supplierId;
                model.CustomerId = getExistingSetting.CustomerId;
                obj.AddSFAsUplift(model, adminSetting);
                TempData["Message"] = "Supplier setting successfully updated";
            }
            return RedirectToAction("OneStreetUplifts", new { @countryId = countryId, @customerId = getExistingSetting.CustomerId ?? 0 });

        }


    }
}
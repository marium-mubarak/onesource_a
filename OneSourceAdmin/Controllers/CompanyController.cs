﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using System.Configuration;
using System.EnterpriseServices;
using System.EnterpriseServices.Internal;
using System.IO;
using System.Web.Helpers;
using Microsoft.Ajax.Utilities;
using OneSourceAdmin.Helper;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : jan 24 2017
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for managing companies"
    /// </summary>
    public class CompanyController : BaseController
    {
        CompanyPresenter companyService;

        public CompanyController()
        {
            companyService = new CompanyPresenter();
        }
        // GET: Company
        //[Route("admin/search-company")]


        /// <summary>
        /// this action is responsible for providing view for searching company like manufacturer, customer etc
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="company"></param>
        /// <param name="main"></param>
        /// <returns></returns>
        public ActionResult Index(int? roleId, string company = null, bool main = false)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var model = new CompanySearchModel();
                    model.RolesList = companyService.GetRolesList(adminSetting.ParentCompanyID).Where(x => !x.Value.ToLower().Contains("prime")).ToList();

                    if (main)
                    {
                        Session["SearchCompanies"] = null;
                    }

                    if (Session["SearchCompanies"] != null)
                    {
                        model.CompaniesList = (List<sp_getCompanies_AdminR_Result>)Session["SearchCompanies"];
                    }
                    ViewBag.RoleId = roleId;
                    ViewBag.searchTxt = company;

                    @ViewBag.AddEditAccess = isAddEditAllow("AdminCompany");
                    return View(model);
                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for searching requested company
        /// </summary>
        /// <param name="company"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Search(string company, int roleId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    Session["CompanySearchText"] = company;
                    Session["CompanySearchroleId"] = roleId;

                    CompanyPresenter cp = new CompanyPresenter();
                    List<sp_getCompanies_AdminR_Result> model = cp.GetCompanies(roleId, company, adminSetting.ParentCompanyID);
                    Session["SearchCompanies"] = model;

                    return Json(new { company, roleId }, JsonRequestBehavior.AllowGet);
                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        //   [Route("admin/company")]

        /// <summary>
        /// this action is responsible for providing searched company detail
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="partyId"></param>
        /// <param name="companyRole"></param>
        /// <returns></returns>
        public ActionResult Company(int companyId, int partyId, int companyRole)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CompanyModel model = new CompanyModel();
                    model.CompanyId = companyId;
                    CompanyPresenter cp = new CompanyPresenter();
                    ViewBag.Message = TempData["Message"];
                    Session["BackUrl"] = null;
                    if (companyId == -1)
                    {
                        ViewBag.Operation = "Add";

                        model = cp.GetCompanyModel(adminSetting.ParentCompanyID, "GlobalServe", -1);
                        model.parentcompanyid = 0;
                    }
                    else
                    {
                        model = cp.GetCompanyModel(adminSetting.ParentCompanyID, "GlobalServe", partyId);
                        model.parentcompanyid = companyId;
                        model = cp.SetParentCompanyDetail(model, partyId, companyId);
                        CompanyRoleModel companyRoleObject = new CompanyRoleModel();
                        if (model != null && !string.IsNullOrWhiteSpace(model.parentcompanyname))
                        {
                            Session["currentCompanyName"] = model.parentcompanyname;
                        }

                        if (companyRole == Convert.ToInt32(ConfigurationManager.AppSettings["CompanyManufacturerId"]))
                        {
                            Session["MFGID"] = cp.GetManufacturerId(companyId);
                            ViewBag.RoleType = "Mfg";

                            CompanyManufacturerModel manufacturer = new CompanyManufacturerModel();
                            //info model
                            CompanyManufacturerInfoModel mfgIfo = cp.GetCompanyManufacturerInfo(companyId);
                            manufacturer.MfgInfo = mfgIfo;
                            //
                            //address model
                            CompanyManufacturerAddressModel mfgAddr = new CompanyManufacturerAddressModel();
                            CountryPresenter countryPresenter = new CountryPresenter();
                            mfgAddr.AddressList = cp.GetAddressList(partyId);
                            mfgAddr.PartyId = partyId;
                            mfgAddr.Countries = countryPresenter.GetAllCountries();
                            manufacturer.MfgAddress = mfgAddr;
                            //
                            //users model
                            manufacturer.mfgUsers = cp.GetManufacturerUsers(Convert.ToInt32(Session["MFGID"]));
                            //
                            //exchange rate
                            manufacturer.mfgExchangeRate = cp.GetMfgExchangeRate(Convert.ToInt32(Session["MFGID"]));
                            //
                            //contacts
                            manufacturer.mfgContacts = cp.GetContacts(Session["ParentCompanyID"].ToString(), Session["UserID"].ToString(), string.Empty, companyId.ToString());

                            companyRoleObject.CompanyManufacturerModel = manufacturer;
                            model.CompanyRoleModel = companyRoleObject;

                        }

                        else if (companyRole == Convert.ToInt32(ConfigurationManager.AppSettings["CompanyCustomerId"]))
                        {
                            var catService = new CustomerCatalogService();
                            ViewBag.RoleType = "Customer";
                            ViewBag.PartyId = partyId;
                            ViewBag.RoleId = companyRole;
                            ViewBag.CompanyId = companyId;

                            ViewBag.Editmsg = TempData["editMsg"];
                            var companyRoleModel = new CompanyRoleModel();
                            companyRoleModel.CompanyCustomerModel = cp.GetCompanyCustomerInfoByCompanyId(companyId, adminSetting);
                            ViewBag.CustomerId = companyRoleModel.CompanyCustomerModel.Customer.CustomerID;

                            var catalogSearchModel = (CatalogSearchModel)(Session["SearchCatalogModel"]);
                            var dbCon = new globalserveEntities();
                            var customerId = companyRoleModel.CompanyCustomerModel.Customer.CustomerID;
                            if (catalogSearchModel != null)
                            {
                                companyRoleModel.CompanyCustomerModel.CatalogSearchModel = catalogSearchModel;
                                if (catalogSearchModel.CatalogType == 2)
                                {
                                    companyRoleModel.CompanyCustomerModel.BundlelList =
                                        catService.GetBundlesByCustomerId(customerId, catalogSearchModel.SearchText).ToList();
                                }
                                else
                                {

                                    companyRoleModel.CompanyCustomerModel.CatalogList =
                                        dbCon.Sp_GetCatalogPartsByCustomer_R(customerId, catalogSearchModel.PriceCodeId,
                                            catalogSearchModel.SearchText).ToList();
                                }

                            }
                            else
                            {
                                companyRoleModel.CompanyCustomerModel.CatalogSearchModel = new CatalogSearchModel
                                {
                                    PriceCodeId = 1,
                                    SearchText = ""
                                };
                                companyRoleModel.CompanyCustomerModel.CatalogList = dbCon.Sp_GetCatalogPartsByCustomer_R(customerId, 1, "").ToList();
                            }




                            model.CompanyRoleModel = companyRoleModel;


                        }
                        else if (companyRole == Convert.ToInt32(ConfigurationManager.AppSettings["CompanySupplierId"]))
                        {
                            ViewBag.RoleType = "Supplier";
                            ViewBag.PartyId = partyId;
                            ViewBag.RoleId = companyRole;
                            ViewBag.CompanyId = companyId;
                            Session["CurrentSFAID"] = cp.GetSupplierId(companyId);
                            CompanySupplierModel supplier = new CompanySupplierModel();
                            //address model
                            CompanySupplierAddressModel supplierAddr = new CompanySupplierAddressModel();
                            CountryPresenter countryPresenter = new CountryPresenter();
                            supplierAddr.AddressList = cp.GetAddressList(partyId);
                            supplierAddr.PartyId = partyId;
                            supplierAddr.Countries = countryPresenter.GetAllCountries();
                            supplier.SupplierAddress = supplierAddr;
                            supplier.IsSupplier = true;
                            supplier.ISLive = cp.IsLiveSupplier(companyId);
                            supplier.SupplierUsers = cp.GetSupplierUsers(companyId);

                            supplier.SupplierContacts = cp.GetContacts(Session["ParentCompanyID"].ToString(), Session["UserID"].ToString(), string.Empty, companyId.ToString());


                            companyRoleObject.CompanySupplierModel = supplier;
                            model.CompanyRoleModel = companyRoleObject;
                            //
                        }
                        model.PartyId = partyId;

                        //back to search page company
                        if (Request.UrlReferrer != null)
                        {
                            if (Request.UrlReferrer.ToString().Contains("search-company"))
                            {
                                Session["CompanyBackUrl"] = Request.UrlReferrer;
                            }
                        }

                    }
                    Session["CurrentCompanyRole"] = "Company";
                    if (model != null && model.roles != null && model.roles.Count > 0 && companyRole != null && companyRole > 0)
                    {

                        var selectRole = model.roles.FirstOrDefault(x => x.roleid == companyRole);
                        if (selectRole != null)
                        {
                            Session["CurrentCompanyRole"] = selectRole.rolename;
                        }

                    }
                    Session["cCompanyId"] = companyId;
                    Session["cRoleId"] = companyRole;
                    Session["cPartyId"] = partyId;
                    ViewBag.source = "c";
                    Session["PreviousUrlMfgUsr"] = null;
                    Session["PreviousUrlMfgAddr"] = null;
                    Session["PreviousUrlSupplierAddr"] = null;
                    Session["PreviousUrlSupplierUsr"] = null;
                    return View(model);
                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// this action is responsible for add update company
        /// </summary>
        /// <param name="companyModel"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public ActionResult AddUpdateCompany(CompanyModel companyModel, int? partyId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    companyModel.userID = adminSetting.UserID;
                    if (companyModel.CompanyId == 0)
                    {
                        var data = cp.CreateCompany(companyModel);
                        if (data != null)
                        {
                            var companyId = data.GetType().GetProperty("companyId").GetValue(data).ToString();
                            var partyid = data.GetType().GetProperty("partyid").GetValue(data).ToString();
                            var errMsg = data.GetType().GetProperty("errMsg").GetValue(data).ToString();
                            var addCount = Convert.ToInt32(data.GetType().GetProperty("addCount").GetValue(data));
                            TempData["Message"] = addCount > 0 ? "Company has been added successfully" : "Company with this name " + "\"" + companyModel.companyName + "\"" + " already exists";
                            if (errMsg.Length > 0)
                            {
                                TempData["Message"] = errMsg;
                                return RedirectToAction("Company", new { @companyId = -1, @partyid = -1 });
                            }
                            if (addCount > 0)//for updating searched list
                            {
                                List<sp_getCompanies_AdminR_Result> model = cp.GetCompanies(Convert.ToInt32(Session["CompanySearchroleId"]), Convert.ToString(Session["CompanySearchText"]), adminSetting.ParentCompanyID);
                                Session["SearchCompanies"] = model;
                            }
                            var roleId = companyModel.roleIds != null ? companyModel.roleIds.Split(',')[0] : "0";
                            return RedirectToAction("Company", new { @companyId = companyId, @partyid = partyid, @companyRole = roleId });
                        }
                        return RedirectToAction("Company");
                    }
                    else
                    {
                        var data = cp.UpdateCompany(companyModel, partyId);
                        var companyId = data.GetType().GetProperty("companyId").GetValue(data).ToString();
                        var partyid = data.GetType().GetProperty("partyid").GetValue(data).ToString();
                        var updateCount = Convert.ToInt32(data.GetType().GetProperty("updateCount").GetValue(data));

                        if (updateCount == -1)
                        {
                            TempData["Message"] = "Company with this name " + "\"" + companyModel.companyName + "\"" + " already exists";
                        }
                        else
                        {
                            TempData["Message"] = "Company has been updated successfully";

                            //update list record
                            if (Session["SearchCompanies"] != null)
                            {
                                int partIdint = Convert.ToInt32(partyid);
                                var company = cp.GetCompanyByPartyId(partIdint);
                                var roleName = cp.GetRoleNameByPartyId(partIdint);

                                var searchedCompanies = (List<sp_getCompanies_AdminR_Result>)Session["SearchCompanies"];
                                searchedCompanies.ToList().ForEach(x =>
                                {
                                    if (x.partyid == partIdint)
                                    {
                                        if (!roleName.ToLower().Contains(x.rolename.ToLower()))
                                        {
                                            //  var item = model.LstQuotes.FirstOrDefault(x => x.QuoteId == QuoteIdToDelFromList);
                                            searchedCompanies.Remove(x);
                                            return;
                                        }
                                        else
                                        {
                                            //x.rolename = roleName;
                                            x.parentcompanyname = companyModel.companyName;
                                            x.isactive = companyModel.isActive ? "Yes" : "No";
                                            return;
                                        }


                                    }
                                });

                            }
                        }

                        var roleId = companyModel.roleIds != null ? companyModel.roleIds.Split(',')[0] : "0";
                        return RedirectToAction("Company", new { @companyId = companyId, @partyid = partyid, @companyRole = roleId });
                    }

                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        #region contact

        /// <summary>
        /// this action is responsible for downloading contact attachment
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadContactAttachement(string contactId, string fileName)
        {
            try
            {
                if (!string.IsNullOrEmpty(contactId) && !string.IsNullOrEmpty(fileName))
                {
                    int TicketId;
                    if (int.TryParse(contactId, out TicketId))
                    {
                        string filepath = Server.MapPath(fileName);
                        string contentType = "application/octet-stream";

                        if (!System.IO.File.Exists(filepath))
                        {
                            return View("~/Views/Shared/PageNotFound.cshtml");
                        }
                        return File(filepath, contentType, Path.GetFileName(filepath));
                    }
                    return RedirectToAction("RequestDetail", new { ticketNo = contactId });
                }
                else
                {
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            return null;
        }

        /// <summary>
        /// this action is responsible for deleting the contact
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public ActionResult DeleteContact(int contactId)
        {
            try
            {
                if (contactId > 0)
                {
                    CompanyPresenter cp = new OneSourceAdminCore.Presenter.CompanyPresenter();
                    int count = cp.DeleteContact(contactId);
                    TempData["Message"] = count > 0 ? "Contact has been deleted successfully" : "Error Occured";

                    if (Request.UrlReferrer != null)
                    {
                        return Redirect(Request.UrlReferrer.ToString());
                    }
                }
                return Redirect("/admin/company");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing contact detail
        /// </summary>
        /// <param name="source"></param>
        /// <param name="contactId"></param>
        /// <param name="companyId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [Route("company/contact/{source}/{contactId?}/{companyId?}/{userId?}")]
        public ActionResult Contact(string source, int contactId, int companyId, int? userid)
        {
            try
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.source = source;
                if (Session["BackUrl"] == null)
                {
                    Session["BackUrl"] = Request.UrlReferrer;
                }

                CompanyPresenter cp = new CompanyPresenter();
                CompanyContactModel ccm = new CompanyContactModel();
                ccm = cp.GetContactModel(Convert.ToInt32(Session["ParentCompanyID"]), contactId, companyId, userid);

                return View("~/Views/Company/AddEditContact.cshtml", ccm);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for add and update contact
        /// </summary>
        /// <param name="model"></param>
        /// <param name="attachFile"></param>
        /// <param name="roleIds"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public ActionResult AddEditContact(sp_getCompanyContactInfo_AdminR_Result model, HttpPostedFileBase attachFile, string roleIds, string source)
        {
            CompanyPresenter cp = new CompanyPresenter();
            int attachCount = 0;
            int contactId = 0;
            try
            {

                if (model != null && model.ContactID > 0)
                {
                    if (attachFile != null && attachFile.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(attachFile.FileName);
                        var fileExtension = Path.GetExtension(attachFile.FileName).ToLower();
                        if (fileExtension == ".doc" || fileExtension == ".docx" || fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".pdf" ||
                            fileExtension == ".ppt" || fileExtension == ".pptx" || fileExtension == ".htm" || fileExtension == ".jpg" || fileExtension == ".jpeg" ||
                            fileExtension == ".gif" || fileExtension == ".msg" || fileExtension == ".csv" || fileExtension == ".txt" || fileExtension == ".png")
                        {
                            if (attachFile.ContentLength <= int.Parse(ConfigurationManager.AppSettings["FileSize"]))
                            {
                                var docId = System.Guid.NewGuid().ToString();
                                string fileNameM = docId + "^" + fileName;
                                var path = Path.Combine(Server.MapPath(ConfigurationManager.AppSettings["UploadURL"] + "/"), fileNameM);
                                attachFile.SaveAs(path);

                                int? daid = cp.AddDigitalAsset(fileName, docId, fileExtension.Split('.')[1], path, ConfigurationManager.AppSettings["UploadURL"] + "/" + fileNameM, int.Parse(Session["UserID"].ToString()), null);
                                if (daid <= 0)
                                {
                                    if (System.IO.File.Exists(path))
                                    {
                                        System.IO.File.Delete(path);
                                    }
                                    TempData["Message"] = "File Attaching Error";
                                }
                                else
                                {
                                    attachCount = cp.AddContactAttachmentInfo(model.ContactID, Convert.ToInt32(daid), adminSetting.UserID);
                                    TempData["Message"] = "Your Changes Have Been Saved";
                                }
                            }
                            else
                            {
                                TempData["Message"] = "Invalid file size. Maximum allowed size is 10MB.";
                            }

                        }
                        else
                        {
                            TempData["Message"] = "Invalid file. Allowed extensions are: .doc,.docx,.xls,.xlsx,.pdf,.ppt,.pptx,.htm,.jpg,.jpeg,.gif,.msg,.csv,.txt";
                        }

                    }
                    contactId = model.ContactID;
                    var data = cp.UpdateContact(model, Convert.ToInt32(Session["UserID"]), roleIds);
                    if (data == null)
                    {
                        TempData["Message"] = "Error Occured";
                        return Redirect("/company/contact/" + source + "/" + 0 + "/" + model.companyid);
                    }
                    var UpdateCount = Convert.ToInt32(data.GetType().GetProperty("UpdateCount").GetValue(data));
                    var msg = Convert.ToString(data.GetType().GetProperty("msg").GetValue(data));

                    TempData["Message"] = msg;
                    contactId = model.ContactID;


                }
                else
                {
                    var data = cp.AddContact(model, adminSetting.UserID, roleIds, adminSetting.ParentCompanyID);
                    if (data == null)
                    {
                        TempData["Message"] = "Error Occured";
                        return Redirect("/company/contact/" + source + "/" + 0 + "/" + model.companyid);
                    }
                    var newcontactId = data.GetType().GetProperty("newcontactId").GetValue(data).ToString();
                    var partyId = data.GetType().GetProperty("partyId").GetValue(data).ToString();
                    var addCount = Convert.ToInt32(data.GetType().GetProperty("addCount").GetValue(data));

                    if (addCount == -1 && string.IsNullOrEmpty(newcontactId))
                    {
                        TempData["Message"] = "Error Occured";
                        return Redirect("/company/contact/" + source + "/" + 0 + "/" + model.companyid);
                    }
                    else if (addCount == -1 && !string.IsNullOrEmpty(newcontactId))
                    {
                        TempData["Message"] = "Contact with this user already exists.";
                        return Redirect("/company/contact/" + source + "/" + 0 + "/" + model.companyid);
                    }
                    else if (addCount > 0)
                    {
                        TempData["Message"] = "Contact has been added successfully";
                        if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && addCount > 0)
                        {
                            return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                        }
                    }

                }

                return Redirect("/company/contact/" + source + "/" + contactId + "/" + model.companyid);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                    TempData["Message"] = ex.InnerException.Message.ToString();
                    return Redirect("/company/contact/" + source + "/" + contactId + "/" + model.companyid);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// this action is responsible for getting contact's users
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public JsonResult GetUsersContact(string value)
        {
            CompanyPresenter cp = new CompanyPresenter();
            var data = cp.GetUserBySearch(value);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CompanyManufacturer

        /// <summary>
        /// this action is responsible for updating company manufacturer info
        /// </summary>
        /// <param name="mfgModel"></param>
        /// <returns></returns>
        public JsonResult UpdateCompanyManufacturerInfo(CompanyManufacturerInfoModel mfgModel)
        {
            CompanyPresenter cp = new CompanyPresenter();
            int count;
            var data = cp.UpdateCompanyManufacturerInfo(mfgModel, Convert.ToInt32(Session["UserID"]), Session["UserName"].ToString(), out count);
            if (count > 0) return Json(data, JsonRequestBehavior.AllowGet);
            else return Json(null, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// this action is responsible for providing manufacturer address
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="addrId"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        [Route("company/manufacturer-address/{tid?}/{addrId?}/{partyId?}")]
        public ActionResult ManufacturerAddress(int tid, int addrId, int partyId)
        {
            try
            {
                if (Session["PreviousUrlMfgAddr"] == null)
                {
                    if (Request.UrlReferrer != null)
                    {
                        Session["PreviousUrlMfgAddr"] = Request.UrlReferrer.ToString();
                    }
                }
                else
                {
                    if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Split('?')[0] == Session["PreviousUrlMfgAddr"].ToString().Split('?')[0])
                    {
                        Session["PreviousUrlMfgAddr"] = Request.UrlReferrer.ToString();
                    }
                }

                ViewBag.CurrentAddressId = TempData["CurrentAddressId"];
                ViewBag.Message = TempData["Message"];

                CompanyPresenter cp = new CompanyPresenter();
                CompanyManufacturerAddressModel model = cp.GetAddressInfo(addrId);
                if (Session["mfgAddrModel"] != null)
                {
                    model.AddressInfo = (sp_GetAddressInfo_AdminR_Result)Session["mfgAddrModel"];
                    Session["mfgAddrModel"] = null;
                }
                model.PartyId = partyId;

                ViewBag.CorporateAddressId = Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]);
                return View("~/Views/Company/Manufacturer/AddEditManufacturerAddress.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for add and update manufacturer's address
        /// </summary>
        /// <param name="mfgAddrModel"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public ActionResult AddEditManufacturerAddress(sp_GetAddressInfo_AdminR_Result mfgAddrModel, int partyId)
        {
            try
            {
                if (mfgAddrModel != null)
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    if (mfgAddrModel.AddressId == 0)
                    {
                        var data = cp.CreateAddress(mfgAddrModel, Convert.ToInt32(Session["UserID"]), partyId, Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]));
                        if (data != null)
                        {
                            var addressId = data.GetType().GetProperty("addressId").GetValue(data).ToString();
                            var partylocationId = data.GetType().GetProperty("partylocationId").GetValue(data).ToString();
                            var addCount = Convert.ToInt32(data.GetType().GetProperty("addCount").GetValue(data));

                            if (addCount > 0)
                                TempData["Message"] = "Address has been added successfully";

                            else if (addCount == -1)
                            {
                                TempData["Message"] = "Please setup one address of type Corporate Headquarter";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                Session["mfgAddrModel"] = mfgAddrModel;

                            }
                            else if (addCount == -2)
                            {
                                TempData["Message"] = "Only one address can be of type Corporate Headquaters";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                Session["mfgAddrModel"] = mfgAddrModel;
                            }

                            //return Redirect("/company/manufacturer-address/3/" + addressId + "/" + partyId);

                            if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && addCount > 0)
                            {
                                return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                            }
                            else
                            {
                                //return RedirectToAction("Company", new { @companyId = -1, @partyId = 0, @companyRole = 0});
                                return Redirect("/company/manufacturer-address/3/" + addressId + "/" + partyId);
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Error Occured";
                            return Redirect("/company/manufacturer-address/3/" + 0 + "/" + partyId);
                        }
                    }
                    else
                    {
                        var data = cp.UpdateAddress(mfgAddrModel, Convert.ToInt32(Session["UserID"]), partyId, Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]));
                        if (data != null)
                        {
                            var addressId = data.GetType().GetProperty("addressId").GetValue(data).ToString();
                            var updateCount = Convert.ToInt32(data.GetType().GetProperty("updateCount").GetValue(data));
                            if (updateCount > 0)
                                TempData["Message"] = "Address has been updated successfully";
                            else if (updateCount == -2)
                            {
                                TempData["Message"] = "Only one address can be of type Corporate Headquaters";
                                var previousAddressTypeId = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                mfgAddrModel.AddressTypeId = previousAddressTypeId;
                                // Session["mfgAddrModel"] = mfgAddrModel;
                                TempData["CurrentAddressId"] = previousAddressTypeId;
                                addressId = mfgAddrModel.AddressId.ToString();
                            }
                            else if (updateCount == -1)
                            {
                                TempData["Message"] = "Please setup one address of type Corporate Headquarter";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                // Session["mfgAddrModel"] = mfgAddrModel;
                                addressId = mfgAddrModel.AddressId.ToString();
                            }


                            return Redirect("/company/manufacturer-address/3/" + addressId + "/" + partyId);

                        }
                        else
                        {
                            TempData["Message"] = "Error Occured";
                            return Redirect("/company/manufacturer-address/3/" + 0 + "/" + partyId);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for deleting manufacturer's address
        /// </summary>
        /// <param name="partyId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public ActionResult DeleteManufacturerAddress(int partyId, int addressId)
        {
            try
            {
                CompanyPresenter cp = new CompanyPresenter();
                var Count = cp.DeleteAddress(partyId, addressId);
                TempData["Message"] = Count > 0 ? "Address has been deleted successfully" : "Error Occured";

                if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && Count > 0)
                {
                    return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                }
                else
                {
                    return Redirect("/company/manufacturer-address/3/0/" + partyId);
                }



            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating TBU and Duties
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="tbu"></param>
        /// <param name="duties"></param>
        /// <returns></returns>
        public JsonResult UpdateTBUandDuties(int countryId, decimal? tbu, decimal? duties)
        {
            CompanyPresenter cp = new CompanyPresenter();

            var count = cp.UpdateCompanyManufacturerTBUandDuties(Convert.ToInt32(Session["UserID"]), tbu, duties, countryId, Convert.ToInt32(Session["MFGID"]));
            if (count > 0) return Json(count, JsonRequestBehavior.AllowGet);
            else return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for getting TBU and Duties and provide view
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public JsonResult GetTBUandDuties(int countryId)
        {
            CompanyPresenter cp = new CompanyPresenter();

            var data = cp.GetTBUandDuties(countryId, Convert.ToInt32(Session["MFGID"]));
            if (data != null) return Json(new { data.Duties, data.TradeBlockUplift }, JsonRequestBehavior.AllowGet);
            else return Json("0", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for updating user access
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="securityLevel"></param>
        /// <returns></returns>
        public JsonResult UpdateUserAccess(int userId, int securityLevel, bool isSupplier = false)
        {
            int count = 0;
            if (userId > 0 && securityLevel > 0)
            {
                CompanyPresenter cp = new CompanyPresenter();
                if (!isSupplier)
                {
                    count = cp.UpdateUserAccess(userId, securityLevel, Convert.ToInt32(Session["UserID"]), "Manufacturer");
                }
                else
                {
                    count = cp.UpdateUserAccess(userId, securityLevel, Convert.ToInt32(Session["UserID"]), "SFA");
                }
            }
            return Json(count > 0 ? true : false, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// this action is responsible for updating manufacturer's user
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("company/manufacturer-user/{tid?}/{userId?}")]
        public ActionResult ManufacturerUser(int tid, int userId)
        {
            try
            {
                if (Session["PreviousUrlMfgUsr"] == null)
                {
                    if (Request.UrlReferrer != null)
                    {
                        Session["PreviousUrlMfgUsr"] = Request.UrlReferrer.ToString();
                    }
                }
                else
                {
                    if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Split('?')[0] == Session["PreviousUrlMfgUsr"].ToString().Split('?')[0])
                    {
                        Session["PreviousUrlMfgUsr"] = Request.UrlReferrer.ToString();
                    }
                }


                ViewBag.Message = TempData["Message"];
                CompanyPresenter cp = new CompanyPresenter();
                CompanyManufacturerUserModel model = cp.GetManufacturerUserDetailModel(userId);


                return View("~/Views/Company/Manufacturer/AddEditManufacturerUser.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for add edit manufacturer's user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditManufacturerUser(sp_getManufacturerUserInfo_AdminR_Result model)
        {
            try
            {
                int count = 0;
                CompanyPresenter cp = new CompanyPresenter();
                if (model.UserID > 0)
                {
                    count = cp.UpdateManufacturerUser(model, Convert.ToInt32(Session["UserID"]));
                    if (count == -1) TempData["Message"] = "A user with username: '" + model.UserName + "' already exists";
                    else
                    {
                        TempData["Message"] = count > 0 ? "User has been updated successfully" : "Error Occured";
                    }
                }
                else
                {
                    count = cp.AddManufacturerUser(model, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["MFGID"]), Convert.ToInt32(Session["ParentCompanyID"]));
                    if (count == -1) TempData["Message"] = "A user with username: '" + model.UserName + "' already exists";
                    else { TempData["Message"] = count > 0 ? "User has been added successfully" : "Error Occured"; }

                    if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && count > 0)
                    {
                        return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                    }
                    else
                    {
                        return Redirect("/company/manufacturer-user/4/" + model.UserID);

                    }
                }

                return Redirect("/company/manufacturer-user/4/" + model.UserID);


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for getting exchange rate info by country
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public JsonResult GetExchangeRateInfoByCountry(int countryId)
        {
            CompanyPresenter cp = new CompanyPresenter();
            var data = cp.GetExchangeRateInfoByCountry(countryId, Convert.ToInt32(Session["MFGID"]));
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for add and update manufacturer's exchange rate
        /// </summary>
        /// <param name="currencyID"></param>
        /// <param name="exchangeRate"></param>
        /// <returns></returns>
        public JsonResult AddUpdateMfgExchangeRate(int currencyID, decimal exchangeRate)
        {
            CompanyPresenter cp = new CompanyPresenter();
            var count = cp.AddUpdateMfgExchangeRate(currencyID, exchangeRate, Convert.ToInt32(Session["MFGID"]), Convert.ToInt32(Session["UserID"]));
            return Json(count, JsonRequestBehavior.AllowGet);
        }



        #endregion


        #region Company Customer

        [ValidateInput(false)]
        public ActionResult UpdateCompanyCustomer(CompanyCustomerModel customerInfo, int companyId, int partyId, int roleId)
        {
            try
            {
                var chkMsg = companyService.UpdateCustomer(customerInfo, adminSetting);
                TempData["editMsg"] = chkMsg.ToString();
                return RedirectToAction("Company", new { companyId = companyId, partyId = partyId, companyRole = roleId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public JsonResult GetCustomerCurrencyByCountryId(int customerId, int countryId)
        {
            try
            {
                var currency = companyService.GetCustomerCurrencyByCountry(customerId, countryId);


                return Json(currency.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult UpdateCustomerCountryCurrency(List<CustomerCurrencyModel> list)
        {
            try
            {
                var chk = companyService.UpdateCustomerCountryCurrencySetups(list, adminSetting);


                return Json(chk, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("admin/customer/user/{userId}/{companyId}/{partyId}/{roleId}")]
        public ActionResult AddEditCustomerUser(int userId, int companyId, int partyId, int roleId)
        {
            try
            {
                var dbCon = new globalserveEntities();
                var primeContractorService = new PrimeContractorService();
                var countryService = new CountryPresenter();
                ViewBag.PartyId = partyId;
                ViewBag.RoleId = roleId;
                ViewBag.CompanyId = companyId;
                ViewBag.Editmsg = TempData["editMsg"];
                ViewBag.EmailMsg = TempData["emailMsg"];
                Session["BackUrl"] = null;
                var customerModel = companyService.GetCompanyCustomerInfoByCompanyId(companyId, adminSetting);
                customerModel.CustomerUser = new Sp_GetCustomerUserByUserId_AdminR_Result();
                if (userId > 0)
                {
                    customerModel.CustomerUser = dbCon.Sp_GetCustomerUserByUserId_AdminR(userId).FirstOrDefault();

                }

                var customerId = customerModel.Customer.CustomerID;
                customerModel.CountryList = companyService.GetCustomerDefaultCountries(customerId);
                customerModel.CurrencyList = companyService.GetCustomerDefaultCurrencies(customerId);
                customerModel.DateFormatList = primeContractorService.GetDateFormatList();
                customerModel.CustomerUserGroupList = companyService.GetCustomerUserGroupList(customerId);
                customerModel.PartyId = partyId;
                customerModel.CompanyId = companyId;
                customerModel.RoleId = roleId;
                if (customerModel.CustomerUser != null && customerModel.CustomerUser.UserID > 0)
                {
                    ViewBag.userContactId = companyService.GetUserContactIdbyUserId(customerModel.CustomerUser.UserID);
                    var getBuyerMetrics = companyService.GetCustomerBuyerMetricsyUserId(customerModel.CustomerUser.UserID);
                    customerModel.UpdatedByUser = countryService.GetUserNameByUserId(customerModel.CustomerUser.LastUpdatedByUserID);
                    if (getBuyerMetrics != null && getBuyerMetrics.expectedMonthlySpendUSD != null)
                    {
                        customerModel.ExpectedMonthlySpend = Convert.ToDecimal(getBuyerMetrics.expectedMonthlySpendUSD);
                    }
                }


                return View("Customer/CustomerUserCrudView", customerModel);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult AddEditCustomerUserPost(CompanyCustomerModel user)
        {
            try
            {
                var userId = user.CustomerUser.UserID;
                if (companyService.ChecktUserByUsername(user.CustomerUser.UserName, userId))
                {
                    TempData["addMsg"] = "userExist";
                    return RedirectToAction("AddEditCustomerUser",
             new { userid = userId, companyId = user.CompanyId, partyId = user.PartyId, roleId = user.RoleId });
                }

                if (user != null && user.CustomerUser != null && user.CustomerUser.UserID > 0)
                {


                    userId = user.CustomerUser.UserID;
                    var chkMsg = companyService.UpdateCustomerUser(user, adminSetting);

                    TempData["editMsg"] = chkMsg.ToString();
                }
                else
                {

                    var chkMsg = companyService.AddNewCustomerUser(user, adminSetting);
                    TempData["addMsg"] = chkMsg.ToString();
                }

                return RedirectToAction("AddEditCustomerUser",
                    new { userid = userId, companyId = user.CompanyId, partyId = user.PartyId, roleId = user.RoleId });

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult ChkDuplicateUserByName(string userName, int userId)
        {
            var chk = companyService.ChecktUserByUsername(userName, userId);
            return Json(chk, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerDefaultAddressByCountryId(int countryId, int customerId)
        {
            try
            {
                var chk = companyService.Sp_GetCustomerAddressesByCountry_AdminR(countryId, customerId);

                return Json(chk.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerDesitnationCountries(int customerId)
        {
            try
            {
                var countries = companyService.GetCustomerDestinationCountriesByCustomerId(customerId);
                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerDestinationAddressByCountryId(int customerId, int countryId)
        {
            try
            {
                var addresses = companyService.GetCustomerDestinationAddressByCountryId(customerId, countryId);
                return Json(addresses.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerInvoiceCountries(int customerId)
        {
            try
            {
                var countries = companyService.GetCustomerInvoiceCountries(customerId);
                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerInvoiceAddressByUserIdAndDestinationId(int customerId, int destinationId, int countryId, int userId)
        {
            try
            {
                var address = companyService.GetCustomerInvoiceAddressByUserIdAndDestinationId(customerId, userId,
                    destinationId, countryId);
                return Json(address.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult UpdateUserPermissions(List<CustomerUserAddressPermissionModel> list)
        {
            try
            {
                var chk = companyService.UpdateUserAddressPermissions(list, adminSetting);


                return Json(chk, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendCredentialToUser(int userId, int company, int party, int role, int customerId)
        {
            try
            {
                if (userId > 0 && company > 0)
                {
                    var getUserEmail = companyService.GetUserEmail(userId);
                    if (getUserEmail != null)
                    {
                        var pcService = new PrimeContractorService();
                        var getCustomerInfo = companyService.GetCustomerById(customerId);
                        var currentUserEmail = pcService.GetGsContactEmailByCustomerId(customerId);
                        var getUserData = companyService.GetUserById(userId, company);
                        var emailFrom = "Automailer@global-serve.com";

                        var companyName = "";
                        var userName = "";
                        var password = "";

                        if (getUserData != null)
                        {
                            companyName = getUserData.CompanyName ?? "";
                            userName = getUserData.UserName ?? "";
                            password = "<br>Your Password  : " + getUserData.Password ?? "";
                            password += "<br><br><p>Please use the above information to logon to : https://gsonesource.global-serve.com </p>";
                        }

                        if (getCustomerInfo != null && !string.IsNullOrWhiteSpace(getCustomerInfo.OnesourceVersion) && getCustomerInfo.OnesourceVersion == "V3")
                        {
                            var getV3Url = ConfigurationManager.AppSettings["ResetPasswordV3"];

                            var resetData = companyService.AddPasswordResetLink(customerId, userId, adminSetting, getV3Url);

                            var urlToResetPassword = resetData.Url + "/" + resetData.ResetToken;
                            password = "<br>Your Reset Password Link : " + urlToResetPassword;
                        }

                        var body = "Company : " + companyName.Trim().ToString() + "<br>";

                        body += "<br>Dear User  <br>Your User Name : " + userName.Trim().ToString() + " ";

                        body += password;
                        
                        body +=
                            "<p>If you need to contact GlobalServe, please call or fax the Customer Center at : </p>";
                        body += "<p>Americas<br>Phone : +1 (914) 729 6800<br>Fax : (914) 729-6894 </p>";
                        body += "<p>Asia<br>Phone : +886 2 77026800</p>";
                        body += "<p>GlobalServe ONESOURCE : https://gsonesource.global-serve.com </p>";
                        var subject = "GlobalServe ONESOURCE - Login Details";

                        var routeData = this.ControllerContext.RouteData;
                        LogException("EMail Body:"+ body+"\n"+"<br>From:"+ emailFrom+"\n TO:"+ getUserEmail, routeData);

                        SendEmailWithFrom(getUserEmail, body, subject, emailFrom);
                    }
                }
                TempData["emailMsg"] = true;
                return RedirectToAction("AddEditCustomerUser",
                    new { userid = userId, companyId = company, partyId = party, roleId = role });

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");

            }

        }

        [Route("admin/customer/address/{companyId}/{partyId}/{roleId}/{customerId}/{addressId?}")]
        public ActionResult AddEditCustomerAddress(int companyId, int partyId, int roleId, int customerId, int? addressId)
        {
            try
            {
                var model = new CompanyCustomerModel();
                model.Address = new sp_GetAddressDetailByCustomer_AdminR_Result();
                model.PartyId = partyId;
                model.RoleId = roleId;
                model.CompanyId = companyId;
                ViewBag.CustomerId = customerId;
                if (addressId > 0)
                {
                    model.Address = companyService.GetCustomerAddressByIdResult(Convert.ToInt32(addressId), customerId);
                }
                model.TimeZoneList = companyService.GetTimeZoneList();
                model.RegionsList = companyService.GetCompanyRegionsList(companyId);
                model.CountryList = companyService.GetCustomerDefaultCountries(customerId);
                model.CarrierList = companyService.GetCarriersList();
                ViewBag.Editmsg = TempData["editMsg"];
                ViewBag.addMsg = TempData["addMsg"];

                return View("Customer/AddressCrudView", model);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult AddEditCustomerAddressPost(CompanyCustomerModel data)
        {
            try
            {
                var model = new CompanyCustomerModel();
                model.Address = new sp_GetAddressDetailByCustomer_AdminR_Result();

                if (data.Address.CustomerAddressID > 0)
                {
                    var chkMsg = companyService.UpdateCustomerAddress(data, adminSetting);
                    TempData["editMsg"] = chkMsg.ToString();
                }
                else
                {
                    var chkMsg = companyService.AddCustomerAddress(data, adminSetting);
                    TempData["addMsg"] = chkMsg.ToString();
                }

                return RedirectToAction("AddEditCustomerAddress",
                    new { companyId = data.CompanyId, partyId = data.PartyId, roleId = data.RoleId, customerId = data.Customer.CustomerID, addressId = data.Address.CustomerAddressID });

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult GetCustomerDestinationAddressDetails(int customerId, int destinationId)
        {
            try
            {
                var data = companyService.GetCustomerShippedAddressDetailByAddressId(destinationId, customerId);

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerCountries(int customerId)
        {
            try
            {
                var countries = companyService.GetCustomerDefaultCountries(customerId);
                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerInvoiceAddressCountries(int customerId)
        {
            try
            {
                var countries = companyService.GetCustomerInvoiceAddressCountries(customerId);
                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerInvoiceAddressByDestinationId(int customerId, int destinationId, int countryId)
        {
            try
            {
                var addressList = companyService.GetCustomerInvoiceAddressesDestinationId(customerId, destinationId, countryId);
                return Json(addressList.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateCustomerInvoiceAddress(List<CustomerUserAddressPermissionModel> list)
        {
            try
            {
                var chk = companyService.UpdateCustomerInvoiceAddress(list, adminSetting);


                return Json(chk, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        //Customer Additional Setup start

        public JsonResult GetPreferredSupplierByCountryAndManufacturer(string basedOn, int basedOnId, int manufacturerId,
            int customerId)
        {
            try
            {
                var suppliers = companyService.GetPreferredSUppliers(basedOn, basedOnId, manufacturerId);
                var selectedId = -1;
                if (basedOn == "country" && customerId > 0)
                {
                    selectedId = companyService.GetSelectedSupplierId(manufacturerId, basedOnId, customerId);
                }
                if (basedOn == "address" && customerId > 0)
                {
                    selectedId = companyService.GetSelectedSupplierForAddressId(manufacturerId, basedOnId, customerId);
                }

                var data = new
                {
                    suppliers = suppliers,
                    selectedId = selectedId
                };
                return Json(data, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateSupplierGenPrefernce(int customerId, int manufacturerId, int countryId, int sfaId)
        {
            try
            {
                var update = companyService.UpdateSupplierGenPreference(customerId, manufacturerId, countryId, sfaId, adminSetting.UserID);
                return Json(update, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateSupplierPreferenceByDestination(int customerId, int manufacturerId, int destinationId, int sfaId)
        {
            try
            {
                var update = companyService.UpdateSupplierPreferenceByDestinations(customerId, manufacturerId, destinationId, sfaId, adminSetting.UserID);
                return Json(update, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerCountryPreferedSuppliers(int customerId, int countryId)
        {
            try
            {
                var sfaList = companyService.GetCountryPreferedSupplierList(customerId, countryId);
                if (sfaList != null && sfaList.Count > 0)
                {
                    return Json(sfaList, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSfaUpliftsById(int customerId, int countryId, int sfaId)
        {
            try
            {
                var uplifts = companyService.GetSupplierStandardUpliftsBySfaId(customerId, countryId, sfaId, 1);


                return Json(uplifts, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateSfaUplifts(int customerId, int countryId, int sfaId, decimal? sfaStandardUplifts, decimal? gsUplifts, bool isPrefered)
        {
            try
            {
                var uplifts = companyService.AddUpdateSfaUplifts(customerId, countryId, sfaId, 1, Convert.ToDecimal(sfaStandardUplifts), Convert.ToDecimal(gsUplifts), isPrefered, adminSetting.UserID);

                return Json(uplifts, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerTbuUpliftsById(int customerId, int countryId, int manufacturerId)
        {
            try
            {
                var uplifts = companyService.GetCustomerTbuUplifts(customerId, countryId, manufacturerId);
                if (uplifts.TradeBlockUplift > 0 || uplifts.Duties > 0)
                {

                    var data = new
                    {
                        duties = uplifts.Duties * 100,
                        blockUplifts = uplifts.TradeBlockUplift * 100
                    };

                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json(null, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateTbuDuties(int customerId, int countryId, int manufacturerId, decimal? blockUplifts, decimal? duties)
        {
            try
            {
                var uplifts = companyService.AddTradeBlockUplifts(customerId, countryId, manufacturerId, Convert.ToDecimal(blockUplifts), Convert.ToDecimal(duties), adminSetting.UserID);

                return Json(uplifts, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSpecialUpliftsByCountry(int customerId, int countryId)
        {
            try
            {
                var specialUlifts = companyService.Sp_GetSpecialUpliftsByCountryId(customerId, countryId);

                return PartialView("Customer/_SpecialUpliftsListPartial", specialUlifts);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult GetSpecialUpliftsManufacturersBySupplier(int customerId, int countryId, int sfaId)
        {
            try
            {
                var manuList = companyService.GetSpecialUpliftsManufacturersBySupplier(customerId, countryId, sfaId);

                return Json(manuList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddSpecialUplifts(int customerId, int countryId, int sfaId, int manfId, int? supplierUplifts, int? gsUplifts)
        {
            try
            {
                var uplifts = companyService.AddSpecialUplifts(customerId, countryId, sfaId, manfId, Convert.ToInt32(supplierUplifts), Convert.ToInt32(gsUplifts), adminSetting.UserID);

                return Json(uplifts, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateSpecialUplifts(int sfaUpliftId, int? supplierUplifts, int? gsUplifts)
        {
            try
            {
                var uplifts = companyService.UpdateSpecialUplifts(sfaUpliftId, Convert.ToInt32(supplierUplifts), Convert.ToInt32(gsUplifts), adminSetting.UserID);

                return Json(uplifts, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteSfaUpliftById(int sfaUpliftId)
        {
            try
            {
                var uplifts = companyService.DeleteManftUpliftById(sfaUpliftId);

                return Json(uplifts, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        //Customer Additional Setup End

        //For customer user prefrences  start

        public JsonResult GetDefaultHomepageByCustomerId(int customerId, string homePageValue, bool isForAllusers)
        {
            try
            {
                var updateHomePage = companyService.UpdateCustomerDefaultHomepage(customerId, homePageValue, isForAllusers, adminSetting.UserID);
                return Json(updateHomePage, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        //For customer user prefrences  end

        public JsonResult GetCustomerAddressesByCountryId(int customerId, int countryId)
        {
            try
            {
                var addresses = companyService.GetCustomerAddressesWithCountryName(customerId, countryId);


                return Json(addresses.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Supplier Company

        /// <summary>
        /// this action is responsible for providing supplier info
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult GetSupplierInfo(int companyId, int custserviceId, int selectedCustomerId, bool isNone = false)
        {
            try
            {
                if (companyId > 0)
                {
                    if (isNone)
                    {
                        Session["SelectedCustomerId"] = null;
                    }
                    if (selectedCustomerId == 0 && Session["SelectedCustomerId"] != null)
                    {
                        selectedCustomerId = Convert.ToInt32(Session["SelectedCustomerId"]);
                    }


                    var data = companyService.GetSupplierInfo(companyId, custserviceId, selectedCustomerId);
                    return PartialView("~/Views/Company/Supplier/_supplierInfo.cshtml", data);
                }
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating supplier information area at supplier info tab
        /// </summary>
        /// <param name="smodel"></param>
        /// <returns></returns>
        public ActionResult UpdateSupplierInformation(SupplierInfoModel smodel)
        {
            if (smodel != null)
            {
                if (smodel.SelectedCustomerId != 0)
                {
                    Session["SelectedCustomerId"] = smodel.SelectedCustomerId;
                }
                if (smodel.CustServiceId > 0)
                {
                    Session["SelectedCustServiceId"] = smodel.CustServiceId;
                }
                int count = companyService.UpdateSupplierInformation(smodel, Convert.ToInt32(Session["UserID"]));
                TempData["Message"] = count > 0 ? "Record updated successfully" : "No record was updated";
            }
            return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
        }

        /// <summary>
        /// this action is responsible for updating customer information area at supplier info tab
        /// </summary>
        /// <param name="smodel"></param>
        /// <returns></returns>
        public ActionResult UpdateCustomerInformation(SupplierInfoModel smodel)
        {
            try
            {
                if (smodel != null)
                {
                    if (smodel.SelectedCustomerId != 0)
                    {
                        Session["SelectedCustomerId"] = smodel.SelectedCustomerId;
                    }
                    if (smodel.CustServiceId > 0)
                    {
                        Session["SelectedCustServiceId"] = smodel.CustServiceId;
                    }
                    int count = companyService.UpdateCustomerInformation(smodel, Convert.ToInt32(Session["UserID"]));
                }
                TempData["Message"] = "Record updated successfully";
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for getting supplier contacts 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetSupplierContacts()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    return PartialView("~/Views/Company/Supplier/_contactModal.cshtml", companyService.GetSupplierContacts());
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// this action is responsible for getting supplier accredications
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="custserviceId"></param>
        /// <param name="selectedCustomerId"></param>
        /// <returns></returns>
        public ActionResult GetSupplierAccreditation(int companyId, int custserviceId, int selectedCustomerId)
        {
            try
            {
                if (companyId > 0)
                {
                    ViewBag.Message = TempData["Message"];
                    var model = companyService.GetSupplierAccreditation(companyId, custserviceId);
                    return PartialView("~/Views/Company/Supplier/_supplierAccreditation.cshtml", model);
                }
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating supplier accredation
        /// </summary>
        /// <param name="custserviceId"></param>
        /// <param name="companyId"></param>
        /// <param name="mfgs"></param>
        /// <returns></returns>
        public ActionResult UpdateSupplierAccreditation(int custserviceId, int companyId, string mfgs)
        {
            if (custserviceId > 0 && companyId > 0)
            {
                int count = companyService.UpdateSupplierAccreditation(custserviceId, companyId, Convert.ToInt32(Session["UserID"]), mfgs);
                TempData["Message"] = count > 0 ? "Record updated successfully" : "Your changes could not be saved at this time.";
                return RedirectToAction("GetSupplierAccreditation", new { @companyId = companyId, @custserviceId = custserviceId, @selectedCustomerId = 0 });
            }
            return null;
        }

        /// <summary>
        /// this action is responsible for getting supplier services
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult GetSupplierServices(int companyId)
        {
            try
            {
                if (companyId > 0)
                {
                    ViewBag.Message = TempData["Message"];
                    var model = companyService.GetSupplierServices(companyId);
                    return PartialView("~/Views/Company/Supplier/_supplierServices.cshtml", model);
                }
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating supplier services
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="serviceIds"></param>
        /// <returns></returns>
        public ActionResult UpdateSupplierService(int companyId, string serviceIds)
        {
            if (companyId > 0)
            {
                int count = companyService.UpdateSupplierService(companyId, Convert.ToInt32(Session["UserID"]), serviceIds);
                TempData["Message"] = count > 0 ? "Record updated successfully" : "Your changes could not be saved at this time.";
                return RedirectToAction("GetSupplierServices", new { @companyId = companyId });
            }
            return null;
        }

        /// <summary>
        /// this action is responsible for providing supplier address view
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="addrId"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        [Route("company/supplier-address/{tid?}/{addrId?}/{partyId?}")]
        public ActionResult SupplierAddress(int tid, int addrId, int partyId)
        {
            try
            {
                if (Session["PreviousUrlSupplierAddr"] == null)
                {
                    if (Request.UrlReferrer != null)
                    {
                        Session["PreviousUrlSupplierAddr"] = Request.UrlReferrer.ToString();
                    }
                }
                else
                {
                    if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Split('?')[0] == Session["PreviousUrlSupplierAddr"].ToString().Split('?')[0])
                    {
                        Session["PreviousUrlSupplierAddr"] = Request.UrlReferrer.ToString();
                    }
                }

                ViewBag.CurrentAddressId = TempData["CurrentAddressId"];
                ViewBag.Message = TempData["Message"];

                CompanyPresenter cp = new CompanyPresenter();
                CompanySupplierAddressModel model = cp.GetSupplierAddressInfo(addrId);
                if (Session["SupplierAddrModel"] != null)
                {
                    model.AddressInfo = (sp_GetAddressInfo_AdminR_Result)Session["SupplierAddrModel"];
                    Session["SupplierAddrModel"] = null;
                }
                model.PartyId = partyId;

                ViewBag.CorporateAddressId = Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]);
                return View("~/Views/Company/Supplier/AddEditSupplierAddress.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for add and edit operation of supplier address
        /// </summary>
        /// <param name="SupplierAddrModel"></param>
        /// <param name="partyId"></param>
        /// <returns></returns>
        public ActionResult AddEditSupplierAddress(sp_GetAddressInfo_AdminR_Result SupplierAddrModel, int partyId)
        {
            try
            {
                if (SupplierAddrModel != null)
                {
                    CompanyPresenter cp = new CompanyPresenter();
                    if (SupplierAddrModel.AddressId == 0)
                    {
                        var data = cp.CreateAddress(SupplierAddrModel, Convert.ToInt32(Session["UserID"]), partyId, Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]));
                        if (data != null)
                        {
                            var addressId = data.GetType().GetProperty("addressId").GetValue(data).ToString();
                            var partylocationId = data.GetType().GetProperty("partylocationId").GetValue(data).ToString();
                            var addCount = Convert.ToInt32(data.GetType().GetProperty("addCount").GetValue(data));

                            if (addCount > 0)
                                TempData["Message"] = "Address has been added successfully";

                            else if (addCount == -1)
                            {
                                TempData["Message"] = "Please setup one address of type Corporate Headquarter";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                Session["SupplierAddrModel"] = SupplierAddrModel;

                            }
                            else if (addCount == -2)
                            {
                                TempData["Message"] = "Only one address can be of type Corporate Headquaters";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                Session["SupplierAddrModel"] = SupplierAddrModel;
                            }

                            //return Redirect("/company/supplier-address/3/" + addressId + "/" + partyId);

                            if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && addCount > 0)
                            {
                                return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                            }
                            else
                            {
                                //return RedirectToAction("Company", new { @companyId = -1, @partyId = 0, @companyRole = 0});
                                return Redirect("/company/supplier-address/3/" + addressId + "/" + partyId);
                            }
                        }
                        else
                        {
                            TempData["Message"] = "Error Occured";
                            return Redirect("/company/supplier-address/3/" + 0 + "/" + partyId);
                        }
                    }
                    else
                    {
                        var data = cp.UpdateAddress(SupplierAddrModel, Convert.ToInt32(Session["UserID"]), partyId, Convert.ToInt32(ConfigurationManager.AppSettings["CorporateHeadquartersId"]));
                        if (data != null)
                        {
                            var addressId = data.GetType().GetProperty("addressId").GetValue(data).ToString();
                            var updateCount = Convert.ToInt32(data.GetType().GetProperty("updateCount").GetValue(data));
                            if (updateCount > 0)
                                TempData["Message"] = "Address has been updated successfully";
                            else if (updateCount == -2)
                            {
                                TempData["Message"] = "Only one address can be of type Corporate Headquaters";
                                var previousAddressTypeId = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));
                                SupplierAddrModel.AddressTypeId = previousAddressTypeId;

                                TempData["CurrentAddressId"] = previousAddressTypeId;
                                addressId = SupplierAddrModel.AddressId.ToString();
                            }
                            else if (updateCount == -1)
                            {
                                TempData["Message"] = "Please setup one address of type Corporate Headquarter";
                                TempData["CurrentAddressId"] = Convert.ToInt32(data.GetType().GetProperty("previousAddressTypeId").GetValue(data));

                                addressId = SupplierAddrModel.AddressId.ToString();
                            }


                            return Redirect("/company/supplier-address/3/" + addressId + "/" + partyId);

                        }
                        else
                        {
                            TempData["Message"] = "Error Occured";
                            return Redirect("/company/supplier-address/3/" + 0 + "/" + partyId);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating supplier's user
        /// </summary>       
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("company/supplier-user/{userId?}")]
        public ActionResult SupplierUser(int userId)
        {
            try
            {
                if (Session["PreviousUrlSupplierUsr"] == null)
                {
                    if (Request.UrlReferrer != null)
                    {
                        Session["PreviousUrlSupplierUsr"] = Request.UrlReferrer.ToString();
                    }
                }
                else
                {
                    if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Split('?')[0] == Session["PreviousUrlSupplierUsr"].ToString().Split('?')[0])
                    {
                        Session["PreviousUrlSupplierUsr"] = Request.UrlReferrer.ToString();
                    }
                }


                ViewBag.Message = TempData["Message"];
                CompanyPresenter cp = new CompanyPresenter();
                CompanySupplierUserModel model = cp.GetSupplierUserDetailModel(userId);


                return View("~/Views/Company/Supplier/AddEditSupplierUser.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for add edit supplier's user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddEditSupplierUser(CompanySupplierUserInfo model)
        {
            try
            {
                int count = 0;
                CompanyPresenter cp = new CompanyPresenter();
                if (model.UserId > 0)
                {
                    count = cp.UpdateSupplierUser(model, Convert.ToInt32(Session["UserID"]));
                    if (count == -1) TempData["Message"] = "A user with username: '" + model.UserName + "' already exists";
                    else
                    {
                        TempData["Message"] = count > 0 ? "User has been updated successfully" : "Error Occured";
                    }
                }
                else
                {
                    count = cp.AddSupplierUser(model, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["CurrentSFAID"]), Convert.ToInt32(Session["ParentCompanyID"]));
                    if (count == -1) TempData["Message"] = "A user with username: '" + model.UserName + "' already exists";
                    else { TempData["Message"] = count > 0 ? "User has been added successfully" : "Error Occured"; }

                    if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && count > 0)
                    {
                        return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                    }
                    else
                    {
                        return Redirect("/company/supplier-user/" + model.UserId);

                    }
                }

                return Redirect("/company/supplier-user/" + model.UserId);


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for deleting the supplier address
        /// </summary>
        /// <param name="partyId"></param>
        /// <param name="addressId"></param>
        /// <returns></returns>
        public ActionResult DeleteSupplierAddress(int partyId, int addressId)
        {
            try
            {
                CompanyPresenter cp = new CompanyPresenter();
                var Count = cp.DeleteAddress(partyId, addressId);
                TempData["Message"] = Count > 0 ? "Address has been deleted successfully" : "Error Occured";

                if (Session["cCompanyId"] != null && Session["cRoleId"] != null && Session["cPartyId"] != null && Count > 0)
                {
                    return RedirectToAction("Company", new { @companyId = Convert.ToInt32(Session["cCompanyId"]), @partyId = Convert.ToInt32(Session["cPartyId"]), @companyRole = Convert.ToInt32(Session["cRoleId"]) });
                }
                else
                {
                    return Redirect("/company/manufacturer-address/3/0/" + partyId);
                }



            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing supplier additional setup view
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult GetSupplierAdditionSetup(int companyId)
        {
            try
            {
                if (companyId > 0)
                {
                    ViewBag.Message = TempData["Message"];
                    var model = companyService.GetSupplierGenManufacturer(companyId);
                    return PartialView("~/Views/Company/Supplier/_additionalSetup.cshtml", model);
                }
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        ///  this action is responsible for providing supplier additional setup tab 'Distribution uplifts'
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult GetSupplierDistri(int companyId)
        {
            try
            {
                if (companyId > 0)
                {
                    ViewBag.Message = TempData["Message"];
                    var model = companyService.GetSupplierManufacturer(companyId);
                    model.Insert(0, new OneSourceAdminCore.Model.CompanySupplierAddSetupModel { Id = 0, Name = "(None)" });
                    return PartialView("~/Views/Company/Supplier/_additionalSetupDistri.cshtml", model);
                }
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing countries and thier distribution uplifts by manufacturerid
        /// </summary>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        public JsonResult GetManufacturerCountries(int mfgId)
        {
            var model = companyService.GetSupplierMfgCountry(mfgId, Convert.ToInt32(Session["CurrentSFAID"]));
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///  this action is responsible for updating distribution uplifts
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="distributionUplift"></param>
        /// <returns></returns>
        public JsonResult UpdateDistributionUplift(int mfgId, decimal? distributionUplift)
        {
            var count = companyService.UpdateDistributionUplift(mfgId, Convert.ToInt32(Session["CurrentSFAID"]), Convert.ToInt32(Session["UserID"]), distributionUplift);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for updating country distribution uplifts
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="countryId"></param>
        /// <param name="distributionUplift"></param>
        /// <returns></returns>
        public JsonResult UpdateCountryDistributionUplift(int mfgId, int countryId, decimal? distributionUplift)
        {
            var count = companyService.UpdateCountryDistributionUplift(mfgId, countryId, Convert.ToInt32(Session["CurrentSFAID"]), Convert.ToInt32(Session["UserID"]), distributionUplift);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this action is responsible for getting supplier manufacturer's tier status
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult GetSupplierMfgTier(int companyId)
        {
            try
            {
                if (companyId > 0)
                {
                    ViewBag.Message = TempData["Message"];
                    var model = companyService.GetSupplierManufacturerTier(companyId);
                    ViewBag.Tiers = companyService.GetTiers();
                    return PartialView("~/Views/Company/Supplier/_additionalMfgTier.cshtml", model);
                }
                return RedirectToAction("Company", new { @companyId = Session["cCompanyId"].ToString(), partyId = Session["cPartyId"].ToString(), companyRole = Session["cRoleId"].ToString() });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for updating manufacturer tier
        /// </summary>
        /// <param name="mfgId"></param>
        /// <param name="comment"></param>
        /// <param name="tierId"></param>
        /// <returns></returns>
        public JsonResult UpdateManufacturerTier(int mfgId, string comment, int? tierId)
        {
            var count = companyService.UpdateManufacturerTier(mfgId, tierId, Convert.ToInt32(Session["CurrentSFAID"]), Convert.ToInt32(Session["UserID"]), comment);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendSupplierUserPassword(string username, string email, int userId)
        {
            try
            {
                UserCredentialModel model = companyService.SendSupplierUserPassword(username, Convert.ToInt32(Session["cCompanyId"]), userId);
                if (model != null)
                {
                    var body = "Company : " + model.CompanyName + "<br>";
                    body += "<br>Dear User  <br>Your User Name : " + model.UserName.Trim().ToString() + " " +
                            "<br>Your Password  : " + model.Password;
                    body +=
                        "<br><p>Please use the above information to logon to : https://gsonesource.global-serve.com </p>";
                    body +=
                        "<p>If you need to contact GlobalServe, please call or fax the Customer Center at : </p>";
                    body += "<p>Americas<br>Phone : +1 (914) 729 6800<br>Fax : (914) 729-6894 </p>";
                    body += "<p>Asia<br>Phone : +886 2 77026800</p>";
                    body += "<p>GlobalServe ONESOURCE : https://gsonesource.global-serve.com </p>";
                    var subject = "GlobalServe ONESOURCE - Login Details";
                    SendEmailWithFrom(email, body, subject, "fmg@global-serve.com");
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }

        }

        #endregion


        #region Customer Catalog Start

        [HttpPost]
        public JsonResult CatalogSearch(string searchTxt, int? priceCodeId, int catalogType)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var catalogSearchModel = new CatalogSearchModel();
                    catalogSearchModel.PriceCodeId = priceCodeId ?? 1;
                    catalogSearchModel.SearchText = searchTxt;
                    catalogSearchModel.CatalogType = catalogType;

                    Session["SearchCatalogModel"] = catalogSearchModel;

                    return Json("1", JsonRequestBehavior.AllowGet);
                }

                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("-1", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult CustomerCatalogAddEdit(int companyId, int partyId, int customerId, int partId, int priceCode)
        {
            try
            {

                ViewBag.CompanyId = companyId;
                ViewBag.PartyId = partyId;
                ViewBag.RoleId = 7;
                ViewBag.CustomerId = customerId;
                ViewBag.PartId = partId;
                ViewBag.PriceCode = priceCode;


                ViewBag.result = TempData["result"];
                ViewBag.CatalogDuplicate = TempData["CatalogDuplicate"];
                ViewBag.AddMsg = TempData["AddMsg"];
                ViewBag.EditMsg = TempData["EditMsg"];

                var catService = new CustomerCatalogService();
                var productMaintainceService = new ProductMaintenancePresent();
                var catModel = new CustomerCatalogModel();

                catModel.PartCategoryList = catService.GetGsPartCategoriesListByCompany(companyId);
                catModel.SupplierList = catService.GetActiveSuppliers();
                catModel.CurrencyList = companyService.GetCustomerDefaultCurrencies(customerId);
                catModel.CountriFyList = companyService.GetCustomerDefaultCountries(customerId);
                catModel.CountriFyList.Add(new GenericList() { Value = "None", Key = 0 });
                catModel.PriceCodeList = productMaintainceService.GetPriceCodeList();
                catModel.UnspscList = productMaintainceService.GetUnsPsCodesList();
                catModel.CatalogPart = new tblCatalogPart();
                catModel.CatalogPart.CustomerID = customerId;
                catModel.CompanyId = companyId;
                catModel.PartyId = partyId;
                catModel.CatalogPart.CatalogPartID = partId;

                catModel.CountrifyCountryList = new List<GenericList>();
                if (partId > 0)
                {
                    catModel.CatalogPart = catService.GetCatalogPartById(customerId, partId);
                    if (catModel.CatalogPart != null && catModel.CatalogPart.ManufacturerPartID > 0)
                    {
                        catModel.ManfPart = catService.GetManfPartById(catModel.CatalogPart.ManufacturerPartID);
                        catModel.CountrifyCountryList = catService.GetQuickSettingCountryList(customerId, partId, priceCode);
                    }
                }
                else
                {
                    catModel.CatalogPart.GlobalPriceCurrencyID = 12; //us dollar
                    catModel.CatalogPart.UNSPSC = "43000000";
                    catModel.CatalogPart.SFAID = 1; //supplier none
                    catModel.Eol = DateTime.Now.AddMonths(1);
                    catModel.CatalogPart.PriceCodeID = priceCode;
                    catModel.CountrifyIn = 0;

                }

                return View("~/Views/CustomerCatalog/CatalogCrudView.cshtml", catModel);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [NoCache]
        public JsonResult GetManfPartSearch(int? page, int? limit, string sortBy, string direction,
            string searchString = null)
        {
            var catalogService = new CustomerCatalogService();
            int total;
            var records = catalogService.GetManfParts(page, limit, sortBy, direction, searchString, out total);
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditCustomerCatalog(CustomerCatalogModel catalogModel)
        {
            try
            {
                var catalogService = new CustomerCatalogService();
                if (catalogModel.CatalogPart.CustomerID > 0 && catalogModel.CatalogPart.Description.Length > 0 &&
                    catalogModel.CatalogPart.GlobalPrice > 0)
                {

                    if (catalogService.IsDuplicate(catalogModel.CatalogPart.ManufacturerPartID,
                        catalogModel.CatalogPart.SFAID, catalogModel.CatalogPart.PriceCodeID,
                        catalogModel.CatalogPart.CustomerID, catalogModel.CatalogPart.CatalogPartID))
                    {
                        TempData["CatalogDuplicate"] = "true";
                    }
                    else
                    {

                        if (catalogModel.CatalogPart.CatalogPartID > 0)
                        {
                            var result = catalogService.UpdateCatalogPart(catalogModel, adminSetting);

                            if (result.Contains("true"))
                            {
                                TempData["EditMsg"] = "true";
                            }
                            else
                            {
                                TempData["EditMsg"] = "false";
                            }
                        }
                        else
                        {
                            var result = catalogService.AddNewCatalogPart(catalogModel, adminSetting);

                            if (result.Contains("CatalogId"))
                            {
                                var catalogId = Convert.ToInt32(result.Split(':')[1]);
                                if (catalogId != null && catalogId > 0)
                                {
                                    catalogModel.CatalogPart.CatalogPartID = catalogId;
                                    TempData["AddMsg"] = "true";
                                }
                                else
                                {
                                    TempData["AddMsg"] = "false";
                                }
                                //  ViewBag.CatalogDuplicate = TempData["CatalogDuplicate"];

                            }
                            else
                            {
                                TempData["AddMsg"] = "false";
                            }
                        }

                    }
                }
                else
                {
                    TempData["result"] = "invalid";
                }
                return RedirectToAction("CustomerCatalogAddEdit", new { @companyId = catalogModel.CompanyId, @partyId = catalogModel.PartyId, @customerId = catalogModel.CatalogPart.CustomerID, @partId = catalogModel.CatalogPart.CatalogPartID, @priceCode = catalogModel.CatalogPart.PriceCodeID });


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }



        public ActionResult CustomerCatalogCountrifyPartAddEdit(int companyId, int partyId, int customerId, int partId, int priceCode, int countryId)
        {
            try
            {
                ViewBag.CompanyId = companyId;
                ViewBag.PartyId = partyId;
                ViewBag.RoleId = 7;
                ViewBag.CustomerId = customerId;
                ViewBag.PartId = partId;
                ViewBag.PriceCode = priceCode;
                ViewBag.CountryId = countryId;

                ViewBag.result = TempData["result"];

                ViewBag.AddMsg = TempData["AddMsg"];
                ViewBag.EditMsg = TempData["EditMsg"];

                var catService = new CustomerCatalogService();
                var productMaintainceService = new ProductMaintenancePresent();
                var catModel = new CustomerCatalogModel();

                catModel.PartCategoryList = catService.GetGsPartCategoriesListByCompany(companyId);
                catModel.SupplierList = catService.GetActiveSuppliers();
                catModel.CurrencyList = companyService.GetCustomerDefaultCurrencies(customerId);
                catModel.CountriFyList = companyService.GetCustomerDefaultCountries(customerId);
                catModel.CountriFyList.Add(new GenericList() { Value = "None", Key = 0 });
                catModel.PriceCodeList = productMaintainceService.GetPriceCodeList();
                catModel.UnspscList = productMaintainceService.GetUnsPsCodesList();
                catModel.CatalogPart = new tblCatalogPart();
                catModel.CatalogPart.CustomerID = customerId;
                catModel.CompanyId = companyId;
                catModel.PartyId = partyId;
                catModel.CatalogPart.CatalogPartID = partId;

                if (partId > 0)
                {
                    catModel.CatalogPart = catService.GetCatalogPartById(customerId, partId);
                    if (catModel.CatalogPart != null && catModel.CatalogPart.ManufacturerPartID > 0)
                    {
                        catModel.ManfPart = catService.GetManfPartById(catModel.CatalogPart.ManufacturerPartID);


                        catModel.CountrifyCountryList = catService.GetCatalogCountrifyCountries(customerId, partId, priceCode);
                        catModel.CountryPartInfo = new Sp_GetCustomerCountryPartDetail_AdminR_Result();

                        catModel.CountryPartInfo.DestinationCountryID = catModel.CountrifyCountryList.FirstOrDefault().Key;
                        ViewBag.CountryId = catModel.CountryPartInfo.DestinationCountryID;
                        if (catModel.CountryPartInfo.DestinationCountryID > 0)
                        {
                            catModel.CountryPartInfo = catService.GetCountryPartInfo(customerId, partId, countryId);
                            if (catModel.CountryPartInfo == null || catModel.CountryPartInfo.CatalogPartID == 0)
                            {
                                catModel.CountryPartInfo.CatalogPartID = partId;
                                catModel.CountryPartInfo.DestinationCountryID = countryId;
                                var chkCountry = catModel.CountriFyList.FirstOrDefault(x => x.Key == countryId);

                                if (chkCountry != null)
                                {
                                    catModel.CountryPartInfo.CountryName = chkCountry.Value;
                                }

                                catModel.CountryPartInfo.EOL = DateTime.Now.AddMonths(1);
                                catModel.CountryPartInfo.PartPrice = catModel.CatalogPart.GlobalPrice;
                                catModel.CountryPartInfo.PartPriceCurrencyID = catModel.CatalogPart.GlobalPriceCurrencyID;
                                catModel.CountryPartInfo.IsActive = true;
                                catModel.CountryPartInfo.PriceCodeID = priceCode;
                                catModel.CountryPartInfo.PartDescription = catModel.CatalogPart.Description;
                                catModel.CountryPartInfo.PartCategoryID = catModel.CatalogPart.PartCategoryID;
                                catModel.CountryPartInfo.DestinationCountryID = countryId;

                                catModel.CatalogPart.UNSPSC = "43000000";
                                catModel.CountryPartInfo.SFAID = 1; //supplier none


                            }


                        }

                    }


                }
                else
                {
                    catModel.CatalogPart.GlobalPriceCurrencyID = 12; //us dollar
                    catModel.CatalogPart.UNSPSC = "43000000";
                    catModel.CatalogPart.SFAID = 1; //supplier none
                    catModel.Eol = DateTime.Now.AddMonths(1);
                    catModel.CatalogPart.PriceCodeID = priceCode;
                    catModel.CountrifyIn = 0;

                }
                return View("~/Views/CustomerCatalog/_CustomerCatalogCountrifyPartView.cshtml", catModel);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditCustomerCountrifyCatalog(CustomerCatalogModel catalogModel)
        {
            try
            {
                var catalogService = new CustomerCatalogService();
                if (catalogModel.CountryPartInfo.CatalogPartID > 0 && catalogModel.CountryPartInfo.ManufacturerPartID > 0 &&
                    catalogModel.CountryPartInfo.PartPrice > 0 && catalogModel.CountryPartInfo.PartDescription.Length > 0)
                {

                    if (catalogModel.CountryPartInfo.CustomerPartID > 0)
                    {
                        var result = catalogService.UpdateCustomerCountryPart(catalogModel, adminSetting);

                        if (result.Contains("true"))
                        {
                            TempData["EditMsg"] = "true";
                        }
                        else
                        {
                            TempData["EditMsg"] = "false";
                        }
                    }
                    else
                    {
                        var result = catalogService.AddCustomerCountryPart(catalogModel, adminSetting);

                        if (result.Contains("CustomerPartId"))
                        {
                            var customerPartId = Convert.ToInt32(result.Split(':')[1]);
                            if (customerPartId != null && customerPartId > 0)
                            {
                                catalogModel.CountryPartInfo.CustomerPartID = customerPartId;
                                TempData["AddMsg"] = "true";
                            }
                            else
                            {
                                TempData["AddMsg"] = "false";
                            }
                        }
                        else
                        {
                            TempData["AddMsg"] = "false";
                        }
                    }


                }
                else
                {
                    TempData["result"] = "invalid";
                }
                return RedirectToAction("CustomerCatalogCountrifyPartAddEdit", new { @companyId = catalogModel.CompanyId, @partyId = catalogModel.PartyId, @customerId = catalogModel.CustomerId, @partId = catalogModel.CatalogPart.CatalogPartID, @priceCode = catalogModel.CatalogPart.PriceCodeID, @countryId = catalogModel.CountryPartInfo.DestinationCountryID });


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public JsonResult UpdateCatalogPartQuickSettings(List<QuickSettingModel> list)
        {
            try
            {
                var catService = new CustomerCatalogService();
                var chk = catService.UpdateCatalogQuickSettings(list, adminSetting);

                return Json(chk, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult CustomerBundleCatalogAddEdit(int companyId, int partyId, int customerId, int catalogStdId)
        {
            try
            {

                ViewBag.CompanyId = companyId;
                ViewBag.PartyId = partyId;
                ViewBag.RoleId = 7;
                ViewBag.CustomerId = customerId;


                ViewBag.result = TempData["result"];
                ViewBag.CatalogDuplicate = TempData["CatalogDuplicate"];
                ViewBag.AddMsg = TempData["AddMsg"];
                ViewBag.EditMsg = TempData["EditMsg"];

                var catService = new CustomerCatalogService();
                var catModel = new CustomerCatalogModel();

                catModel.CompanyId = companyId;
                catModel.PartyId = partyId;

                catModel.CustomerId = customerId;

                catModel.PartCategoryList = catService.GetGsPartCategoriesListByCompany(companyId);

                catModel.CountriFyList = companyService.GetCustomerDefaultCountries(customerId);
                catModel.SupplierList = catService.GetActiveSuppliers();
                catModel.CatalogStandard = new tblCatalogStandard();
                catModel.CatalogStandard.CustomerID = customerId;

                if (catalogStdId > 0)
                {
                    catModel.CatalogStandard = catService.GetCatalogStandardById(customerId, catalogStdId);
                    catModel.BundleParts = catService.GetBundlesPartsById(catalogStdId);
                }

                return View("~/Views/CustomerCatalog/BundlesCrudView.cshtml", catModel);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        [NoCache]
        public JsonResult GetCustomerCatalogParts(int customerId, int? page, int? limit, string sortBy, string direction,
            string searchString = null)
        {
            var catalogService = new CustomerCatalogService();
            int total;
            var records = catalogService.GetCustomerCatalogPartsByCustomerId(customerId, page, limit, sortBy, direction,
                searchString, out total);
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }



        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditCustomerBundle(CustomerCatalogModel catalogModel)
        {
            try
            {
                var catalogService = new CustomerCatalogService();
                if (catalogModel.CatalogStandard.CustomerID > 0 && catalogModel.CatalogStandard.Description.Length > 0 &&
                    !string.IsNullOrWhiteSpace(catalogModel.CatalogStandard.CountryIDList) && catalogModel.BundleParts != null && catalogModel.BundleParts.Any(x => x.CatalogPartId > 0))
                {

                    if (catalogService.IsDuplicateBundleCheck(1, 1, catalogModel.CatalogStandard.CustomerID, catalogModel.CatalogStandard.CatalogStandardID, catalogModel.CatalogStandard.Description))
                    {
                        TempData["CatalogDuplicate"] = "true";
                    }
                    else
                    {

                        if (catalogModel.CatalogStandard.CatalogStandardID > 0)
                        {
                            var result = catalogService.UpdateBundle(catalogModel, adminSetting);

                            if (result.Contains("true"))
                            {
                                TempData["EditMsg"] = "true";
                            }
                            else
                            {
                                TempData["EditMsg"] = "false";
                            }
                        }
                        else
                        {
                            var result = catalogService.AddNewBundle(catalogModel, adminSetting);

                            if (result.Contains("CatStdId"))
                            {
                                var catStdId = Convert.ToInt32(result.Split(':')[1]);
                                if (catStdId != null && catStdId > 0)
                                {
                                    catalogModel.CatalogStandard.CatalogStandardID = catStdId;
                                    TempData["AddMsg"] = "true";
                                }
                                else
                                {
                                    TempData["AddMsg"] = "false";
                                }
                                //  ViewBag.CatalogDuplicate = TempData["CatalogDuplicate"];

                            }
                            else
                            {
                                TempData["AddMsg"] = "false";
                            }
                        }

                    }
                }
                else
                {
                    TempData["result"] = "invalid";
                }
                return RedirectToAction("CustomerBundleCatalogAddEdit", new { @companyId = catalogModel.CompanyId, @partyId = catalogModel.PartyId, @customerId = catalogModel.CatalogStandard.CustomerID, @catalogStdId = catalogModel.CatalogStandard.CatalogStandardID });


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult BundleCategorySort(int companyId, int partyId, int customerId)
        {
            try
            {
                ViewBag.CompanyId = companyId;
                ViewBag.PartyId = partyId;
                ViewBag.RoleId = 7;
                ViewBag.CustomerId = customerId;

                var model = new List<sp_GetBundleCategoriesSorted_AdminR_Result>();
                model = companyService.getBundleCategoriesSortedByCustomerID(customerId);

                return View("~/Views/CustomerCatalog/BundleCategorySortView.cshtml", model);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult BundleCategorySortEdit(List<sp_GetBundleCategoriesSorted_AdminR_Result> bundleCategorySortList, string companyId, string partyId)
        {
            try
            {
                var model = new List<sp_GetBundleCategoriesSorted_AdminR_Result>();
                if (companyService.updateBundleCategoriesSortedByCustomerID(bundleCategorySortList))
                {
                    TempData["Message"] = "Records updated successfully";
                    return RedirectToAction("BundleCategorySort", new { @companyId = companyId, partyId = partyId, @customerId = bundleCategorySortList[0].CustomerID });
                }
                else
                {
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }



        #endregion Customer Catalog End

        public JsonResult GetCategoriesListForUserRestriction(int customerId, int userId)
        {
            try
            {
                var categories = companyService.GetCategoriesListForUserRestrictionByCustomerId(customerId, userId);


                return Json(categories.ToArray(), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddRestrictedCategoriesByUser(int customerId, int userId, string categoryIds)
        {
            try
            {
                var chkStatus = companyService.AddRestrictedCategoriesForUser(userId, categoryIds, adminSetting.UserID);

                return Json(chkStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddCustomerCountryDisclaimers(int customerId, string disclaimer, string countryIds)
        {
            try
            {
                var chkStatus = companyService.AddCustomerCountryDisclaimer(customerId, disclaimer, countryIds, adminSetting.UserID);

                if (chkStatus)
                {
                    var getUpdateDisclaimerList = companyService.GetCountryDisclaimerListByCustomerId(customerId).Where(x => !string.IsNullOrWhiteSpace(x.Disclaimer));
                    if (getUpdateDisclaimerList.Count() > 0)
                    {
                        return Json(getUpdateDisclaimerList, JsonRequestBehavior.AllowGet);
                    }
                    return Json("AllDeleted", JsonRequestBehavior.AllowGet);

                }
                return Json(chkStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RemoveCustomerCountryDisclaimer(int customerId, int countryId)
        {
            try
            {
                var chkStatus = companyService.RemoveCustomerCountryDisclaimer(customerId, countryId);

                return Json(chkStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }



    }



}
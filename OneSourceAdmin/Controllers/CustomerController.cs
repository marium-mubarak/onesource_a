﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : July 15 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Customer alert detail"
    /// </summary>
    public class CustomerController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of customer alert 
        /// </summary>
        /// <returns></returns>
   
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    CustomerModel model = new CustomerModel
                    {
                        Customers = cp.GetCustomers(),
                        CustomerAlerts = cp.GetCustomerAlert()
                    };
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("CustomerAlert");
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for adding and editing customer alert record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddCustomerAlert(tblCustomerAlert model)
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    if (!ValidateFields(model)) return RedirectToAction("Index");

                    CustomerPresenter cp = new CustomerPresenter();
                    int count;
                    //For Edit
                    if (model.CustomerAlertId!= 0)
                    {
                        count = cp.EditCustomerAlert(model, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Alert has been edited";
                        return RedirectToAction("Index");
                    }
                    //

                    count = cp.AddCustomerAlert(model, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Alert has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for deleting customer alert record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteAlert(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int customerId;
                    if (!int.TryParse(id, out customerId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    CustomerPresenter cp = new CustomerPresenter();
                    int count = cp.DeleteCustomerAlert(customerId);
                    if (count > 0) TempData["Message"] = "Alert has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// This method is responsible for checking customer alert data comming from request
        /// </summary>
        /// <param name="model"></param>
        public bool ValidateFields(tblCustomerAlert model)
        {
            if (model.CustomerId == 0)
            {
                TempData["Message"] = "Please Provide Customer";
                return false;
            }
            if (Convert.ToDateTime(model.StartDate).Date.Year == 1)
            {
                TempData["Message"] = "Please Provide Start Date";
                return false;
            }
            if (Convert.ToDateTime(model.EndDate).Date.Year > 1)
            {
                if (model.StartDate.Date > Convert.ToDateTime(model.EndDate).Date)
                {
                    TempData["Message"] = "Start Date must be less than End Date";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(model.Alert))
            {
                TempData["Message"] = "Please Provide Alert";
                return false;
            }

            return true;
        }
	}
}
﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using System.Configuration;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Sep 27 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Site Budget detail"
    /// </summary>
    public class SLASettingController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of SLA setting
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>

        public ActionResult Index(string customerId = null)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerPresenter cp = new CustomerPresenter();
                    LSASettingPresenter relp = new LSASettingPresenter();
                    SLASettingModel model = relp.GetLSASettingModel();
                    model.CustomerId = customerId ?? "";
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("SlaSettings");
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for Adding new SLA
        /// </summary>
        /// <param name="SLAName"></param>
        /// <returns></returns>
        public ActionResult AddSLA(string SLAName)
        {
            LSASettingPresenter relp = new LSASettingPresenter();
            int count = relp.AddSLA(SLAName, Convert.ToInt32(Session["UserID"]));
            TempData["Message"] = count > 0 ? "SLA has been added" : "SLA already exists";
            return RedirectToAction("Index");
        }
        /// <summary>
        /// This action is reponsible for returning the SLA information of customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetSLAinfo(int customerId)
        {
            LSASettingPresenter relp = new LSASettingPresenter();
            SLASettingModel data = relp.GetSLAinfo(customerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is responsible for Adding SLA to Customer mapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddSLAToCustomer(SLAModel model)
        {
            LSASettingPresenter relp = new LSASettingPresenter();
            int count = relp.AddSLACustomerWise(model, Convert.ToInt32(Session["UserID"]));
            if (count > 0) TempData["Message"] = "SLA has been set";
            count = relp.SetCustomerView(model.Viewable, model.CustomerId);
            if (count > 0) TempData["Message"] = "SLA has been set";
            return RedirectToAction("Index", new { customerId = model.CustomerId });
        }
        public ActionResult DeleteCustomerSLA(string hCustomerId, string SLAIds)
        {

            if (!string.IsNullOrEmpty(hCustomerId) && !string.IsNullOrEmpty(SLAIds))
            {
                LSASettingPresenter relp = new LSASettingPresenter();
                int count = relp.DeleteCustomerSLA(hCustomerId, SLAIds);
                if (count > 0) TempData["Message"] = "Customer SLA record has been deleted";
                return RedirectToAction("Index", new { customerId = hCustomerId });

            }
            else
            {
                TempData["Message"] = "Please provide correct info!";
                return RedirectToAction("Index");
            }
        }



        #region OrderQuoteSLA

        public ActionResult OrderQuoteSLA()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("LocaleOrderQuoteSla");
                    LSASettingPresenter relp = new LSASettingPresenter();
                    return View(relp.GetOrderQuoteSLAs());
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        public ActionResult UpdateOrderQuoteSLA(List<sp_getSLAs_AdminR_Result> sp_getSLAs_AdminR_Result)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    LSASettingPresenter lsa = new LSASettingPresenter();
                    int count = lsa.UpdateOrderQuoteSLA(sp_getSLAs_AdminR_Result, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Record has been updated successfully";
                    else TempData["Message"] = "Error occured";
                    return RedirectToAction("OrderQuoteSLA");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion

        #region Other OEM Specific SLA's
        //[Route("locale-settings/oem-specific-sla")]
        public ActionResult OEMSpecificSLAs()
        {
            try
            {
                LSASettingPresenter relp = new LSASettingPresenter();
                OEMSpecificSLAsModel model = relp.GetOEMSpecificSLAModel();
                @ViewBag.AddEditAccess = isAddEditAllow("OEMSpecificSLAs");
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        public JsonResult GetOEMSpecificSLAs(int countryId, int manufacturerId)
        {
            LSASettingPresenter obj = new LSASettingPresenter();
            var data = obj.GetSLAByOEM(countryId, manufacturerId);
            if (data == null)
                return Json("0", JsonRequestBehavior.AllowGet);
            else
                return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateOEMSpecificSLAs(OEMSpecificSLAsModel model)
        {
            if (model != null)
            {
                LSASettingPresenter obj = new LSASettingPresenter();
                int count = obj.UpdateOEMSpecificSLAs(model, adminSetting.UserID);
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region local taxes
        //[Route("locale-settings/local-taxes")]
        public ActionResult LocalTaxes()
        {
            try
            {
                CountryPresenter cp = new CountryPresenter();
                var countries = cp.GetCountries();
                countries.Insert(0, new GenericList { Key = 0, Value = "(None)" });
                ViewBag.Countries = countries;
                @ViewBag.AddEditAccess = isAddEditAllow("LocalTaxes");
                return View();
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public JsonResult GetCountryTax(int countryId)
        {
            LSASettingPresenter obj = new LSASettingPresenter();
            var data = obj.GetCountryTax(countryId);
            if (data == null)
                return Json("0", JsonRequestBehavior.AllowGet);
            else
                return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateCountryTax(int countryId, decimal? localtax)
        {
            if (countryId > 0)
            {
                LSASettingPresenter obj = new LSASettingPresenter();
                int count = obj.UpdateCountryTax(countryId, localtax, adminSetting.UserID);
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region sla history
        /// <summary>
        /// this action is responsible for providing sla history
        /// </summary>
        /// <param name="slaId"></param>
        /// <returns></returns>
        [Route("sla-history/{slaId}")]
        public ActionResult GetSLAHistory(int slaId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Request.UrlReferrer != null && !Convert.ToString(Request.UrlReferrer).Contains("sla-history"))
                    {                       
                       ViewBag.BackUrl= Request.UrlReferrer;                       
                    }
                    else 
                    {
                        ViewBag.BackUrl = "/admin-dashboard";
                    }
                    LSASettingPresenter relp = new LSASettingPresenter();
                    var model = relp.GetSLAHistory(slaId);
                    ViewBag.Message = TempData["Message"];
                    return View("~/Views/SLASetting/SLAHistory.cshtml", model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for updating timer to start state
        /// </summary>
        /// <param name="slaId"></param>
        /// <returns></returns>
        public ActionResult StartTimer(int slaId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    LSASettingPresenter relp = new LSASettingPresenter();
                    var isSubmitted = relp.StartTimer(slaId, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["ContactID"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]));
                    TempData["Message"] = isSubmitted == true ? "Your request has been successfully sent is being processed; your changes will take effect momentarily." : "No record was updated.";
                    return RedirectToAction("GetSLAHistory", new { @slaId = slaId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        /// <summary>
        /// this action is responsible for updating timer to stop state
        /// </summary>
        /// <param name="slaId"></param>
        /// <returns></returns>
        public ActionResult StopTimer(int slaId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    LSASettingPresenter relp = new LSASettingPresenter();
                    var isSubmitted = relp.StopTimer(slaId, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["ContactID"]), Convert.ToInt32(ConfigurationManager.AppSettings["ServerTimeZone"]));
                    TempData["Message"] = isSubmitted == true ? "Your request has been successfully sent is being processed; your changes will take effect momentarily." : "No record was updated.";
                    return RedirectToAction("GetSLAHistory", new { @slaId = slaId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for adding comment to SLA
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="slaId"></param>
        /// <returns></returns>
        public ActionResult AddSLAComment(string comment, int slaId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    LSASettingPresenter relp = new LSASettingPresenter();
                    var count = relp.AddSLAComment(comment, slaId, Convert.ToString(Session["UserName"]));
                    TempData["Message"] = count > 0 ? "Comment has been added successfully." : "No record was updated.";
                    return RedirectToAction("GetSLAHistory", new { @slaId = slaId });
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for modifying/updating SLA
        /// </summary>
        /// <param name="model"></param>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public ActionResult ModifySLA(OneSourceAdminCore.Model.SLAHistoryUpdateModel model,string resolution)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (model != null)
                    {
                        LSASettingPresenter relp = new LSASettingPresenter();
                        var count = relp.ModifySLA(model, resolution,Convert.ToString(Session["UserName"]),Convert.ToInt32(Session["UserID"]));
                        TempData["Message"] = count > 0 ? "Your request has been successfully sent is being processed; your changes will take effect momentarily." : "No record was updated.";
                        return RedirectToAction("GetSLAHistory", new { @slaId = model.slainstanceid });
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
    }

}
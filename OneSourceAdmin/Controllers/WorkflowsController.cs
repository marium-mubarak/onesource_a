﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class WorkflowsController : BaseController
    {

        private OneSourceAdminCore.Presenter.WorkflowService workflowService;

        public WorkflowsController()
        {
            workflowService = new WorkflowService();
        }


        //
        // GET: /Workflows/
        public ActionResult Index(string search)
        {
            try
            {
                var model = workflowService.GetWorkFlowList(search);
                return View("Listing", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public JsonResult GetWorkFlowDetails(int id)
        {
            try
            {
                var data = workflowService.GetWorkflowDetailsById(id);

                if (data != null)
                {
                    var sequenceSteps = workflowService.GetWorkSequenceById(id);
                    var newData = new
                    {

                        Name = (string.IsNullOrWhiteSpace(data.WorkflowName) ? "-" : data.WorkflowName),
                        Description = (string.IsNullOrWhiteSpace(data.WorkflowDesc) ? "-" : data.WorkflowDesc),
                        ErrorCancel = data.CancelOnError ? "Yes" : "No",
                        ExecuteSlaPer = data.OncePerSLA ? "Yes" : "No",
                        InactiveCompletion = data.MakeInactiveOnCompletion ? "Yes" : "No",
                        Steps = sequenceSteps

                        //ActionName = data.ActionName,
                        //IsError = data.IsError != null && data.IsError.Value ? "Yes" : "No",


                    };

                    return Json(newData, JsonRequestBehavior.AllowGet);
                }
                return Json("null", JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult CopyWorkFlow(int id)
        {
            try
            {

                var chkCopy = workflowService.CopyWorkFlowById(id);



                return Json(chkCopy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddEditWorkFlow(int id)
        {
            try
            {
                //    var model = workflowService.GetWorkFlowList(search);
                //return View("Listing", model);
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

    }
}
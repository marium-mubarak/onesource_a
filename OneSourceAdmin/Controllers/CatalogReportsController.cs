﻿using System;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using Newtonsoft.Json;
using System.Data;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Helper;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace OneSourceAdmin.Controllers
{
    public class CatalogReportsController : BaseController
    {

        private AdminMenuService adminMenuService;
        private CustomerPresenter customerService;
        private CatalogReportsPresenter catalogService;


        public CatalogReportsController()
        {
            adminMenuService = new AdminMenuService();
            customerService = new CustomerPresenter();
            catalogService = new CatalogReportsPresenter();
        }
        public CatalogReportModel GetCatalogModel(int routeId)
        {
            var model = new CatalogReportModel();
            try
            {
                
                var route = adminMenuService.GetMenuById(routeId);
                if (route != null)
                {
                    string name = route.MenuName.Trim().ToLower().Replace(" ", "");
                    var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));


                    model.routeId = route.MenuId;
                    model.routeName = name;
                    model.reportName = route.MenuName;
                    model.parentRouteId = parentRoute.MenuId;
                    model.parentName = parentRoute.MenuName;
                    model.parentRoute = parentRoute.MenuLink;

                    if (name == "bundlesandassociatedparts")//Adnan 18-june-2019
                    {
                        model.excelAction = "ExportToExcel";
                        model.ListCustomers = customerService.GetCustomersWithPriority();
                    }
                    else if (name == "eolproducts")//Adnan 18-june-2019
                    {
                        model.excelAction = "ExportExpiredProductOfCustomerToExcel";
                        model.ListCustomers = customerService.GetCustomersWithPriority();
                        model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    }
                    else if (name == "customerproductbyoem")//Adnan 19-june-2019
                    {
                        model.excelAction = "ExportExpiredProductOfCustomerToExcel";
                        model.ListCustomers = customerService.GetCustomersWithPriority();
                        model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                        model.ListManufacturer = catalogService.GetManufactures();
                        model.ListManufacturer.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                    }
                    else if (name == "catalogcategorybycustomer")//Adnan 27-june-2019
                    {

                        model.excelAction = "ExportExpiredProductOfCustomerToExcel";
                        model.ListCustomers = customerService.GetCustomersWithPriority();
                        model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                        model.ListPartCategories = catalogService.GetCategories();
                        model.ListPartCategories.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                    }
                    else if (name == "catalogbycountry")//Adnan 28-june-2019
                    {

                        model.excelAction = "ExportExpiredProductOfCustomerToExcel";
                        model.ListCustomers = customerService.GetCustomersWithPriority();
                        model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                        model.ListCountries = catalogService.GetCountries();
                        model.ListCountries.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                    }
                    else if (name == "msrp")
                    {
                        model.excelAction = "ExportExpiredProductOfCustomerToExcel";
                        model.ListCustomers = customerService.GetCustomersWithPriority();
                        model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    }
                    else if (name == "expiringproducts")
                    {
                        model.excelAction = "ExportExpiredProductOfCustomerToExcel";
                    }
                }
                
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
              
            }
            return model;
        }

        public ActionResult Index(int id)
        {
            try
            {
                var model = GetCatalogModel(id);

                 return View(model);
                
            
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }


        public ActionResult GetReportData(CatalogReportModel model)
        {
            try
            {
                if (model.routeName == "bundlesandassociatedparts")
                {
                    var data = string.Empty;
                    //var dt = catalogService.GetCatalogReportData(Session["routeId"].ConvertObjectToString(), Session["reportName"].ConvertObjectToString(), model);
                    var dt = catalogService.GetCatalogReportData(model.routeId.ToString(), model.reportName, model);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                       // Session["CatalogReportDataTable"] = dt;
                        var finalDt = dt.Copy();
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(4);
                        finalDt.Columns.RemoveAt(1);
                        var view = new DataView(finalDt);
                        var distinctValues = view.ToTable(true);
                        data = JsonConvert.SerializeObject(distinctValues);
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                    else
                    {
                        ViewBag.Message = "NoRecordsFound";
                      //  Session["CatalogReportDataTable"] = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }
                else if (model.routeName == "catalogcategoryofcustomer")
                {
                    var data = string.Empty;
                    var dt = GetCatalogCategoryOfCustomer(model.CustomerID, model.PartCategoryID, false);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var view = new DataView(dt);
                        var distinctValues = view.ToTable(true);
                        data = JsonConvert.SerializeObject(distinctValues);
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                    else
                    {
                        ViewBag.Message = "NoRecordsFound";
                        // Session["CatalogReportDataTable"] = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }
                else if (model.routeName == "catalogbycountry")
                {
                    var data = string.Empty;
                    var dt = GetCatalogbyCountry(model.CustomerID, model.CountryID, false);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var view = new DataView(dt);
                        var distinctValues = view.ToTable(true);
                        data = JsonConvert.SerializeObject(distinctValues);
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                    else
                    {
                        ViewBag.Message = "NoRecordsFound";
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }
                else
                {
                    ViewBag.Message = "NoRecordsFound";
                    return Json(new { data = "", msg = ViewBag.Message });
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ConvertObjectToString(), routeData);
                }

                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult RedirectToParent(string parentRoute,string parentRouteId)
        {
            Response.Redirect(string.Format("~/{0}/{1}", parentRoute, parentRouteId));
            return null;
        }


        public JsonResult GetBundlesByCustomerID(string customerIds)
        {
            try
            {
                var bundles = catalogService.GetBundlesByCustomer(customerIds);
                if (bundles != null)
                {
                    var categoriesList = bundles.Select(x => new
                    {
                        Value = x.Key.ToString(),
                        Text = x.Value,
                        isSelected = false
                    }).ToList();
                    return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetBundleItems(CatalogReportModel model,int bundleId)
        {
            try
            {
                var dt = catalogService.GetCatalogReportData(model.routeId.ToString(), model.reportName, model);
                // var dt = (DataTable)Session["CatalogReportDataTable"];
                var list = dt.ToList<sp_BundlesAssociatedParts_AdminR_Result>();
                var filteredData = list.Where(x => x.CatalogStandardID == bundleId).ToList();
                var finalDt = filteredData.ToDataTable();
                return new JsonResult() { Data = JsonConvert.SerializeObject(finalDt), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return new JsonResult() { Data = "", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }


        public ActionResult ExportToExcel(CatalogReportModel model)
        {
            try
            {
                // var model = new CatalogReportModel();
                string filename = model.reportName; //Session["reportName"].ToString().Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");

                if (model != null)
                {
                    var dt = catalogService.GetCatalogReportData(model.routeId.ToString(), model.reportName, model);//(DataTable)Session["CatalogReportDataTable"];

                    var list = dt.ToList<CatalogReportMainModel>();

                    var finalDt = new DataTable();
                    //Iterate the dt here
                    finalDt.Columns.Add("SKU");
                    finalDt.Columns.Add("Description");
                    finalDt.Columns.Add("Customer");
                    finalDt.Columns.Add("Price");
                    finalDt.Columns.Add(" ");
                    finalDt.Columns.Add("Countries");

                    var distinctBundles = list.Select(x => x.CatalogStandardID).Distinct();

                    foreach (var bundle in distinctBundles)
                    {
                        var bundleItems = list.Where(x => x.CatalogStandardID == bundle).ToList();
                        var dr = finalDt.NewRow();
                        dr["SKU"] = " ";
                        dr["Description"] = bundleItems[0].CatalogDescription;
                        dr["Customer"] = bundleItems[0].CustomerName;
                        dr["Countries"] = bundleItems[0].Countries;
                        finalDt.Rows.Add(dr);

                        foreach (var item in bundleItems)
                        {
                            var itemRow = finalDt.NewRow();
                            itemRow["SKU"] = item.ManufacturerSKU;
                            itemRow["Description"] = item.PartDescription;
                            itemRow["Customer"] = "Quantity: " + item.Quantity;
                            itemRow["Price"] = string.Format("{0} ({1})", item.GlobalPrice, item.Currency);
                            itemRow[" "] = item.IsOption == true ? "Optional" : "Mandatory";
                            finalDt.Rows.Add(itemRow);
                        }

                        var rowSpace = finalDt.NewRow();
                        rowSpace[" "] = " ";
                        finalDt.Rows.Add(rowSpace);
                    }

                    ReportHelper.ExportToExcel(finalDt, filename, Response);
                }
                else
                {
                    ViewBag.Message = "NoData";
                }

                model.ListCustomers = customerService.GetCustomersWithPriority();
                return View("Index", model);
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        #region Expired products of customer by Adnan on 18-june-2019
        public ActionResult ExportExpiredProductOfCustomerToExcel(FormCollection col)
        {
            try
            {
                var customerId = Convert.ToInt32(col["customerId"]);
                var catalogTypeId = Convert.ToInt32(col["catalogTypeId"]);
                var manaufacturerId = Convert.ToInt32(col["manufacturerId"]);
                var partcategoryId = Convert.ToInt32(col["partcategoryId"]);
                var countryId = Convert.ToInt32(col["countryId"]);
                var reportName = Convert.ToString(col["reportName"]);
                var routeId = Convert.ToInt32(col["routeId"]);
                Response.Buffer = true;
                var model = GetCatalogModel(routeId);
                DataTable dt = null;
                if (reportName == "EOL Products" && customerId > 0 && catalogTypeId > 0)
                {
                    dt = GetProductOfCustomerReport(customerId, catalogTypeId, 0);
                    //model.ListCustomers = customerService.GetCustomersWithPriority();
                    //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportName == "Expiring Products")
                {
                    dt = GetExpiringProductsReport();
                }
                else if (reportName == "MSRP" && customerId > 0)
                {
                    dt = GetMSRPReport(customerId);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportName == "Customer Product by OEM" && customerId > 0 && manaufacturerId > 0)
                {
                    dt = GetProductOfCustomerReport(customerId, 0, manaufacturerId);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                  //  model.ListManufacturer = catalogService.GetManufactures();
                  //  model.ListManufacturer.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                }
                else if (reportName == "Catalog Category by Customer" && customerId > 0 && partcategoryId > 0)
                {
                    dt = GetCatalogCategoryOfCustomer(customerId, partcategoryId, true);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                  //  model.ListPartCategories = catalogService.GetCategories();
                  //  model.ListPartCategories.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                }
                else if (reportName == "Catalog by Country" && customerId > 0 && countryId > 0)
                {
                    dt = GetCatalogbyCountry(customerId, countryId, true);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                  //  model.ListCountries = catalogService.GetCountries();
                  //  model.ListCountries.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, reportName, Response);
                }
                else if (dt != null && dt.Rows.Count == 0)
                {
                    ViewBag.Message = "NoData";
                }
                else
                {
                    ViewBag.Message = "Error";
                }


                return View("Index", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        public DataTable GetProductOfCustomerReport(int customerId, int catalogTypeId, int manufacturerId)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ExpiredProductsRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@customerId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@priceCodeId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@manufacturerId", SqlDbType.Int));

                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = catalogTypeId;
                    command.Parameters[2].Value = manufacturerId;
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetProductOfCustomerReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        #endregion

        #region MSRP Report
        public DataTable GetMSRPReport(int customerId)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_MSRPRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));



                    command.Parameters[0].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetMSRPReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

        #region Catalog Category of Customer  by Adnan on 27-june-2019
        public DataTable GetCatalogCategoryOfCustomer(int customerId, int partcategoryId, bool Isexport)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_GetCatalogCategoryByCustomer_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@PartCategoryID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@IsExport", SqlDbType.Bit));


                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = partcategoryId;
                    command.Parameters[2].Value = Isexport;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetCatalogCategoryOfCustomer \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion

        #region Catalog by country and customer by Adnan on 28-june-2019
        public DataTable GetCatalogbyCountry(int customerId, int countryId, bool Isexport)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_GetCatalogByCustomerAndCountry_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CountryID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@IsExport", SqlDbType.Bit));


                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = countryId;
                    command.Parameters[2].Value = Isexport;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetCatalogCategoryOfCustomer \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion


        #region Expiring Products
        public DataTable GetExpiringProductsReport()
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ExpiringProductsRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    //command.Parameters.Add(new SqlParameter("@startdate", SqlDbType.NVarChar));
                    //command.Parameters.Add(new SqlParameter("@enddate", SqlDbType.NVarChar));

                    //command.Parameters[0].Value = startdate;
                    //command.Parameters[1].Value = enddate;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetExpiringReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion
    }
}
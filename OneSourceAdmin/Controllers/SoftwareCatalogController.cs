﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using System.IO;
using System.Data;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class SoftwareCatalogController : BaseController
    {
        SoftwareCatalogPresenter _service;

        public SoftwareCatalogController()
        {
            _service = new SoftwareCatalogPresenter();
        }

        public ActionResult UploadCatalog()
        {
            TempData["Message"] = TempData["Message"] != null ? TempData["Message"].ToString() : "";
            return View();
        }


        [HttpPost]
        public ActionResult UploadSoftwareCatalog()
        {
            try
            {
                var fileType = Request.Form["fileType"];
                var file = Request.Files[0];

                //Get the file type to check it whether its excel file or not
                var fileExtenstion = Path.GetExtension(file.FileName);

                if (fileExtenstion == ".xlsx" || fileExtenstion == ".xls")
                {
                    TempData["Message"] = "";
                    bool isFileValid = true;
                    bool allOk = true;
                    string sheetName = "";
                    int rowsInserted = 0;

                    //Save the file on server
                    var folderPath = Server.MapPath("~/uploads/software_catalog");
                    var fileNamePrefix = Path.GetFileName(file.FileName).Replace(" ", "");
                    var fileName = fileNamePrefix + DateTime.Now.ToString("-ddMMMyyyy-HHmmssfff") + ".xlsx";
                    var path = Path.Combine(folderPath, fileName);

                    //Check if the path exists or not
                    if (!Directory.Exists(folderPath))
                        System.IO.Directory.CreateDirectory(folderPath);

                    file.SaveAs(path);

                    DataSet ds = new DataSet();

                    //Using ClosedXML for data reading the excel file
                    XLWorkbook workBook = new XLWorkbook(path);

                    //Checking the format of all the sheets for the file

                    foreach (IXLWorksheet workSheet in workBook.Worksheets)
                    {
                        var row1 = workSheet.Rows(1,1).ToList();
                        if (!ValidateFileType(row1[0], fileType))
                        {
                            TempData["Message"] = "Please provide the " + fileType + " file in correct format.";
                            isFileValid = false;
                            break;
                        }
                    }

                    if (isFileValid)
                    {
                        //Iterate through each sheet
                        foreach (IXLWorksheet workSheet in workBook.Worksheets)
                        {
                            sheetName = workSheet.Name.ToString();

                            //Create a new DataTable.
                            DataTable dt = new DataTable();

                            //Loop through the Worksheet rows.
                            bool firstRow = true;
                            var lastRow = workSheet.LastRowUsed().RowNumber();
                            var allRows = workSheet.Rows(1,lastRow);
                            foreach (IXLRow row in allRows)
                            {
                                if (!row.IsEmpty())
                                {
                                    //Use the first row to add columns to DataTable.
                                    if (firstRow)
                                    {
                                        foreach (IXLCell cell in row.Cells())
                                        {
                                            dt.Columns.Add(cell.Value.ToString());
                                        }

                                        firstRow = false;
                                    }
                                    else
                                    {
                                        //Add rows to DataTable.
                                        dt.Rows.Add();
                                        int i = 0;
                                        foreach (IXLCell cell in row.Cells())
                                        {

                                            dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                            i++;
                                        }
                                    }
                                }
                            }

                            //Add the additional columns in MPSA if any
                            if (fileType == "MPSA")
                            {
                                dt.Columns.Add("Program", typeof(String));

                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    dt.Rows[i]["Program"] = "MPSA";

                                }
                            }

                            //Increment the total rows inserted
                            rowsInserted += dt.Rows.Count;

                            //insert the data  for this sheet in db
                            if (!_service.BulkCopyDT(dt, fileType))
                            {
                                allOk = false;
                                break;
                            }
                        }

                        if (allOk)
                        {
                            TempData["Message"] = rowsInserted + " rows inserted successfully";
                        }
                        else
                        {
                            TempData["Message"] = "Sheet: " + sheetName + " has some issues in uploading. Please check the file.";
                        }
                    }
                }
                else
                {
                    TempData["Message"] = "Please provide a file of type .xlsx or .xls";
                }

                return RedirectToAction("UploadCatalog");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                if (ex.Message.Contains("corrupted data"))
                {
                    TempData["Message"] = ex.Message;
                    return RedirectToAction("UploadCatalog");
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        internal bool ValidateFileType(IXLRow row, string fileType)
        {
            try
            {
                var columns = row.Cells().Select(x => x.Value.ToString().ToUpper()).ToList();
                if (fileType == "MPSA")
                {
                    if (columns.Contains("ITEM NUMBER") && columns.Contains("ORDERING PART NAME") && columns.Contains("PART SHORT NAME") && columns.Contains("ITEM GROUP"))
                    {
                        return true;
                    }
                }
                if (fileType == "SelectPlus")
                {
                    if (columns.Contains("PRICE LIST") && columns.Contains("ACD INDICATOR") && columns.Contains("PART NUMBER") && columns.Contains("ITEM NAME"))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return false;
            }

        }

        public ActionResult CustomersSoftwareSettings(int? customerId, int? settingId)
        {
            try
            {
                var model = new SoftwareSettingsModel();
                if (settingId > 0)
                {
                    model = _service.GetCustomerSettingsById(Convert.ToInt32(settingId));
                    model.PoolSettingList = _service.GetPoolSettingByCustomerSettingId(Convert.ToInt32(settingId));
                }

                if (TempData["Msg"] != null)
                {
                    ViewBag.Message = TempData["Msg"];
                }
                model.CustomersList = _service.GetActiveCustomersList();
                model.ContractTypeList = _service.GetContractListTypeList();
                model.AgreementTypeList = _service.GetAgreementTypeList();
                model.PoolList = _service.GetPoolList();
                model.ProgramsList = _service.GetProgramsList();
                model.CustomerId = Convert.ToInt32(customerId);
                model.SettingId = Convert.ToInt32(settingId); 
                model.CustomerSettings = _service.GetCustomerSettingListById(customerId > 0 ? Convert.ToInt32(customerId) : model.CustomersList.FirstOrDefault().Key);

                return View("CustomerSoftwareSettings", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        [HttpPost]
        public ActionResult AddUpdateSoftwareCustomerSettings(SoftwareSettingsModel data)
        {
            try
            {

                if (data != null && data.CustomerId > 0)
                {
                    var status = _service.AddUpdateConnectionSettings(data, adminSetting);
                    if (status)
                    {
                        TempData["Msg"] = "Request procesessed successfully";
                    }
                    else
                    {
                        TempData["Msg"] = "Error while processing your request";
                    }
                    return RedirectToAction("CustomersSoftwareSettings", new { customerId = data.CustomerId });
                }

                TempData["Msg"] = "Please fill all mandatory fields";
                return RedirectToAction("CustomersSoftwareSettings");

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }


    }
}
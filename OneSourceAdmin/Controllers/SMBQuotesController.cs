﻿using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class SMBQuotesController : BaseController
    {
        SMBQuotesPresenter sq;

        // GET: SMBQuotes
        public ActionResult SMBQuotes()
        {
            sq = new SMBQuotesPresenter();

            var model = new SMBQuotesModel();
           // model.ListSMBUsers.AddRange(sq.GetSMBUsers());
            return View(model);
        }
        public ActionResult GetQuoteIds(int accountManagerId,string quoteType)
        {
            sq = new SMBQuotesPresenter();

            var quoteIds = sq.GetQuoteIdByAccountManagerandQuoteType(accountManagerId, quoteType);

            return Json(new { QuoteIds = quoteIds },JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSMBUsers(string quoteType)
        {
            sq = new SMBQuotesPresenter();

            var users = sq.GetSMBUsers(quoteType).ToList();
            
            return Json(new { users = users }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReponseQuoteLineItems(string quoteType, int quoteId)
        {
            sq = new SMBQuotesPresenter();
            var quoteTypeId = 0;
            if(quoteType=="Hardware Quotes")
            {
                quoteTypeId = 1;
            }
            else if(quoteType=="Software Quotes")
            {
                quoteTypeId = 2;
            }
            else if(quoteType=="Asset Disposal Quotes")
            {
                quoteTypeId = 3;
            }
            else if(quoteType=="IT Service Quotes")
            {
                quoteTypeId = 4;
            }

            var responseLineItems = sq.GetQuoteResponseLineItems(quoteId, quoteTypeId);

            return Json(new { LineItems = responseLineItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteResponseLineItems(int[] ids)
        {
            try
            {
                sq = new SMBQuotesPresenter();
                var count = 0;
                if (ids.Count() > 0)
                {
                    count = sq.DeleteResponseLineItems(ids);
                }
                return Json(new { data = count },JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
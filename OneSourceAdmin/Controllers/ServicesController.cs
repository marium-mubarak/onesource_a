﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class ServicesController : BaseController
    {


        private ServiceQuotesService _service;

        public ServicesController()
        {
            _service = new ServiceQuotesService();
        }

        
        // GET: Services
        public ActionResult Index()
        {
            var model = _service.GetServicesList();

            return View(model);
        }
    }
}
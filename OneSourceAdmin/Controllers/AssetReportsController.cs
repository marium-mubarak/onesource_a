﻿using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OneSourceAdmin.Controllers
{
    public class AssetReportsController : BaseController
    {
        #region Fields

        CustomerPresenter _customerService;
        UserPresenter _userService;
        AssetReportsPresenter _reportService;
        BudgetPresenter _budgetService;
        AdminMenuService _adminMenuService;

        #endregion

        #region Constructor
        public AssetReportsController()
        {
            _userService = new UserPresenter();
            _reportService = new AssetReportsPresenter();
            _customerService = new CustomerPresenter();
            _budgetService = new BudgetPresenter();
            _adminMenuService = new AdminMenuService();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Report Landing Page by Specific Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Report(int id)
        {
            try
            {
                var route = _adminMenuService.GetMenuById(id);
                if (route != null)
                {
                    string name = route.MenuName.Trim().ToLower().Replace(" ", "");
                    var parentId = 0;
                    Int32.TryParse(route.ParentMenuId.ToString(), out parentId);
                    LogException("MenuId : " + id + ",ParentMenuId : " + parentId, this.ControllerContext.RouteData);
                    var parentRoute = _adminMenuService.GetMenuById(parentId);



                    var model = new AssetReportModel();

                    model.routeId = route.MenuId;
                    model.routeName = name;
                    model.ReportName = route.MenuName;
                    model.parentRouteId = parentRoute.MenuId;
                    model.parentName = parentRoute.MenuName;
                    model.parentRoute = parentRoute.MenuLink;

                    model.ListCustomers = _customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                    {
                        Text = s.CustomerName,
                        Value = s.CustomerId.ToString()
                    }).ToList();

                    if (name == "assetsbymanufacturer")
                    {
                        model.ListManufacturers = _reportService.GetManufactures().Select(m => new SelectListItem
                        {
                            Text = m.ManufacturerName,
                            Value = m.ManufacturerId.ToString()
                        }).ToList(); ;
                    }

                    model.TempAssetLocationModel = model;
                    model.TempAssetLocationTable = null;
                    return View("Report", model);

                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        /// <summary>
        /// Get Report Data to Grid
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetReportData(AssetReportModel model)
        {
            try
            {
                DateTime? start = null;
                DateTime? end = null;

                if (!string.IsNullOrEmpty(model.StartDate))
                {
                    start = Convert.ToDateTime(model.StartDate);
                }
                if (!string.IsNullOrEmpty(model.EndDate))
                {
                    end = Convert.ToDateTime(model.EndDate);
                }
                if (start != null && end != null)
                {
                    if (start.Value.Date > end.Value.Date)
                        ViewBag.Message = "DateError";
                }

                var buttonName = Request.Form["submit"];
                if (buttonName == "Reset")
                {
                    model.TempAssetLocationTable = null;
                    model.TempAssetLocationModel = null;

                    Session["ErrorMessage"] = null;
                    return RedirectToAction("Report", new { id = Convert.ToInt32(model.routeId) });
                }
                var companyId = 0;
                if (model.CustomerId > 0)
                {
                    companyId = _reportService.GetCompanyIdByCustomerId(model.CustomerId);
                }
                model.CompanyId = companyId;

                var dt = _reportService.GetAssetReportData(model);
                object obj = null;
                if (dt.Rows.Count > 0)
                {
                    model.TempAssetLocationTable = dt;

                    if (model.ReportName == "Asset Depreciation Summary")
                    {
                        var list = dt.Select().ToList();

                        var totalNumberOfAffectedAssets = list.Count;
                        var lifemonths = 0;
                        List<string> countries = new List<string>();
                        List<string> manufacturers = new List<string>();
                        var totalExpiredAssetsValue = 0.0;
                        var totalAssetValuationForStartDate = 0.0;
                        var totalAssetValuationForEndDate = 0.0;
                        var totalDepreciation = 0.0;
                        foreach (var item in list)
                        {
                            var row = item.ItemArray;
                            lifemonths = lifemonths + Convert.ToInt32(row[6]);
                            countries.Add(row[5].ToString());
                            manufacturers.Add(row[1].ToString());
                        }
                        totalExpiredAssetsValue = Math.Round(list.Sum(x => Convert.ToDouble(x.ItemArray[19])), 2);
                        totalAssetValuationForStartDate = Math.Round(list.Sum(x => Convert.ToDouble(x.ItemArray[9])), 2);
                        totalAssetValuationForEndDate = Math.Round(list.Sum(x => Convert.ToDouble(x.ItemArray[11])), 2);
                        totalDepreciation = Math.Round(list.Sum(x => Convert.ToDouble(x.ItemArray[13])), 2);
                        countries = countries.Distinct().ToList();
                        manufacturers = manufacturers.Distinct().ToList();

                        var totalNumberOfAffectedCountries = countries.Count;
                        var totalNumberOfAffectedManufacturers = manufacturers.Count;
                        var totalExpiredAssets = list.Where(x => Convert.ToBoolean(x.ItemArray[18].ToString()) == true).Count();
                        var averageLife = lifemonths / list.Count;

                        obj = new
                        {
                            TotalNumberOfAffectedAssets = totalNumberOfAffectedAssets,
                            AverageLife = averageLife,
                            TotalNumberOfAffectedCountries = totalNumberOfAffectedCountries,
                            TotalNumberOfAffectedManufacturers = totalNumberOfAffectedManufacturers,
                            TotalExpiredAssets = totalExpiredAssets,
                            ValueForExpiredAssets = totalExpiredAssetsValue,
                            AssetValuationTotalForStartDate = totalAssetValuationForStartDate,
                            AssetValuationTotalorEndDate = totalAssetValuationForEndDate,
                            TotalAssetValuationChange = totalDepreciation,
                            StartDate = model.StartDate,
                            EndDate = model.EndDate
                        };
                    }
                    Session["ErrorMessage"] = null;
                }
                else
                {
                    if (ViewBag.Message != "DateError")
                    {
                        ViewBag.Message = "No records found.";
                    }
                    model.TempAssetLocationTable = null;
                    
                    return Json(new { data = "", msg = ViewBag.Message });
                }
                model.TempAssetLocationModel = model;
                object data = null;
                if (obj != null)
                {
                    data = JsonConvert.SerializeObject(obj);
                }
                else
                {
                    data = JsonConvert.SerializeObject(dt);
                }
                return Json(new { data = data, msg = ViewBag.Message,model = model.CompanyId});
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Exports Data to Excel
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult ExportToExcel(AssetReportModel model)
        {
            try
            {
                var companyId = 0;
                if (model.CustomerId > 0)
                {
                    companyId = _reportService.GetCompanyIdByCustomerId(model.CustomerId);
                }
                model.CompanyId = companyId;
                var dt = _reportService.GetAssetReportData(model);

                if (dt != null)
                    ReportHelper.ExportToExcel(dt, model.ReportName, Response);
                else
                {
                    Session["ErrorMessage"] = "NoData";
                    return RedirectToAction("Report", new { id = Convert.ToInt32(model.routeId) });
                }
                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ExportAssetByCustomerToExcel(FormCollection col)
        {
            try
            {
                var model = new AssetReportModel();
                var customerId = Convert.ToInt32(col["customerId"]);
                var startDate = Convert.ToString(col["startDate"]);
                var endDate = Convert.ToString(col["endDate"]);
                model.routeName = col["routeName"];
                model.ReportName = col["reportName"].ToString();
                model.routeId = Convert.ToInt16(col["routeId"]);

                var dt = GetAssetByCustomerIntoDatatable(customerId, startDate , endDate);
                model.ListCustomers = _customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                {
                    Text = s.CustomerName,
                    Value = s.CustomerId.ToString()
                }).ToList();
                if (dt != null && dt.Rows.Count > 0)
                    ReportHelper.ExportToExcel(dt, model.ReportName, Response);
                else
                {
                    Session["ErrorMessage"] = "NoData";
                    return RedirectToAction("Report", new { id = Convert.ToInt32(model.routeId) });
                }
                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException(ex.InnerException.ToString(), routeData);
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        /// <summary>
        /// Exports Data to Pdf
        /// </summary>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult ExportToPdf(FormCollection collection,AssetReportModel model)
        {
            try
            {
                
                var dt = _reportService.GetAssetReportData(model);
                if (dt != null)
                {
                    PdfPTable pdfTable = FillDataTableToPdf(dt);
                    if (model.ReportName == "Asset Depreciation Summary")
                    {
                        model.routeName = collection["routeName"];
                        string totalaffectedassets = collection["totalaffectedassets"].ToString();
                        string avglife = collection["avglife"].ToString();
                        string totalaffectedcountries = collection["totalaffectedcountries"].ToString();
                        string totalmfgs = collection["totalaffectedmfgs"].ToString();
                        string totalexpiredassets = collection["totalexpiredassets"].ToString();
                        string valueofexpiredassets = collection["valueofexpiredassets"].ToString();
                        string assetvaluationtotalstrdate = collection["assetvaluationtotalstrdate"].ToString();
                        string assetvaluationtotalenddate = collection["assetvaluationtotalenddate"].ToString();
                        string totalassetvaluationchange = collection["totalassetvaluationchange"].ToString();
                        string startdt = collection["startdt"].ToString();
                        string enddt = collection["enddt"].ToString();
                        ExportHtmlToPdf(totalaffectedassets, avglife, totalaffectedcountries, totalmfgs, totalexpiredassets, valueofexpiredassets
                            , assetvaluationtotalstrdate, assetvaluationtotalenddate, totalassetvaluationchange, startdt, enddt);
                    }
                    else
                        ReportHelper.ExportToPdf(dt, pdfTable, model.ReportName, Response, Server);
                }
                else
                {
                    Session["ErrorMessage"] = "NoData";
                    return RedirectToAction("Report", new { id = Convert.ToInt32(model.routeId) });
                }
                return View("Report", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException(ex.InnerException.ToString(), routeData);
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        /// <summary>
        /// Get Addresses by Customer Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        public JsonResult GetAddressesByCustomerId(int customerId, int selectedId)
        {
            try
            {
                var addressList = _reportService.GetLocationsByCustomerId(customerId);
                var addresses = addressList.Select(x => new
                {
                    Value = x.CustomerAddressID.ToString(),
                    Text = x.CondensedAddress,
                    SelectedId = !addressList.Select(s => s.CustomerAddressID).Contains(selectedId) ? 0 : selectedId
                }).ToList();
                return Json(addresses.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException(ex.InnerException.ToString(), routeData);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Country List by Customer Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="selectedId"></param>
        /// <returns></returns>
        public JsonResult GetCountryByCustomerId(int customerId, int selectedId)
        {
            try
            {
                var countriesList = _budgetService.GetCountryByCustomer(customerId);
                var countries = countriesList.Select(x => new
                {
                    Value = x.CountryID.ToString(),
                    Text = x.CountryName,
                    SelectedId = !countriesList.Select(s => s.CountryID).Contains(selectedId) ? 0 : selectedId
                }).ToList();

                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                    LogException(ex.InnerException.ToString(), routeData);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Redirects to parent menu
        /// </summary>
        /// <returns></returns>
        public ActionResult RedirectToParent(AssetReportModel model)
        {
            Response.Redirect("~/" + model.parentRoute + "/" + model.parentRouteId);
            return null;
        }

        /// <summary>
        /// Get Report Chart Data from the generated data of grid
        /// </summary>
        /// <returns>Chart specific modified data</returns>
        public JsonResult GetReportChartData(AssetReportModel model, bool? assetType = null)
        {
            try
            {
                var dt = _reportService.GetAssetReportData(model);
                if (dt != null)
                {
                    var list = dt.AsEnumerable().Distinct().ToArray();
                    var reportData = _reportService.GetReportDataToExport(model, list, assetType);
                    
                    return Json(new { data = reportData, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { data = " ", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        #endregion

        #region Export Asset by Customer Report To Datatable By Adnan Nooruddin on 12-july-2019
        public DataTable GetAssetByCustomerIntoDatatable(int customerId, string startDate, string endDate)// Added by Adnan on 12-7-2019 for asset by customer Report
        {
            try
            {

                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_CustomerAssetRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetAssetByCustomerIntoDatatable \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Utilities

        public PdfPTable FillDataTableToPdf(DataTable data)
        {
            try
            {

                var dt = data;
                var columnsCount = dt.Columns.Count;
                BaseColor color = WebColors.GetRGBColor("#ffffff");
                iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD, color);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
                BaseColor backgrounColor = WebColors.GetRGBColor("#3b9ad2");

                PdfPTable table = new PdfPTable(data.Columns.Count);
                PdfPRow row = null;
                float[] widths = new float[data.Columns.Count];

                for (int i = 0; i < data.Columns.Count; i++)
                {
                    widths[i] = 15f;
                }

                table.SetWidths(widths);

                table.WidthPercentage = 100;
                int iCol = 0;
                string colname = "";

                int[] rowItems = new int[columnsCount];

                for (int i = 0; i < columnsCount; i++)
                {
                    rowItems[i] = i;
                }

                foreach (DataColumn c in dt.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(c.ColumnName , fontLabel));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.BackgroundColor = backgrounColor;
                    table.AddCell(cell);
                }

                foreach (DataRow r in dt.Rows)
                {
                    for (int i = 0; i < rowItems.Count(); i++)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(r[i].ToString(),fontNormal));
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(cell);
                        }
                    }
                }

                return table;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        protected void ExportHtmlToPdf(string totalaffectedassets, string avglife, string totalaffectedcountries,
            string totalaffectedmfgs, string totalexpiredassets, string valueofexpiredassets,
            string assetvaluationtotalstrdate, string assetvaluationtotalenddate, string totalassetvaluationchange,
            string startdt, string enddt)
        {
            try
            {
                //Create PDF Table
                PdfPTable tableLayout = new PdfPTable(4);

                var content = Add_Content_To_PDF(tableLayout, totalaffectedassets, avglife, totalaffectedcountries, totalaffectedmfgs, totalexpiredassets, valueofexpiredassets,
                    assetvaluationtotalstrdate, assetvaluationtotalenddate, totalassetvaluationchange, startdt, enddt);

                iTextSharp.text.Image logoImg =
                    iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                logoImg.ScaleAbsolute(250, 45);


                using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
                {

                    string filename = string.Empty;

                    Document document = new Document(PageSize.A4, 10, 10, 20, 10);
                    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                    document.Open();
                    document.Add(logoImg);
                    document.Add(new Chunk("\n"));
                    document.Add(content);
                    document.Add(new Chunk("\n"));


                    document.Close();
                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();

                    Response.Clear();

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment; filename=AssetDepreciationSummary.pdf");
                    Response.ContentType = "application/pdf";
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.BinaryWrite(bytes);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }

        }



        private PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, string totalaffectedassets, string avglife, string totalaffectedcountries,
            string totalaffectedmfgs, string totalexpiredassets, string valueofexpiredassets,
            string assetvaluationtotalstrdate, string assetvaluationtotalenddate, string totalassetvaluationchange,
            string startdt, string enddt)
        {
            float[] headers = { 40, 60, 40, 60 };  //Header Widths
            tableLayout.SetWidths(headers);        //Set the pdf headers
            tableLayout.WidthPercentage = 98;       //Set the PDF File witdh percentage
            int cols = 0;
            //Add Title to the PDF file at the top

            tableLayout.AddCell(new PdfPCell(new Phrase("Asset Depreciation Summary:", new Font(Font.FontFamily.HELVETICA, 11, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(startdt + " through " + enddt, new Font(Font.FontFamily.HELVETICA, 10, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 1, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("Total # of Affected Assets :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(totalaffectedassets, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("Average Life (months) :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(avglife, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("Total # of Affected Countries :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(totalaffectedcountries, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("Total # of Manufacturers :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(totalaffectedmfgs, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("Total Expired (Fully Depreciated) Assets for Period :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 0, PaddingBottom = 5, PaddingLeft = 105, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(totalexpiredassets, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("Value of Expired Assets for Period :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 0, PaddingBottom = 5, PaddingLeft = 105, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(valueofexpiredassets, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("Asset Valuation Total as of ( 03/01/2018 ) :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 0, PaddingBottom = 5, PaddingLeft = 105, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(assetvaluationtotalstrdate, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            tableLayout.AddCell(new PdfPCell(new Phrase("Asset Valuation Total as of ( 03/01/2018 ) :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 0, PaddingBottom = 5, PaddingLeft = 105, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(assetvaluationtotalenddate, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            tableLayout.AddCell(new PdfPCell(new Phrase("Total Asset Valuation Change for Period :", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 2, Border = 0, PaddingBottom = 5, PaddingLeft = 105, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase(totalassetvaluationchange, new Font(Font.FontFamily.HELVETICA, 7, 0, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });
            tableLayout.AddCell(new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, 1, new iTextSharp.text.BaseColor(System.Drawing.Color.Black)))) { Colspan = 0, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            return tableLayout;
        }


        #endregion
    }


}
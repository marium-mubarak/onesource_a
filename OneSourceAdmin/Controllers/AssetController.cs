﻿using OfficeOpenXml;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class AssetController : BaseController
    {
        // GET: Asset
        public ActionResult AssetSerialNumberUploader()
        {
            try
            {
                ModelToUpdate mu = new ModelToUpdate();

                if (TempData["Message"] == "" || TempData["Message"] == null)
                {
                    TempData["Message"] = "";
                }
                else
                {
                    mu = (ModelToUpdate)TempData["Message"];
                }
                return View(mu);
            }
            catch(Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                ViewBag.error = "Issue while connecting please try later.";
                return View();
            }        
        }

        [HttpPost]
        public ActionResult GetExcelFile()
        {
            try
            {
                ModelToUpdate mu = new ModelToUpdate();
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];
                    string fileName = file.FileName;
                    if(fileName == "")
                    {
                        mu.FileError = "Excel file is required.";
                        TempData["Message"] = mu;
                    }
                    else
                    {
                        if (fileName.Contains(".xlsx") || fileName.Contains(".xls"))
                        {
                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                IDictionary<int, string> dict = new Dictionary<int, string>();
                                List<string> ColumnList = new List<string>();
                                //get the first worksheet in the workbook
                                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                                int colCount = worksheet.Dimension.End.Column;  //get Column Count
                                int rowCount = worksheet.Dimension.End.Row;     //get row count


                                for (int col = 1; col <= colCount; col++)
                                {
                                    string HeaderName = worksheet.Cells[1, col].Value?.ToString().ToLower().Trim();
                                    if (!string.IsNullOrEmpty(HeaderName))
                                    {
                                        if (HeaderName.Equals("Session Tracking Number", StringComparison.OrdinalIgnoreCase))
                                        {
                                            dict.Add(col, nameof(AssetSerialNumberViewModel.SessionTrackingNumber));
                                        }
                                        else if (HeaderName.Equals("SKU", StringComparison.OrdinalIgnoreCase))
                                        {
                                            dict.Add(col, nameof(AssetSerialNumberViewModel.ManufacturerSKU));
                                        }
                                        else if (HeaderName.Equals("Serial Number", StringComparison.OrdinalIgnoreCase))
                                        {
                                            dict.Add(col, nameof(AssetSerialNumberViewModel.SerialNum));
                                        }
                                        else if (HeaderName.Equals("Asset Tag Number", StringComparison.OrdinalIgnoreCase))
                                        {
                                            dict.Add(col, nameof(AssetSerialNumberViewModel.AssetTagNum));
                                        }
                                        else if (HeaderName.Equals("Mac Address", StringComparison.OrdinalIgnoreCase))
                                        {
                                            dict.Add(col, nameof(AssetSerialNumberViewModel.MACAddress));
                                        }
                                    }
                                }
                                if (dict.Count() == 5)
                                {
                                    var fileDataListModel = new List<AssetSerialNumberViewModel>();

                                    for (int row = 2; row <= rowCount; row++)
                                    {

                                        if (string.IsNullOrEmpty(worksheet.Cells[row, 1].Value?.ToString()))
                                        {
                                            break;
                                        }

                                        fileDataListModel.Add(new AssetSerialNumberViewModel()
                                        {

                                            SessionTrackingNumber =
                                            //Check if object is not null
                                            worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.SessionTrackingNumber)).FirstOrDefault().Key].Value != null
                                            //Convert it to string and check if string is null or empty
                                            && !string.IsNullOrEmpty(worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.SessionTrackingNumber)).FirstOrDefault().Key].Value.ToString())
                                            //success case
                                            ? Convert.ToInt32(worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.SessionTrackingNumber)).FirstOrDefault().Key].Value)
                                            //failure case
                                            : (int?)null,



                                            ManufacturerSKU = worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.ManufacturerSKU)).FirstOrDefault().Key].Value != null &&
                                                            !string.IsNullOrWhiteSpace(worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.ManufacturerSKU)).FirstOrDefault().Key].Value.ToString())
                                                            ? (worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.ManufacturerSKU)).FirstOrDefault().Key].Value).ToString()
                                                            : null,


                                            SerialNum = worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.SerialNum)).FirstOrDefault().Key].Value != null &&
                                                        !string.IsNullOrWhiteSpace(worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.SerialNum)).FirstOrDefault().Key].Value.ToString())
                                                        ? (worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.SerialNum)).FirstOrDefault().Key].Value).ToString()
                                                        : null,


                                            AssetTagNum = worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.AssetTagNum)).FirstOrDefault().Key].Value != null &&
                                                        !string.IsNullOrWhiteSpace(worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.AssetTagNum)).FirstOrDefault().Key].Value.ToString())
                                                        ? (worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.AssetTagNum)).FirstOrDefault().Key].Value).ToString()
                                                        : null,
                                            MACAddress = worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.MACAddress)).FirstOrDefault().Key].Value != null &&
                                                        !string.IsNullOrWhiteSpace(worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.MACAddress)).FirstOrDefault().Key].Value.ToString())
                                                        ? (worksheet.Cells[row, dict.Where(x => x.Value == nameof(AssetSerialNumberViewModel.MACAddress)).FirstOrDefault().Key].Value).ToString()
                                                        : null,
                                        });
                                    }
                                    var ap = new AssetPresenter();
                                    var returnObject = ap.GetAssetDataToUpdate(fileDataListModel, fileName);
                                    TempData["Message"] = returnObject;
                                }
                                else
                                {
                                    mu.FileError = "Please upload valid file by downloading template";
                                    TempData["Message"] = mu;
                                }
                            }
                        }
                        else
                        {
                            mu.FileError = "Please upload valid file by downloading template";
                            TempData["Message"] = mu;
                        }
                    }                                    
                }
                else
                {
                    mu.FileError = "Excel file is required.";
                    TempData["Message"] = mu;
                }
                return RedirectToAction("AssetSerialNumberUploader");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                ViewBag.error = "Issue while connecting please try later.";
                return RedirectToAction("AssetSerialNumberUploader");
            }
        }
            
    }
}
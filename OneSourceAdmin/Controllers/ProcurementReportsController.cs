﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System.Reflection;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Diagnostics;
using OneSourceAdminCore.Helper;
using System.Data.Entity.SqlServer;

namespace OneSourceAdmin.Controllers
{
    public class ProcurementReportsController : BaseController
    {
        CurrencyPresenter currencyService;
        CustomerPresenter customerService;
        ProcurementReportsPresenter procurementService;
        CountryPresenter countryService;
        AdminMenuService adminMenuService;
        RegionPresenter regionService = new RegionPresenter();
        SFAsPresenter supplierService;

        #region Constructor
        public ProcurementReportsController()
        {
            procurementService = new ProcurementReportsPresenter();
            customerService = new CustomerPresenter();
            countryService = new CountryPresenter();
            adminMenuService = new AdminMenuService();
            currencyService = new CurrencyPresenter();
            regionService = new RegionPresenter();
            supplierService = new SFAsPresenter();
        }

        #endregion

        #region Utilities
        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();
        }
        #endregion

        #region Procurement Reports
        /// <summary>
        /// Aurangzeb Khan
        /// Landing Action for Procurement Reports. If the ReportId is the Id of the previous report, then it will take to its particular actions but if the
        /// report is from the new reports, then it is going to take the route to a single aciton which is generic for all the new reports.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProcurementReportModel GetProcurementModel(int id)
        {
            ProcurementReportModel model = new ProcurementReportModel();
            var route = adminMenuService.GetMenuById(id);
            string name = route.MenuName.ToLower().Trim().Replace(" ", "");
            var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));



            model.routeId = route.MenuId;
            model.routeName = name;
            model.reportName = route.MenuName;
            model.parentRouteId = parentRoute.MenuId;
            model.parentName = parentRoute.MenuName;
            model.parentRoute = parentRoute.MenuLink;
            var excelAction = "";
            var pdfAction = "";
            var MainAction = "";
            var CSVAction = "";
            if (name == "quotesummaryreport")
            {
                excelAction = "GetQuoteSummaryReportExcelFileDataTable";
                pdfAction = "ExportQuoteSummaryReportDataTableToPdf";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                model.CustomerId = 0;
                model.QuoteSummaryReportModel.CountryId = 0;
                //  Response.Redirect("~/" + route.MenuLink);

            }
            else if (name == "amountspentbyuser")
            {
                excelAction = "GetExcelFileISDAmountDataTable";
                pdfAction = "ExportISDAmountDataTableToPdf";
                MainAction = "";

                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                model.CustomerId = 0;
                model.ISDAmountSpendByUserReportModel.UserId = 0;
                model.ISDAmountSpendByUserReportModel.TotalOrderValueCount = null;

                // Response.Redirect("~/" + route.MenuLink);
            }
            else if (name == "preferredsupplierbycountry")
            {
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListColumns = Enum.GetValues(typeof(PreferredSupplierColumns)).Cast<PreferredSupplierColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();

                excelAction = "GetExcelFileDataTable";
                pdfAction = "ExportDataTableToPdf";
                MainAction = "";
                model.ColumnIds = "";
                // Response.Redirect("~/" + route.MenuLink);

            }
            else if (name == "customerordersreport")
            {
                excelAction = "ExportCustomerOrderstoExcel";
                pdfAction = "ExportCustomerOrdersToPdf";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                // Response.Redirect("~/" + route.MenuLink);
            }
            else if (name == "partdetailreport" || name == "partdetailwithasset")
            {
                excelAction = "ExportProcurementReportToExcel";
                pdfAction = "ExportProcurementReportToPdf";
                MainAction = "";

                //partdetail report
                model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                {
                    Text = s.CustomerName,
                    Value = s.CustomerId.ToString()
                }).ToList();

                model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();

                model.ListCurrencies = currencyService.GetCurrencies();

                model.ColumnIds = "";
                model.CurrencyId = "0";
                model.CustomerIds = "";
                

              //  Response.Redirect("~/" + route.MenuLink);
            }
            else if (name == "openordersreport" || name == "openorderscomment" || name == "openordersreportwithsocshift" || name == "partdetailforallcustomers")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                CSVAction = "ExportProcurementReportToCSV";

                // added for open orders report with SOC shift (SUNNY GOHAR)
                SOCShiftPresenter ssp = new SOCShiftPresenter();
                model.ListSOCShifts = ssp.GetSOCShifts();
                model.ListSOCShifts.Add(new GenericList() { Key = -1, Value = "--Select--" });
                model.ListSOCShifts = model.ListSOCShifts.OrderBy(x => x.Key).ToList();
                //  return View(model);
            }
            else if (name == "shippedorders")
            {
                excelAction = "ExportShippedOrdersToExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCurrencies = currencyService.GetCurrencies();
                model.ListCurrencies.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });
                //Response.Redirect("~/" + route.MenuLink);
            }
            else if (name == "openquoteswithsocshift")
            {
                excelAction = "ExportOpenQuotesWithSOCtoExcel";
                pdfAction = "";
                MainAction = "";

            }
            else if (name == "addressesbycustomer") //Added to Addresses by Customer (Adnan 13-june-2019)
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                // return View(model);

            }
            else if (name == "ytdreport")//Added by Adnan on 1-7-2019 for Year To End Report
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                // return View(model);
            }
            else if (name == "raworderdetail")//Added by Adnan on 2-7-2019 for Raw Order Detail Report
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                //return View(model);
            }
            else if (name == "uninvoicedorderdetail")//Added by Adnan on 2-7-2019 for UnInvoiced Order Detail Report
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                // return View(model);
            }
            else if (name == "invoicedorderdetail")//Added by Adnan on 10-7-2019 for Invoiced Order Detail Report
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";

                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                model.ListSuppliers = supplierService.GetSuppliersList();
                model.ListSuppliers.Insert(0, new GenericList { Key = 0, Value = "--Select--" });

                // return View(model);
            }
            //Added by Adnan for cancelled order report
            else if (name == "cancelledorders")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                //model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                //model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                // return View(model);
            }
            //--------------------------------------
            else if (name == "revenuebycustomer")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                // return View(model);
            }
            else if (name == "spendbysupplier")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                model.ListSuppliers = supplierService.GetSuppliersList();
                model.ListSuppliers.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                // return View(model);
            }
            else if (name == "projectedrevenuebycustomer")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                //  return View(model);
            }
            else if (name == "spendrevenuebycustomer")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                // return View(model);
            }
            else if (name == "oldquoteresponse" || name == "newquoteresponse" || name == "oldquoteorderconversion" || name == "newquoteorderconversion")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                //  return View(model);
            }
            else if (name == "savedbaskets" || name == "inprogressshippedrevenue")
            {
                excelAction = "ExportOpenOrderstoExcel";
                pdfAction = "";
                MainAction = "";
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                //  return View(model);
            }
            else
            {
                //Session["TempProcurementReportTable"] = null;
                excelAction = "ExportProcurementReportToExcel";
                pdfAction = "ExportProcurementReportToPdf";
                MainAction = "";
                CSVAction = "ExportProcurementReportToCSV";

                //if (model.reportName == "SLA - GSP Pricing - New Quotes" || model.reportName == "SLA - GSP Pricing - Old Quotes")
                //{
                //    // Session["TempProcurementReportModel"] = null;
                //}

                model.ListCurrencies = currencyService.GetCurrencies();
                model.ListCurrencies.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });
                model.ListCustomers = customerService.GetCustomersWithPriority();
                if (model.reportName != "Order Report" && model.reportName != "Quote Report" && model.reportName != "Jira Orders")
                {
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                }
                if (model.reportName != "Revenue By Country, Manufacturer" && model.reportName != "SLA - GSP Pricing - New Quotes" && model.reportName != "SLA - GSP Pricing - Old Quotes" && model.reportName != "Part Detail For All Customers")
                {
                    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                }

                model.ListRegions = regionService.GetRegions();

                //added for part detail with asset
                model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();




            }

            model.excelAction = excelAction;
            model.pdfAction = pdfAction;
            model.MainAction = MainAction;
            model.CSVAction = CSVAction;
            return model;
        }
        public ActionResult ProcurementReport(int id)
        {
            try
            {
                if (id > 0)
                {
                    var model = GetProcurementModel(id);
                    return View(model);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult GetReportData(ProcurementReportModel model)
        {
            try
            {
                var buttonName = Request.Form["submit"];
                if (buttonName == "Reset")
                {
                    //Session["TempProcurementReportTable"] = null;
                    // Session["TempProcurementReportModel"] = null;
                    ViewBag.Message = null;
                    return RedirectToAction("ProcurementReport", new { id = model.routeId });
                }

                string data = "";

                if (model.routeName != "openorderswithregion" && model.routeName != "openquoteswithregion")
                {
                    DateTime FromDate = DateTime.Parse(model.StartDate).Date;
                    DateTime ToDate = DateTime.Parse(model.EndDate).Date;

                    if (FromDate > ToDate)
                    {
                        ViewBag.Message = "Start date should not be greater than end date";
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }


                if (model.routeName == "openorderswithregion" || model.routeName == "openquoteswithregion")
                {
                    var regionCountries = regionService.GetRegionWiseCountry(model.RegionId).Select(x => x.CountryId);
                    if (regionCountries.Count() > 0)
                    {
                        model.CountryIDs = String.Join(",", regionCountries);
                    }
                    else
                    {
                        ViewBag.Message = "NoRecordsFound";
                        //Session["TempProcurementReportTable"] = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }

                var dt = ProcurementReportTable(model); //procurementService.GetProcurementReportData(model.routeId.ToString(), model.reportName, model);

                if (dt == null)
                {
                    ViewBag.Message = "NoRecordsFound";
                    // Session["TempProcurementReportTable"] = null;
                    return Json(new { data = data, msg = ViewBag.Message });
                }

                if (dt.Rows.Count > 0)
                {

                    ViewBag.Message = null;

                    //if (model.routeName == "amountspentbydeliveryaddress" || model.routeName == "amountspentbycountry"
                    //    || model.routeName == "amountspentbycountry" || model.routeName == "amountspentbycountry(sortedbymanufacturer)"
                    //    || model.routeName == "amountspentbymanufacturer" || model.routeName == "amountspentbypartcategory"
                    //    || model.routeName == "amountspentbycountry,mfg,part")
                    //{
                    //    DataColumnCollection columns = dt.Columns;

                    //    if (columns.Contains("blank"))
                    //    {
                    //        dt.Columns.Remove("blank");
                    //    }

                    //    if (model.routeName == "amountspentbymanufacturer")
                    //    {
                    //        var currencyInfo = currencyService.GetCurrencyInfo(Convert.ToInt32(model.CurrencyId));
                    //        for (int i = 0; i < dt.Rows.Count; i++)
                    //        {
                    //            dt.Rows[i]["Currency"] = currencyInfo.FirstOrDefault().CurrencyName;
                    //        }
                    //    }

                    //    //if (columns.Contains("CurrencyName"))
                    //    //{
                    //    //    dt.Columns.Remove("CurrencyName");
                    //    //}
                    //    //if (columns.Contains("Currency"))
                    //    //{
                    //    //    dt.Columns.Remove("Currency");
                    //    //}

                    //    if (model.routeName != "amountspentbycountry,mfg,part")
                    //    {
                    //        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByDeliveryAddress_R_Result>(dt);

                    //        var distinctMonths = list.Select(x => x.Month).Distinct();
                    //        double grandTotal = 0.0;
                    //        foreach (var item in distinctMonths)
                    //        {
                    //            var monthTotal = list.Where(x => x.Month == item).Sum(x => Convert.ToDouble(x.Total));
                    //            grandTotal += monthTotal;
                    //            DataRow dr = dt.NewRow();

                    //            dr["Month"] = "Month Total";
                    //            dr["Total"] = monthTotal.ToString("0.00");

                    //            var rows = dt.Select("Month='" + item + "'");
                    //            var indexOfRow = dt.Rows.IndexOf(rows[rows.Count() - 1]);
                    //            dt.Rows.InsertAt(dr, indexOfRow + 1);
                    //        }


                    //        DataRow dr1 = dt.NewRow();
                    //        dr1["Month"] = " ";

                    //        DataRow dr2 = dt.NewRow();
                    //        dr2["Month"] = "Grand Total";
                    //        dr2["Total"] = grandTotal.ToString("0.00");

                    //        dt.Rows.Add(dr1);
                    //        dt.Rows.Add(dr2);

                    //    }
                    //}

                    if (model.routeName == "newquoteallcustomers" || model.routeName == "oldquoteallcustomers"
                        || model.routeName == "jiraorders")
                    {
                        if (dt.Rows.Count > 500)
                        {
                              ViewBag.Message = "DownloadExcel";
                        // Session["TempProcurementReportTable"] = dt;
                        data = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                        }
                      
                    }

                    if (model.routeName == "amountspentbydeliveryaddress,category")
                    {
                        if (dt.Rows.Count > 500)
                        {
                            ViewBag.Message = "DownloadExcel";
                            // Session["TempProcurementReportTable"] = dt;
                            data = null;
                            return Json(new { data = data, msg = ViewBag.Message });
                        }
                    }

                    if (model.routeName == "orderbystatus,country" || model.routeName == "orderreport"
                        || model.routeName == "quotereport" || model.routeName == "openorderswithregion")
                    {
                        if (dt.Rows.Count > 1000)
                        {
                            ViewBag.Message = "DownloadExcel";
                            //   Session["TempProcurementReportTable"] = dt;
                            data = null;
                            return Json(new { data = data, msg = ViewBag.Message });
                        }
                    }

                    if (model.reportName == "SLA - GSP Pricing - New Quotes" || model.reportName == "SLA - GSP Pricing - Old Quotes")
                    {
                        if (dt.Rows.Count > 1000)
                        {
                            ViewBag.Message = "DownloadExcel";
                            // Session["TempProcurementReportTable"] = dt;
                            data = null;
                            return Json(new { data = data, msg = ViewBag.Message });
                        }
                    }

                    Session["TempProcurementReportTable"] = dt;
                }
                else
                {
                    ViewBag.Message = "NoRecordsFound";
                    //  Session["TempProcurementReportTable"] = null;
                    return Json(new { data = data, msg = ViewBag.Message });
                }

                // Session["TempProcurementReportModel"] = model;
                data = JsonConvert.SerializeObject(dt);


                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                ViewBag.Message = "NoRecordsFound";
                // Session["TempProcurementReportTable"] = null;
                return Json(new { data = "", msg = ViewBag.Message });
            }
        }

        /// <summary>
        /// Generic method to convert any Datatable to list
        /// </summary>
        /// <typeparam name="T">Generic</typeparam>
        /// <param name="dt">Datatable</param>
        /// <returns>List</returns>
        private List<T> ConvertDataTable<T>(DataTable dt, ProcurementReportModel model)
        {

            try
            {
                List<T> data = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    if (model.routeName == "amountspentbydeliveryaddress" || model.routeName == "amountspentbycountry"
                        || model.routeName == "amountspentbycountry(sortedbymanufacturer)" || model.routeName == "amountspentbymanufacturer"
                        || model.routeName == "amountspentbypartcategory" || model.routeName == "amountspentbycountry,mfg,part")
                    {

                        //if (Session["routeName"].ToString() == "amountspentbycountry,mfg,part")
                        //{
                        //    if (row.ItemArray[3].ToString() != "" || row.ItemArray[4].ToString() != "" || row.ItemArray[3] != null || row.ItemArray[4] != null)
                        //    {
                        //        T item = GetItem<T>(row);
                        //        data.Add(item);
                        //    }
                        //}
                        //else
                        //{
                        if (row[1].ToString() != "")
                        {
                            T item = GetItem<T>(row);
                            data.Add(item);
                        }
                        // }
                    }
                    else
                    {
                        T item = GetItem<T>(row);
                        data.Add(item);
                    }
                }
                return data;

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        private T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        if (column.ToString() == "Total" || column.ToString() == "SKU" || column.ToString() == "Description")
                        {
                            if (dr[column.ColumnName].ToString() != "")
                            {
                                pro.SetValue(obj, dr[column.ColumnName], null);
                            }
                            else
                            {
                                pro.SetValue(obj, null, null);
                            }
                        }
                        else
                        {
                            if (dr[column.ColumnName].ToString() != "")
                            {
                                pro.SetValue(obj, dr[column.ColumnName], null);
                            }
                            else
                            {
                                pro.SetValue(obj, null, null);
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            return obj;
        }

        public ActionResult ExportProcurementReportToExcel(ProcurementReportModel model)
        {
            try
            {
                //var model = new ProcurementReportModel();
                string filename = model.reportName;//Session["reportName"].ToString().Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");
                //var temp = model; //(ProcurementReportModel)Session["TempProcurementReportModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                DataTable dt = null;
                if (model.routeName == "openquotesreport")
                {
                     dt = GetOpenQouteDataTable();
                }
                else
                {
                    if (model.routeName == "openorderswithregion" || model.routeName == "openquoteswithregion")
                    {
                        var regionCountries = regionService.GetRegionWiseCountry(model.RegionId).Select(x => x.CountryId);
                        if (regionCountries.Count() > 0)
                        {
                            model.CountryIDs = String.Join(",", regionCountries);
                            if (model.routeName == "openorderswithregion")
                            {
                                dt = ProcurementReportTable(model);//(DataTable)Session["TempProcurementReportTable"];
                            }
                            else
                            {
                                dt = GetOpenQouteWithRegionDataTable(model.CountryIDs);
                            }
                           
                        }
                        else
                        {
                            ViewBag.Message = "NoRecordsFound";
                           
                          
                        }
                    }
                    else
                    {
                        dt = ProcurementReportTable(model);//(DataTable)Session["TempProcurementReportTable"];
                    }
                  
                   
                }
                if (dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, filename, Response);
                   
                }
                else
                {
                    ViewBag.Message = "NoData";
                }

                //model.ListCurrencies = currencyService.GetCurrencies();
                //model.ListCurrencies.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });
                //model.ListCustomers = customerService.GetCustomersWithPriority();
                //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                //if (model.reportName != "Revenue By Country, Manufacturer" && model.reportName != "SLA - GSP Pricing - New Quotes"
                //&& model.reportName != "SLA - GSP Pricing - Old Quotes")
                //{
                //    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                //    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                //}
                //model.ListRegions = regionService.GetRegions();
                var tempModel = GetProcurementModel(model.routeId);

                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ExportProcurementReportToPdf(ProcurementReportModel model)
        {
            try
            {
                // var model = new ProcurementReportModel();
                //var temp = (ProcurementReportModel)Session["TempProcurementReportModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                //if (Session["TempProcurementReportTable"] != null)
                //{
                var dt = ProcurementReportTable(model); //(DataTable)Session["TempProcurementReportTable"];
                string filename = model.reportName;//Session["reportName"].ToString().Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");

                PdfPTable pdfTable = FillDataTableToPdf(dt);
                ReportHelper.ExportToPdf(dt, pdfTable, filename, Response, Server);
                //}
                //else
                //{
                if (dt.Rows.Count > 0)
                {
                    ViewBag.Message = "NoData";
                }

                //model.ListCurrencies = currencyService.GetCurrencies();
                //model.ListCurrencies.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });
                //model.ListCustomers = customerService.GetCustomersWithPriority();
                //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                //if (model.reportName != "Revenue By Country, Manufacturer" && model.reportName != "SLA - GSP Pricing - New Quotes"
                //&& model.reportName != "SLA - GSP Pricing - Old Quotes")
                //{
                //    model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                //    model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                //}
                //}
                var tempModel = GetProcurementModel(model.routeId);
                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ExportProcurementReportToCSV(ProcurementReportModel model)
        {
            try
            {
                //var model = new ProcurementReportModel();
                string filename = model.reportName;//Session["reportName"].ToString().Trim();
                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");
                //var temp = (ProcurementReportModel)Session["TempProcurementReportModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                //if (Session["TempProcurementReportTable"] != null)
                //{
                DataTable dt;
                if (filename == "PartDetailForAllCustomers") // M Waleed Nasir (29-01-2020)
                {
                    dt = GetPartDetailForAllCustomersReport(model.StartDate, model.EndDate);
                }
                else
                {
                    dt = ProcurementReportTable(model);//(DataTable)Session["TempProcurementReportTable"];
                }
                
                ReportHelper.ExportToCSV(dt, filename, Response);
                //}
                //else
                //{
                if (dt.Rows.Count > 0)
                {
                    ViewBag.Message = "NoData";
                }

                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                //  }

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public ActionResult RedirectToParent(string parentRoute, string parentRouteId)
        {

            // Response.Redirect("~/" + System.Web.HttpContext.Current.Session["parentRoute"].ToString() + "/" + System.Web.HttpContext.Current.Session["parentRouteId"].ToString());
            Response.Redirect("~/" + parentRoute + "/" + parentRouteId);
            return null;
        }

        public JsonResult GetReportChartData(ProcurementReportModel model)
        {
            try
            {
                GetReportData(model);
                var dt = ProcurementReportTable(model); //(DataTable)Session["TempProcurementReportTable"];

                if (dt != null)
                {
                    if (model.routeName == "amountspentbydeliveryaddress")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByDeliveryAddress_R_Result>(dt, model);
                        var distinctMonths = list.Select(x => x.Month).Distinct().ToList();
                        var distinctAddresses = list.Where(x => x.Customer_Address != null).Select(x => x.Customer_Address).Distinct().ToList();

                        foreach (var address in distinctAddresses)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctMonths.Count(); i++)
                            {
                                //var addressValueForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Customer_Address == address).Select(x => x.Total).FirstOrDefault();
                                //stack.Add(addressValueForMonths != null ? Convert.ToDouble(addressValueForMonths) : 0.0);

                                var temp = list.Where(x => x.Month == distinctMonths[i] && x.Customer_Address == address).Select(x => x.Total);
                                Double addressValueForMonths = 0.0;
                                foreach (var value in temp)
                                {
                                    addressValueForMonths += Convert.ToDouble(value);
                                }
                                stack.Add(addressValueForMonths != null ? Convert.ToDouble(addressValueForMonths.ToString("0.00")) : 0.0);
                            }

                            var data = new
                            {
                                name = address,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctMonths.ToArray(),
                            leftLabel = "Total Amount Spent"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.routeName == "amountspentbycountry")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByCountry_R_Result>(dt, model);
                        var distinctMonths = list.Select(x => x.Month).Distinct().ToList();
                        var distinctCountries = list.Where(x => x.Country != null).Select(x => x.Country).Distinct().ToList();

                        foreach (var country in distinctCountries)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctMonths.Count(); i++)
                            {
                                //var countryValueForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Country == country).Select(x => x.Total).FirstOrDefault();
                                //stack.Add(countryValueForMonths != null ? Convert.ToDouble(countryValueForMonths) : 0.0);

                                var temp = list.Where(x => x.Month == distinctMonths[i] && x.Country == country).Select(x => x.Total);
                                Double countryValueForMonths = 0.0;
                                foreach (var value in temp)
                                {
                                    countryValueForMonths += Convert.ToDouble(value);
                                }
                                stack.Add(countryValueForMonths != null ? Convert.ToDouble(countryValueForMonths.ToString("0.00")) : 0.0);
                            }

                            var data = new
                            {
                                name = country,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctMonths.ToArray(),
                            leftLabel = "Total Amount Spent"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.routeName == "amountspentbycountry(sortedbymanufacturer)")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByCountrySortMfg_R_Result>(dt, model);
                        var distinctMonths = list.Select(x => x.Month).Distinct().ToList();
                        var distinctManufacturers = list.Where(x => x.Manufacturer != null).Select(x => x.Manufacturer).Distinct().ToList();

                        foreach (var manufacturer in distinctManufacturers)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctMonths.Count(); i++)
                            {
                                //var manufacturerForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total).FirstOrDefault();
                                //stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths) : 0.0);

                                var temp = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total);
                                Double manufacturerForMonths = 0.0;
                                foreach (var value in temp)
                                {
                                    manufacturerForMonths += Convert.ToDouble(value);
                                }
                                stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths.ToString("0.00")) : 0.0);

                            }

                            var data = new
                            {
                                name = manufacturer,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctMonths.ToArray(),
                            leftLabel = "Total Amount Spent"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.routeName == "ordershipmentbycountry")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_OrderShipTimeByCountry_R_Result>(dt, model);
                        var distinctCountries = list.Select(x => x.Country).Distinct().ToList();

                        foreach (var country in distinctCountries)
                        {
                            var day = 0;

                            int[] days = new int[distinctCountries.Count()];

                            var bizDaysForCountry = list.Where(x => x.Country == country).Select(x => x.bizdays).ToList();

                            foreach (var item in bizDaysForCountry)
                            {
                                day += Convert.ToInt32(item);
                            }

                            for (int i = 0; i < distinctCountries.Count(); i++)
                            {
                                if (distinctCountries[i].ToString() == country)
                                {
                                    days[i] = day;
                                }
                                else
                                {
                                    days[i] = 0;
                                }

                            }
                            var data = new
                            {
                                name = country,
                                data = days.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            seriesList = distinctCountries,
                            dataList = FinalList.ToArray(),
                            leftLabel = "Avg of Elapsed Days(Business)"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.routeName == "amountspentbymanufacturer")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByManufacturer_R_Result>(dt, model);
                        var distinctMonths = list.Select(x => x.Month).Distinct().ToList();
                        var distinctManufacturers = list.Where(x => x.Manufacturer != null).Select(x => x.Manufacturer).Distinct().ToList();

                        foreach (var manufacturer in distinctManufacturers)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctMonths.Count(); i++)
                            {
                                //var manufacturerForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total).FirstOrDefault();
                                //stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths) : 0.0);

                                var temp = list.Where(x => x.Month == distinctMonths[i] && x.Manufacturer == manufacturer).Select(x => x.Total);
                                Double manufacturerForMonths = 0.0;
                                foreach (var value in temp)
                                {
                                    manufacturerForMonths += Convert.ToDouble(value);
                                }
                                stack.Add(manufacturerForMonths != null ? Convert.ToDouble(manufacturerForMonths.ToString("0.00")) : 0.0);

                            }

                            var data = new
                            {
                                name = manufacturer,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctMonths.ToArray(),
                            leftLabel = "Order Total"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.routeName == "amountspentbypartcategory")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByPart2_R_Result>(dt, model);
                        var distinctMonths = list.Select(x => x.Month).Distinct().ToList();
                        var distinctCategory = list.Where(x => x.Category != null).Select(x => x.Category).Distinct().ToList();

                        foreach (var category in distinctCategory)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctMonths.Count(); i++)
                            {
                                //var addressValueForMonths = list.Where(x => x.Month == distinctMonths[i] && x.Category == category).Select(x => x.Total).FirstOrDefault();
                                //stack.Add(addressValueForMonths != null ? Convert.ToDouble(addressValueForMonths) : 0.0);

                                var temp = list.Where(x => x.Month == distinctMonths[i] && x.Category == category).Select(x => x.Total);
                                Double addressValueForMonths = 0.0;
                                foreach (var value in temp)
                                {
                                    addressValueForMonths += Convert.ToDouble(value);
                                }
                                stack.Add(addressValueForMonths != null ? Convert.ToDouble(addressValueForMonths.ToString("0.00")) : 0.0);

                            }

                            var data = new
                            {
                                name = category,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctMonths.ToArray(),
                            leftLabel = "Order Total"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.routeName == "amountspentbycountry,mfg,part")
                    {
                        List<object> FinalList = new List<object>();
                        var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByCountryManufacturerPart_R_Result>(dt, model);
                        list = list.OrderBy(x => DateTime.Parse(x.Month)).ToList();
                        var distinctMonths = list.Select(x => x.Month).Distinct().ToList();
                        var distinctCountry = list.Where(x => x.Country != null).Select(x => x.Country).Distinct().ToList();

                        foreach (var country in distinctCountry)
                        {
                            List<double> stack = new List<double>();
                            for (int i = 0; i < distinctMonths.Count(); i++)
                            {
                                var temp = list.Where(x => x.Month == distinctMonths[i] && x.Country == country).Select(x => x.Total);
                                Double addressValueForMonths = 0.0;
                                foreach (var value in temp)
                                {
                                    addressValueForMonths += Convert.ToDouble(value);
                                }
                                stack.Add(addressValueForMonths != null ? Convert.ToDouble(addressValueForMonths.ToString("0.00")) : 0.0);
                            }

                            var data = new
                            {
                                name = country,
                                data = stack.ToArray()
                            };

                            FinalList.Add(data);
                        }

                        var obj = new
                        {
                            dataList = FinalList.ToArray(),
                            seriesList = distinctMonths.ToArray(),
                            leftLabel = "Sum of Orders"
                        };

                        return Json(new { data = obj, msg = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { data = "null", msg = "No records found" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        public JsonResult GetOrderStatuses()
        {
            try
            {
                var statusList = procurementService.GetOrderStatus();
                var statuses = statusList.Select(x => new
                {
                    Value = x.OrderStatusId.ToString(),
                    Text = x.OrderStatusName
                }).ToList();

                return Json(statuses.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public DataTable ProcurementReportTable(ProcurementReportModel model)
        {
            DataTable dt = new DataTable();
            dt = procurementService.GetProcurementReportData(model.routeId.ToString(), model.reportName, model);// (DataTable)Session["TempProcurementReportTable"];
            if (model.routeName == "amountspentbydeliveryaddress" || model.routeName == "amountspentbycountry"
                    || model.routeName == "amountspentbycountry" || model.routeName == "amountspentbycountry(sortedbymanufacturer)"
                    || model.routeName == "amountspentbymanufacturer" || model.routeName == "amountspentbypartcategory"
                    || model.routeName == "amountspentbycountry,mfg,part")
            {
                DataColumnCollection columns = dt.Columns;

                if (columns.Contains("blank"))
                {
                    dt.Columns.Remove("blank");
                }

                if (model.routeName == "amountspentbymanufacturer")
                {
                    var currencyInfo = currencyService.GetCurrencyInfo(Convert.ToInt32(model.CurrencyId));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["Currency"] = currencyInfo.FirstOrDefault().CurrencyName;
                    }
                }

                if (model.routeName != "amountspentbycountry,mfg,part")
                {
                    var list = ConvertDataTable<sp_Rpt_ISD_AmountSpentByDeliveryAddress_R_Result>(dt, model);

                    var distinctMonths = list.Select(x => x.Month).Distinct();
                    double grandTotal = 0.0;
                    foreach (var item in distinctMonths)
                    {
                        var monthTotal = list.Where(x => x.Month == item).Sum(x => Convert.ToDouble(x.Total));
                        grandTotal += monthTotal;
                        DataRow dr = dt.NewRow();

                        dr["Month"] = "Month Total";
                        dr["Total"] = monthTotal.ToString("0.00");

                        var rows = dt.Select("Month='" + item + "'");
                        var indexOfRow = dt.Rows.IndexOf(rows[rows.Count() - 1]);
                        dt.Rows.InsertAt(dr, indexOfRow + 1);
                    }


                    DataRow dr1 = dt.NewRow();
                    dr1["Month"] = " ";

                    DataRow dr2 = dt.NewRow();
                    dr2["Month"] = "Grand Total";
                    dr2["Total"] = grandTotal.ToString("0.00");

                    dt.Rows.Add(dr1);
                    dt.Rows.Add(dr2);

                }
            }
            return dt;
        }
        #endregion

        #region Preferred Supplier By Country
        public ActionResult PreferredSupplierByCountry()
        { //sunny gohar on 9th Jan 2018

            try
            {
                var model = new ProcurementReportModel();
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListColumns = Enum.GetValues(typeof(PreferredSupplierColumns)).Cast<PreferredSupplierColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        public JsonResult GetData(string customerId, string columnIds = null)
        {
            try
            {
                var model = new ProcurementReportModel();
                model.CustomerId = Convert.ToInt32(customerId);
                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListColumns = Enum.GetValues(typeof(PreferredSupplierColumns)).Cast<PreferredSupplierColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();
                var dt = GetDataTable(customerId, columnIds);
                if (dt.Rows.Count > 0)
                {
                    model.PreferredSupplierDataTable = dt;
                    //  Session["tempPSTable"] = model.PreferredSupplierDataTable;

                }
                else
                {
                    ViewBag.Message = "No records found!";
                    //  Session["tempPSTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message });
                }
                if (model.CustomerId > 0)
                {
                    //Session["tempPSModel"] = model;
                }
                else
                {
                    // model = (ProcurementReportModel)Session["tempPSModel"];
                }

                var data = JsonConvert.SerializeObject(dt);
                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        public DataTable GetDataTable(string customerId, string columnIds)
        {
            try
            {
                IEnumerable<PreferredSupplierColumns> columnsEnum = Enum.GetValues(typeof(PreferredSupplierColumns)).Cast<PreferredSupplierColumns>();

                UserPresenter user = new UserPresenter();

                if (customerId == null || customerId == "")
                {
                    ViewBag.Message = "Please select customer!";
                    return null;
                }

                List<int> columnIdsList = new List<int>();
                if (columnIds != "null" && columnIds != "")
                {
                    var arr = columnIds.Split(',');
                    int[] intIds = arr.Select(x => int.Parse(x)).ToArray();
                    columnIdsList.AddRange(intIds);
                }

                var cols = columnsEnum.Where(x => columnIdsList.Contains((int)x)).Select(x => new
                {
                    Name = x.ToString() == "IsPreferred" ? "sfaup.IsPreferred" : x.ToString() == "LastUpdated" ? "sfaup.LastUpdated" : x.ToString() == "LastUpdatedByUserID" ? "sfaup.LastUpdatedByUserID" : x.ToString() == "SFAStandardUplift" ? "sfaup.SFAStandardUplift" : x.ToString() == "PrimeContractorUplift" ? "sfaup.PrimeContractorUplift" : x.ToString(),
                    Description = GetEnumDescription(x)
                }).ToList();

                if (cols.Count > 0)
                {
                    cols.RemoveAll(x => x.Name.Equals("SFAName")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("CountryName")); // remove already defined columns in SP from list
                }

                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ISDCountryGSPProcRep_R", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ColumnList", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerIds", SqlDbType.Text));
                    command.Parameters[0].Value = "";
                    if (cols.Count > 0)
                    {
                        var colString = string.Join(",", cols.Select(c => string.Concat(c.Name + " as " + '"' + c.Description + '"')));
                        command.Parameters[0].Value = "," + colString;
                    }
                    command.Parameters[1].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }

                var isPreffered = dt.Columns["Uplift Preffered"];
                var updatedBy = dt.Columns["Uplift Last Updated By"];
                var index = -1;
                var u_index = -1;
                if (isPreffered != null)
                {
                    index = dt.Columns["Uplift Preffered"].Ordinal;
                }
                if (updatedBy != null)
                {
                    u_index = dt.Columns["Uplift Last Updated By"].Ordinal;
                }

                var data = dt;

                DataTable dtCloned = dt.Clone();
                if (index > -1)
                {
                    dtCloned.Columns[index].DataType = typeof(string);
                }
                if (u_index > -1)
                {
                    dtCloned.Columns[u_index].DataType = typeof(string);
                }
                foreach (DataRow row in dt.Rows)
                {
                    dtCloned.ImportRow(row);
                }
                foreach (DataRow row in dtCloned.Rows)
                {
                    if (index > -1)
                    {
                        row[index] = row[index].Equals("True") ? "Yes" : "No";
                    }
                    if (u_index > -1)
                    {
                        var userId = row[u_index];
                        row[u_index] = user.GetUsers().FirstOrDefault(x => x.UserID == Convert.ToInt32(userId)).UserName;
                    }
                }

                return dtCloned;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        public ActionResult GetExcelFileDataTable(string customerId, string columnIds, string reportName, int routeId)
        {
            try
            {
                //var model = new ProcurementReportModel();
                //model.CustomerId = Convert.ToInt32(customerId);
                //model.ListCustomers = customerService.GetCustomersWithPriority();
                //model.ListColumns = Enum.GetValues(typeof(PreferredSupplierColumns)).Cast<PreferredSupplierColumns>().Select(s => new SelectListItem
                //{
                //    Text = GetEnumDescription(s),
                //    Value = ((int)s).ToString()
                //}).ToList();

                //var temp = (ProcurementReportModel)Session["tempPSModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                //if (model.PreferredSupplierDataTable != null)
                //{

                var model = GetProcurementModel(routeId);
                var dt = GetDataTable(customerId, columnIds);

                ReportHelper.ExportToExcel(dt, reportName, Response);
                // }
                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public ActionResult ExportDataTableToPdf(string customerId, string columnIds, string reportName,int routeId)
        {
            try
            {
                //var model = new ProcurementReportModel();
                //model.CustomerId = Convert.ToInt32(customerId2);
                //model.ListCustomers = customerService.GetCustomersWithPriority();
                //model.ListColumns = Enum.GetValues(typeof(PreferredSupplierColumns)).Cast<PreferredSupplierColumns>().Select(s => new SelectListItem
                //{
                //    Text = GetEnumDescription(s),
                //    Value = ((int)s).ToString()
                //}).ToList();
                //var temp = (ProcurementReportModel)Session["tempPSModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                //if (model.PreferredSupplierDataTable != null)
                //{
                var model = GetProcurementModel(routeId);

                var dt = GetDataTable(customerId, columnIds);
                model.PreferredSupplierDataTable = dt;
                PdfPTable pdfTable = FillDataTableToPdf(model.PreferredSupplierDataTable);
                ReportHelper.ExportToPdf(model.PreferredSupplierDataTable, pdfTable, reportName, Response, Server);
                //}
                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        #endregion

        #region Part Detail

        public ActionResult PartDetailReport()
        { //sunny gohar on 9th Jan 2018

            try
            {
                var model = new ProcurementReportModel();
                model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                {
                    Text = s.CustomerName,
                    Value = s.CustomerId.ToString()
                }).ToList();

                model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();

                model.ListCurrencies = currencyService.GetCurrencies();

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        public DataTable GetPartDetailDataTable(string customerIds, string columnIds, string currencyId, string startDate, string endDate, string topRows = "1")
        {
            try
            {
                if (String.IsNullOrWhiteSpace(currencyId))
                {
                    currencyId = "12"; // Default USD
                }
                IEnumerable<PartDetailColumns> columnsEnum = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>();

                UserPresenter user = new UserPresenter();

                List<int> columnIdsList = new List<int>();
                if (columnIds != "null" && columnIds != "")
                {
                    var arr = columnIds.Split(',');
                    int[] intIds = arr.Select(x => int.Parse(x)).ToArray();
                    columnIdsList.AddRange(intIds);
                }

                var cols = columnsEnum.Where(x => columnIdsList.Contains((int)x)).Select(x => new
                {
                    Name = x.ToString() == "Order_Country" ? "Order_Country as 'Order Country'" :
                    x.ToString() == "Destination_PO" ? "replace(Destination_PO, ',', ';') as 'Destination PO'" :
                    x.ToString() == "Accepted_Date" ? "cast(month(Accepted_Date) as varchar(5)) + '/' + cast(day(Accepted_Date) as varchar(5)) + '/' + cast(year(Accepted_Date) as varchar(5)) as 'Accepted Date'" :
                    x.ToString() == "Business_Days_Open" ? "dbo.GetWorkingDayswithinRange(o.OrderDate,GETDATE()) as 'Business Days Open'" :
                    x.ToString() == "Delivery_SLA_Business_Days" ? "replace(cds.days, ',', ';') as 'Delivery SLA (Business Days)'" :
                    x.ToString() == "Status" ? "replace(Status, ',', ';') as Status" :
                    x.ToString() == "Shipping_Address" ? "replace(Shipping_Address, ',', ';') as 'Shipping Address'" :
                    x.ToString() == "Billing_Address" ? "replace(Billing_Address, ',', ';') as 'Billing Address'" :
                    x.ToString() == "Bundle" ? "replace(IsNull(Bundle, 'N/A'), ',', ';') as Bundle" :
                    x.ToString() == "Category" ? "replace(Category, ',', ';') as Category" :
                    x.ToString() == "Manufacturer" ? "replace(Manufacturer, ',', ';') as Manufacturer" :
                    x.ToString() == "Supplier" ? "replace(Supplier, ',', ';') as Supplier" :
                    x.ToString() == "MFG_Tracking_Info" ? "replace(MFG_Tracking_Info, ',', ';') as 'MFG Tracking Info'" :
                    x.ToString() == "Catalog_Type" ? "replace(Catalog_Type, ',', ';') as 'Catalog Type'" :
                    x.ToString() == "Buyer" ? "replace(Buyer, ',', ';') as Buyer" :
                    //x.ToString() == "Order_Currency" ? "replace(cur.currencyname, ',', ';') as 'Order Currency'" : Removed from optional (SUNNY GOHAR : 18th Dec 2018)
                    x.ToString() == "Item_Price" ? "replace(Item_Price, ',', ';') as 'Item Price USD'" :
                    x.ToString() == "Item_Total" ? "replace(Item_Total, ',', ';') as 'Item Total USD'" :
                    //  x.ToString() == "Item_Price_USD" ? "CAST(ROUND(oi.lineitempriceusd*pcx.ExchangeRateToUSDollars,2) As varchar) as 'Item Price USD'" :
                    x.ToString() == "Quantity" ? "Quantity as Quantity" :
                    // x.ToString() == "Item_Total_USD" ? "CAST(ROUND(oi.lineitemtotalusd*pcx.ExchangeRateToUSDollars,2) As varchar) as 'Item Total USD'" :
                    x.ToString() == "DateDelivered" ? "cast(month(Delivered_Date) as varchar(5)) + '/' + cast(day(Delivered_Date) as varchar(5)) + '/' + cast(year(Delivered_Date) as varchar(5)) as 'Date Delivered'" : x.ToString(),//Added Date Delivered as optional column ( SUNNY GOHAR : 14th Jan 2019 )
                    Description = GetEnumDescription(x)
                }).ToList();

                if (cols.Count > 0)
                {
                    cols.RemoveAll(x => x.Name.Equals("Customer")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Tracking_No")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Order_PO")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Order_Date")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("ETS")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Ship_Date")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Part_Description")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("SKU")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Currency")); // remove already defined columns in SP from list
                }

                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ISDPartDetailedProcRep_R", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ColumnList", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerIds", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CurrencyID", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@topRows", SqlDbType.Text));
                    command.Parameters[0].Value = "";
                    command.Parameters[1].Value = "";
                    command.Parameters[2].Value = "";
                    command.Parameters[3].Value = "";
                    command.Parameters[4].Value = "";
                    command.Parameters[5].Value = topRows.ToString();
                    if (cols.Count > 0)
                    {
                        var colString = string.Join(",", cols.Select(c => c.Name));
                        command.Parameters[0].Value = "," + colString;
                    }
                    if (customerIds != "" && customerIds != null && customerIds != "0" && customerIds != "null")
                    {
                        command.Parameters[1].Value = customerIds;
                    }
                    if (startDate != "" && startDate != null && startDate != "null")
                    {
                        command.Parameters[2].Value = startDate;
                    }
                    if (endDate != "" && endDate != null && endDate != "null")
                    {
                        command.Parameters[3].Value = endDate;
                    }
                    if (currencyId != "" && currencyId != null && currencyId != "0" && currencyId != "null")
                    {
                        command.Parameters[4].Value = currencyId;
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }

                    //Aurangzeb Khan
                    //4-12-2018

                    dt.Columns.Add("MSRP", typeof(System.String)).SetOrdinal(11);

                    foreach (DataRow row in dt.Rows)
                    {
                        if (!string.IsNullOrEmpty(row["MSRP_NewQuote"].ToString()))
                        {
                            row["MSRP"] = row["MSRP_NewQuote"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(row["MSRP_OldQuote"].ToString()))
                        {
                            row["MSRP"] = row["MSRP_OldQuote"].ToString();
                        }
                        else
                        {
                            row["MSRP"] = "-";
                        }

                        if (dt.Columns.Contains("Date Delivered"))
                        {
                            if (!string.IsNullOrEmpty(row["Date Delivered"].ToString()))
                            {
                                row["Date Delivered"] = row["Date Delivered"].ToString();
                            }
                            else
                            {
                                row["Date Delivered"] = "-";
                            }
                        }

                    }

                    dt.Columns.Remove("MSRP_NewQuote");
                    dt.Columns.Remove("MSRP_OldQuote");

                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetPartDetailDataTable \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        /// <summary>
        /// Aurangzeb Khan
        /// 1-11-2018
        /// Modified - Added Item# in excel output
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPartDetailData(string customerIds, string currencyId, string startDate, string endDate,string routeId, string columnIds = null)
        {
            try
            {
                DateTime? start = null;
                DateTime? end = null;

                if (!string.IsNullOrEmpty(startDate))
                {
                    start = Convert.ToDateTime(startDate);
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    end = Convert.ToDateTime(endDate);
                }

                if (start != null && end != null)
                {
                    if (start.Value.Date > end.Value.Date)
                    {
                        ViewBag.Message = "DateError";
                    }
                }

                var model = GetProcurementModel(Convert.ToInt32(routeId));// new ProcurementReportModel();

                CurrencyPresenter cup = new CurrencyPresenter();

                //model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                //{
                //    Text = s.CustomerName,
                //    Value = s.CustomerId.ToString()
                //}).ToList();

                //model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                //{
                //    Text = GetEnumDescription(s),
                //    Value = ((int)s).ToString()
                //}).ToList();

                //model.ListCurrencies = cup.GetCurrencies();

                var dt = GetPartDetailDataTable(customerIds, columnIds, currencyId, startDate, endDate);
                if (dt.Rows.Count > 0)
                {
                    //Session["tempPSTable"] = dt.Copy();
                    dt.Columns.Remove("CatalogStandardId");
                    dt.Columns.Remove("Bundle Description");


                }
                else
                {
                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                   // Session["tempPSTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message });
                }

                var data = JsonConvert.SerializeObject(dt);
                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        /// <summary>
        /// Aurangzeb Khan
        /// 1-11-2018
        /// Modified - Added Item# in excel output
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPartDetailExcelFileDataTable()
        {
            try
            {
                var customerId = Request.Form["customerIds"];
                var columnIds = Request.Form["columnIds"];
                var currencyId = Request.Form["currencyId"];
                var startDate = Request.Form["startDate"];
                var endDate = Request.Form["endDate"];
                var routeId = Request.Form["routeId"];
                var reportname = Request.Form["reportname"];

                var model = GetProcurementModel(Convert.ToInt32(routeId));//new ProcurementReportModel();

                CurrencyPresenter cup = new CurrencyPresenter();

                //model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                //{
                //    Text = s.CustomerName,
                //    Value = s.CustomerId.ToString()
                //}).ToList();

                //model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                //{
                //    Text = GetEnumDescription(s),
                //    Value = ((int)s).ToString()
                //}).ToList();

                //model.ListCurrencies = cup.GetCurrencies();

                var dt = GetPartDetailDataTable(customerId, columnIds, currencyId, startDate, endDate, "0");

                //Iterate through dt and replace catalog id with item number according to repition of the item

                dt.Columns.Add("Bundle Item#").SetOrdinal(dt.Columns["Part Description"].Ordinal);

                string tempCatalogId = "";
                string tempOrderNum = "";
                int itemCount = 1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["CatalogStandardId"].ToString() != "")
                    {
                        if (dt.Rows[i]["CatalogStandardId"].ToString() == tempCatalogId && dt.Rows[i]["Tracking #"].ToString() == tempOrderNum)
                        {
                            dt.Rows[i]["Bundle Item#"] = itemCount;
                        }
                        else
                        {
                            tempCatalogId = dt.Rows[i]["CatalogStandardId"].ToString();
                            tempOrderNum = dt.Rows[i]["Tracking #"].ToString();
                            itemCount = 1;
                            dt.Rows[i]["Bundle Item#"] = itemCount;
                        }
                        itemCount++;
                    }
                }

                dt.Columns.Remove("CatalogStandardId");

                ReportHelper.ExportToExcel(dt, reportname, Response);

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        #region Part Detail with Asset

        public DataTable GetPartDetailWithAssetDataTable(string customerIds, string columnIds, string currencyId, string startDate, string endDate, string topRows = "1", string serialNumberRequired = "0")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(serialNumberRequired))
                {
                    serialNumberRequired = "0";
                }
                if (String.IsNullOrWhiteSpace(currencyId))
                {
                    currencyId = "12"; // Default USD
                }
                IEnumerable<PartDetailColumns> columnsEnum = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>();

                UserPresenter user = new UserPresenter();

                List<int> columnIdsList = new List<int>();
                if (columnIds != "null" && columnIds != "")
                {
                    var arr = columnIds.Split(',');
                    int[] intIds = arr.Select(x => int.Parse(x)).ToArray();
                    columnIdsList.AddRange(intIds);
                }

                var cols = columnsEnum.Where(x => columnIdsList.Contains((int)x)).Select(x => new
                {
                    Name = x.ToString() == "Order_Country" ? "o.Order_Country as 'Order Country'" :
                    x.ToString() == "Destination_PO" ? "replace(o.Destination_PO, ',', ';') as 'Destination PO'" :
                    x.ToString() == "Accepted_Date" ? "cast(month(o.Accepted_Date) as varchar(5)) + '/' + cast(day(o.Accepted_Date) as varchar(5)) + '/' + cast(year(o.Accepted_Date) as varchar(5)) as 'Accepted Date'" :
                    x.ToString() == "Business_Days_Open" ? "dbo.GetWorkingDayswithinRange(o.Order_Date,GETDATE()) as 'Business Days Open'" :
                    x.ToString() == "Delivery_SLA_Business_Days" ? "replace(cds.days, ',', ';') as 'Delivery SLA (Business Days)'" :
                    x.ToString() == "Status" ? "replace(o.Status, ',', ';') as Status" :
                    x.ToString() == "Shipping_Address" ? "replace(o.Shipping_Address, ',', ';') as 'Shipping Address'" :
                    x.ToString() == "Billing_Address" ? "replace(o.Billing_Address, ',', ';') as 'Billing Address'" :
                    x.ToString() == "Bundle" ? "replace(IsNull(o.Bundle, 'N/A'), ',', ';') as Bundle" :
                    x.ToString() == "Category" ? "replace(o.category, ',', ';') as Category" :
                    x.ToString() == "Manufacturer" ? "replace(o.Manufacturer, ',', ';') as Manufacturer" :
                    x.ToString() == "Supplier" ? "replace(o.Supplier, ',', ';') as Supplier" :
                    x.ToString() == "MFG_Tracking_Info" ? "replace(o.MFG_Tracking_Info, ',', ';') as 'MFG Tracking Info'" :
                    x.ToString() == "Catalog_Type" ? "replace(o.Catalog_Type, ',', ';') as 'Catalog Type'" :
                    x.ToString() == "Buyer" ? "replace(o.Buyer, ',', ';') as Buyer" :
                    x.ToString() == "Order_Currency" ? "replace(o.Currency, ',', ';') as 'Order Currency'" :
                    x.ToString() == "Item_Price" ? "replace(o.Item_Price, ',', ';') as 'Item Price'" :
                    x.ToString() == "Item_Total" ? "replace(o.Item_total, ',', ';') as 'Item Total'" :
                    x.ToString() == "Quantity" ? "o.quantity as Quantity" : x.ToString(),
                    Description = GetEnumDescription(x)
                }).ToList();

                if (cols.Count > 0)
                {
                    cols.RemoveAll(x => x.Name.Equals("Customer")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Tracking_No")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Order_PO")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Order_Date")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("ETS")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Ship_Date")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Part_Description")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("SKU")); // remove already defined columns in SP from list
                    cols.RemoveAll(x => x.Name.Equals("Currency")); // remove already defined columns in SP from list
                }

                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ISDPartDetailedwAsset_R", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@ColumnList", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerIds", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CurrencyID", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@topRows", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@SerialNumberRequired", SqlDbType.Text));
                    command.Parameters[0].Value = "";
                    command.Parameters[1].Value = "";
                    command.Parameters[2].Value = "";
                    command.Parameters[3].Value = "";
                    command.Parameters[4].Value = "";
                    command.Parameters[5].Value = topRows.ToString();
                    command.Parameters[6].Value = serialNumberRequired.ToString();
                    if (cols.Count > 0)
                    {
                        var colString = string.Join(",", cols.Select(c => c.Name));
                        command.Parameters[0].Value = "," + colString;
                    }
                    if (customerIds != "" && customerIds != null && customerIds != "0" && customerIds != "null")
                    {
                        command.Parameters[1].Value = customerIds;
                    }
                    if (startDate != "" && startDate != null && startDate != "null")
                    {
                        command.Parameters[2].Value = startDate;
                    }
                    if (endDate != "" && endDate != null && endDate != "null")
                    {
                        command.Parameters[3].Value = endDate;
                    }
                    if (currencyId != "" && currencyId != null && currencyId != "0" && currencyId != "null")
                    {
                        command.Parameters[4].Value = currencyId;
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                DataView view = new DataView(dt);
                DataTable distinctValues = view.ToTable(true);
                return distinctValues;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        public JsonResult GetPartDetailWithAssetData(string customerIds, string currencyId, string startDate, string endDate, string columnIds = null, string serialNumberRequired = "0")
        {
            try
            {
                DateTime? start = null;
                DateTime? end = null;

                if (!string.IsNullOrEmpty(startDate))
                {
                    start = Convert.ToDateTime(startDate);
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    end = Convert.ToDateTime(endDate);
                }

                if (start != null && end != null)
                {
                    if (start.Value.Date > end.Value.Date)
                    {
                        ViewBag.Message = "DateError";
                    }
                }

                var model = new ProcurementReportModel();

                CurrencyPresenter cup = new CurrencyPresenter();

                model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                {
                    Text = s.CustomerName,
                    Value = s.CustomerId.ToString()
                }).ToList();

                model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();

                model.ListCurrencies = cup.GetCurrencies();

                var dt = GetPartDetailWithAssetDataTable(customerIds, columnIds, currencyId, startDate, endDate, serialNumberRequired: serialNumberRequired);
                if (dt.Rows.Count > 0)
                {
                    //Session["tempPSTable"] = dt;

                }
                else
                {
                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                    //Session["tempPSTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message });
                }

                var data = JsonConvert.SerializeObject(dt);
                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        public ActionResult GetPartDetailWithAssetExcelFileDataTable()
        {
            try
            {
                var customerId = Request.Form["customerIds1"];
                var columnIds = Request.Form["columnIds1"];
                var currencyId = Request.Form["currencyId1"];
                var startDate = Request.Form["startDate1"];
                var endDate = Request.Form["endDate1"];
                var serialNumRequired = Request.Form["serialnumrequired"];
                var reportName = Request.Form["reportName"];

                var model = new ProcurementReportModel();

                CurrencyPresenter cup = new CurrencyPresenter();

                model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                {
                    Text = s.CustomerName,
                    Value = s.CustomerId.ToString()
                }).ToList();

                model.ListColumns = Enum.GetValues(typeof(PartDetailColumns)).Cast<PartDetailColumns>().Select(s => new SelectListItem
                {
                    Text = GetEnumDescription(s),
                    Value = ((int)s).ToString()
                }).ToList();

                model.ListCurrencies = cup.GetCurrencies();

                var dt = GetPartDetailWithAssetDataTable(customerId, columnIds, currencyId, startDate, endDate, "0", serialNumRequired);

                ReportHelper.ExportToExcel(dt, reportName, Response);

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        #endregion

        #endregion

        #region Customer Orders
        /// <summary>
        /// Get Customer Order
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="orderTrackingNumber"></param>
        /// <param name="destinationTrackingNumber"></param>
        /// <returns></returns>
        public List<CustomerOrderItems> GetCustomerOrderItems(int sessionTrackingNumber, int orderTrackingNumber, int destinationTrackingNumber)
        {
            return procurementService.GetCustomerOrderItems(sessionTrackingNumber, orderTrackingNumber, destinationTrackingNumber);
        }
        /// <summary>
        ///  Get Customer Orders List
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<CustomerOrderReportModel> GetCustomerOrdersList(int customerId, string startDate, string endDate)
        {
            try
            {
                DateTime? start = null;
                DateTime? end = null;

                if (!string.IsNullOrEmpty(startDate))
                {
                    start = Convert.ToDateTime(startDate);
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    end = Convert.ToDateTime(endDate);
                }

                if (start != null && end != null)
                {
                    if (start.Value.Date > end.Value.Date)
                    {
                        ViewBag.Message = "DateError";
                        return null;
                    }
                }

                return procurementService.GetCustomerOrderDetails(customerId, start, end);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        /// <summary>
        /// Customer Orders Report View
        /// </summary>
        /// <returns></returns>
        public ActionResult CustomerOrdersReport()
        {
            try
            {
                //sunny gohar on 9th Jan 2018
                CurrencyPresenter cup = new CurrencyPresenter();

                var model = new ProcurementReportModel();
                model.ListCustomers = customerService.GetCustomersWithPriority();
                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        /// <summary>
        /// Get customer orders
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult GetCustomerOrders(ProcurementReportModel model)
        {
            try
            {
                CurrencyPresenter cup = new CurrencyPresenter();
                var tempModel = GetProcurementModel(model.routeId);
                if (model.CustomerId > 0)
                {
                    tempModel.CustomerOrderReportListModel.CustomerOrderDetails = GetCustomerOrdersList(Convert.ToInt32(model.CustomerId), model.StartDate, model.EndDate);
                    //Session["tempModel"] = model;
                    tempModel.CustomerId = model.CustomerId;
                }
                else
                {
                   // model = (ProcurementReportModel)Session["tempModel"];
                }
                // model.ListCustomers = customerService.GetCustomersWithPriority();
               
                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        /// <summary>
        /// Export Customer Orders to Excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportCustomerOrderstoExcel(ProcurementReportModel model)
        {
            try
            {
                //var model = new ProcurementReportModel();
                //var temp = (ProcurementReportModel)Session["tempModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                var tempModel = GetProcurementModel(model.routeId);
                if (model.CustomerId > 0)
                {
                    var dt = ToDataTable<CustomerOrderReportModel>(GetCustomerOrdersList(Convert.ToInt32(model.CustomerId), model.StartDate, model.EndDate));
                    //ToDataTable<CustomerOrderReportModel>(model.CustomerOrderReportListModel.CustomerOrderDetails);

                    ReportHelper.ExportToExcel(dt, model.reportName, Response);
                    tempModel.CustomerId = model.CustomerId;
                }
                
                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        /// <summary>
        /// Export Customer Orders to Pdf
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportCustomerOrdersToPdf(ProcurementReportModel model)
        {
            try
            {

                //var model = new ProcurementReportModel();
                //var temp = (ProcurementReportModel)Session["tempModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                var tempModel = GetProcurementModel(model.routeId);
                if (model.CustomerId > 0)
                {
                    var dt =  ToDataTable<CustomerOrderReportModel>(GetCustomerOrdersList(Convert.ToInt32(model.CustomerId), model.StartDate, model.EndDate)); //ToDataTable<CustomerOrderReportModel>(model.CustomerOrderReportListModel.CustomerOrderDetails);

                    iTextSharp.text.Image logoImg =
                    iTextSharp.text.Image.GetInstance(Server.MapPath("/Content/assets/images/onesource-logo.jpg"));
                    logoImg.ScaleAbsolute(250, 45);

                    var isPreffered = dt.Columns["Standard Uplift IsPreffered"];
                    var index = -1;
                    if (isPreffered != null)
                    {
                        index = dt.Columns["Standard Uplift IsPreffered"].Ordinal;
                    }

                    PdfPTable pdfTable = FillDataTableToPdf(dt);

                    ReportHelper.ExportToPdf(dt, pdfTable, model.reportName, Response, Server);
                    tempModel.CustomerId = model.CustomerId;
                }
              
                return View("ProcurementReport", tempModel);


            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        /// <summary>
        /// Get Customer Order Items
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="orderTrackingNumber"></param>
        /// <param name="destinationTrackingNumber"></param>
        /// <returns></returns>
        public ActionResult GetOrderItems(int sessionTrackingNumber, int orderTrackingNumber, int destinationTrackingNumber)
        {
            try
            {
                if (sessionTrackingNumber > 0 && orderTrackingNumber > 0 && destinationTrackingNumber > 0)
                {
                    var list = GetCustomerOrderItems(sessionTrackingNumber, orderTrackingNumber, destinationTrackingNumber);

                    if (list.Count <= 0)
                    {
                        return Json("", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string html = string.Empty;

                        html = "<table class='table table-striped grid - table'>";
                        html += "<thead>";
                        html += "<tr>";
                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Sku";
                        html += "</th>";
                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Item Description";
                        html += "</th>";
                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Unit Price (USD)";
                        html += "</th>";
                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Quantity";
                        html += "</th>";
                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Total Price (USD)";
                        html += "</th>";
                        html += "</tr>";
                        html += "</thead>";

                        html += "<tbody>";
                        for (int i = 0; i < list.Count; i++)
                        {
                            html += "<tr>";
                            html += "<td>";
                            html += list[i].Sku;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].ItemDescription;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].UnitPrice;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].Quantity;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].TotalPrice;
                            html += "</td>";
                            html += "</tr>";
                        }

                        html += "</tbody>";

                        return Json(html, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        #endregion
        
        
        #region ISD Amount Spent By User
        /// <summary>
        /// Landing action for ISD Amount spent by user
        /// </summary>
        /// <returns>View</returns>
        public ActionResult CustomerSpendingReport()
        {
            try
            {
                var model = new ProcurementReportModel();
                Session["CustomerSpendingData"] = null;

                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                model.CustomerId = 0;
                model.ISDAmountSpendByUserReportModel.UserId = 0;
                model.ISDAmountSpendByUserReportModel.TotalOrderValueCount = null;

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Gets the data for ISD Amount Spent By User
        /// </summary>
        /// <param name="model"></param>
        /// <returns>model</returns>
        public ActionResult GetISDAmountData(ProcurementReportModel model)
        {
            try
            {

                var buttonName = Request.Form["submit"];
                var tempModel = GetProcurementModel(model.routeId);//(ProcurementReportModel)Session["CustomerSpendingData"];

                if (buttonName == "Generate")
                {

                    if (model.CustomerId > 0)
                    {
                        if (model.ISDAmountSpendByUserReportModel.UserId > 0)
                        {
                            if (model.StartDate != null && model.EndDate != null)
                            {
                                DateTime FromDate = DateTime.Parse(model.StartDate).Date;
                                DateTime ToDate = DateTime.Parse(model.EndDate).Date;

                                if (FromDate > ToDate)
                                {
                                    ViewBag.Message = "DateError";
                                    //model.ListCustomers = customerService.GetCustomersWithPriority();
                                    //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                                    //Session["CustomerSpendingData"] = null;
                                    return View("~/Views/ProcurementReports/ProcurementReport.cshtml", model);
                                }
                            }
                            else if ((model.StartDate != null && model.EndDate == null) || (model.StartDate == null && model.EndDate != null))
                            {
                                ViewBag.Message = "MissingDateError";
                                //model.ListCustomers = customerService.GetCustomersWithPriority();
                                //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                                //Session["CustomerSpendingData"] = null;
                                return View("~/Views/ProcurementReports/ProcurementReport.cshtml", model);
                            }

                            if (model.CustomerId > 0 && model.ISDAmountSpendByUserReportModel.UserId > 0)
                            {
                                //if (tempModel.CustomerId != model.CustomerId || tempModel.ISDAmountSpendByUserReportModel.UserId != model.ISDAmountSpendByUserReportModel.UserId || tempModel.StartDate != model.StartDate || tempModel.EndDate != model.EndDate)
                                //{
                                    tempModel.ISDAmountSpendByUserReportModel.ListOrders = procurementService.GetOrdersByUserId(model.ISDAmountSpendByUserReportModel.UserId, model.StartDate, model.EndDate);
                                    tempModel.CustomerId = model.CustomerId;
                                    tempModel.ISDAmountSpendByUserReportModel.UserId = model.ISDAmountSpendByUserReportModel.UserId;
                                    tempModel.StartDate = model.StartDate;
                                    tempModel.EndDate = model.EndDate;

                                    double totalSum = 0.0;
                                foreach (var item in tempModel.ISDAmountSpendByUserReportModel.ListOrders)
                                {
                                    totalSum += Convert.ToDouble(item.TotalOrderValueUSD);
                                }


                                tempModel.ISDAmountSpendByUserReportModel.TotalOrderValueCount = totalSum.ToString("0.00");
                                //}
                              //  model = tempModel;
                               // Session["CustomerSpendingData"] = tempModel;
                            }
                            //else
                            //{
                            //   // Session["CustomerSpendingData"] = model;
                            //    model.ISDAmountSpendByUserReportModel.ListOrders = procurementService.GetOrdersByUserId(model.ISDAmountSpendByUserReportModel.UserId, model.StartDate, model.EndDate);

                            //    double totalSum = 0.0;
                            //    foreach (var item in model.ISDAmountSpendByUserReportModel.ListOrders)
                            //    {
                            //        totalSum += Convert.ToDouble(item.TotalOrderValueUSD);
                            //    }
                            //    model.ISDAmountSpendByUserReportModel.TotalOrderValueCount = totalSum.ToString("0.00");

                            //    model.ListCustomers = customerService.GetCustomersWithPriority();
                            //    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                            //}
                        }
                        else
                        {
                            ViewBag.Message = "NoUserError";
                            //model.ListCustomers = customerService.GetCustomersWithPriority();
                            //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                            //Session["CustomerSpendingData"] = null;
                        }
                    }
                    else
                    {
                        ViewBag.Message = "NoCustomerError";
                        //model.ListCustomers = customerService.GetCustomersWithPriority();
                        //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                        //Session["CustomerSpendingData"] = null;
                    }

                }
                else if (buttonName == "Reset")
                {
                    // return RedirectToAction("CustomerSpendingReport");
                    return View("~/Views/ProcurementReports/ProcurementReport.cshtml", tempModel);
                }
                else
                {
                    //if (tempModel != null)
                    //{
                    //    model = tempModel;
                    //    Session["CustomerSpendingData"] = tempModel;
                    //}
                    //else
                    //{
                        //return RedirectToAction("CustomerSpendingReport");
                        return View("~/Views/ProcurementReports/ProcurementReport.cshtml", tempModel);
                   // }
                }

                return View("~/Views/ProcurementReports/ProcurementReport.cshtml", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Excel export for ISD Amount Spent By User
        /// </summary>
        /// <returns></returns>
        public ActionResult GetExcelFileISDAmountDataTable(ProcurementReportModel model)
        {
            try
            {
                // var model = new ProcurementReportModel();
                var tempModel = GetProcurementModel(model.routeId);//(ProcurementReportModel)Session["CustomerSpendingData"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                if (model.CustomerId > 0 && model.ISDAmountSpendByUserReportModel.UserId > 0)
                {
                    var listOutput = procurementService.GetOrdersByUserId(model.ISDAmountSpendByUserReportModel.UserId, model.StartDate, model.EndDate);//temp.ISDAmountSpendByUserReportModel.ListOrders;
                    var dt = ToDataTable(listOutput);

                    DataRow dr = dt.NewRow();
                    dr["SessionTrackingNumber"] = "";
                    dr["OrderPO"] = "";
                    dr["OrderDate"] = "";
                    dr["TotalOrderValueUSD"] = "";
                    dt.Rows.Add(dr);

                    DataRow dr2 = dt.NewRow();
                    dr2["SessionTrackingNumber"] = "";
                    dr2["OrderPO"] = "";
                    dr2["OrderDate"] = "Total";

                    double totalSum = 0.0;
                    foreach (var item in listOutput)
                    {
                        totalSum += Convert.ToDouble(item.TotalOrderValueUSD);
                    }
                    tempModel.ISDAmountSpendByUserReportModel.TotalOrderValueCount = totalSum.ToString("0.00");

                    dr2["TotalOrderValueUSD"] = totalSum.ToString("0.00");
                    dt.Rows.Add(dr2);

                    ReportHelper.ExportToExcel(dt, "AmountSpentByUser", Response);
                    tempModel.CustomerId = model.CustomerId;
                    tempModel.ISDAmountSpendByUserReportModel.UserId = model.ISDAmountSpendByUserReportModel.UserId;

                }
                else
                {
                    ViewBag.Message = "NoData";
                    //model.ListCustomers = customerService.GetCustomersWithPriority();
                    //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                }

                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        ///  Pdf export for ISD Amount Spent By User
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportISDAmountDataTableToPdf(ProcurementReportModel model)
        {
            try
            {
                //var model = new ProcurementReportModel();
                var tempModel = GetProcurementModel(model.routeId);//(ProcurementReportModel)Session["CustomerSpendingData"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                if (model.CustomerId > 0  && model.ISDAmountSpendByUserReportModel.UserId > 0)
                {
                    var listOutput = procurementService.GetOrdersByUserId(model.ISDAmountSpendByUserReportModel.UserId, model.StartDate, model.EndDate);//temp.ISDAmountSpendByUserReportModel.ListOrders;
                    var dt = ToDataTable(listOutput);

                    DataRow dr = dt.NewRow();
                    dr["SessionTrackingNumber"] = "";
                    dr["OrderPO"] = "";
                    dr["OrderDate"] = "";
                    dr["TotalOrderValueUSD"] = "";
                    dt.Rows.Add(dr);

                    DataRow dr2 = dt.NewRow();
                    dr2["SessionTrackingNumber"] = "";
                    dr2["OrderPO"] = "";
                    dr2["OrderDate"] = "Total";

                    double totalSum = 0.0;
                    foreach (var item in listOutput)
                    {
                        totalSum += Convert.ToDouble(item.TotalOrderValueUSD);
                    }
                    tempModel.ISDAmountSpendByUserReportModel.TotalOrderValueCount = totalSum.ToString("0.00");
                    dr2["TotalOrderValueUSD"] = totalSum.ToString("0.00");
                    dt.Rows.Add(dr2);

                    PdfPTable pdfTable = FillDataTableToPdf(dt);
                    ReportHelper.ExportToPdf(dt, pdfTable, "AmountSpentByUser", Response, Server);
                    tempModel.CustomerId = model.CustomerId;
                    tempModel.ISDAmountSpendByUserReportModel.UserId = model.ISDAmountSpendByUserReportModel.UserId;

                }
                else
                {
                    ViewBag.Message = "NoData";
                    //model.ListCustomers = customerService.GetCustomersWithPriority();
                    //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                }
                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Get User By Customer ID
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="selectedId"></param>
        /// <returns>UserList</returns>
        public JsonResult GetUserCustomerId(int customerId, int selectedId)
        {
            try
            {
                var usersList = procurementService.GetUserByCustomerId(customerId);
                var countries = usersList.Select(x => new
                {
                    Value = x.UserID.ToString(),
                    Text = x.UserName,
                    SelectedId = !usersList.Select(s => s.UserID).Contains(selectedId) ? 0 : selectedId
                }).ToList();

                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Fills PDF Table from Data Table
        /// </summary>
        /// <param name="data">Datatable</param>
        /// <returns>PDF Table</returns>
        public PdfPTable FillDataTableToPdf(DataTable data)
        {
            try
            {
                var dt = data;
                var columnsCount = dt.Columns.Count;
                BaseColor color = WebColors.GetRGBColor("#ffffff");
                iTextSharp.text.Font fontLabel = new iTextSharp.text.Font(null, 8f, iTextSharp.text.Font.BOLD, color);
                iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(null, 8f);
                BaseColor backgrounColor = WebColors.GetRGBColor("#3b9ad2");

                PdfPTable table = new PdfPTable(data.Columns.Count);
                PdfPRow row = null;
                float[] widths = new float[data.Columns.Count];

                for (int i = 0; i < data.Columns.Count; i++)
                {
                    widths[i] = 15f;
                }

                table.SetWidths(widths);

                table.WidthPercentage = 100;
                int iCol = 0;
                string colname = "";

                int[] rowItems = new int[columnsCount];

                for (int i = 0; i < columnsCount; i++)
                {
                    rowItems[i] = i;
                }

                foreach (DataColumn c in dt.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(c.ColumnName, fontLabel));
                    cell.BackgroundColor = backgrounColor;
                    table.AddCell(cell);
                }

                foreach (DataRow r in dt.Rows)
                {
                    for (int i = 0; i < rowItems.Count(); i++)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            table.AddCell(new Phrase(r[i].ToString(), fontNormal));
                        }
                    }
                }

                return table;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        /// <summary>
        /// Aurangzeb Khan
        /// This table converts a list to datatable
        /// </summary>
        /// <param name="T">generic parameter</param>
        /// <returns>Datatable</returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (prop.Name == "SessionTrackingNumber")
                {
                    dataTable.Columns.Add(prop.Name, typeof(string));
                }
                else
                {
                    dataTable.Columns.Add(prop.Name, type);
                }

            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        #endregion

        //point
        #region Quote Summary Report
        /// <summary>
        /// Landing Action for Quote Summary Report
        /// </summary>
        /// <returns></returns>
        public ActionResult QuoteSummaryReport()
        {
            try
            {
                var model = new ProcurementReportModel();
                Session["QuoteSummaryData"] = null;

                model.ListCustomers = customerService.GetCustomersWithPriority();
                model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                model.CustomerId = 0;
                model.QuoteSummaryReportModel.CountryId = 0;

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Gets the data for the Quote Summary Report
        /// </summary>
        /// <param name="model"></param>
        /// <returns>model</returns>
        public ActionResult GetQuote(ProcurementReportModel model)
        {
            try
            {
                var buttonName = Request.Form["submit"];
                var tempModel = GetProcurementModel(model.routeId);//(ProcurementReportModel)Session["QuoteSummaryData"];
                if (buttonName == "Generate")
                {

                    if (model.StartDate != null && model.EndDate != null)
                    {
                        DateTime FromDate = DateTime.Parse(model.StartDate).Date;
                        DateTime ToDate = DateTime.Parse(model.EndDate).Date;

                        if (FromDate > ToDate)
                        {
                            ViewBag.Message = "DateError";
                            //model.ListCustomers = customerService.GetCustomersWithPriority();
                            //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                            //return View("~/Views/ProcurementReports/ProcurementReport.cshtml", model);
                            return View("ProcurementReport", tempModel);
                        }
                    }

                    if (model.CustomerId != 0)
                    {
                        //if (tempModel != null)
                        //{
                        //    if (tempModel.CustomerId != model.CustomerId || tempModel.QuoteSummaryReportModel.CountryId != model.QuoteSummaryReportModel.CountryId || tempModel.StartDate != model.StartDate || tempModel.EndDate != model.EndDate)
                        //    {


                                tempModel.QuoteSummaryReportModel.ListOutputData = procurementService.getQuoteSummaryReportData(model.CustomerId, model.QuoteSummaryReportModel.CountryId, model.StartDate, model.EndDate);
                                tempModel.CustomerId = model.CustomerId;
                                tempModel.QuoteSummaryReportModel.CountryId = model.QuoteSummaryReportModel.CountryId;
                                tempModel.StartDate = model.StartDate;
                                tempModel.EndDate = model.EndDate;
                            //}
                           // model = tempModel;
                           // Session["QuoteSummaryData"] = tempModel;
                        //}
                        //else
                        //{
                        //   // Session["QuoteSummaryData"] = model;
                        //    //model.QuoteSummaryReportModel.ListOutputData = procurementService.getQuoteSummaryReportData(model.CustomerId, model.QuoteSummaryReportModel.CountryId, model.StartDate, model.EndDate);
                        //    //model.ListCustomers = customerService.GetCustomersWithPriority();
                        //    //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                        //}
                    }
                    else
                    {
                        ViewBag.Message = "NoCustomerError";
                        //model.ListCustomers = customerService.GetCustomersWithPriority();
                        //model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                       // Session["QuoteSummaryData"] = null;
                    }
                }
                else if (buttonName == "Reset")
                {
                    //   return RedirectToAction("QuoteSummaryReport");
                    return View("ProcurementReport", tempModel);
                }
                else
                {
                    //if (tempModel != null)
                    //{

                    //    model = tempModel;
                    //    //Session["QuoteSummaryData"] = tempModel;
                    //}
                    //else
                    //{
                        // return RedirectToAction("QuoteSummaryReport");
                        return View("ProcurementReport", tempModel);
                   // }
                }

                return View("~/Views/ProcurementReports/ProcurementReport.cshtml", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Exports Quote Summary Report data to Excel
        /// </summary>
        /// <returns></returns>
        public ActionResult GetQuoteSummaryReportExcelFileDataTable(ProcurementReportModel model)
        {
            try
            {
                //  var model = new ProcurementReportModel();
                var tempModel = GetProcurementModel(model.routeId);//(ProcurementReportModel)Session["QuoteSummaryData"];
                //if (temp != null)
                //{
                //    model = temp;
                //}

                if (model.CustomerId > 0)
                {
                    var listOutput = procurementService.getQuoteSummaryReportData(model.CustomerId, model.QuoteSummaryReportModel.CountryId, model.StartDate, model.EndDate);//temp.QuoteSummaryReportModel.ListOutputData;
                    var dt = ToDataTable(listOutput);
                    ReportHelper.ExportToExcel(dt, "QuoteSummaryReportModel", Response);
                }
                else
                {
                    ViewBag.Message = "NoData";
                    //    model.ListCustomers = customerService.GetCustomersWithPriority();
                    //    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });
                    
                }

                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Exports Quote Summary Report data to Pdf
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportQuoteSummaryReportDataTableToPdf(ProcurementReportModel model)
        {
            try
            {
                //  var model = new ProcurementReportModel();
                //var temp = (ProcurementReportModel)Session["QuoteSummaryData"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                //if (temp != null && temp.QuoteSummaryReportModel.ListOutputData.Count > 0)
                //{
                //    var listOutput = temp.QuoteSummaryReportModel.ListOutputData;
                //    var dt = ToDataTable(listOutput);

                //    PdfPTable pdfTable = FillDataTableToPdf(dt);
                //    ReportHelper.ExportToPdf(dt, pdfTable, "QuoteSummaryReportModel", Response, Server);
                //}
                //else
                //{
                //    ViewBag.Message = "NoData";
                //    model.ListCustomers = customerService.GetCustomersWithPriority();
                //    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                //}
              
                var tempModel = GetProcurementModel(model.routeId);//(ProcurementReportModel)Session["QuoteSummaryData"];
              

                if (model.CustomerId > 0)
                {
                    var listOutput = procurementService.getQuoteSummaryReportData(model.CustomerId, model.QuoteSummaryReportModel.CountryId, model.StartDate, model.EndDate);//temp.QuoteSummaryReportModel.ListOutputData;
                    var dt = ToDataTable(listOutput);
                    PdfPTable pdfTable = FillDataTableToPdf(dt);
                    ReportHelper.ExportToPdf(dt, pdfTable, "QuoteSummaryReportModel", Response, Server);
                }
                else
                {
                    ViewBag.Message = "NoData";
                   
                }

                return View("ProcurementReport", tempModel);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// Returns the quote response details for a particular quote
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns>quote response details</returns>
        public ActionResult GetQuoteDetails(int quoteId, string currencySymbol)
        {
            try
            {
                if (quoteId > 0)
                {
                    var list = procurementService.getQuoteResponseByQuoteID(quoteId);

                    if (list.Count > 0)
                    {
                        var dt = ToDataTable(list);

                        var data = JsonConvert.SerializeObject(dt);

                        string html = string.Empty;

                        html = "<table class='table table-striped'>";
                        html += "<thead>";
                        html += "<tr>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Manufacturer";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "SKU";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "PartCategory";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Description";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Quantity";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "Unit Price (" + currencySymbol + ")";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 50px;'>";
                        html += "EOL (mm/dd/yyyy)";
                        html += "</th>";

                        html += "</tr>";
                        html += "</thead>";

                        html += "<tbody>";
                        for (int i = 0; i < list.Count; i++)
                        {
                            html += "<tr>";
                            html += "<td>";
                            html += list[i].Manufacturer;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].SKU;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].PartCategory;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].Description;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].Qunatity;
                            html += "</td>";
                            html += "<td>";
                            html += Convert.ToDouble(list[i].Price).ToString("0.00");
                            html += "</td>";
                            html += "<td>";
                            html += list[i].EOL;
                            html += "</td>";
                            html += "</tr>";
                        }

                        html += "</tbody>";

                        return Json(html, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("<div style='margin-top:10px;margin-bottom:0px;' class='alert alert-warning'> Quote response not found!</div>", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// Returns Countries for a particular customer 
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="selectedId"></param>
        /// <returns>Countries list</returns>
        public JsonResult GetCountryByCustomerId(int customerId, int selectedId)
        {
            try
            {
                var countriesList = countryService.GetCountries();
                var countries = countriesList.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value,
                    SelectedId = !countriesList.Select(s => s.Key).Contains(selectedId) ? 0 : selectedId
                }).ToList();

                return Json(countries.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Open Orders Report

        /// <summary>
        /// Open Order Report Page
        /// </summary>
        /// <returns>View</returns>
        public ActionResult OpenOrdersReport()
        { //sunny gohar on 9th Jan 2018

            try
            {
                return View("ProcurementReport");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// Utility method for getting Open Orders data and converting it to datatable
        /// </summary>
        /// <returns>Datatable</returns>
        public DataTable GetOpenOrdersDataTable()
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OpenOrdersReport_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                DataTable distinctValues = null;

                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    //dt.Columns.Add("Comments By Admin");
                //    //dt.Columns.Add("Comments By Supplier");

                //    foreach (DataRow row in dt.Rows)
                //    {
                //        var AdminComments = (from r in dt.AsEnumerable()
                //                             where r.Field<int>("Session Tracking #") == Convert.ToInt32(row["Session Tracking #"])
                //                             let objectArray = new List<string>
                //                                  {
                //                                    r.Field<string>("Comment By Admin")
                //                                  }
                //                             select String.Join(",", objectArray)).Distinct();

                //        var SupplierComments = (from r in dt.AsEnumerable()
                //                                where r.Field<int>("Session Tracking #") == Convert.ToInt32(row["Session Tracking #"])
                //                                let objectArray = new List<string>
                //                                          {
                //                                    r.Field<string>("Comment By Supplier")
                //                                          }
                //                                select String.Join(",", objectArray)).Distinct();

                //        if (AdminComments.Count() > 0)
                //        {
                //            var strValue = "";
                //            foreach (var comment in AdminComments)
                //            {
                //                strValue += String.Format(comment + " {0} ", "<br/>");
                //            }
                //            row["Comment By Admin"] = strValue;
                //        }

                //        if (SupplierComments.Count() > 0)
                //        {
                //            var strValue = "";
                //            foreach (var comment in SupplierComments)
                //            {
                //                strValue += String.Format(comment + " {0} ", "<br/>");
                //            }
                //            row["Comment By Supplier"] = strValue;
                //        }

                //        row["Comment By Supplier"] = (row["Comment By Supplier"]==null)?"-": row["Comment By Supplier"].ToString();
                //        row["Comment By Admin"] = (row["Comment By Admin"] == null) ? "-" : row["Comment By Admin"].ToString();

                //    }
                //}

                DataView view = new DataView(dt);
                distinctValues = view.ToTable(true);
                System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = distinctValues;

                return distinctValues;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        /// <summary>
        /// Action method for getting open orders data from database
        /// </summary>
        /// <returns>datatable in json format</returns>
        public JsonResult GetOpenOrdersData()
        {
            try
            {
                var model = new ProcurementReportModel();
                var dt = GetOpenOrdersDataTable();
                if (dt.Rows.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["tempTable"] = dt;
                }
                else
                {
                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                    System.Web.HttpContext.Current.Session["tempTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message });
                }


                var data = JsonConvert.SerializeObject(dt);
                var data2 = new
                {
                    data = data
                };

                //return Json(new { data = data, msg = ViewBag.Message },JsonRequestBehavior.AllowGet);
                return new JsonResult() { Data = data2, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        /// <summary>
        /// Get open orders comments on popups
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <returns>datatable in json format</returns>
        public JsonResult GetOpenOrderComment(string sessionTrackingNumber)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(sessionTrackingNumber))
                {

                    var commentDt = procurementService.GetOpenOrderComment(Convert.ToInt64(sessionTrackingNumber));
                    var data = JsonConvert.SerializeObject(commentDt);
                    return Json(new { data = data }, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        //public ActionResult IsExportOpenOrderstoExcel()
        //{
        //    return null
        //}

        public ActionResult ExportOpenOrderstoExcel(FormCollection col)
        {
            try
            {
                var socShiftId = Convert.ToInt32(col["SOCShiftId"]);
                var startDate = Convert.ToString(col["StartDate"]); //Convert.ToString(col["dateStart"]);
                var endDate = Convert.ToString(col["EndDate"]); //Convert.ToString(col["dateEnd"]);
                var customerId = Convert.ToString(col["customerId"]);
                var supplierId = Convert.ToString(col["supplierId"]);
                var parentcompanyId = Convert.ToInt32(col["ParentCompanyID"]);
                var reportname = Convert.ToString(col["reportname"]);
                var routeId = Convert.ToInt32(col["routeId"]);
                Response.Buffer = true;
                var model = GetProcurementModel(routeId);// new ProcurementReportModel();
                DataTable dt = null;
                if (reportname == "Open Orders Report")
                {
                    dt = GetOpenOrdersDataTable();
                }
                else if (reportname == "Open Orders Comment")
                {
                    dt = GetOpenOrdersCommentReportDataTable();
                }
                else if (reportname == "Open Orders Report With SOC Shift")
                {
                    dt = GetOpenOrdersReportWithSOCShiftDataTable(socShiftId);
                }
                else if (reportname == "Part Detail For All Customers")
                {
                    dt = GetPartDetailForAllCustomersReport(startDate, endDate);
                }
                else if (reportname == "Addresses By Customer")// Added by Adnan 13-june-19
                {
                    dt = GetAddressByCustomerReport(customerId);


                    //model.ListCustomers = customerService.GetCustomersWithPriority();
                   // model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "YTD Report")// Added by Adnan 1-7-19 for YTD Report
                {
                    var tempCustomerIds = "";
                    var customerIds = customerService.GetCustomerWithParentCompanyId(parentcompanyId, Convert.ToInt32(customerId));
                    if (customerIds != null && customerIds.Count > 0)
                    {
                        tempCustomerIds = string.Join(",", customerIds.Select(n => n.ToString()).ToArray());
                    }
                    var EndDate = DateTime.Now;
                    var StartDate = EndDate.AddMonths(-12);
                    dt = GetYeartoEndReport(tempCustomerIds, StartDate.ToString(), EndDate.ToString());
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                   // model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Raw Order Detail")//Added by Adnan 2-7-2019 for Raw Order Detail Report
                {
                    dt = GetRawOrderDetailReport(Convert.ToInt32(customerId), startDate, endDate);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "UnInvoiced Order Detail")//Added by Adnan 2-7-2019 for UnInvoiced Order Detail Report
                {
                    dt = GetUnInvoicedOrderDetailReport(Convert.ToInt32(customerId), startDate, endDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Invoiced Order Detail")//Added by Adnan 10-7-2019 for UnInvoiced Order Detail Report
                {
                    dt = GetInvoicedOrderDetailReport(Convert.ToInt32(customerId), Convert.ToInt32(supplierId), startDate, endDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                    model.ListSuppliers = supplierService.GetSuppliersList();
                    model.ListSuppliers.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                }
                else if (reportname == "Revenue by Customer")
                {
                    dt = GetRevenueByCustomerReport(Convert.ToInt32(customerId), startDate, endDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Spend By supplier")
                {
                    dt = GetSpendBySupplierReport(Convert.ToInt32(customerId), Convert.ToInt32(supplierId), startDate, endDate);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                   // model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                  //  model.ListSuppliers = supplierService.GetSuppliersList();
                  //  model.ListSuppliers.Insert(0, new GenericList { Key = 0, Value = "--Select--" });
                }
                else if (reportname == "Projected Revenue by customer")
                {
                    dt = GetProjectedRevenueByCustomerReport(Convert.ToInt32(customerId), startDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Spend revenue by customer")
                {
                    dt = GetSpendRevenueByCustomerReport(Convert.ToInt32(customerId), startDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Old Quote Response")
                {
                    dt = GetOldQuoteResponseReport(Convert.ToInt32(customerId), startDate, endDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "New Quote Response")
                {
                    dt = GetNewQuoteResponseReport(Convert.ToInt32(customerId), startDate, endDate);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                   // model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "New Quote Order Conversion")
                {
                    dt = GetNewQuoteOrderConversionReport(Convert.ToInt32(customerId), startDate, endDate);
                  //  model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Old Quote Order Conversion")
                {
                    dt = GetOldQuoteOrderConversionReport(Convert.ToInt32(customerId), startDate, endDate);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                   // model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "Saved Baskets")
                {
                    dt = GetSavedBasketsReport(Convert.ToInt32(customerId), startDate, endDate);
                   // model.ListCustomers = customerService.GetCustomersWithPriority();
                  //  model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                }
                else if (reportname == "In Progress Shipped Revenue")
                {
                    dt = GetInProgressShippedRevenueReport(startDate, endDate);
                }
                else if (reportname == "Cancelled Orders")
                {
                    SqlParameter[] paremeter = new SqlParameter[]
               {
                    new SqlParameter("@Startdate", startDate),
                    new SqlParameter("@Enddate", endDate)
               };
                    dt = GetDataFromSP(paremeter, "sp_getCancelledOrders_AdminR");//GetCancelledOrderReport(startDate, endDate);
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, reportname, Response);
                }
                else
                {
                    ViewBag.Message = "Error";
                }


                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// Aurangzeb Khan
        /// 4-12-2018
        /// Export for Quotes Report with Excel
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public ActionResult ExportOpenQuotesWithSOCtoExcel(FormCollection col)
        {
            try
            {
                var socShiftId = Convert.ToInt32(col["socshiftId"]);
                var startDate = Convert.ToString(col["dateStart"]);
                var endDate = Convert.ToString(col["dateEnd"]);
                var reportName = Convert.ToString(col["reportName"]);
                var routeId = Convert.ToInt32(col["routeId"]);

                var model = GetProcurementModel(routeId); //new ProcurementReportModel();
                DataTable dt = new DataTable();

                dt = GetOpenQuotesReportWithSOCShiftDataTable();
                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, reportName, Response);
                }
                else
                {
                    ViewBag.Message = "Error";
                }

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }


        /// <summary>
        /// Utility method for getting Open Orders data with SOC Shift and converting it to datatable
        /// </summary>
        /// <returns>Datatable</returns>
        public DataTable GetOpenOrdersReportWithSOCShiftDataTable(int socShiftId = 0)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OpenOrdersReportWithSOCShifts_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (socShiftId > 0)
                    {
                        command.Parameters.Add(new SqlParameter("@SOCShiftId", SqlDbType.Int));
                        command.Parameters[0].Value = socShiftId;
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                DataTable distinctValues = null;

                DataView view = new DataView(dt);
                distinctValues = view.ToTable(true);
                System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = distinctValues;

                return distinctValues;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }


        /// <summary>
        /// Aurangzeb Khan
        /// 4-12-2018
        /// Utility method for getting Open Quotes data with SOC Shift and converting it to datatable
        /// </summary>
        /// <returns>Datatable</returns>
        public DataTable GetOpenQuotesReportWithSOCShiftDataTable(int socShiftId = 0)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OpenQuotesReportWithSOCShifts_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (socShiftId > 0)
                    {
                        command.Parameters.Add(new SqlParameter("@SOCShiftId", SqlDbType.Int));
                        command.Parameters[0].Value = socShiftId;
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                //DataTable distinctValues = null;

                //DataView view = new DataView(dt);
                //distinctValues = view.ToTable(true);
                System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = dt;

                return dt;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Open Orders Comment

        /// <summary>
        /// Open Orders Comment Report 
        /// </summary>
        /// <returns>View</returns>
        public ActionResult OpenOrdersCommentReport()
        { //sunny gohar on 9th Jan 2018

            try
            {
                return View("ProcurementReport");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        /// <summary>
        /// Utility method to get open orders comment report's data and convert it to data table 
        /// </summary>
        /// <returns>datatable with open orders comment data</returns>
        public DataTable GetOpenOrdersCommentReportDataTable()
        {

            DataTable dt = new DataTable();

            try
            {
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OpenOrdersComment_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }
                if (dt != null && dt.Rows.Count > 0)
                {

                    System.Web.HttpContext.Current.Session["TempViewCommentsTable"] = dt.Copy();

                    dt.Columns.Add("Comments By Admin");
                    dt.Columns.Add("Comments By Supplier");

                    foreach (DataRow row in dt.Rows)
                    {

                        var onlyAdminComments = (from r in dt.AsEnumerable()
                                                 where r.Field<object>("Session Tracking #").ToString() == row["Session Tracking #"].ToString() && r.Field<object>("Admin Comment").ToString() == "Yes" && r.Field<object>("Supplier").ToString() == ""
                                                 let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comment").ToString(),r.Field<object>("Supplier").ToString(),r.Field<object>("User").ToString(),r.Field<object>("Comment Date").ToString()
                                                  }
                                                 group objectArray by objectArray[1] into g
                                                 select new { Supplier = g.Key, CommentDetails = g.Select(x => new { Comment = x[0], User = x[2], Dated = x[3] }).ToList() }).Distinct();

                        var AdminCommentsForSupplier = (from r in dt.AsEnumerable()
                                                        where r.Field<object>("Session Tracking #").ToString() == row["Session Tracking #"].ToString() && r.Field<object>("Admin Comment").ToString() == "Yes" && r.Field<object>("Supplier").ToString() != ""
                                                        let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comment").ToString(),r.Field<object>("Supplier").ToString(),r.Field<object>("User").ToString(),r.Field<object>("Comment Date").ToString()
                                                  }
                                                        group objectArray by objectArray[1] into g
                                                        select new { Supplier = g.Key, CommentDetails = g.Select(x => new { Comment = x[0], User = x[2], Dated = x[3] }).ToList() }).Distinct();


                        var SupplierComments = (from r in dt.AsEnumerable()
                                                where r.Field<object>("Session Tracking #").ToString() == row["Session Tracking #"].ToString() && r.Field<object>("Admin Comment").ToString() == "No"
                                                let objectArray = new List<object>
                                                          {
                                                    r.Field<object>("Comment").ToString(),r.Field<object>("Supplier").ToString(),r.Field<object>("User").ToString(),r.Field<object>("Comment Date").ToString()
                                                          }
                                                group objectArray by objectArray[1] into g
                                                select new { Supplier = g.Key, CommentDetails = g.Select(x => new { Comment = x[0], User = x[2], Dated = x[3] }).ToList() }).Distinct();


                        row["Comments By Supplier"] = "-";
                        row["Comments By Admin"] = "-";

                        var strValue = "";

                        if (AdminCommentsForSupplier.Count() > 0)
                        {
                            foreach (var obj in AdminCommentsForSupplier)
                            {
                                var supplierName = obj.Supplier;
                                var count2 = 0;
                                strValue += String.Format("{0} Comments For " + supplierName + " {1}  {2} {2} ", new object[] { "<b>", "</b>", "<br/>" });
                                foreach (var com in obj.CommentDetails)
                                {
                                    count2++;
                                    var commentDate = com.Dated != null ? ("( Commented : " + Convert.ToDateTime(com.Dated.ToString()).ToString("dddd, dd MMMM yyyy") + " )") : "";

                                    strValue += String.Format(count2 + ") ( " + com.User + " ) : " + com.Comment + " " + commentDate + " {2} {2} ", new object[] { "<b>", "</b>", "<br/>" });
                                }


                            }
                            row["Comments By Admin"] = strValue;
                        }

                        if (onlyAdminComments.Count() > 0)
                        {

                            foreach (var obj in onlyAdminComments)
                            {
                                var count2 = 0;
                                strValue += String.Format("{0} Comments For All {1} {2} {2} ", new object[] { "<b>", "</b>", "<br/>" });

                                foreach (var com in obj.CommentDetails)
                                {
                                    count2++;
                                    var commentDate = com.Dated != null ? ("( Commented : " + Convert.ToDateTime(com.Dated.ToString()).ToString("dddd, dd MMMM yyyy") + " )") : "";

                                    strValue += String.Format(count2 + ") ( " + com.User + " ) : " + com.Comment + " " + commentDate + " {2} {2} ", new object[] { "<b>", "</b>", "<br/>" });
                                }
                            }
                            row["Comments By Admin"] = strValue;
                        }

                        if (SupplierComments.Count() > 0)
                        {
                            var strValue2 = "";
                            foreach (var obj in SupplierComments)
                            {
                                var count2 = 0;
                                foreach (var com in obj.CommentDetails)
                                {
                                    count2++;
                                    var commentDate = com.Dated != null ? ("( Commented : " + Convert.ToDateTime(com.Dated.ToString()).ToString("dddd, dd MMMM yyyy") + " )") : "";

                                    strValue2 += String.Format(count2 + ") ( " + com.User + " ) : " + com.Comment + " " + commentDate + " {0} {0} ", "<br/>");
                                }
                            }
                            row["Comments By Supplier"] = strValue2;
                        }

                    }
                    //dt.Columns.Remove("Comment");
                    //DataView view = new DataView(dt);
                    //DataTable distinctValues = view.ToTable(true, "Session Tracking #", "Comments By Admin", "Comments By Supplier");

                    //Session["TempProcurementReportTable"] = distinctValues;
                    dt.Columns.Remove("Comment");
                    DataView view = new DataView(dt);
                    DataTable distinctValues = view.ToTable(true, "Session Tracking #", "Comments By Admin", "Comments By Supplier");
                    System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = distinctValues;
                    return distinctValues;
                }

                return dt;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return dt;
            }


        }

        /// <summary>
        /// Action method to return datatable of open orders data
        /// </summary>
        /// <returns>datatable of open orders data</returns>
        public JsonResult GetOpenOrdersCommentData()
        {
            try
            {
                var model = new ProcurementReportModel();
                var dt = GetOpenOrdersCommentReportDataTable();
                if (dt != null && dt.Rows.Count > 0)
                {
                    Session["tempTable"] = dt;
                }
                else
                {
                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                    Session["tempTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message });
                }

                var data = JsonConvert.SerializeObject(dt);
                return Json(new { data = data, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        /// <summary>
        /// Get comments on popup 
        /// </summary>
        /// <param name="sessionTrackingNumber"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        public JsonResult GetOpenOrderCommentReportsCommentBySessionTrackingNumber(string sessionTrackingNumber, bool isAdmin)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(sessionTrackingNumber))
                {

                    var dt = (DataTable)Session["TempViewCommentsTable"];
                    var dt2 = new DataTable();
                    dt2.Columns.Add("Comments");

                    var strValue = "";

                    DataRow dr = dt2.NewRow();

                    if (isAdmin)
                    {
                        var onlyAdminComments = (from r in dt.AsEnumerable()
                                                 where r.Field<object>("Session Tracking #").ToString() == sessionTrackingNumber && r.Field<object>("Admin Comment").ToString() == "Yes" && r.Field<object>("Supplier").ToString() == ""
                                                 let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comment").ToString()
                                                  }
                                                 select String.Join(",", objectArray)).Distinct();

                        var AdminCommentsForSupplier = (from r in dt.AsEnumerable()
                                                        where r.Field<object>("Session Tracking #").ToString() == sessionTrackingNumber && r.Field<object>("Admin Comment").ToString() == "Yes" && r.Field<object>("Supplier").ToString() != ""
                                                        let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comment").ToString(),r.Field<object>("Supplier").ToString()
                                                  }
                                                        group objectArray by objectArray[1] into g
                                                        select new { Supplier = g.Key, Comments = g.Select(x => x[0]).ToList() }).Distinct();
                        dr["Comments"] = "-";

                        if (AdminCommentsForSupplier.Count() > 0)
                        {
                            var count = 0;


                            foreach (var obj in AdminCommentsForSupplier)
                            {
                                count++;

                                var supplierName = obj.Supplier;
                                var count2 = 0;
                                strValue += String.Format("{0} Comments For " + supplierName + " {1}  {2} {2} ", new object[] { "<b>", "</b>", "<br/>" });
                                foreach (var com in obj.Comments)
                                {
                                    count2++;
                                    strValue += String.Format(count2 + ") " + com + " {2} {2} ", new object[] { "<b>", "</b>", "<br/>" });
                                }


                            }
                            dr["Comments"] = strValue;
                        }

                        if (onlyAdminComments.Count() > 0)
                        {

                            var count = 0;

                            foreach (var comment in onlyAdminComments)
                            {
                                count++;
                                strValue += String.Format("{0} Comments For All {1} {2} {2} " + count + ") " + comment + " {2} {2}", new object[] { "<b>", "</b>", "<br/>" });
                            }
                            dr["Comments"] = strValue;
                        }

                    }
                    else
                    {
                        var SupplierComments = (from r in dt.AsEnumerable()
                                                where r.Field<object>("Session Tracking #").ToString() == sessionTrackingNumber && r.Field<object>("Admin Comment").ToString() == "No"
                                                let objectArray = new List<object>
                                                          {
                                                    r.Field<object>("Comment").ToString()
                                                          }
                                                select String.Join(",", objectArray)).Distinct();

                        if (SupplierComments.Count() > 0)
                        {
                            var strValue2 = "";
                            var count = 0;
                            foreach (var comment in SupplierComments)
                            {
                                count++;
                                strValue2 += String.Format(count + ") " + comment + " {0} {0}", "<br/>");
                            }
                            dr["Comments"] = strValue;
                        }
                    }

                    dt2.Rows.Add(dr);

                    var data = JsonConvert.SerializeObject(dt2);
                    return Json(new { data = data }, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }

        #endregion

        #region Open Quotes Report
        /// <summary>
        /// Aurangzeb Khan
        /// 20-6-2018
        /// </summary>
        /// <returns>Datatable of export and Datatable of the data to be viewed in grid.</returns>
        public DataTable GetOpenQouteDataTable()
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable outPutDT = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OpenQuotesReport_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["CompleteOpenQuoteDT"] = dt.Copy();
                    var dt2 = dt.Clone();
                    dt.Columns.Remove("IsGSComment");
                    dt.Columns.Remove("CommentDate");
                    dt.Columns.Remove("CommentsBy");
                    outPutDT = dt.Clone();

                    int tempQuoteId = 0;

                    foreach (DataRow row in dt.Rows)
                    {
                        if (tempQuoteId != Convert.ToInt32(row["Quote #"]))
                        {
                            tempQuoteId = Convert.ToInt32(row["Quote #"]);
                            var comments = from r in dt.AsEnumerable()
                                           where r.Field<int>("Quote #") == Convert.ToInt32(row["Quote #"])
                                           let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comments")
                                                  }
                                           select String.Join(",", objectArray);

                            var comments2 = comments.Where(x => !String.IsNullOrEmpty(x)).ToList();

                            if (comments2.Count() > 0)
                            {
                                var link = "<a style='color:#3b9ad2;' href='#' onclick='ViewComment(" + Convert.ToInt32(row["Quote #"]) + ")'>View Comments (" + comments2.Count() + ")</a>";
                                row["Comments"] = link;
                            }
                            else
                            {
                                row["Comments"] = "-";
                            }

                            outPutDT.Rows.Add(row.ItemArray);
                        }
                    }

                    tempQuoteId = 0;
                    var dt3 = (DataTable)System.Web.HttpContext.Current.Session["CompleteOpenQuoteDT"];
                    var originalDT = dt3.Copy();
                    dt2.Columns.Add("AdminComments");
                    originalDT.Columns.Add("AdminComments");
                    foreach (DataRow row in originalDT.Rows)
                    {
                        if (tempQuoteId != Convert.ToInt32(row["Quote #"]))
                        {

                            //if (Convert.ToInt32(row["Quote #"]) == 251)
                            //{ var a = 1; }

                            tempQuoteId = Convert.ToInt32(row["Quote #"]);
                            var comments = (from r in originalDT.AsEnumerable()
                                            where r.Field<int>("Quote #") == Convert.ToInt32(row["Quote #"])
                                            let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comments"),r.Field<object>("CommentsBy"),r.Field<object>("IsGSComment"), r.Field<object>("CommentDate")
                                                  }
                                            group objectArray by objectArray[1] into g
                                            select new { CommentBy = g.Key, Comments = g.Select(x => new { Comment = x[0], IsGSC = x[2], Date = x[3] }).ToList() }).Distinct();
                            var strValue = "-";
                            var strAdminValue = "-";
                            foreach (var obj in comments)
                            {
                                var commentsByUser = obj.CommentBy != null ? obj.CommentBy : "-";

                                var notNullComments = obj.Comments.Where(x => !String.IsNullOrEmpty((string)x.Comment)).Select(s => new { Comment = s.Comment, IsGSC = s.IsGSC, Date = s.Date }).ToList();

                                var byValue = "";
                                if (notNullComments.Count() > 0)
                                {

                                    var normalCount = 0;
                                    var gscCount = 0;

                                    byValue = String.Format("<b>Comment By: " + commentsByUser + "</b><br/>");
                                    foreach (var comment in notNullComments)
                                    {
                                        if (comment.IsGSC.ToString() == "Yes")
                                        {
                                            gscCount++;
                                            if (strAdminValue == "-" || !strAdminValue.Contains("Comment By: " + commentsByUser.ToString()))
                                            {
                                                strAdminValue += byValue + String.Format(gscCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                            else
                                            {
                                                strAdminValue += String.Format(gscCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                        }
                                        else
                                        {
                                            normalCount++;
                                            if (strValue == "-" || !strValue.Contains("Comment By: " + commentsByUser.ToString()))
                                            {
                                                strValue += byValue + String.Format(normalCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                            else
                                            {
                                                strValue += String.Format(normalCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                        }
                                    }

                                }
                            }

                            row["Comments"] = strValue;
                            row["AdminComments"] = strAdminValue;


                            dt2.Rows.Add(row.ItemArray);
                        }
                    }
                    dt2.Columns.Remove("CommentsBy");
                    dt2.Columns.Remove("CommentDate");
                    dt2.Columns.Remove("IsGSComment");
                    System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = dt2;
                }
                DataView view = new DataView(outPutDT);
                DataTable distinctValues = view.ToTable(true);
                return distinctValues;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        /// <summary>
        /// Aurangzeb Khan
        /// 20-6-2018
        /// </summary>
        /// <returns>Get the datatable from GetOpenQouteDataTable and binds it in grid.</returns>
        public JsonResult GetOpenQouteData()
        {
            try
            {
                var model = new ProcurementReportModel();
                var dt = GetOpenQouteDataTable();
                if (dt.Rows.Count > 0)
                {
                   // System.Web.HttpContext.Current.Session["tempTable"] = dt;
                }
                else
                {
                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                  //  System.Web.HttpContext.Current.Session["tempTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message });
                }

                var data = JsonConvert.SerializeObject(dt);
                return Json(new { data = data, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        /// <summary>
        /// Aurangzeb Khan
        /// 20-6-2018
        /// </summary>
        /// <returns>get comments by selected quote ID to show the .</returns>
        public JsonResult GetOpenQuoteComment(string quoteId,string routeName)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(quoteId))
                {
                    var dt = new DataTable();
                    if (routeName == "openquoteswithregion")
                    {
                        dt = (DataTable)System.Web.HttpContext.Current.Session["CompleteOpenQuoteWithRegionDT"];
                    }
                    else
                    {
                        dt =(DataTable)System.Web.HttpContext.Current.Session["CompleteOpenQuoteDT"];
                    }

                    DataTable commentDt = new DataTable();
                    commentDt.Columns.Add(" ");
                    commentDt.Columns.Add("Comments");
                    commentDt.Columns.Add("Date");
                    commentDt.Columns.Add("CommentBy");
                    commentDt.Columns.Add("IsGSComment");
                    var count = 0;

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["Quote #"].ToString() == quoteId && row["Comments"] != null && row["Comments"].ToString() != "")
                        {
                            count++;
                            DataRow dr = commentDt.NewRow();
                            dr[" "] = count;
                            dr["Comments"] = row["Comments"];
                            dr["Date"] = Convert.ToDateTime(row["CommentDate"]).ToShortDateString();
                            dr["IsGSComment"] = row["IsGSComment"];
                            dr["CommentBy"] = row["CommentsBy"];
                            commentDt.Rows.Add(dr);
                        }
                    }
                    var data = JsonConvert.SerializeObject(commentDt);
                    return Json(new { data = data }, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

        #region Open Orders Report
        /// <summary>
        /// Aurangzeb Khan
        /// 8-10-2018
        /// </summary>
        /// <returns>Countries list .</returns>
        public ActionResult GetCountiresByRegion(int regionId)
        {
            try
            {
                if (regionId > 0)
                {
                    var list = regionService.GetRegionWiseCountry(regionId);

                    if (list.Count > 0)
                    {
                        var dt = ToDataTable(list);

                        var data = JsonConvert.SerializeObject(dt);

                        string html = string.Empty;

                        html = "<table class='table table-striped'>";
                        html += "<thead>";
                        html += "<tr>";

                        html += "<th class='grid-header' style='width: 20px;'>";
                        html += "CountryId";
                        html += "</th>";

                        html += "<th class='grid-header' style='width: 150px;'>";
                        html += "Country Name";
                        html += "</th>";

                        html += "</tr>";
                        html += "</thead>";

                        html += "<tbody>";
                        for (int i = 0; i < list.Count; i++)
                        {
                            html += "<tr>";
                            html += "<td>";
                            html += list[i].CountryId;
                            html += "</td>";
                            html += "<td>";
                            html += list[i].CountryName;
                            html += "</td>";
                            html += "</tr>";
                        }

                        html += "</tbody>";

                        return Json(html, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("<div style='margin-top:10px;margin-bottom:0px;' class='alert alert-warning'> No countries found!</div>", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }
        #endregion

        #region Open Quotes With Region Report
        /// <summary>
        /// Aurangzeb Khan
        /// 9-10-2018
        /// </summary>
        /// <returns>Datatable of export and Datatable of the data to be viewed in grid.</returns>
        public DataTable GetOpenQouteWithRegionDataTable(string countryIds)
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable outPutDT = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OpenQuotesReportWRegion_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@CountryIDs", SqlDbType.Text));
                    command.Parameters[0].Value = (countryIds != null) ? countryIds.TrimEnd(',') : "";

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["CompleteOpenQuoteWithRegionDT"] = dt.Copy();
                    var dt2 = dt.Clone();
                    dt.Columns.Remove("IsGSComment");
                    dt.Columns.Remove("CommentDate");
                    dt.Columns.Remove("CommentsBy");
                    outPutDT = dt.Clone();

                    int tempQuoteId = 0;

                    foreach (DataRow row in dt.Rows)
                    {
                        if (tempQuoteId != Convert.ToInt32(row["Quote #"]))
                        {
                            tempQuoteId = Convert.ToInt32(row["Quote #"]);
                            var comments = from r in dt.AsEnumerable()
                                           where r.Field<int>("Quote #") == Convert.ToInt32(row["Quote #"])
                                           let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comments")
                                                  }
                                           select String.Join(",", objectArray);

                            var comments2 = comments.Where(x => !String.IsNullOrEmpty(x)).ToList();

                            if (comments2.Count() > 0)
                            {
                                var link = "<a style='color:#3b9ad2;' href='#' onclick='ViewComment(" + Convert.ToInt32(row["Quote #"]) + ")'>View Comments (" + comments2.Count() + ")</a>";
                                row["Comments"] = link;
                            }
                            else
                            {
                                row["Comments"] = "-";
                            }

                            outPutDT.Rows.Add(row.ItemArray);
                        }
                    }

                    tempQuoteId = 0;
                    var dt3 = (DataTable)System.Web.HttpContext.Current.Session["CompleteOpenQuoteWithRegionDT"];
                    var originalDT = dt3.Copy();
                    dt2.Columns.Add("AdminComments");
                    originalDT.Columns.Add("AdminComments");
                    foreach (DataRow row in originalDT.Rows)
                    {
                        if (tempQuoteId != Convert.ToInt32(row["Quote #"]))
                        {

                            //if (Convert.ToInt32(row["Quote #"]) == 251)
                            //{ var a = 1; }

                            tempQuoteId = Convert.ToInt32(row["Quote #"]);
                            var comments = (from r in originalDT.AsEnumerable()
                                            where r.Field<int>("Quote #") == Convert.ToInt32(row["Quote #"])
                                            let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("Comments"),r.Field<object>("CommentsBy"),r.Field<object>("IsGSComment"), r.Field<object>("CommentDate")
                                                  }
                                            group objectArray by objectArray[1] into g
                                            select new { CommentBy = g.Key, Comments = g.Select(x => new { Comment = x[0], IsGSC = x[2], Date = x[3] }).ToList() }).Distinct();
                            var strValue = "-";
                            var strAdminValue = "-";
                            foreach (var obj in comments)
                            {
                                var commentsByUser = obj.CommentBy != null ? obj.CommentBy : "-";

                                var notNullComments = obj.Comments.Where(x => !String.IsNullOrEmpty((string)x.Comment)).Select(s => new { Comment = s.Comment, IsGSC = s.IsGSC, Date = s.Date }).ToList();

                                var byValue = "";
                                if (notNullComments.Count() > 0)
                                {

                                    var normalCount = 0;
                                    var gscCount = 0;

                                    byValue = String.Format("<b>Comment By: " + commentsByUser + "</b><br/>");
                                    foreach (var comment in notNullComments)
                                    {
                                        if (comment.IsGSC.ToString() == "Yes")
                                        {
                                            gscCount++;
                                            if (strAdminValue == "-" || !strAdminValue.Contains("Comment By: " + commentsByUser.ToString()))
                                            {
                                                strAdminValue += byValue + String.Format(gscCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                            else
                                            {
                                                strAdminValue += String.Format(gscCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                        }
                                        else
                                        {
                                            normalCount++;
                                            if (strValue == "-" || !strValue.Contains("Comment By: " + commentsByUser.ToString()))
                                            {
                                                strValue += byValue + String.Format(normalCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                            else
                                            {
                                                strValue += String.Format(normalCount + ") " + comment.Comment + " (Commented: " + Convert.ToDateTime(comment.Date).ToString("dddd, dd MMMM yyyy") + "){0} ", "<br/>");
                                            }
                                        }
                                    }

                                }
                            }

                            row["Comments"] = strValue;
                            row["AdminComments"] = strAdminValue;


                            dt2.Rows.Add(row.ItemArray);
                        }
                    }
                    dt2.Columns.Remove("CommentsBy");
                    dt2.Columns.Remove("CommentDate");
                    dt2.Columns.Remove("IsGSComment");
                    System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = dt2;
                }
                DataView view = new DataView(outPutDT);
                DataTable distinctValues = view.ToTable(true);
                return distinctValues;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        /// <summary>
        /// Aurangzeb Khan
        /// 9-10-2018
        /// </summary>
        /// <returns>Get the datatable from GetOpenQouteDataWithRegionTable and binds it in grid.</returns>
        public JsonResult GetOpenQouteWithRegionData(int regionid = 0)
        {
            try
            {
                var model = new ProcurementReportModel();
                var regionCountries = regionService.GetRegionWiseCountry(regionid).Select(x => x.CountryId);

                if (regionCountries.Count() <= 0)
                {
                    ViewBag.Message = "No records found!";
                    System.Web.HttpContext.Current.Session["tempTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
                }

                var CountryIDs = String.Join(",", regionCountries);
                var dt = GetOpenQouteWithRegionDataTable(CountryIDs);

                if (dt == null)
                {
                    ViewBag.Message = "No records found!";
                    System.Web.HttpContext.Current.Session["tempTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
                }

                if (dt.Rows.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["tempTable"] = dt;
                }
                else
                {
                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                    System.Web.HttpContext.Current.Session["tempTable"] = null;
                    return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
                }

                var data = JsonConvert.SerializeObject(dt);
                return Json(new { data = data, msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                ViewBag.Message = "No records found!";
                System.Web.HttpContext.Current.Session["tempTable"] = null;
                return Json(new { data = "", msg = ViewBag.Message }, JsonRequestBehavior.AllowGet);
            }


        }
        #endregion

        #region Part Detail For All Customers

        public DataTable GetPartDetailForAllCustomersReport(string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_PartDetailForAllCustomers_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.VarChar));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.VarChar));
                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(dt);
                    }
                }

                return dt;
                //DataTable distinctValues = null;

                //DataView view = new DataView(dt);
                //distinctValues = view.ToTable(true);
                //System.Web.HttpContext.Current.Session["TempProcurementReportTable"] = distinctValues;

                //return distinctValues;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Shipped Orders Report


        public ActionResult ExportShippedOrdersToExcel(FormCollection formCollection)
        {
            try
            {
                var reportName = formCollection["reportName"].ConvertObjectToString();//Session["reportName"].ConvertObjectToString();
                var dateStart = formCollection["StartDate"].ConvertObjectToString();//formCollection["dateStart"].ConvertObjectToString();
                var dateEnd = formCollection["EndDate"].ConvertObjectToString();//formCollection["dateEnd"].ConvertObjectToString();
                var currencyId = Convert.ToInt32(formCollection["currencyId"]);
                var routeId = Convert.ToInt32(formCollection["routeId"]);
                Response.Buffer = true;

                var model = GetProcurementModel(routeId);//new ProcurementReportModel { ListCurrencies = currencyService.GetCurrencies() };
              //  model.ListCurrencies.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });
                var dt = procurementService.GetShippedOrdersReport(dateStart, dateEnd, currencyId);

                if (dt != null && dt.Rows.Count > 0)
                {
                    ReportHelper.ExportToExcel(dt, reportName, Response, 1800);
                }
                else
                {
                    ViewBag.Message = "Error";
                }

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ConvertObjectToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public ActionResult ShippedOrdersReport()
        {
            try
            {
                var model = new ProcurementReportModel { ListCurrencies = currencyService.GetCurrencies() };

                return View("ProcurementReport", model);
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ConvertObjectToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public JsonResult GetShippedOrders(string startDate, string endDate, string currencyId)
        {
            try
            {
                DateTime? start = null;
                DateTime? end = null;

                if (!string.IsNullOrEmpty(startDate))
                {
                    start = Convert.ToDateTime(startDate);
                }

                if (!string.IsNullOrEmpty(endDate))
                {
                    end = Convert.ToDateTime(endDate);
                }

                if (start != null && end != null)
                {
                    if (start.Value.Date > end.Value.Date)
                    {
                        ViewBag.Message = "DateError";
                    }
                }

                var model = new ProcurementReportModel();
                var cup = new CurrencyPresenter();
                var data = string.Empty;
                model.ListCurrencies = cup.GetCurrencies();

                var dt = procurementService.GetShippedOrdersReport(startDate, endDate, currencyId.ConvertObjectToInt32());

                if (dt.Rows.Count > 0)
                {
                    Session["tempPSTable"] = dt.Copy();
                    data = JsonConvert.SerializeObject(dt);
                }
                else
                {
                    Session["tempPSTable"] = null;

                    if (ViewBag.Message != "DateError") // to show date error first!
                    {
                        ViewBag.Message = "No records found!";
                    }
                }

                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ConvertObjectToString(), routeData);
                }

                return null;
            }
        }

        #endregion

        #region Addresses by customer by Adnan nooruddin on 12-june-2019
        public JsonResult GetAddressByCustomer(string customerIds)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        public DataTable GetAddressByCustomerReport(string customerId)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OrderAddressbyCustomer_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Text));

                    command.Parameters[0].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        dt.Columns.Remove("CustomerDestinationID");
                    }

                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetAddressByCustomerReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Year to End Report by Adnan Nooruddin on 1-july-2019 
        public DataTable GetYeartoEndReport(string customerId, string startdate, string enddate)
        {
            try
            {


                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_YTDOrderRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@startdate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@enddate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@customerIds", SqlDbType.NVarChar));

                    command.Parameters[0].Value = startdate;
                    command.Parameters[1].Value = enddate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetYeartoEndReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Raw Order Detail Report by Adnan Nooruddin on 2-july-2019

        public DataTable GetRawOrderDetailReport(int customerId, string startdate, string enddate)
        {
            try
            {


                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OrderDetailsRawDataRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));

                    command.Parameters[0].Value = startdate;
                    command.Parameters[1].Value = enddate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetRawOrderDetailReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region UnInvoiced Order Detail By Adnan Nooruddin on 2-july-2019
        public DataTable GetUnInvoicedOrderDetailReport(int customerId, string startdate, string enddate)
        {
            try
            {


                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OrderDetailNotInvoicedRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));

                    command.Parameters[0].Value = startdate;
                    command.Parameters[1].Value = enddate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetUnInvoicedOrderDetailReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Invoiced Order Detail By Adnan Nooruddin on 10-july-2019
        public DataTable GetInvoicedOrderDetailReport(int customerId, int supplierId, string startdate, string enddate)
        {
            try
            {


                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_InvoicedOrdersByCustSFARep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@SFAId", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.NVarChar));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.NVarChar));


                    command.Parameters[0].Value = customerId;
                    command.Parameters[1].Value = supplierId;
                    command.Parameters[2].Value = startdate;
                    command.Parameters[3].Value = enddate;



                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetInvoicedOrderDetailReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region Revenue by Customer by adnan noooruddin on 23-7-2019
        public DataTable GetRevenueByCustomerReport(int customerId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_RevenueByCustomerRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetRevenueByCustomerReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

        #region Spend By supplier by adnan noooruddin on 23-7-2019
        public DataTable GetSpendBySupplierReport(int customerId, int supplierId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_SpendBySupplierRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@SupplierID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;
                    command.Parameters[3].Value = supplierId;


                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetSpendBySupplierReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        #endregion

        #region Projected Revenue by customer by adnan noooruddin on 23-7-2019
        public DataTable GetProjectedRevenueByCustomerReport(int customerId, string startDate)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_ProjectedRevenueByCustomerRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = customerId;


                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetProjectedRevenueByCustomerReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

        #region Spend revenue by customer by adnan noooruddin on 23-7-2019
        public DataTable GetSpendRevenueByCustomerReport(int customerId, string startDate)
        {
            try
            {
                DataTable dt = new DataTable();
                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_SpendRevenueByCustomerRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = customerId;


                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetSpendRevenueByCustomerReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

        #region Old Quote Response by adnan nooruddin on 24-07-2019
        public DataTable GetOldQuoteResponseReport(int customerId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OldQuoteReponsesRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetOldQuoteResponseReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion

        #region New Quote Response by adnan nooruddin on 24-07-2019
        public DataTable GetNewQuoteResponseReport(int customerId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_NewQuoteReponsesRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetNewQuoteResponseReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion

        #region Old Quote Order Conversion by adnan nooruddin on 24-07-2019

        public DataTable GetOldQuoteOrderConversionReport(int customerId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OldQuoteOrderConversionRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetOldQuoteOrderConversionReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion

        #region Cancelled Orders by adnan nooruddin on 1-10-2019
        public ActionResult GetCancelledOrdersReportData(ProcurementReportModel model)
        {
            try
            {
                var buttonName = Request.Form["submit"];
                if (buttonName == "Reset")
                {
                    Session["TempProcurementReportTable"] = null;
                    Session["TempProcurementReportModel"] = null;
                    ViewBag.Message = null;
                    return RedirectToAction("ProcurementReport", new { id = Convert.ToInt32(Session["routeId"]) });
                }
                string data = "";
                DateTime FromDate = DateTime.Parse(model.StartDate).Date;
                DateTime ToDate = DateTime.Parse(model.EndDate).Date;

                if (FromDate > ToDate)
                {
                    ViewBag.Message = "Start date should not be greater than end date";
                    return Json(new { data = data, msg = ViewBag.Message });
                }
                SqlParameter[] paremeter = new SqlParameter[]
                {
                    new SqlParameter("@Startdate", model.StartDate),
                    new SqlParameter("@Enddate", model.EndDate)
                };
                var dt = GetDataFromSP(paremeter, "sp_getCancelledOrders_AdminR");// GetCancelledOrderReport(model.StartDate,model.EndDate);

                if (dt == null)
                {
                    ViewBag.Message = "NoRecordsFound";
                    Session["TempProcurementReportTable"] = null;
                    return Json(new { data = data, msg = ViewBag.Message });
                }

                if (dt.Rows.Count > 0)
                {
                    ViewBag.Message = null;
                    if (dt.Rows.Count > 1000)
                    {
                        ViewBag.Message = "DownloadExcel";
                        Session["TempProcurementReportTable"] = dt;
                        data = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }
                }
                else
                {
                    ViewBag.Message = "NoRecordsFound";
                    Session["TempProcurementReportTable"] = null;
                    return Json(new { data = data, msg = ViewBag.Message });
                }

                Session["TempProcurementReportModel"] = model;
                data = JsonConvert.SerializeObject(dt);


                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                ViewBag.Message = "NoRecordsFound";
                Session["TempProcurementReportTable"] = null;
                return Json(new { data = "", msg = ViewBag.Message });
            }

        }

        public DataTable GetCancelledOrderReport(string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_getCancelledOrders_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));


                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;


                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetCancelledOrderReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion

        #region New Quote Order Conversion by adnan nooruddin on 24-07-2019
        public DataTable GetNewQuoteOrderConversionReport(int customerId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_NewQuoteOrderConversionRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetNewQuoteOrderConversionReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }

        }
        #endregion

        #region Saved Baskets by adnan nooruddin on 25-07-2019
        public DataTable GetSavedBasketsReport(int customerId, string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_SavedBasketRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;
                    command.Parameters[2].Value = customerId;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetSavedBasketsReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

        #region In Progress Shipped Revenue by adnan on 25-07-2019
        public DataTable GetInProgressShippedRevenueReport(string startDate, string endDate)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_InProgressShippedRevenueRep_Admin", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@Startdate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@Enddate", SqlDbType.Text));


                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;


                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetInProgressShippedRevenueReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion

    }
}
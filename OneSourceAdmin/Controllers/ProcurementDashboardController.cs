﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System.Reflection;
using System.Data;
using System.Web.UI.WebControls;



namespace OneSourceAdmin.Controllers
{
    public class ProcurementDashboardController : BaseController
    {
        private AdminMenuService adminMenuService;
        private SLAReportPresenter slaReportService;
        private BudgetPresenter budget;
        private CustomerPresenter customerService;
        private ProcurementDashboardPresenter procurementDashboardService;

        public ProcurementDashboardController()
        {
            adminMenuService = new AdminMenuService();
            slaReportService = new SLAReportPresenter();
            budget = new BudgetPresenter();
            customerService = new CustomerPresenter();
            procurementDashboardService = new ProcurementDashboardPresenter();
        }
        // GET: ProcurementDashboard
        public ActionResult Index()
        {
            var model = new SLAReportModel();

            model.ListCustomers = customerService.GetCustomersWithPriority();
            model.ListManufacturers = slaReportService.GetManufacturers();

            Session["Model"] = null;
            return View(model);
        }

        public JsonResult GetReportChartData(SLAReportModel model)
        {
            try
            {
                model.CategoryIds = procurementDashboardService.GetProdCatIDs(Convert.ToInt32(model.CustomerIds));
                model.StartDate = DateTime.Now.AddMonths(-5).ToShortDateString();
                model.EndDate = DateTime.Now.ToShortDateString();

                object DeliveryMetrics = null;
                object OrderAcceptance = null;
                object QuoteAcceptance = null;

                var dtDeliveryMetrics = slaReportService.GetReportData("0", "Delivery Metric PC & Server", "Summary", model);
                var dtOrderAcceptance = slaReportService.GetReportData("0", "Order Acceptance", "Summary", model);
                var dtQuoteAcceptance = slaReportService.GetReportData("0", "Quote Response", "Summary", model);

                if (dtDeliveryMetrics != null && dtDeliveryMetrics.Rows.Count > 0)
                {

                    dtDeliveryMetrics.Columns.Add("Score", typeof(System.String));
                    DataView dv = dtDeliveryMetrics.DefaultView;
                    dv.Sort = "CountryName asc";
                    dtDeliveryMetrics = dv.ToTable();

                    foreach (DataRow row in dtDeliveryMetrics.Rows)
                    {
                        int score = (int)(Convert.ToDouble(row["withinSLA"]) / Convert.ToDouble(row["Total"]) * 100);
                        row["Score"] = score;   // or set it to some other value
                    }

                    List<object> FinalList = new List<object>();
                    var list = ConvertDataTable<RE_rptSLADeliverySummary_Result>(dtDeliveryMetrics);
                    var distinctMonths = list.Select(x => x.Orderdate).Distinct().ToList();
                    int[] scores = new int[distinctMonths.Count()];

                    foreach (var month in distinctMonths)
                    {
                        var score = 0;

                        var ScoresForMonth = list.Where(x => x.Orderdate == month).Select(x => x.Score).ToList();

                        foreach (var item in ScoresForMonth)
                        {
                            score += Convert.ToInt32(item);
                        }

                        for (int i = 0; i < distinctMonths.Count(); i++)
                        {
                            if (distinctMonths[i].ToString() == month)
                            {
                                scores[i] = score;
                            }
                            else
                            {
                                scores[i] = 0;
                            }

                        }

                        var data = new
                        {
                            name = month,
                            data = scores.ToArray()
                        };

                        FinalList.Add(data);
                    }

                    string SLAForMetric = procurementDashboardService.GetSLAForMetrics(Convert.ToInt32(model.CustomerIds), Convert.ToInt32(model.CountryIds), Convert.ToInt32(model.ManufacturerIds));

                    var obj = new
                    {
                        seriesList = distinctMonths,
                        dataList = FinalList.ToArray(),
                        reportName = "Delivery Metrics",
                        subtitle = SLAForMetric
                    };

                    DeliveryMetrics = obj;
                }

                if (dtOrderAcceptance != null && dtOrderAcceptance.Rows.Count > 0)
                {

                    dtOrderAcceptance.Columns.Add("Score", typeof(System.String));
                    DataView dv = dtOrderAcceptance.DefaultView;
                    dv.Sort = "CountryName asc";
                    dtOrderAcceptance = dv.ToTable();

                    foreach (DataRow row in dtOrderAcceptance.Rows)
                    {
                        int score = (int)(Convert.ToDouble(row["withinSLA"]) / Convert.ToDouble(row["Total"]) * 100);
                        row["Score"] = score;   // or set it to some other value
                    }

                    List<object> FinalList = new List<object>();
                    var list = ConvertDataTable<RE_rptSLAOrderAcceptanceSummary_AdminR_Result>(dtOrderAcceptance);
                    var distinctMonths = list.Select(x => x.Orderdate).Distinct().ToList();
                    int[] scores = new int[distinctMonths.Count()];

                    foreach (var month in distinctMonths)
                    {
                        var score = 0;

                        var ScoresForMonth = list.Where(x => x.Orderdate == month).Select(x => x.Score).ToList();

                        foreach (var item in ScoresForMonth)
                        {
                            score += Convert.ToInt32(item);
                        }

                        for (int i = 0; i < distinctMonths.Count(); i++)
                        {
                            if (distinctMonths[i].ToString() == month)
                            {
                                scores[i] = score;
                            }
                            else
                            {
                                scores[i] = 0;
                            }

                        }

                        var data = new
                        {
                            name = month,
                            data = scores.ToArray()
                        };

                        FinalList.Add(data);
                    }
                    string SLAForOrderAcceptance = procurementDashboardService.GetSLAForOrderAcceptance(Convert.ToInt32(model.CountryIds));

                    var obj = new
                    {
                        seriesList = distinctMonths,
                        dataList = FinalList.ToArray(),
                        reportName = "Order Acceptance",
                        subtitle = SLAForOrderAcceptance
                    };

                    OrderAcceptance = obj;
                }

                if (dtQuoteAcceptance != null && dtQuoteAcceptance.Rows.Count > 0)
                {

                    dtQuoteAcceptance.Columns.Add("Score", typeof(System.String));
                    DataView dv = dtQuoteAcceptance.DefaultView;
                    dv.Sort = "Country asc";
                    dtQuoteAcceptance = dv.ToTable();

                    foreach (DataRow row in dtQuoteAcceptance.Rows)
                    {
                        int score = (int)(Convert.ToDouble(row["withinSLA"]) / Convert.ToDouble(row["Total"]) * 100);
                        row["Score"] = score;   // or set it to some other value
                    }

                    List<object> FinalList = new List<object>();
                    var list = ConvertDataTable<RE_rptQuoteResponseSummary_AdminR_Result>(dtQuoteAcceptance);
                    var distinctMonths = list.Select(x => x.Create_date).Distinct().ToList();
                    int[] scores = new int[distinctMonths.Count()];

                    foreach (var month in distinctMonths)
                    {
                        var score = 0;

                        var ScoresForMonth = list.Where(x => x.Create_date == month).Select(x => x.Score).ToList();

                        foreach (var item in ScoresForMonth)
                        {
                            score += Convert.ToInt32(item);
                        }

                        for (int i = 0; i < distinctMonths.Count(); i++)
                        {
                            if (distinctMonths[i].ToString() == month)
                            {
                                scores[i] = score;
                            }
                            else
                            {
                                scores[i] = 0;
                            }

                        }

                        var data = new
                        {
                            name = month,
                            data = scores.ToArray()
                        };

                        FinalList.Add(data);
                    }
                    string SLAForQuoteAcceptance = procurementDashboardService.GetSLAForQuoteResponse(Convert.ToInt32(model.CountryIds));

                    var obj = new
                    {
                        seriesList = distinctMonths,
                        dataList = FinalList.ToArray(),
                        reportName = "Quote Acceptance",
                        subtitle = SLAForQuoteAcceptance
                    };

                    QuoteAcceptance = obj;
                }

                return Json(new { DeliveryMetrics = DeliveryMetrics, OrderAcceptance = OrderAcceptance, QuoteAcceptance = QuoteAcceptance }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
            return null;
        }

        private List<T> ConvertDataTable<T>(DataTable dt)
        {

            try
            {
                List<T> data = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    if (row[0].ToString() != "")
                    {
                        T item = GetItem<T>(row);
                        data.Add(item);
                    }
                }
                return data;

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        private T GetItem<T>(DataRow dr)
        {
            try
            {
                Type temp = typeof(T);
                T obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in dr.Table.Columns)
                {
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {
                        if (pro.Name == column.ColumnName)
                        {
                                pro.SetValue(obj, dr[column.ColumnName], null);
                        }
                        else
                            continue;
                    }
                }
                return obj;
            }


            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                T obj = Activator.CreateInstance<T>();
                return obj;
            }
        }
    }
}
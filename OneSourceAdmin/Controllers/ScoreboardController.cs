﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdmin.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class ScoreboardController : BaseController
    {
        private ScoreboardService sbService;

        public ScoreboardController()
        {
            sbService = new ScoreboardService();
        }

        public ActionResult Index()
        {
            try
            {
                var model = new ScoreboardModel();

                model.FiltersList = sbService.GetScoreboardFilter(adminSetting.UserID, adminSetting.ParentCompanyID);
                model.DefaultFilterValue = sbService.GetDefaultFilter(adminSetting.UserID);
                model.InternalRequest = false;
                model.InternalRequest = true;

                return View("~/Views/Scoreboard/Listing.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [NoCache]
        public JsonResult GetScorboardList(int? page, int? limit, string sortBy, string direction, bool internalRequest, bool systemRequest, string filterStr = null)
        {
            int total;
            var records = sbService.GetScoreboardList(page, limit, sortBy, direction, internalRequest, systemRequest, filterStr, adminSetting.UserID, out total);
            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;


namespace OneSourceAdmin.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/
        //public string Index()
        //{
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:3748/api/GetRequestData/");
        //    byte[] bytes;

        //string contents = System.IO.File.ReadAllText(Server.MapPath("~/Exception/newFile.xml"));

        //    bytes = System.Text.Encoding.ASCII.GetBytes(contents);
        //    request.ContentType = "text/xml; encoding='utf-8'";
        //    request.ContentLength = bytes.Length;
        //    request.Method = "POST";
        //    Stream requestStream = request.GetRequestStream();
        //    requestStream.Write(bytes, 0, bytes.Length);
        //    requestStream.Close();
        //    HttpWebResponse response;
        //    response = (HttpWebResponse)request.GetResponse();
        //    if (response.StatusCode == HttpStatusCode.OK)
        //    {
        //        Stream responseStream = response.GetResponseStream();
        //        string responseStr = new StreamReader(responseStream).ReadToEnd();
        //        return responseStr;
        //    };
        //    return "false";
        //}

        public string PostFile()
        {
            String fullFilePath = @"C:\Platypi\Duckbills_JennyLind_CA.XML";
            String uri = @"http://localhost:3748/api/dplat/sendXML/";
            SendXMLFile(fullFilePath, uri, 500);
            return "ok";
        }


        public static string SendXMLFile(string xmlFilepath, string uri, int timeout)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;
            request.ContentType = "application/xml";
            request.Method = "POST";

            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Exception/newFile.xml")))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }
                byte[] postBytes = Encoding.UTF8.GetBytes(sb.ToString());

                if (timeout < 0)
                {
                    request.ReadWriteTimeout = timeout;
                    request.Timeout = timeout;
                }

                request.ContentLength = postBytes.Length;

                try
                {
                    Stream requestStream = request.GetRequestStream();

                    requestStream.Write(postBytes, 0, postBytes.Length);
                    requestStream.Close();

                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        return response.ToString();
                    }
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);
                    request.Abort();
                    return string.Empty;
                }
            }
        }

    }
}
﻿using Newtonsoft.Json;
using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    public class PriceDeskReportsController : BaseController
    {
        CurrencyPresenter currencyService;
        CustomerPresenter customerService;
        PriceDeskReportsPresenter pricedeskReportsPresenter;
        CountryPresenter countryService;
        AdminMenuService adminMenuService;
        SFAsPresenter _sfa;

        #region Constructor
        public PriceDeskReportsController()
        {
            pricedeskReportsPresenter = new PriceDeskReportsPresenter();
            customerService = new CustomerPresenter();
            countryService = new CountryPresenter();
            adminMenuService = new AdminMenuService();
            currencyService = new CurrencyPresenter();
            _sfa = new SFAsPresenter();
        }

        #endregion

        #region Utilities
        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();
        }


        #endregion

        #region Methods

        public PriceDeskReportModel GetPriceDeskModel(int id)
        {
            PriceDeskReportModel model = new PriceDeskReportModel();
            var route = adminMenuService.GetMenuById(id);
            string name = route.MenuName.ToLower().Trim().Replace(" ", "");
            var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));
            model.routeId = route.MenuId;
            model.routeName = name;
            model.reportName = route.MenuName;
            model.parentRouteId = parentRoute.MenuId;
            model.parentName = parentRoute.MenuName;
            model.parentRoute = parentRoute.MenuLink;
            model.excelAction = "ExportPriceDeskReportToExcel";
            model.pdfAction = "ExportProcurementReportToPdf";
            model.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).Select(s => new Suppliers { SupplierName = s.SFAName ?? "-", SupplierId = s.SFAID }).ToList();
            model.Suppliers.Insert(0, new Suppliers { SupplierId = 0, SupplierName = "--Select--" });

            return model;
        }
        public ActionResult PriceDeskReport(int id, bool? fromback = false)
        {
            try
            {

                if (id > 0)
                {
                    PriceDeskReportModel model = GetPriceDeskModel(id);// new PriceDeskReportModel();

                    //var route = adminMenuService.GetMenuById(id);
                    //string name = route.MenuName.ToLower().Trim().Replace(" ", "");
                    //var parentRoute = adminMenuService.GetMenuById(Convert.ToInt32(route.ParentMenuId));

                    //Session["routeId"] = route.MenuId;
                    //Session["routeName"] = name;
                    //Session["reportName"] = route.MenuName;
                    //Session["parentRouteId"] = parentRoute.MenuId;
                    //Session["parentName"] = parentRoute.MenuName;
                    //Session["parentRoute"] = parentRoute.MenuLink;

                    //model.routeId = route.MenuId;
                    //model.routeName = name;
                    //model.reportName = route.MenuName;
                    //model.parentRouteId = parentRoute.MenuId;
                    //model.parentName = parentRoute.MenuName;
                    //model.parentRoute = parentRoute.MenuLink;

                    Session["TempProcurementReportTable"] = null;
                   // Session["excelAction"] = "ExportPriceDeskReportToExcel";
                 //  model.excelAction = "ExportPriceDeskReportToExcel";

                    //Session["pdfAction"] = "ExportProcurementReportToPdf";
                 //   model.pdfAction = "ExportProcurementReportToPdf";

                    Session["MainAction"] = "";
                    Session["Error"] = null;
                    Session["ids"] = null;
                    if (fromback != true)
                    {
                        Session["TempReportTable"] = null;
                        Session["TempReportViewTable"] = null;
                        Session["TempReportModel"] = null;
                        Session["TempFilters"] = null;

                    }

                    //if (Session["TempReportModel"] != null)
                    //{
                    //  //  model = (PriceDeskReportModel)Session["TempReportModel"];
                    //}
                    //model.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).Select(s => new Suppliers { SupplierName = s.SFAName ?? "-", SupplierId = s.SFAID }).ToList();
                    //model.Suppliers.Insert(0, new Suppliers { SupplierId = 0, SupplierName = "--Select--" });


                //    return View(model);

                //}
                //else
                //{
                //    var model = (PriceDeskReportModel)Session["TempReportModel"];

                    return View(model);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult RedirectToParent(string parentRoute, int parentRouteId)
        {
            //var parentRoute = parentRoute;//Session["parentRoute"].ToString();
            //var routeId = routeId// Session["parentRouteId"].ToString();
            Session["TempFilters"] = null;
            Session["TempReportModel"] = null;
            Session["Error"] = null;
            Session["ids"] = null;

            Response.Redirect("~/" + parentRoute + "/" + parentRouteId);
            return null;
        }

        [Route("GetReportData")]
        public ActionResult GetReportData(PriceDeskReportModel model)
        {
            try
            {
                var buttonName = Request.Form["submit"];
                if (buttonName == "Reset")
                {
                    Session["TempReportTable"] = null;
                    Session["TempReportViewTable"] = null;
                    Session["TempReportModel"] = null;
                    Session["TempFilters"] = null;
                    ViewBag.Message = null;
                    Session["Error"] = null;
                    Session["ids"] = null;
                    return RedirectToAction("PriceDeskReport", new { id = Convert.ToInt32(Session["routeId"]) });
                }

                string data = "";
                DateTime FromDate = DateTime.Now;
                DateTime ToDate = DateTime.Now;

                if (model.StartDate != null)
                {
                    FromDate = DateTime.Parse(model.StartDate).Date;
                }
                if (model.EndDate != null)
                {
                    ToDate = DateTime.Parse(model.EndDate).Date;
                }

                if (model.StartDate != null && model.EndDate != null)
                {
                    if (FromDate > ToDate)
                    {
                        ViewBag.Message = "Start date should not be greater than end date";
                    }
                }

                if (ViewBag.Message != "" && ViewBag.Message != null)
                {
                    return Json(new { data = "", msg = ViewBag.Message });
                }
                else
                {
                    var dt = pricedeskReportsPresenter.GetPriceDeskReportData(model.reportName, model);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ViewBag.Message = null;
                        Session["TempReportTable"] = dt;
                        DataView view = new DataView(dt);
                        dt.Columns.Add("No. Of Quotes");

                        DataTable distinctValues = new DataTable();

                        if (model.reportName == "Quotes By Supplier Report")
                        {
                            distinctValues = view.ToTable(true, "Supplier", "AccountManagerName", "BusinessTypes", "No. Of Quotes");
                            distinctValues.Columns.Add("Action");

                            foreach (DataRow row in distinctValues.Rows)
                            {
                                var Quotes = (from r in dt.AsEnumerable()
                                              where r.Field<object>("Supplier").ToString() == row["Supplier"].ToString() && r.Field<object>("AccountManagerName").ToString() == row["AccountManagerName"].ToString() && r.Field<object>("BusinessTypes").ToString() == row["BusinessTypes"].ToString()
                                              let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("QuoteId").ToString(),r.Field<object>("Supplier").ToString()
                                                  }
                                              group objectArray by objectArray[1] into g
                                              select new { Supplier = g.Key, Quotes = g.Select(x => new { count = Convert.ToInt32(x[0].ToString()) }).Count(), QuoteId = g.Select(x => x[0].ToString()).ToList() }).Distinct();

                                var QuoteIds = string.Join(",", Quotes.FirstOrDefault().QuoteId);

                                row["No. Of Quotes"] = Quotes.FirstOrDefault().Quotes;


                                foreach (DataRow dr in distinctValues.Rows)
                                {
                                    var obj = new
                                    {
                                        ids = QuoteIds.ToString()
                                    };
                                    row["Action"] = "<a id='DetailBtn' onclick='GetQuoteDetailsBySupplier(this)' endDate='" + model.EndDate + "' startDate='" + model.StartDate + "' supplierId='" + model.SupplierId + "'  ids='" + QuoteIds + "' quoteTypeId='" + model.QuoteTypeId + "' status='" + model.Status + "' style='color:#3b9ad2;'> Details </a>";
                                    //  row["Action"] = "<a id='DetailBtn' onclick='GetQuoteDetailsBySupplier(this)' ids='" + QuoteIds + "' quoteTypeId='" + model.QuoteTypeId + "' status='" + model.Status + "' style='color:#3b9ad2;'> Details </a>";
                                }
                            }
                        }

                        if (model.reportName == "Quotes By Status Report")
                        {
                            distinctValues = view.ToTable(true, "Quote #", "Quote Reference", "Account Manager", "Payment Terms",
                                "Details", "Special Instructions", "Delivery Charges",
                                "Currency", "Status", "Last Updated", "Updated By User", "Pending Start", "Pending End");

                        }

                        if (model.reportName == "Quotes Count By Account Manager")
                        {
                            distinctValues = view.ToTable(true, "Account Manager Name", "Account Manager Type", "Hardware Quotes", "Software Quotes",
                                "Asset Disposal Quotes", "IT Service Quotes");

                        }

                        var dtexport = distinctValues.Copy();
                        if (dtexport.Columns.Contains("Action") == true)
                        {
                            dtexport.Columns.Remove("Action");
                        }
                        Session["TempReportViewTable"] = dtexport;
                        data = JsonConvert.SerializeObject(distinctValues);

                        Session["TempFilters"] = true;
                    }
                    else
                    {
                        ViewBag.Message = "NoRecordsFound";
                        Session["TempReportTable"] = null;
                        Session["TempReportViewTable"] = null;
                        Session["TempReportModel"] = null;
                        return Json(new { data = data, msg = ViewBag.Message });
                    }

                    Session["TempReportModel"] = model;
                }

                return Json(new { data = data, msg = ViewBag.Message });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        public DataTable GetPriceDeskDatatable(string reportName, PriceDeskReportModel model)
        {

            var dt = pricedeskReportsPresenter.GetPriceDeskReportData(reportName, model);

            if (dt != null && dt.Rows.Count > 0)
            {
                ViewBag.Message = null;
                Session["TempReportTable"] = dt;
                DataView view = new DataView(dt);
                dt.Columns.Add("No. Of Quotes");

                DataTable distinctValues = new DataTable();

                if (reportName == "Quotes By Supplier Report")
                {
                    distinctValues = view.ToTable(true, "Supplier", "AccountManagerName", "BusinessTypes", "No. Of Quotes");
                    distinctValues.Columns.Add("Action");

                    foreach (DataRow row in distinctValues.Rows)
                    {
                        var Quotes = (from r in dt.AsEnumerable()
                                      where r.Field<object>("Supplier").ToString() == row["Supplier"].ToString() && r.Field<object>("AccountManagerName").ToString() == row["AccountManagerName"].ToString() && r.Field<object>("BusinessTypes").ToString() == row["BusinessTypes"].ToString()
                                      let objectArray = new List<object>
                                                  {
                                                    r.Field<object>("QuoteId").ToString(),r.Field<object>("Supplier").ToString()
                                                  }
                                      group objectArray by objectArray[1] into g
                                      select new { Supplier = g.Key, Quotes = g.Select(x => new { count = Convert.ToInt32(x[0].ToString()) }).Count(), QuoteId = g.Select(x => x[0].ToString()).ToList() }).Distinct();

                        var QuoteIds = string.Join(",", Quotes.FirstOrDefault().QuoteId);

                        row["No. Of Quotes"] = Quotes.FirstOrDefault().Quotes;


                        foreach (DataRow dr in distinctValues.Rows)
                        {
                            var obj = new
                            {
                                ids = QuoteIds.ToString()
                            };
                            row["Action"] = "<a id='DetailBtn' onclick='GetQuoteDetailsBySupplier(this)' endDate='" + model.EndDate + "' startDate='" + model.StartDate + "' supplierId='" + model.SupplierId+"'  ids='" + QuoteIds + "' quoteTypeId='" + model.QuoteTypeId + "' status='" + model.Status + "' style='color:#3b9ad2;'> Details </a>";
                        }
                    }
                }

                if (reportName == "Quotes By Status Report")
                {
                    distinctValues = view.ToTable(true, "Quote #", "Quote Reference", "Account Manager", "Payment Terms",
                        "Details", "Special Instructions", "Delivery Charges",
                        "Currency", "Status", "Last Updated", "Updated By User", "Pending Start", "Pending End");

                }

                if (reportName == "Quotes Count By Account Manager")
                {
                    distinctValues = view.ToTable(true, "Account Manager Name", "Account Manager Type", "Hardware Quotes", "Software Quotes",
                        "Asset Disposal Quotes", "IT Service Quotes");

                }

                var dtexport = distinctValues.Copy();
                if (dtexport.Columns.Contains("Action") == true)
                {
                    dtexport.Columns.Remove("Action");
                }

               

                return distinctValues;
            }
            return null;
        }

        public ActionResult GoToDetails(string ids, string quoteTypeId, string status, int routeId,int supplierId, string startDate , string endDate)
        {
            try
            {
                Session["ids"] = ids;
                TempData["quoteTypeId"] = Convert.ToInt32(quoteTypeId);
                TempData["status"] = status;
                var model = GetPriceDeskModel(routeId);//new PriceDeskReportModel();
                if (ViewBag.Message == "NoDetailsToExport")
                {
                    return View("Details", model);
                }
                return RedirectToAction("Details" , new { @ids = ids  , @quoteTypeId  = quoteTypeId , @status = status, @routeId = routeId , @supplierId = supplierId, @startDate = startDate , @endDate  = endDate});
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Go to Detail page with supplier id
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public ActionResult Details(string ids, string quoteTypeId , string status, int routeId, int supplierId, string startDate, string endDate)
        {
            try
            {
                PriceDeskReportModel model = GetPriceDeskModel(routeId);
                if (ids != null)
                {
                    model.Quoteids = ids;
                    model.QuoteTypeId = Convert.ToInt32(quoteTypeId);// Convert.ToInt32(TempData["quoteTypeId"].ToString());
                    model.Status = status; //TempData["status"].ToString();
                    model.SupplierId = supplierId;
                    model.StartDate = startDate;
                    model.EndDate = endDate;
                }
                else
                {
                    return RedirectToAction("PriceDeskReport", new { id = routeId });
                }

                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("SupplierQuotes")]
        public JsonResult GetSupplierQuotes(PriceDeskReportModel model)
        {
            try
            {
                var idArray = model.Quoteids.Split(',');
                var idsList = new List<int>();
                foreach (var item in idArray)
                {
                    var id = Convert.ToInt32(item);
                    idsList.Add(id);
                }

                var dt = PriceDeskReportsPresenter.ToDataTable<QuotesBySuppiler>(pricedeskReportsPresenter.GetSupplierQuotes(idsList, model.QuoteTypeId, model.Status));
                Session["QuotesTable"] = dt;
                var data = JsonConvert.SerializeObject(dt);

                return new JsonResult() { Data = data, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return new JsonResult() { Data = "", MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        public JsonResult GetQuoteResponseItems(PriceDeskReportModel model)
        {
            try
            {
                var dt = PriceDeskReportsPresenter.ToDataTable<QuoteResponseItems>(pricedeskReportsPresenter.GetQuoteResponseLineItems(model.Quoteids, model.QuoteTypeId));

                var data = JsonConvert.SerializeObject(dt);

                return new JsonResult() { Data = data, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            }
            catch (Exception ex)
            {
                var routedata = this.ControllerContext.RouteData;
                LogException(ex.Message, routedata);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routedata);
                }
                return new JsonResult() { Data = "", MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        public ActionResult ExportPriceDeskReportToExcel(FormCollection col)
        {
            try
            {
                var model = new PriceDeskReportModel();
                string filename = Convert.ToString(col["reportName"]);// Session["reportName"].ToString().Trim();
                string reportName = Convert.ToString(col["reportName"]);// Session["reportName"].ToString();
                var quotetypeId = Convert.ToInt32(col["quotetypeId"]);// Added by Adnan on 11-7-2019 for Quote Report
                var startDate = Convert.ToString(col["dateStart"]);
                var endDate = Convert.ToString(col["dateEnd"]);

                var supplierId = Convert.ToInt32(col["supplierId"]);
                var quoteStatusId = Convert.ToString(col["quoteStatusId"]);
                var accountManagerId = Convert.ToInt32(col["accountManagerId"]);
              

                filename = filename.Replace(" ", "");
                filename = filename.Replace(",", "");
                if (reportName == "Quote Report")// Added by Adnan on 11-7-2019 for Quote Report
                {
                    var dt = new DataTable();

                    switch (quotetypeId)
                    {
                        case 1:
                            dt = GetPriceDeskDataIntoDatatable(1);
                            break;
                        case 2:
                            dt = GetPriceDeskDataIntoDatatable(2);
                            break;
                        case 3:
                            dt = GetPriceDeskDataIntoDatatable(3);
                            break;
                        case 4:
                            dt = GetPriceDeskDataIntoDatatable(4);
                            break;
                        default:
                            break;
                    }
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ReportHelper.ExportToExcel(dt, reportName, Response);
                    }
                    else
                    {
                        ViewBag.Message = "NoData";
                    }

                }
                else if (reportName == "JDE Traxx Orders")
                {
                    var dt = GetJDETraxxOrdersReport(startDate,endDate);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ReportHelper.ExportToExcel(dt, reportName, Response);
                    }
                    else
                    {
                        ViewBag.Message = "NoData";
                    }
                }
                else
                {
                    var temp = new PriceDeskReportModel();
                    temp.reportName = reportName;
                    temp.StartDate = startDate;
                    temp.EndDate = endDate;
                    temp.SupplierId = supplierId;
                    temp.Status = quoteStatusId;
                    temp.QuoteTypeId = quotetypeId;
                    temp.AccountManagerId = accountManagerId;
                    //if (temp != null)
                    //{
                    //    model = temp;
                    //}
                    //if (Session["TempReportViewTable"] != null)
                    //{
                    var dt = GetPriceDeskDatatable(reportName, temp);//(DataTable)Session["TempReportViewTable"];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ReportHelper.ExportToExcel(dt, filename, Response);
                    }
                    //}
                    else
                    {
                        ViewBag.Message = "NoData";
                    }
                }

                model.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).Select(s => new Suppliers { SupplierName = s.SFAName ?? "-", SupplierId = s.SFAID }).ToList();
                model.Suppliers.Insert(0, new Suppliers { SupplierId = 0, SupplierName = "--Select--" });
                return View("PriceDeskReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public ActionResult ExportQuotesWithDetail(PriceDeskReportModel model)
        {
            try
            {
                //var temp = (PriceDeskReportModel)Session["TempReportModel"];
                //if (temp != null)
                //{
                //    model = temp;
                //}
                if (model != null)
                {


                    //    var dt = GetPriceDeskDatatable(model.reportName, model);
                    var idArray = model.Quoteids.Split(',');
                    var idsList = new List<int>();
                    foreach (var item in idArray)
                    {
                        var id = Convert.ToInt32(item);
                        idsList.Add(id);
                    }

                    var dt = PriceDeskReportsPresenter.ToDataTable<QuotesBySuppiler>(pricedeskReportsPresenter.GetSupplierQuotes(idsList, model.QuoteTypeId, model.Status));
                 
                    var dt1 = pricedeskReportsPresenter.GetSupplierQuoteDetailsToExport(dt, model.QuoteTypeId); ; //pricedeskReportsPresenter.GetSupplierQuoteDetailsToExport((DataTable)Session["QuotesTable"], model.QuoteTypeId); 
                    if (dt1.Rows.Count > 0)
                    {
                        ReportHelper.ExportToExcel(dt1, "QuoteResponseItems", Response);
                    }
                    else
                    {
                        Session["Error"] = "NoDetailsToExport";
                        return RedirectToAction("GoToDetails", new { ids = model.Quoteids, quoteTypeId = model.QuoteTypeId, status = model.Status ,supplierId = model.SupplierId, startDate = model.StartDate, endDate = model.StartDate });
                    }
                }
                else
                {
                    Session["Error"] = "NoDetailsToExport";
                    return RedirectToAction("GoToDetails", new { ids = model.Quoteids, quoteTypeId = model.QuoteTypeId, status = model.Status, supplierId = model.SupplierId, startDate = model.StartDate, endDate = model.StartDate });
                }
                model.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).Select(s => new Suppliers { SupplierName = s.SFAName ?? "-", SupplierId = s.SFAID }).ToList();
                model.Suppliers.Insert(0, new Suppliers { SupplierId = 0, SupplierName = "--Select--" });
                return View("PriceDeskReport", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        #endregion

        #region Export Price Desk Report To Datatable By Adnan Nooruddin on 11-july-2019
        public DataTable GetPriceDeskDataIntoDatatable(int quoteType)// Added by Adnan on 11-7-2019 for Quote Report
        {
            try
            {

                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_QuoteRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@QuoteType", SqlDbType.Int));

                    command.Parameters[0].Value = quoteType;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetPriceDeskReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }

        #endregion

        #region JDE Traxx Orders To Datatable By Adnan Nooruddin on 25-july-2019
        public DataTable GetJDETraxxOrdersReport(string startDate,string endDate)
        {
            try
            {

                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_JDETraxxOrdersRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.Text));

                    command.Parameters[0].Value = startDate;
                    command.Parameters[1].Value = endDate;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetJDETraxxOrdersReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }
        }
        #endregion
    }
}
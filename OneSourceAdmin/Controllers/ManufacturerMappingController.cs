﻿using OneSourceAdminCore;
using OneSourceAdminCore.Helper;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 31 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing manufacturer mapping detail between one source and one street"
    /// </summary>
    public class ManufacturerMappingController : BaseController
    {
        /// <summary>
        /// This Action is responsible for returning Index View of manufacturer mapping
        /// </summary>
        /// <returns></returns>
       
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var mp = new MappingPresenter();
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("OsManufacturerMapping");
                    return View("Index", mp.GetOneStreetManufacturerMappingDetails(0, true));
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible for updating the mapping record between one source and one street
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateManufacturerMapping(string onesourceMan, string[] onestreetMan)
        {
            try
            {
                var message = "Please provide correct data";

                if (Session["UserID"] != null)
                {
                    if (!string.IsNullOrEmpty(onesourceMan))
                    {
                        var mp = new MappingPresenter();
                        int a = mp.UpdateManufacturerMapping(onesourceMan, onestreetMan, Convert.ToInt32(Session["UserID"]));
                        message = a > 0 ? "Manufacturers successfully updated !" : "No record was updated";
                    }
                }

                TempData["Message"] = message;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// This Action is responsible for deleting the mapping
        /// </summary>
        /// <param name="osrManId"></param>
        /// <param name="ostManId"></param>
        /// <returns></returns>
        public ActionResult DeleteMapping(int osrManId, int ostManId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (osrManId == 0 || ostManId == 0)
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }

                    var mp = new MappingPresenter();
                    int count = mp.DeleteManufacturerMapping(osrManId, ostManId);
                    if (count > 0) TempData["Message"] = "Mapping has been deleted successfully.";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This action is responsible for returning Json Resul of One Street categories and unmapped categories
        /// </summary>
        /// <returns></returns>
        public void ExportManufacturerMappingReportToExcel()
        {
            try
            {
                var mp = new MappingPresenter();

                var temp = mp.GetMapUnmapManufacturer().MappingDetail.Select(x => new
                {
                    OneSourceManufacturerName = x.OnesourceMan,
                    OneStreetManufacturerName = x.OnestreetMan
                }).ToList();

                if (temp != null)
                {
                    var gridview = new GridView();
                    gridview.DataSource = temp;
                    gridview.DataBind();

                    // Clear all the content from the current response
                    Response.ClearContent();
                    Response.Buffer = true;
                    // set the header
                    Response.AddHeader("content-disposition", "attachment;filename = One_Category_Mapped_Manufacturers.xls");

                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";

                    // create HtmlTextWriter object with StringWriter
                    using (var sw = new StringWriter())
                    {
                        using (var htw = new HtmlTextWriter(sw))
                        {
                            // render the GridView to the HtmlTextWriter
                            gridview.RenderControl(htw);
                            // Output the GridView content saved into StringWriter
                            Response.Output.Write(sw.ToString());
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "NoData";
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;

                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }

        /// <summary>
        /// This action is responsible for returning Json Result of One Street unmapped categories
        /// </summary>
        /// <returns></returns>
        public void ExportToExcel()
        {
            try
            {
                var mp = new MappingPresenter();

                var unmappedOneStreetManufacturers = mp.GetMapUnmapManufacturer().UnmappedManufacturer
                    .Select(x => new
                    {
                        OneStreetManufacturerName = x.ManufacturerName
                    })
                    .ToList();

                if (unmappedOneStreetManufacturers != null)
                {
                    var gridview = new GridView();
                    gridview.DataSource = unmappedOneStreetManufacturers;
                    gridview.DataBind();

                    var fileName = string.Format("UnmappedOneStreetManufacturers_{0}_{1}.xls",
                        DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());

                    Response.DownloadExcel(gridview, fileName, "application/ms-excel");
                }
                else
                {
                    ViewBag.Message = "No data found";
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
            }
        }

        /// <summary>
        /// This action is responsible for the retrieval of unmapped onestreet manufacturers against one source manufacturers
        /// </summary>
        /// <param name="oneSourceManufacturerId"></param>
        /// <returns></returns>
        public JsonResult GetOneStreetManufacturerMappingDetails(int oneSourceManufacturerId)
        {
            try
            {
                if (Session["UserID"] != null)
                {
                    var mp = new MappingPresenter();
                    var data = mp.GetManufacturersMappingDetailByOSCategoryId(oneSourceManufacturerId);
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var routeData = ControllerContext.RouteData;
                LogException(ex.Message, routeData);

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }

                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
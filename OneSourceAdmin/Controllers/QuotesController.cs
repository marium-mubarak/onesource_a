﻿using iTextSharp.text.pdf;
using OneSourceAdmin.Helper;
using OneSourceAdmin.Models;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    [NoCache]
    public class QuotesController : BaseController
    {

        HQRequestPresenter _hardwareQuotes;
        SoftwareQuotePresenter _softwareQuotes;
        ITServiceQuotePresenter _itservicequotes;
        AssetDisposalQuote _assetdisposalquotes;
        SFAsPresenter sfas;
        QuoteRequestPresenter quoterequest;
        string location = ConfigurationManager.AppSettings["UploadURLQuotes"];
        string sitelocation = ConfigurationManager.AppSettings["sitelocation"];

        public QuotesController()
        {
            _hardwareQuotes = new HQRequestPresenter();
            _itservicequotes = new ITServiceQuotePresenter();
            _softwareQuotes = new SoftwareQuotePresenter();
            _assetdisposalquotes = new AssetDisposalQuote();
            sfas = new SFAsPresenter();
            quoterequest = new QuoteRequestPresenter();
        }

        #region Hardware Quotes

        //[Route("hardware-quote")]

        public ActionResult Index()
        {
            try
            {
                //HardwareQuoteModel hq = new HardwareQuoteModel();
                HardwareEntryModel hq = new HardwareEntryModel();
                hq.LineItems = new LineItemForm();
                var hardwarequote = new HardwareQuoteModel();
                hq.QuoteModel = hardwarequote;

                Session["listItem"] = null;
                return View(hq);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        //[Route("hardware-quote")]
        [HttpPost]
        public ActionResult SaveHardwareQuote(HardwareEntryModel qentry)
        {
            try
            {
                var hq = qentry.QuoteModel;

                var _hq = _hardwareQuotes.HardwareQuote;
                var filename1 = string.Empty;
                var filename2 = string.Empty;
                var filename3 = string.Empty;
                var filename4 = string.Empty;
                var filename5 = string.Empty;
                var optionid = string.Empty;

                optionid = Request["optionid"].ToString();

                if (Request.Files.AllKeys.Any())
                {
                    if (Request.Files.Count > 0)
                    {
                        // Get the uploaded image from the Files collection
                        var httpPostedFile1 = Request.Files["file1"];
                        var httpPostedFile2 = Request.Files["file2"];
                        var httpPostedFile3 = Request.Files["file3"];
                        var httpPostedFile4 = Request.Files["file4"];
                        var httpPostedFile5 = Request.Files["file5"];

                        if (httpPostedFile1.ContentLength > 0)
                        {
                            filename1 = httpPostedFile1.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename1);
                            httpPostedFile1.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile2.ContentLength > 0)
                        {
                            filename2 = httpPostedFile2.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename2);
                            httpPostedFile2.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile3.ContentLength > 0)
                        {
                            filename3 = httpPostedFile3.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename3);
                            httpPostedFile3.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile4.ContentLength > 0)
                        {
                            filename4 = httpPostedFile4.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename4);
                            httpPostedFile4.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile5.ContentLength > 0)
                        {
                            filename5 = httpPostedFile5.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename5);
                            httpPostedFile5.SaveAs(fileSavePath);
                        }
                    }
                }

                _hq.Attachment1 = filename1;
                _hq.Attachment2 = filename2;
                _hq.Attachment3 = filename3;
                _hq.Attachment4 = filename4;
                _hq.Attachment5 = filename5;

                _hq.City = hq.City;
                _hq.Comments = hq.Comments;
                _hq.CountryId = Convert.ToInt32(hq.CountryId);
                _hq.CreatedBy = adminSetting.UserID;
                _hq.CreatedDate = DateTime.Now;
                _hq.CurrencyId = Convert.ToInt32(hq.CurrencyId);
                _hq.CustomerDeliveryAddress = hq.DeliveryAddress;
                _hq.CustomerName = hq.CustomerName;
                _hq.Description = hq.Description;
                _hq.ExpectedPricePoint = hq.PricePoint;
                _hq.HQOptionComment = hq.OptionalComment;
                _hq.HQOptionId = Convert.ToInt32(optionid);
                _hq.Manufacturer = hq.Manufacturer;
                _hq.ProjectTimeLine = hq.Timeline;
                _hq.Quantity = Convert.ToInt32(hq.Qunaitity);
                _hq.SKU = hq.Sku;
                _hq.AccountManagerName = hq.AccountManagerName;
                _hq.SfaId = Convert.ToInt32(hq.SupplierID);
                _hq.GlobalDealId = hq.GlobalDealId;
                _hq.OEMAccountManager = hq.OEMAccountManager;
                _hq.EndUserAccountDetails = hq.EndUserAccountDetails;

                var i = _hardwareQuotes.Save();

                if (i > 0)
                {
                    var quoteid = _hardwareQuotes.Get().OrderByDescending(x => x.HardwareQuoteId).FirstOrDefault().HardwareQuoteId;
                    var quote = _hardwareQuotes.Get().FirstOrDefault(x => x.HardwareQuoteId == quoteid);

                    UserPresenter _user = new UserPresenter();
                    var userInfo = _user.GetContactInfo(adminSetting.UserID);
                    var userEmail = userInfo.Email;

                    var _lineitem = _hardwareQuotes.QuoteLineItems;

                    _lineitem.CratedDate = DateTime.Now;
                    _lineitem.CreatedBy = adminSetting.UserID;
                    _lineitem.Description = quote.Description;
                    _lineitem.Manufacturer = quote.Manufacturer;
                    _lineitem.Quantity = quote.Quantity;
                    _lineitem.QuoteId = quoteid;
                    _lineitem.QuoteTypeId = 1;
                    _lineitem.SKU = quote.SKU;
                    _hardwareQuotes.SaveLineItems();


                    if (Session["listItem"] != null)
                    {
                        var tempItems = (List<TempLineItem>)Session["listItem"];

                        if (tempItems.Count > 0)
                        {
                            //var _lineitem = _hardwareQuotes.QuoteLineItems;

                            //_lineitem.CratedDate = DateTime.Now;
                            //_lineitem.CreatedBy = adminSetting.UserID;
                            //_lineitem.Description = quote.Description;
                            //_lineitem.Manufacturer = quote.Manufacturer;
                            //_lineitem.Quantity = quote.Quantity;
                            //_lineitem.QuoteId = quoteid;
                            //_lineitem.QuoteTypeId = 1;
                            //_lineitem.SKU = quote.SKU;
                            //_hardwareQuotes.SaveLineItems();


                            foreach (var item in tempItems)
                            {
                                _lineitem.CratedDate = DateTime.Now;
                                _lineitem.CreatedBy = adminSetting.UserID;
                                _lineitem.Description = item.Description;
                                _lineitem.Manufacturer = item.Manufacturer;
                                _lineitem.Quantity = item.Qunantity;
                                _lineitem.QuoteId = quoteid;
                                _lineitem.QuoteTypeId = 1;
                                _lineitem.SKU = item.SKU;
                                _hardwareQuotes.SaveLineItems();
                            }
                        }

                        Session["listItem"] = null;
                    }

                    if (quote != null)
                    {
                        var message = string.Empty;

                        message = "Customer: " + quote.CustomerName + "<br>";
                        message += "Requested By: " + userInfo.FirstName + " " + userInfo.LastName + "<br>";
                        message += "<p>Thank you for placing the following quote request on GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.HardwareQuoteId.ToString() + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to view the status of the quote request.</p>";
                        message += "<p>Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";


                        #region Old Emails
                        //message = "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Customer Delivery Address: " + quote.CustomerDeliveryAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "Expected Price Point: " + quote.ExpectedPricePoint + "<br>";
                        //message += "Quantity: " + quote.Quantity.ToString() + "<br>";
                        //message += "Description: " + quote.Description + "<br>";
                        //message += "SKU: " + quote.SKU + "<br>";
                        //message += "Manufacturer: " + quote.Manufacturer + "<br>";
                        //message += "Lead Type: " + quote.HQOption + "<br>";
                        //message += "Global Deal Id: " + quote.GlobalDealId + "<br>";
                        //message += "OEM Account Manager Contact Details: " + quote.OEMAccountManager + "<br>";
                        //message += "End User Contact Details: " + quote.EndUserAccountDetails + "<br>";

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment1))
                        //{
                        //    message += "<hr><br>";
                        //    message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment2))
                        //{
                        //    message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment3))
                        //{
                        //    message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment4))
                        //{
                        //    message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment5))
                        //{
                        //    message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        //}                       
                        #endregion
                        var subject = "GlobalServe ONESOURCE - New Hardware Quote Request";
                        var toemail = "smbquoterequests@global-serve.com"; //smbquoterequest@global-serve.com

                        //if (string.IsNullOrWhiteSpace(quote.SupplierEmail))
                        //{
                        //    toemail = "it@global-serve.com";
                        //}
                        //else
                        //{
                        //    toemail = quote.SupplierEmail;
                        //}

                        SendEmail(toemail, message, subject);
                        SendEmail(userEmail, message, subject);
                    }
                }


                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }



        }

        //[Route("hardwares")]
        public ActionResult GetHardwareQuotes()
        {
            try
            {
                HardwareQuoteListModel hardquote = new HardwareQuoteListModel();
                SFAsPresenter _sfa = new SFAsPresenter();

                hardquote.HardwareQuotes = _hardwareQuotes.Get().OrderByDescending(x => x.HardwareQuoteId).ToList();

                if (adminSetting.UserGroupID == 4)
                {
                    hardquote.HardwareQuotes = _hardwareQuotes.Get().Where(x => x.CreatedBy == adminSetting.UserID).OrderByDescending(x => x.HardwareQuoteId).ToList();
                }


                hardquote.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).ToList();
                return View("HdardwareListView", hardquote);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("get-hardware-quote-byid")]
        public ActionResult GetHardwareQuoteById(int id)
        {
            try
            {
                var hard = _hardwareQuotes.Get().FirstOrDefault(x => x.HardwareQuoteId == id);
                if (hard.SfaId == 0)
                {
                    hard.Supplier = "Not Assigned";
                }
                else
                {
                    hard.Supplier = sfas.GetSFAs().FirstOrDefault(x => x.SFAID == hard.SfaId).SFAName;
                }
                return Json(hard, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("update-hardware-quote")]
        [HttpPost]
        public ActionResult UpdateHardwareQuote()
        {
            try
            {
                var id = Request["quoteid"].ToString();
                var supplierId = Request["supplierid"].ToString();
                //var quantity = Request["quantity"].ToString();
                //var description = Request["description"].ToString();
                //var sku = Request["sku"].ToString();

                var _hqoute = _hardwareQuotes.GetHarwareQuotes().FirstOrDefault(x => x.HardwareQuoteId == Convert.ToInt32(id));

                if (_hqoute != null)
                {
                    _hqoute.SfaId = Convert.ToInt32(supplierId);
                    //_hqoute.Quantity = Convert.ToInt32(quantity);
                    //_hqoute.Description = description;
                    //_hqoute.SKU = sku;
                }

                _hardwareQuotes.HardwareQuote = _hqoute;

                var i = _hardwareQuotes.Save();

                if (i > 0)
                {
                    var quote = _hardwareQuotes.Get().FirstOrDefault(x => x.HardwareQuoteId == Convert.ToInt32(id));
                    SFAsPresenter sfa = new SFAsPresenter();
                    var supplier = sfa.GetSFAs().FirstOrDefault(x => x.SFAID == quote.SfaId);

                    if (quote != null)
                    {
                        var message = string.Empty;

                        message = "GSP: " + supplier.SFAName + "<br>";
                        message += "Customer: " + quote.CustomerName + "<br>";

                        message += "<p>The following quote request comes from GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.HardwareQuoteId + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to respond to the quote request.</p>";
                        message += "<p>If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        //message += "<p>EMEA<br>Phone: +44 (0) 870 22 00515</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";


                        #region Old Email
                        //message = "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Customer Delivery Address: " + quote.CustomerDeliveryAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "Expected Price Point: " + quote.ExpectedPricePoint + "<br>";
                        //message += "Quantity: " + quote.Quantity.ToString() + "<br>";
                        //message += "Description: " + quote.Description + "<br>";
                        //message += "SKU: " + quote.SKU + "<br>";
                        //message += "Manufacturer: " + quote.Manufacturer + "<br>";
                        //message += "Lead Type: " + quote.HQOption + "<br>";
                        //message += "Global Deal Id: " + quote.GlobalDealId + "<br>";
                        //message += "OEM Account Manager Contact Details: " + quote.OEMAccountManager + "<br>";
                        //message += "End User Contact Details: " + quote.EndUserAccountDetails + "<br>";
                        //message += "<hr><br>";
                        //message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        #endregion

                        var subject = "GlobalServe ONESOURCE - New Hardware Quote Request - " + quote.HardwareQuoteId.ToString();
                        var toemail = ""; //                    

                        if (supplier != null)
                        {
                            if (string.IsNullOrWhiteSpace(supplier.Email))
                            {
                                toemail = "it@global-serve.com";
                            }
                            else
                            {
                                toemail = supplier.Email;
                            }
                        }
                        var emails = toemail.Split(';');

                        foreach (var item in emails)
                        {
                            SendEmail(item, message, subject);
                        }


                    }

                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }


        #endregion

        #region Software Quotes

        //[Route("software-quote")]
        public ActionResult SWQuoteRequest()
        {
            try
            {
                SoftwareEntryModel sentry = new SoftwareEntryModel();

                sentry.SoftwareQuote = new SoftwareQuoteModel();
                sentry.LineItems = new LineItemForm();
                Session["listItem"] = null;
                return View("SoftwareQuoteView", sentry);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult SaveSQQuotes(SoftwareEntryModel qentry)
        {
            try
            {
                var sw = qentry.SoftwareQuote;
                var _sq = _softwareQuotes.SoftwareQuote;
                var filename1 = string.Empty;
                var filename2 = string.Empty;
                var filename3 = string.Empty;
                var filename4 = string.Empty;
                var filename5 = string.Empty;
                var optionid = string.Empty;

                optionid = Request["optionid"].ToString();

                if (Request.Files.AllKeys.Any())
                {
                    if (Request.Files.Count > 0)
                    {
                        // Get the uploaded image from the Files collection
                        var httpPostedFile1 = Request.Files["file1"];
                        var httpPostedFile2 = Request.Files["file2"];
                        var httpPostedFile3 = Request.Files["file3"];
                        var httpPostedFile4 = Request.Files["file4"];
                        var httpPostedFile5 = Request.Files["file5"];

                        if (httpPostedFile1.ContentLength > 0)
                        {
                            filename1 = httpPostedFile1.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename1);
                            httpPostedFile1.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile2.ContentLength > 0)
                        {
                            filename2 = httpPostedFile2.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename2);
                            httpPostedFile2.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile3.ContentLength > 0)
                        {
                            filename3 = httpPostedFile3.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename3);
                            httpPostedFile3.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile4.ContentLength > 0)
                        {
                            filename4 = httpPostedFile4.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename4);
                            httpPostedFile4.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile5.ContentLength > 0)
                        {
                            filename5 = httpPostedFile5.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename5);
                            httpPostedFile5.SaveAs(fileSavePath);
                        }
                    }
                }

                _sq.Attachment1 = filename1;
                _sq.Attachment2 = filename2;
                _sq.Attachment3 = filename3;
                _sq.Attachment4 = filename4;
                _sq.Attachment5 = filename5;

                _sq.City = sw.City;
                _sq.Comments = sw.Comments;
                _sq.CountryId = Convert.ToInt32(sw.CountryId);
                _sq.CreatedBy = adminSetting.UserID;
                _sq.CreatedDate = DateTime.Now;
                _sq.CurrencyId = Convert.ToInt32(sw.CurrencyId);
                _sq.CustomerName = sw.CustomerName;
                _sq.DeliveryAddress = sw.DeliveryAddress;
                _sq.Description = sw.Description;
                _sq.ExpectedPricePoint = sw.PricePoint;
                _sq.InvoiceAddress = sw.InvoiceAddress;
                _sq.LisensePeriod = sw.LicensePeriod;
                _sq.LisenseTypeId = sw.LicenseType;
                _sq.Manufacturer = sw.Manufacturer;
                _sq.OptionId = Convert.ToInt32(sw.OptionId);
                _sq.OptionComment = sw.OptionalComment;
                _sq.ProjectTimeline = sw.Timeline;
                _sq.Quantity = Convert.ToInt32(sw.Qunaitity);
                _sq.SerialNumber = sw.SerialNumber;
                _sq.SKU = sw.Sku;
                _sq.SfaId = Convert.ToInt32(sw.SupplierID);
                _sq.AccountManagerName = sw.AccountManagerName;
                _sq.GlobalDealId = sw.GlobalDealId;
                _sq.OEMAccountManager = sw.OEMAccountManager;
                _sq.EndUserAccountDetails = sw.EndUserAccountDetails;

                var i = _softwareQuotes.Save();



                if (i > 0)
                {
                    var quoteid = _softwareQuotes.Get().OrderByDescending(x => x.SoftwareQuoteId).FirstOrDefault().SoftwareQuoteId;
                    var quote = _softwareQuotes.Get().FirstOrDefault(x => x.SoftwareQuoteId == quoteid);

                    UserPresenter _user = new UserPresenter();
                    var userInfo = _user.GetContactInfo(adminSetting.UserID);
                    var userEmail = userInfo.Email;

                    var _lineitem = _hardwareQuotes.QuoteLineItems;

                    _lineitem.CratedDate = DateTime.Now;
                    _lineitem.CreatedBy = adminSetting.UserID;
                    _lineitem.Description = quote.Description;
                    _lineitem.Manufacturer = quote.Manufacturer;
                    _lineitem.Quantity = quote.Quantity;
                    _lineitem.QuoteId = quoteid;
                    _lineitem.QuoteTypeId = 2;
                    _lineitem.SKU = quote.SKU;
                    _hardwareQuotes.SaveLineItems();


                    if (Session["listItem"] != null)
                    {
                        var tempItems = (List<TempLineItem>)Session["listItem"];

                        if (tempItems.Count > 0)
                        {
                            //var _lineitem = _hardwareQuotes.QuoteLineItems;

                            //_lineitem.CratedDate = DateTime.Now;
                            //_lineitem.CreatedBy = adminSetting.UserID;
                            //_lineitem.Description = quote.Description;
                            //_lineitem.Manufacturer = quote.Manufacturer;
                            //_lineitem.Quantity = quote.Quantity;
                            //_lineitem.QuoteId = quoteid;
                            //_lineitem.QuoteTypeId = 2;
                            //_lineitem.SKU = quote.SKU;
                            //_lineitem.SerailNumber = quote.SerialNumber;
                            //_hardwareQuotes.SaveLineItems();


                            foreach (var item in tempItems)
                            {
                                _lineitem.CratedDate = DateTime.Now;
                                _lineitem.CreatedBy = adminSetting.UserID;
                                _lineitem.Description = item.Description;
                                _lineitem.Manufacturer = item.Manufacturer;
                                _lineitem.Quantity = item.Qunantity;
                                _lineitem.QuoteId = quoteid;
                                _lineitem.SerailNumber = quote.SerialNumber;
                                _lineitem.QuoteTypeId = 2;
                                _lineitem.SKU = item.SKU;
                                _hardwareQuotes.SaveLineItems();
                            }
                        }

                        Session["listItem"] = null;
                    }


                    if (quote != null)
                    {
                        var message = "";

                        message = "Customer: " + quote.CustomerName + "<br>";
                        message += "Requested By: " + userInfo.FirstName + " " + userInfo.LastName + "<br>";
                        message += "<p>Thank you for placing the following quote request on GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.SoftwareQuoteId.ToString() + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to view the status of the quote request.</p>";
                        message += "<p>Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";

                        #region Old Emails
                        //message = "License Type: " + quote.LisenseTypeId + "<br>";
                        //message += "License Period: " + quote.LisensePeriod + "<br>";
                        //message += "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Customer Delivery Address: " + quote.DeliveryAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "Expected Price Point: " + quote.ExpectedPricePoint + "<br>";
                        //message += "Quantity: " + quote.Quantity + "<br>";
                        //message += "Description: " + quote.Description + "<br>";
                        //message += "SKU: " + quote.SKU + "<br>";
                        //message += "Serial #: " + quote.SerialNumber + "<br>";
                        //message += "Manufacturer: " + quote.Manufacturer + "<br>";
                        //message += "Lead Type: " + quote.HQOption + "<br>";
                        //message += "Global Deal Id: " + quote.GlobalDealId + "<br>";
                        //message += "OEM Account Manager Contact Details: " + quote.OEMAccountManager + "<br>";
                        //message += "End User Contact Details: " + quote.EndUserAccountDetails + "<br>";
                        //message += "Comments: " + quote.Comments + "<br>";

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment1))
                        //{
                        //    message += "<hr><br>";
                        //    message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment2))
                        //{
                        //    message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment3))
                        //{
                        //    message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment4))
                        //{
                        //    message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment5))
                        //{
                        //    message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        //}
                        #endregion


                        var subject = "GlobalServe ONESOURCE - New Software Quote Request";
                        var toemail = "smbquoterequests@global-serve.com";
                        //if (string.IsNullOrWhiteSpace(quote.SupplierEmail))
                        //{
                        //    toemail = "it@global-serve.com";
                        //}
                        //else
                        //{
                        //    toemail = quote.SupplierEmail;
                        //}

                        SendEmail(toemail, message, subject);
                        SendEmail(userEmail, message, subject);
                    }
                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        //[Route("softwares")]
        public ActionResult Softwares()
        {
            try
            {
                SoftwareQuoteList sqlst = new SoftwareQuoteList();

                sqlst.SoftwareQList = _softwareQuotes.Get().OrderByDescending(x => x.SoftwareQuoteId).ToList();

                if (adminSetting.UserGroupID == 4)
                {
                    sqlst.SoftwareQList = _softwareQuotes.Get().Where(x => x.CreatedBy == adminSetting.UserID).OrderByDescending(x => x.SoftwareQuoteId).ToList();
                }

                SFAsPresenter _sfa = new SFAsPresenter();
                sqlst.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).ToList();

                return View("SoftwareListView", sqlst);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("get-software-quote-byid")]
        public ActionResult GetSoftwareQuoteById(int id)
        {
            try
            {
                var soft = _softwareQuotes.Get().FirstOrDefault(x => x.SoftwareQuoteId == id);
                if (soft.SfaId == 0)
                {
                    soft.Supplier = "Not Assigned";
                }
                else
                {
                    soft.Supplier = sfas.GetSFAs().FirstOrDefault(x => x.SFAID == soft.SfaId).SFAName;
                }
                return Json(soft, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("update-software-quote")]
        [HttpPost]
        public ActionResult UpdateSoftwareQuote()
        {
            try
            {
                var id = Request["quoteid"].ToString();
                var supplierId = Request["supplierid"].ToString();
                //var quantity = Request["quantity"].ToString();
                //var description = Request["description"].ToString();
                //var serailno = Request["serailno"].ToString();
                //var sku = Request["sku"].ToString();

                var _sqoute = _softwareQuotes.GetSoftwareQuotes().FirstOrDefault(x => x.SoftwareQuoteId == Convert.ToInt32(id));

                if (_sqoute != null)
                {
                    _sqoute.SfaId = Convert.ToInt32(supplierId);
                    //_sqoute.Quantity = Convert.ToInt32(quantity);
                    //_sqoute.Description = description;
                    //_sqoute.SerialNumber = serailno;
                    //_sqoute.SKU = sku;
                }

                _softwareQuotes.SoftwareQuote = _sqoute;
                var i = _softwareQuotes.Save();

                if (i > 0)
                {
                    var quote = _softwareQuotes.Get().FirstOrDefault(x => x.SoftwareQuoteId == Convert.ToInt32(id));

                    SFAsPresenter sfa = new SFAsPresenter();
                    var supplier = sfa.GetSFAs().FirstOrDefault(x => x.SFAID == quote.SfaId);

                    if (quote != null)
                    {
                        var message = "";

                        message = "GSP: " + supplier.SFAName + "<br>";
                        message += "Customer: " + quote.CustomerName + "<br>";

                        message += "<p>The following quote request comes from GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.SoftwareQuoteId + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to respond to the quote request.</p>";
                        message += "<p>If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        //message += "<p>EMEA<br>Phone: +44 (0) 870 22 00515</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";

                        #region Olde Email

                        //message = "License Type: " + quote.LisenseTypeId + "<br>";
                        //message += "License Period: " + quote.LisensePeriod + "<br>";
                        //message += "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Customer Delivery Address: " + quote.DeliveryAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "Expected Price Point: " + quote.ExpectedPricePoint + "<br>";
                        //message += "Quantity: " + quote.Quantity + "<br>";
                        //message += "Description: " + quote.Description + "<br>";
                        //message += "SKU: " + quote.SKU + "<br>";
                        //message += "Serial #: " + quote.SerialNumber + "<br>";
                        //message += "Manufacturer: " + quote.Manufacturer + "<br>";
                        //message += "Lead Type: " + quote.HQOption + "<br>";
                        //message += "Global Deal Id: " + quote.GlobalDealId + "<br>";
                        //message += "OEM Account Manager Contact Details: " + quote.OEMAccountManager + "<br>";
                        //message += "End User Contact Details: " + quote.EndUserAccountDetails + "<br>";
                        //message += "Comments: " + quote.Comments + "<br>";
                        //message += "<hr><br>";
                        //message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        #endregion
                        var subject = "GlobalServe ONESOURCE - New Software Quote Request - " + quote.SoftwareQuoteId.ToString();
                        var toemail = "";



                        if (supplier != null)
                        {
                            if (string.IsNullOrWhiteSpace(supplier.Email))
                            {
                                toemail = "it@global-serve.com";
                            }
                            else
                            {
                                toemail = supplier.Email;
                            }
                        }

                        var emails = toemail.Split(';');

                        foreach (var item in emails)
                        {
                            SendEmail(item, message, subject);
                        }

                        //SendEmail(toemail, message, subject);
                    }
                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }


        #endregion

        #region IT Service Quotes

        //[Route("it-service-quote")]
        public ActionResult ITServiceQuote()
        {
            try
            {
                ITServicesEnteryModel itsm = new ITServicesEnteryModel();

                itsm.LineItems = new ITServicesLineItemModel();
                itsm.ServiceModel = new ITServiceModel();
                Session["listItem"] = null;
                return View("ITServiceQuoteView", itsm);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult SaveITQuote(ITServicesEnteryModel itsrevice)
        {
            try
            {
                var iq = itsrevice.ServiceModel;
                var _iq = _itservicequotes.ServiceQuote;
                var filename1 = string.Empty;
                var filename2 = string.Empty;
                var filename3 = string.Empty;
                var filename4 = string.Empty;
                var filename5 = string.Empty;

                if (Request.Files.AllKeys.Any())
                {
                    if (Request.Files.Count > 0)
                    {
                        // Get the uploaded image from the Files collection
                        var httpPostedFile1 = Request.Files["file1"];
                        var httpPostedFile2 = Request.Files["file2"];
                        var httpPostedFile3 = Request.Files["file3"];
                        var httpPostedFile4 = Request.Files["file4"];
                        var httpPostedFile5 = Request.Files["file5"];

                        if (httpPostedFile1.ContentLength > 0)
                        {
                            filename1 = httpPostedFile1.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename1);
                            httpPostedFile1.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile2.ContentLength > 0)
                        {
                            filename2 = httpPostedFile2.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename2);
                            httpPostedFile2.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile3.ContentLength > 0)
                        {
                            filename3 = httpPostedFile3.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename3);
                            httpPostedFile3.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile4.ContentLength > 0)
                        {
                            filename4 = httpPostedFile4.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename4);
                            httpPostedFile4.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile5.ContentLength > 0)
                        {
                            filename5 = httpPostedFile5.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename5);
                            httpPostedFile5.SaveAs(fileSavePath);
                        }
                    }
                }

                _iq.Attachment1 = filename1;
                _iq.Attachment2 = filename2;
                _iq.Attachment3 = filename3;
                _iq.Attachment4 = filename4;
                _iq.Attachment5 = filename5;

                _iq.City = iq.City;
                _iq.Comments = iq.Comments;
                _iq.CountryId = Convert.ToInt32(iq.CountryId);
                _iq.CreatedBy = adminSetting.UserID;
                _iq.CreatedDate = DateTime.Now;
                _iq.CurrencyId = Convert.ToInt32(iq.CurrencyId);
                _iq.CustomerName = iq.CustomerName;
                _iq.EngineerAvailabilityId = Convert.ToInt32(iq.EngineerAvailability);
                _iq.EquipmentDetails = iq.EquipmentDetails;
                _iq.InvoiceAddress = iq.InvoiceAddress;
                _iq.InvoiceContact = iq.InvoiceContact;
                _iq.NoofResources = Convert.ToInt32(iq.NumberofResources);
                _iq.ProjectExpectedDuration = iq.ExpectedDuration;
                _iq.ProjectName = iq.ProjectName;
                _iq.ProjectOverView = iq.Overview;
                _iq.ProjectStartDate = iq.ProjectStartDate;
                _iq.RatesId = Convert.ToInt32(iq.Rates);
                _iq.RatesPoint = iq.ExpectedRatePoint;
                _iq.ScopeofWork = iq.ScopeofWork;
                _iq.ServiceTypeId = Convert.ToInt32(iq.ServiceTypeId);
                _iq.SiteAddress = iq.SiteAddres;
                _iq.SiteContact = iq.SiteContact;
                _iq.SLAId = Convert.ToInt32(iq.SALId);
                _iq.SfaId = Convert.ToInt32(iq.SupplierID);
                _iq.AccountManagerName = iq.AccountManagerName;

                var i = _itservicequotes.Save();

                if (i > 0)
                {
                    var quoteid = _itservicequotes.Get().OrderByDescending(x => x.ISQuoteId).FirstOrDefault().ISQuoteId;
                    var quote = _itservicequotes.Get().FirstOrDefault(x => x.ISQuoteId == quoteid);

                    UserPresenter _user = new UserPresenter();
                    var userInfo = _user.GetContactInfo(adminSetting.UserID);
                    var userEmail = userInfo.Email;

                    var _lineitem = _hardwareQuotes.QuoteLineItems;

                    _lineitem.CratedDate = DateTime.Now;
                    _lineitem.CreatedBy = adminSetting.UserID;
                    _lineitem.SLATypeId = quote.SLAId;
                    _lineitem.ServiceTypeId = quote.ServiceTypeId;
                    _lineitem.Resources = quote.NoofResources;
                    _lineitem.EngineerAvailability = quote.EngineerAvailabilityId;
                    _lineitem.QuoteId = quoteid;
                    _lineitem.QuoteTypeId = 4;
                    _lineitem.Description = quote.ScopeofWork;
                    _hardwareQuotes.SaveLineItems();


                    if (Session["listItem"] != null)
                    {
                        var tempItems = (List<TempLineItem>)Session["listItem"];

                        if (tempItems.Count > 0)
                        {
                            //var _lineitem = _hardwareQuotes.QuoteLineItems;

                            //_lineitem.CratedDate = DateTime.Now;
                            //_lineitem.CreatedBy = adminSetting.UserID;
                            //_lineitem.SLATypeId = quote.SLAId;
                            //_lineitem.ServiceTypeId = quote.ServiceTypeId;
                            //_lineitem.Resources = quote.NoofResources;
                            //_lineitem.EngineerAvailability = quote.EngineerAvailabilityId;
                            //_lineitem.QuoteId = quoteid;
                            //_lineitem.QuoteTypeId = 4;
                            //_lineitem.Description = quote.ScopeofWork;                            
                            //_hardwareQuotes.SaveLineItems();


                            foreach (var item in tempItems)
                            {
                                _lineitem.CratedDate = DateTime.Now;
                                _lineitem.CreatedBy = adminSetting.UserID;
                                _lineitem.SLATypeId = item.SLAId;
                                _lineitem.ServiceTypeId = item.ServiceId;
                                _lineitem.Resources = item.Resources;
                                _lineitem.EngineerAvailability = item.EngAvId;
                                _lineitem.QuoteId = quoteid;
                                _lineitem.QuoteTypeId = 4;
                                _lineitem.Description = item.Description;
                                _hardwareQuotes.SaveLineItems();
                            }
                        }

                        Session["listItem"] = null;
                    }


                    if (quote != null)
                    {
                        var message = "";

                        message = "Customer: " + quote.CustomerName + "<br>";
                        message += "Requested By: " + userInfo.FirstName + " " + userInfo.LastName + "<br>";
                        message += "<p>Thank you for placing the following quote request on GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.ISQuoteId.ToString() + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to view the status of the quote request.</p>";
                        message += "<p>Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";

                        #region Old Email
                        //message = "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Supplier: " + quote.Supplier + "<br>";
                        //message += "Customer Site Address: " + quote.SiteAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "No of Resources: " + quote.NoofResources + "<br>";
                        //message += "Project Overview: " + quote.ProjectOverView + "<br>";
                        //message += "Detailed Scope of Work: " + quote.ScopeofWork + "<br>";
                        //message += "Equipment Details: " + quote.EquipmentDetails + "<br>";
                        //message += "SLA: " + quote.SLAType + "<br>";
                        //message += "Engineer Availability: " + quote.EngineerAvailabilityId.ToString() == "1" ? "Dispatch Onsite" : "FTE (Full time Engineer)" + "<br>";
                        //message += "Comment: " + quote.Comments + "<br>";

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment1))
                        //{
                        //    message += "<hr><br>";
                        //    message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment2))
                        //{
                        //    message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment3))
                        //{
                        //    message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment4))
                        //{
                        //    message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment5))
                        //{
                        //    message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        //}

                        #endregion

                        var subject = "GlobalServe ONESOURCE - IT Service Quote Request";
                        var toemail = "smbquoterequests@global-serve.com";

                        //if (string.IsNullOrWhiteSpace(quote.SupplierEmail))
                        //{
                        //    toemail = "it@global-serve.com";
                        //}
                        //else
                        //{
                        //    toemail = quote.SupplierEmail;
                        //}

                        SendEmail(toemail, message, subject);
                        SendEmail(userEmail, message, subject);

                    }
                }



                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }

        }

        //[Route("it-services")]
        public ActionResult ITServices()
        {
            try
            {
                ITServiceListModel isM = new ITServiceListModel();

                isM.ITServiceList = _itservicequotes.Get().OrderByDescending(x => x.ISQuoteId).ToList();

                if (adminSetting.UserGroupID == 4)
                {
                    isM.ITServiceList = _itservicequotes.Get().Where(x => x.CreatedBy == adminSetting.UserID).OrderByDescending(x => x.ISQuoteId).ToList();
                }

                isM.ServiceType = _itservicequotes.GetServiceType();
                isM.SLATypes = _itservicequotes.GetSLA();
                SFAsPresenter _sfa = new SFAsPresenter();
                isM.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).ToList();
                return View("ItServiceListView", isM);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("get-it-service-byid")]
        public ActionResult GetITServiceById(int id)
        {
            try
            {
                var itservice = _itservicequotes.Get().FirstOrDefault(x => x.ISQuoteId == id);
                if (itservice.SfaId == 0)
                {
                    itservice.Supplier = "Not Assigned";
                }
                else
                {
                    itservice.Supplier = sfas.GetSFAs().FirstOrDefault(x => x.SFAID == itservice.SfaId).SFAName;
                }
                return Json(itservice, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("update-it-service-quote")]
        [HttpPost]
        public ActionResult UpdateItServiceQuote()
        {
            try
            {
                //var resources = Request["resources"].ToString();
                //var scopeofwork = Request["scopeofwork"].ToString();
                //var servicetypeid = Request["servicetypeid"].ToString();
                //var slatypeid = Request["slatypeid"].ToString();
                var id = Request["quoteid"].ToString();
                var supplierid = Request["supplierid"].ToString();

                var _itquote = _itservicequotes.GetItServiceQuote().FirstOrDefault(x => x.ISQuoteId == Convert.ToInt32(id));

                if (_itquote != null)
                {
                    _itquote.SfaId = Convert.ToInt32(supplierid);
                    //_itquote.NoofResources = Convert.ToInt32(resources);
                    //_itquote.ScopeofWork = scopeofwork;
                    //_itquote.SLAId = Convert.ToInt32(slatypeid);
                    //_itquote.ServiceTypeId = Convert.ToInt32(servicetypeid);
                }

                _itservicequotes.ServiceQuote = _itquote;
                var i = _itservicequotes.Save();

                if (i > 0)
                {
                    var quote = _itservicequotes.Get().FirstOrDefault(x => x.ISQuoteId == Convert.ToInt32(id));
                    SFAsPresenter sfa = new SFAsPresenter();
                    var supplier = sfa.GetSFAs().FirstOrDefault(x => x.SFAID == quote.SfaId);

                    if (quote != null)
                    {
                        var message = "";

                        message = "GSP: " + supplier.SFAName + "<br>";
                        message += "Customer: " + quote.CustomerName + "<br>";

                        message += "<p>The following quote request comes from GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.ISQuoteId + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to respond to the quote request.</p>";
                        message += "<p>If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        //message += "<p>EMEA<br>Phone: +44 (0) 870 22 00515</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";

                        #region Old Email
                        //message = "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Supplier: " + quote.Supplier + "<br>";
                        //message += "Customer Site Address: " + quote.SiteAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "No of Resources: " + quote.NoofResources + "<br>";
                        //message += "Project Overview: " + quote.ProjectOverView + "<br>";
                        //message += "Detailed Scope of Work: " + quote.ScopeofWork + "<br>";
                        //message += "Equipment Details: " + quote.EquipmentDetails + "<br>";
                        //message += "SLA: " + quote.SLAType + "<br>";
                        //message += "Engineer Availability: " + quote.EngineerAvailabilityId.ToString() == "1" ? "Dispatch Onsite" : "FTE (Full time Engineer)" + "<br>";
                        //message += "Comment: " + quote.Comments + "<br>";
                        //message += "<hr><br>";
                        //message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        #endregion
                        var subject = "GlobalServe ONESOURCE - New IT Service Quote Request - " + quote.ISQuoteId.ToString();
                        var toemail = "";

                        if (supplier != null)
                        {
                            if (string.IsNullOrWhiteSpace(supplier.Email))
                            {
                                toemail = "it@global-serve.com";
                            }
                            else
                            {
                                toemail = supplier.Email;
                            }
                        }

                        var emails = toemail.Split(';');

                        foreach (var item in emails)
                        {
                            SendEmail(item, message, subject);
                        }

                        //SendEmail(toemail, message, subject);

                    }
                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }


        #endregion

        #region Asset Disposal Quotes

        //[Route("asset-disposal-quote")]
        public ActionResult AssetDisposalQuote()
        {
            try
            {
                AssetDisposalEntryModel aE = new AssetDisposalEntryModel();
                aE.AssetDisposalQuote = new AssetDisposalModel();
                aE.LineItems = new AssetDisposalLineItem();
                Session["listItem"] = null;
                return View("AssetDisposalView", aE);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult SaveAssetDisposalQuote(AssetDisposalEntryModel aem)
        {
            try
            {
                var ad = aem.AssetDisposalQuote;
                var _ad = _assetdisposalquotes.AssetDisposal;
                var filename1 = string.Empty;
                var filename2 = string.Empty;
                var filename3 = string.Empty;
                var filename4 = string.Empty;
                var filename5 = string.Empty;

                if (Request.Files.AllKeys.Any())
                {
                    if (Request.Files.Count > 0)
                    {
                        // Get the uploaded image from the Files collection
                        var httpPostedFile1 = Request.Files["file1"];
                        var httpPostedFile2 = Request.Files["file2"];
                        var httpPostedFile3 = Request.Files["file3"];
                        var httpPostedFile4 = Request.Files["file4"];
                        var httpPostedFile5 = Request.Files["file5"];

                        if (httpPostedFile1.ContentLength > 0)
                        {
                            filename1 = httpPostedFile1.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename1);
                            httpPostedFile1.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile2.ContentLength > 0)
                        {
                            filename2 = httpPostedFile2.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename2);
                            httpPostedFile2.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile3.ContentLength > 0)
                        {
                            filename3 = httpPostedFile3.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename3);
                            httpPostedFile3.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile4.ContentLength > 0)
                        {
                            filename4 = httpPostedFile4.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename4);
                            httpPostedFile4.SaveAs(fileSavePath);
                        }

                        if (httpPostedFile5.ContentLength > 0)
                        {
                            filename5 = httpPostedFile5.FileName;
                            var fileSavePath = Path.Combine(Server.MapPath("~/" + location), filename5);
                            httpPostedFile5.SaveAs(fileSavePath);
                        }
                    }
                }

                _ad.Attachment1 = filename1;
                _ad.Attachment2 = filename2;
                _ad.Attachment3 = filename3;
                _ad.Attachment4 = filename4;
                _ad.Attachment5 = filename5;

                _ad.Category = ad.Category;
                _ad.City = ad.City;
                _ad.Comments = ad.Comments;
                _ad.CountryId = Convert.ToInt32(ad.CountryId);
                _ad.CraetedBy = adminSetting.UserID;
                _ad.CreatedDate = DateTime.Now;
                _ad.CurrencyId = Convert.ToInt32(ad.CurrencyId);
                _ad.CustomerName = ad.CustomerName;
                _ad.Description = ad.Description;
                _ad.ExpectedPricePoint = ad.ExpectedPricePoint;
                _ad.InvoiceAddress = ad.InvoiceAddress;
                _ad.InvoiceContact = ad.InvoiceContact;
                _ad.PickUpAddress = ad.PickAddress;
                _ad.PickUpContact = ad.PickupContact;
                _ad.ProjectTimeline = ad.ProjectTimeLine;
                _ad.Quantity = Convert.ToInt32(ad.Quantity);
                _ad.AccountManagerName = ad.AccountManagerName;
                _ad.SfaId = Convert.ToInt32(ad.SupplierID);

                var i = _assetdisposalquotes.Save();

                if (i > 0)
                {
                    var quoteid = _assetdisposalquotes.Get().OrderByDescending(x => x.AssertDisposalId).FirstOrDefault().AssertDisposalId;
                    var quote = _assetdisposalquotes.Get().FirstOrDefault(x => x.AssertDisposalId == quoteid);

                    UserPresenter _user = new UserPresenter();
                    var userInfo = _user.GetContactInfo(adminSetting.UserID);
                    var userEmail = userInfo.Email;

                    var _lineitem = _hardwareQuotes.QuoteLineItems;

                    _lineitem.CratedDate = DateTime.Now;
                    _lineitem.CreatedBy = adminSetting.UserID;
                    _lineitem.Description = quote.Description;
                    _lineitem.Category = quote.Category;
                    _lineitem.Quantity = quote.Quantity;
                    _lineitem.QuoteId = quoteid;
                    _lineitem.QuoteTypeId = 3;
                    _hardwareQuotes.SaveLineItems();

                    if (Session["listItem"] != null)
                    {
                        var tempItems = (List<TempLineItem>)Session["listItem"];

                        if (tempItems.Count > 0)
                        {
                            //var _lineitem = _hardwareQuotes.QuoteLineItems;

                            //_lineitem.CratedDate = DateTime.Now;
                            //_lineitem.CreatedBy = adminSetting.UserID;
                            //_lineitem.Description = quote.Description;
                            //_lineitem.Category = quote.Category;
                            //_lineitem.Quantity = quote.Quantity;
                            //_lineitem.QuoteId = quoteid;
                            //_lineitem.QuoteTypeId = 3;                            
                            //_hardwareQuotes.SaveLineItems();


                            foreach (var item in tempItems)
                            {
                                _lineitem.CratedDate = DateTime.Now;
                                _lineitem.CreatedBy = adminSetting.UserID;
                                _lineitem.Description = item.Description;
                                _lineitem.Category = item.Categroy;
                                _lineitem.Quantity = item.Qunantity;
                                _lineitem.QuoteId = quoteid;
                                _lineitem.QuoteTypeId = 3;
                                _hardwareQuotes.SaveLineItems();
                            }
                        }

                        Session["listItem"] = null;
                    }

                    if (quote != null)
                    {
                        var message = "";

                        message += "Customer: " + quote.CustomerName + "<br>";
                        message += "Requested By: " + userInfo.FirstName + " " + userInfo.LastName + "<br>"; //" + userInfo.FirstName + " " + userInfo.MiddleName + " " + userInfo.LastName + "<br>";
                        message += "<p>Thank you for placing the following quote request on GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.AssertDisposalId.ToString() + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to view the status of the quote request.</p>";
                        message += "<p>Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";
                        //message += "<p>userEmail : " + userEmail + "</p>";

                        #region Old Email
                        //message = "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Customer Pick-up Address: " + quote.PickUpAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "Expected Price Point: " + quote.ExpectedPricePoint + "<br>";
                        //message += "Quantity: " + quote.Quantity + "<br>";
                        //message += "Description: " + quote.Description + "<br>";
                        //message += "Category: " + quote.Category + "<br>";
                        //message += "Comments: " + quote.Comments + "<br>";
                        //message += "Email: " + quote.SupplierEmail + "<br>";

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment1))
                        //{
                        //    message += "<hr><br>";
                        //    message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment2))
                        //{
                        //    message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment3))
                        //{
                        //    message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment4))
                        //{
                        //    message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //}

                        //if (!string.IsNullOrWhiteSpace(quote.Attachment5))
                        //{
                        //    message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";
                        //}

                        #endregion

                        var subject = "GlobalServe ONESOURCE - Asset Disposal Quote Request";
                        var toemail = "smbquoterequests@global-serve.com";

                        //if (string.IsNullOrWhiteSpace(quote.SupplierEmail))
                        //{
                        //    toemail = "it@global-serve.com";
                        //}
                        //else
                        //{
                        //    toemail = quote.SupplierEmail;
                        //}

                        SendEmail("muhammadshahid.ali@gmail.com", message, subject);
                        SendEmail(userEmail, message, subject);
                    }
                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.ToString(), routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.ToString().ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        //[Route("asset-disposal")]
        public ActionResult AssetDisposalList()
        {
            try
            {
                AssetDisposalQuoteList assetlst = new AssetDisposalQuoteList();

                assetlst.AssetDisposalList = _assetdisposalquotes.Get().OrderByDescending(x => x.AssertDisposalId).ToList();

                if (adminSetting.UserGroupID == 4)
                {
                    assetlst.AssetDisposalList = _assetdisposalquotes.Get().Where(x => x.CraetedBy == adminSetting.UserID).OrderByDescending(x => x.AssertDisposalId).ToList();
                }

                SFAsPresenter _sfa = new SFAsPresenter();
                assetlst.Suppliers = _sfa.GetSFAs().OrderBy(x => x.SFAName).ToList();
                return View("AssetDisposalListView", assetlst);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("get-asset-disposal-byid")]
        public ActionResult GetAssetDisposalById(int id)
        {
            try
            {

                var asset = _assetdisposalquotes.Get().FirstOrDefault(x => x.AssertDisposalId == id);
                if (asset.SfaId == 0)
                {
                    asset.Supplier = "Not Assigned";
                }
                else
                {
                    asset.Supplier = sfas.GetSFAs().FirstOrDefault(x => x.SFAID == asset.SfaId).SFAName;
                }

                return Json(asset, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("update-asset-disposal-quote")]
        [HttpPost]
        public ActionResult UpdateAssetDisposalQuote()
        {
            try
            {
                var id = Request["quoteid"].ToString();
                var supplierId = Request["supplierid"].ToString();
                //var quantity = Request["quantity"].ToString();
                //var description = Request["description"].ToString();
                //var category = Request["category"].ToString();

                var _assetquote = _assetdisposalquotes.GetAssetDisposalQuotes().FirstOrDefault(x => x.AssertDisposalId == Convert.ToInt32(id));

                if (_assetquote != null)
                {
                    _assetquote.SfaId = Convert.ToInt32(supplierId);
                    //_assetquote.Quantity = Convert.ToInt32(quantity);
                    //_assetquote.Description = description;
                    //_assetquote.Category = category;
                }

                _assetdisposalquotes.AssetDisposal = _assetquote;
                var i = _assetdisposalquotes.Save();

                if (i > 0)
                {
                    var quote = _assetdisposalquotes.Get().FirstOrDefault(x => x.AssertDisposalId == Convert.ToInt32(id));
                    SFAsPresenter sfa = new SFAsPresenter();
                    var supplier = sfa.GetSFAs().FirstOrDefault(x => x.SFAID == quote.SfaId);

                    if (quote != null)
                    {
                        var message = "";

                        message = "GSP: " + supplier.SFAName + "<br>";
                        message += "Customer: " + quote.CustomerName + "<br>";

                        message += "<p>The following quote request comes from GlobalServe ONESOURCE</p>";

                        message += "Quote #:      " + quote.AssertDisposalId + "<br>";
                        message += "Request Date: " + DateTime.Now.ToString() + "<br>";
                        message += "Country:      " + quote.Country + "<br>";

                        message += "<p>Please logon to http://gsonesource.global-serve.com:8080 to respond to the quote request.</p>";
                        message += "<p>If you need to contact GlobalServe, please call or fax the Customer Center at:</p>";
                        //message += "<p>EMEA<br>Phone: +44 (0) 870 22 00515</p>";
                        message += "<p>Americas<br>Phone: +1 (914) 729 6800<br>Fax : 201-408-5275</p>";
                        message += "<p>GlobalServe ONESOURCE: http://gsonesource.global-serve.com:8080</p>";


                        #region Old Email

                        //message = "Customer Name: " + quote.CustomerName + "<br>";
                        //message += "Account Manager: " + quote.AccountManagerName + "<br>";
                        //message += "Customer Pick-up Address: " + quote.PickUpAddress + "<br>";
                        //message += "Country: " + quote.Country + "<br>";
                        //message += "Currency: " + quote.Currency + "<br>";
                        //message += "City: " + quote.City + "<br>";
                        //message += "Expected Price Point: " + quote.ExpectedPricePoint + "<br>";
                        //message += "Quantity: " + quote.Quantity + "<br>";
                        //message += "Description: " + quote.Description + "<br>";
                        //message += "Category: " + quote.Category + "<br>";
                        //message += "Comments: " + quote.Comments + "<br>";
                        //message += "Email: " + quote.SupplierEmail + "<br>";
                        //message += "<hr><br>";
                        //message += "Attachment1: <a href='" + sitelocation + quote.Attachment1 + "'>" + quote.Attachment1 + "</a><br>";
                        //message += "Attachment2: <a href='" + sitelocation + quote.Attachment2 + "'>" + quote.Attachment2 + "</a><br>";
                        //message += "Attachment3: <a href='" + sitelocation + quote.Attachment3 + "'>" + quote.Attachment3 + "</a><br>";
                        //message += "Attachment4: <a href='" + sitelocation + quote.Attachment4 + "'>" + quote.Attachment4 + "</a><br>";
                        //message += "Attachment5: <a href='" + sitelocation + quote.Attachment5 + "'>" + quote.Attachment5 + "</a><br>";

                        #endregion

                        var subject = "GlobalServe ONESOURCE - New Asset Disposal Quote Request - " + quote.AssertDisposalId.ToString();
                        var toemail = "";


                        if (supplier != null)
                        {
                            if (string.IsNullOrWhiteSpace(supplier.Email))
                            {
                                toemail = "it@global-serve.com";
                            }
                            else
                            {
                                toemail = supplier.Email;
                            }
                        }

                        var emails = toemail.Split(';');

                        foreach (var item in emails)
                        {
                            SendEmail(item, message, subject);
                        }

                        //SendEmail(toemail, message, subject);
                    }
                }

                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }


        #endregion

        #region Country & Currencies

        [Route("get-local-currency")]
        public ActionResult GetLocalCurrency(int id)
        {
            try
            {
                var _currency = _hardwareQuotes.Currencies().FirstOrDefault(x => x.CountryID == id && x.IsActive == true);
                string currency = string.Empty;

                if (_currency != null)
                {
                    currency = _currency.CurrencyName + " (" + _currency.CurrencySymbol + ");" + _currency.CurrencyID;
                }
                else
                {
                    _currency = _hardwareQuotes.Currencies().FirstOrDefault(x => x.CurrencySymbol == "USD" && x.IsActive == true);
                    currency = _currency.CurrencyName + " (" + _currency.CurrencySymbol + ");" + _currency.CurrencyID;
                }

                return Json(currency, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        #endregion

        #region Quote Line Item
        [HttpPost]
        public ActionResult InsertLineItem(HardwareEntryModel col)
        {
            try
            {
                var quantity = col.LineItems.Quantity;
                var description = col.LineItems.Description;
                var sku = col.LineItems.SKU;
                var manufacturer = col.LineItems.Manufacturer;
                string jsonTable = string.Empty;
                List<TempLineItem> _lstlineItem = new List<TempLineItem>();

                jsonTable = "<table class='table table-striped'>";
                jsonTable += "<thead><tr>";
                jsonTable += "<th>Quantity</th>";
                jsonTable += "<th>Description</th>";
                jsonTable += "<th>SKU</th>";
                jsonTable += "<th>Manufacturer</th>";
                jsonTable += "<th class='editHead'></th>";
                jsonTable += "</tr></thead>";
                jsonTable += "<tbody>";


                if (Session["listItem"] == null)
                {
                    _lstlineItem.Add(new Models.TempLineItem { Id = 1, Description = description, Manufacturer = manufacturer, Qunantity = quantity, SKU = sku, QuoteTypeId = 1 });
                    Session["listItem"] = _lstlineItem;

                    foreach (var item in _lstlineItem)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Qunantity + "</td>";
                        jsonTable += "<td>" + item.Description + "</td>";
                        jsonTable += "<td>" + item.SKU + "</td>";
                        jsonTable += "<td>" + item.Manufacturer + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }
                else
                {
                    var tempItems = (List<TempLineItem>)Session["listItem"];

                    var maxId = tempItems.Max(x => x.Id);
                    tempItems.Add(new TempLineItem { Id = (maxId + 1), Description = description, Manufacturer = manufacturer, Qunantity = quantity, SKU = sku, QuoteTypeId = 1 });
                    Session["listItem"] = tempItems;

                    foreach (var item in tempItems)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Qunantity + "</td>";
                        jsonTable += "<td>" + item.Description + "</td>";
                        jsonTable += "<td>" + item.SKU + "</td>";
                        jsonTable += "<td>" + item.Manufacturer + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }

                jsonTable += "</tbody>";
                jsonTable += "</table>";


                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult InsertSoftwareLineItem(SoftwareEntryModel col)
        {
            try
            {
                var quantity = col.LineItems.Quantity;
                var description = col.LineItems.Description;
                var sku = col.LineItems.SKU;
                var manufacturer = col.LineItems.Manufacturer;
                var serailNumber = col.LineItems.SerailNumber;
                string jsonTable = string.Empty;
                List<TempLineItem> _lstlineItem = new List<TempLineItem>();

                jsonTable = "<table class='table table-striped'>";
                jsonTable += "<thead><tr>";
                jsonTable += "<th>Quantity</th>";
                jsonTable += "<th>Description</th>";
                jsonTable += "<th>SKU</th>";
                jsonTable += "<th>Serail Number</th>";
                jsonTable += "<th>Manufacturer</th>";
                jsonTable += "<th class='editHead'></th>";
                jsonTable += "</tr></thead>";
                jsonTable += "<tbody>";


                if (Session["listItem"] == null)
                {
                    _lstlineItem.Add(new Models.TempLineItem { Id = 1, Description = description, SerailNumber = serailNumber, Manufacturer = manufacturer, Qunantity = quantity, SKU = sku, QuoteTypeId = 2 });
                    Session["listItem"] = _lstlineItem;

                    foreach (var item in _lstlineItem)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Qunantity + "</td>";
                        jsonTable += "<td>" + item.Description + "</td>";
                        jsonTable += "<td>" + item.SKU + "</td>";
                        jsonTable += "<td>" + item.SerailNumber + "</td>";
                        jsonTable += "<td>" + item.Manufacturer + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }
                else
                {
                    var tempItems = (List<TempLineItem>)Session["listItem"];

                    var maxId = tempItems.Max(x => x.Id);
                    tempItems.Add(new TempLineItem { Id = (maxId + 1), Description = description, SerailNumber = serailNumber, Manufacturer = manufacturer, Qunantity = quantity, SKU = sku, QuoteTypeId = 2 });
                    Session["listItem"] = tempItems;

                    foreach (var item in tempItems)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Qunantity + "</td>";
                        jsonTable += "<td>" + item.Description + "</td>";
                        jsonTable += "<td>" + item.SKU + "</td>";
                        jsonTable += "<td>" + item.SerailNumber + "</td>";
                        jsonTable += "<td>" + item.Manufacturer + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }

                jsonTable += "</tbody>";
                jsonTable += "</table>";


                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult InsertAssetLineItem(AssetDisposalEntryModel col)
        {
            try
            {
                var quantity = col.LineItems.Quantity;
                var description = col.LineItems.Description;
                var category = col.LineItems.Category;
                string jsonTable = string.Empty;
                List<TempLineItem> _lstlineItem = new List<TempLineItem>();

                jsonTable = "<table class='table table-striped'>";
                jsonTable += "<thead><tr>";
                jsonTable += "<th>Quantity</th>";
                jsonTable += "<th>Description</th>";
                jsonTable += "<th>Category</th>";
                jsonTable += "<th class='editHead'></th>";
                jsonTable += "</tr></thead>";
                jsonTable += "<tbody>";


                if (Session["listItem"] == null)
                {
                    _lstlineItem.Add(new Models.TempLineItem { Id = 1, Description = description, Categroy = category, Qunantity = quantity, QuoteTypeId = 3 });
                    Session["listItem"] = _lstlineItem;

                    foreach (var item in _lstlineItem)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Qunantity + "</td>";
                        jsonTable += "<td>" + item.Description + "</td>";
                        jsonTable += "<td>" + item.Categroy + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }
                else
                {
                    var tempItems = (List<TempLineItem>)Session["listItem"];

                    var maxId = tempItems.Max(x => x.Id);
                    tempItems.Add(new TempLineItem { Id = (maxId + 1), Description = description, Categroy = category, Qunantity = quantity, QuoteTypeId = 3 });
                    Session["listItem"] = tempItems;

                    foreach (var item in tempItems)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Qunantity + "</td>";
                        jsonTable += "<td>" + item.Description + "</td>";
                        jsonTable += "<td>" + item.Categroy + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }

                jsonTable += "</tbody>";
                jsonTable += "</table>";


                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [HttpPost]
        public ActionResult InsertServiceLineItem(ITServicesEnteryModel col)
        {
            try
            {
                var resources = col.LineItems.NumberofResources;
                var engavail = col.LineItems.EngineerAvailability;
                var equDetails = col.LineItems.EquipmentDetails;
                var slaId = col.LineItems.SALId;
                var scopeofwork = col.LineItems.ScopeofWork;
                var servicetypeid = col.LineItems.ServiceTypeId;

                string jsonTable = string.Empty;
                List<TempLineItem> _lstlineItem = new List<TempLineItem>();

                jsonTable = "<table class='table table-striped'>";
                jsonTable += "<thead><tr>";
                jsonTable += "<th>Resources</th>";
                jsonTable += "<th>Sla Type</th>";
                jsonTable += "<th>Engineer Availability</th>";
                jsonTable += "<th>Service Type</th>";
                //jsonTable += "<th>Hardware Eq Details</th>";
                jsonTable += "<th class='editHead'></th>";
                jsonTable += "</tr></thead>";
                jsonTable += "<tbody>";

                var servicetype = _itservicequotes.GetServiceType().FirstOrDefault(x => x.ServiceTypeId == Convert.ToInt32(servicetypeid)).ServiceTypeName;
                var sla = _itservicequotes.GetSLA().FirstOrDefault(x => x.SLATypeId == Convert.ToInt32(slaId)).SLAName;
                var engaval = engavail == "1" ? "Dispatch Onsite" : "FTE (Full time Engineer)";

                if (Session["listItem"] == null)
                {
                    _lstlineItem.Add(new Models.TempLineItem { Id = 1, Resources = resources, SLAType = sla, SLAId = Convert.ToInt32(slaId), EngAv = engaval, EngAvId = Convert.ToInt32(engavail), ServiceType = servicetype, ServiceId = Convert.ToInt32(servicetypeid), Description = scopeofwork, HardwareDetails = equDetails, QuoteTypeId = 4 });
                    Session["listItem"] = _lstlineItem;

                    foreach (var item in _lstlineItem)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Resources + "</td>";
                        jsonTable += "<td>" + item.SLAType + "</td>";
                        jsonTable += "<td>" + item.EngAv + "</td>";
                        jsonTable += "<td>" + item.ServiceType + "</td>";
                        //jsonTable += "<td>" + item.HardwareDetails + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }
                else
                {
                    var tempItems = (List<TempLineItem>)Session["listItem"];

                    var maxId = tempItems.Max(x => x.Id);
                    tempItems.Add(new Models.TempLineItem { Id = (maxId + 1), Resources = resources, SLAType = sla, SLAId = Convert.ToInt32(slaId), EngAv = engaval, EngAvId = Convert.ToInt32(engavail), ServiceType = servicetype, ServiceId = Convert.ToInt32(servicetypeid), Description = scopeofwork, HardwareDetails = equDetails, QuoteTypeId = 4 });
                    Session["listItem"] = tempItems;

                    foreach (var item in tempItems)
                    {
                        jsonTable += "<tr>";
                        jsonTable += "<td>" + item.Resources + "</td>";
                        jsonTable += "<td>" + item.SLAType + "</td>";
                        jsonTable += "<td>" + item.EngAv + "</td>";
                        jsonTable += "<td>" + item.ServiceType + "</td>";
                        //jsonTable += "<td>" + item.HardwareDetails + "</td>";
                        jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                        jsonTable += "</tr>";
                    }
                }

                jsonTable += "</tbody>";
                jsonTable += "</table>";


                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("delete-line-item")]
        public ActionResult DeleteLineItme(int id)
        {
            try
            {
                List<TempLineItem> _lstlineItem = new List<TempLineItem>();
                string jsonTable = string.Empty;

                if (Session["listItem"] != null)
                {

                    var tempItems = (List<TempLineItem>)Session["listItem"];
                    var _tempItem = tempItems.FirstOrDefault(x => x.Id == id);
                    tempItems.Remove(_tempItem);

                    Session["listItem"] = tempItems;

                    if (tempItems.Count > 0)
                    {
                        if (_tempItem.QuoteTypeId == 1)
                        {

                            jsonTable = "<table class='table table-striped'>";
                            jsonTable += "<thead><tr>";
                            jsonTable += "<th>Quantity</th>";
                            jsonTable += "<th>Description</th>";
                            jsonTable += "<th>SKU</th>";
                            jsonTable += "<th>Manufacturer</th>";
                            jsonTable += "<th class='editHead'></th>";
                            jsonTable += "</tr></thead>";
                            jsonTable += "<tbody>";

                            foreach (var item in tempItems)
                            {
                                jsonTable += "<tr>";
                                jsonTable += "<td>" + item.Qunantity + "</td>";
                                jsonTable += "<td>" + item.Description + "</td>";
                                jsonTable += "<td>" + item.SKU + "</td>";
                                jsonTable += "<td>" + item.Manufacturer + "</td>";
                                jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                                jsonTable += "</tr>";
                            }
                        }
                        else if (_tempItem.QuoteTypeId == 2)
                        {
                            jsonTable = "<table class='table table-striped'>";
                            jsonTable += "<thead><tr>";
                            jsonTable += "<th>Quantity</th>";
                            jsonTable += "<th>Description</th>";
                            jsonTable += "<th>SKU</th>";
                            jsonTable += "<th>Serial #</th>";
                            jsonTable += "<th>Manufacturer</th>";
                            jsonTable += "<th class='editHead'></th>";
                            jsonTable += "</tr></thead>";
                            jsonTable += "<tbody>";

                            foreach (var item in tempItems)
                            {
                                jsonTable += "<tr>";
                                jsonTable += "<td>" + item.Qunantity + "</td>";
                                jsonTable += "<td>" + item.Description + "</td>";
                                jsonTable += "<td>" + item.SKU + "</td>";
                                jsonTable += "<td>" + item.SerailNumber + "</td>";
                                jsonTable += "<td>" + item.Manufacturer + "</td>";
                                jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                                jsonTable += "</tr>";
                            }
                        }
                        else if (_tempItem.QuoteTypeId == 3)
                        {
                            jsonTable = "<table class='table table-striped'>";
                            jsonTable += "<thead><tr>";
                            jsonTable += "<th>Quantity</th>";
                            jsonTable += "<th>Description</th>";
                            jsonTable += "<th>Category</th>";
                            jsonTable += "<th class='editHead'></th>";
                            jsonTable += "</tr></thead>";
                            jsonTable += "<tbody>";

                            foreach (var item in tempItems)
                            {
                                jsonTable += "<tr>";
                                jsonTable += "<td>" + item.Qunantity + "</td>";
                                jsonTable += "<td>" + item.Description + "</td>";
                                jsonTable += "<td>" + item.Categroy + "</td>";
                                jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                                jsonTable += "</tr>";
                            }
                        }
                        else if (_tempItem.QuoteTypeId == 4)
                        {
                            jsonTable = "<table class='table table-striped'>";
                            jsonTable += "<thead><tr>";
                            jsonTable += "<th>Resources</th>";
                            jsonTable += "<th>Sla Type</th>";
                            jsonTable += "<th>Engineer Availability</th>";
                            jsonTable += "<th>Service Type</th>";
                            //jsonTable += "<th>Hardware Eq Details</th>";
                            jsonTable += "<th class='editHead'></th>";
                            jsonTable += "</tr></thead>";
                            jsonTable += "<tbody>";

                            foreach (var item in tempItems)
                            {
                                jsonTable += "<tr>";
                                jsonTable += "<td>" + item.Resources + "</td>";
                                jsonTable += "<td>" + item.SLAType + "</td>";
                                jsonTable += "<td>" + item.EngAv + "</td>";
                                jsonTable += "<td>" + item.ServiceType + "</td>";
                                //jsonTable += "<td>" + item.HardwareDetails + "</td>";
                                jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.Id + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>";
                                jsonTable += "</tr>";
                            }
                        }

                        jsonTable += "</tbody>";
                        jsonTable += "</table>";
                    }
                    else
                    {
                        Session["listItem"] = null;
                    }
                }

                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("get-line-item")]
        public ActionResult GetLineItem(int id, int quotetype)
        {
            try
            {
                string jsonTable = string.Empty;
                var _tempItem = _hardwareQuotes.GetQuoteLineItemByQuoteId(id, quotetype);

                if (_tempItem.Count > 0)
                {
                    jsonTable = "<table class='table table-striped'>";
                    jsonTable += "<thead><tr>";

                    if (quotetype == 1)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>SKU</th>";
                        jsonTable += "<th>Manufacturer</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.SKU + "</td>";
                            jsonTable += "<td>" + item.Manufacturer + "</td>";
                            jsonTable += "<td class='editCol'><a id='delButton' onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (quotetype == 2)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>SKU</th>";
                        jsonTable += "<th>Serial Number</th>";
                        jsonTable += "<th>Manufacturer</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.SKU + "</td>";
                            jsonTable += "<td>" + item.SerailNumber + "</td>";
                            jsonTable += "<td>" + item.Manufacturer + "</td>";
                            jsonTable += "<td class='editCol'><a id='delButton' onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (quotetype == 3)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>Category</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.Category + "</td>";
                            jsonTable += "<td class='editCol'><a id='delButton' onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (quotetype == 4)
                    {
                        jsonTable = "<table class='table table-striped'>";
                        jsonTable += "<thead><tr>";
                        jsonTable += "<th>Resources</th>";
                        jsonTable += "<th>Sla</th>";
                        jsonTable += "<th>Availability</th>";
                        jsonTable += "<th>Service</th>";
                        //jsonTable += "<th>Hardware Eq Details</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            var servicetype = _itservicequotes.GetServiceType().FirstOrDefault(x => x.ServiceTypeId == Convert.ToInt32(item.ServiceTypeId)).ServiceTypeName;
                            var sla = _itservicequotes.GetSLA().FirstOrDefault(x => x.SLATypeId == Convert.ToInt32(item.SLATypeId)).SLAName;
                            var engaval = item.EngineerAvailability == 1 ? "Dispatch Onsite" : "FTE (Full time Engineer)";

                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Resources + "</td>";
                            jsonTable += "<td>" + sla + "</td>";
                            jsonTable += "<td>" + engaval + "</td>";
                            jsonTable += "<td>" + servicetype + "</td>";
                            jsonTable += "<td class='editCol'><a id='delButton' onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }

                    jsonTable += "</tbody>";
                    jsonTable += "</table>";
                }

                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("get-line-item-byid")]
        public ActionResult GetLineItemById(int id)
        {
            try
            {
                var _tempItem = _hardwareQuotes.GetQouteLineItemById(id);
                return Json(_tempItem, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        [Route("update-line-item")]
        [HttpPost]
        public ActionResult UpdateLineItem()
        {
            try
            {
                var id = Request["id"].ToString();

                var lineitem = _hardwareQuotes.GetQouteLineItemById(Convert.ToInt32(id));
                var quoteid = lineitem.QuoteId;
                var quotetype = lineitem.QuoteTypeId;

                if (quotetype == 1)
                {
                    var manfuacturer = Request["manufacturer"].ToString();
                    var sku = Request["sku"].ToString();
                    var quantity = Request["quantity"].ToString();
                    var description = Request["description"].ToString();
                    lineitem.Manufacturer = manfuacturer;
                    lineitem.Quantity = Convert.ToInt32(quantity);
                    lineitem.Description = description;
                    lineitem.SKU = sku;
                }
                else if (quotetype == 2)
                {
                    var manfuacturer = Request["manufacturer"].ToString();
                    var sku = Request["sku"].ToString();
                    var serialnumber = Request["serailnumber"].ToString();
                    var quantity = Request["quantity"].ToString();
                    var description = Request["description"].ToString();

                    lineitem.Manufacturer = manfuacturer;
                    lineitem.Quantity = Convert.ToInt32(quantity);
                    lineitem.Description = description;
                    lineitem.SKU = sku;
                    lineitem.SerailNumber = serialnumber;
                }
                else if (quotetype == 3)
                {
                    var category = Request["category"].ToString();
                    var quantity = Request["quantity"].ToString();
                    var description = Request["description"].ToString();

                    lineitem.Category = category;
                    lineitem.Quantity = Convert.ToInt32(quantity);
                    lineitem.Description = description;
                }
                else if (quotetype == 4)
                {
                    var resource = Request["resource"].ToString();
                    var scopeofwork = Request["scopeofwork"].ToString();
                    var servicetype = Request["servicetype"].ToString();
                    var slaid = Request["slaid"].ToString();
                    var engid = Request["engid"].ToString();

                    lineitem.Resources = Convert.ToInt32(resource);
                    lineitem.ServiceTypeId = Convert.ToInt32(servicetype);
                    lineitem.SLATypeId = Convert.ToInt32(slaid);
                    lineitem.EngineerAvailability = Convert.ToInt32(engid);
                    lineitem.Description = scopeofwork;
                }

                _hardwareQuotes.QuoteLineItems = lineitem;

                _hardwareQuotes.UpdateLineItem();


                string jsonTable = string.Empty;
                var _tempItem = _hardwareQuotes.GetQuoteLineItemByQuoteId((int)quoteid, (int)quotetype);

                if (_tempItem.Count > 0)
                {
                    jsonTable = "<table class='table table-striped'>";
                    jsonTable += "<thead><tr>";

                    if (quotetype == 1)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>SKU</th>";
                        jsonTable += "<th>Manufacturer</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.SKU + "</td>";
                            jsonTable += "<td>" + item.Manufacturer + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (quotetype == 2)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>SKU</th>";
                        jsonTable += "<th>Serial Number</th>";
                        jsonTable += "<th>Manufacturer</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.SKU + "</td>";
                            jsonTable += "<td>" + item.SerailNumber + "</td>";
                            jsonTable += "<td>" + item.Manufacturer + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (quotetype == 4)
                    {
                        jsonTable = "<table class='table table-striped'>";
                        jsonTable += "<thead><tr>";
                        jsonTable += "<th>Resources</th>";
                        jsonTable += "<th>Sla</th>";
                        jsonTable += "<th>Availability</th>";
                        jsonTable += "<th>Service</th>";
                        //jsonTable += "<th>Hardware Eq Details</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            var servicetype = _itservicequotes.GetServiceType().FirstOrDefault(x => x.ServiceTypeId == Convert.ToInt32(item.ServiceTypeId)).ServiceTypeName;
                            var sla = _itservicequotes.GetSLA().FirstOrDefault(x => x.SLATypeId == Convert.ToInt32(item.SLATypeId)).SLAName;
                            var engaval = item.EngineerAvailability == 1 ? "Dispatch Onsite" : "FTE (Full time Engineer)";

                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Resources + "</td>";
                            jsonTable += "<td>" + sla + "</td>";
                            jsonTable += "<td>" + engaval + "</td>";
                            jsonTable += "<td>" + servicetype + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (quotetype == 3)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>Category</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.Category + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }

                    jsonTable += "</tbody>";
                    jsonTable += "</table>";
                }

                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.ToString(), routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        [Route("delete-line-item-byid")]
        public ActionResult DeleteLineItem(int id, int quoteid, int qoutetype)
        {
            try
            {
                string jsonTable = string.Empty;
                _hardwareQuotes.DeleteLineItems(id);
                var _tempItem = _hardwareQuotes.GetQuoteLineItemByQuoteId(quoteid, qoutetype);

                if (_tempItem.Count > 0)
                {
                    jsonTable = "<table class='table table-striped'>";
                    jsonTable += "<thead><tr>";

                    if (qoutetype == 1)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>SKU</th>";
                        jsonTable += "<th>Manufacturer</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.SKU + "</td>";
                            jsonTable += "<td>" + item.Manufacturer + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (qoutetype == 2)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>SKU</th>";
                        jsonTable += "<th>Serail Number</th>";
                        jsonTable += "<th>Manufacturer</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.SKU + "</td>";
                            jsonTable += "<td>" + item.SerailNumber + "</td>";
                            jsonTable += "<td>" + item.Manufacturer + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (qoutetype == 3)
                    {
                        jsonTable += "<th>Quantity</th>";
                        jsonTable += "<th>Description</th>";
                        jsonTable += "<th>Category</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Quantity + "</td>";
                            jsonTable += "<td>" + item.Description + "</td>";
                            jsonTable += "<td>" + item.Category + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }
                    else if (qoutetype == 4)
                    {
                        jsonTable = "<table class='table table-striped'>";
                        jsonTable += "<thead><tr>";
                        jsonTable += "<th>Resources</th>";
                        jsonTable += "<th>Sla</th>";
                        jsonTable += "<th>Availability</th>";
                        jsonTable += "<th>Service</th>";
                        //jsonTable += "<th>Hardware Eq Details</th>";
                        jsonTable += "<th class='editHead'></th>";
                        jsonTable += "</tr></thead>";
                        jsonTable += "<tbody>";

                        foreach (var item in _tempItem)
                        {
                            var servicetype = _itservicequotes.GetServiceType().FirstOrDefault(x => x.ServiceTypeId == Convert.ToInt32(item.ServiceTypeId)).ServiceTypeName;
                            var sla = _itservicequotes.GetSLA().FirstOrDefault(x => x.SLATypeId == Convert.ToInt32(item.SLATypeId)).SLAName;
                            var engaval = item.EngineerAvailability == 1 ? "Dispatch Onsite" : "FTE (Full time Engineer)";

                            jsonTable += "<tr>";
                            jsonTable += "<td>" + item.Resources + "</td>";
                            jsonTable += "<td>" + sla + "</td>";
                            jsonTable += "<td>" + engaval + "</td>";
                            jsonTable += "<td>" + servicetype + "</td>";
                            jsonTable += "<td class='editCol'><a onclick='DeleteLineItem(" + item.QuoteLineItemId + "," + item.QuoteId + "," + item.QuoteTypeId + ")' style='color: black'><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;" +
                                "<a onclick='EditLineItem(" + item.QuoteLineItemId + ")' style='color: black'><i class='fa fa-pencil-square' aria-hidden='true'></i></a></td>";
                            jsonTable += "</tr>";

                        }
                    }

                    jsonTable += "</tbody>";
                    jsonTable += "</table>";
                }

                return Json(jsonTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        #endregion


        public ActionResult BulkQuotesUpload()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    var model = new BulkQuoteRequestModel();
                    CustomerPresenter cp = new CustomerPresenter();
                    @ViewBag.AddEditAccess = isAddEditAllow("BulkQuotesUpload");
                    model.CustomerList = cp.GetCustomers();
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }


        }


        public JsonResult GetCountries(int customerId)
        {
            CountryPresenter cp = new CountryPresenter();
            return Json(cp.GetCountryByCustomer(customerId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerCountryCurrencies(int customerId, int countryId)
        {
            CurrencyPresenter cp = new CurrencyPresenter();
            return Json(cp.GetCustomerCountryCurrency(customerId, countryId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSFAs(int customerId, int countryId)
        {
            SFAsPresenter sfa = new SFAsPresenter();
            return Json(sfa.GetSFAs(customerId, countryId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetCustomerUsers(int customerId)
        {
            CustomerPresenter cp = new CustomerPresenter();
            return Json(cp.GetCustomerUsers(customerId), JsonRequestBehavior.AllowGet);

        }
        public DataTable removeEmptyColumns(DataTable dataTable)
        {
          var dtResult =  dataTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field =>
 field is System.DBNull || string.Compare((field as string), string.Empty) == 0)).CopyToDataTable();

            return dtResult;
        }

        [HttpPost]
        public ActionResult UploadBundleQuotes(BulkQuoteRequestModel model)
        {
            try
            {
                int resInt = 0, rowNo = 1;
                var userId = Session["UserID"].ToString();
                DataSet dsData = new DataSet();
                DataSet dsError = new DataSet();
                DataTable dtErr = new DataTable();

                OleDbDataAdapter dp;

                //Add Columns in Error dataset
                dtErr.Columns.Add("RowNo", typeof(int));
                dtErr.Columns.Add("Column", typeof(string));
                dtErr.Columns.Add("Value", typeof(string));
                dtErr.Columns.Add("ErrDesc", typeof(string));
                TempData["QuoteRequestMessage"] = "";

                var fileType = Request.Form["fileType"];
                var file = Request.Files[0];
                var fileExtenstion = Path.GetExtension(file.FileName);
                var folderPath = Server.MapPath("~/uploads/Quote_Upload");





                var fileName = "Bulk-Quotes-Request" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + fileExtenstion;
                var path = Path.Combine(folderPath, fileName);

                //Check if the path exists or not
                if (!Directory.Exists(folderPath))
                    System.IO.Directory.CreateDirectory(folderPath);

                file.SaveAs(path);

                string conStr = String.Empty;
                string cmd = string.Empty;

                if (fileExtenstion == "xls")
                {
                    //Then make connection string for xls
                    conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + "; Extended Properties=Excel 8.0";
                }
                else
                {
                    //Else make connection string for xlsx
                    conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + "; Extended Properties=Excel 8.0";
                }

                cmd = "Select * from [Sheet1$]";
                dp = new OleDbDataAdapter(cmd, conStr);

                //Fill the dataset with excel data
                dp.Fill(dsData);
                var table = removeEmptyColumns(dsData.Tables[0]);
                var count = table.Rows.Count;//dsData.Tables[0].Rows.Count;

                if (count == 0)
                {
                    //Then set error label text
                    TempData["QuoteRequestMessage"] = "Uploaded file is empty, please enter data and upload again.";
                }
                
                //Loop to iterate through records in dataset
                foreach (DataRow row in table.Rows)
                {
                   
                            rowNo++;
                            if (string.IsNullOrEmpty(row["DESCRIPTION"].ToString()))
                            {
                                dtErr.Rows.Add(rowNo, "DESCRIPTION", row["DESCRIPTION"].ToString().Trim(), "Please provide description");

                            }
                            if (string.IsNullOrEmpty(row["MANUFACTURER"].ToString()))
                            {
                                dtErr.Rows.Add(rowNo, "MANUFACTURER", row["MANUFACTURER"].ToString().Trim(), "Please provide manufacturer");
                            }

                            if (string.IsNullOrEmpty(row["SKU"].ToString()))
                            {
                                dtErr.Rows.Add(rowNo, "SKU", row["SKU"].ToString().Trim(), "Please provide SKU");
                            }

                            if (int.TryParse(row["REQUIRED_QUANTITY"].ToString().Trim(), out resInt) == false)
                            {

                                if (row["REQUIRED_QUANTITY"].ToString().Trim().Length < 1)
                                {

                                    dtErr.Rows.Add(rowNo, "REQUIRED_QUANTITY", row["REQUIRED_QUANTITY"].ToString().Trim(), "Please provide required quantity");
                                }
                                else
                                {
                                    dtErr.Rows.Add(rowNo, "REQUIRED_QUANTITY", row["REQUIRED_QUANTITY"].ToString().Trim(), "Please enter a valid value for required quantity");
                                }
                            }
                            if (int.TryParse(row["REQUIRED_QUANTITY"].ToString().Trim(), out resInt) == true)
                            {

                                if (Convert.ToInt32(row["REQUIRED_QUANTITY"].ToString()) < 0 || Convert.ToInt32(row["REQUIRED_QUANTITY"].ToString()) == 0)
                                {

                                    dtErr.Rows.Add(rowNo, "REQUIRED_QUANTITY", row["REQUIRED_QUANTITY"].ToString().Trim(), "required quantity should be greater than zero");
                                }


                            }
                        
                }

                dsError.Tables.Add(dtErr);

                if (dsError.Tables[0].Rows.Count > 0)
                {
                    model.UploadDataTable = null;
                    model.ErrorDataTable = dsError.Tables[0];
                }
                else
                {
                    model.UploadDataTable = table;//dsData.Tables[0];
                    model.ErrorDataTable = null;
                    model.IsdUserID = Convert.ToInt32(userId);
                    var partquoteId = InsertPartQuote(model);
                    var partQuoteRequestModel = (from DataRow dr in table.Rows
                                                 select new OneSourceAdminCore.Model.PartQuoteRequestModel()
                                                 {
                                                     PartQuoteID = partquoteId,
                                                     Manufacturer = dr["MANUFACTURER"].ToString(),
                                                     SKU = dr["SKU"].ToString(),
                                                     Description = dr["DESCRIPTION"].ToString(),
                                                     Quantity = Convert.ToInt32(dr["REQUIRED_QUANTITY"].ToString()),
                                                     IsActive = true,
                                                     LastUpdated = DateTime.Now,
                                                     LastUpdatedByUserID = model.CustomerUserID,
                                                     CatalogStandardID = null,
                                                     CatalogID = null

                                                 }).ToList();

                    quoterequest.InsertPartQuoteRequest(partQuoteRequestModel);
                    if (partquoteId > 0)
                    {
                        NotifyQuoteRequest(partquoteId, null, "oldquote");
                        TempData["QuoteRequestMessage"] = "Your Quote submitted sucessfully. Quote#" + partquoteId;
                    }
                    else
                    {
                        TempData["QuoteRequestMessage"] = "Your Quote doesnot submitted sucessfully";
                    }

                }
                CustomerPresenter cp = new CustomerPresenter();
                @ViewBag.AddEditAccess = isAddEditAllow("BulkQuotesUpload");
                model.CustomerList = cp.GetCustomers();
                return View("BulkQuotesUpload", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        public int InsertPartQuote(BulkQuoteRequestModel model)
        {

            try
            {
                var partquoteId = 0;

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_BulkQuotesRequest", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerRef", SqlDbType.Text));
                    command.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CountryID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@CurrencyID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@SFAID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@IsdUserID", SqlDbType.Int));



                    command.Parameters[0].Value = model.CustomerRef;
                    command.Parameters[1].Value = model.CustomerUserID;
                    command.Parameters[2].Value = model.CustomerID;
                    command.Parameters[3].Value = model.CountryID;
                    command.Parameters[4].Value = model.CurrencyID;
                    command.Parameters[5].Value = model.SupplierID;
                    command.Parameters[6].Value = model.CustomerUserID;
                    command.Parameters[7].Value = model.IsdUserID;


                    command.Parameters.Add("@PartQuoteID", SqlDbType.VarChar, 30);
                    command.Parameters["@PartQuoteID"].Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    partquoteId = Convert.ToInt32(command.Parameters["@PartQuoteID"].Value.ToString());

                }
                return partquoteId;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>InsertPartQuote \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return 0;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QuoteID"></param>
        /// <param name="dsComments"></param>
        /// <param name="quoteType"></param>
        private void NotifyQuoteRequest(int QuoteID, DataSet dsComments, string quoteType)
        {
            string sMailFrom = null;
            string sMailBody = null;
            string sMailTo = null;
            string sMailCC = null;
            string sMailBCC = null;
            string sMailSubject = null;
            string CustomerID = null;
            string CountryID = null;
            string SFAID = null;
            string MissingEmail = null;
            string EmailSignature = null;
            try
            {

                EmailSignature =
                    "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:";
                EmailSignature = EmailSignature + Environment.NewLine + Environment.NewLine;
                EmailSignature = EmailSignature + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                 Environment.NewLine + "fax : 201-408-5275";
                EmailSignature = EmailSignature + Environment.NewLine + Environment.NewLine;
                EmailSignature = EmailSignature + "GlobalServe ONESOURCE: https://gsonesource.global-serve.com";


                var partQuoteEmail = quoterequest.GetPrimeContractorEmail();

                MissingEmail = partQuoteEmail;


                var custromerInfo = ((quoteType == "newquote")
                    ? quoterequest.GetCustomerInfo(QuoteID)
                    : quoterequest.GetCustomerInfo_OldQuote(QuoteID));

                CustomerID = custromerInfo.CustomerID.ToString();
                CountryID = custromerInfo.CountryID.ToString();
                SFAID = custromerInfo.SFAID.ToString();

                if (!string.IsNullOrEmpty(custromerInfo.CustomerEmail))
                {
                    sMailFrom = custromerInfo.CustomerEmail;
                }
                else
                {
                    sMailFrom = MissingEmail;
                }

                sMailBCC = sMailFrom;


                if (string.IsNullOrEmpty(custromerInfo.UserEmail))
                {
                    sMailTo = "";
                }
                else
                {
                    sMailTo = custromerInfo.UserEmail;
                }
                if (sMailTo.Length == 0)
                {
                    sMailTo = MissingEmail;
                }

                if (string.IsNullOrEmpty(custromerInfo.NetpQuoteEmail))
                {
                    sMailCC = "";
                }
                else
                {
                    sMailCC = custromerInfo.NetpQuoteEmail;
                }

                sMailSubject = "GlobalServe ONESOURCE - New Quote Request - (" + custromerInfo.CustomerName + ") - " +
                               QuoteID;

                sMailBody = "Customer: " + custromerInfo.CustomerName + Environment.NewLine;
                sMailBody = sMailBody + "Requested By: " + custromerInfo.UserName + Environment.NewLine +
                            Environment.NewLine;

                sMailBody = sMailBody + "Thank you for placing the following quote request on GlobalServe ONESOURCE" +
                            Environment.NewLine + Environment.NewLine;

                sMailBody = sMailBody + "Quote #: " + QuoteID + Environment.NewLine;
                sMailBody = sMailBody + "Request Date: " + custromerInfo.CreateDate + Environment.NewLine;
                sMailBody = sMailBody + "Country: " + custromerInfo.CountryName + Environment.NewLine;
                sMailBody = sMailBody + "Quote Reference: " + custromerInfo.CustomerReference + Environment.NewLine +
                            Environment.NewLine;
                if ((dsComments != null && dsComments.Tables.Count != 0 && dsComments.Tables[0].Rows.Count > 0))
                {
                    if (dsComments.Tables[0].Rows[0].ItemArray[3].ToString().Length > 0)
                    {
                        sMailBody = sMailBody + "Comments: ";
                        foreach (DataRow drow in dsComments.Tables[0].Rows)
                        {
                            sMailBody = sMailBody + drow[3].ToString().Replace("<br />", "") + Environment.NewLine +
                                        Environment.NewLine;
                        }
                    }

                }
                //245 is Mars
                if (Convert.ToDouble(CustomerID) == 245)
                {
                    sMailBody = sMailBody +
                                "To view the status of the quote request, if you are an eBuy user please logon to GlobalServe ONESOURCE through your eBuy system. If you are a direct GlobalServe ONESOURCE user please log onto https://gsonesource.global-serve.com." +
                                Environment.NewLine + Environment.NewLine;
                }
                else
                {
                    sMailBody = sMailBody +
                                "Please logon to https://gsonesource.global-serve.com to view the status of the quote request." +
                                Environment.NewLine + Environment.NewLine;
                }

                sMailBody = sMailBody + EmailSignature;


                OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(sMailTo, sMailFrom, sMailCC, "", sMailBody, sMailSubject);
                NotifyQuoteRequestToSupplier(QuoteID, dsComments, quoteType, custromerInfo, sMailFrom);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException("Notify Quote Request Supplier=>From(" + sMailFrom + "),To(" + sMailTo + ")=> Exception" + ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("Notify Quote Request =>From(" + sMailFrom + "),To(" + sMailTo + ")=>" + ex.InnerException.ToString(), routeData);
                }
            }

        }

        private void NotifyQuoteRequestToSupplier(int QuoteID, DataSet dsComments, string quoteType, sp_GetCustomerInfo_R_Result custromerInfo, string sMailFrom)
        {
            string sMailBody = null;
            string sMailTo = null;
            string sMailCC = null;
            string sMailBCC = null;
            string sMailSubject = null;
            string CustomerID = null;
            string CountryID = null;
            string SFAID = null;
            string MissingEmail = null;
            string EmailSignature = null;
            try
            {


                sMailCC = sMailFrom;

                EmailSignature =
                    "Thank you for using GlobalServe ONESOURCE. If you need to contact GlobalServe, please call or fax the Customer Center at:";
                EmailSignature = EmailSignature + Environment.NewLine + Environment.NewLine;
                EmailSignature = EmailSignature + "Americas" + Environment.NewLine + "phone: +1 (914) 729 6800" +
                                 Environment.NewLine + "fax : 201-408-5275";
                EmailSignature = EmailSignature + Environment.NewLine + Environment.NewLine;
                EmailSignature = EmailSignature + "GlobalServe ONESOURCE: https://onesource.global-serve.com";

                if (string.IsNullOrEmpty(custromerInfo.SupplierEmail))
                {
                    sMailTo = "";
                }
                else
                {
                    sMailTo = custromerInfo.SupplierEmail;
                }
                sMailSubject = "GlobalServe ONESOURCE - New Quote Request - (" + custromerInfo.CustomerName + ") - " +
                              QuoteID;
                sMailBody = "GSP: " + custromerInfo.SFAName + Environment.NewLine;
                sMailBody = sMailBody + "Customer: " + custromerInfo.CustomerName + Environment.NewLine;
                sMailBody = sMailBody + "Requested By: " + custromerInfo.UserName + Environment.NewLine +
                            Environment.NewLine;
                sMailBody = sMailBody + "The following quote request comes from GlobalServe ONESOURCE" +
                            Environment.NewLine + Environment.NewLine;

                sMailBody = sMailBody + "Quote #: " + QuoteID + Environment.NewLine;
                sMailBody = sMailBody + "Request Date: " + custromerInfo.CreateDate + Environment.NewLine;
                sMailBody = sMailBody + "Country: " + custromerInfo.CountryName + Environment.NewLine;
                sMailBody = sMailBody + "Quote Reference: " + custromerInfo.CustomerReference + Environment.NewLine +
                            Environment.NewLine;
                if ((dsComments != null && dsComments.Tables.Count != 0 && dsComments.Tables[0].Rows.Count > 0))
                {
                    if (dsComments.Tables[0].Rows[0].ItemArray[3].ToString().Length > 0)
                    {
                        sMailBody = sMailBody + "Comments: ";
                        foreach (DataRow drow in dsComments.Tables[0].Rows)
                        {
                            sMailBody = sMailBody + drow[3].ToString().Replace("<br />", "") + Environment.NewLine +
                                        Environment.NewLine;
                        }
                    }

                }
                sMailBody = sMailBody +
                                "Please logon to https://onesource.global-serve.com to view the status of the quote request." +
                                Environment.NewLine + Environment.NewLine;
                var sfaCustomerContact = quoterequest.GetSFACustomerContactEmails(custromerInfo.SFAID, custromerInfo.CustomerID, custromerInfo.CountryID);


                foreach (var item in sfaCustomerContact)
                {
                    if (!string.IsNullOrEmpty(item.Email))
                    {
                        if (string.IsNullOrEmpty(sMailTo))
                        {
                            sMailTo = sMailTo + item.Email;
                        }
                        else
                        {
                            sMailTo = sMailTo + ";" + item.Email;
                        }
                    }
                }

                OneSourceAdminCore.Helper.StaticHelper.SendEmailMessage(sMailTo, sMailFrom, sMailCC, "", sMailBody, sMailSubject);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException("Notify Quote Request Supplier=>From(" + sMailFrom + "),To(" + sMailTo + ")=> Exception" + ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("Notify Quote Request Supplier=>From(" + sMailFrom + "),To(" + sMailTo + ")=> Exception" + ex.InnerException.ToString(), routeData);
                }
            }
        }

    

    }
}

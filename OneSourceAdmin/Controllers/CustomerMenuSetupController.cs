﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class CustomerMenuSetupController : BaseController
    {

        private CustomerMenuService menuService;
        public CustomerMenuSetupController()
        {
            menuService = new CustomerMenuService();
        }


        public ActionResult Index()
        {
            try
            {
                var model = menuService.GetActiveCustomersWithMenu();
                return View(model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        public JsonResult UpdateCustomerMenu(int menuId, string menuName, int customerId, bool isRestricted)
        {
            try
            {
                var data = menuService.UpdateCustomerMenuRestriction(customerId,menuId,menuName,isRestricted,adminSetting.UserID);
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
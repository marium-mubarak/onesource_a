﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    public class SupplierSettingsController : BaseController
    {

        private SFAsPresenter service;
        public SupplierSettingsController()
        {
            service = new SFAsPresenter();
        }

        // GET: SupplierSettings
        public ActionResult SupplierRoleName()
        {
            try
            {
                var model = new SupplierRoleModel();

                model.SfaList = service.GetActiveSuppliers();
                model.SfaDetailNameList = service.GetSfaDetailNameList();
                return View("SupplierRoleNameView", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetSfaNameDetails(int sfaId)
        {
            try
            {
                var details = service.GetSfaNameDetailsBySfaId(sfaId);
                if (details != null)
                {
                    return Json(details, JsonRequestBehavior.AllowGet);
                }
                return Json("0", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateSfaDetails(int sfaId, string sfaName)
        {
            try
            {
                if (sfaId > 0 && !string.IsNullOrWhiteSpace(sfaName))
                {
                    var chkAdd = service.UpdateSfaDetails(sfaId, sfaName);
                    if (chkAdd)
                    {
                        return Json("1", JsonRequestBehavior.AllowGet);
                    }

                }

                return Json("0", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
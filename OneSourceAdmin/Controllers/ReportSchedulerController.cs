﻿using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Entity;
using Newtonsoft.Json;
using System.Data;

namespace OneSourceAdmin.Controllers
{
    public class ReportSchedulerController : BaseController
    {
        CurrencyPresenter currencyService;
        CustomerPresenter customerService;
        ReportSchedulerPresenter schedulerService;
        SLAReportPresenter slaReportService;
        CountryPresenter countryService;
        AdminMenuService adminMenuService;
        ProcurementReportsPresenter procurementService;
        SupportReportsPresenter supportReportService;
        AssetReportsPresenter assetReportService;
        AssetDispositionPresenter assetDisposition;
        SFAsPresenter sfa;
        PriceDeskReportsPresenter pricedeskReportsPresenter;

        public ReportSchedulerController()
        {
            currencyService = new CurrencyPresenter();
            schedulerService = new ReportSchedulerPresenter();
            customerService = new CustomerPresenter();
            countryService = new CountryPresenter();
            slaReportService = new SLAReportPresenter();
            adminMenuService = new AdminMenuService();
            procurementService = new ProcurementReportsPresenter();
            supportReportService = new SupportReportsPresenter();
            assetReportService = new AssetReportsPresenter();
            assetDisposition = new AssetDispositionPresenter();
            sfa = new SFAsPresenter();
            pricedeskReportsPresenter = new PriceDeskReportsPresenter();
        }


        public ActionResult Index()
        {
            Session["mainModel"] = null;
            var model = new ReportSchedulerModel();
            model.mainModel = new tblReportSchedulerSetting();
            model.ReportTypes = schedulerService.GetAllReportsSubMenu();
            model.ReportTypes.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });
            model.ReportSettingsList = schedulerService.GetAllReportSchedulerSettings();
            if (TempData["Error"] != null)
            {
                ViewBag.Message = TempData["Error"].ToString();
            }
            return View(model);
        }

        public JsonResult ReportTypeSubmission(tblReportSchedulerSetting model)
        {
            Session["mainModel"] = model;
            return null;
        }

        /// <summary>
        /// Report Detail Page, Show filters according to the selected report and type (SUNNY GOHAR : 6th Aug 2018)
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportDetail()
        {
            try
            {
                if (Session["mainModel"] != null)
                {
                    var model = new ReportSchedulerModel();
                    model.mainModel = (tblReportSchedulerSetting)Session["mainModel"];

                    var route = adminMenuService.GetMenuById(Convert.ToInt32(model.mainModel.ReportId));
                    ViewBag.ReportName = route.MenuName;
                    string name = route.MenuName.ToLower().Trim().Replace(" ", "");
                    var parentReport = adminMenuService.GetMenuById(Convert.ToInt32(model.mainModel.ReportTypeId));
                    string parentName = parentReport.MenuName.ToLower().Trim().Replace(" ", "");
                    Session["routeName"] = name;
                    Session["parentReportName"] = parentName;
                    model.ListCurrencies = currencyService.GetCurrencies();
                    model.ListCustomers = customerService.GetCustomersWithPriority();
                    model.ListCustomers.Insert(0, new CustomerCompanyClass { CustomerId = 0, CustomerName = "--Select--", ParentCompanyId = 0 });

                    //for specific reports not having prefilled date time ( SUNNY GOHAR : 6th Aug 2018 )
                    if (Session["routeName"].ToString() != "revenuebycountry,manufacturer"
                        && Session["routeName"].ToString() != "assetdepreciationsummary"
                        && Session["routeName"].ToString() != "assetsummaryreport"
                        && Session["routeName"].ToString() != "quotesbysupplierreport"
                        && Session["routeName"].ToString() != "quotescountbyaccountmanager"
                        && Session["routeName"].ToString() != "quotesbystatusreport"
                        && Session["routeName"].ToString() != "partdetailreport"
                        && Session["routeName"].ToString() != "partdetailwithasset"
                        && Session["routeName"].ToString() != "sla-gsppricing-newquotes" && Session["routeName"].ToString() != "sla-gsppricing-oldquotes")
                    {
                        model.StartDate = DateTime.Now.AddMonths(-6).ToString("MM/dd/yyyy");
                        model.EndDate = DateTime.Now.ToString("MM/dd/yyyy");
                    }

                    //for specific reports having manufacturer dropdown ( SUNNY GOHAR : 6th Aug 2018 )
                    if (Session["routeName"].ToString() == "deliverymetricpc&server" || Session["routeName"].ToString() == "deliverymetricpc" || Session["routeName"].ToString() == "deliverymetricservers" || Session["routeName"].ToString() == "deliverymetricperipherals"
                    || Session["routeName"].ToString() == "orderacceptance" || Session["routeName"].ToString() == "quoteresponse" || Session["routeName"].ToString() == "assetsbymanufacturer")
                    {
                        model.ListManufacturers = slaReportService.GetManufacturers();
                    }

                    //for specific reports having below dropdowns ( SUNNY GOHAR : 6th Aug 2018 )
                    if (Session["routeName"].ToString() == "assetsummaryreport")
                    {
                        model.AssetDispositionCustomers = assetDisposition.GetCustomers();
                        model.ListAssetTypes = assetDisposition.GetAssetTypes();
                        model.ListProductCategories = assetDisposition.GetProductCategories();

                        model.ListCountries.Insert(0, new SelectListItem { Text = "--Select--", Value = "-1" });
                        model.ListRegions.Insert(0, new SelectListItem { Text = "--Select--", Value = "-1" });
                    }

                    //for specific reports having suppliers dropdown ( SUNNY GOHAR : 6th Aug 2018 )
                    if (Session["routeName"].ToString() == "quotesbysupplierreport")
                    {
                        model.Suppliers = sfa.GetSFAs().OrderBy(x => x.SFAName).Select(s => new Suppliers { SupplierName = s.SFAName ?? "-", SupplierId = s.SFAID }).ToList();
                        model.Suppliers.Insert(0, new Suppliers { SupplierId = 0, SupplierName = "--Select--" });
                    }

                    //for specific reports having account manager dropdown ( SUNNY GOHAR : 6th Aug 2018 )
                    if (Session["routeName"].ToString() == "quotescountbyaccountmanager")
                    {
                        var _accounts = pricedeskReportsPresenter.GetUserWithInfo().Where(x => x.UserGroupID == 20).OrderBy(x => x.UserName).ToList(); // added by sunny gohar

                        foreach (var item in _accounts)
                        {
                            model.AccountManagers.Add(new AccountManager { AccountManagerId = item.UserID, AccountManagerName = item.UserName });
                        }
                        model.AccountManagers.Add(new AccountManager { AccountManagerId = 0, AccountManagerName = "--Select--" });
                    }
                    if (Session["routeName"].ToString() == "partdetailreport" || Session["routeName"].ToString() == "partdetailwithasset")
                    {
                        model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                        {
                            Text = s.CustomerName,
                            Value = s.CustomerId.ToString()
                        }).ToList();
                    }

                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// Saves the report settings (filters) if the records for selected filters are found
        /// Otherwise returns 'No Records Found' type of message and doesn't save the record (SUNNY GOHAR : 6th Aug 2018)   
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SubmitSchedule(ReportSchedulerModel model)
        {
            try
            {
                var parentReport = adminMenuService.GetMenuById(Convert.ToInt32(model.mainModel.ReportTypeId));
                var childReport = adminMenuService.GetMenuById(Convert.ToInt32(model.mainModel.ReportId));
                model.StartDate = model.mainModel.StartDate;
                model.EndDate = model.mainModel.EndDate;
                DataTable dt = new DataTable();
                if (parentReport.MenuName == "Procurement")
                {
                    if (Session["routeName"].ToString() == "preferredsupplierbycountry")
                    {
                        dt = new ProcurementReportsController().GetDataTable(model.mainModel.CustomerIds, "0,1,2,3,4,5,6,7");
                    }
                    else if (Session["routeName"].ToString() == "partdetailreport")
                    {
                        //with one column to just check if record is available (Sunny Gohar : 30th July 2018)
                        dt = new ProcurementReportsController().GetPartDetailDataTable(model.mainModel.CustomerIds,
                            "", model.mainModel.CurrencyIds, model.StartDate, model.EndDate, "0");
                    }
                    else if (Session["routeName"].ToString() == "partdetailwithasset")
                    {
                        //with one column to just check if record is available (Sunny Gohar : 30th July 2018)
                        dt = new ProcurementReportsController().GetPartDetailWithAssetDataTable(model.mainModel.CustomerIds,
                           "", model.mainModel.CurrencyIds, model.StartDate, model.EndDate);
                    }
                    else if (Session["routeName"].ToString() == "customerordersreport")
                    {
                        var list = new ProcurementReportsController().GetCustomerOrdersList(Convert.ToInt32(model.mainModel.CustomerIds), model.StartDate, model.EndDate);
                        dt = ProcurementReportsController.ToDataTable<CustomerOrderReportModel>(list);
                    }
                    else if (Session["routeName"].ToString() == "quotesummaryreport")
                    {
                        var list = procurementService.getQuoteSummaryReportData(Convert.ToInt32(model.mainModel.CustomerIds), Convert.ToInt32(model.mainModel.CountryIds), model.StartDate, model.EndDate);
                        dt = ProcurementReportsController.ToDataTable(list);
                    }
                    else if (Session["routeName"].ToString() == "amountspentbyuser")
                    {
                        var list = procurementService.GetOrdersByUserId(Convert.ToInt32(model.mainModel.UserIds), model.StartDate, model.EndDate);
                        dt = ProcurementReportsController.ToDataTable(list);
                    }
                    else if (Session["routeName"].ToString() == "openordersreport")
                    {
                        dt = new ProcurementReportsController().GetOpenOrdersDataTable();
                    }
                    else if (Session["routeName"].ToString() == "openorderscomment")
                    {
                        dt = new ProcurementReportsController().GetOpenOrdersCommentReportDataTable();
                    }
                    else if (Session["routeName"].ToString() == "openquotesreport")
                    {
                        dt = new ProcurementReportsController().GetOpenQouteDataTable();
                    }
                    else
                    {
                        ProcurementReportModel procurementModel = new ProcurementReportModel();
                        procurementModel.StartDate = model.StartDate;
                        procurementModel.EndDate = model.EndDate;
                        procurementModel.CustomerId = Convert.ToInt32(model.mainModel.CustomerIds);
                        procurementModel.CurrencyId = model.mainModel.CurrencyIds;
                        procurementModel.CountryIDs = model.mainModel.CountryIds;
                        procurementModel.OrderStatus = model.mainModel.OrderStatus;

                        dt = procurementService.GetProcurementReportData(model.mainModel.ReportId.ToString(), childReport.MenuName, procurementModel);
                    }
                }
                else if (parentReport.MenuName == "Support")
                {
                    var companyId = 0;
                    if (model.mainModel.CustomerIds != null)
                    {
                        companyId = assetReportService.GetCompanyIdByCustomerId(Convert.ToInt32(model.mainModel.CustomerIds));
                    }

                    SupportReportModel supportModel = new SupportReportModel();
                    supportModel.StartDate = model.StartDate;
                    supportModel.EndDate = model.EndDate;
                    supportModel.CompanyId = companyId;
                    supportModel.CountryIDs = model.mainModel.CountryIds;
                    supportModel.RequestTypeIDs = model.mainModel.RequestTypes;

                    dt = supportReportService.GetReportData(model.mainModel.ReportId.ToString(), childReport.MenuName, supportModel);
                }

                else if (parentReport.MenuName == "SLAReport")
                {
                    SLAReportModel slaModel = new SLAReportModel();
                    slaModel.StartDate = model.StartDate;
                    slaModel.EndDate = model.EndDate;
                    slaModel.CustomerIds = model.mainModel.CompanyIds;
                    slaModel.ManufacturerIds = model.mainModel.ManufacturerIds;
                    slaModel.CountryIds = model.mainModel.CountryIds;
                    slaModel.CategoryIds = model.mainModel.ProductCategoryIds;
                    slaModel.BusinessSegments = model.mainModel.BusinessSegments;
                    slaModel.RegionIds = model.mainModel.RegionIds;

                    dt = slaReportService.GetReportData(model.mainModel.ReportId.ToString(), childReport.MenuName, "Summary", slaModel);
                }
                else if (parentReport.MenuName == "Asset")
                {

                    var companyId = 0;
                    if (model.mainModel.CustomerIds != null)
                    {
                        companyId = assetReportService.GetCompanyIdByCustomerId(Convert.ToInt32(model.mainModel.CustomerIds));
                    }

                    AssetReportModel assetModel = new AssetReportModel();
                    assetModel.StartDate = model.StartDate;
                    assetModel.EndDate = model.EndDate;
                    assetModel.CustomerId = Convert.ToInt32(model.mainModel.CustomerIds);
                    assetModel.CompanyId = companyId;
                    assetModel.CountryIDs = model.mainModel.CountryIds;
                    assetModel.AddressIDs = model.mainModel.LocationIds;
                    assetModel.ManufacturerIDs = model.mainModel.ManufacturerIds;
                    //var assetReportModel = new AssetReportModel();
                     //changed from parameters to model due to session things discussed with danyal and have to work on it for report scheduler 
                    //dt = assetReportService.GetAssetReportData(childReport.MenuName, assetModel);
                }
                else if (parentReport.MenuName == "AssetDisposition")
                {
                    //SubType = Main
                    AssetDispositionModel assetDispositionModel = new AssetDispositionModel();
                    assetDispositionModel.StartDate = model.StartDate;
                    assetDispositionModel.EndDate = model.EndDate;
                    assetDispositionModel.CustomerId = model.mainModel.CustomerIds;
                    assetDispositionModel.CountryIds = model.mainModel.CountryIds;
                    assetDispositionModel.AssetTypes = model.mainModel.AssetTypeIds;
                    assetDispositionModel.PartCategoryIds = model.mainModel.PartCategoryIds;
                    assetDispositionModel.RegionCountry = model.mainModel.RegionCountry;
                    assetDispositionModel.RegionIds = model.mainModel.RegionIds;
                    assetDispositionModel.SubType = "Main";
                    dt = GetAssetDispositionDataTable(assetDispositionModel, childReport.MenuName);
                }
                else if (parentReport.MenuName == "PriceDesk")
                {
                    //SubType = Main
                    PriceDeskReportModel priceDeskModel = new PriceDeskReportModel();
                    priceDeskModel.StartDate = model.StartDate;
                    priceDeskModel.EndDate = model.EndDate;
                    priceDeskModel.SupplierId = Convert.ToInt32(model.mainModel.SupplierIds);
                    priceDeskModel.QuoteTypeId = Convert.ToInt32(model.mainModel.QuoteTypeId);
                    priceDeskModel.Status = model.mainModel.Status;
                    priceDeskModel.AccountManagerId = Convert.ToInt32(model.mainModel.AccountManagerId);
                    dt = pricedeskReportsPresenter.GetPriceDeskReportData(childReport.MenuName, priceDeskModel);
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        model.mainModel.CreateDate = DateTime.Now.ToShortDateString();
                        model.mainModel.CreatedBy = Convert.ToInt32(Session["UserID"]);
                        model.mainModel.IsActive = true;

                        var inserted = schedulerService.NewSchedule(model.mainModel);
                        if (inserted)
                        {
                            return Json(new { msg = "success" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { msg = "Something went wrong, please try again later" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return Json(new { msg = "No records found, please change your search criteria" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(new { msg = "Something went wrong, please try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets Sub Reports by reports Id and fills the dropdown for sub report (SUNNY GOHAR : 6th Aug 2018)
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public JsonResult GetSubReportsByReportId(int reportId)
        {
            try
            {
                var reports = schedulerService.GetReportItemsByReportId(reportId);
                var reportsList = reports.Select(x => new
                {
                    Value = x.Key.ToString(),
                    Text = x.Value
                }).ToList();

                return Json(reportsList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Gets Product Categoires by Customer Id (SUNNY GOHAR : 6th Aug 2018)
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetProductCategoryByCustomerID(int customerId)
        {
            try
            {
                var categories = slaReportService.GetProductCategoryByCustomerID(customerId);
                if (categories != null)
                {
                    if (Session["routeName"].ToString() == "deliverymetricpc&server")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = (x.Value == "Desktops" || x.Value == "Laptops" || x.Value == "Servers") ? true : false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }
                    else if (Session["routeName"].ToString() == "deliverymetricpc")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = (x.Value == "Desktops" || x.Value == "Laptops") ? true : false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }
                    else if (Session["routeName"].ToString() == "deliverymetricservers")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = (x.Value == "Servers") ? true : false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }

                    else if (Session["routeName"].ToString() == "deliverymetricperipherals")
                    {
                        var categoriesList = categories.Select(x => new
                        {
                            Value = x.Key.ToString(),
                            Text = x.Value,
                            isSelected = false
                        }).ToList();
                        return Json(categoriesList.ToArray(), JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets Report Scheduler Setting Details by its Id (SUNNY GOHAR : 6th Aug 2018)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetSettingDetailById(int id)
        {
            try
            {
                var model = new ReportSchedulerModel();
                model.ReportSettings = schedulerService.GetReportSchedulerSettingById(id);
                
                if (model.ReportSettings == null)
                {
                    ViewBag.Message = "No Records Found";
                }
                model.mainModel = new tblReportSchedulerSetting();
                model.ReportTypes = schedulerService.GetAllReportsSubMenu();
                model.ReportTypes.Insert(0, new GenericList { Key = 0, Value = "--Select--", Count = 1 });

                if (model.ReportSettings != null)
                {

                    if (model.ReportSettings.Currencies != null && model.ReportSettings.Currencies != "")
                    {
                        var selectedCurrencies = schedulerService.GetSelectedCurrencies(id);

                        model.SelectedCurrencies = currencyService.GetCurrencies().Select(s => new SelectListItem
                        {
                            Text = s.Value,
                            Value = s.Key.ToString(),
                            Selected = selectedCurrencies.Contains(s.Key.ToString()) ? true : false
                        }).OrderByDescending(x => x.Selected == true).ToList();
                    }
                    if (model.ReportSettings.Manufacturers != null && model.ReportSettings.Manufacturers != "")
                    {
                        var selectedManufacturers = schedulerService.GetSelectedManufacturers(id);
                        model.SelectedManufacturers = slaReportService.GetManufacturers().Select(s => new SelectListItem
                        {
                            Text = s.Value,
                            Value = s.Key.ToString(),
                            Selected = selectedManufacturers.Contains(s.Key.ToString()) ? true : false
                        }).OrderByDescending(x => x.Selected == true).ToList();
                    }
                    if (model.ReportSettings.CustomerIds != null && model.ReportSettings.CustomerIds != "")
                    {
                        var selectedCustomers = schedulerService.GetSelectedCustomers(id);
                        model.ListPartDetailCustomers = customerService.GetCustomersWithPriority().Select(s => new SelectListItem
                        {
                            Text = s.CustomerName,
                            Value = s.CustomerId.ToString(),
                            Selected = selectedCustomers.Contains(s.CustomerId.ToString()) ? true : false
                        }).OrderByDescending(x => x.Selected == true).ToList();

                        if (!model.ReportSettings.CustomerIds.Contains(",") && model.ReportSettings.Locations != null && model.ReportSettings.Locations != "")
                        {
                            var selectedLocations = schedulerService.GetSelectedLocations(id);

                            model.ListLocations = assetReportService.GetLocationsByCustomerId(Convert.ToInt32(model.ReportSettings.CustomerIds)).Select(s => new SelectListItem
                            {
                                Value = s.CustomerAddressID.ToString(),
                                Text = s.CondensedAddress,
                                Selected = selectedLocations.Contains(s.CustomerAddressID.ToString()) ? true : false
                            }).OrderByDescending(x => x.Selected == true).ToList();

                            if (model.ReportSettings.Companies != null && model.ReportSettings.Companies != "")
                            {
                                var selectedCompanies = schedulerService.GetSelectedCompanies(id);
                                var companies = slaReportService.GetSubcompaniesByCustomerID(Convert.ToInt32(model.ReportSettings.CustomerIds));
                                if (companies != null)
                                {
                                    model.ListCompanies = companies.Select(s => new SelectListItem
                                    {
                                        Text = s.Value,
                                        Value = s.Key.ToString(),
                                        Selected = selectedCompanies.Contains(s.Key.ToString()) ? true : false
                                    }).OrderByDescending(x => x.Selected == true).ToList();
                                }

                            }
                        }
                        else if (model.ReportSettings.Locations != null && model.ReportSettings.Locations != "")
                        {
                            model.ListLocations.Insert(0, new SelectListItem { Value = 0.ToString(), Text = "All Selected" });
                        }
                    }
                    if (model.ReportSettings.CountryIds != null && model.ReportSettings.CountryIds != "")
                    {
                        var countries = countryService.GetAllCountries();
                        var selectedCountries = schedulerService.GetSelectedCountries(id);
                        model.ListCountries = countries.Select(s => new SelectListItem
                        {
                            Text = s.Value,
                            Value = s.Key.ToString(),
                            Selected = selectedCountries.Contains(s.Key.ToString()) ? true : false
                        }).OrderByDescending(x => x.Selected == true).ToList();
                    }
                    if (model.ReportSettings.Suppliers != null && model.ReportSettings.Suppliers != "")
                    {
                        var selectedSuppliers = schedulerService.GetSelectedSuppliers(id);
                        model.SelectedSuppliers = sfa.GetSFAs().OrderBy(x => x.SFAName).Select(s => new SelectListItem
                        {
                            Text = s.SFAName,
                            Value = s.SFAID.ToString(),
                            Selected = selectedSuppliers.Contains(s.SFAID.ToString()) ? true : false
                        }).OrderByDescending(x => x.Selected == true).ToList();
                    }

                    if (model.ReportSettings.Categories != null && model.ReportSettings.Categories != "")
                    {
                        string[] categories = new string[] { };
                        if (model.ReportSettings.Report.ToString() == "Asset Summary Report")
                        {
                            categories = schedulerService.GetSelectedPartCategories(id);
                        }
                        else
                        {
                            categories = schedulerService.GetSelectedProductCategories(id);
                        }

                        model.ListProductCategories = assetDisposition.GetProductCategories().Select(s => new SelectListItem
                        {
                            Text = s.Text,
                            Value = s.Value.ToString(),
                            Selected = categories.Contains(s.Value.ToString()) ? true : false
                        }).OrderByDescending(x => x.Selected == true).ToList();
                    }
                    if (model.ReportSettings.Report.ToString() == "Asset Summary Report")
                    {
                        model.ReportSettings.Assets = null;
                    }

                }

                return View("ReportSettingDetail", model);
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }
        /// <summary>
        /// Removes Scheduler Settings by Id (SUNNY GOHAR : 6th Aug 2018)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveSettingDetailById(int id)
        {
            try
            {
                var deleted = schedulerService.RemoveSettingDetailById(id);
                if (deleted > 0)
                {
                    TempData["Error"] = "Setting Removed";
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        #region Utilities

        /// <summary>
        /// Asset Disposition Datatable Manipulation
        /// </summary>
        /// <param name="model"></param>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public DataTable GetAssetDispositionDataTable(AssetDispositionModel model, string reportName)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(model.AssetTypes))
                {
                    model.AssetTypes = model.AssetTypes.TrimEnd(',');
                }

                var partCategories = model.PartCategoryIds;


                if (partCategories == null || partCategories == "")
                {
                    var allCats = assetDisposition.GetProductCategories();
                    for (int i = 0; i < allCats.Count; i++)
                    {
                        if (i == 5)
                        {
                            model.CategoryNames = model.CategoryNames + allCats[i].Text + "...";
                            break;
                        }
                        else
                        {
                            model.CategoryNames = model.CategoryNames + allCats[i].Text + ",";
                        }
                    }

                    model.CategoryNames = model.CategoryNames.TrimEnd(',');
                }

                if (model.CountryIds == null || model.CountryIds == "")
                {
                    var allConts = assetDisposition.GetCountries(Convert.ToInt32(model.CustomerId));
                    for (int i = 0; i < allConts.Count; i++)
                    {
                        if (i == 5)
                        {
                            model.CountryNames = model.CountryNames + allConts[i].Text + "...";
                            break;
                        }
                        else
                        {
                            model.CountryNames = model.CountryNames + allConts[i].Text + ", ";
                        }
                    }
                    model.CountryNames = model.CountryNames.TrimEnd(',');
                }

                if (model.AssetTypes == null || model.AssetTypes == "")
                {
                    var allassets = assetDisposition.GetAssetTypes();
                    for (int i = 0; i < allassets.Count; i++)
                    {
                        if (i == 5)
                        {
                            model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + "...";
                            break;
                        }
                        else
                        {
                            model.AssetTypeNames = model.AssetTypeNames + allassets[i].Text + ",";
                        }
                    }
                    model.AssetTypeNames = model.AssetTypeNames.TrimEnd(',');
                }

                if (model.RegionNames != null && model.RegionNames != "")
                {
                    model.RegionNames = model.RegionNames.TrimEnd(',');
                }

                DataTable dt = null;
                if (model.RegionCountry == "CountryWise")
                {
                    var totalAssetDt = assetDisposition.GetTotalAssetsByCountry(reportName, model.CustomerId, model.CountryIds
                   , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                    model.TotalAssets = Convert.ToInt32(totalAssetDt.Rows[0].ItemArray.FirstOrDefault());
                }
                else
                {
                    var totalAssetDt = assetDisposition.GetTotalAssetsByRegion(reportName, model.CustomerId, model.RegionIds
                 , model.StartDate, model.EndDate, model.AssetTypes, partCategories);

                    model.TotalAssets = Convert.ToInt32(totalAssetDt.Rows[0].ItemArray.FirstOrDefault());
                }
                dt = assetDisposition.GetAssetDispositionReportData(reportName, model.CustomerId, model.CountryIds, model.RegionIds
                      , model.StartDate, model.EndDate, model.AssetTypes, partCategories, model.RegionCountry, model.SubType);

                if (dt == null)
                {
                    dt = new DataTable();
                }

                return dt;
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return new DataTable();
            }

        }

        #endregion
    }
}
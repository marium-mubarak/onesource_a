﻿using OneSourceAdminCore.Helper;
using OneSourceAdminCore.Presenter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.Controllers
{
    /// <summary>
    /// Date created : Oct 18 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for providing Customer's manufacturer detail"
    /// </summary>
    public class OstCustomerManufacturerController : BaseController
    {
        //
        // GET: /OstCustomerManufacturer/

        /// <summary>
        /// This Action is responsible for returning Index View of Customer's Manufacturer
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? customerId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CustomerManufacturerPresenter up = new CustomerManufacturerPresenter();
                    ViewBag.Message = TempData["Message"];
                    @ViewBag.AddEditAccess = isAddEditAllow("OsManufacturerSetup");
                    ViewBag.CustomerId = customerId > 0 ? customerId : null;
                    return View(up.GetCustomerManufacturers());
                    
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This action is responsible for updating the customer's manufacturer record
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="manufacturerIds"></param>
        /// <returns></returns>
        public ActionResult UpdateCustomerManufacturerSetting(string customerId, string manufacturerIds)
        {
            if (!string.IsNullOrEmpty(customerId))
            {
                CustomerManufacturerPresenter cm = new CustomerManufacturerPresenter();
                int count = cm.UpdateCustomerManufacturerRecord(customerId, manufacturerIds, Convert.ToInt32(Session["UserID"]));
                TempData["Message"] = count > 0 ? "Record successfully updated" : "No record was updated";

            }
            else
            {
                TempData["Message"] = "No record was update";
            }
            return RedirectToAction("Index",new { @customerId=customerId});
        }

        /// <summary>
        /// This action is responsible for providing Json record of customer's manufacturer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public JsonResult GetCustomerManufacturer(int customerId)
        {
            CustomerManufacturerPresenter cm = new CustomerManufacturerPresenter();
            List<long> result = cm.GetCustomerManufacturer(customerId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Catalog Setup Report by Adnan 21-june-2019    
        /// <summary>
        /// This Action is responsible for Excel Export Of catalog setup
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult ExportToExcel(FormCollection col)
        {
            try
            {
                var customerId = Convert.ToInt32(col["customerId"]);
            
                Response.Buffer = true;
                DataTable dt = null;
                CustomerManufacturerPresenter up = new CustomerManufacturerPresenter();
                if (customerId > 0)
                {
                    dt = GetManufacturerSetupReport(customerId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ReportHelper.ExportToExcel(dt, "Manufacturer Setup", Response);
                    }
                    else
                    {
                        ViewBag.Message = "Error";
                    }

                }

                return View("Index", up.GetCustomerManufacturers());
            }
            catch (Exception ex)
            {

                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }


        }

        public DataTable GetManufacturerSetupReport(int customerId)
        {
            try
            {
                DataTable dt = new DataTable();

                var efConnectionString = ConfigurationManager.ConnectionStrings["globalserveEntities"].ConnectionString;
                var builder = new EntityConnectionStringBuilder(efConnectionString);
                var regularConnectionString = builder.ProviderConnectionString;

                SqlConnection con = new SqlConnection(regularConnectionString);
                con.Open();

                using (SqlCommand command = new SqlCommand("sp_OneStreetGetManufacturerSetupRep_AdminR", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int));
                   

                    command.Parameters[0].Value = customerId;
                
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }


                    return dt;
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException("from -->>GetManufacturerSetupReport \n-->> " + ex.InnerException.ToString(), routeData);
                }
                return null;
            }


        }
        #endregion
    }
}
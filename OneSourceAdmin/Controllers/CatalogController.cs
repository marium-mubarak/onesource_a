﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using OneSourceAdminCore.Entity;
using OneSourceAdminCore.Model;
using OneSourceAdminCore.Presenter;

namespace OneSourceAdmin.Controllers
{
    /// <summary>   
    /// Date created : Feb 09 2016
    /// Created by : Muhammad Wasim (BSCE,MS-CSIT)
    /// This Controller Class is responsible for Adding, Editing, Listing of Catalog Alert"
    /// </summary>
    public class CatalogController : BaseController
    {
        /// <summary>
        /// This Action is responsible For providing View Of Catalog Alert (Alert>>Catalog)
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        CatalogPresenter catpresenter;
        public CatalogController()
        {
            catpresenter = new CatalogPresenter();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CatalogModel model = new CatalogModel();
                    CatalogPresenter cp = new CatalogPresenter();
                    model.CatalogAlerts = cp.GetCatalogAlert();
                    if (TempData["SkuResult"] != null)
                    {
                        model.ManufacturerPartId = TempData["SkuResult"].ToString().Split(';')[1];
                        model.ManufacturerSku = TempData["SkuResult"].ToString().Split(';')[0];
                        model.CustomerId = TempData["SkuResult"].ToString().Split(';')[2] == "0" ? null : TempData["SkuResult"].ToString().Split(';')[2];
                        model.Description = TempData["SkuResult"].ToString().Split(';')[3];
                    }
                    @ViewBag.AddEditAccess = isAddEditAllow("CatalogAlert");
                    model.CustomerList = cp.GetCustomerList();
                    ViewBag.Message = TempData["Message"];
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is responsible For Adding and Editing of Catalog Alert
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        [HttpPost]
        public ActionResult AddCatalogAlert(tblCatalogAlert mode, string description)
        {
            if (Session["UserID"] != null)
            {
                try
                {

                    if (!ValidateFields(mode)) return RedirectToAction("Index");

                    CatalogPresenter cp = new CatalogPresenter();
                    var dbCon = new globalserveEntities();
                    var chkCatalogAlert =
                        dbCon.tblCatalogAlerts.FirstOrDefault(x => x.CustomerId != null && x.CustomerId == mode.CustomerId && x.SKU == mode.SKU);
                    int count;


                    //For Edit
                    if (mode.CatalogAlertId != 0)
                    {
                        if (chkCatalogAlert != null && chkCatalogAlert.CatalogAlertId != mode.CatalogAlertId)
                        {
                            TempData["Message"] = "Alert already exist";
                            return RedirectToAction("Index");
                        }
                        count = cp.EditCatalogAlert(mode, description, Convert.ToInt32(Session["UserID"]));
                        if (count > 0) TempData["Message"] = "Alert has been edited";
                        return RedirectToAction("Index");
                    }

                    // check already exist
                    if (chkCatalogAlert != null)
                    {

                        TempData["Message"] = "Alert Already Exist";
                        return RedirectToAction("Index");

                    }

                    count = cp.AddCatalogAlert(mode, description, Convert.ToInt32(Session["UserID"]));
                    if (count > 0) TempData["Message"] = "Alert has been added";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        /// <summary>
        /// This Action is responsible For Casting Session of SearchResults which has been created in Search Action --Post Wala :)
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Search View if all Working Well
        /// </returns>


        [HttpGet]
        //  [Route("catalogsku")]
        public ActionResult Search()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["SearchResults"] != null)
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        if (Convert.ToString(Session["SearchResults"]) != "0")
                        {
                            CatalogModel model = new CatalogModel
                            {
                                CatalogSearchModel = (List<sp_getSearchCatalogSKU_R_Result>)Session["SearchResults"],
                                CustomerList = cp.GetCustomerList()
                            };
                            return View(model);
                        }
                        else
                        {
                            Session["AssetSearchResults"] = null;

                            CatalogModel model = new CatalogModel
                            {
                                CatalogSearchModel = cp.GetSearchCatalogSku(null, null),
                                CustomerList = cp.GetCustomerList()
                            };
                            return View(model);
                        }
                    }
                    else
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        CatalogModel model = new CatalogModel
                        {
                            CatalogSearchModel = cp.GetSearchCatalogSku(null, null),
                            CustomerList = cp.GetCustomerList()
                        };
                        return View(model);
                    }


                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        /// <summary>
        /// This Action is responsible For Getting SKU in order to add sku to catalog
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Search View if all Working Well
        /// </returns>


        [HttpPost]
        //[Route("catalogsku/{manufacturerSku}/{customerId}")]
        public ActionResult Search(string manufacturerSku, int customerId = 0)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CatalogPresenter cp = new CatalogPresenter();
                    CatalogModel model = new CatalogModel();
                    if (!string.IsNullOrEmpty(manufacturerSku) && customerId != 0)
                        model.CatalogSearchModel = cp.GetSearchCatalogSku(manufacturerSku, customerId);
                    else if (string.IsNullOrEmpty(manufacturerSku) && customerId != 0)
                    {
                        model.CatalogSearchModel = cp.GetSearchCatalogSku(null, customerId);
                    }
                    else if (!string.IsNullOrEmpty(manufacturerSku) && customerId == 0)
                    {
                        model.CatalogSearchModel = cp.GetSearchCatalogSku(manufacturerSku, null);
                    }
                    else
                    {
                        model.CatalogSearchModel = cp.GetSearchCatalogSku(null, null);
                    }
                    if (model.CatalogSearchModel != null && model.CatalogSearchModel.Count > 0)
                    {
                        Session["SearchResults"] = model.CatalogSearchModel;
                    }
                    else
                    {
                        Session["SearchResults"] = "0";
                    }
                    Session["SelectedCustomerId"] = customerId;
                    model.CustomerList = cp.GetCustomerList();
                    return View(model);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// This Action is was called when user select sku from search view of Catalog
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public JsonResult GetSku(string manufacturerSku, string manufacturerPartId, string customerId, string description)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(manufacturerSku) && !string.IsNullOrEmpty(manufacturerPartId))
                    {
                        string ncustomerId;
                        if (!string.IsNullOrEmpty(customerId))
                        {
                            ncustomerId = customerId;
                        }
                        else
                        {
                            ncustomerId = "0";
                        }

                        TempData["SkuResult"] = manufacturerSku + ";" + manufacturerPartId + ";" + ncustomerId + ";" + description;
                        return Json("1", JsonRequestBehavior.AllowGet);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }

        }
        /// <summary>
        /// This Action is responsible For Deleting selected catalog 
        /// </summary>
        /// <returns> 
        /// Return Error View in case of Exception
        /// Return to Login Action of Account Controller in case of session timeout
        /// Return Index View if all Working Well
        /// </returns>
        public ActionResult DeleteAlert(string id)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    int countryId;
                    if (!int.TryParse(id, out countryId))
                    {
                        return View("~/Views/Shared/ErrorPage.cshtml");
                    }
                    CatalogPresenter cp = new CatalogPresenter();
                    int count = cp.DeleteCatalogAlert(countryId);
                    if (count > 0) TempData["Message"] = "Alert has been deleted";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        /// <summary>
        /// This Action is responsible For Validating Data which has been post to AddCatalogAlert Action
        /// </summary>
        /// <returns> 
        /// Return boolean result
        /// </returns>
        public bool ValidateFields(tblCatalogAlert model)
        {
            if (string.IsNullOrEmpty(model.SKU))
            {
                TempData["Message"] = "Please Provide Sku";
                return false;
            }
            if (Convert.ToDateTime(model.StartDate).Date.Year == 1)
            {
                TempData["Message"] = "Please Provide Start Date";
                return false;
            }
            if (Convert.ToDateTime(model.EndDate).Date.Year > 1)
            {
                if (model.StartDate.Date > Convert.ToDateTime(model.EndDate).Date)
                {
                    TempData["Message"] = "Start Date must be less than End Date";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(model.Alert))
            {
                TempData["Message"] = "Please Provide Alert";
                return false;
            }

            return true;
        }

        #region SFA Catalog Tracking

        /// <summary>
        /// this action is responsible for  providing filter view
        /// </summary>
        /// <returns></returns>
        public ActionResult SFACatalogSearch()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        var model = cp.GetSFACatalogModel(Convert.ToInt32(Session["SFACountryID"]), Convert.ToInt32(Session["SFAID"]));
                        return View("~/Views/Catalog/SFA/SFACatalogSearch.cshtml", model);
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        ///this action is responsible for seaching by providing parameters
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchCatalogRequest(SFACatalogModel model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        var lst = cp.SearchCatalog(model, Convert.ToInt32(Session["SFAID"]));
                        model.SearchList = lst;
                        // model.CurrencyName = cp.GetCurrencyName(model.currencyId);
                        Session["SearchCatalogModel"] = model;
                        return RedirectToAction("SearchCatalogList");
                    }
                    else
                    {

                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        /// <summary>
        /// this action is responsible for providing catalog list
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchCatalogList()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CatalogPresenter cp = new CatalogPresenter();
                    if (Session["SearchCatalogModel"] != null)
                    {
                        var model = (SFACatalogModel)Session["SearchCatalogModel"];
                        ViewBag.CurrencyName = cp.GetCurrencyName(model.currencyId);
                        return View("~/Views/Catalog/SFA/CatalogList.cshtml", model.SearchList);
                    }
                    else
                    {
                        var model = (SFACatalogModel)Session["SearchCatalogModel"];
                        var data = cp.SearchCatalog(model, Convert.ToInt32(Session["SFAID"]));
                        model.SearchList = data;
                        ViewBag.CurrencyName = cp.GetCurrencyName(model.currencyId);
                        Session["SearchCatalogResult"] = model;
                        return View("~/Views/Catalog/SFA/CatalogList.cshtml", model.SearchList);
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for providing part detail view
        /// </summary>
        /// <param name="mfgId"></param>
        /// <returns></returns>
        [Route("part-detail/{mfgId}")]
        public ActionResult PartDetail(string mfgId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(mfgId))
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        return View("~/Views/Catalog/SFA/PartInfo.cshtml", cp.GetPartInfo(mfgId));
                    }
                    else
                    {
                        return RedirectToAction("SearchCatalogList");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        ///  this action is responsible for  providing filter view for domestic catalog
        /// </summary>
        /// <returns></returns>
        public ActionResult SFADomesticCatalogSearch()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        var model = cp.GetSFADomesticCatalogModel(Convert.ToInt32(Session["SFAID"]));
                        return View("~/Views/Catalog/SFA/SFADomesticCatalogSearch.cshtml", model);
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        ///this action is responsible for seaching by providing parameters
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchDomesticCatalogRequest(SFACatalogModel model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        var lst = cp.SearchDomesticCatalog(model, Convert.ToInt32(Session["SFAID"]));
                        model.SearchDomesticList = lst;
                        // model.CurrencyName = cp.GetCurrencyName(model.currencyId);
                        Session["SearchCatalogModel"] = model;
                        return RedirectToAction("SearchDomesticCatalogList");
                    }
                    else
                    {

                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }


        /// <summary>
        /// this action is responsible for providing catalog list
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchDomesticCatalogList()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CatalogPresenter cp = new CatalogPresenter();
                    if (Session["SearchCatalogModel"] != null)
                    {
                        var model = (SFACatalogModel)Session["SearchCatalogModel"];
                        return View("~/Views/Catalog/SFA/DomesticCatalogList.cshtml", model.SearchDomesticList);
                    }
                    else
                    {
                        var model = (SFACatalogModel)Session["SearchCatalogModel"];
                        var data = cp.SearchDomesticCatalog(model, Convert.ToInt32(Session["SFAID"]));
                        model.SearchDomesticList = data;
                        Session["SearchCatalogResult"] = model;
                        return View("~/Views/Catalog/SFA/DomesticCatalogList.cshtml", model.SearchDomesticList);
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// this action is responsible for updating domestic part
        /// </summary>
        /// <param name="partId"></param>
        /// <param name="custId"></param>
        /// <returns></returns>
        [Route("domestic-part/{partId}/{custId}")]
        public ActionResult EditDomesticPart(int partId, int custId)
        {
            try
            {
                var model = catpresenter.EditDomesticPartModel(Convert.ToInt32(Session["SFAID"]), custId, partId);
                ViewBag.Message = TempData["Message"];
                return View("~/Views/Catalog/SFA/EditDomesticPart.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for providing adding view domestic part
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="mfgPartId"></param>
        /// <returns></returns>
        [Route("add-domestic-part/{customerId}/{mfgPartId}")]
        public ActionResult AddDomesticPart(int customerId = 0, int mfgPartId = 0)
        {
            try
            {
                Session["CurrentSKU"] = null;
                ViewBag.Message = TempData["Message"];
                Session["CurrentCatCustId"] = customerId;
                var model = catpresenter.GetDomesticPartAddModel(customerId, mfgPartId, Convert.ToInt32(Session["SFAID"]));
                return View("~/Views/Catalog/SFA/AddDomesticPart.cshtml", model);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for setting country to catalog
        /// </summary>
        /// <param name="countries"></param>
        /// <param name="customerPartId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ActionResult CountrifyPart(string countries, int customerPartId, int customerId)
        {
            try
            {
                int count = catpresenter.CountrifyPart(countries, customerPartId, customerId, Convert.ToInt32(Session["SFAID"]), Convert.ToInt32(Session["UserID"]));
                TempData["Message"] = count > 0 ? "Your changes have been saved" : "No record was updated.";
                return RedirectToAction("EditDomesticPart", new { @partId = customerPartId, @custId = customerId });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for adding part
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddUpdateDomesticPart(DomesticPartModel model)
        {
            try
            {
                int Id = catpresenter.AddUpdateDomesticPart(model, Convert.ToInt32(Session["SFAID"]), Convert.ToInt32(Session["UserID"]));
                if (Id > 0)
                {
                    TempData["Message"] = "Your changes have been saved.";
                    return RedirectToAction("EditDomesticPart", new { @partId = model.CustomerPartID, @custId = model.CustomerID });
                }
                else
                {
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for searching manufacturer part
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        [Route("manufacturer-part-search/{sku}")]
        public ActionResult GetManufacturerPart(string sku = "")
        {
            try
            {
                if (Session["CurrentSKU"] == null)
                {
                    Session["CurrentSKU"] = sku == "0" ? null : sku;
                }
                if (Session["CurrentSKU"] != null && sku == "0")
                {
                    sku = Convert.ToString(Session["CurrentSKU"]);
                }
                var data = catpresenter.GetManufacturerPart(sku);
                ViewBag.CurrentSKU = sku;
                return View("~/Views/Catalog/SFA/MfgPartSearch.cshtml", data);
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for handle empty value for searching manufacturer
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public ActionResult GetManufacturerPartP(string sku = "")
        {
            try
            {
                sku = string.IsNullOrEmpty(sku) ? "-1" : sku;
                return RedirectToAction("GetManufacturerPart", new { @sku = sku });
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }

        /// <summary>
        /// this action is responsible for adding domestic category part
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public ActionResult AddDomesticCatPart(DomesticAddPartModel model)
        {
            try
            {
                if (model != null && model.MfgPartId != 0 && model.CustomerId != 0)
                {
                    var count = catpresenter.AddDomesticCatPart(model, Convert.ToInt32(Session["SFAID"]), Convert.ToInt32(Session["UserID"]), Convert.ToString(Session["ContactEmail"]));
                    if (count == -1)
                    {
                        TempData["Message"] = "A part with this customer, manufacturer and SKU already exists.";
                    }
                    else
                    {
                        TempData["Message"] = count > 0 ? "Record updated successfully" : "No record was updated successfullly";
                    }

                    return RedirectToAction("AddDomesticPart", new { @customerId = 0, @mfgPartId = 0 });
                }
                else
                {
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            catch (Exception ex)
            {
                var routeData = this.ControllerContext.RouteData;
                LogException(ex.Message, routeData);
                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException.ToString(), routeData);
                }
                return View("~/Views/Shared/ErrorPage.cshtml");
            }
        }


        /// <summary>
        /// this action is responsible for  providing filter view
        /// </summary>
        /// <returns></returns>
        public ActionResult SFACatalogBundleSearch()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        var model = cp.GetSFACatalogModel(Convert.ToInt32(Session["SFACountryID"]), Convert.ToInt32(Session["SFAID"]));
                        return View("~/Views/Catalog/SFA/SFABundleSearch.cshtml", model);
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult SearchCatalogBundleRequest(SFACatalogModel model)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (Session["UserGroupName"].ToString() == "SFA")
                    {
                        var lst = catpresenter.SearchCatalogBundleRequest(model);
                                                
                        model.SearchCatalogBundleList = lst;
                        Session["SearchCatalogBundleModel"] = model;
                        return RedirectToAction("SearchCatalogBundleList");

                    }
                    else
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        /// <summary>
        /// this action is responsible for providing catalog list
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchCatalogBundleList()
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    CatalogPresenter cp = new CatalogPresenter();
                    if (Session["SearchCatalogBundleModel"] != null)
                    {
                        var model = (SFACatalogModel)Session["SearchCatalogBundleModel"];
                        return View("~/Views/Catalog/SFA/CatalogBundleList.cshtml", model.SearchCatalogBundleList);
                    }
                    else
                    {
                        var model = (SFACatalogModel)Session["SearchCatalogBundleModel"];
                        
                        var data = catpresenter.SearchCatalogBundleRequest(model);
                        model.SearchCatalogBundleList = data;
                        Session["SearchCatalogBundleModel"] = model;
                        return View("~/Views/Catalog/SFA/CatalogBundleList.cshtml", model.SearchList);
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Route("catalog-bundle-detail/{catId}")]
        public ActionResult CatalogBundleDetail(int catId)
        {
            if (Session["UserID"] != null)
            {
                try
                {
                    if (catId>0)
                    {
                        CatalogPresenter cp = new CatalogPresenter();
                        return View("~/Views/Catalog/SFA/CatalogBundleInfo.cshtml", cp.GetCatalogBundleInfo(catId));
                    }
                    else
                    {
                        return RedirectToAction("SearchCatalogList");
                    }
                }
                catch (Exception ex)
                {
                    var routeData = this.ControllerContext.RouteData;
                    LogException(ex.Message, routeData);
                    if (ex.InnerException != null)
                    {
                        LogException(ex.InnerException.ToString(), routeData);
                    }
                    return View("~/Views/Shared/ErrorPage.cshtml");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
    }
}
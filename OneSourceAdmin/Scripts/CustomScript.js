﻿//track change made to form plugin
$.fn.extend({
    trackChanges: function () {
        $(":input", this).change(function () {
            $(this.form).data("changed", true);
        });
    },
    isChanged: function () {
        return this.data("changed");
    }
});
//end

$("select[idFor]").change(function () {
    var element = ("#" + $(this).attr("idFor"));
    $(element).val($(this).val());
});



$("select[valueFor]").change(function () {
    var element = ("#" + $(this).attr("valueFor"));
    $(element).val("");
    var selectedText = $('#' + $(this).attr("id") + " option:selected").text();
    if (selectedText.toLowerCase().indexOf("select") >= 0) {

    } else {
        $(element).val(selectedText);
    }
});

function StartLoader() {
    //for loader
    var $preloadHTML = $('<div class="loader"><img src="/Content/images/loading.gif"></div>');
    var $loadContainer = $("#adshowblock").css("background-color", "white").css("position", "fixed");
    $loadContainer.html($preloadHTML);
    //
}
function EndLoader() {
    //loader
    $("#adshowblock").html("");
    $("#adshowblock").removeAttr("style");
    //end loader
}

$('.searchGridResult').keyup(function () {

    var containSpans = $(".table span:contains('" + $(this).val() + "')");
    containSpans.each(function (e, t) {
        $(t).parent().parent().show();
    });
    var notContainSpans = $(".table span:not(:contains('" + $(this).val() + "'))");
    notContainSpans.each(function (e, t) {
        $(t).parent().parent().hide();
    });
});

jQuery.expr[':'].contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};




///TO SHOW HIDE LOADER
function showHideLoader(show) {
    if (show === 1) {
        //loader
        var preloadHtml = $('<div class="loader"><img src="/Content/images/loading.gif"></div>');
        var $loadContainer = $("#adshowblock").css("background-color", "white").css("position", "fixed").css("z-index", "99999999");
        $loadContainer.html(preloadHtml);
        ////loader end

    } else {
        //loader
        $("#adshowblock").html("");
        $("#adshowblock").removeAttr("style");
        //end loader
    }
}

function showHideLoaderMaster(show) {
    if (show == 1) {
        //loader
        var preloadHtml = $('<div class="loader"><img src="/Content/images/loading.gif"></div>');
        var $loadContainer = $("#adshowblockmaster").css("background-color", "white").css("position", "fixed").css("z-index", "999999999");
        $loadContainer.html(preloadHtml);
        ////loader end

    } else {
        //loader
        $("#adshowblockmaster").html("");
        $("#adshowblockmaster").removeAttr("style");
        //end loader
    }
}

function getStringDateFromJsonDate(jsonDate, serverDate) {

    var currentTime = new Date(parseInt(jsonDate.substring(jsonDate.lastIndexOf(')'), jsonDate.indexOf('(') + 1)))
    var month = currentTime.getMonth() + 1
    var day = currentTime.getDate()
    var year = currentTime.getFullYear()

    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;

    var secs = currentTime.getSeconds();
    secs = secs < 10 ? '0' + secs : secs;

    var strTime = hours + ':' + minutes + ':' + secs + ' ' + ampm;

    var dateCurrenct = new Date(month + "/" + day + "/" + year + " " + strTime);
    serverDate = new Date(serverDate);
    var timeDiff = Math.abs(dateCurrenct.getTime() - serverDate.getTime());
    var diffHour = Math.floor(timeDiff / (1000 * 3600));//for Server and

    hours = currentTime.getHours() - diffHour;
    ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'

    strTime = hours + ':' + minutes + ':' + secs + ' ' + ampm;

    var date = month + "/" + day + "/" + year + " " + strTime;

    return date;
}
function getMonthNameWithDate(jsonDate) {

    if (jsonDate != null) {
        var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
        ];
        var currentTime = new Date(parseInt(jsonDate.substring(jsonDate.lastIndexOf(')'), jsonDate.indexOf('(') + 1)))
        var month = currentTime.getMonth();
        var day = currentTime.getDate()
        return monthNames[month] + " " + day;
    }
    return "";
}
$(".allownumericwithoutdecimal").on("keypress keyup blur", function (event) {
    //$(this).val($(this).val().replace(/[^\d].+/, ""));
    //if ((event.which < 48 || event.which > 57)) {
    //    event.preventDefault();
    //}
    if ((event.which < 48 || event.which > 57) && event.which != 8 && event.which != 0 && !event.ctrlKey) {
        event.preventDefault();
    }

}).bind("paste", function (e) {
    e.preventDefault();
});

$(".allowNumWoutDecimalWPast").on("keypress keyup blur", function (event) {
    if ((event.which < 48 || event.which > 57) && event.which != 8 && event.which != 0 && !event.ctrlKey) {
        event.preventDefault();
    }
}).bind("paste", function (e) {
    var pastText = e.originalEvent.clipboardData.getData('text');
    var reg = /^\d+$/;
    if (!reg.test(pastText)) {
        e.preventDefault();
    }
});;

$(".allownumericwithhippen").on("keypress keyup blur", function (event) {

    //$(this).val($(this).val().replace(/[^\d-].+/, ""));
    if ((event.which < 48 || event.which > 57) && event.which != 45 && (event.which < 65 || event.which > 90) && (event.which < 97 || event.which > 122) && event.which != 8 && event.which != 0 && !event.ctrlKey) {

        event.preventDefault();
    }
}).bind("paste", function (e) {
    e.preventDefault();
});

$(".numericwithdecimal").on("keypress keyup blur", function (event) {
    if ((event.which < 46 || event.which > 57 || event.which == 47) && event.which != 8 && event.which != 0 && !event.ctrlKey) {
        event.preventDefault();
    } // prevent if not number/dot
    if (event.which == 46 && $(this).val().indexOf('.') != -1) {
        event.preventDefault();
    } // prevent if already dot
}).bind("paste", function (e) {
    e.preventDefault();
});


///By Danyal Manzoor to show notification or error 
function showNotification(msg) {
    $('.msgDiv').html(msg.trim()).fadeIn('slow').delay(3000).fadeOut('slow');
}


//For toggle checkbox switch button
$('body').on('change', "input[type='checkbox']", function () {

    var element = ("#" + $(this).attr("checkboxFor"));
    if ($(element).data("isbit") == true) {
        if ($(this).prop("checked")) {
            $(element).val("1");
        } else {
            $(element).val("0");
        }
    } else {
        $(element).val($(this).prop("checked"));
    }
});


//For activation of tab 

function activateTabById(tabId) {
    $('.nav-tabs a[href="#' + tabId + '"]').tab('show');
    $('.nav-tabs a[href="#' + tabId + '"]').click();

    $('html,body').animate({ scrollTop: 0 }, 'slow');
}

$(".phoneOnly").blur(function () {
    var msg = $(this).attr('error-msg');
    var value = $(this).val();
    if (value != "") {
        // var reg = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/;
        var reg = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\ \\\/]?)?((?:\(?\d{1,}\)?[\-\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/;
        if (!reg.test(value)) {
            alert(msg);
            $(this).val('');
        } else {
            if (value == " ") {
                alert(msg);
                $(this).val('');
            }
        }

        //   
    }
});

var getDaysInMonth = function (month) {
    return new Date(new Date().getFullYear(), month, 0).getDate();
}

//attachments work
$('input[type="file"]').change(function () {


    if ($(this).get(0).files.length > 0) {

        for (var i = 0; i < $(this).get(0).files.length; i++) {

            var name = $(this).get(0).files[i].name;
            name = name.substring(0, name.lastIndexOf('.'));

            if (name.length > 100) {
                alert('File name is too long, File name must have 100 or less characters');
                $(this).val('');
                return false;
            }

        }


    }
});

$(document).on('change', '.btn-file :file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function () {
    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {
        $('#headerMsg').text("");
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });

});
//attachments work

///add current tab id in session
$("ul.nav-tabs li a").click(function () {

    if ($(this).parent().parent().hasClass("subTabs")) {
        return;
    }
    var id = $(this).attr("href").replace("#", "");

    $.ajax({
        url: '/PrimeContractor/AddCurrentTabIdToSession',
        type: 'POST',
        data: {
            tabId: id
        },
        dataType: 'json',
        success: function (data) {
            if (data != null && data == true) {
                $("#currentTabId").val(id);
            }
        }
    });
});


function ActivateTabFromUrlOrSession() {

    var getTabId = window.location.hash.substr(1);

    if (getTabId != null && getTabId != "") {
        activateTabById(getTabId);
    } else {
        var getCurrentTabFromSession = $("#currentTabId").val();
        if (getCurrentTabFromSession != null && getCurrentTabFromSession != "") {
            activateTabById(getCurrentTabFromSession);
        }
    }
}


///for multiselect comma septrated ids
$("select[multiselectidfor]").change(function () {

    var element = ("#" + $(this).attr("multiselectidfor"));
    var thisselect = ("#" + $(this).attr("id"));
    $(element).val("");
    var selMulti = $.map($(thisselect + " option:selected"), function (el, i) {
        return $(el).val();
    });
    $(element).val(selMulti.join(", "));
});




//email validation 
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

﻿using OneSourceAdminCore.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneSourceAdmin.ViewModels
{
    public class OneStreetPropertyViewModel
    {
        [Required]
        public int CustomerId { get; set; }
        [Required(ErrorMessage="Please select customer.")]
        public List<SelectListItem> CustomerName { get; set; }
        public int ManufacturerId { get; set; }
        public List<GenericList> ManufacturerName { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public DateTime LastUpdated { get; set; }
        public int UpdatedBy { get; set; }

    }
}
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data
Imports Microsoft.Win32
Imports GSSupport
'--------------------------------------------------------------------------
' Description   :   This class contains the generaly used methods to 
'                   Input/Output from Database. Here we will pass the 
'                   quesries to function and the function will open the 
'                   connection to Db, execute the query and return the 
'                   result in expected format.
'--------------------------------------------------------------------------
Namespace GSSupportTicket
    Public Class IOfromDB

        'read the Connection string from web.config file
        Public Shared thisDBString As String = GSSupportManager.GetDBString()

        Public Shared Function ProcessSQLExecute(ByVal sql As String, ByVal RefLogpath As String) As Boolean

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql
                Cmd.ExecuteNonQuery()

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLExecute, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLExecute", ex.ToString() & ", at query: " & sql)
                Return False
            End Try

            Conn.Close()
            Return True
        End Function

        '-------------------------------------------------------------
        ' Usage     :   This function is currently used by 
        '               XMLGenerator.GeneratePurchaseOrderXML.
        '
        ' Purpose   :   This function is used to execute the passed 
        '               query and return the result in sql datareader. 
        '               It is used execute those queries where we need 
        '               the result in the datareader.  
        '-------------------------------------------------------------
        Public Shared Function ProcessSQLReader(ByVal sql As String, ByVal RefLogpath As String) As SqlDataReader

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim rdr As SqlDataReader

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql
                rdr = Cmd.ExecuteReader(CommandBehavior.CloseConnection)
                Cmd.Dispose()

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLReader, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLReader", ex.ToString() & ", at query: " & sql)
                Return Nothing
            End Try

            'Conn.Close()
            Return rdr
        End Function

        '-------------------------------------------------------------
        ' Usage     :   This function is currently used by 
        '               XMLParser.DellOrderAccepted.
        '
        ' Purpose   :   This function is used to execute the passed 
        '               query and return the result in integer. 
        '               It is used execute those queries where we need 
        '               only single integer in return. 
        '-------------------------------------------------------------
        Public Shared Function ProcessSQLInteger(ByVal sql As String, ByVal RefLogpath As String) As Int32
            Dim result As Integer = 0

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim rdr As SqlDataReader
            Dim Query As String
            Query = sql

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = Query
                rdr = Cmd.ExecuteReader()
                If rdr.Read Then
                    result = rdr.GetInt32(0)
                End If

                rdr.Close()
            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at ServiceEntity.ProcessSQLInteger, Error Detai:" & ex.ToString() & ", at query: " & Query)
                Handler.WriteLogfile("ServiceEntity", "ProcessSQLInteger", ex.ToString() & ", at query: " & Query)
                Return result
            End Try

            Conn.Close()
            Return result
        End Function


        Public Shared Function ProcessSPExecute(ByVal cmd As SqlCommand, ByVal RefLogpath As String) As Boolean
            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)


            Try
                Conn.Open()
                cmd.CommandTimeout = 2000
                cmd.Connection = Conn
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSPExecute, Error Detail:" & ex.ToString())
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSPExecute", ex.ToString())
                Return False
            End Try

            Conn.Close()
            Return True
        End Function


        '-------------------------------------------------------------
        ' Usage     :   This function is currently used by 
        '               XMLParser.DellOrderAccepted.
        '
        ' Purpose   :   This function is used to execute the passed 
        '               query and return the result in integer. 
        '               It is used execute those queries where we need 
        '               only single integer in return. 
        '-------------------------------------------------------------
        Public Shared Function ProcessSQLDate(ByVal sql As String, ByVal RefLogpath As String) As DateTime
            Dim result As DateTime

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim rdr As SqlDataReader

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql
                rdr = Cmd.ExecuteReader()
                If rdr.Read Then
                    result = rdr.GetDateTime(0)
                End If

                rdr.Close()
            Catch ex As Exception
                Conn.Close()

                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at ServiceEntity.ProcessSQLDate, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("ServiceEntity", "ProcessSQLDate", ex.ToString() & ", at query: " & sql)
                Return result
            End Try

            Conn.Close()
            Return result
        End Function

        Public Shared Function ProcessSQLString(ByVal sql As String, ByVal RefLogpath As String) As String

            Dim result As String = ""



            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim rdr As SqlDataReader

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql
                rdr = Cmd.ExecuteReader()
                If rdr.Read Then
                    result = rdr.GetString(0)
                End If

                rdr.Close()
            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at ServiceEntity.ProcessSQLDate, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("ServiceEntity", "ProcessSQLDate", ex.ToString() & ", at query: " & sql)
                Return result
            End Try

            Conn.Close()
            Return result
        End Function



        Public Shared Function ProcessSPExecuteDateSet(ByVal cmd As SqlCommand, ByVal RefLogpath As String) As DataSet

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim DAdap As SqlDataAdapter
            Dim ds As New DataSet



            Try
                Conn.Open()
                cmd.CommandTimeout = 2000
                cmd.Connection = Conn
                DAdap = New SqlDataAdapter(cmd)
                DAdap.Fill(ds)

                Conn.Close()

                Return ds

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSPExecute, Error Detail:" & ex.ToString())
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSPExecute", ex.ToString())
                Return Nothing
            End Try
        End Function


        '-------------------------------------------------------------
        ' Usage     :   
        '
        ' Purpose   :   This function is used to execute the passed 
        '               query and return the result as boolean. It is 
        '               used execute Data manipulation queries.  
        '-------------------------------------------------------------
        Public Shared Function ProcessSQLDataSet(ByVal sql As String, ByVal RefLogpath As String) As DataSet
            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim DAdap As SqlDataAdapter
            Dim ds As New DataSet

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql

                DAdap = New SqlDataAdapter(Cmd)
                DAdap.Fill(ds)

                Conn.Close()

                Return ds

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLDataSet, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLDataSet", ex.ToString() & ", at query: " & sql)
                Return Nothing
            End Try



        End Function

        Public Shared Function ProcessSQLCommand(ByVal cmd As SqlCommand) As DataSet

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim DAdap As SqlDataAdapter
            Dim ds As New DataSet

            Try
                Conn.Open()
                cmd.CommandTimeout = 2000
                cmd.Connection = Conn

                DAdap = New SqlDataAdapter(cmd)
                DAdap.Fill(ds)


            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLCommand, Error Detail:" & ex.ToString())
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLCommand", ex.ToString())
                Return Nothing
            End Try

            Conn.Close()
            Return ds
        End Function

        Public Shared Function ProcessSQLCommandReader(ByVal cmd As SqlCommand) As SqlDataReader

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim dr As SqlDataReader

            Try
                Conn.Open()
                cmd.CommandTimeout = 2000
                cmd.Connection = Conn

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                cmd.Dispose()

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLCommandReader, Error Detail:" & ex.ToString())
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLCommandReader", ex.ToString())
                Return Nothing
            End Try

            Return dr
        End Function

        Public Shared Sub ProcessSQLCommandExecute(ByVal cmd As SqlCommand)

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Try
                Conn.Open()
                cmd.CommandTimeout = 2000
                cmd.Connection = Conn

                cmd.ExecuteNonQuery()
                cmd.Dispose()

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLCommandExecute, Error Detail:" & ex.ToString())
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLCommandExecute", ex.ToString())
            End Try


        End Sub

        Public Shared Function ProcessExportDataSet(ByVal cmd As SqlCommand, ByRef ds As DataSet, ByVal TblName As String) As DataSet

            'if connection string not found get it from Config file
            'If thisDBString.Length = 0 Then
            '    thisDBString = Registry.Service.GetDBString
            'End If

            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim DAdap As SqlDataAdapter

            Try
                Conn.Open()
                cmd.CommandTimeout = 2000
                cmd.Connection = Conn

                DAdap = New SqlDataAdapter(cmd)
                DAdap.Fill(ds, TblName)


            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessExportDataSet, Error Detail:" & ex.ToString())
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessExportDataSet", ex.ToString())
                Return Nothing
            End Try

            Conn.Close()
            Return ds
        End Function

        '-------------------------------------------------------------
        ' Usage     :   
        '
        ' Purpose   :   This function is used to execute the passed 
        '               query and return the result as boolean. It is 
        '               used execute Data manipulation queries.  
        '-------------------------------------------------------------
        Public Shared Function ProcessSQLDataSet_CDS(ByVal sql As String, ByVal RefLogpath As String) As DataSet
            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim DAdap As SqlDataAdapter
            Dim ds As New DataSet

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql

                DAdap = New SqlDataAdapter(Cmd)
                DAdap.Fill(ds)

                Conn.Close()

                Return ds

            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_IOfromDB.ProcessSQLDataSet, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("onesource_IOfromDB", "ProcessSQLDataSet", ex.ToString() & ", at query: " & sql)
                Return Nothing
            End Try



        End Function
        Public Shared Function ProcessSQLString_CDS(ByVal sql As String, ByVal RefLogpath As String) As String

            Dim result As String = ""



            'create connnection and command objects
            Dim Conn As New SqlConnection(thisDBString)
            Dim Cmd As New SqlCommand()
            Dim rdr As SqlDataReader

            Try
                Conn.Open()
                Cmd.CommandTimeout = 2000
                Cmd.Connection = Conn
                Cmd.CommandText = sql
                rdr = Cmd.ExecuteReader()
                If rdr.Read Then
                    result = rdr.GetString(0)
                End If

                rdr.Close()
            Catch ex As Exception
                Conn.Close()
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at ServiceEntity.ProcessSQLDate, Error Detail:" & ex.ToString() & ", at query: " & sql)
                Handler.WriteLogfile("ServiceEntity", "ProcessSQLDate", ex.ToString() & ", at query: " & sql)
                Return result
            End Try

            Conn.Close()
            Return result
        End Function
    End Class
End Namespace


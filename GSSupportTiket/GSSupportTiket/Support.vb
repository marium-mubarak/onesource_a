﻿Imports System.Data.SqlClient

Namespace GSSupportTicket
    Public Class Support

        Public Shared RowCount As Integer  ' Items count per page in a gird. It is being used for custom paging

#Region "Quotes"

        Public Shared Function GetQuoteFilterStatusList() As DataSet
            Dim sSql As String
            sSql = "select quotestatusid, quotestatus from tblquotestatus order by quotestatusid"
            Return IOfromDB.ProcessSQLDataSet(sSql, "")
        End Function

        Public Shared Function GetCountryComboList(ByVal userid As String) As DataSet
            Dim sSql As String
            sSql = "select distinct pq.countryid, c.countryname from tblquotesummary pq" &
                           " inner join tblcountries c on c.countryid=pq.countryid" &
                           " where pq.createdby=" & userid &
                           " order by c.countryname"
            Return IOfromDB.ProcessSQLDataSet(sSql, "")
        End Function

        Public Shared Function GetSupplierList(ByVal userid As String) As DataSet
            Dim sSql As String
            sSql = "select distinct pq.sfaid, sfa.sfaname from tblquotesummary pq" &
                " inner join tblsfas sfa on sfa.sfaid=pq.sfaid" &
                " where pq.createdby=" & userid &
                " order by sfa.sfaname"
            Return IOfromDB.ProcessSQLDataSet(sSql, "")
        End Function

        Public Shared Function SetUserPreference(ByVal key As String, ByVal value As String, ByVal userid As String) As Boolean
            Dim PrefID As Integer
            Dim UserCount As Integer = 0
            Dim SQL As String = ""

            If (Len(Trim(key)) = 0) Or (Len(Trim(value)) = 0) Or (Len(Trim(userid)) = 0) Then
                Return False
            End If

            SQL = "SELECT ISNULL(COUNT(userid), 0) FROM tblUserPreference WHERE userid = " & userid & " and " &
             " preferenceid in (select preferenceid from tblPreference where upper(preferencename) = '" & UCase(key) & "')"

            UserCount = IOfromDB.ProcessSQLInteger(SQL, "")

            If UserCount > 0 Then
                SQL = "update tblUserPreference set lastupdated='" & Now() & "', lastupdatedbyuserid=" & userid & ", preferencevalue='" & value & "' where userid=" & userid & " and " &
                             " preferenceid in (select preferenceid from tblPreference where upper(preferencename) = '" & UCase(key) & "')"
                IOfromDB.ProcessSQLExecute(SQL, "")
            Else
                SQL = "select ISNULL(preferenceid, 0) from tblPreference where upper(preferencename) = '" & UCase(key) & "'"
                PrefID = IOfromDB.ProcessSQLInteger(SQL, "")
                If PrefID <> 0 Then
                    SQL = "insert into tblUserPreference(preferenceid, preferencevalue, userid, lastupdatedbyuserid, lastupdated, isactive) " &
                     " values (" & PrefID & ",'" & value & "'," & userid & "," & userid & ",'" & Now() & "',1)"
                    IOfromDB.ProcessSQLExecute(SQL, "")
                End If
            End If
            Return True
        End Function

        Public Shared Function GetQuotesRequestlist(ByVal UserGroupName As String, ByVal DateFormatID As String, ByVal UserSecurityLevel As String,
                                                    ByVal FilterID As String, ByVal CustomerID As String, ByVal UserID As String, ByVal SFAID As String, ByVal totalmonthstodisplay As String) As DataSet

            Dim sSQL, filterstring, tablejoinstring As String
            filterstring = ""
            tablejoinstring = ""

            If FilterID <> "" Then
                filterstring = GetFilterString(FilterID)
            End If

            If UserGroupName = "CUSTOMER" Then
                sSQL = "select distinct cast(pq.quoteid as varchar) as 'id', '../../quote/customerview.aspx?back=QuoteDashboard&quoteid=' + cast(pq.quoteid as varchar) as 'path',"
            ElseIf UserGroupName = "SFA" Then
                sSQL = "select distinct '<!--' + replace(str(pq.quoteid, 12), ' ', '0') + '--><a href=../../quote/supplierview.aspx?back=QuoteDashboard&quoteid=' + cast(pq.quoteid as varchar) + '>' + cast(pq.quoteid as varchar) + '</a>' as 'QuoteNo',"
            Else
                sSQL = "select distinct '<!--' + replace(str(pq.quoteid, 12), ' ', '0') + '--><a href=../../quote/primecontractorview.aspx?back=QuoteDashboard&quoteid=' + cast(pq.quoteid as varchar) + '>' + cast(pq.quoteid as varchar) + '</a>' as 'QuoteNo',"
            End If
            sSQL = sSQL & " pq.customerreference as 'QuoteRef', isnull(con.firstname, '') + ' ' + isnull(con.lastname, '') as 'RequestedBy',"
            If Len(DateFormatID) = 0 Then
                sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', pq.createdate) as varchar) + '-->' + CAST(Day(pq.createdate) as varchar) + '/' + CAST(Month(pq.createdate) as varchar) + '/' + CAST(Year(pq.createdate) as varchar) As 'Request Date',"
            ElseIf DateFormatID = 1 Then
                sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', pq.createdate) as varchar) + '-->' + CAST(Day(pq.createdate) as varchar) + '/' + CAST(Month(pq.createdate) as varchar) + '/' + CAST(Year(pq.createdate) as varchar) As 'Request Date',"
            ElseIf DateFormatID = 2 Then
                sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', pq.createdate) as varchar) + '-->' + CAST(Month(pq.createdate) as varchar) + '/' + CAST(Day(pq.createdate) as varchar) + '/' + CAST(Year(pq.createdate) as varchar) As 'Request Date',"
            End If
            If UserGroupName <> "CUSTOMER" Then
                sSQL = sSQL & " cust.customername as 'Customer',"
            Else
                sSQL = sSQL & " '' as 'Customer',"
            End If
            If UserGroupName <> "SFA" Then
                sSQL = sSQL & " sfa.sfaname as 'GSP',"
            Else
                sSQL = sSQL & " '' as 'GSP',"
            End If
            sSQL = sSQL & " c.countryname as 'Country', pqs.quotestatus as 'Status'"

            tablejoinstring = " from tblquotesummary pq" &
             " left outer join tblquoterequests pqreq on pqreq.quoteid=pq.quoteid" &
             " left outer join tblquoteresponses pqres on pqres.quoteid=pq.quoteid" &
             " left outer join tblquotestatus pqs on pqs.quotestatusid=pq.quotestatusid" &
             " left outer join tblcontact con on con.userid=pq.createdby" &
             " left outer join tblcustomers cust on cust.customerid=pq.customerid" &
             " left outer join tblcountries c on c.countryid=pq.countryid" &
             " left outer join tblsfas sfa on sfa.sfaid=pq.sfaid" &
             " where pq.isactive=1 AND DATEDIFF(M, pq.createdate,GETDATE()) < " & totalmonthstodisplay & " "
            If UserGroupName = "CUSTOMER" Then
                If UserSecurityLevel >= 4 Then
                    tablejoinstring = tablejoinstring & " and pq.customerid=" & CustomerID
                Else
                    tablejoinstring = tablejoinstring & " and pq.createdby=" & UserID
                End If
            End If
            If UserGroupName = "SFA" Then
                tablejoinstring = tablejoinstring & " and pq.sfaid=" & SFAID
            End If
            If filterstring <> "" Then
                tablejoinstring = tablejoinstring & filterstring
            End If

            sSQL = sSQL & tablejoinstring & " Order by 'id' desc "

            Return IOfromDB.ProcessSQLDataSet(sSQL, "")

        End Function

        Public Shared Function GetQuotesListforBarChart(ByVal UserGroupName As String, ByVal UserSecurityLevel As String,
                                                       ByVal FilterID As String, ByVal CustomerID As String, ByVal UserID As String, ByVal SFAID As String, ByVal MonthstoDisplay As String) As SqlDataReader
            Dim filterstring, tablejoinstring As String
            filterstring = ""
            tablejoinstring = ""
            Dim cmd As SqlCommand
            Dim param As SqlParameter
            Dim BarChartSql As String

            If FilterID <> "" Then
                filterstring = GetFilterString(FilterID)
            End If

            cmd = New SqlCommand
            tablejoinstring = " from tblquotesummary pq" &
                     " left outer join tblquoterequests pqreq on pqreq.quoteid=pq.quoteid" &
                     " left outer join tblquoteresponses pqres on pqres.quoteid=pq.quoteid" &
                     " left outer join tblquotestatus pqs on pqs.quotestatusid=pq.quotestatusid" &
                     " left outer join tblcontact con on con.userid=pq.createdby" &
                     " left outer join tblcustomers cust on cust.customerid=pq.customerid" &
                     " left outer join tblcountries c on c.countryid=pq.countryid" &
                     " left outer join tblsfas sfa on sfa.sfaid=pq.sfaid" &
                     " where pq.isactive=1"
            If UserGroupName = "CUSTOMER" Then
                If UserSecurityLevel >= 4 Then
                    tablejoinstring = tablejoinstring & " and pq.customerid=" & CustomerID

                    'param = New SqlParameter
                    'param.ParameterName = "@cid"
                    'param.Value = CustomerID
                    'cmd.Parameters.Add(param)
                Else
                    tablejoinstring = tablejoinstring & " and pq.createdby=@uid"

                    param = New SqlParameter
                    param.ParameterName = "@uid"
                    param.Value = UserID
                    cmd.Parameters.Add(param)
                End If
            End If
            If UserGroupName = "SFA" Then
                tablejoinstring = tablejoinstring & " and pq.sfaid=@sfaid"

                param = New SqlParameter
                param.ParameterName = "@sfaid"
                param.Value = SFAID
                cmd.Parameters.Add(param)
            End If
            If filterstring <> "" Then
                tablejoinstring = tablejoinstring & filterstring
            End If
            If MonthstoDisplay < 24 Then
                BarChartSql = "SELECT SUBSTRING(CreatedMonth,0,4) as CreatedMonth,SUM(StatusCount) AS StatusCount FROM (SELECT     TOP (100) PERCENT CAST(DATENAME(mm, pq.CreateDate) AS varchar) AS CreatedMonth,month(pq.CreateDate)+ year(pq.CreateDate)*12 as num,COUNT(pqs.QuoteStatus) AS StatusCount "
                BarChartSql = BarChartSql & tablejoinstring & " AND DATEDIFF(M, pq.createdate,GETDATE()) < " & MonthstoDisplay & " GROUP BY pq.CreateDate ORDER BY pq.CreateDate DESC) AS tbltemp GROUP BY CreatedMonth order by Max(num)"
            Else
                BarChartSql = "SELECT QCheck as CreatedMonth, SUM(StatusCount) AS StatusCount " &
                                " FROM (SELECT TOP (100) PERCENT CAST(DATENAME(mm, pq.CreateDate) AS varchar) AS CreatedMonth, month(pq.CreateDate)+ year(pq.CreateDate)*12 as num, " &
                                " CASE MONTH(pq.CreateDate) " &
                                " WHEN '1' THEN '1st Q ' WHEN '2' THEN '1st Q' WHEN '3' THEN '1st Q' WHEN '4' THEN '2nd Q'	WHEN '5' THEN '2nd Q' WHEN '6' THEN '2nd Q' " &
                                " WHEN '7' THEN '3rd Q ' WHEN '8' THEN '3rd Q'	WHEN '9' THEN '3rd Q' WHEN '10' THEN '4th Q' WHEN '11' THEN '4th Q'	WHEN '12' THEN '4th Q' " &
                                " END + ' ' + CAST(year(pq.CreateDate) as varchar) AS QCheck,  " &
                                "COUNT(pqs.QuoteStatus) AS StatusCount "
                BarChartSql = BarChartSql & tablejoinstring & " AND DATEDIFF(M, pq.createdate,GETDATE()) < " & MonthstoDisplay & " GROUP BY pq.CreateDate ORDER BY pq.CreateDate DESC) AS tbltemp GROUP BY QCheck order by Max(num)"
            End If

            cmd.CommandText = BarChartSql

            Return IOfromDB.ProcessSQLCommandReader(cmd)

        End Function

        Public Shared Function GetQuotesListforPieChart(ByVal UserGroupName As String, ByVal UserSecurityLevel As String, ByVal FilterID As String,
                                                           ByVal CustomerID As String, ByVal UserID As String, ByVal SFAID As String, ByVal totalmonthstodisplay As String) As SqlDataReader
            Dim filterstring, tablejoinstring As String
            filterstring = ""
            tablejoinstring = ""

            Dim cmd As SqlCommand
            If FilterID <> "" Then
                filterstring = GetFilterString(FilterID)
            End If

            cmd = New SqlCommand
            tablejoinstring = " from tblquotesummary pq" &
                     " left outer join tblquoterequests pqreq on pqreq.quoteid=pq.quoteid" &
                     " left outer join tblquoteresponses pqres on pqres.quoteid=pq.quoteid" &
                     " left outer join tblquotestatus pqs on pqs.quotestatusid=pq.quotestatusid" &
                     " left outer join tblcontact con on con.userid=pq.createdby" &
                     " left outer join tblcustomers cust on cust.customerid=pq.customerid" &
                     " left outer join tblcountries c on c.countryid=pq.countryid" &
                     " left outer join tblsfas sfa on sfa.sfaid=pq.sfaid" &
                     " where pq.isactive=1 AND DATEDIFF(M, pq.createdate,GETDATE()) < " & totalmonthstodisplay & " "
            If UserGroupName = "CUSTOMER" Then
                If UserSecurityLevel >= 4 Then
                    tablejoinstring = tablejoinstring & " and pq.customerid=" & CustomerID
                Else
                    tablejoinstring = tablejoinstring & " and pq.createdby=" & UserID
                End If
            End If
            If UserGroupName = "SFA" Then
                tablejoinstring = tablejoinstring & " and pq.sfaid=" & SFAID


            End If
            If filterstring <> "" Then
                tablejoinstring = tablejoinstring & filterstring
            End If

            Dim pieChartSql As String = "select pqs.quotestatus as 'Status', COUNT(pqs.quotestatus) as StatusCount "
            pieChartSql = pieChartSql & tablejoinstring & " GROUP BY pqs.quotestatus "

            cmd.CommandText = pieChartSql

            Return IOfromDB.ProcessSQLCommandReader(cmd)

        End Function


#End Region

#Region "Request"

        Public Shared Function GetActionRequestTypeDefaultTypeID(ByVal companyid As String) As Integer
            Dim dr As SqlDataReader
            Dim cmd As SqlCommand
            Dim param As SqlParameter
            Dim countartypeid As Integer


            cmd = New SqlCommand("select count(artypeid) from tblactionrequesttype where isactive=1 and isselectable=1 and companyid=@CId")

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            dr = IOfromDB.ProcessSQLCommandReader(cmd)
            cmd = Nothing

            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    countartypeid = Convert.ToInt32(dr.Item(0))
                    dr.Close()
                End If
            End If


            If countartypeid = 0 Then
                Return IOfromDB.ProcessSQLInteger("select artypeid from tblactionrequesttype where isactive=1 and isselectable=1 and isdefault=1 and companyid is null", "")
            Else

                cmd = New SqlCommand("select artypeid from tblactionrequesttype where isactive=1 and isselectable=1 and isdefault=1 and companyid=@CId")

                param = New SqlParameter
                param.ParameterName = "@CId"
                param.Value = companyid
                cmd.Parameters.Add(param)

                dr = IOfromDB.ProcessSQLCommandReader(cmd)
                cmd = Nothing

                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        dr.Read()
                        Return Convert.ToInt32(dr.Item(0))
                        dr.Close()
                    End If
                End If

            End If
        End Function

        Public Shared Function GetDefaultPriorityID(ByVal companyid As String, ByVal GlobalServeCompanyID As String) As Integer

            Dim dr As SqlDataReader
            Dim cmd As SqlCommand
            Dim param As SqlParameter
            Dim count As Integer

            cmd = New SqlCommand("select count(priorityid) from tblpriority where isactive=1 and companyid=@CompanyId")

            param = New SqlParameter
            param.ParameterName = "@CompanyId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            dr = IOfromDB.ProcessSQLCommandReader(cmd)
            cmd = Nothing

            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    count = Convert.ToInt32(dr.Item(0))
                    dr.Close()
                End If
            End If

            If count = 0 Then
                cmd = New SqlCommand("select isnull(cast(priorityid as varchar), '') from tblpriority where isactive=1 and prioritydefaultint=priorityintvalue and companyid=@CId")

                param = New SqlParameter
                param.ParameterName = "@CId"
                param.Value = GlobalServeCompanyID
                cmd.Parameters.Add(param)

                dr = IOfromDB.ProcessSQLCommandReader(cmd)
                cmd = Nothing

                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        dr.Read()
                        Return Convert.ToString(dr.Item(0))
                        dr.Close()
                    End If
                End If
            Else
                cmd = New SqlCommand("select isnull(cast(priorityid as varchar), '') from tblpriority where isactive=1 and prioritydefaultint=priorityintvalue and companyid=@CId")

                param = New SqlParameter
                param.ParameterName = "@CId"
                param.Value = companyid
                cmd.Parameters.Add(param)

                dr = IOfromDB.ProcessSQLCommandReader(cmd)
                cmd = Nothing

                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        dr.Read()
                        Return Convert.ToString(dr.Item(0))
                        dr.Close()
                    End If
                End If
            End If
        End Function

        Public Shared Function GetRequestTypelist(ByVal companyid As String) As DataSet
            Dim dr As SqlDataReader
            Dim ds As DataSet
            Dim cmd As SqlCommand
            Dim param As SqlParameter
            Dim count As Integer

            cmd = New SqlCommand("select count(artypeid) from tblactionrequesttype where isactive=1 and isselectable=1 and companyid in (@CId)")

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            dr = IOfromDB.ProcessSQLCommandReader(cmd)
            cmd = Nothing

            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    count = Convert.ToInt32(dr.Item(0))
                    dr.Close()
                End If
            End If

            Dim sSQL As String = ""
            If count = 0 Then
                sSQL = "select artypeid, artypename from tblactionrequesttype where isactive=1 and isselectable=1 and (companyid in (@CId) or companyid is null) order by artypename desc"
            Else
                sSQL = "select artypeid, artypename from tblactionrequesttype where isactive=1 and isselectable=1 and companyid in (@CId)  order by artypename desc"
            End If
            cmd = New SqlCommand
            cmd.CommandText = sSQL

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            ds = New DataSet
            ds = IOfromDB.ProcessSQLCommand(cmd)

            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetCompanyPriorityId(ByVal companyid As String) As DataSet
            Dim sql As String
            Dim ds As DataSet
            Dim cmd As New SqlCommand
            Dim param As SqlParameter

            sql = "select priorityID from tblPriority where companyid = @CId"
            cmd.CommandText = sql

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            ds = New DataSet
            ds = IOfromDB.ProcessSQLCommand(cmd)

            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetCompanyPriority(ByVal companyid As String, ByVal includeGS As Boolean) As DataSet
            Dim sSQL As String
            Dim ds As DataSet
            Dim cmd As New SqlCommand
            Dim param As SqlParameter


            If includeGS Then

                sSQL = "select priorityid, priorityname, parentcompanyname, priorityIntValue, priorityDefaultInt from tblPriority left join tblParentCompanies on tblPriority.CompanyID = tblParentCompanies.parentcompanyid where tblPriority.isActive=1 and (companyid in (select parentcompanyid from tblParentCompanies where parentcompanyname='GlobalServe') or companyid in (@CId)) order by priorityintvalue"

            Else
                sSQL = "select priorityid, priorityname, parentcompanyname, priorityIntValue, priorityDefaultInt from tblPriority left join tblParentCompanies on tblPriority.CompanyID = tblParentCompanies.parentcompanyid where tblPriority.isActive=1 and companyid in (@CId) order by priorityintvalue"

            End If
            cmd.CommandText = sSQL

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            ds = New DataSet
            ds = IOfromDB.ProcessSQLCommand(cmd)

            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetCompanyCustomLocationList(ByVal companyid As String) As DataSet
            Dim sSQL As String
            Dim cmd As New SqlCommand
            Dim ds As DataSet
            Dim param As SqlParameter

            sSQL = "select ca.customeraddressid, substring(c.countryname + ' - ' + ca.customeraddressname + ' ' + ca.address1, 0, 60) as customeraddressname" &
                    " , c.countryid" &
                     " from tblcustomeraddresses ca, tblcountries c" &
                     " where c.countryid=ca.countryid and ca.isactive=1" &
                     " and ca.customerid in (select customerid from tblcustomers where parentcompanyid=@CId)" &
                     " order by c.countryname, ca.customeraddressname"

            cmd.CommandText = sSQL

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            ds = New DataSet
            ds = IOfromDB.ProcessSQLCommand(cmd)

            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        Public Shared Function GetAffectiveUserList(ByVal companyid As String) As DataSet
            Dim sSQL As String
            Dim ds As DataSet
            Dim cmd As New SqlCommand
            Dim param As SqlParameter

            sSQL = "select c.contactid, isnull(c.firstname, '') + isnull(c.lastname, '') + isnull('(' + u.username + ')', '') as contactname" &
                " from tblcontact c left outer join tblusers u on u.userid=c.userid" &
                " where c.isactive=1 and u.isactive=1 and c.companyid=@CId" &
                " order by c.firstname, c.lastname"
            cmd.CommandText = sSQL

            param = New SqlParameter
            param.ParameterName = "@CId"
            param.Value = companyid
            cmd.Parameters.Add(param)

            ds = New DataSet
            ds = IOfromDB.ProcessSQLCommand(cmd)

            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        End Function

        ''' <summary>
        ''' Returns 'vbConst' from 'tblTimeZones'
        ''' This function is called in OneSource pages:
        ''' others\Convert.aspx.vb
        ''' Support\Requests\New.aspx.vb
        ''' Support\Requests\Ticket.aspx.vb
        ''' </summary>
        ''' <param name="tz"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetTimeZoneVBConst(ByVal tz As String) As String
            Dim tzconst As String = ""
            GetTimeZoneVBConst = 1
            Dim tzsql As String = "select vbConst from tblFields where fieldid in (select fieldID from tblTimeZones  where timezoneid = " & tz & ")"
            Dim ds As DataSet = IOfromDB.ProcessSQLDataSet(tzsql, "")

            If Not ds Is Nothing Then
                For Each row As DataRow In ds.Tables(0).Rows
                    tzconst = row("vbConst").ToString()
                Next
            End If
            Return tzconst

        End Function

        Public Shared Function ConvertTime(ByVal ddate As Date, ByVal tz1 As String, ByVal tz2 As String) As String
            Dim SQL As String = ""
            Dim ds As DataSet
            Dim GMTDeltaHours1, GMTDeltaHours2, Difference As Double
            Difference = 0
            If Len(tz1) > 0 And Len(tz2) > 0 Then
                SQL = "SELECT GMTDeltaHours FROM tblTimeZones WHERE TimeZoneID=" & tz1

                ds = IOfromDB.ProcessSQLDataSet(SQL, "")
                If Not ds Is Nothing Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        GMTDeltaHours1 = CDbl(ds.Tables(0).Rows(0)("GMTDeltaHours"))
                        ds.Dispose()
                    End If
                End If

                SQL = "SELECT GMTDeltaHours FROM tblTimeZones WHERE TimeZoneID=" & tz2

                ds = IOfromDB.ProcessSQLDataSet(SQL, "")
                If Not ds Is Nothing Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        GMTDeltaHours2 = CDbl(ds.Tables(0).Rows(0)("GMTDeltaHours"))
                        ds.Dispose()
                    End If
                End If
                Difference = GMTDeltaHours2 - GMTDeltaHours1
            End If
            Return ddate.AddHours(Difference).ToString()
        End Function

        Public Shared Function GetTimeZoneID(ByVal tz) As String
            Dim sql, tzid As String
            tzid = ""

            sql = "select timezoneid from tblTimezones where fieldid in (select fieldID from tblFields where vbConst=" & tz & ")"

            Dim ds As DataSet = IOfromDB.ProcessSQLDataSet(sql, "")

            If Not ds Is Nothing Then
                For Each row As DataRow In ds.Tables(0).Rows
                    tzid = row("timezoneid").ToString()
                Next
            End If
            Return tzid
        End Function

        Public Shared Function SubmitRequest(ByVal submituserid As String, ByVal submitusername As String, ByVal artypeid As String, ByVal priorityid As String,
                                ByVal assetid As String, ByVal requestdategmtoffset As String, ByVal requestdategmtoffsetstring As String, ByVal companyid As String, ByVal companydisplay As String,
                                ByVal addressid As String, ByVal addressdisplay As String, ByVal affectedcontactid As String, ByVal affectedcontactname As String, ByVal gscomments As String, ByVal summary As String,
                                ByVal desc As String, ByVal UserSecurityLevel As String, ByVal ServerTimeZone As String,
                                ByVal ContactID As String, ByVal SerialNumber As String, ByVal AssetTag As String, ByVal CustomerTicketNo As String, ByVal Contactname As String,
                                ByVal ContactPhone As String, ByVal priorityText As String, ByVal requestTypeText As String, ByRef ticketid As String) As Boolean
            Try


                Dim ds, dsassetinfo As DataSet
                Dim dr As SqlDataReader
                Dim cmd As SqlCommand
                Dim param As SqlParameter

                ticketid = ""

                Dim requestdate As String = ""
                Dim requestdatelocal As String = ""

                Dim requestdatetimezoneid As String = ""

                Dim sql As String = ""

                Dim GMTOffsetString As String = ""
                Dim GMTOffset As String = ""
                Dim customertzvbconst As String = ""
                Dim ar As GSSupport.ActionRequest

                Dim tzid As Integer

                Dim d As DateTime = DateTime.Now

                ticketid = 0


                requestdate = d.Year & "-" & d.Month & "-" & d.Day & " " & d.Hour & ":" & d.Minute & ":" & d.Second
                requestdatelocal = d.Year & "-" & d.Month & "-" & d.Day & " " & d.Hour & ":" & d.Minute & ":" & d.Second


                requestdatetimezoneid = ""

                If Len(addressid) > 0 Then


                    cmd = New SqlCommand
                    Dim addresssql As String = "select isnull(timezoneid, 0) as timezoneid, isnull(vbConst, 0) as vbConst, timezoneshort, GMTDeltaMinutes" &
                       " from tblTimeZones, tblFields" &
                       " where tblTimeZones.fieldid=tblFields.fieldid and timezoneid in (select isnull(timezoneid, 0) from tblCustomerAddresses" &
                       " where customeraddressid=@AddId)"
                    cmd.CommandText = addresssql

                    param = New SqlParameter
                    param.ParameterName = "@AddId"
                    param.Value = addressid
                    cmd.Parameters.Add(param)

                    ds = New DataSet
                    ds = IOfromDB.ProcessSQLCommand(cmd)
                    cmd = Nothing

                    If Not ds Is Nothing Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            tzid = CInt(ds.Tables(0).Rows(0)("timezoneid"))
                            GMTOffsetString = ds.Tables(0).Rows(0)("timezoneshort")
                            GMTOffset = CInt(ds.Tables(0).Rows(0)("GMTDeltaMinutes"))
                            customertzvbconst = ds.Tables(0).Rows(0)("vbConst")
                        Else
                            cmd = New SqlCommand
                            sql = "select isnull(timezoneid, 0) as timezoneid, timezoneshort, GMTDeltaMinutes, isnull(vbConst, 0) as vbConst" &
                                " from tblTimeZones, tblFields where tblFields.fieldid=tblTimezones.fieldid" &
                                " and timezoneid in (select timezoneid from tblCountryDefaultTimeZone where countryid in (select countryid" &
                                 " from tblCustomerAddresses where customeraddressid=@AddId))"
                            cmd.CommandText = sql

                            param = New SqlParameter
                            param.ParameterName = "@AddId"
                            param.Value = addressid
                            cmd.Parameters.Add(param)

                            ds = New DataSet
                            ds = IOfromDB.ProcessSQLCommand(cmd)
                            cmd = Nothing

                            If Not ds Is Nothing Then
                                If ds.Tables(0).Rows.Count > 0 Then
                                    tzid = CInt(ds.Tables(0).Rows(0)("timezoneid"))
                                    GMTOffsetString = ds.Tables(0).Rows(0)("timezoneshort")
                                    GMTOffset = CInt(ds.Tables(0).Rows(0)("GMTDeltaMinutes"))
                                    customertzvbconst = ds.Tables(0).Rows(0)("vbConst")
                                End If

                            End If
                        End If
                    Else
                        cmd = New SqlCommand
                        sql = "select isnull(timezoneid, 0) as timezoneid, timezoneshort, GMTDeltaMinutes, isnull(vbConst, 0) as vbConst" &
                            " from tblTimeZones, tblFields where tblFields.fieldid=tblTimezones.fieldid" &
                            " and timezoneid in (select timezoneid from tblCountryDefaultTimeZone where countryid in (select countryid" &
                             " from tblCustomerAddresses where customeraddressid=@AddId))"
                        cmd.CommandText = sql

                        param = New SqlParameter
                        param.ParameterName = "@AddId"
                        param.Value = addressid
                        cmd.Parameters.Add(param)

                        ds = New DataSet
                        ds = IOfromDB.ProcessSQLCommand(cmd)
                        cmd = Nothing

                        If Not ds Is Nothing Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                tzid = CInt(ds.Tables(0).Rows(0)("timezoneid"))
                                GMTOffsetString = ds.Tables(0).Rows(0)("timezoneshort")
                                GMTOffset = CInt(ds.Tables(0).Rows(0)("GMTDeltaMinutes"))
                                customertzvbconst = ds.Tables(0).Rows(0)("vbConst")
                            End If

                        End If
                    End If

                End If

                ' Request date is the submitted request date converted to server time zone time
                Dim vbtmp As String
                vbtmp = ServerTimeZone
                If Len(customertzvbconst) > 0 Then
                    If CInt(customertzvbconst) > 0 Then
                        vbtmp = customertzvbconst
                    End If
                End If

                If Len(requestdatetimezoneid) > 0 Then
                    requestdate = ConvertTime(requestdate, GetTimeZoneVBConst(requestdatetimezoneid.ToString()), ServerTimeZone)
                Else
                    requestdatetimezoneid = GetTimeZoneID(ServerTimeZone)
                End If

                If Len(vbtmp) > 0 Then
                    requestdatelocal = ConvertTime(requestdate, ServerTimeZone, vbtmp)
                Else
                    ' If time zone is not available yet, set request date local to request date
                    requestdatelocal = requestdate
                End If

                requestdategmtoffset = GMTOffset
                requestdategmtoffsetstring = GMTOffsetString
                If Len(tzid) > 0 Then
                    requestdatetimezoneid = tzid
                End If

                ' Set AR values
                ar = New GSSupport.ActionRequest()
                'Dim s As String = GSSupport.GSSupportManager.GetDBString()
                ' Submit user id will always be logged on user id
                ar.SubmitUserID = CInt(submituserid)
                ar.AffectedContactID = CInt(ContactID)
                ar.RequestDateLocal = Now()
                ar.RequestDate = Now()
                ar.RequestDateTimeZoneVBConst = ServerTimeZone


                ar.Summary = summary
                ar.Description = desc

                If Len(assetid) > 0 Then
                    ' Get all the asset information and add it to the request
                    cmd = New SqlCommand
                    sql = "select ao.assettypeid, isnull(ao.model, '') as model, ao.manufacturer, ao.manufacturerid," &
                    " ao.customerpartcategoryid, pc.partcategoryname" &
                    " from gsnetp_tbl_assetsowned ao" &
                    " inner join gsnetp_tbl_assets a on a.assetsownedid = ao.assetsownedid and a.isactive=1" &
                    " left join tblpartcategories pc on pc.partcategoryid = ao.customerpartcategoryid" &
                    " where a.assetid=@AssetId"
                    cmd.CommandText = sql

                    param = New SqlParameter
                    param.ParameterName = "@AssetId"
                    param.Value = assetid
                    cmd.Parameters.Add(param)

                    dsassetinfo = New DataSet
                    dsassetinfo = IOfromDB.ProcessSQLCommand(cmd)
                    cmd = Nothing

                    If Not dsassetinfo Is Nothing Then
                        For Each row As DataRow In dsassetinfo.Tables(0).Rows
                            If Len(row("assettypeid")) > 1 Then
                                ar.AddFieldWithDisplay("assetmfgsku", row("assettypeid"), row("assettypeid"))
                                If Not row("customerpartcategoryid") Is Nothing Then
                                    ar.AddFieldWithDisplay("assetcustomercategory", Convert.ToString(row("customerpartcategoryid")), Convert.ToString(row("partcategoryname")))
                                End If
                                ar.AddFieldWithDisplay("assetmodel", row("model"), row("model"))
                                If Not row("manufacturerid") Is Nothing Then
                                    ar.AddFieldByDisplay("Manufacturer of Affected Asset", row("manufacturerid"), row("manufacturer"))
                                End If
                            End If
                        Next
                    End If

                    ar.SerialNum = SerialNumber
                    ar.AddFieldByDisplay("Serial Number", SerialNumber)
                    ar.AssetTag = AssetTag
                End If

                'See if there's special coverage for this address
                Dim coverageid As Integer = 0
                If Len(addressid) > 0 Then

                    cmd = New SqlCommand("select coverageid from tblCustomerAddresses where customeraddressid=@AddId")

                    param = New SqlParameter
                    param.ParameterName = "@AddId"
                    param.Value = addressid
                    cmd.Parameters.Add(param)

                    dr = IOfromDB.ProcessSQLCommandReader(cmd)
                    cmd = Nothing

                    If Not dr Is Nothing Then
                        If dr.HasRows Then
                            dr.Read()
                            If Not DBNull.Value Is dr.Item(0) Then
                                coverageid = Convert.ToInt32(dr.Item(0))
                            End If
                            dr.Close()
                        End If
                    End If
                End If

                If Len(assetid) > 0 Then
                    If CLng(assetid) > 0 Then
                        ar.GSAssetID = assetid
                        ' See if there's special coverage for this asset
                        Dim tmpint As Integer = 0

                        cmd = New SqlCommand("select coverageid from gsnetp_tbl_assets where assetid=@AssetId")

                        param = New SqlParameter
                        param.ParameterName = "@AssetId"
                        param.Value = assetid
                        cmd.Parameters.Add(param)

                        dr = IOfromDB.ProcessSQLCommandReader(cmd)
                        cmd = Nothing

                        If Not dr Is Nothing Then
                            If dr.HasRows Then
                                dr.Read()
                                If Not DBNull.Value Is dr.Item(0) Then
                                    tmpint = Convert.ToInt32(dr.Item(0))
                                    If tmpint > 0 Then
                                        coverageid = tmpint
                                    End If
                                End If
                                dr.Close()
                            End If
                        End If

                    End If
                End If
                ar.AddFieldByDisplay("Coverage Type", coverageid)

                ar.SubmitDate = Now()
                ar.RequestDate = requestdate
                ar.RequestDateLocal = requestdatelocal
                ar.RequestDateTimeZoneID = requestdatetimezoneid
                If Len(requestdategmtoffset) > 0 Then
                    ar.RequestDateLocalGMTOffset = requestdategmtoffset
                End If
                ar.RequestDateLocalGMTOffsetString = requestdategmtoffsetstring

                ar.CustomerRequestID = CustomerTicketNo
                ar.AddFieldByDisplay("Customer Ticket ID", CustomerTicketNo)

                ar.TicketContactName = Contactname
                ar.AddFieldByDisplay("Ticket Contact Name", Contactname)
                ar.TicketContactPhone = ContactPhone
                ar.AddFieldByDisplay("Ticket Contact Phone", ContactPhone)


                ' CompanyID must come before address so we can find country
                ar.SetCompanyID(CType(companyid, Int32), companydisplay)
                ar.SetPriorityID(CType(priorityid, Int32), priorityText)
                ar.AddFieldByDisplay("Ticket Priority", priorityid, priorityText)
                ar.SetTypeID(CType(artypeid, Int32), requestTypeText)
                ar.AddFieldByDisplay("Request Type", CType(artypeid, Int32), requestTypeText)

                If Len(addressid) > 0 Then
                    cmd = New SqlCommand("select isnull('<br>' + address1, '') + isnull('<br>' + address2, '') + isnull('<br>' + city, '') + isnull('  ' + stateprovinceregion, '') + isnull(' ' + postalcode, '')  + isnull(' ' + countryname, '') from tblcustomeraddresses, tblcountries where tblcountries.countryid=tblcustomeraddresses.countryid and tblcustomeraddresses.customeraddressid=@AddId")

                    param = New SqlParameter
                    param.ParameterName = "@AddId"
                    param.Value = addressid
                    cmd.Parameters.Add(param)

                    dr = IOfromDB.ProcessSQLCommandReader(cmd)
                    cmd = Nothing

                    If Not dr Is Nothing Then
                        If dr.HasRows Then
                            dr.Read()
                            addressdisplay = addressdisplay & dr.Item(0)
                            dr.Close()
                        End If
                    End If
                End If
                ar.SetAddressID(addressid, addressdisplay)

                ' Add phone numbers
                If Len(affectedcontactid) > 0 Then
                    cmd = New SqlCommand("select IsNull(workphonenumber, '') from tblContact where contactid=@ContactId")

                    param = New SqlParameter
                    param.ParameterName = "@ContactId"
                    param.Value = affectedcontactid
                    cmd.Parameters.Add(param)

                    dr = IOfromDB.ProcessSQLCommandReader(cmd)
                    cmd = Nothing

                    If Not dr Is Nothing Then
                        If dr.HasRows Then
                            dr.Read()
                            ar.AddFieldByDisplay("Affected Contact Phone", dr.Item(0))
                            dr.Close()
                        End If
                    End If

                End If
                If Len(submituserid) > 0 Then
                    cmd = New SqlCommand("select IsNull(workphonenumber, '') from tblContact where userid=@UserId")

                    param = New SqlParameter
                    param.ParameterName = "@UserId"
                    param.Value = submituserid
                    cmd.Parameters.Add(param)

                    dr = IOfromDB.ProcessSQLCommandReader(cmd)
                    cmd = Nothing

                    If Not dr Is Nothing Then
                        If dr.HasRows Then
                            dr.Read()
                            ar.AddFieldByDisplay("Submitting User Phone", dr.Item(0))
                            dr.Close()
                        End If
                    End If
                End If

                ' Get countryid and countryname
                Dim countryid As Integer = 0
                Dim countryname As String = ""
                If Len(addressid) > 0 Then

                    cmd = New SqlCommand("select countryid from tblCustomerAddresses where customeraddressid=@AddId")

                    param = New SqlParameter
                    param.ParameterName = "@AddId"
                    param.Value = addressid
                    cmd.Parameters.Add(param)

                    dr = IOfromDB.ProcessSQLCommandReader(cmd)
                    cmd = Nothing

                    If Not dr Is Nothing Then
                        If dr.HasRows Then
                            dr.Read()
                            countryid = Convert.ToInt32(dr.Item(0))
                            dr.Close()
                        End If
                    End If

                    cmd = New SqlCommand("select countryname from tblCountries where countryid=@CountryId")

                    param = New SqlParameter
                    param.ParameterName = "@CountryId"
                    param.Value = countryid
                    cmd.Parameters.Add(param)

                    dr = IOfromDB.ProcessSQLCommandReader(cmd)
                    cmd = Nothing

                    If Not dr Is Nothing Then
                        If dr.HasRows Then
                            dr.Read()
                            countryname = dr.Item(0)
                            dr.Close()
                        End If
                    End If

                    ar.AddFieldByDisplay("Country of Affected User", countryid, countryname)

                    If Len(assetid) > 0 Then
                        If assetid > 0 Then
                            ar.AddFieldByDisplay("Country of Affected Asset", countryid, countryname)
                        End If
                    End If
                End If

                ar.SetAffectedContactID(affectedcontactid, affectedcontactname)

                ar.AddFieldByDisplay("Ticket Summary", summary)
                ar.AddFieldByDisplay("Ticket Description", desc)
                ar.AddFieldByDisplay("Ticket Submitted By", submituserid, submitusername)
                ar.AddFieldByDisplay("Ticket Affected User", affectedcontactid, affectedcontactname)


                ' CustomerID field gets set to companyid for now...
                ar.CustomerID = companyid
                ar.Qualify = True
                ar.GenerateTicket = True
                ar.GSComments = gscomments

                ' Category is new request
                ar.CategoryName = "New Request"

                ar.TicketCreateLocal = ConvertTime(Now, ServerTimeZone, GetTimeZoneVBConst(requestdatetimezoneid))
                ar.AddFieldWithDisplay("customeraddressid", CType(addressid, Int32), addressdisplay)
                ar.AddFieldWithDisplay("tktsummary", summary, summary)
                ar.AddFieldWithDisplay("tktdesc", desc, desc)
                ar.AddFieldWithDisplay("tktaffecteduser", CType(affectedcontactid, Int32), affectedcontactname)
                ar.AddFieldWithDisplay("tktsubmitby", CType(submituserid, Int32), submitusername)
                If ar.Submit() Then
                    ticketid = ar.GSTicketID
                    Return True
                Else
                    Return False

                End If
            Catch ex As Exception
                Handler.WriteLogtoEViewer("GSOneSource", "Error accured at onesource_Support.SubmitRequest, Error Detail:" & ex.ToString)
                Handler.WriteLogfile("onesource_Support", "SubmitRequest", ex.ToString())
                Return False
            End Try

        End Function

        Public Shared Function GetAttach(ByVal TicketID As Integer) As DataSet
            Dim sqlAttach As String = "select daid, docid, daname, daurl, isnull(mimetypeicon, 'blank.gif') as mimetypeicon from tblDigitalAssets, tblMimeTypes " &
                     "where tblMimeTypes.mimetypeid =* tblDigitalAssets.damimetypeid and daid in (select daid from " &
                     " tblTicketAttachments where ticketid = " & TicketID & ") "

            Return IOfromDB.ProcessSQLDataSet(sqlAttach, "")
        End Function

        Public Shared Function GetSubmitUserCompanyID(ByVal TicketID As Integer) As Integer
            Dim Sql As String = "select isnull(sfa.parentcompanyid, 0) from tblticket t left join tblactionrequest ar on ar.arid=t.arid left join tblsfausers u on u.userid=ar.submittedbyuserid left join tblsfas sfa on sfa.sfaid=u.sfaid where t.ticketid=" & TicketID

            Return IOfromDB.ProcessSQLInteger(Sql, "")
        End Function

        Public Shared Function GetSFAEmail(ByVal SfaID As Integer) As DataSet
            Dim Sql As String = "select replace(isnull(c.email, '-'), ',', ';') as email, replace(isnull(c.firstname + ' ' + c.lastname, '-'), ',', '') as contactname, replace(isnull(c.workphonenumber, '-'), ',', '') as sfaphone, replace(isnull(c.title, '-'), ',', '') as title from tblcontact c inner join tblrole r on r.rolename='Support Contact' and r.isactive=1 inner join tblpartyrole pr on pr.partyid=c.partyid and pr.roleid=r.roleid and pr.isactive=1 inner join tblsfas sfa on sfa.parentcompanyid=c.companyid where c.isactive=1 and sfa.sfaid=" & SfaID
            Return IOfromDB.ProcessSQLDataSet(Sql, "")
        End Function

        Public Shared Function GetHistoryDS(ByVal UserGroupName As String, ByVal TimeZoneName As String, ByVal TicketID As Integer) As DataSet
            Dim Sql As String
            If UserGroupName = "ISD" Or UserGroupName = "SFA" Then
                Sql = "select tt.logdate  as 'Log Date', '" & TimeZoneName & "' as '&shy;',  u.username as 'Update By', IsNull(a.actionName, '') as 'Event', " &
                        " ts.tktstatusname as 'Status', IsNull(tt.comment, '') as 'Comments', tt.ticketLogID from tblTicketTracking as tt left join " &
                        " tblActions a on tt.actionid = a.actionid left join tblticketstatus ts on tt.statusid = ts.tktstatusid left join tblUsers u on " &
                        " u.userid = tt.lastupdateuserid where ticketid= " & TicketID
            Else
                Sql = "select tt.logdate as 'Log Date', '" & TimeZoneName & "' as '&shy;', u.username as 'Update By', IsNull(a.actionName, '') as 'Event', " &
                        " ts.tktstatusname as 'Status', IsNull(tt.comment, '') as 'Comments' from tblTicketTracking as tt left join tblActions a on " &
                        " tt.actionid = a.actionid left join tblticketstatus ts on tt.statusid = ts.tktstatusid left join tblUsers u on " &
                        " u.userid = tt.lastupdateuserid where tt.isPublic = 1 and ticketid=" & TicketID
            End If
            Return IOfromDB.ProcessSQLDataSet(Sql, "")
        End Function

        Public Shared Function GetTicketsInfo(ByVal TicketID As Integer, ByVal DateFormatID As String) As DataSet
            Dim sql As String = ""
            sql = "select distinct t.ticketid, u.username, ar.submittedbyuserid, con2.email as submittinguseremail, con.contactid, " &
                    " con.firstname, con.lastname, pc.parentcompanyname, ar.companyid as parentcompanyid, isnull(ar.addressid, 0) as addressid, " &
                    " pa.addressname as nccondensedaddress, ad.address1 as ncaddress1, ad.address2 as ncaddress2, ad.city as nccity, " &
                    " ad.stateprovinceregion as ncstateprovinceregion, cou.countryname as nccountryname, ad.postalcode as ncpostalcode, ca.condensedaddress, " &
                    " ca.address1, ca.address2, ca.city, ca.stateprovinceregion, ca.postalcode, cou2.countryid, cou2.countryname as custcountry, " &
                    " t.sfaticketid, t.customerticketid, t.ticketcontactname, t.ticketcontactphonevoice, ar.requesttimezoneid, " &
                    " tz.timezoneshort,"

            If Len(DateFormatID) = 0 Then
                sql = sql & " CAST(Day(ar.requestdate) as varchar) + '/' + CAST(Month(ar.requestdate) as varchar) + '/' + CAST(Year(ar.requestdate) as varchar) + ' ' +  right(convert(varchar(19),ar.requestdate),8) As 'requestdate',"
                sql = sql & " CAST(Day(ar.requestdatelocal) as varchar) + '/' + CAST(Month(ar.requestdatelocal) as varchar) + '/' + CAST(Year(ar.requestdatelocal) as varchar) + ' ' +  right(convert(varchar(19),ar.requestdatelocal),8) As 'requestdatelocal',"
                sql = sql & " CAST(Day(t.createdate) as varchar) + '/' + CAST(Month(t.createdate) as varchar) + '/' + CAST(Year(t.createdate) as varchar) As 'createdate',"
                sql = sql & " CAST(Day(ar.datereceived) as varchar) + '/' + CAST(Month(ar.datereceived) as varchar) + '/' + CAST(Year(ar.datereceived) as varchar) As 'ar.datereceived',"
                sql = sql & " CAST(Day(t.closedate) as varchar) + '/' + CAST(Month(t.closedate) as varchar) + '/' + CAST(Year(t.closedate) as varchar) As 'closedate',"
            ElseIf DateFormatID = 1 Then
                sql = sql & " CAST(Day(ar.requestdate) as varchar) + '/' + CAST(Month(ar.requestdate) as varchar) + '/' + CAST(Year(ar.requestdate) as varchar) + ' ' +  right(convert(varchar(19),ar.requestdate),8) As 'requestdate',"
                sql = sql & " CAST(Day(ar.requestdatelocal) as varchar) + '/' + CAST(Month(ar.requestdatelocal) as varchar) + '/' + CAST(Year(ar.requestdatelocal) as varchar) + ' ' +  right(convert(varchar(19),ar.requestdatelocal),8) As 'requestdatelocal',"
                sql = sql & " CAST(Day(t.createdate) as varchar) + '/' + CAST(Month(t.createdate) as varchar) + '/' + CAST(Year(t.createdate) as varchar) As 'createdate',"
                sql = sql & " CAST(Day(ar.datereceived) as varchar) + '/' + CAST(Month(ar.datereceived) as varchar) + '/' + CAST(Year(ar.datereceived) as varchar) As 'datereceived',"
                sql = sql & " CAST(Day(t.closedate) as varchar) + '/' + CAST(Month(t.closedate) as varchar) + '/' + CAST(Year(t.closedate) as varchar) As 'closedate',"
            ElseIf DateFormatID = 2 Then
                sql = sql & " CAST(Month(ar.requestdate) as varchar) + '/' + CAST(Day(ar.requestdate) as varchar) + '/' + CAST(Year(ar.requestdate) as varchar) + ' ' +  right(convert(varchar(19),ar.requestdate),8) As 'requestdate',"
                sql = sql & " CAST(Month(ar.requestdatelocal) as varchar) + '/' + CAST(Day(ar.requestdatelocal) as varchar) + '/' + CAST(Year(ar.requestdatelocal) as varchar) + ' ' +  right(convert(varchar(19),ar.requestdatelocal),8) As 'requestdatelocal',"
                sql = sql & " CAST(Month(t.createdate) as varchar) + '/' + CAST(Day(t.createdate) as varchar) + '/' + CAST(Year(t.createdate) as varchar) As 'createdate',"
                sql = sql & " CAST(Month(ar.datereceived) as varchar) + '/' + CAST(Day(ar.datereceived) as varchar) + '/' + CAST(Year(ar.datereceived) as varchar) As 'datereceived',"
                sql = sql & " CAST(Month(t.closedate) as varchar) + '/' + CAST(Day(t.closedate) as varchar) + '/' + CAST(Year(t.closedate) as varchar) As 'closedate',"
            End If


            sql = sql & "ar.requestgmtoffsetstring,  t.tktstatusid, ts.tktstatusname, " &
                    " isnull(t.priorityid, 0) as priorityid, tp.priorityname, t.tkttypeid, art.artypename, t.escalationLevel, ar.assetid, " &
                    " a.assettagnum, a.serialnum, isnull(t.sfaid, 0) as sfaid,pc2.parentcompanyid as sfacompanyid, pc2.partyid as sfapartyid, " &
                    " pr.roleid as sfarole, t.replaceserial, t.workedonserial, t.csrid,isnull(con3.email, 'cc@global-serve.com') as csremail, " &
                    " t.sfapo, t.customerpo, t.hoursonsite, t.minsonsite, t.hourstravel, t.minstravel, t.appointmentdate, isnull(t.isbillable, 0) as isbillable, " &
                    " isnull(t.billamount, 0) as billamount, isnull(t.billamountcurrencyid, 0) as billamountcurrencyid, isnull(ar.gscomments, '') as gscomments, " &
                    " isnull(ar.arname, '') as arname, isnull(ar.ardesc, '') as ardesc, t.resolutionText, ar.serialnum as override_serialnum, " &
                    " ar.assettagnum as override_assettagnum from tblactionrequest ar left join tblticket t on t.arid=ar.arid left join tblusers u " &
                    " on u.userid=ar.submittedbyuserid left join tblcontact con2 on con2.userid=ar.submittedbyuserid left join tblcontact con on " &
                    " con.contactid=ar.affectedusercontactid left join tblparentcompanies pc on pc.parentcompanyid=ar.companyid left join tbladdress ad " &
                    " on ad.addressid=ar.addressid left join tblpartyaddress pa on pa.addressid=ad.addressid and pa.partyid=pc.partyid left join tblcountries " &
                    " cou on cou.countryid=ad.countryid left join tblcustomeraddresses ca on ca.customeraddressid=ar.addressid left join tblcountries cou2 " &
                    " on cou2.countryid=ca.countryid left join tbltimezones tz on tz.timezoneid=ar.requesttimezoneid left join tblticketstatus ts on " &
                    " ts.tktstatusid=t.tktstatusid left join tblpriority tp on tp.priorityid=t.priorityid left join tblactionrequesttype art " &
                    " on art.artypeid=t.tkttypeid left join gsnetp_tbl_assets a on a.assetid=ar.assetid left join tblsfas sfa on sfa.sfaid=t.sfaid left join " &
                    " tblparentcompanies pc2 on pc2.parentcompanyid=sfa.parentcompanyid left join tblpartyrole pr on pr.partyid=pc2.partyid left join " &
                    " tblcontact con3 on con3.contactid=t.csrid where t.ticketid = " & TicketID
            Return IOfromDB.ProcessSQLDataSet(sql, "")
        End Function

        Public Shared Function GetParentCompanyCustCount(ByVal ParentCompanyID As Integer) As Integer
            Dim sqlCust As String = "select count(customerid) from tblCustomers where parentcompanyid=" & ParentCompanyID
            Return IOfromDB.ProcessSQLInteger(sqlCust, "")
        End Function

        Public Shared Function Attachments(ByVal TicketID As Integer) As DataSet
            Dim Sql As String = "select daid, docid, daname, daurl, isnull(mimetypeicon, 'blank.gif') as mimetypeicon from tblDigitalAssets, " &
                                    " tblMimeTypes where tblMimeTypes.mimetypeid =* tblDigitalAssets.damimetypeid and daid in (select daid from " &
                                    " tblTicketAttachments where ticketid = " & TicketID & ") "
            Return IOfromDB.ProcessSQLDataSet(Sql, "")
        End Function

        Public Sub DeleteDigitalAsset(ByVal TicketID As Integer, ByVal DaId As String)
            Try

                Dim Sql As String = ""
                Sql = "delete from tblTicketAttachments where TicketId=" & TicketID & " and daid=" & DaId
                IOfromDB.ProcessSQLExecute(Sql, "")

                Sql = "delete from tblDigitalAssets where daid=" & DaId

                IOfromDB.ProcessSQLExecute(Sql, "")

            Catch ex As Exception
                Handler.WriteLogtoEViewer("GSOneSource", "Error " & ex.ToString)
            End Try
        End Sub

        Public Shared Function GetServerTimeZone(ByVal ServerTimeZone As String) As String
            Dim SQL As String = " select timezoneshort from tblTimeZones where isActive =1 and timezoneid= " & ServerTimeZone
            Return IOfromDB.ProcessSQLString(SQL, "")
        End Function

        Public Shared Function GetAssetManufacturer(ByVal ticketid As Integer, ByRef tmpmodel As String, ByRef ds As DataSet) As String

            Dim tmpmfgid As String
            tmpmodel = ""
            tmpmfgid = ""

            Dim AMSql As String = "select isnull(isnull(ao.manufacturerid, t.override_manufacturerid), -1) as manufacturerid, " &
             " isnull(isnull(ao.model, t.override_model), '') as model " &
             " from tblticket t " &
             " join tblactionrequest ar on ar.arid = t.arid " &
             " left join gsnetp_tbl_assets a on a.assetid = ar.assetid " &
             " left join gsnetp_tbl_assetsowned ao on ao.assetsownedid = a.assetsownedid " &
             " where t.ticketid= " & ticketid

            Dim AMds As DataSet = IOfromDB.ProcessSQLDataSet(AMSql, "")
            For Each row As DataRow In AMds.Tables(0).Rows
                tmpmodel = row(1).ToString()
                tmpmfgid = row(0).ToString()
                'Handler.WriteLogtoEViewer("GSOneSource", tmpmfgid)
            Next
            'Handler.WriteLogtoEViewer("GSOneSource", tmpmfgid)
            Dim rt As String = ""
            If Len(tmpmfgid) > 0 Then
                rt = IOfromDB.ProcessSQLString("select manufacturername from tblManufacturers where manufacturerid=" & tmpmfgid, "")
            End If
            'Handler.WriteLogtoEViewer("GSOneSource", rt)
            Return rt
        End Function

        Public Shared Function GetrequestedActionID() As Integer
            Dim SQL As String = "select actionid from tblActions where upper(actionname) ='EMAIL TICKET'"
            Return IOfromDB.ProcessSQLInteger(SQL, "")
        End Function

        Public Shared Function GetTicketStatusID(ByVal arCos As String, ByVal requestType As String) As DataSet
            Dim SQL As String = "select tktstatusid from tblticketstatus where isactive=1 and companyid=" & arCos & " and (artypeid in (" & requestType & ") or artypeid is null)"
            Return IOfromDB.ProcessSQLDataSet(SQL, "")
        End Function

        Public Shared Function GetStatusNull() As DataSet
            Dim SQL As String = "select ts.tktstatusid, ts.tktstatusname, isNULL(pc.parentcompanyname,'') as parentcompanyname from tblticketstatus ts" &
                 " left join tblparentcompanies pc on pc.parentcompanyid=ts.companyid" &
                 " where ts.isactive=1 and ts.companyid is null order by statusintvalue"
            Return IOfromDB.ProcessSQLDataSet(SQL, "")
        End Function

        Public Shared Function GetStatusCustomType(ByVal companyid As String, ByVal requestType As String) As DataSet
            Dim SQL As String = "select ts.tktstatusid, ts.tktstatusname, isNULL(pc.parentcompanyname,'') as parentcompanyname from tblticketstatus ts" &
                 " left join tblparentcompanies pc on pc.parentcompanyid=ts.companyid" &
                 " where ts.isactive=1 and ts.companyid in (" & companyid & ") and (artypeid in (" & requestType & ") or artypeid is null)" &
                 " order by statusintvalue"
            Return IOfromDB.ProcessSQLDataSet(SQL, "")
        End Function

        Public Shared Function GetStatusCustom(ByVal companyid As String) As DataSet
            Dim SQL As String = "select ts.tktstatusid, ts.tktstatusname, isNULL(pc.parentcompanyname,'') as parentcompanyname from tblticketstatus ts" &
                 " left join tblparentcompanies pc on pc.parentcompanyid=ts.companyid" &
                 " where ts.isactive=1 and ts.companyid in (" & companyid & ") and ts.artypeid is null order by statusintvalue"
            Return IOfromDB.ProcessSQLDataSet(SQL, "")
        End Function

        Public Shared Function GetStatusNothing(ByVal companyid As String) As DataSet
            Dim SQL As String = "select ts.tktstatusid, ts.tktstatusname, isNULL(pc.parentcompanyname,'') as parentcompanyname from tblticketstatus ts" &
                 " left join tblparentcompanies pc on pc.parentcompanyid=ts.companyid" &
                 " where ts.isactive=1 and ts.companyid in (" & companyid & ") order by statusintvalue"
            Return IOfromDB.ProcessSQLDataSet(SQL, "")
        End Function

#End Region

#Region "Order"

        Public Shared Function GetAllCurrencyList() As DataSet
            Dim sqlCurrency As String = "SELECT DISTINCT currencyname + '(' + symbol + ')'  AS currencyname, currencyid " &
             " from tblCurrencies where isActive=1 order by currencyname"
            Return IOfromDB.ProcessSQLDataSet(sqlCurrency, "")
        End Function

        Public Shared Function GetOrderList(ByVal FilterID As String, ByVal UserGroupName As String, ByVal UserSecurityLevel As String, ByVal UserID As String, ByVal CustomerID As String, ByVal SFAID As String, ByVal ParentCompanyID As String, ByVal totalmonthstodisplay As String, ByVal Currencyid As String, ByVal DateFormatID As String) As DataSet
            Dim sSQL, filterstring As String
            filterstring = ""
            If FilterID <> "" Then
                filterstring = GetFilterString(FilterID)
            End If

            sSQL = "SELECT DISTINCT  top(500) oi.SessionTrackingNumber as STrackNo, oi.OrderTrackingNumber as OTrackNo, oi.DestinationTrackingNumber as DTrackNo, " &
                          "CAST(oi.SessionTrackingNumber as varchar(15)) + '-' + CAST(oi.OrderTrackingNumber as varchar(15)) + '-' + CAST(oi.DestinationTrackingNumber as varchar(15)) As 'TrackingNo', '../../ordertracking/OrderTrackingDetail.aspx?STrackNo=' + CAST(oi.SessionTrackingNumber as varchar(15)) + '&OTrackNo=' + CAST(oi.OrderTrackingNumber as varchar(15)) + '&DTrackNo=' + CAST(oi.DestinationTrackingNumber as varchar(15)) as 'path', "

            sSQL = sSQL & " CASE o.OrderPO WHEN '' THEN '&shy;'  ELSE o.OrderPO END 'OrderPO',"
            sSQL = sSQL & " CASE od.DestinationPO WHEN '' THEN '&shy;' ELSE od.DestinationPO END 'DestPORef',"
            'sSQL = sSQL & "o.OrderDate as 'OrderDate',"
            If Len(DateFormatID) = 0 Then
                sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', o.OrderDate) as varchar) + '-->' + CAST(Day(o.OrderDate) as varchar) + '/' + CAST(Month(o.OrderDate) as varchar) + '/' + CAST(Year(o.OrderDate) as varchar) As 'OrderDate',"
            ElseIf DateFormatID = 1 Then
                sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', o.OrderDate) as varchar) + '-->' + CAST(Day(o.OrderDate) as varchar) + '/' + CAST(Month(o.OrderDate) as varchar) + '/' + CAST(Year(o.OrderDate) as varchar) As 'OrderDate',"
            ElseIf DateFormatID = 2 Then
                sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', o.OrderDate) as varchar) + '-->' + CAST(Month(o.OrderDate) as varchar) + '/' + CAST(Day(o.OrderDate) as varchar) + '/' + CAST(Year(o.OrderDate) as varchar) As 'OrderDate',"
            End If

            sSQL = sSQL & " od.ETA as 'ETS' ,"
            sSQL = sSQL & " ca.CondensedAddress As 'DeliveryAddress', os.OrderStatusCode As Status," &
             " CONVERT(DECIMAL(18,2), SUM(oi.LineItemTotal)) As Total, c.Symbol as CharSymbol, c.CurrencySymbol As Currency"

            Dim joins As String = " FROM tblOrders o, tblOrderDestinations od, tblCustomerAddresses ca," &
             " tblCurrencies c, tblOrderItems oi, tblOrderStatus os "

            If UserGroupName = "CUSTOMER" Then
                If UserSecurityLevel >= 5 Then

                    joins = joins & " , tblCustomers cust " &
                    " WHERE o.CustomerID in (select customerid from tblcustomers where parentcompanyid in (select parentcompanyid from tblparentcompanies" &
                    " where parentparentcompanyid=" & ParentCompanyID & ") or customerid=" & CustomerID & ")" &
                    " AND cust.CustomerID=o.CustomerID" &
                    " AND ca.CustomerID=o.CustomerID"

                ElseIf UserSecurityLevel >= 4 Then

                    joins = joins & " WHERE o.CustomerID=" & CustomerID &
                    " AND ca.CustomerID=o.CustomerID"

                Else
                    joins = joins & " WHERE o.CustomerID=" & CustomerID &
                     " AND ca.CustomerID=o.CustomerID"

                    joins = joins & " AND (o.OrderedByUserID=" & UserID &
                    " OR o.OrderCreatedByUserID=" & UserID & ")"
                End If

            ElseIf UserGroupName = "ISD" Then

                joins = joins & " , tblCustomers cust WHERE o.CustomerID=cust.CustomerID" &
                " AND ca.CustomerID=o.CustomerID"

            ElseIf UserGroupName = "SFA" Then

                joins = joins & " , tblCustomers cust WHERE o.CustomerID=cust.CustomerID" &
                " AND ca.CustomerID=o.CustomerID" &
                " AND oi.SFAID=" & SFAID

            End If

            If filterstring <> "" Then
                joins = joins & filterstring
            End If
            joins = joins & " AND oi.SessionTrackingNumber=o.SessionTrackingNumber" &
             " AND oi.OrderTrackingNumber=o.OrderTrackingNumber" &
             " AND (oi.ItemStatusID NOT IN (SELECT OrderItemStatusID FROM tblOrderItemStatus" &
             " WHERE OrderItemStatusCode='X') OR oi.ItemStatusID IS NULL)" &
             " AND od.SessionTrackingNumber=oi.SessionTrackingNumber" &
             " AND od.OrderTrackingNumber=oi.OrderTrackingNumber" &
             " AND od.DestinationTrackingNumber = oi.DestinationTrackingNumber" &
             " AND od.CustomerDestinationID=ca.CustomerAddressID" &
             " AND c.CurrencyID=o.OrderDestinationCurrencyID" &
             " AND os.OrderStatusID=o.OrderStatusID" &
             " AND (DATEDIFF(M, o.OrderDate,GETDATE()) < " & totalmonthstodisplay & ")"

            If Currencyid <> 0 Then
                joins = joins & " AND c.CurrencyID=" & Currencyid ' Added: 5 Oct 2010
            End If


            Dim grpby As String = " GROUP BY o.OrderPO, oi.SessionTrackingNumber, oi.OrderTrackingNumber," &
             " oi.DestinationTrackingNumber, o.OrderDate, od.DestinationPO, " &
             " ca.CondensedAddress, os.OrderStatusCode, c.Symbol, c.CurrencySymbol, od.ETA"
            'If UserGroupName = "CUSTOMER" And UserSecurityLevel < 5 Then
            'Else
            '    grpby = grpby & " cust.CustomerName,"
            'End If

            sSQL = sSQL & joins & grpby & " Order by 'TrackingNo' desc"

            Dim ds As DataSet = IOfromDB.ProcessSQLDataSet(sSQL, "")
            If Not ds Is Nothing Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    dr.BeginEdit()
                    If dr("DestPORef") Is DBNull.Value Or dr("DestPORef").ToString() = "" Then
                        dr("DestPORef") = " "

                    End If
                    dr.EndEdit()
                    dr.AcceptChanges()

                Next
            End If
            Return ds
        End Function

        'Public Shared Function GetOrderList(ByVal FilterID As String, ByVal UserGroupName As String, ByVal UserSecurityLevel As String, ByVal UserID As String, ByVal CustomerID As String, ByVal SFAID As String, ByVal ParentCompanyID As String, ByVal totalmonthstodisplay As String, ByVal Currencyid As String, ByVal DateFormatID As String, ByVal StartRow As Integer, ByVal PageSize As Integer) As DataSet
        '    Dim sSQL, filterstring As String
        '    filterstring = ""
        '    If FilterID <> "" Then
        '        filterstring = GetFilterString(FilterID)
        '    End If

        '    '-------------- Basic query START ------------
        '    sSQL = "SELECT DISTINCT oi.SessionTrackingNumber as STrackNo, oi.OrderTrackingNumber as OTrackNo, oi.DestinationTrackingNumber as DTrackNo, " & _
        '                  "CAST(oi.SessionTrackingNumber as varchar(15)) + '-' + CAST(oi.OrderTrackingNumber as varchar(15)) + '-' + CAST(oi.DestinationTrackingNumber as varchar(15)) As 'TrackingNo', '../../ordertracking/OrderTrackingDetail.aspx?STrackNo=' + CAST(oi.SessionTrackingNumber as varchar(15)) + '&OTrackNo=' + CAST(oi.OrderTrackingNumber as varchar(15)) + '&DTrackNo=' + CAST(oi.DestinationTrackingNumber as varchar(15)) as 'path', "

        '    sSQL = sSQL & " CASE o.OrderPO WHEN '' THEN '&shy;'  ELSE o.OrderPO END 'OrderPO',"
        '    sSQL = sSQL & " CASE od.DestinationPO WHEN '' THEN '&shy;' ELSE od.DestinationPO END 'DestPORef',"
        '    'sSQL = sSQL & "o.OrderDate as 'OrderDate',"
        '    If Len(DateFormatID) = 0 Then
        '        sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', o.OrderDate) as varchar) + '-->' + CAST(Day(o.OrderDate) as varchar) + '/' + CAST(Month(o.OrderDate) as varchar) + '/' + CAST(Year(o.OrderDate) as varchar) As 'OrderDate',"
        '    ElseIf DateFormatID = 1 Then
        '        sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', o.OrderDate) as varchar) + '-->' + CAST(Day(o.OrderDate) as varchar) + '/' + CAST(Month(o.OrderDate) as varchar) + '/' + CAST(Year(o.OrderDate) as varchar) As 'OrderDate',"
        '    ElseIf DateFormatID = 2 Then
        '        sSQL = sSQL & " '<!--' + CAST(DATEDIFF(s, '1/1/1970', o.OrderDate) as varchar) + '-->' + CAST(Month(o.OrderDate) as varchar) + '/' + CAST(Day(o.OrderDate) as varchar) + '/' + CAST(Year(o.OrderDate) as varchar) As 'OrderDate',"
        '    End If

        '    sSQL = sSQL & " od.ETA as 'ETS' ,"
        '    sSQL = sSQL & " ca.CondensedAddress As 'DeliveryAddress', os.OrderStatusCode As Status," & _
        '     " CONVERT(DECIMAL(18,2), SUM(oi.LineItemTotal)) As Total, c.Symbol as CharSymbol, c.CurrencySymbol As Currency" & _
        '     ", Row_Number() Over(Order By oi.SessionTrackingNumber desc) As RowNum"

        '    '------------- Basic query END -------------

        '    '------------ Joins query START ------------
        '    Dim joins As String = " FROM tblOrders o, tblOrderDestinations od, tblCustomerAddresses ca," & _
        '     " tblCurrencies c, tblOrderItems oi, tblOrderStatus os "
        '    If UserGroupName = "CUSTOMER" Then
        '        If UserSecurityLevel >= 5 Then
        '            joins = joins & " , tblCustomers cust " & _
        '            " WHERE o.CustomerID in (select customerid from tblcustomers where parentcompanyid in (select parentcompanyid from tblparentcompanies" & _
        '            " where parentparentcompanyid=" & ParentCompanyID & ") or customerid=" & CustomerID & ")" & _
        '            " AND cust.CustomerID=o.CustomerID" & _
        '            " AND ca.CustomerID=o.CustomerID"
        '        ElseIf UserSecurityLevel >= 4 Then
        '            joins = joins & " WHERE o.CustomerID=" & CustomerID & _
        '            " AND ca.CustomerID=o.CustomerID "
        '        Else
        '            joins = joins & " WHERE o.CustomerID=" & CustomerID & _
        '             " AND ca.CustomerID=o.CustomerID"
        '            joins = joins & " AND (o.OrderedByUserID=" & UserID & _
        '            " OR o.OrderCreatedByUserID=" & UserID & ")"
        '        End If
        '    ElseIf UserGroupName = "ISD" Then
        '        joins = joins & " , tblCustomers cust WHERE o.CustomerID=cust.CustomerID" & _
        '        " AND ca.CustomerID=o.CustomerID"
        '    ElseIf UserGroupName = "SFA" Then
        '        joins = joins & " , tblCustomers cust WHERE o.CustomerID=cust.CustomerID" & _
        '        " AND ca.CustomerID=o.CustomerID" & _
        '        " AND oi.SFAID=" & SFAID
        '    End If
        '    If filterstring <> "" Then
        '        joins = joins & filterstring
        '    End If
        '    joins = joins & " AND oi.SessionTrackingNumber=o.SessionTrackingNumber" & _
        '     " AND oi.OrderTrackingNumber=o.OrderTrackingNumber" & _
        '     " AND (oi.ItemStatusID NOT IN (SELECT OrderItemStatusID FROM tblOrderItemStatus" & _
        '     " WHERE OrderItemStatusCode='X') OR oi.ItemStatusID IS NULL)" & _
        '     " AND od.SessionTrackingNumber=oi.SessionTrackingNumber" & _
        '     " AND od.OrderTrackingNumber=oi.OrderTrackingNumber" & _
        '     " AND od.DestinationTrackingNumber = oi.DestinationTrackingNumber" & _
        '     " AND od.CustomerDestinationID=ca.CustomerAddressID" & _
        '     " AND c.CurrencyID=o.OrderDestinationCurrencyID" & _
        '     " AND os.OrderStatusID=o.OrderStatusID" & _
        '     " AND (DATEDIFF(M, o.OrderDate,GETDATE()) < " & totalmonthstodisplay & ")" & _
        '     " AND c.CurrencyID=" & Currencyid ' Added on 1 Oct 2010

        '    '------------ Joins query END ------------

        '    '--------- Group By query START ----------
        '    Dim grpby As String = " GROUP BY o.OrderPO, oi.SessionTrackingNumber, oi.OrderTrackingNumber," & _
        '     " oi.DestinationTrackingNumber, o.OrderDate, od.DestinationPO, " & _
        '     " ca.CondensedAddress, os.OrderStatusCode, c.Symbol, c.CurrencySymbol, od.ETA "
        '    'If UserGroupName = "CUSTOMER" And UserSecurityLevel < 5 Then
        '    'Else
        '    '    grpby = grpby & " cust.CustomerName,"
        '    'End If
        '    '---------- Group By query END -----------

        '    sSQL = sSQL & joins & grpby

        '    '-------------- Added: 1 Oct 2010 | Last Updated: 1 Oct 2010 -------------
        '    Dim custpage As String = " Where RowNum > " & StartRow.ToString() & " And RowNum <= " & (StartRow + PageSize).ToString()

        '    Dim StrPageCount As String = "select count(*) from(" & sSQL & ") as pageresult "

        '    Dim conn As SqlConnection = New SqlConnection(IOfromDB.thisDBString)
        '    conn.Open()
        '    Dim cmd As SqlCommand = New SqlCommand(StrPageCount, conn)
        '    RowCount = Convert.ToInt32(cmd.ExecuteScalar())

        '    sSQL = "select * from(" & sSQL & ") as pageresult " & custpage
        '    '-------------------------------------------------------------------------

        '    Dim ds As DataSet = IOfromDB.ProcessSQLDataSet(sSQL, "")
        '    If Not ds Is Nothing Then
        '        For Each dr As DataRow In ds.Tables(0).Rows
        '            dr.BeginEdit()
        '            If dr("DestPORef") Is DBNull.Value Or dr("DestPORef").ToString() = "" Then
        '                dr("DestPORef") = " "

        '            End If
        '            dr.EndEdit()
        '            dr.AcceptChanges()

        '        Next
        '    End If
        '    Return ds
        'End Function

        Public Shared Function GetOrderPieList(ByVal FilterID As String, ByVal UserGroupName As String, ByVal UserSecurityLevel As String, ByVal UserID As String, ByVal CustomerID As String, ByVal SFAID As String, ByVal ParentCompanyID As String, ByVal totalmonthstodisplay As String, ByVal Currencyid As String) As SqlDataReader
            Dim filterstring As String
            filterstring = ""
            Dim cmd As SqlCommand
            Dim dr As SqlDataReader

            If FilterID <> "" Then
                filterstring = GetFilterString(FilterID)
            End If

            cmd = New SqlCommand
            Dim joins As String = " FROM tblOrders o, tblOrderDestinations od, tblCustomerAddresses ca," &
                     " tblCurrencies c, tblOrderItems oi, tblOrderStatus os "

            If UserGroupName = "CUSTOMER" Then
                If UserSecurityLevel >= 5 Then

                    joins = joins & " , tblCustomers cust " &
                    " WHERE o.CustomerID in (select customerid from tblcustomers where parentcompanyid in (select parentcompanyid from tblparentcompanies" &
                    " where parentparentcompanyid=" & ParentCompanyID & ") or customerid= " & CustomerID & ")" &
                    " AND cust.CustomerID=o.CustomerID" &
                    " AND ca.CustomerID=o.CustomerID"

                ElseIf UserSecurityLevel >= 4 Then

                    joins = joins & " WHERE o.CustomerID=" & CustomerID & " AND ca.CustomerID=o.CustomerID"
                Else
                    joins = joins & " WHERE o.CustomerID=" & CustomerID & " AND ca.CustomerID=o.CustomerID"

                    joins = joins & " AND (o.OrderedByUserID=" & UserID & " OR o.OrderCreatedByUserID=" & UserID & ")"

                End If

            ElseIf UserGroupName = "ISD" Then

                joins = joins & " , tblCustomers cust WHERE o.CustomerID=cust.CustomerID" &
                " AND ca.CustomerID=o.CustomerID"

            ElseIf UserGroupName = "SFA" Then

                joins = joins & " , tblCustomers cust WHERE o.CustomerID=cust.CustomerID" &
                " AND ca.CustomerID=o.CustomerID" &
                " AND oi.SFAID=" & SFAID
            End If
            If filterstring <> "" Then
                joins = joins & filterstring
            End If
            joins = joins & " AND oi.SessionTrackingNumber=o.SessionTrackingNumber" &
             " AND oi.OrderTrackingNumber=o.OrderTrackingNumber" &
             " AND (oi.ItemStatusID NOT IN (SELECT OrderItemStatusID FROM tblOrderItemStatus" &
             " WHERE OrderItemStatusCode='X') OR oi.ItemStatusID IS NULL)" &
             " AND od.SessionTrackingNumber=oi.SessionTrackingNumber" &
             " AND od.OrderTrackingNumber=oi.OrderTrackingNumber" &
             " AND od.DestinationTrackingNumber = oi.DestinationTrackingNumber" &
             " AND od.CustomerDestinationID=ca.CustomerAddressID" &
             " AND c.CurrencyID=o.OrderDestinationCurrencyID" &
             " AND os.OrderStatusID=o.OrderStatusID" &
            " AND (DATEDIFF(M, o.OrderDate,GETDATE()) < " & totalmonthstodisplay & ")"

            If Currencyid <> 0 Then
                joins = joins & " AND currencyid = " & Currencyid
            End If

            Dim PieSql As String = "select os.OrderStatusCode As Status, COUNT(os.OrderStatusCode) As StatusCount "
            PieSql = PieSql & joins & " GROUP BY os.OrderStatusCode "

            cmd.CommandText = PieSql

            dr = IOfromDB.ProcessSQLCommandReader(cmd)

            Return dr

        End Function

        Public Shared Function GetOrderBarList(ByVal FilterID As String, ByVal UserGroupName As String, ByVal UserSecurityLevel As String, ByVal UserID As String, ByVal CustomerID As String, ByVal SFAID As String, ByVal ParentCompanyID As String, ByVal totalmonthstodisplay As String, ByVal Currencyid As String) As SqlDataReader
            Dim filterstring As String
            filterstring = ""
            Dim cmd As SqlCommand
            Dim param As SqlParameter
            Dim BarChartSql As String

            If FilterID <> "" Then
                filterstring = GetFilterString(FilterID)
            End If

            cmd = New SqlCommand
            Dim joins As String = " FROM tblOrders o, tblOrderDestinations od, tblCustomerAddresses ca," &
                     " tblCurrencies c, tblOrderItems oi, tblOrderStatus os "

            If UserGroupName = "CUSTOMER" Then
                If UserSecurityLevel >= 5 Then

                    joins = joins & " , tblCustomers cust " &
                    " WHERE o.CustomerID in (select customerid from tblcustomers where parentcompanyid in (select parentcompanyid from tblparentcompanies" &
                    " where parentparentcompanyid=" & ParentCompanyID & ") or customerid= " & CustomerID & ")" &
                    " AND cust.CustomerID=o.CustomerID" &
                    " AND ca.CustomerID=o.CustomerID"


                ElseIf UserSecurityLevel >= 4 Then
                    joins = joins & " WHERE o.CustomerID=" & CustomerID & " AND ca.CustomerID=o.CustomerID"
                Else
                    joins = joins & " WHERE o.CustomerID=" & CustomerID & " AND ca.CustomerID=o.CustomerID AND (o.OrderedByUserID=" & UserID & " OR o.OrderCreatedByUserID=" & UserID & ")"
                End If


            End If

            If filterstring <> "" Then
                joins = joins & filterstring
            End If
            joins = joins & " AND oi.SessionTrackingNumber=o.SessionTrackingNumber" &
             " AND oi.OrderTrackingNumber=o.OrderTrackingNumber" &
             " AND (oi.ItemStatusID NOT IN (SELECT OrderItemStatusID FROM tblOrderItemStatus" &
             " WHERE OrderItemStatusCode='X') OR oi.ItemStatusID IS NULL)" &
             " AND od.SessionTrackingNumber=oi.SessionTrackingNumber" &
             " AND od.OrderTrackingNumber=oi.OrderTrackingNumber" &
             " AND od.DestinationTrackingNumber = oi.DestinationTrackingNumber" &
             " AND od.CustomerDestinationID=ca.CustomerAddressID" &
             " AND c.CurrencyID=o.OrderDestinationCurrencyID" &
             " AND os.OrderStatusID=o.OrderStatusID"

            If Currencyid <> 0 Then
                joins = joins & " AND c.currencyid = " & Currencyid
            End If

            If totalmonthstodisplay < 24 Then
                BarChartSql = "SELECT SUBSTRING(tbltemp.CreatedMonth,0,4) as CreatedMonth, SUM(tbltemp.Total) * Max(ISNULL(erTo.ExchangeRateToUSDollars, 1)) / Max(ISNULL(erfrom.ExchangeRateToUSDollars, 1))  AS AmountTotal FROM tblManufacturerExchangeRates AS erfrom INNER JOIN tblManufacturers AS mfg ON " &
                " erfrom.ManufacturerID = mfg.ManufacturerID INNER JOIN tblManufacturerExchangeRates AS erTo ON mfg.ManufacturerID = erTo.ManufacturerID  " &
                "CROSS Join (SELECT   top (100) percent   CAST(DATENAME(mm, o.OrderDate) AS varchar) AS CreatedMonth, month(o.OrderDate)+ year(o.OrderDate)*12 as num,CONVERT(DECIMAL(18,2), SUM(oi.LineItemTotal)) As Total "

                BarChartSql = BarChartSql & joins & " AND (DATEDIFF(M, o.OrderDate,GETDATE()) < " & totalmonthstodisplay & ")"

                If Currencyid <> 0 Then
                    'Passing currencyid in tblManufacturerExchangeRates to convert the total amount into respective currency
                    'BarChartSql = BarChartSql & "GROUP BY o.OrderDate  ORDER BY o.OrderDate DESC) AS tbltemp where (erTo.currencyid= " & Currencyid & ") and (erfrom.currencyid=12) AND (upper(mfg.manufacturername) = 'EXRATEMFG') GROUP BY tbltemp.CreatedMonth order by max(num) "
                    BarChartSql = BarChartSql & "GROUP BY o.OrderDate  ORDER BY o.OrderDate DESC) AS tbltemp where (erTo.currencyid= 12) and (erfrom.currencyid=12) AND (upper(mfg.manufacturername) = 'EXRATEMFG') GROUP BY tbltemp.CreatedMonth order by max(num) "
                Else
                    'Passing USD currencyid in tblManufacturerExchangeRates to convert the total amount into USD by default (If All currencies are selected)
                    BarChartSql = BarChartSql & "GROUP BY o.OrderDate  ORDER BY o.OrderDate DESC) AS tbltemp where (erTo.currencyid=12) and (erfrom.currencyid=12) AND (upper(mfg.manufacturername) = 'EXRATEMFG') GROUP BY tbltemp.CreatedMonth order by max(num) "
                End If

            Else
                BarChartSql = "SELECT " &
                              "tbltemp.QCheck + ' ' + CAST(tbltemp.Years as varchar) as CreatedMonth, " &
                              "SUM(tbltemp.Total) * Max(ISNULL(erTo.ExchangeRateToUSDollars, 1)) / Max(ISNULL(erfrom.ExchangeRateToUSDollars, 1))  AS AmountTotal  " &
                              "FROM tblManufacturerExchangeRates AS erfrom INNER JOIN tblManufacturers AS mfg ON " &
                              " erfrom.ManufacturerID = mfg.ManufacturerID  " &
                              "INNER JOIN tblManufacturerExchangeRates AS erTo ON mfg.ManufacturerID = erTo.ManufacturerID  " &
                              "CROSS Join  " &
                              "(SELECT   top (100) percent   CAST(DATENAME(mm, o.OrderDate) AS varchar) AS CreatedMonth,  " &
                              "month(o.OrderDate)+ year(o.OrderDate)*12 as num, " &
                              " CASE MONTH(OrderDate)  " &
                              " WHEN '1' THEN '1st Q' " &
                              " WHEN '2' THEN '1st Q' " &
                              " WHEN '3' THEN '1st Q' " &
                              " WHEN '4' THEN '2nd Q' " &
                              " WHEN '5' THEN '2nd Q' " &
                              " WHEN '6' THEN '2nd Q' " &
                              " WHEN '7' THEN '3rd Q' " &
                              " WHEN '8' THEN '3rd Q' " &
                              " WHEN '9' THEN '3rd Q' " &
                              " WHEN '10' THEN '4th Q' " &
                              " WHEN '11' THEN '4th Q' " &
                              " WHEN '12' THEN '4th Q' " &
                              " END AS QCheck,  " &
                              " year(o.OrderDate) as Years, " &
                              " CONVERT(DECIMAL(18,2), SUM(oi.LineItemTotal)) As Total "

                BarChartSql = BarChartSql & joins & " AND (DATEDIFF(M, o.OrderDate,GETDATE()) < " & totalmonthstodisplay & ")"
                If Currencyid <> 0 Then
                    'Passing currencyid in tblManufacturerExchangeRates to convert the total amount into respective currency
                    'BarChartSql = BarChartSql & "GROUP BY o.OrderDate, year(o.OrderDate)  ORDER BY o.OrderDate DESC) AS tbltemp where (erTo.currencyid= " & Currencyid & ") and (erfrom.currencyid=12) AND (upper(mfg.manufacturername) = 'EXRATEMFG') GROUP BY tbltemp.QCheck + ' ' + CAST(tbltemp.Years as varchar) order by max(num) "
                    BarChartSql = BarChartSql & "GROUP BY o.OrderDate, year(o.OrderDate)  ORDER BY o.OrderDate DESC) AS tbltemp where (erTo.currencyid= 12) and (erfrom.currencyid=12) AND (upper(mfg.manufacturername) = 'EXRATEMFG') GROUP BY tbltemp.QCheck + ' ' + CAST(tbltemp.Years as varchar) order by max(num) "
                Else
                    'Passing USD currencyid in tblManufacturerExchangeRates to convert the total amount into USD by default (If All currencies are selected)
                    BarChartSql = BarChartSql & "GROUP BY o.OrderDate, year(o.OrderDate)  ORDER BY o.OrderDate DESC) AS tbltemp where (erTo.currencyid=12) and (erfrom.currencyid=12) AND (upper(mfg.manufacturername) = 'EXRATEMFG') GROUP BY tbltemp.QCheck + ' ' + CAST(tbltemp.Years as varchar) order by max(num) "
                End If


            End If

            cmd.CommandText = BarChartSql

            Return IOfromDB.ProcessSQLCommandReader(cmd)

        End Function

#End Region

#Region "General"

        Public Shared Function GetFilterString(ByVal FilterID As String) As String
            Dim cmd As SqlCommand
            Dim dr As SqlDataReader
            cmd = New SqlCommand
            cmd.CommandText = "select filterstring from tblOrderFilters where filterid=" & FilterID
            dr = IOfromDB.ProcessSQLCommandReader(cmd)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    GetFilterString = dr.Item(0)
                    dr.Close()
                End If
            End If
            cmd = Nothing
            GetFilterString = ""
        End Function

#End Region

    End Class
End Namespace


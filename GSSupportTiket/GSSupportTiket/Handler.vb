Imports Microsoft.VisualBasic
Imports System.Diagnostics
Imports System.IO
'--------------------------------------------------------------------------
' Description   :   This class contains the generaly used methods 
'                   by all other classes of this webservice. For example 
'                   writting Error to Event Viewer entry, writting error to 
'                   log files and creating some directories on specified 
'                   paths if they not exist.
'--------------------------------------------------------------------------
Namespace GSSupportTicket
    Public Class Handler
        Public Shared LogPath As String = "C:\Expception"

        '----Writting error message entry to Event viewer------.
        Public Shared Sub WriteLogtoEViewer(ByVal source As String, ByVal message As String)

            'if specified event source not exist before create it
            If (EventLog.SourceExists(source) = False) Then
                EventLog.CreateEventSource(source, source)
            End If

            'write passed message to souce in the event viewer.
            EventLog.WriteEntry(source, message)
        End Sub

        '------a function for writing error to Log file------
        Public Shared Sub WriteLogfile(ByVal ClassName As String, ByVal MethodName As String, ByVal Message As String)

            ' read the error log path from web.config file
            LogPath = "C:\Expception"


            ' create directories for specified path if not exist
            CreateDir(LogPath)
            ' create log file for writing log
            Dim errorlog As String = LogPath & ClassName & "_" & MethodName & "_" & Now().ToString("MMMM dd yyyy") & " " & Now().Hour & "_" & Now().Minute & "_" & Now().Second & ".txt"
            'create writter for writting log
            Dim writer As System.IO.StreamWriter = File.CreateText(errorlog)

            ' write error description to log file
            writer.WriteLine(Message)
            writer.Flush()
            writer.Close()
        End Sub

        'a function for creating directory for specified path if not exist.
        Public Shared Sub CreateDir(ByVal Path As String)
            If Directory.Exists(Path) = False Then
                Directory.CreateDirectory(Path)
            End If
        End Sub

        'a function for creating directory for specified path if not exist.
        Public Shared Sub DeleteDir(ByVal Path As String)
            If Directory.Exists(Path) = True Then
                Directory.Delete(Path, True)
            End If
        End Sub

        'function for reading XML from file and return it in string data type.
        Public Shared Function ReturnXMLStringfromFile(ByVal file1 As String) As String

            Dim filestream As StreamReader
            Dim readcontents As String

            filestream = File.OpenText(file1)
            readcontents = filestream.ReadToEnd()
            filestream.Close()
            Return readcontents

        End Function

        '------------------------------------------------------------
        'This function is used to write provid string to specified xml file.
        '------------------------------------------------------------
        Public Shared Function SaveXMLStringtoFile(ByVal strXML As String, ByVal fileName As String) As Boolean

            CreateDir(fileName.Substring(0, fileName.LastIndexOf("\")) & "\")
            Dim FileWriter As StreamWriter

            FileWriter = File.CreateText(fileName)
            FileWriter.WriteLine(strXML)
            FileWriter.Close()
            Return True
        End Function

        '-----------------------createPORequest----------------------
        ' This function is used to move the file from "FromPath" provided to "ToPath"
        '------------------------------------------------------------
        Public Shared Function MoveFile(ByVal FromPath As String, ByVal ToPath As String) As Boolean
            CreateDir(ToPath.Substring(0, ToPath.LastIndexOf("\")) & "\")
            File.Move(FromPath, ToPath)
            Return True
        End Function

    End Class
End Namespace

